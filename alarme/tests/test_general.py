from contextlib import ContextDecorator
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from io import StringIO
from pathlib import Path
from unittest import skipIf

from freezegun import freeze_time

from django import forms
from django.conf import settings
from django.contrib.auth.models import Permission
from django.core import mail
from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.management import call_command
from django.test import TestCase, tag
from django.test.utils import modify_settings, override_settings
from django.urls import reverse
from django.utils.dateformat import format as django_format
from django.utils.timezone import get_current_timezone, localtime, make_aware, now

from client.forms import ReferentForm
from client.models import (
    AdresseClient, AdressePresence, Alerte, Client, ClientType, ProfClient,
    Professionnel, Referent, ReferentTel,
)
from alarme.forms import MissionForm
from alarme.models import (
    Alarme, ArticleFacture, FacturationPolicyBase, Facture, Frais, Installation,
    InstallBase, Materiel, MaterielClient, Mission, ModeleAlarme, TypeAbo,
    TypeMateriel, TypeMission
)
from alarme.views_stats import Month
from benevole.forms import BenevoleForm
from benevole.models import Activite, Activites, Benevole
from common.export import openxml_contenttype
from common.models import Utilisateur
from common.test_utils import TempMediaRootMixin
from common.utils import canton_abrev

today = date.today()

NOT_SET = object()


class default_facturation_policy(ContextDecorator):
    def __enter__(self):
        self._old_policy = InstallBase.fact_policy
        InstallBase.fact_policy = FacturationPolicyBase()
        return self

    def __exit__(self, *exc):
        InstallBase.fact_policy = self._old_policy
        return False


class TestUtils:
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_client', 'change_client', 'view_alarme', 'change_alarme', 'change_mission',
                'view_facture', 'change_professionnel',
            ]))
        )
        cls.article = ArticleFacture.objects.create(
            code='ABO1', designation='Abonnement mensuel', prix=Decimal('37.45')
        )
        ArticleFacture.objects.create(
            code=settings.CODES_ARTICLE_INSTALLATION[0],
            designation="Taxe de raccordement : frais de dossier et d’installation",
            prix=80
        )
        ArticleFacture.objects.create(
            code="40.305",
            designation="Intervention samaritain",
            prix=60
        )
        cls.abo = TypeAbo.objects.create(nom='standard', article=cls.article)
        cls.modele = ModeleAlarme.objects.create(nom='Neat NOVO 4G')
        cls.modele.abos.add(cls.abo)
        cls.typem = TypeMission.objects.create(nom='Nouvelle installation', code='NEW')
        cls.type_emetteur = TypeMateriel.objects.create(nom='Émetteur')

    def create_client_with_installations(self, num: int, client_data=(), debut_install=None):
        """Avec bulk_create, les signaux ne sont pas envoyés pour la création des Installations."""
        clients = []
        for i in range(num):
            if len(client_data) > i and client_data[i]:
                client_data[i].setdefault('type_client', ['alarme'])
                clients.append(Client(nom='Donzé', prenom=f'Léa{i}', no_debiteur=i, **client_data[i]))
            else:
                clients.append(Client(nom='Donzé', prenom=f'Léa{i}', type_client=['alarme'], no_debiteur=i))
        clients = Client.objects.bulk_create(clients)
        alarmes = Alarme.objects.bulk_create(
            [Alarme(modele=self.modele, no_appareil=i) for i in range(num)]
        )
        return Installation.objects.bulk_create([
            Installation(
                client=clients[i], alarme=alarmes[i], abonnement=self.abo,
                date_debut=debut_install or (date.today() - timedelta(days=60)),
            ) for i in range(num)
        ])
        mail.outbox = []

    def nouvelle_install_par_mission(self, client=None, alarme=None):
        today = date.today()
        if client is None:
            client = Client.objects.create(
                nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont"
            )
        mission = Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10)
        )
        if alarme is None:
            alarme = Alarme.objects.create(modele=self.modele, no_serie=999)
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('mission-edit', args=[mission.pk]), data={
                'type_mission': mission.type_mission.pk,
                'client': client.pk,
                'delai': mission.delai,
                'alarme': alarme.pk,
                'abonnement': self.abo.pk,
                'effectuee': today.strftime('%d.%m.%Y'),
                'frais-TOTAL_FORMS': 0,
                'frais-INITIAL_FORMS': 0,
            })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json()['result'], 'OK')
        return client

    def assertJournalMessage(self, obj, message, user=NOT_SET):
        journal = obj.journaux.latest()
        self.assertEqual(journal.description, message)
        self.assertEqual(journal.qui, self.user if user is NOT_SET else user)


class ClientTests(TempMediaRootMixin, TestUtils, TestCase):

    def test_home_nouveaux_clients(self):
        Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3), type_client=['alarme'],
        )
        cl2 = Client.objects.create(
            nom="Dupont", prenom="Jeanne", npa='2000', localite='Neuchâtel', type_client=['alarme'],
        )
        Mission.objects.create(
            type_mission=self.typem,
            client=cl2,
            delai=today + timedelta(days=10)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertEqual(len(response.context['nouveaux']['clients']), 1)
        self.assertEqual(len(response.context['nouveaux']['missions']), 1)

    def test_client_list(self):
        self.client.force_login(self.user)
        client1 = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3), type_client=['alarme'],
            tel_1='078 888 88 44', envoi_facture=False,
        )
        client2 = Client.objects.create(
            nom="Dupônt", prenom="Mélissa", npa='2345', date_naissance=date(1948, 2, 24), type_client=['alarme'],
        )
        Client.objects.create(
            nom="Autre", prenom="Client", npa='2345', date_naissance=date(1950, 3, 21), type_client=['transport'],
        )
        install_old, install = self.create_client_with_installations(2)
        install_old.date_fin_abo = today - timedelta(days=10)
        install_old.save()
        response = self.client.get(reverse('clients'))
        self.assertQuerySetEqual(response.context['object_list'], [])
        # Filter by client name
        response = self.client.get(reverse('clients') + '?nom_prenom=pont')
        self.assertQuerySetEqual(response.context['object_list'], [client2])
        # Filter by tel
        response = self.client.get(reverse('clients') + '?tel=8844')
        self.assertQuerySetEqual(response.context['object_list'], [client1])
        # Filter by liste - sans envoi facture
        response = self.client.get(reverse('clients') + '?listes=no_fact')
        self.assertQuerySetEqual(response.context['object_list'], [client1])
        # Filter by liste - résiliation en cours
        response = self.client.get(reverse('clients') + '?listes=resil')
        self.assertQuerySetEqual(response.context['object_list'], [install_old.client])
        self.assertContains(response, f'résilié le {install_old.date_fin_abo.strftime("%d.%m.%Y")}')
        # Filter by liste - résiliation en cours
        response = self.client.get(reverse('clients') + '?listes=actifs')
        self.assertQuerySetEqual(response.context['object_list'], [install.client])

    def test_client_list_archives(self):
        # Créer install passée, client est maintenant seulement 'transport'
        install = self.create_client_with_installations(1)[0]
        install.date_fin_abo = today - timedelta(days=60)
        install.save()
        install.client.type_client = ['transport']
        install.client.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-archives'))
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('clients-archives') + '?nom_prenom=donz')
        self.assertQuerySetEqual(response.context['object_list'], [install.client])

    def test_client_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-new'))
        self.assertContains(
            response,
            '<input type="text" name="nom" maxlength="60" class="form-control" required="" id="id_nom">',
            html=True
        )
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'npalocalite': '2345 Petaouchnok',
            'langues': ['fr', 'it'],
            'date_naissance': '3.12.1945',
            'tel_1': '0551234567',
            'remarques_ext': '',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        client = Client.objects.get(nom='Dupond')
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual(client.type_client, ['alarme'])
        self.assertEqual(client.date_naissance, date(1945, 12, 3))
        self.assertEqual(client.tel_1, '055 123 45 67')
        self.assertJournalMessage(client, "Création du client")

    def test_client_edit(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3),
            type_client=[ClientType.TRANSPORT, ClientType.VISITE], langues=['fr'],
        )
        ProfClient.objects.create(
            client=client, professionnel=Professionnel.objects.create(nom="Docteur super", tel_1="088 888 88 99")
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-edit', args=[client.pk]), data={
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'npa': '2345',
            'localite': '',
            'npalocalite': '2345 Petaouchnok',
            'langues': ['fr', 'it'],
            'date_naissance': '3.11.1945',
            'situation_choix': 'Seul-e',
            'envoi_facture': 'on',
            'courrier_envoye': 'on',
            'remarques_ext': '',
        })
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        client.refresh_from_db()
        self.assertEqual(client.langues, ['fr', 'it'])
        self.assertEqual(client.situation_vie, 'Seul-e')
        self.assertEqual(
            set(client.type_client), set([ClientType.TRANSPORT, ClientType.VISITE, ClientType.ALARME])
        )
        self.assertEqual(client.courrier_envoye, date.today())
        self.maxDiff = None
        expected_changes = (
            "date de naissance (de «1945-12-03» à «1945-11-03»), "
            "langues (de «français» à «français, italien»), le courrier de bienvenue a "
            f"été envoyé (de «faux» à «{date.today().strftime('%Y-%m-%d')}»), ajout de "
            "localité («Petaouchnok»), ajout de situation de vie («Seul-e»)"
        )
        self.assertJournalMessage(client, f"Modification du client: {expected_changes}")
        # Alertes pour autres secteurs
        expected_msg = f"Modifications depuis l’application «alarme»: {expected_changes}"
        self.assertQuerySetEqual(
            client.alertes.all().values_list('cible', 'alerte', 'recu_le__date').order_by('cible'),
            [
                ('transport', expected_msg, date.today()),
                ('visite', expected_msg, date.today())
            ]
        )

    def test_client_edit_rue(self):
        """When rue changes, the journal includes previous and current address in description."""
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", date_naissance=date(1945, 12, 3),
            rue='chemin des Fleurs 3', npa='2345', localite='Quelquepart',
        )
        self.client.force_login(self.user)
        self.client.post(reverse('client-edit', args=[client.pk]), data={
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'rue': 'Avenue Magnifique 5',
            'npalocalite': '2999 Ailleurs',
            'langues': ['fr'],
            'date_naissance': '3.12.1945',
            'remarques_ext': '',
        })
        self.assertJournalMessage(
            client,
            "Ancienne adresse: chemin des Fleurs 3, 2345 Quelquepart<br>"
            "Nouvelle adresse: Avenue Magnifique 5, 2999 Ailleurs"
        )

    def test_client_edit_donnees_medic(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", date_naissance=date(1945, 12, 3),
            rue='chemin des Fleurs 3', npa='2345', localite='Quelquepart',
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-donneesmedicales', args=[client.pk]), data={
            'allergies': 'Aux noix',
            'diabete': 'False',
            'oxygene': 'True',
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            client,
            "Modification des données médicales (ajout de allergies («Aux noix»), "
            "ajout de diabète («faux»), ajout de oxygène («vrai»))"
        )

    def test_client_archive(self):
        # Client sans alarme peut être archivé si pas de mission ouverte.
        client = Client.objects.create(nom='Donzé', prenom='Léa', type_client=['alarme'])
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))
        mission = Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10)
        )
        self.assertFalse(client.can_be_archived(self.user, 'alarme'))
        mission.effectuee = date.today()
        mission.save()
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))

        install = self.create_client_with_installations(1)[0]
        referent = Referent.objects.create(client=install.client, nom='Schmid')
        self.assertFalse(install.client.can_be_archived(self.user, 'alarme'))
        install.date_fin_abo = today - timedelta(days=35)
        install.save()
        client = install.client
        self.assertFalse(client.can_be_archived(self.user, 'alarme'))
        install.retour_mat = today - timedelta(days=33)
        install.save()
        self.assertTrue(client.can_be_archived(self.user, 'alarme'))

        self.client.force_login(self.user)
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, reverse('clients'))
        client.refresh_from_db()
        referent.refresh_from_db()
        self.assertIsNotNone(client.archive_le)
        self.assertIsNotNone(referent.date_archive)
        # client-archive is a toggle, will be reactivated the second time
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, client.get_absolute_url())
        client.refresh_from_db()
        referent.refresh_from_db()
        self.assertIsNone(client.archive_le)
        self.assertIsNone(referent.date_archive)

    def test_client_install_passee(self):
        """
        Si la dernière installation est passée, elle apparaît quand même.
        """
        install = self.create_client_with_installations(1)[0]
        install.date_fin_abo = today - timedelta(days=5)
        install.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[install.client.pk]))
        self.assertContains(
            response,
            '<p class="mb-0 align-self-center">Appareil installé (depuis le {debut} <b>jusqu’au {fin}</b>): Neat NOVO 4G (-), abo standard</p>'.format(
                debut=install.date_debut.strftime('%d.%m.%Y'),
                fin=install.date_fin_abo.strftime('%d.%m.%Y')
            ),
            html=True
        )

    def test_client_derniere_alarme(self):
        install = self.create_client_with_installations(1, debut_install=date.today() - timedelta(days=10))[0]
        install.date_fin_abo = today - timedelta(days=5)
        install.save()
        install2 = Installation.objects.create(
            client=install.client, alarme=Alarme.objects.create(modele=self.modele, no_appareil='9876'),
            abonnement=self.abo, date_debut=date.today() - timedelta(days=5),
        )
        self.assertEqual(install.client.derniere_alarme(), install2)
        # Now test when installation_set is prefetched on queryset
        client = Client.objects.filter(pk=install.client_id).prefetch_related('installation_set')[0]
        self.assertEqual(client.derniere_alarme(), install2)

    def test_adresse_absence_edition(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme'])
        adr = AdresseClient.objects.create(client=client, hospitalisation=True)
        AdressePresence.objects.create(
            adresse=adr, depuis=today - timedelta(days=10), jusqua=today - timedelta(days=2)
        )
        pres = AdressePresence.objects.create(adresse=adr, depuis=today - timedelta(days=10))
        self.client.force_login(self.user)
        # test home tab
        response = self.client.get(reverse('home'))
        self.assertContains(response, 'Absences/Hospit. <span class="badge bg-secondary">1</span>')
        response = self.client.post(reverse('client-absence-edit', args=[client.pk, pres.pk]), data={
            'jusqua': '',
            'dernier_contact': today.strftime('%Y-%m-%d'),
            'remarque': 'Toujours absent',
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page#absences'})
        self.assertJournalMessage(
            client,
            f"Suivi d’absence (dernier contact: {today.strftime('%d.%m.%Y')}, remarque: Toujours absent)")

    def test_referent_recoit_courrier(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        repondant = Referent.objects.create(client=client, nom='Schmid', repondant=1)
        referent = Referent.objects.create(client=client, nom='Dupond', referent=1)
        self.assertTrue(repondant.recoit_courrier())
        self.assertFalse(referent.recoit_courrier())

    def test_contact_facturation_unique_par_type(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', facturation_pour=['al-tout'])
        form_data = {
            'client': str(client.pk), 'nom': 'Hirz',
            'rue': 'Rue des Fleurs 4', 'npalocalite': '2000 Neuchâtel',
            'facturation_pour': ['al-tout'],
            'referenttel_set-INITIAL_FORMS': 0, 'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(client=client, data=form_data, initial={})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'facturation_pour': ['Il existe déjà un contact de facturation pour ce client et cet item']}
        )
        form = ReferentForm(client=client, instance=referent, data=form_data, initial={})
        self.assertTrue(form.is_valid())
        # OK si un contact de facturation est archivé
        referent.date_archive = date.today()
        referent.save()
        form = ReferentForm(client=client, data=form_data, initial={})
        self.assertTrue(form.is_valid())

    def test_professionnel_new_from_client(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-professionnel-add', args=[client.pk]))
        response = self.client.post(reverse('client-professionnel-add', args=[client.pk]), data={
            'type_pro': 'Médecin',
            'nom': 'Dr Maboule',
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'tel_1': '076 111 11 22',
            'remarque_client': "Pour madame",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.profclient_set.count(), 1)
        self.assertEqual(client.profclient_set.first().remarque, "Pour madame")
        self.assertJournalMessage(client, "Ajout du professionnel «Dr Maboule (Petaouchnok, Médecin)» (Pour madame)")
        # Now test link to an existing prof.
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        response = self.client.post(reverse('client-professionnel-add', args=[client.pk]), data={
            'professionnel': 'Dr Jekill',
            'professionnel_pk': str(pro.pk),
            'remarque_client': "Pour madame",
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.profclient_set.count(), 2)
        self.assertEqual(client.profclient_set.get(professionnel__nom='Dr Jekill').remarque, "Pour madame")
        self.assertJournalMessage(client, "Ajout du professionnel «Dr Jekill (Petaouchnok, Médecin)» (Pour madame)")

    def test_alerte_edit(self):
        client = Client.objects.create(nom="Dupond", prenom="Julie")
        with (Path(__file__).parent / 'alarmpost.pdf').open(mode='rb') as fh:
            alerte = Alerte.objects.create(
                client=client, cible=ClientType.ALARME, fichier=File(fh, name='alarmpost.pdf'),
                alerte='Remarque du fichier',
                recu_le=make_aware(datetime(2022, 8, 8, 12, 0))
            )
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-alerte-edit', args=[alerte.pk]), data={
            'traite_le': 'on',
            'remarque': 'Tout est OK',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        alerte.refresh_from_db()
        self.assertEqual(alerte.traite_le.date(), today)
        self.assertEqual(alerte.remarque, 'Tout est OK')
        self.assertEqual(alerte.par, self.user)
        self.assertJournalMessage(client, "Alerte traitée: Tout est OK")

    def test_installation_new_from_client(self):
        # deprecated: new install should go through new intervention
        client1 = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        self.client.force_login(self.user)
        new_install_url = reverse('install-new') + f'?client={client1.pk}'
        response = self.client.get(new_install_url)
        self.assertNotContains(response, "Changement d’alarme")
        self.assertContains(response, "Client: <b>Dupond Ladislas</b>")
        response = self.client.post(new_install_url, data={
            'client': client1.pk,
            'alarme': alarme1.pk,
            'alarme_select': 'Novo 4G',
            'abonnement': self.abo.pk,
            'date_debut': today.strftime('%d.%m.%Y'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})

    def test_installation_new_from_alarme(self):
        # deprecated: new install should go through new intervention
        client1 = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        self.client.force_login(self.user)
        new_install_url = reverse('install-new') + f'?alarme={alarme1.pk}'
        response = self.client.get(new_install_url)
        self.assertNotContains(response, "Changement d’appareil")
        self.assertNotContains(response, "id_alarme_select")
        self.assertContains(response, "Appareil: <b>Neat NOVO 4G (-)</b>")
        response = self.client.post(new_install_url, data={
            'client': client1.pk,
            'alarme': alarme1.pk,
            'abonnement': self.abo.pk,
            'date_debut': today.strftime('%d.%m.%Y'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})

    def test_installation_edit(self):
        install = self.create_client_with_installations(1, debut_install=date(2023, 1, 10))[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('install-edit', args=[install.pk]))
        self.assertContains(response, 'Client: <b>Donzé Léa0</b>')
        response = self.client.post(reverse('install-edit', args=[install.pk]), data={
            'client': install.client.pk,
            'abonnement': install.abonnement.pk,
            'date_debut': "2023-01-03",
            'date_fin_abo': "",
            'motif_fin': "",
            'retour_mat': "",
            'remarques': "Correction début",
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertJournalMessage(
            install.client,
            "Modification de l’installation alarme (date d’installation "
            "(de «2023-01-10» à «2023-01-03»), ajout de remarques («Correction début»))"
        )

    def test_installation_close_with_mission(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('install-close', args=[install.pk]))
        self.assertContains(response, 'Client: <b>Donzé Léa0</b>')
        in_eight_days = today + timedelta(days=8)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            # Form install
            'date_fin_abo': today,
            'motif_fin': 'décès',
            'remarques': 'Annoncé ce jour par sa fille',
            # Form mission
            'interv_cb': 'on',
            'type_mission': TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL').pk,
            'description': 'Texte',
            'delai': in_eight_days.strftime('%Y-%m-%d'),
            'benevole': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.date_fin_abo, today)
        self.assertEqual(install.motif_fin, 'décès')
        self.assertIsNone(install.retour_mat)
        self.assertJournalMessage(
            install.client,
            f"Fin de l’abonnement le {today.strftime('%d.%m.%Y')}, motif Décès, Annoncé ce jour par sa fille"
        )
        mission = install.client.mission_set.first()
        self.assertEqual(mission.type_mission.nom, 'Désinstallation')
        self.assertIsNone(mission.effectuee)

    def test_installation_close_without_mission(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            # Form install
            'date_fin_abo': today,
            'motif_fin': 'décès',
            'mat_achete': '',
            'remarques': '',
            # Form mission
            'interv_cb': '',
            'type_mission': TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL').pk,
            'description': '',
            'delai': '',
            'benevole': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.client.mission_set.count(), 0)

    def test_installation_close_mat_achete(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.post(reverse('install-close', args=[install.pk]), data={
            'date_fin_abo': today + timedelta(days=3),
            'motif_fin': 'décès',
            'mat_achete': 'on',
            'remarques': '',
            'interv_cb': '',
        })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        install.refresh_from_db()
        self.assertEqual(install.client.mission_set.count(), 0)
        self.assertEqual(install.alarme.date_archive, today + timedelta(days=3))

    def test_installation_close_cancel(self):
        install = self.create_client_with_installations(1)[0]
        install.date_fin_abo = today + timedelta(days=3)
        install.motif_fin = 'autre'
        install.remarques = "Ne veut plus"
        install.save()
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-edit', args=[install.client.pk]))
        self.assertContains(response, "Annuler…</button>")
        response = self.client.post(
            reverse('install-close-cancel', args=[install.pk]),
            data={'comment': "Ne voulait plus. Mais a changé d’avis."}
        )
        install.refresh_from_db()
        self.assertIsNone(install.date_fin_abo)
        self.assertEqual(install.motif_fin, "")
        self.assertJournalMessage(
            install.client,
            'La résiliation a été annulée: Ne voulait plus. Mais a changé d’avis.'
        )

    def test_ajout_emetteur(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        emetteur = Materiel.objects.create(type_mat=self.type_emetteur)
        abo_autre = TypeAbo.objects.create(
            nom='Autre abo',
            article=ArticleFacture.objects.create(
                code='xxO1', designation='Autre', prix=Decimal('2')
            )
        )
        abo_emetteur = TypeAbo.objects.create(
            nom='Émetteur supp.',
            article=ArticleFacture.objects.create(
                code='EMO1', designation='Émetteur', prix=Decimal('11.30')
            )
        )
        self.type_emetteur.abos.add(abo_emetteur)
        self.client.force_login(self.user)
        post_data = {
            'materiel': emetteur.pk,
            'materiel_select': 'Émet',
            'abonnement': abo_autre.pk,
            'date_debut': '',  # Date manquante
        }
        url = reverse('materielclient-new', args=[client.pk])
        response = self.client.post(url, data=post_data)
        self.assertEqual(
            response.context['form'].errors, {
                'date_debut': ['Ce champ est obligatoire.'],
                'abonnement': ['Ce matériel n’est pas compatible avec l’abonnement Autre abo'],
            }
        )
        post_data.update({'date_debut': today.replace(day=1), 'abonnement': abo_emetteur.pk})
        response = self.client.post(url, data=post_data)
        if 'html' in response['Content-Type']:
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})

    def test_ajout_emetteur_facturable(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        article = ArticleFacture.objects.create(
            code='ACH1', designation='Émetteur achat', prix=Decimal('25.00')
        )
        type_mat = TypeMateriel.objects.create(nom='Émetteur achat', article_achat=article)
        emetteur = Materiel.objects.create(type_mat=type_mat)
        self.client.force_login(self.user)
        post_data = {
            'materiel': emetteur.pk,
            'materiel_select': 'Émet',
            'type_mat': '',
            'abonnement': '',
            'date_debut': date.today() - timedelta(days=10),
        }
        response = self.client.post(reverse('materielclient-new', args=[client.pk]), data=post_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertEqual(client.factures.first().montant, Decimal('25.00'))

    def test_impression_questionnaire(self):
        install = self.create_client_with_installations(1)[0]
        client = install.client
        Referent.objects.create(client=client, nom='Schmid', repondant=1)
        client.samaritains = 2
        client.save()
        ProfClient.objects.create(
            client=client, priorite=1,
            professionnel=Professionnel.objects.create(nom="Supersama", type_pro='Sama')
        )
        ProfClient.objects.create(
            client=client, priorite=2,
            professionnel=Professionnel.objects.create(nom="Sama2", type_pro='Sama')
        )
        Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        url = reverse('client-questionnaire', args=[client.pk])
        response = self.client.get(url)
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_signature_questionnaire(self):
        png_data = (
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAA"
            "C0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII="
        )
        install = self.create_client_with_installations(1)[0]
        Mission.objects.create(
            client=install.client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        url = reverse('client-questionnaire-signer', args=[install.client.pk])
        response = self.client.get(url)
        self.assertContains(response, "<iframe")
        self.assertContains(response, "<canvas")
        # Test sig_data absent
        response = self.client.post(url, data={'sig_data': '', 'lieu': 'Dombresson'})
        self.assertContains(response, "Une erreur a été détectée")
        response = self.client.post(url, data={'sig_data': png_data, 'lieu': 'Dombresson'})
        self.assertRedirects(response, install.client.get_absolute_url())
        quest = install.client.fichiers.first()
        self.assertEqual(quest.titre, f"Questionnaire signé du {today.strftime('%d.%m.%Y')}")
        self.assertEqual(
            quest.fichier.name,
            f"clients/questionnaire_donze-lea0_{today.strftime('%Y_%m_%d')}.pdf"
        )

    def test_client_contrat(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-contrat', args=[install.client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_signature_contrat(self):
        png_data = (
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAA"
            "C0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII="
        )
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        url = reverse('client-contrat-signer', args=[install.client.pk])
        response = self.client.get(url)
        self.assertContains(response, "<iframe")
        self.assertContains(response, "<canvas")
        # Test sig_data absent
        response = self.client.post(url, data={'sig_data_cr': '', 'lieu': 'Dombresson'})
        self.assertContains(response, "Une erreur a été détectée")
        response = self.client.post(url, data={
            'sig_data_cr': png_data, 'sig_data_cl': png_data, 'lieu': 'Dombresson'
        })
        self.assertRedirects(response, install.client.get_absolute_url())
        quest = install.client.fichiers.first()
        self.assertEqual(quest.titre, f"Contrat signé du {today.strftime('%d.%m.%Y')}")
        self.assertEqual(
            quest.fichier.name,
            f"clients/contrat_donze-lea0_{today.strftime('%Y_%m_%d')}.pdf"
        )

    def test_impression_courrier(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), type_client=['alarme'],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-courrier', args=[client.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_impression_courrier_referent(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345', localite="Petaouchnok",
            date_naissance=date(1945, 12, 3), type_client=['alarme'],
        )
        referent = Referent.objects.create(
            client=client, nom='Schmid', prenom='Esther', salutation='F', referent=1
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-referent-courrier', args=[client.pk, referent.pk]))
        self.assertEqual(response['Content-Type'], 'application/pdf')


class MissionTests(TestUtils, TestCase):
    def test_mission_new(self):
        install = self.create_client_with_installations(1)[0]
        self.client.force_login(self.user)
        in4days = today + timedelta(days=4)
        form_data = {
            'description': "Installation chez Mme Trucmuche",
            'type_mission': TypeMission.objects.create(nom='Installation').pk,
            'planifiee_0': in4days.strftime('%d.%m.%Y'),
            'planifiee_1': '15:30',
            'delai': (today + timedelta(days=10)).strftime('%d.%m.%Y'),
        }
        response = self.client.post(reverse('mission-new', args=[install.client_id]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        journal = install.client.journaux.latest()
        self.assertEqual(journal.description, 'Nouvelle intervention: Installation: Installation chez Mme Trucmuche')
        self.assertEqual(journal.qui, self.user)
        mission = Mission.objects.get(client=install.client)
        self.assertEqual(
            localtime(mission.planifiee),
            datetime.combine(in4days, time(15, 30), tzinfo=get_current_timezone())
        )
        # Édition
        benevole = Benevole.objects.create(
            nom='Valjean', prenom='Jean', npa='2300', activites=[Activites.INSTALLATION]
        )
        form_data.update({
            'benevole': benevole.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            install.client,
            f'Modification d’une intervention (ajout de effectuée le («{today.strftime("%Y-%m-%d")}»), '
            'ajout de bénévole («Valjean Jean»))'
        )
        autre_benevole = Benevole.objects.create(
            nom='Miche', prenom='Cosette', npa='2300', activites=[Activites.INSTALLATION]
        )
        form_data['benevole'] = autre_benevole.pk
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=form_data)
        self.assertJournalMessage(
            install.client,
            'Modification d’une intervention (bénévole (de «Valjean Jean» à «Miche Cosette»))'
        )

    def test_mission_form_validation(self):
        # n° serie/abonnement manquants:
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        alarme = Alarme.objects.create(modele=self.modele, no_serie=999)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        mission_data = {
            'type_mission': self.typem.pk,
            'delai': mission.delai,
            'abonnement': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        for post_data in [
            {**mission_data, 'effectuee': today.strftime('%d.%m.%Y')},
        ]:
            form = MissionForm(instance=mission, data=post_data)
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors,
                {
                    'alarme_select': ['Vous devez indiquer l’appareil installé/à installer.'],
                    'abonnement': ['Vous devez indiquer l’abonnement choisi.'],
                }
            )
        # Alarme ne correspond pas à l'abo
        mission.refresh_from_db()
        mission_data.update({
            'alarme_select': 'Novo 4G',
            'alarme': alarme.pk,
            'abonnement': TypeAbo.objects.create(nom='autre', article=self.article).pk,
        })
        form = MissionForm(instance=mission, data=mission_data)
        self.assertIn('alarme', form.fields)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'alarme_select': ['L’appareil Neat NOVO 4G (999) n’est pas compatible avec l’abonnement autre']}
        )
        # Pas de date effectuée dans le futur
        mission_data.update({'abonnement': self.abo.pk, 'effectuee': date.today() + timedelta(days=10)})
        form = MissionForm(instance=mission, data=mission_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'effectuee': ['Il n’est pas autorisé de mettre une date d’intervention effectuée dans le futur']}
        )

    def test_mission_nouvelle_installation(self):
        """
        Quand une mission de nouvelle installation est effectuée, une nouvelle
        instance Installation est automatiquement créée.
        """
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        alarme = Alarme.objects.create(modele=self.modele, no_serie=999, chez_benevole=benev)

        self.client.force_login(self.user)
        edit_url = reverse('mission-edit', args=[mission.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, "N° de série (SN)")

        mission_data = {
            'type_mission': self.typem.pk,
            'delai': mission.delai,
            'effectuee': '',
            'alarme_select': 'Novo 4G',
            'alarme': alarme.pk,
            'abonnement': self.abo.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(edit_url, data=mission_data)
        self.assertEqual(response.json()['result'], 'OK')
        alarme.refresh_from_db()
        # L'appareil est toujours considéré chez le bénévole tant que la mission
        # n'est pas déclarée comme effectuée.
        self.assertIsNotNone(alarme.chez_benevole)
        mission.refresh_from_db()
        self.assertEqual(mission.alarme, alarme)
        response = self.client.get(edit_url)
        self.assertContains(
            response,
            f'<input type="hidden" name="alarme" value="{alarme.pk}" '
            'data-jsonurl="/alarmes/json/" class="form-control" id="id_alarme">'
        )
        self.assertContains(
            response,
            '<input type="text" name="alarme_select" value="Neat NOVO 4G (999)" class="form-control autocomplete"'
            ' autocomplete="off" data-searchurl="/alarmes/search/" data-pkfield="alarme" id="id_alarme_select">'
        )

        # Déclarer comme effectuée
        mission_data['effectuee'] = today.strftime('%d.%m.%Y')
        response = self.client.post(edit_url, data=mission_data)
        self.assertEqual(client.alarme_actuelle().alarme, alarme)
        alarme.refresh_from_db()
        self.assertIsNone(alarme.chez_benevole)
        journal = client.journaux.latest()
        self.assertEqual(
            journal.description,
            'Installation d’un nouvel appareil (Neat NOVO 4G (999), standard)'
        )

    def test_mission_change_alarme(self):
        """
        Quand une mission de changement d’installation est effectuée, l'ancienne
        instance est close et une nouvelle instance Installation est automatiquement créée.
        """
        article = ArticleFacture.objects.create(
            code='ABO2', designation='Abo mensuel 2', prix=Decimal('25.00')
        )
        abo_autre = TypeAbo.objects.create(nom='spécial', article=article)
        self.modele.abos.add(abo_autre)
        install = self.create_client_with_installations(1)[0]
        client = install.client
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        typem = TypeMission.objects.create(nom='Changement d’installation', code='CHANGE')
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=typem
        )
        nouvelle_alarme = Alarme.objects.create(modele=self.modele, no_serie=1999, chez_benevole=benev)
        self.client.force_login(self.user)
        response = self.client.get(reverse('mission-edit', args=[mission.pk]))
        self.assertContains(response, "N° de série (SN)")
        self.assertContains(response, f'<option value="{self.abo.pk}" selected>standard</option>', html=True)
        post_data = {
            'type_mission': mission.type_mission.pk,
            'delai': mission.delai,
            'alarme_select': 'Novo 4G',
            'alarme': nouvelle_alarme.pk,
            'abonnement': abo_autre.pk,
            'effectuee': today.strftime('%d.%m.%Y'),
            'benevole': benev.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(client.alarme_actuelle().alarme, nouvelle_alarme)
        journal = client.journaux.latest()
        self.assertEqual(
            journal.description,
            'Changement d’appareil, ancien: Neat NOVO 4G (-), nouveau: Neat NOVO 4G (1999)\n'
            'Changement d’abonnement, ancien: standard, nouveau: spécial'
        )
        install.refresh_from_db()
        self.assertEqual(install.date_fin_abo, today - timedelta(days=1))
        self.assertEqual(install.retour_mat, today)
        self.assertEqual(install.alarme.chez_benevole, benev)
        new_install = install.client.installation_set.get(date_fin_abo__isnull=True)
        self.assertEqual(new_install.date_debut, today)
        self.assertEqual(new_install.alarme, nouvelle_alarme)
        self.assertEqual(new_install.abonnement, abo_autre)
        nouvelle_alarme.refresh_from_db()
        self.assertIsNone(nouvelle_alarme.chez_benevole)
        # Réédition après effectuée
        del post_data['alarme_select']
        del post_data['alarme']
        del post_data['abonnement']
        post_data['km'] = 15
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        mission.refresh_from_db()
        self.assertEqual(mission.km, 15)

    def test_mission_remove_alarme(self):
        install = self.create_client_with_installations(1)[0]
        client = install.client
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        typem = TypeMission.objects.create(nom='Désinstallation', code='UNINSTALL')
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=typem,
            benevole=benev,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-edit', args=[mission.pk]), data={
            'type_mission': mission.type_mission.pk,
            'delai': mission.delai,
            'effectuee': today.strftime('%d.%m.%Y'),
            'benevole': benev.pk,
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')
        install.refresh_from_db()
        self.assertEqual(install.retour_mat, today)
        self.assertEqual(install.alarme.chez_benevole, benev)

    def test_mission_delete(self):
        self.user.user_permissions.add(Permission.objects.get(codename='delete_mission'))
        client = Client.objects.create(nom='Donzé', prenom='Léa', no_debiteur=1)
        mission = Mission.objects.create(
            client=client,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        mission_sans_client = Mission.objects.create(
            client=None,
            delai=today + timedelta(days=10),
            type_mission=self.typem
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-delete', args=[mission.pk]), data={})
        self.assertQuerySetEqual(Mission.objects.filter(pk=mission.pk), [])
        self.assertJournalMessage(client, "Annulation de l’intervention «Nouvelle installation»")
        response = self.client.post(reverse('mission-delete', args=[mission_sans_client.pk]), data={})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertQuerySetEqual(Mission.objects.filter(pk=mission_sans_client.pk), [])


class MaterielTests(TestUtils, TestCase):
    def test_materiel_list(self):
        mat1 = Materiel.objects.create(type_mat=self.type_emetteur)
        archived = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='9988', date_archive=date(2022, 1, 1))
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-liste'))
        self.assertQuerySetEqual(response.context['object_list'], [mat1])
        response = self.client.get(reverse('materiel-liste') + '?archived=on')
        self.assertQuerySetEqual(response.context['object_list'], [archived])

    def test_alarme_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarme-new'))
        self.assertContains(response, '<h2>Nouvel appareil</h2>')
        response = self.client.post(reverse('alarme-new'), data={
            'modele': self.modele.pk,
            'no_appareil': '1234',
            'no_serie': 'xy553322',
            'date_achat': '12.4.2020',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('alarmes'))
        alarme = Alarme.objects.get(no_serie='xy553322')
        self.assertIsNotNone(alarme.modele)
        journal = alarme.journaux.latest()
        self.assertEqual(journal.description, "Création de l’appareil")
        self.assertEqual(journal.qui, self.user)

    def test_alarme_edit_benev(self):
        alarme = Alarme.objects.create(modele=self.modele, no_serie='123456')
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        self.client.force_login(self.user)
        response = self.client.post(reverse('alarme-edit', args=[alarme.pk]), data={
            'modele': self.modele.pk,
            'no_serie': '123456',
            'chez_benevole': benev.pk,
        })
        self.assertRedirects(response, reverse('alarmes'))
        self.assertJournalMessage(alarme, "Modification de l’appareil: chez le bénévole Duplain Irma")

    def test_alarmes_list(self):
        install = self.create_client_with_installations(1)[0]
        alarme1 = Alarme.objects.create(modele=self.modele, no_serie='123456')
        archived = Alarme.objects.create(modele=self.modele, no_serie='123456', date_archive=date(2022, 1, 1))
        self.client.force_login(self.user)
        response = self.client.get(reverse('alarmes'))
        self.assertQuerySetEqual(response.context['object_list'], [install.alarme, alarme1,])
        response = self.client.get(reverse('alarmes') + '?archived=on')
        self.assertQuerySetEqual(response.context['object_list'], [archived])
        response = self.client.get(reverse('alarmes') + '?en_stock=on')
        self.assertQuerySetEqual(response.context['object_list'], [alarme1])
        response = self.client.get(reverse('alarmes') + '?en_stock=off')
        self.assertQuerySetEqual(response.context['object_list'], [install.alarme])

    def test_alarme_a_reviser(self):
        yest = today - timedelta(days=1)
        install1, install2 = self.create_client_with_installations(3)[0:2]
        install1.date_fin_abo = yest
        install1.retour_mat = yest
        install1.save()
        install2.date_fin_abo = yest
        install2.retour_mat = yest
        install2.save()
        install2.alarme.date_revision = yest
        install2.alarme.save()

        with self.settings(APPAREIL_REVISION_ACTIF=True):
            self.assertEqual(Alarme.objects.a_reviser().count(), 1)
            self.assertTrue(install1.alarme.a_reviser())
            self.assertFalse(install2.alarme.a_reviser())
            # Search view should not find install1
            self.client.force_login(self.user)
            response = self.client.get(reverse('alarme-search') + '?q=neat')
            self.assertEqual(len(response.json()), 1)
            self.assertEqual(response.json()[0]['value'], install2.alarme.pk)

        with self.settings(APPAREIL_REVISION_ACTIF=False):
            self.assertEqual(Alarme.objects.a_reviser().count(), 0)
            self.assertFalse(install1.alarme.a_reviser())
            response = self.client.get(reverse('alarme-search') + '?q=neat')
            self.assertEqual(len(response.json()), 2)

    def test_alarme_archive(self):
        alarme = Alarme.objects.create(
            modele=self.modele, no_serie='123456',
            chez_benevole=Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('alarme-archive', args=[alarme.pk]), data={})
        self.assertRedirects(response, reverse('home'), fetch_redirect_response=False)
        alarme.refresh_from_db()
        self.assertEqual(alarme.date_archive, date.today())
        self.assertIsNone(alarme.chez_benevole)

    def test_materiel_edit(self):
        mat = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='1234')
        Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        Benevole.objects.create(nom="Doe", prenom="John", activites=[Activites.TRANSPORT])
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-edit', args=[mat.pk]))
        self.assertContains(response, "1234")
        self.assertNotContains(response, "Doe")
        response = self.client.post(reverse('materiel-edit', args=[mat.pk]), data={
            'type_mat': self.type_emetteur.pk,
            'no_ref': '5678',
            'date_achat': '2022-01-06',
            'prix_achat': '',
            'fournisseur': '',
            'chez_benevole': '',
        })
        mat.refresh_from_db()
        self.assertEqual(mat.no_ref, '5678')
        self.assertEqual(mat.date_achat, date(2022, 1, 6))

    def test_search_materiel(self):
        Materiel.objects.create(type_mat=self.type_emetteur, no_ref='1234')
        mat2 = Materiel.objects.create(type_mat=self.type_emetteur, no_ref='5555')
        Materiel.objects.create(type_mat=self.type_emetteur, no_ref='9999')
        self.client.force_login(self.user)
        response = self.client.get(reverse('materiel-search') + '?q=55')
        self.assertEqual(response.json(), [{'label': 'Émetteur (5555)', 'value': mat2.pk}])


@tag("factures")
class FactureTests(TestUtils, TestCase):
    def test_facture_new(self):
        """Créer manuellement une facture pour un client."""
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", npa='2345', date_naissance=date(1945, 12, 3))
        self.client.force_login(self.user)
        response = self.client.get(reverse('facture-new', args=[client.pk]))
        self.assertContains(response, '<label for="id_article" class="required">Article :</label>', html=True)
        response = self.client.post(reverse('facture-new', args=[client.pk]), data={
            'date_facture': '2022-12-02',
            'article': self.article.pk,
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})
        self.assertJournalMessage(
            client,
            "Création d’une facture de CHF 37.45 du 02.12.2022 (Abonnement mensuel)"
        )
        fact = client.factures.first()
        self.assertEqual(fact.date_facture, date(2022, 12, 2))
        self.assertEqual(fact.mois_facture, date(2022, 12, 2))
        self.assertEqual(fact.libelle, self.article.designation)
        self.assertEqual(fact.montant, self.article.prix)
        self.assertIsNone(fact.exporte)

    def test_facture_delete(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.get(code=settings.CODES_ARTICLE_INSTALLATION[0])
        facture = Facture.objects.create(
            client=install.client, install=install,
            date_facture=today,
            article=article,
            libelle=article.designation, montant=article.prix,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facture-delete', args=[facture.client.pk, facture.pk]))
        self.assertEqual(response.json()['result'], 'OK')

    def test_facture_change_article(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.get(code=settings.CODES_ARTICLE_INSTALLATION[0])
        facture = Facture.objects.create(
            client=install.client, install=install,
            date_facture=today,
            article=article,
            libelle=article.designation, montant=article.prix,
        )
        autre_article = ArticleFacture.objects.create(code='AUTRE', designation="Blah", prix=100)
        self.client.force_login(self.user)
        edit_url = reverse('facture-edit', args=[install.client.pk, facture.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, article.designation)
        demain = today + timedelta(days=1)
        response = self.client.post(edit_url, data={
            'date_facture': demain.strftime('%Y-%m-%d'),
            'article': autre_article.pk,
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            install.client,
            f"Modification d’une facture (date de facturation (de «{today}» à «{demain}»), "
            "Libellé (de «Taxe de raccordement : frais de dossier et d’installation» à «Blah»), "
            "Montant (de «80.00» à «100.00»))"
        )
        facture.refresh_from_db()
        self.assertEqual(facture.date_facture, demain)
        self.assertEqual(facture.libelle, "Blah")
        self.assertEqual(facture.montant, 100)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_facture_list(self):
        today = date.today()
        self.create_client_with_installations(10)
        Installation.generer_factures(today)
        self.client.force_login(self.user)
        response = self.client.get(reverse('factures'))
        self.assertEqual(len(response.context['facture_list']), 10)
        # Test filtrer par date
        moispasse = today - timedelta(days=32)
        fact1 = Facture.objects.first()
        fact1.date_facture = moispasse
        fact1.save()
        date_as_month = moispasse.strftime('%m.%Y')
        response = self.client.get(reverse('factures') + f'?date_facture={date_as_month}')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['facture_list']), 1)
        self.assertContains(response, f'?date_facture={date_as_month}&export=1')
        # Test exportation
        response = self.client.get(reverse('factures') + f'?export=1&date_facture={date_as_month}')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_factures_non_transmises(self):
        day_last_month = date.today().replace(day=1) - timedelta(days=12)
        installs = self.create_client_with_installations(2)
        Installation.generer_factures(day_last_month)
        self.client.force_login(self.user)
        response = self.client.get(reverse('factures-non-transmises'))
        self.assertEqual(len(response.context['facture_list']), 2)
        # Test filter
        article_inst = ArticleFacture.objects.get(code=settings.CODES_ARTICLE_INSTALLATION[0])
        Facture.objects.create(
            client=installs[0].client, install=installs[0],
            date_facture=day_last_month,
            mois_facture=day_last_month,
            article=article_inst,
            libelle=article_inst.designation, montant=article_inst.prix,
        )
        response = self.client.get(reverse('factures-non-transmises') + f'?article={article_inst.pk}')
        self.assertEqual(len(response.context['facture_list']), 1)

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures(self):
        self.user.user_permissions.add(Permission.objects.get(codename='add_facture'))
        article = ArticleFacture.objects.create(
            code='EMO1', designation='Émetteur', prix=Decimal('11.30')
        )
        abo_emetteur = TypeAbo.objects.create(nom='Émetteur supp.', article=article)

        today = date.today()
        installs = self.create_client_with_installations(6)
        # Install courante -> facture
        installs[0].date_fin_abo = today + timedelta(days=60)
        # Créer facture mois précédent pour installs[0]
        Facture.objects.create(
            client=installs[0].client, install=installs[0],
            date_facture=today - timedelta(days=31),
            mois_facture=(today - timedelta(days=31)).replace(day=1),
            article=self.article,
            libelle='Facture mois précédent', montant=37.5,
        )
        # .update() pour éviter les signaux
        # Install passée -> pas de facture
        Installation.objects.filter(pk=installs[1].pk).update(
            date_debut=today - timedelta(days=90), date_fin_abo=today - timedelta(days=60)
        )
        # Install fin fin mois -> facture
        Installation.objects.filter(pk=installs[2].pk).update(date_fin_abo=today.replace(day=25))
        # Install début début mois -> facture + frais install
        Installation.objects.filter(pk=installs[3].pk).update(date_debut=today.replace(day=5))
        # Install future -> pas de facture
        Installation.objects.filter(pk=installs[4].pk).update(date_debut=today + timedelta(days=60))
        # Install avec déjà facture du mois -> pas de facture
        exist = Facture.objects.create(
            client=installs[5].client, install=installs[5],
            date_facture=today, mois_facture=today.replace(day=1),
            article=self.article,
            libelle='Facture de ce mois', montant=self.article.prix,
        )
        # Matériel -> facture abo (mais pas install.)
        MaterielClient.objects.create(
            client=installs[3].client,
            materiel=Materiel.objects.create(type_mat=self.type_emetteur),
            abonnement=abo_emetteur,
            date_debut=today.replace(day=5),
        )
        num_factures = Installation.generer_factures(today)
        num_factures += MaterielClient.generer_factures(today)
        self.assertEqual(num_factures, 3 + 1 + 1)  # 1 installation, 1 émetteur
        factures_abo = list(Facture.objects.exclude(pk=exist.pk).filter(
            mois_facture=today.replace(day=1)
        ).order_by('client_id'))
        self.assertEqual(
            sorted([fact.montant for fact in factures_abo]),
            [Decimal('11.30'), Decimal('37.45'), Decimal('37.45'), Decimal('37.45')]
        )
        self.assertEqual(factures_abo[0].libelle, f"Service d’alarme à domicile - {django_format(today, 'F Y')}")
        self.assertEqual(factures_abo[3].libelle, f"Émetteur supp. - {django_format(today, 'F Y')}")
        factures_inst = Facture.objects.filter(montant=80)
        self.assertEqual(
            sorted(factures_inst.values_list('montant', flat=True)),
            [Decimal('80.00')]
        )

        # Factures jamais à double
        num_factures = Installation.generer_factures(today)
        num_factures += MaterielClient.generer_factures(today)
        self.assertEqual(num_factures, 0)

        # Tester la vue
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('facturation-mensuelle', args=[today.year, today.month]),
            {'date_facture': today}
        )
        self.assertRedirects(response, reverse('facturation'))

    @default_facturation_policy()
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures_installs_a_cheval(self):
        """
        Si un client possède plusieurs installations successives (différentes
        alarmes, par ex.), la facture en tient compte (une seule facture).
        """
        today = date.today()
        install1 = self.create_client_with_installations(1)[0]
        install1.date_debut = today - timedelta(days=60)
        install1.date_fin_abo = today.replace(day=10) - timedelta(days=1)
        install1.save()
        install1_pk = install1.pk
        Alarme.objects.create(modele=ModeleAlarme.objects.first(), no_appareil='99')
        install2 = install1
        install2.pk = None
        install2.date_debut = today.replace(day=10)
        install2.date_fin_abo = None
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)
        facture = Facture.objects.get(client=install1.client)
        self.assertEqual(facture.montant, install1.abonnement.article.prix)
        # Changement install aux limites mensuelles
        # (fin install fin du mois passe, nouvelle install dès 1er du mois courant)
        install1 = Installation.objects.get(pk=install1_pk)
        install1.date_fin_abo = today.replace(day=1) - timedelta(days=1)
        install1.save()
        install2.date_debut = today.replace(day=1)
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)
        # Changement install le mois passé
        # (fin et nouvelle installs le mois passé)
        install1 = Installation.objects.get(pk=install1_pk)
        install1.date_fin_abo = today.replace(day=1) - timedelta(days=17)
        install1.save()
        install2.date_debut = today.replace(day=1) - timedelta(days=16)
        install2.save()
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(today)
        self.assertEqual(num_factures, 1)

    @override_settings(NOTESFRAIS_TRANSMISSION_URLNAME=None)
    def test_defraiements(self):
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont",
            type_client=['alarme'],
        )
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        m1 = Mission.objects.create(
            type_mission=self.typem, client=client, benevole=benev,
            delai=date.today(), effectuee=date(2023, 5, 12), km=26
        )
        Frais.objects.create(mission=m1, typ='repas', cout='18.35', descriptif='Repas')
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')

        self.client.force_login(self.user)
        response = self.client.get(reverse('defraiements') + '?year=2023&month=5')
        self.assertEqual(
            response.context['object_list'][0], {
                'benevole__id': benev.pk, 'benevole__nom': 'Duplain', 'benevole__prenom': 'Irma',
                'frais_repas': Decimal('18.35'), 'frais_autre': Decimal('4.50'), 'frais_sum': Decimal('22.85'),
                'kms': Decimal('52.0'), 'num_visites': 2, 'montant_total': Decimal('69.25'), 'note_frais': None
            }
        )


class StatsTests(TestUtils, TestCase):
    def test_stats_installations(self):
        installs = self.create_client_with_installations(5)
        installs[0].date_debut = today
        installs[0].client.samaritains = 1
        installs[0].client.save(update_fields=['samaritains'])
        installs[0].save()
        installs[1].date_fin_abo = today
        installs[1].save()
        installs[2].date_fin_abo = today - timedelta(days=40)
        installs[2].save()
        if settings.CODE_ARTICLE_INTERV_SAMA:
            Facture.objects.create(
                client=installs[0].client, date_facture=today,
                article=ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA),
                montant=20.5,
            )

        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-index'))
        this_month = Month.from_date(today)
        self.assertEqual(response.context['stats']['current'][this_month], 4)
        self.assertEqual(response.context['stats']['new'][this_month], 1)
        self.assertEqual(response.context['stats']['end'][this_month], 1)
        if settings.CODE_ARTICLE_INTERV_SAMA:
            self.assertEqual(response.context['stats']['interv_sama'][this_month], 1)
        # Listes
        samas_url = reverse('stats-list-samas', args=[this_month.year, this_month.month])
        response = self.client.get(samas_url)
        self.assertContains(response, "Clients avec répondants samaritains")
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(samas_url + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        response = self.client.get(reverse('stats-list-installs', args=[this_month.year, this_month.month]))
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(
            response,
            f'<td><a href="{reverse("client-edit", args=[installs[0].client_id])}">Donzé</a> Léa0</td>',
            html=True
        )
        self.assertContains(response, '<a href="/">Accueil</a>', html=True)
        response = self.client.get(reverse('stats-list-uninstalls', args=[this_month.year, this_month.month]))
        self.assertEqual(len(response.context['object_list']), 1)

    def test_stats_benevoles(self):
        typem = TypeMission.objects.create(nom='Installation', code='NEW')
        Mission.objects.bulk_create([
            Mission(
                type_mission=typem, effectuee='2023-03-12',
                duree_client=timedelta(seconds=2800), duree_seul=timedelta(), km=5
            ),
            Mission(
                type_mission=typem, effectuee='2023-03-29',
                duree_client=timedelta(seconds=1500), duree_seul=timedelta(seconds=300), km=0
            ),
            # Non pris en compte:
            Mission(
                type_mission=typem, effectuee=None,
                duree_client=timedelta(seconds=2800), duree_seul=timedelta(), km=5
            ),
        ])
        self.client.force_login(self.user)
        url = reverse('stats-benevoles') + '?start_month=2&start_year=2023&end_month=3&end_year=2023'
        response = self.client.get(url)
        month = Month(2023, 3)
        self.assertEqual(response.context['stats']['durees_client'][month].seconds, 4300)
        self.assertEqual(response.context['stats']['durees_seul'][month].seconds, 300)
        self.assertEqual(response.context['stats']['kms'][month], 5)
        response = self.client.get(url + '&export=1')
        self.assertEqual(response.status_code, 200)


class OtherTests(TempMediaRootMixin, TestUtils, TestCase):
    def test_home_page_alarmpost_sans_fichier(self):
        client = Client.objects.create(nom='Donzé', prenom='Léa')
        Alerte.objects.create(client=client, cible=ClientType.ALARME, recu_le=now())
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_home_controles(self):
        self.client.force_login(self.user)
        install = self.create_client_with_installations(1)[0]
        Referent.objects.create(client=install.client, nom='Schmid', repondant=1)
        client = Client.objects.create(nom='Smith', prenom='Jill', type_client=['alarme'])
        Referent.objects.create(client=client, nom='Schmid', repondant=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('controles'))
        self.assertQuerySetEqual(response.context['courriers_non_envoyes'], [install.client])

    def test_phone_validation(self):
        from common.fields import PhoneCharField, PhoneNumberValidator

        phonenumber_validator = PhoneNumberValidator()
        phonenumber_validator('032 950 10 05')
        phonenumber_validator('0033 380414860')
        phonenumber_validator('0033 3 80 41 48 60')
        phonenumber_validator('0049 4 04 92 92 111')
        phonenumber_validator('0033 380414860')
        phonenumber_validator('+33 380414860')
        phonenumber_validator('+44 208 1033 146')
        with self.assertRaises(ValidationError):
            phonenumber_validator('0044 60')
        with self.assertRaises(ValidationError):
            phonenumber_validator('+41')
        with self.assertRaises(ValidationError):
            phonenumber_validator('032 555 55 55 55')

        class SomeForm(forms.Form):
            tel = PhoneCharField()

        form = SomeForm(data={'tel': '+41'})
        self.assertFalse(form.is_valid())

    def test_npalocalite_formfield(self):
        benev = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.INSTALLATION])
        form = BenevoleForm(instance=benev)
        form_html = form.as_p()
        self.assertInHTML('<label for="id_npalocalite">NPA Localité :</label>', form_html)
        self.assertInHTML(
            '<input type="text" name="npalocalite" class="autocomplete form-control" autocomplete="off" '
            'placeholder="N° postal ou nom de localité" data-searchurl="/npa/autocomplete/" id="id_npalocalite">',
            form_html
        )
        self.assertInHTML('<input type="hidden" name="npa" class="form-control" id="id_npa">', form_html)
        self.assertInHTML('<input type="hidden" name="localite" class="form-control" id="id_localite">', form_html)
        benev.npa = '2300'
        benev.localite = 'La Chaux-de-Fonds'
        benev.save()
        form = BenevoleForm(instance=benev)
        form_html = form.as_p()
        self.assertIn('value="2300 La Chaux-de-Fonds"', form_html)
        form_data = {
            'nom': 'Duplain',
            'prenom': 'Irma',
            'genre': 'F',
            'npalocalite': '2000',
            'activite_set-TOTAL_FORMS': '0',
            'activite_set-INITIAL_FORMS': '0',
        }
        form = BenevoleForm(instance=benev, data=form_data)
        self.assertFalse(form.is_valid())
        form_data['npalocalite'] = '2000 Neuchâtel'
        form = BenevoleForm(instance=benev, data=form_data)
        self.assertTrue(form.is_valid())
        form.save()
        benev.refresh_from_db()
        self.assertEqual(benev.npa, '2000')
        self.assertEqual(benev.localite, 'Neuchâtel')

    def test_read_alerte_pdf(self):
        from alarme.management.commands.read_messages import save_payload

        cl = Client.objects.create(
            nom='Dupond', prenom='Julie', npa='2345', localite='Les Breuleux', type_client=['alarme'],
        )
        pdf_content = (Path(__file__).parent / 'alarmpost.pdf').read_bytes()
        save_payload(pdf_content)
        self.assertEqual(cl.alertes.count(), 1)
        self.assertEqual(cl.alertes.first().alerte, 'Akku vide_NEMO_email CRS_hoo')

    @skipIf(settings.CODE_ARTICLE_INTERV_SAMA == '', "CODE_ARTICLE_INTERV_SAMA needed for this test")
    def test_read_alerte_samas(self):
        from alarme.management.commands.read_messages import Command

        cl = Client.objects.create(
            nom='Pascal', prenom='Blaise', npa='2000', localite='Neuchâtel', type_client=['alarme'],
        )
        mail_content = (Path(__file__).parent / 'courriel_samas.html').read_bytes()
        Command().handle_jot_message(mail_content)
        self.assertEqual(cl.factures.count(), 1)
        fact = cl.factures.first()
        self.assertEqual(fact.libelle, "Intervention samaritain du 14.01.2023 (22:35)")
        self.assertEqual(fact.montant, Decimal(60))
        self.assertJournalMessage(
            cl,
            "Intervention samaritain facturée. Samaritain: Super Sama. Description: "
            "M. Pascal m'attendait dans son lit. Il devait aller aux toilettes et "
            "avait peur de tomber. Je l'ai amené jusqu'aux toilettes et ensuite l'ai "
            "aider à se recoucher",
            user=None
        )
        # Deuxième lecture avec mêmes infos date/heure -> pas de 2ème facture
        Command().handle_jot_message(mail_content)
        self.assertEqual(cl.factures.count(), 1)

    def test_auto_archive(self):
        """
        Archivage automatique des clients alarme et des bénévoles par management command.
        """
        Client.objects.create(nom='Donzé', prenom='Nouveau', no_debiteur=7007)
        installs = self.create_client_with_installations(5)
        installs[1].date_fin_abo = today - timedelta(days=2)
        installs[1].save()
        installs[2].date_fin_abo = today - timedelta(days=45)
        installs[2].save()
        # Only this one should be archived
        installs[3].date_fin_abo = today - timedelta(days=45)
        installs[3].retour_mat = today - timedelta(days=40)
        installs[3].save()
        ref = Referent.objects.create(client=installs[3].client, nom='Schmid', prenom='Claire', referent=1)
        # This one should no longer an 'alarme' client
        installs[4].client.type_client = ['alarme', 'transport']
        installs[4].client.save()
        installs[4].date_fin_abo = today - timedelta(days=45)
        installs[4].retour_mat = today - timedelta(days=40)
        installs[4].save()
        Benevole.objects.create(
            nom='Valjean', prenom='Jean', npa='2300', activites=[Activites.INSTALLATION]
        )
        Activite.objects.filter(benevole__nom='Valjean').update(fin=date.today() - timedelta(days=1))

        out = StringIO()
        call_command('archive_clients_benevs', verbosity=2, stdout=out, no_color=True)
        self.assertEqual(out.getvalue(), "2 client(s) archivé(s)\n1 bénévole(s) archivé(s)\n")
        installs[4].client.refresh_from_db()
        self.assertEqual(installs[4].client.type_client, ['transport'])
        ref.refresh_from_db()
        self.assertEqual(ref.date_archive, today)

    def test_exportation_admin(self):
        user = Utilisateur.objects.create_superuser('admin@example.org', password='test')
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456')
        alarme2 = Alarme.objects.create(modele=self.modele, no_appareil='78910')
        change_url = reverse('admin:alarme_alarme_changelist')
        # POST data to change_url
        self.client.force_login(user)
        response = self.client.post(change_url, {
            'action': 'export', '_selected_action': [alarme1.pk, alarme2.pk]
        })
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    @override_settings(TEST_INSTANCE=False)
    @modify_settings(ALLOWED_HOSTS={'prepend': 'alarme.croix-rouge-ne.ch'})
    def test_envoi_centrale(self):
        install = self.create_client_with_installations(1, client_data=[{
            'rue': 'Rue du Stand 99',
            'npa': '2000', 'localite': 'Neuchâtel',
            'tel_1': '032 222 22 22',
            'langues': ['fr'],
            'donnees_medic': {'troubles_eloc': 'Aphasie', 'diabete': False, 'epilepsie': True, 'oxygene': None},
            'situation_vie': 'Seul-e',
            'animal_comp': '3 chiens',
            'type_logement': 'appartement',
            'type_logement_info': 'Entrée par derrière',
            'etage': '1er',
            'ascenseur': True,
            'code_entree': '12345',
            'nb_pieces': '3.5',
            'cles': 'clé sous le paillasson',
            'samaritains': True,
        }])[0]
        install.alarme.no_serie = "X5432B1"
        install.alarme.save()
        ref = Referent.objects.create(
            client=install.client, nom='Schmid', prenom='Claire', npa='2000', localite='Neuchâtel',
            referent=1, repondant=2
        )
        ReferentTel.objects.create(referent=ref, priorite=1, tel='079 999 99 99')
        ProfClient.objects.create(
            client=install.client, professionnel=Professionnel.objects.create(
                nom="Cabinet GROUPE", rue="Rue Basse 14", npa='1700', localite='Fribourg',
                tel_1="088 888 88 99", remarque="Dr Knox"
            ),
            remarque="Si urgence",
        )
        ProfClient.objects.create(
            client=install.client, priorite=2,
            professionnel=Professionnel.objects.create(nom="Supersama", type_pro='Sama')
        )
        ProfClient.objects.create(
            client=install.client, priorite=1,
            professionnel=Professionnel.objects.create(nom="SamaP1", prenom="Céline", type_pro='Sama')
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[install.client.pk]))
        subj = response.context['form'].initial['subject']
        self.assertRegex(subj, r'\[Croix-Rouge (NE|JU)\] Mise à jour client Donzé Léa0')
        body = response.context['form'].initial['body']
        self.assertEqual(body, f"""Client Alarme Croix-Rouge {canton_abrev()}
====
Nom: Donzé
Prénom: Léa0
Rue: Rue du Stand 99
NPA, localité: 2000 Neuchâtel
Tél. 1: 032 222 22 22
Tél. 2: 
Courriel: 
Date de naissance: None
Langue(s): français

Appareil: Neat NOVO 4G, SIM , SN X5432B1

Situation de vie: Seul-e
Animal de compagnie: 3 chiens
Type de logement: Appartement, Entrée par derrière
Nb de pièces: 3.5
Étage: 1er, avec ascenseur
Code d’entrée: 12345
Infos clés: clé sous le paillasson

Données médicales
========
Troubles de l’élocution: Aphasie
Diabète: Non
Epilepsie: Oui
Oxygène: ?

Médecins/soignant-e-s:
 - Cabinet GROUPE, Rue Basse 14, 1700 Fribourg, 088 888 88 99, Dr Knox, (Si urgence)

Répondants
========
1. Samaritains
    1. SamaP1 Céline
    2. Supersama 

2. Schmid Claire, , 2000 Neuchâtel
Tél 1: 079 999 99 99
Relation: 

Personnes à prévenir en cas d’urgence
========
1. Schmid Claire, , 2000 Neuchâtel
Tél 1: 079 999 99 99
Relation: 

Historique récent (30j.)
========

""")  # noqa
        response = self.client.post(reverse('client-envoicentrale', args=[install.client.pk]), data={
            'subject': subj,
            'body': body,
        })
        self.assertRedirects(response, reverse('client-edit', args=[install.client.pk]))
        self.assertEqual(mail.outbox[0].recipients(), [settings.CENTRALE_EMAIL])
        self.assertEqual(mail.outbox[0].subject, subj)
        self.assertTrue(mail.outbox[0].body.startswith(
            "[Message envoyé par Valjean Jean <me@example.org>, depuis https://alarme.croix-rouge-ne.ch]\n\n"
        ))
        self.assertJournalMessage(install.client, f"Envoi des données du jour à {settings.CENTRALE_EMAIL}")
        self.assertIn("<b>Destinataire:</b>", install.client.journaux.latest().details)

    def test_envoi_centrale_appareil_de_mission(self):
        """
        Les données d'appareil doivent apparaître dans le message même si
        l'installation n'a pas encore eu lieu.
        """
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', type_client=['alarme'],
            rue="Fleurs 432", npa="2800", localite="Delémont",
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[client.pk]))
        self.assertIn("Appareil: pas encore défini\n", response.context['form'].initial['body'])

        Mission.objects.create(
            type_mission=self.typem,
            client=client,
            delai=today + timedelta(days=10),
            planifiee=now(),
            alarme=Alarme.objects.create(modele=self.modele, no_serie=999)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-envoicentrale', args=[client.pk]))
        body = response.context['form'].initial['body']
        self.assertIn(
            f"""
Appareil: Neat NOVO 4G, SIM , SN 999
Type d’intervention: Nouvelle installation
Intervention planifiée le: {localtime(now()).strftime('%d.%m.%Y %H:%M')}
""",
            body
        )
