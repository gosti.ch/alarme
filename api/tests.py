from datetime import date
from unittest.mock import ANY, patch

from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings

from client.forms import ReferentForm
from client.models import Client, Referent
from benevole.models import Activites, Benevole
from api import signals

api_timeout = signals.CIDSyncHandlerBase.api_timeout


@override_settings(API_ALLOWED_IPS=["127.0.0.1"], API_ALLOWED_TOKENS=["abcdefghij1234"])
class APIServerTests(TestCase):
    """
    Tests des API de synchronisation en provenance de l'app. externe.
    """
    client_data = {
        "id": 22, # ID logiciel tiers
        "title": "Sir",  # Valeurs possibles : Sir, Madam
        "name": "Dupont",
        "firstname": "Jean",
        "street": "Rue de la Poste 1",
        "zip": "1000",
        "city": "Lausanne",
        "country": "",
        "phone": "021 211 21 21",
        "mobile": "079 211 21 21",
        "email": "jean@dupont.com",
        "birthdate": "1942-12-31",
        "deathdate": "",
    }

    def test_create_client(self):
        post_data = self.client_data
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        json_resp = response.json()
        self.assertIn('id', json_resp)
        client = Client.objects.get(nom="Dupont")
        self.assertEqual(json_resp['id'], client.pk)
        self.assertEqual(client.id_externe, 22)
        self.assertEqual(client.date_naissance, date(1942, 12, 31))
        self.assertIsNone(client.date_deces)
        journal = client.journaux.latest()
        self.assertEqual(journal.description, "Création du client par CID")
        self.assertEqual(journal.qui, None)
        # Même requête une seconde fois, doublon détecté
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {'error':
                {'code': 409, 'doublon_id': client.pk, 'message': 'Ce client semble être un doublon.'}
            }
        )

    def test_create_client_no_birthdate(self):
        post_data = {**self.client_data, "birthdate": False}
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        client = Client.objects.get(nom="Dupont")
        self.assertIsNone(client.date_naissance)

    def test_create_client_error(self):
        post_data = {**self.client_data, "city": ''}
        response = self.client.post(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(), {
                'error': {
                    'code': 400,
                    'message': 'Sorry, the data are not valid.',
                    'errors': {'localite': [{'message': 'Ce champ est obligatoire.', 'code': 'required'}]}
                }
            }
        )

    def test_update_client(self):
        client = Client.objects.create(
            id_externe=22, type_client=['alarme', 'transport'],
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            remarques_int_alarme="Remarque interne",
        )
        post_data = {**self.client_data, "id": client.id_externe}
        response = self.client.put(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        client.refresh_from_db()
        self.assertEqual(client.nom, "Dupont")
        self.assertEqual(client.date_naissance, date(1942, 12, 31))
        # Champ existant et non synchronisé intact
        self.assertEqual(client.remarques_int_alarme, "Remarque interne")
        journal = client.journaux.latest()
        self.maxDiff = None
        self.assertEqual(
            journal.description,
            "Modification du client par CID: nom (de «Dupond» à «Dupont»), ajout de genre («M»), "
            "rue (de «Rue du Stand 3» à «Rue de la Poste 1»), npa (de «2000» à «1000»), "
            "localité (de «Neuchâtel» à «Lausanne»), ajout de tél. principal («021 211 21 21»), "
            "ajout de tél. secondaire («079 211 21 21»), ajout de courriel («jean@dupont.com»), "
            "ajout de date de naissance («1942-12-31»)"
        )
        self.assertEqual(journal.qui, None)
        self.assertEqual(client.alertes.count(), 2)
        self.assertTrue(client.alertes.all()[0].alerte.startswith('Modifications depuis l’ERP: nom'))
        self.assertEqual(set([al.cible for al in client.alertes.all()]), {'alarme', 'transport'})

        # Date de décès
        post_data['deathdate'] = '2023-05-20'
        response = self.client.put(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        client.refresh_from_db()
        self.assertEqual(client.date_deces, date(2023, 5, 20))
        self.assertEqual(
            client.alertes.latest().alerte,
            "Cette personne a été marquée comme décédée depuis l’ERP"
        )

    def test_update_referent(self):
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        referent0 = Referent.objects.create(
            client=cl0, nom='Dupond', prenom='Jean', rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            no_debiteur='22', facturation_pour=['al-abo', 'al-tout', 'transp'],
        )
        referent1 = Referent.objects.create(
            client=cl1, nom='Dupond', prenom='Jean', rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            no_debiteur='22', facturation_pour=['al-abo', 'al-tout', 'transp'],
        )
        post_data = {**self.client_data, "id": 22}
        response = self.client.put(
            '/api/partners', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        referent0.refresh_from_db()
        self.assertEqual(referent0.nom, "Dupont")
        referent1.refresh_from_db()
        self.assertEqual(referent1.nom, "Dupont")
        journal = cl0.journaux.latest()
        self.maxDiff = None
        self.assertEqual(
            journal.description,
            "Modification du contact «Jean Dupont, adresse de fact. pour Doe John» par CID: "
            "nom (de «Dupond» à «Dupont»), rue (de «Rue du Stand 3» à «Rue de la Poste 1»), "
            "npa (de «2000» à «1000»), localité (de «Neuchâtel» à «Lausanne»), "
            "ajout de courriel («jean@dupont.com»)"
        )

    def test_create_benevole(self):
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "NE",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1972-12-31",
            #"tag" => Liste valeurs possibles?
            "account": "CH41...",
        }
        # Avec erreur de formulaire
        with self.assertLogs('api', level='ERROR') as cm:
            response = self.client.post(
                '/api/employees', {**post_data, "phone": "1234"}, content_type='application/json',
                headers={"authorization": "Bearer abcdefghij1234"}
            )
        self.assertEqual(
            cm.output,
            ['ERROR:api:Benevole form errors: * tel_prive\n  * Le numéro de téléphone n’est pas valide']
        )
        self.assertEqual(
            response.json(),
            {'error': {
                'code': 400, 'message': "Sorry, the data are not valid.",
                'errors': {'tel_prive': [{'message': 'Le numéro de téléphone n’est pas valide', 'code': ''}]},
            }}
        )
        response = self.client.post(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        benev = Benevole.objects.get(nom="Dupont")
        log = benev.journaux.latest()
        self.assertEqual(log.description, "Création du client par CID")
        self.assertEqual(response.json()['id'], benev.pk)
        self.assertEqual(benev.id_externe, 22)
        self.assertEqual(benev.date_naissance, date(1972, 12, 31))
        # Même requête une seconde fois, doublon détecté
        response = self.client.post(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {'error':
                {'code': 409, 'doublon_id': benev.pk, 'message': 'Ce bénévole semble être un doublon.'}
            }
        )

    def test_update_benevole(self):
        benev = Benevole.objects.create(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[Activites.TRANSPORT]
        )
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1942-12-31",
        }
        response = self.client.put(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        benev.refresh_from_db()
        log = benev.journaux.latest()
        self.assertTrue(log.description.startswith("Modification du bénévole depuis CID:"))
        self.assertIn("ajout de courriel («jean@dupont.com»)", log.description)
        self.assertIsNone(log.qui)
        self.assertEqual(benev.nom, "Dupont")
        self.assertEqual(benev.courriel, "jean@dupont.com")
        self.assertEqual(benev.date_naissance, date(1942, 12, 31))
        self.assertEqual(benev.tel_mobile, "079 211 21 21")

    def test_update_benevole_non_existant(self):
        """
        Si une requête de modification survient pour un bénévole non existant,
        ce bénévole est créé.
        """
        post_data = {
            "id": 22, # ID logiciel tiers
            "name": "Dupont",
            "firstname": "Jean",
            "gender": "M",
            "street": "Rue de la Poste 1",
            "zip": "2000",
            "city": "Neuchâtel",
            "country": "",
            "phone": "021 211 21 21",
            "mobile": "079 211 21 21",
            "email": "jean@dupont.com",
            "birthdate": "1942-12-31",
        }
        response = self.client.put(
            '/api/employees', post_data, content_type='application/json',
            headers={"authorization": "Bearer abcdefghij1234"}
        )
        self.assertEqual(response.status_code, 200)
        benev = Benevole.objects.get(nom="Dupont")
        self.assertEqual(benev.id_externe, 22)


class APIClientTests(TestCase):
    """
    Test des API de synchronisation vers CID-ERP.
    """
    def setUp(self):
        """Patching httpx.put and httpx.post for all tests."""
        super().setUp()
        patcher_post = patch('api.signals.httpx.post')
        patcher_put = patch('api.signals.httpx.put')
        self.mock_post = patcher_post.start()
        self.mock_put = patcher_put.start()
        self.addCleanup(patcher_post.stop)
        self.addCleanup(patcher_put.stop)

    @override_settings(ADMINS=[('admin', 'test@example.org')])
    def test_client_creation_error(self):
        self.mock_post.return_value.status_code = 400
        self.mock_post.return_value.json.return_value = {
            "error": {"code": 400, "message": "Field \"street\" is required in JSON dict and must have a value"}
        }
        self.mock_post.return_value.ok = False
        with signals.connect_sync_signals():
            cl = Client.objects.create(
                nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
                date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
            )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            f"Trying to push new Client {cl.pk}, got response with status code 400 from CID server. "
            "Error details: "
            "{'code': 400, 'message': 'Field \"street\" is required in JSON dict and must have a value'}\n"
            f'JSON payload: {{\n  "id": {cl.pk},\n  "name": "Doe",\n  "firstname": "John",\n  "title": "Sir",\n  '
            '"street": "",\n  "zip": "2000",\n  "city": "Neuch\\u00e2tel",\n  "email": "",\n  '
            '"birthdate": "1950-04-03",\n  "deathdate": false,\n  "active": true,\n  "olderpid": "",\n  '
            '"phone": "",\n  "mobile": "078 444 44 44",\n  "country": "NE"\n}'
        )

    def test_client_creation(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        with signals.connect_sync_signals():
            cl = Client.objects.create(
                nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
                date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
            )
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': cl.pk,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '', 'mobile': '078 444 44 44', 'olderpid': '', 'active': True,
            },
            auth=ANY,
            timeout=api_timeout,
        )
        cl.refresh_from_db()
        self.assertEqual(cl.id_externe, 6523)

    def test_client_creation_missing_values(self):
        with signals.connect_sync_signals():
            Client.objects.create(
                nom="Doe", prenom="John", genre='M', npa="", localite="Neuchâtel",
                date_naissance=date(1950, 4, 3)
            )
        self.mock_post.assert_not_called()

    def test_client_update(self):
        cl = Client.objects.create(
            id_externe=6523, nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_1='078 444 44 44'
        )
        with signals.connect_sync_signals():
            cl.tel_2 = '031 310 31 31'
            cl.save()
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': cl.pk,
                'name': 'Doe', 'firstname': 'John', 'title': 'Sir',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03', 'deathdate': False,
                'phone': '031 310 31 31', 'mobile': '078 444 44 44',
                'olderpid': '', 'active': True,
            },
            auth=ANY,
            timeout=api_timeout,
        )

    def test_referent_creation(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        # Test sans facturation_pour
        with signals.connect_sync_signals():
            form_data = {
                'nom': 'Schmid',
                'prenom': 'Marco',
                'npalocalite': "2000 Neuchâtel",
                'referenttel_set-INITIAL_FORMS': 0,
                'referenttel_set-TOTAL_FORMS': 1,
                'referenttel_set-0-tel': '033 000 33 00',
            }
            form = ReferentForm(client=cl0, instance=None, data=form_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            form.save()
        self.mock_post.assert_not_called()

        # Test avec facturation_pour
        with signals.connect_sync_signals():
            form_data['facturation_pour'] = ['al-tout']
            form_data['rue'] = "Rue des Fleurs 4"
            form_data['no_debiteur'] = "98765"
            form = ReferentForm(client=cl0, instance=None, data=form_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            referent = form.save()
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}partners',
            json={
                'id': referent.pk,
                'name': 'Schmid', 'firstname': 'Marco',
                'street': 'Rue des Fleurs 4', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'phone': '033 000 33 00', 'mobile': '', 'olderpid': '98765', 'active': True,
            },
            auth=ANY,
            timeout=api_timeout,
        )
        referent.refresh_from_db()
        self.assertEqual(referent.id_externe, 6523)
        # Si le même référent est ajouté pour un autre client, pas de post.
        with signals.connect_sync_signals():
            referent = Referent.objects.create(
                client=cl1, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
                facturation_pour=['al-tout'], no_debiteur='98765',
            )
        self.mock_post.assert_called_once()

    def test_referent_update(self):
        # Note: referent sync is only called from form.save(), not as post_save.
        self.mock_post.return_value.json.return_value = {'id': 6523}
        cl0 = Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel",
        )
        cl1 = Client.objects.create(
            nom="Smith", prenom="Jill", npa="2000", localite="Neuchâtel",
        )
        referent = Referent.objects.create(
            client=cl0, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
        )
        with signals.connect_sync_signals():
            # Test referent ajout facturation_pour
            form_data = {f: getattr(referent, f) for f in ['nom', 'prenom']}
            form_data.update({
                'rue': "Rue des Fleurs 4",
                'npalocalite': "2000 Neuchâtel",
                'facturation_pour': ['al-tout'],
                'no_debiteur': "98765",
                'referenttel_set-INITIAL_FORMS': 0,
                'referenttel_set-TOTAL_FORMS': 1,
                'referenttel_set-0-tel': '078 777 77 77',
            })
            form = ReferentForm(client=cl0, instance=referent, data=form_data, initial={})
            self.assertTrue(form.is_valid(), form.errors)
            form.save()
            self.mock_post.assert_called_once_with(
                f'{settings.CID_API_URL}partners',
                json={
                    'id': referent.pk,
                    'name': 'Schmid', 'firstname': 'Marco',
                    'street': 'Rue des Fleurs 4', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                    'email': '', 'phone': '', 'mobile': '078 777 77 77', 'olderpid': '98765', 'active': True,
                },
                auth=ANY,
                timeout=api_timeout,
            )
            referent.refresh_from_db()
            self.assertEqual(referent.id_externe, 6523)
            # Autre référent avec même no_debiteur: test modification adresse
            referent_cl1 = Referent.objects.create(
                client=cl1, nom='Schmid', prenom='Marco', npa="2000", localite="Neuchâtel",
                id_externe=6523, no_debiteur='98765', facturation_pour=['al-tout'],
            )
            form_data = {f: getattr(referent, f) for f in ['nom', 'prenom', 'no_debiteur', 'facturation_pour']}
            form_data.update({
                'npalocalite': "2000 Neuchâtel",
                'rue': "Rue des Terreaux 14",
                'referenttel_set-INITIAL_FORMS': 0,
                'referenttel_set-TOTAL_FORMS': 0,
            })
            form = ReferentForm(client=cl1, instance=referent_cl1, data=form_data, initial={})
            self.assertTrue(form.is_valid())
            form.save()
            self.mock_put.assert_called_once_with(
                f'{settings.CID_API_URL}partners',
                json={
                    'id': referent_cl1.pk,
                    'name': 'Schmid', 'firstname': 'Marco',
                    'street': 'Rue des Terreaux 14', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                    'email': '', 'phone': '', 'mobile': '', 'olderpid': '98765', 'active': True,
                },
                auth=ANY,
                timeout=api_timeout,
            )
            referent_cl1.refresh_from_db()
            self.assertEqual(referent_cl1.rue, "Rue des Terreaux 14")

    def test_benevole_creation(self):
        self.mock_post.return_value.status_code = 200
        self.mock_post.return_value.json.return_value = {'id': 6523}
        with signals.connect_sync_signals():
            benev = Benevole.objects.create(
                nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
                date_naissance=date(1950, 4, 3), courriel="john.doe@example.org",
                tel_mobile='078 444 44 44', activites=[Activites.INSTALLATION],
            )
        self.mock_post.assert_called_once_with(
            f'{settings.CID_API_URL}employees',
            json={
                'id': benev.pk,
                'name': 'Doe', 'firstname': 'John', 'gender': 'M',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': 'john.doe@example.org', 'birthdate': '1950-04-03',
                'phone': '', 'mobile': '078 444 44 44'
            },
            auth=ANY,
            timeout=api_timeout,
        )
        benev.refresh_from_db()
        self.assertEqual(benev.id_externe, 6523)

    def test_benevole_creation_sans_date_naissance(self):
        """Pas de synchro si date de naissance manquante"""
        with signals.connect_sync_signals():
            Benevole.objects.create(
                nom="Doe", prenom="John", genre='M', npa="2000", localite="Neuchâtel",
                date_naissance=None, courriel="john.doe@example.org",
                tel_mobile='078 444 44 44', activites=[Activites.INSTALLATION],
            )
        self.mock_post.assert_not_called()

    def test_benevole_update(self):
        benev = Benevole.objects.create(
            id_externe=6523, nom="Doe", prenom="John", npa="2000", genre='M', localite="Neuchâtel",
            date_naissance=date(1950, 4, 3), tel_mobile='078 444 44 44', activites=[Activites.INSTALLATION],
        )
        with signals.connect_sync_signals():
            benev.tel_prive = '031 310 31 31'
            benev.save()
        self.mock_put.assert_called_once_with(
            f'{settings.CID_API_URL}employees',
            json={
                'id': benev.pk,
                'name': 'Doe', 'firstname': 'John', 'gender': 'M',
                'street': '', 'zip': '2000', 'city': 'Neuchâtel', 'country': 'NE',
                'email': '', 'birthdate': '1950-04-03',
                'phone': '031 310 31 31', 'mobile': '078 444 44 44'
            },
            auth=ANY,
            timeout=api_timeout,
        )
