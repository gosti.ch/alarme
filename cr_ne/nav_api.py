import json
import httpx

from django.conf import settings
from django.utils.timezone import now

from alarme.utils import last_day_of_month

SALUTATION_MADAME = 2
SALUTATION_MONSIEUR = 1
SALUTATION_MME_ET_M = 4
SALUTATION_ENTREPRISE = 3


class APIException(Exception):
    pass


class NavisionAPI:
    log_file_path = 'nav_api.log'

    def __init__(self, status):
        self.settings = settings.NAVISION_API_SETTINGS[status]
        self.token = None

    def get_token(self):
        if self.token is not None:
            return self.token
        endpoint = f"https://login.microsoftonline.com/{self.settings['TENANT']}.onmicrosoft.com/oauth2/token"
        response = httpx.post(endpoint, data={
            'grant_type': 'client_credentials',
            'client_id': self.settings['CLIENTID'],
            'client_secret': self.settings['CLIENTSECRET'],
            'resource': self.settings['RESOURCE'],
        }, headers={
            'Cache-Control': 'no-cache',
        })
        if response.status_code == 200:
            self.token = response.json()['access_token']
        else:
            response.raise_for_status()
        return self.token

    def _auth_headers(self):
        token = self.get_token()
        return {
            'Authorization': f'Bearer {token}',
            'Ocp-Apim-Subscription-Key': self.settings['SUBSCRIPTIONKEY'],
            'Ocp-Apim-Trace': 'true',
        }

    def get_articles(self):
        headers = self._auth_headers()
        response = httpx.get(
            f"{self.settings['HOST']}/payments/articles/?subscription-key={self.settings['SUBSCRIPTIONKEY']}&kv=ne",
            headers = headers
        )
        if response.status_code != 200:
            response.raise_for_status()
        return json.loads(response.content)

    def create_invoice(self, type_facture, factures, **kwargs):
        facture0 = factures[0]
        client = facture0.client
        if facture0.mois_facture:
            # last day of month
            postingDate = last_day_of_month(facture0.mois_facture)
            reference = f"ALCR, {postingDate.replace(day=1).strftime('%d.%m.%y')}..{postingDate.strftime('%d.%m.%y')}"
        else:
            postingDate = now()
            reference = ""
        salutation = {'F': SALUTATION_MADAME, 'M': SALUTATION_MONSIEUR}.get(client.genre)
        if client.nom_part:
            salutation = SALUTATION_MME_ET_M
        invoice_data = {
            "identification": {
                "registrationNo": f"facture-{facture0.pk}",
                "externalReference": f"client-{client.pk}",
            },
            "address": [
                {
                    "type": "postal address",
                    "navAddressNo": client.no_debiteur,
                    "streetAddress": client.rue,
                    "streetAdditionalAddress": client.c_o or None,
                    "postalCode": client.npa,
                    "addressLocality": client.localite,
                    #"addressCountry": "CH",
                    "firstName": client.prenoms,
                    "lastName": client.noms,
                    "salutation": salutation,
                    "emailPrimary": client.courriel,
                    "emailSecondary": "",
                    "phonePrivate": client.tel_1,
                    "phoneBusiness": client.tel_2,
                    "mobile": "",
                    "languageCode": "FR",
                    "navAddressNo": ""
                },
                {
                    "type": "invoice address",
                    "streetAddress": "",
                    "postalCode": "",
                    "addressLocality": "",
                    "firstName": "",
                    "lastName": "",
                    "salutation": None,
                    "streetAdditionalAddress": "",
                    "emailPrimary": "",
                    "emailSecondary": "",
                    "phonePrivate": "",
                    "phoneBusiness": "",
                    "mobile": "",
                    "languageCode": "",
                    "navAddressNo": ""
                }
            ],
            "billingMeta": {
                # Empty default seems better than former "BVR"
                "paymentMethodCode": "" if (client.envoi_facture or not facture0.mois_facture) else "DA",
                "postingDate": postingDate.strftime("%d%m%Y"),  # Date de comptabilisation
                "invoiceDate": facture0.date_facture.strftime("%d%m%Y"),
                "invoiceReference": reference,
                "invoiceDueDays": 30,
                "partialPayment": False,
                "installments": 0,
                "salesPerson": "Isabelle Manes",
            },
            "article": {
                "articleCode": ",".join([f.article.code for f in factures]),
                "articleAmount": ",".join(['1' for f in factures]),
                #"articleDescription": ";".join([f.article.designation for f in factures]),
            }
        }
        fact_adresse = getattr(facture0, 'adresse_facturation', None) or client.adresse_facturation(facture0)
        if fact_adresse != client:
            salutation = {
                'M': SALUTATION_MONSIEUR, 'F': SALUTATION_MADAME,
                'P': SALUTATION_MME_ET_M, 'E': SALUTATION_ENTREPRISE,
                '': None
            }.get(fact_adresse.salutation)
            invoice_data['address'][1] = {
                "type": "invoice address",
                "streetAddress": fact_adresse.rue,
                "streetAdditionalAddress": "",
                "postalCode": fact_adresse.npa,
                "addressLocality": fact_adresse.localite,
                "firstName": fact_adresse.prenom,
                "lastName": fact_adresse.nom,
                "salutation": salutation,
                "emailPrimary": fact_adresse.courriel,
                "emailSecondary": "",
                "phonePrivate": "",
                "phoneBusiness": "",
                "mobile": "",
                "languageCode": "FR",
                "navAddressNo": ""
            }
            if not fact_adresse.prenom and fact_adresse.complement:
                # Adresse d'entreprise
                invoice_data['address'][1].update({
                    "salutation": SALUTATION_ENTREPRISE,
                    "lastName": fact_adresse.nom,
                    "streetAdditionalAddress": fact_adresse.complement,
                    "navAddressNo": fact_adresse.no_debiteur,
                })

        # Dumps ourselves (instead of passing to json) to avoid ascii encoding.
        data = json.dumps(invoice_data, ensure_ascii=False, allow_nan=False, indent=2)
        with open(settings.MEDIA_ROOT / self.log_file_path, 'a') as fh:
            fh.write(data)
        headers = {**self._auth_headers(), 'Content-Type': 'application/json'}
        response = httpx.post(
            f"{self.settings['HOST']}/payments/invoice/?subscription-key={self.settings['SUBSCRIPTIONKEY']}&kv=ne",
            data=data.encode('utf-8'),
            headers=headers
        )
        if response.status_code == 540:
            raise APIException(response.json()[0]['fr:'])
        if response.status_code > 299:
            raise APIException(f'Error {response.status_code}: {response.content.decode()}')
        return response.json()[0]  # {'provInvoiceCreated': ...}
