from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0011_referent_contact_visites'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='fonds_transport',
            field=models.BooleanField(default=False, verbose_name='Fonds Transport handicap'),
        ),
    ]
