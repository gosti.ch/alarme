import calendar
from datetime import date, timedelta

from django.conf import settings
from django.db.models import Count, Max, Min, Prefetch, Q, Sum
from django.db.models.functions import TruncMonth
from django.utils.dates import MONTHS
from django.views.generic import TemplateView

from alarme.models import ArticleFacture, Facture, Mission, ModeleAlarme
from client.models import Client, Referent
from common.stat_utils import Month, StatsMixin
from .views import ClientListView


class StatsInstallView(StatsMixin, TemplateView):
    template_name = 'stats/installations.html'
    stat_items = {
        'current': {
            'label': 'Installations en cours',
            'help': "Nombre d’installations qui ont été actives au moins un jour dans le mois concerné",
        },
        'samas': {'label': 'Abo samaritains'},
        'new': {'label': 'Nouvelles installations'},
        'end': {'label': 'Désinstallations'},
        'age': {'label': 'Âge moyen des bénéficiaires'},
    }

    def get_stats(self, months):
        self.clients = Client.objects.actifs_entre(self.date_start, self.date_end)
        models = list(ModeleAlarme.objects.all().order_by('nom'))
        counters = self.init_counters(self.stat_items.keys(), months)
        counter_models = self.init_counters([m.nom for m in models], months)
        for client in self.clients:
            for month in months:
                if month.is_future():
                    continue
                month_start, month_end = self.month_limits(month)
                if (
                    client.debut_install < month_end and (
                        client.en_cours or client.fin_install >= month_start
                    )
                ):
                    counters['current'][month] += 1
                    counter_models[client.modele][month] += 1
                    if client.samaritains:
                        counters['samas'][month] += (2 if client.nom_part else 1)
                    if client.date_naissance:
                        counters['age'][month].append(date(month.year, month.month, 1) - client.date_naissance)
                    if client.date_naissance_part:
                        counters['age'][month].append(date(month.year, month.month, 1) - client.date_naissance_part)
                if month == Month.from_date(client.debut_install):
                    counters['new'][month] += 1
                    counters['new']['total'] += 1
                if not client.en_cours and client.fin_install and month == Month.from_date(client.fin_install):
                    counters['end'][month] += 1
                    counters['end']['total'] += 1
        for month in months:
            if month.is_future():
                continue
            num_ages = len(counters['age'][month])
            counters['age'][month] = (
                sum([c.days for c in counters['age'][month]]) / num_ages / 365.25
                if num_ages else '-'
            )
        if settings.CODE_ARTICLE_INTERV_SAMA:
            self.stat_items['interv_sama'] = {'label': 'Interventions samaritains'}
            counters.update(self.init_counters(['interv_sama'], months))
            facts = Facture.objects.filter(
                date_facture__gte=self.date_start, date_facture__lte=self.date_end,
                article__code=settings.CODE_ARTICLE_INTERV_SAMA
            )
            for fact in facts:
                counters['interv_sama'][Month(fact.date_facture.year, fact.date_facture.month)] += 1
                counters['interv_sama']['total'] += 1

        return {
            'labels': list(self.stat_items.values()),
            'stats': {key: counters[key] for key in self.stat_items.keys()},
            'stat_models': {key: counter_models[key] for key in [m.nom for m in models]},
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['art_sama'] = ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA).pk
        except ArticleFacture.DoesNotExist:
            pass
        return context

    def export_data(self, export, context):
        super().export_data(export, context)
        export.add_sheet("Clients")
        export.write_line([
            'Nom', 'Prénom', 'Rue', 'NPA', 'Localité', 'Date de naiss.', 'Début install.', 'Fin install.'
        ], bold=True, col_widths=[20, 20, 30, 8, 20, 15, 15, 15])
        for client in self.clients:
            export.write_line([
                client.noms, client.prenoms, client.rue, client.npa, client.localite,
                client.date_naissance, client.debut_install, client.fin_install,
            ])

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Installations alarme'] + [str(month) for month in months] + ['Total']
        for key, data in self.stat_items.items():
            yield [data['label']] + [context['stats'][key][month] for month in months] + [context['stats'][key]['total']]


class ClientsSamaView(ClientListView):
    """Liste des clients samaritains du mois."""
    template_name = 'alarme/client_list.html'
    base_template = 'base.html'

    def get_title(self):
        return f"Clients avec répondants samaritains en {MONTHS[self.kwargs['month']]} {self.kwargs['year']}"

    def get_queryset(self):
        month_start = date(self.kwargs['year'], self.kwargs['month'], 1)
        month_end = date(
            self.kwargs['year'], self.kwargs['month'], calendar.monthrange(self.kwargs['year'], self.kwargs['month'])[1]
        )
        return Client.objects.actifs_entre(month_start, month_end).filter(samaritains__isnull=False).order_by('nom')


class NewInstallsView(ClientListView):
    """Liste des installations du mois."""
    template_name = 'alarme/client_list.html'
    filter_formclass = None
    base_template = 'base.html'

    def get_title(self):
        return f"Clients avec nouvelle installation en {MONTHS[self.kwargs['month']]} {self.kwargs['year']}"

    def get_queryset(self):
        this_month = date(self.kwargs['year'], self.kwargs['month'], 1)
        clients = Client.objects.annotate(
            debut_install=TruncMonth(Min('installation__date_debut')),
            date_tout_debut=Min('installation__date_debut')
        ).filter(
            debut_install=this_month
        ).order_by('nom')
        if self.export_flag:
            clients = clients.prefetch_related(
                Prefetch('referent_set', to_attr="debiteurs", queryset=Referent.objects.filter(
                    date_archive=None, facturation_pour__overlap=['al-abo', 'al-tout']
                ))
            )
            clients._hints['_export_autres_debiteurs'] = True
        return clients


class UninstallsView(ClientListView):
    """Liste des désinstallations du mois."""
    template_name = 'alarme/client_list.html'
    filter_formclass = None
    base_template = 'base.html'

    def get_title(self):
        return f"Clients avec désinstallation en {MONTHS[self.kwargs['month']]} {self.kwargs['year']}"

    def get_queryset(self):
        this_month = date(self.kwargs['year'], self.kwargs['month'], 1)
        return Client.objects.annotate(
            fin_install=TruncMonth(Max('installation__date_fin_abo')),
            en_cours=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
        ).filter(
            en_cours=0, fin_install=this_month
        ).order_by('nom')


class StatsBenevView(StatsMixin, TemplateView):
    template_name = 'stats/benevoles.html'
    stat_items = {
        'durees_client': {'label': 'Temps passé par les bénévoles avec les clients'},
        'durees_seul': {'label': 'Temps passé par les bénévoles sans client'},
        'kms': {'label': 'Km défrayés aux bénévoles'},
        'frais': {'label': 'Frais remboursés aux bénévoles'},
    }

    def get_stats(self, months):
        counters = self.init_counters(['durees_client', 'durees_seul'], months, total=timedelta())
        counters.update(self.init_counters(['kms', 'frais'], months))
        intervs = Mission.objects.filter(
            effectuee__range=(self.date_start, self.date_end)
        ).annotate(mois=TruncMonth('effectuee')
        ).values('mois').annotate(
            durees_client=Sum('duree_client'),
            durees_seul=Sum('duree_seul'),
            kms=Sum('km'),
            frais=Sum('frais__cout'),
        ).order_by()

        stats = {Month.from_date(line['mois']): line for line in intervs}
        for month in months:
            if month.is_future():
                continue
            for key in self.stat_items.keys():
                counters[key][month] = stats.get(month, {}).get(key)
                if counters[key][month]:
                    counters[key]['total'] += counters[key][month]

        return {
            'labels': self.stat_items,
            'stats': {key: counters[key] for key in self.stat_items.keys()},
        }

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Bénévoles alarme'] + [str(month) for month in months] + ['Total']
        for key, data in self.stat_items.items():
            yield [data['label']] + [context['stats'][key][month] for month in months] + [context['stats'][key]['total']]
