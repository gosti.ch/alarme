import json
from datetime import date
from decimal import Decimal

import httpx

from django.conf import settings
from django.core.mail import mail_admins
from django.utils.dateformat import format as django_format

CID_JOURNAL_CODES = {
    'alarme': 'F520',  # Facturation MAD - Alarme (CHF)
    'transport': 'F510',  # Facturation MAD - Transports (CHF)
    'savd': 'F500',  # Facturation MAD - SAVD (MedLink) (CHF)
    'deb': 'DEB',  # Factures débiteurs (CHF)
}
# F200: Facturation FORMATION (CHF)
# F400: Facturation SAP_GED/ASE (MedLink) (CHF)
# F210: Facturation BABY-SITTING (CHF)
CID_COMPTES_TRANSPORT = {
    'AVS': '51.100',
    'NON_AVS': '51.110',
    'FORFAIT_ALLER': '51.120',
    'FORFAIT_AR': '51.130',
    'ATTENTE': '51.200',
    'FRAIS': '51.290',
    'ANNULATION': '51.300',
}


def is_success(response):
    try:
        json_resp = response.json()
    except ValueError:
        return False
    return response.is_success and 'error' not in json_resp


def report_error(response, operation, json_sent=None):
    try:
        resp_data = response.json()
    except Exception:
        error = "Unknown error"
    else:
        error = str(resp_data.get('error', ''))
    msg = (
        f"{operation}, got response with status code {response.status_code} from CID server. "
        f"Error details: {error}"
    )
    if json_sent:
        msg += f"\nJSON payload: {json.dumps(json_sent, indent=2)}"
    mail_admins("CID Error", msg)


class BearerAuth(httpx.Auth):
    def __init__(self):
        self.token = settings.CID_API_TOKEN

    def auth_flow(self, request):
        request.headers["authorization"] = f"Bearer {self.token}"
        yield request


class CIDApi:
    partner_url = f"{settings.CID_API_URL}partners"
    employees_url = f"{settings.CID_API_URL}employees"
    invoices_url = f"{settings.CID_API_URL}invoices"
    expenses_url = f"{settings.CID_API_URL}expenses"

    def post(self, url, data):
        try:
            response = httpx.post(url, json=data, auth=BearerAuth(), timeout=6)
        except httpx.TimeoutException:
            # Store on a stack and retry later?
            mail_admins(
                "CID timeout",
                f"Got timeout when trying to send data to {url} to CID server."
            )
        return response

    def create_invoice(self, type_facture, factures):
        if type_facture not in CID_JOURNAL_CODES:
            raise ValueError("Type de facture non pris en charge")
        facture0 = factures[0]
        journal = CID_JOURNAL_CODES[type_facture]
        client = facture0.client
        if not client.id_externe:
            raise ValueError("Impossible d’envoyer une facture si le client n’est pas lié à un partenaire CID")
        debiteur = client.adresse_facturation(facture0)
        fact_data = {
            "debitorid": client.id_externe if client is debiteur else int(debiteur.no_debiteur),
            "beneficiaryid": facture0.client.id_externe,
            "invoicedate": facture0.date_facture.strftime("%Y-%m-%d"),
            "period": facture0.mois_facture.strftime("%m/%Y") if facture0.mois_facture else "",
            "journal": journal,  # Journal comptable, dépendant du type de prestation
            "lines": [],
        }
        total = Decimal()
        if type_facture == 'alarme':
            for fact in factures:
                fact_data["lines"].append({
                    #"productid": 26, -> ID CID du produit
                    "productref": fact.article.code,
                    "description": fact.article.designation,
                    "quantity": 1.0,
                    "unitamount": float(fact.article.prix),
                })
                total += fact.article.prix
        elif type_facture == 'transport':
            # une ligne par transport ?
            for transp in facture0.get_transports():
                donnees = transp.donnees_facturation()
                if donnees['km'] > 0:
                    fact_data["lines"].append({
                        "productref": (
                            CID_COMPTES_TRANSPORT['AVS' if donnees['avs'] else 'NON_AVS']
                        ),
                        "description": f"Transport du {django_format(donnees['rendez-vous'], 'd.m.Y H:i')}",
                        "quantity": float(donnees['km']),
                        "unitamount": float(transp.tarif_km),
                    })
                if donnees.get('cout_forfait'):
                    aller_ret = transp.aller_ret_map.get(transp.retour)
                    fact_data["lines"].append({
                        "productref": (
                            CID_COMPTES_TRANSPORT['FORFAIT_AR' if transp.retour else 'FORFAIT_ALLER']
                        ),
                        "description": f"Forfait {aller_ret['label'].lower()}",
                        "quantity": 1.0,
                        "unitamount": float(donnees['cout_forfait']),
                    })
                if donnees.get('cout_attente'):
                    fact_data["lines"].append({
                        "productref": CID_COMPTES_TRANSPORT['ATTENTE'],
                        "description": "Frais de temps d’attente",
                        "quantity": 1.0,
                        "unitamount": float(donnees['cout_attente']),
                    })
                if donnees.get('annulation'):
                    fact_data["lines"].append({
                        "productref": CID_COMPTES_TRANSPORT['ANNULATION'],
                        "description": "Annulation de dernière minute",
                        "quantity": 1.0,
                        "unitamount": float(donnees['annulation']),
                    })
                for frais in donnees['frais']:
                    fact_data["lines"].append({
                        "productref": CID_COMPTES_TRANSPORT['FRAIS'],
                        "description": frais['descr'],
                        "quantity": 1.0,
                        "unitamount": float(frais['cout']),
                    })
            total = facture0.montant_total
        response = self.post(self.invoices_url, fact_data)
        return response.json()

    def create_expense(self, note):
        if not note.benevole.id_externe:
            raise ValueError(f"Le bénévole {note.benevole} n'est pas synchronisé sur CID")
        note_data = {
            "id": note.pk,
            "date": date.today().strftime('%Y-%m-%d'),  # Date du mois ?
            "description": f"Note de frais {note.service}",
            "employeeid": note.benevole.id_externe,
            "lines": [],
        }
        total = Decimal(0)
        for line in note.lignes.all():
            note_data['lines'].append({
                "productref": line.libelle.no,
                "quantity": float(line.quantite),
                "unitamount": float(line.montant_unit),
            })
            total += line.montant
        response = self.post(self.expenses_url, note_data)
        return response.json()
