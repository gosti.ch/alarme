from collections import defaultdict
from copy import copy
from datetime import date, datetime, time, timedelta, timezone as tz
from decimal import Decimal
from operator import attrgetter

from dateutil import rrule
from stdnum.ch import esr

from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Count, F, Max, Min, Prefetch, Q, Sum
from django.db.models.functions import TruncMonth
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import format_html

from client.models import Client, Referent
from benevole.models import Benevole
from common.distance import ORSUnavailable, distance_real, distance_vo
from common.fields import PhoneNumberField
from common.models import GeolocMixin, Utilisateur

RRULE_FREQ_MAP = {
    "YEARLY": rrule.YEARLY,
    "MONTHLY": rrule.MONTHLY,
    "WEEKLY": rrule.WEEKLY,
    "DAILY": rrule.DAILY,
    "HOURLY": rrule.HOURLY,
    "MINUTELY": rrule.MINUTELY,
    "SECONDLY": rrule.SECONDLY,
}
RRULE_WDAY_MAP = {'MO': 0, 'TU': 1, 'WE': 2, 'TH': 3, 'FR': 4, 'SA': 5, 'SU': 6}


class Regle(models.Model):
    # https://tools.ietf.org/html/rfc5545
    # See also https://dateutil.readthedocs.io/en/stable/rrule.html
    FREQ_CHOICES = (
        ("YEARLY", "Chaque année"),
        ("MONTHLY", "Chaque mois"),
        ("WEEKLY", "Chaque semaine"),
        ("DAILY", "Chaque jour"),
        ("HOURLY", "Chaque heure"),
        ("MINUTELY", "Chaque minute"),
        ("SECONDLY", "Chaque seconde"),
    )
    nom = models.CharField("Nom", max_length=40)
    description = models.TextField("Description")
    frequence = models.CharField("Fréquence", choices=FREQ_CHOICES, max_length=10)
    params = models.TextField("Params", blank=True)

    class Meta:
        verbose_name = "Règle"
        verbose_name_plural = "Règles"

    def __str__(self):
        return "Règle %s - params %s" % (self.nom, self.params)


class OccurrenceExistError(Exception):
    pass


class OccurrenceMixin:
    def get_occurrences(self, start, end=None):
        """Return list of date occurrences of this event between start and end."""
        start_rule = self.get_rrule()
        # _exceptions used in .memory_instance()
        self._exceptions = self.get_exceptions()
        initial_offset = timezone.localtime(self.debut).utcoffset()
        # Adjust start/end to consider timezone change from initial event timezone
        start = fix_occurrence_date(start, initial_offset, reverse=True)
        if end:
            fix_occurrence_date(end, initial_offset, reverse=True)
        if self.debut.time() < start.time() < self.fin.time():
            # Adjust start time to include events overlapping start.
            start = datetime.combine(start, self.debut.time(), tzinfo=start.tzinfo)
        # FIXME: limit results if end is None and event has no end.
        if start_rule:
            for occ_start in start_rule.xafter(start, inc=True):
                occ_start = fix_occurrence_date(occ_start, initial_offset)
                if end and occ_start >= end:
                    break
                yield self.memory_instance(occ_start)
        elif self.debut >= start and (not end or self.fin <= end):
            yield self

    def get_next_occurrence(self):
        return next(self.get_occurrences(timezone.now()), None)

    def get_exceptions(self):
        return {}

    def memory_instance(self, start):
        raise NotImplementedError

    def get_rrule(self):
        if self.regle is None:
            return
        frequency = RRULE_FREQ_MAP[self.regle.frequence]

        def convert_param(key, value):
            if key == 'byweekday':
                return key, [RRULE_WDAY_MAP[v] for v in value.split(',')]
            elif key == 'interval':
                return key, int(value)
            else:
                return key, value

        if self.regle.params:
            params = {key: val for key, val in [
                convert_param(*(param.split(':'))) for param in self.regle.params.split(';')
            ]}
        else:
            params = {}
        return rrule.rrule(frequency, dtstart=self.debut, until=self.fin_recurrence, **params)


class AdresseClient(GeolocMixin):
    def __init__(self, client):
        self.rue = client.rue
        self.npa = client.npa
        self.localite = client.localite
        self.empl_geo = client.empl_geo

    def __str__(self):
        return f"{self.rue}, {self.npa} {self.localite}"

    def __repr__(self):
        return f"<AdresseClient: {str(self)}>"

    def __eq__(self, other):
        if not hasattr(other, 'rue'):
            return False
        return (self.rue == other.rue and self.empl_geo == other.empl_geo)

    def __html__(self):
        return format_html(
            '<span class="adresse">{}, {} {}</span>', self.rue or "-", self.npa, self.localite
        )


class AdresseManager(models.Manager):
    def actives(self):
        return self.filter(Q(date_archive=None) | Q(date_archive__gt=date.today()))


class Adresse(GeolocMixin, models.Model):
    nom = models.CharField("Nom", max_length=120)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    tel = PhoneNumberField("Téléphone", blank=True)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    date_archive = models.DateField("Archivée le", null=True, blank=True)

    objects = AdresseManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['nom'], condition=Q(date_archive__isnull=True), name='nom_non_archive_unique'
            ),
        ]

    def __str__(self):
        return f"{self.nom}, {self.rue or '-'}, {self.npa} {self.localite}"

    def __eq__(self, other):
        if not hasattr(other, 'rue'):
            return False
        return (self.rue == other.rue and self.empl_geo == other.empl_geo)

    def __html__(self):
        return format_html(
            '{}<br><span class="adresse">{}, {} {}</span>',
            self.nom, self.rue or "-", self.npa, self.localite
        )


class TransportModelQuerySet(models.QuerySet):
    def prefetch_first_trajets(self):
        """Prefetch first trajets for each TransportModel and cache them in .trajets_tries"""
        query = self.annotate(first_transp=Min('transports__pk'))
        trajets = list(Trajet.objects.filter(
            transport__in=query.values_list('first_transp', flat=True)
        ).select_related(
            'transport', 'transport__client', 'origine_adr', 'destination_adr'
        ).order_by('heure_depart'))
        trajets_par_model = defaultdict(list)
        for tr in trajets:
            trajets_par_model[tr.transport.modele_id].append(tr)
        for mod in query:
            mod.trajets_tries = trajets_par_model[mod.pk]
        return query


class TransportModel(OccurrenceMixin, models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='models')
    heure_depart = models.TimeField("Heure de départ", null=True)
    heure_rdv = models.DateTimeField("Heure de rendez-vous")
    duree_rdv = models.DurationField("Durée du rendez-vous", null=True, blank=True)
    retour = models.BooleanField(default=True)
    remarques = models.TextField("Remarques", blank=True)
    regle = models.ForeignKey(
        Regle, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    suspension_depuis = models.DateField("Suspension dès le", null=True, blank=True)
    fin_recurrence = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    objects = TransportModelQuerySet.as_manager()

    def __str__(self):
        return f"Modèle de transport pour {self.client} ({self.regle}, dès {self.heure_rdv.date()})"

    def _check_trajets(self):
        """
        Populer self.trajets_tries avec les trajets de la première occurrence
        concrète (ou liste vide, s'il n'y en a pas encore).
        """
        if not hasattr(self, 'trajets_tries'):
            first = self.transports.order_by('heure_rdv').first()
            self.trajets_tries = first.trajets_tries if first else []

    @classmethod
    def toutes_occurrences(cls, client=None, filter_func=None, prefetch_trajets=True):
        """Renvoyer toutes les occurrences de modèles pour les 60 prochains jours."""
        modeles = cls.objects.filter(
            Q(fin_recurrence__isnull=True) | Q(fin_recurrence__gt=timezone.now())
        ).select_related('client', 'regle').prefetch_related(
            # prefetch (future) real instances
            Prefetch(
                'transports',
                queryset=Transport.objects.filter(date__gte=date.today()),
                to_attr='futurs_transports'
            )
        )
        if client is not None:
            modeles = modeles.filter(client=client)
        # As late as possible as it evaluates the query
        if prefetch_trajets:
            modeles = modeles.prefetch_first_trajets()

        modele_transports = []
        for modele in modeles:
            # Générer futures occurrences du mois à venir
            days = 60
            occs = modele.generer_jusqua(timezone.now() + timedelta(days=days))
            if filter_func:
                occs = filter_func(occs)
            modele_transports.extend(occs)
        return modele_transports

    @property
    def debut(self):
        # for OccurrenceMixin
        return self.heure_rdv

    @property
    def fin(self):
        self._check_trajets()
        return self.trajets_tries[-1].heure_arrivee

    def memory_instance(self, occ_start):
        return Transport(
            client=self.client, date=occ_start.date(),
            heure_rdv=timezone.localtime(occ_start), duree_rdv=self.duree_rdv,
            modele=self, retour=self.retour, remarques=self.remarques,
            statut=Transport.StatutChoices.SAISI
        )

    def get_occurrence(self, day):
        start = datetime.combine(day, time(0, 0), tzinfo=timezone.get_current_timezone())
        end = datetime.combine(day, time(23, 59), tzinfo=timezone.get_current_timezone())
        return next(self.get_occurrences(start=start, end=end), None)

    def generer_jusqua(self, fin):
        """Renvoyer les occurrences futures jusqu'à `fin`, en excluant les occurrences concrétisées."""
        existing_dates = {tr.date for tr in getattr(self, 'futurs_transports', self.transports.all())}
        if self.suspension_depuis:
            # Temporary (in memory) set fin_recurrence to suspension_depuis
            dt = self.suspension_depuis
            self.fin_recurrence = timezone.make_aware(datetime(dt.year, dt.month, dt.day, 0, 0))
        return [
            occ for occ in self.get_occurrences(timezone.now(), end=fin)
            if occ.date not in existing_dates
        ]


class TransportManager(models.Manager):
    def a_facturer_pour(self, un_du_mois, billed=None, client=None):
        """Renvoi le QuerySet des transports à facturer pour le mois `mois`."""
        assert un_du_mois.day == 1
        query = Transport.objects.annotate(
            month=TruncMonth('date')
        ).filter(
            statut__in=[Transport.StatutChoices.CONTROLE, Transport.StatutChoices.ANNULE],
            statut_fact__lt=Transport.FacturationChoices.PAS_FACTURER,
            month=un_du_mois,
        ).prefetch_related('frais').order_by('client__nom', 'client__id', 'heure_rdv')
        if billed is not None:
            query = query.filter(date_facture__isnull=not billed)
        if client is not None:
            return query.filter(client=client)
        #return query.filter(client__referent__facturation_pour__contains=['transp'])
        return query


class Transport(models.Model):
    class StatutChoices(models.IntegerChoices):
        SAISI = 1, "Saisi"
        ATTRIBUE = 2, "Attribué"
        CONFIRME = 3, "Confirmé"
        EFFECTUE = 4, "Effectué"
        RAPPORTE = 5, "Rapporté"
        CONTROLE = 6, "Contrôlé"
        ANNULE = 10, "Annulé"

    class FacturationChoices(models.IntegerChoices):
        FACTURER = 1, "Facturer le transport"
        FACTURER_ANNUL = 2, "Facturer frais d’annulation"
        FACTURER_ANNUL_KM = 3, "Facturer frais d’annulation et km chauffeur"
        PAS_FACTURER = 99, "Ne rien facturer"

    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='transports')
    modele = models.ForeignKey(
        TransportModel, on_delete=models.SET_NULL, null=True, blank=True, related_name='transports'
    )
    chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.PROTECT, related_name='transports'
    )
    chauff_aller_dist = models.PositiveIntegerField("Distance chauffeur-départ [m]", blank=True, null=True)
    chauff_retour_dist = models.PositiveIntegerField("Distance arrivée-chauffeur [m]", blank=True, null=True)
    chauffeur_vu = models.BooleanField("Vu par chauffeur", default=False)
    date = models.DateField("Date du transport")
    heure_rdv = models.DateTimeField("Heure de rendez-vous")
    duree_rdv = models.DurationField("Durée du rendez-vous", null=True, blank=True)
    retour = models.BooleanField(default=True)
    duree_eff = models.DurationField("Durée effective du trajet", null=True, blank=True)
    km = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)
    statut = models.PositiveSmallIntegerField(choices=StatutChoices.choices, default=StatutChoices.SAISI)
    temps_attente = models.DurationField("Temps d’attente", null=True, blank=True)
    remarques = models.TextField("Remarques", blank=True)
    rapport_chauffeur = models.TextField("Commentaires du chauffeur", blank=True)
    date_annulation = models.DateTimeField("Date d’annulation", blank=True, null=True)
    raison_annulation = models.TextField("Raison de l’annulation", blank=True)
    statut_fact = models.PositiveSmallIntegerField(
        "Statut de facturation", choices=FacturationChoices.choices, default=FacturationChoices.FACTURER
    )
    defrayer_chauffeur = models.BooleanField("Défrayer les km du chauffeur", null=True)
    date_facture = models.DateField("Date de facture", blank=True, null=True)

    ev_category = 'transport'
    aller_ret_map = {
        True: {'label': "Aller-retour"},
        False: {'label': "Aller simple"}
    }
    MIN_KM_FACTURES = Decimal('4.0')
    COUT_ATTENTE_UNITE = Decimal('5.00')
    FRAIS_CLIENT_SUPPL = Decimal('5.00')

    objects = TransportManager()

    class Meta:
        indexes = [
            models.Index(fields=['statut'], name='index_statut'),
            models.Index(fields=['heure_rdv'], name='index_heure_rdv'),
        ]
        permissions = [
            ("gestion_finances", "Autorisé à gérer finances et facturation"),
        ]

    def __str__(self):
        return f"Transport pour {self.client} le {self.date.strftime('%d.%m.%Y')}"

    def __lt__(self, other):
        return getattr(self, 'depart', self.heure_rdv) < getattr(other, 'depart', other.heure_rdv)

    @classmethod
    def check_effectues(self, chauffeur=None):
        # Vérifier si certains transports confirmés doivent être passés en effectués
        averifier = Transport.objects.filter(statut=Transport.StatutChoices.CONFIRME, date__lte=date.today())
        if chauffeur is not None:
            averifier = averifier.filter(chauffeur=chauffeur)
        for transp in averifier:
            if transp.est_passe:
                transp.statut = Transport.StatutChoices.EFFECTUE
                transp.save()

    @property
    def has_trajets(self):
        return bool(self.trajets_tries)

    @property
    def has_trajet_commun(self):
        return any(tr.commun_id for tr in self.trajets_tries)

    @cached_property
    def trajets_tries(self):
        if self.pk:
            return sorted(list(self.trajets.all()), key=attrgetter('heure_depart'))
        if self.modele:
            self.modele._check_trajets()
            trajets_tries = []
            for idx, traj in enumerate(self.modele.trajets_tries):
                trajet = copy(traj)
                trajet.transport = self
                trajet.to_date(self.heure_rdv)
                if idx == 0 and self.modele.heure_depart:
                    trajet.heure_depart = datetime.combine(
                        self.date, self.modele.heure_depart, tzinfo=timezone.get_current_timezone()
                    )
                trajets_tries.append(trajet)
            return trajets_tries
        return []

    def concretiser(self):
        if self.modele.transports.filter(date=self.date).exists():
            raise OccurrenceExistError()
        trajets = self.trajets_tries
        self.save()
        for traj in trajets:
            traj.pk = None
            traj._state.adding = True
            traj.transport = self
            # To be define if duree_calc and dist_calc should be recalculated for that instance.
            traj.save()

    @property
    def duree_calc(self):
        """Renvoie la durée totale estimée du transport, y.c. le retour pour un aller simple."""
        duree = sum(
            [traj.get_duree_calc() for traj in self.trajets_tries],
            timedelta()
        )
        if self.retour is False:
            duree += duree
        else:
            duree += self.duree_rdv
        return duree

    @property
    def duree_retour(self):
        """Durée du trajet retour, seulement pour un aller-retour."""
        duree = timedelta(0)
        if self.retour is False:
            return duree
        retour = False
        for traj in self.trajets_tries:
            if retour:
                duree += traj.get_duree_calc()
            if traj.destination_princ:
                retour = True
        return duree

    @property
    def dist_calc(self):
        return sum(traj.get_dist_calc() for traj in self.trajets_tries)

    @property
    def dist_calc_chauffeur(self):
        """En mètres. Si pas dist réelle, se rabattre sur estimation distances chauffeur à vol d'oiseau + 15%"""
        return (
            (self.chauff_aller_dist or
             int(distance_vo(self.chauffeur.empl_geo, self.adresse_depart().empl_geo) * 1150)
            ) +
            self.dist_calc +
            (self.chauff_retour_dist or
             int(distance_vo(self.trajets_tries[-1].destination.empl_geo, self.chauffeur.empl_geo) * 1150)
            )
        )

    @property
    def est_passe(self):
        if self.heure_arrivee:
            return (self.heure_arrivee + timedelta(seconds=900)) < timezone.now()
        # Estimation
        return self.heure_rdv + self.duree_rdv + timedelta(hours=4) < timezone.now()

    @property
    def est_passe_lointain(self):
        """Considéré comme lointain si plus vieux que le 1 du mois passé."""
        return self.date < (date.today().replace(day=1) - timedelta(days=31))

    def adresse_depart(self):
        return self.trajets_tries[0].origine

    def adresse_arrivee(self):
        return self.trajets_tries[-1].destination

    @property
    def heure_depart(self):
        return timezone.localtime(self.trajets_tries[0].heure_depart) if self.trajets_tries else None

    @property
    def origine(self):
        return self.trajets_tries[0].origine

    @property
    def _trajet_dest_princ(self):
        return next((traj for traj in self.trajets_tries if traj.destination_princ), None)

    @property
    def destination(self):
        return self._trajet_dest_princ.destination

    @property
    def vers_domicile(self):
        return len(self.trajets_tries) == 1 and self.trajets_tries[0].destination_domicile

    @property
    def heure_depart_retour(self):
        traj_princ = self._trajet_dest_princ
        if not traj_princ:
            return None
        next_ind = self.trajets_tries.index(traj_princ) + 1
        try:
            return timezone.localtime(self.trajets_tries[next_ind].heure_depart)
        except IndexError:
            return None

    @property
    def heure_arrivee(self):
        """
        Estimation +/- grossière de l'heure de fin du transport (compte tenu d'un
        retour dans tous les cas)
        """
        heure = max(self.trajets_tries[-1].heure_arrivee, timezone.localtime(self.heure_rdv))
        if heure:
            return heure.replace(second=0)
        return heure

    def can_edit(self, user):
        return False  # Edit on an calendar point of view.

    def can_edit_rapport(self, benev):
        """
        Édition possible d'un rapport de transport par un bénévole.
        Dès le 2 d'un mois, seuls les transports du mois courant sont éditables.
        """
        if date.today().day == 1:
            un_du_mois = (date.today() - timedelta(days=1)).replace(day=1)
        else:
            un_du_mois = date.today().replace(day=1)
        return self.date >= un_du_mois and self.chauffeur == benev

    def chauffeurs_potentiels(self):
        """
        Renvoie une liste hiérarchisée des chauffeurs potentiels:
        1. chauffeurs dispos, triés par préférence (ou incompat.) client, distance
        2. chauffeurs non dispos, même tri
        """
        chauffeurs = list(
            Benevole.objects.par_domaine('transport', actifs=True).exclude(
                pk__in=self.refus.values_list('chauffeur_id', flat=True) if self.pk else []
            ).prefetch_related(
                Prefetch('preferences', queryset=Preference.objects.filter(client=self.client))
            )
        )
        debut = self.heure_depart
        fin = self.heure_arrivee
        dispos = Dispo.get_for_period(debut, fin)
        for dispo in dispos:
            dispo._match = 'full' if (dispo.fin >= fin and dispo.debut <= debut) else 'partial'
        # Get all chauffeurs having at least one overlapping attributed trajet
        overlap_transp = set(Trajet.objects.annotate(
            heure_arrivee=F('heure_depart') + F('duree_calc')
        ).filter(
            transport__chauffeur__isnull=False, heure_arrivee__gt=debut, heure_depart__lt=fin
        ).values_list('transport__chauffeur_id', flat=True))
        chauffeurs_dispos_full = set()
        chauffeurs_dispos_partial = set()
        chauffeurs_dispos_cancelled = set()
        for dispo in dispos:
            if dispo.cancelled or dispo.categorie == 'absence':
                chauffeurs_dispos_cancelled.add(dispo.chauffeur_id)
            elif dispo.chauffeur_id not in overlap_transp:
                if dispo._match == 'full':
                    chauffeurs_dispos_full.add(dispo.chauffeur_id)
                else:
                    chauffeurs_dispos_partial.add(dispo.chauffeur_id)
        depart_geo = self.adresse_depart().empl_geo
        client_handis = set(self.client.handicaps_verbose)
        for chauffeur in chauffeurs:
            # Annoter les chauffeurs avec préférences, incompatibilités
            chauffeur.pref = next(iter(pref for pref in chauffeur.preferences.all()), None)
            chauffeur.incompats_client = client_handis & set(chauffeur.incompats_verbose)
            # Annoter les chauffeurs avec distance_vo (vol d'oiseau)
            if self.retour:
                chauffeur.distance_vo = distance_vo(chauffeur.empl_geo, depart_geo)
            else:
                # Pour un aller simple, le chauffeur peut aussi habiter près de la destination.
                dists = [d for d in [
                    distance_vo(chauffeur.empl_geo, depart_geo),
                    distance_vo(chauffeur.empl_geo, self.destination.empl_geo),
                ] if d is not None]
                chauffeur.distance_vo = min(dists) if dists else None
            if chauffeur.pk in chauffeurs_dispos_cancelled:
                chauffeur.priorite_dispo = 4
            elif chauffeur.pk in chauffeurs_dispos_full:
                chauffeur.priorite_dispo = 1
            elif chauffeur.pk in chauffeurs_dispos_partial:
                chauffeur.priorite_dispo = 2
            else:
                chauffeur.priorite_dispo = 3
        return sorted(
            chauffeurs,
            key=lambda ch: (
                ch.priorite_dispo,
                {'pref': -1, 'incomp': 1, None: len(ch.incompats_client)}.get(ch.pref and ch.pref.typ),
                ch.distance_vo if ch.distance_vo is not None else 1000
            )
        )

    def calc_chauffeur_dists(self, save=False):
        """
        Calcule les distances entre domicile chauffeur et départ, arrivée et domicile chauffeur.
        """
        try:
            self.chauff_aller_dist = distance_real(
                self.chauffeur.empl_geo, self.adresse_depart().empl_geo
            ).get('distance')
            self.chauff_retour_dist = distance_real(
                self.adresse_arrivee().empl_geo, self.chauffeur.empl_geo
            ).get('distance')
        except ORSUnavailable:
            pass
        else:
            if save:
                self.save(update_fields=['chauff_aller_dist', 'chauff_retour_dist'])

    def match(self, referent):
        return 1 if 'transp' in referent.facturation_pour else 0

    @property
    def rapport_complet(self):
        if self.statut == Transport.StatutChoices.ANNULE and self.rapport_chauffeur:
            return True
        if self.km and self.duree_eff:
            return True
        return False

    @property
    def temps_attente_conforme(self):
        return not self.temps_attente or self.temps_attente <= timedelta(hours=3)

    @property
    def km_conforme(self):
        dist_calc = self.dist_calc_chauffeur
        return ((self.km * 1000 - dist_calc) / dist_calc) <= 0.05

    @property
    def rapport_conforme(self):
        if not self.rapport_complet:
            return False
        if (
            not self.temps_attente_conforme or
            self.rapport_chauffeur != "" or
            any(fr.non_conforme for fr in self.frais.all()) or
            not self.km_conforme
        ):
            return False
        return True

    @property
    def tarif_km(self):
        return settings.TARIF_AVS if self.client.is_avs(self.heure_depart.date()) else settings.TARIF_NON_AVS

    @property
    def km_calc(self):
        """Calcul des km à facturer pour ce transport."""
        metres = sum([tr.dist_calc for tr in self.trajets_tries])
        if self.retour is False:
            # En cas d'aller simple, le kilométrage du retour est additionné (retour du chauffeur)
            metres_retour = Trajet.distance_entre(self.client, self.adresse_arrivee(), self.adresse_depart())
            if metres_retour is None:
                metres = metres * 2
            else:
                metres += metres_retour
        return round(metres / Decimal(1000), 1)

    def cout_km(self, from_calc=True):
        kms = max(self.km_calc, self.MIN_KM_FACTURES) if from_calc else self.km
        if kms is None:
            raise ValueError(f"Unable to compute cout_km when km is None (for {self})")
        return round(kms * self.tarif_km, 2)

    def frais_total(self):
        return sum(fr.cout for fr in self.frais.all())

    def cout_attente(self):
        """
        Facturation du temps d’attente pour chaque demi-heure dépassant 90 minutes.
        Renvoie (quantité, montant total).
        """
        if self.temps_attente and self.temps_attente.total_seconds() / 60 >= 90:
            num_periodes = int(((self.temps_attente.total_seconds() / 60 - 90) // 30))
            return num_periodes, num_periodes * self.COUT_ATTENTE_UNITE
        return 0, Decimal('0')

    def cout_annulation(self):
        _cout_annul = 0
        for depuis, cout in settings.FORFAIT_ANNULATION.items():
            if self.date > depuis:
                _cout_annul = cout
        return _cout_annul

    def donnees_facturation(self):
        """Renvoyer dictionnaire avec données de facturation"""
        if self.statut_fact == Transport.FacturationChoices.PAS_FACTURER:
            return None
        infos = {
            'no': self.pk,
            'rendez-vous': timezone.localtime(self.heure_rdv),
            'type': self.trajets_tries[0].get_typ_display(),
            'depart': str(self.adresse_depart()),
            'destination': str(self.destination),
            'avs': self.client.is_avs(self.heure_depart.date()),
        }
        if self.statut == Transport.StatutChoices.ANNULE:
            cout_annul = self.cout_annulation()
            km, cout_km = Decimal(0), Decimal(0)
            if self.statut_fact == Transport.FacturationChoices.FACTURER_ANNUL_KM:
                km = self.km
                cout_km = self.cout_km(from_calc=False)
            infos.update({
                'km': km,
                'cout_km': cout_km,
                'annulation': cout_annul,
                'total': cout_km + cout_annul,
            })
        else:
            cout_km = self.cout_km(from_calc=True)
            forfait = settings.FORFAIT_ALLER_RETOUR if self.retour else settings.FORFAIT_ALLER
            cout_total = cout_km + forfait
            infos.update({
                'km': max(self.km_calc, self.MIN_KM_FACTURES),
                'cout_km': cout_km,
                'cout_forfait': forfait,
            })
            _, cout_attente = self.cout_attente()
            if cout_attente > 0:
                infos['cout_attente'] = cout_attente
                cout_total += infos['cout_attente']

            liste_frais = []
            for frais in self.frais.filter(cout__gt=0):
                cout_total += frais.cout
                liste_frais.append({
                    'descr': "Facturation des frais de repas" if frais.typ == 'repas' else "Facturation des frais",
                    'cout': frais.cout,
                })
            infos.update({
                'frais': liste_frais,
                'total': cout_total,
            })
        return infos

    @classmethod
    def frais_mensuels(cls, mois):
        """Calcul des frais des chauffeurs pour le `mois`."""
        transports = Transport.objects.annotate(
            month=TruncMonth('heure_rdv', output_field=models.DateField()),
            repas=Sum('frais__cout', filter=Q(frais__typ='repas')),
            autres_frais=Sum('frais__cout', filter=~Q(frais__typ='repas')),
        ).filter(
            Q(month=mois) & (
                Q(statut=Transport.StatutChoices.CONTROLE) |
                (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur=True))
            )
        ).select_related('chauffeur').order_by('chauffeur__nom')
        chauffeurs = {}
        for transp in transports:
            chauffeurs.setdefault(transp.chauffeur, {key: Decimal(0) for key in [
                'kms', 'attente_nb', 'frais_attente', 'plusieurs_nb', 'frais_plusieurs',
                'frais_repas', 'frais_divers',
            ]})
            chauffeurs[transp.chauffeur]['kms'] += max(transp.km or 0, Transport.MIN_KM_FACTURES)
            nb, cout = transp.cout_attente()
            chauffeurs[transp.chauffeur]['attente_nb'] += nb
            chauffeurs[transp.chauffeur]['frais_attente'] += cout
            chauffeurs[transp.chauffeur]['frais_repas'] += transp.repas or 0
            chauffeurs[transp.chauffeur]['frais_divers'] += transp.autres_frais or 0
        # Transports à plusieurs clients
        communs = TrajetCommun.objects.annotate(
            month=TruncMonth('date')
        ).filter(month=mois).annotate(
            nb_plusieurs=Count('trajet') - 1
        )
        for commun in communs:
            if commun.chauffeur not in chauffeurs:
                continue
            chauffeurs[commun.chauffeur]['plusieurs_nb'] += commun.nb_plusieurs
            chauffeurs[commun.chauffeur]['frais_plusieurs'] += Transport.FRAIS_CLIENT_SUPPL * commun.nb_plusieurs
        return chauffeurs

    @classmethod
    def get_for_person(cls, chauffeur, start, end):
        # Granularité à la journée pour le moment.
        start = start.date()  # astimezone(timezone.utc)
        end = end.date()  # astimezone(timezone.utc)
        # To be tested
        return chauffeur.transports.filter(
            date__gte=start, date__lte=end
        ).order_by('heure_rdv')

    ### Event API ### NOQA
    @property
    def debut(self):
        """Event API"""
        return self.heure_depart

    @property
    def fin(self):
        """Event API"""
        return self.heure_arrivee

    @property
    def description(self):
        """Event API"""
        return f"Transport pour {self.client}"

    def spans(self):
        """
        Event API.
        Return number of 15min slices it occupies.
        """
        return round((self.fin - self.debut).seconds / 60 / 15, 1)

    def url_attrib(self):
        if self.pk:
            return reverse('transport-attrib', args=[self.pk])
        else:
            return reverse('transport-model-attrib', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])

    def url_edit(self):
        if self.pk:
            return reverse('transport-edit', args=[self.pk])
        else:
            return reverse('transport-model-edit', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])

    def url_cancel(self):
        if self.pk:
            return reverse('transport-cancel', args=[self.pk])
        else:
            return reverse('transport-model-cancel', args=[self.modele.pk, self.heure_rdv.strftime('%Y%m%d')])


class TrajetCommun(models.Model):
    """Trajet effectué par un même chauffeur pour plusieurs clients."""
    date = models.DateField()
    chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.PROTECT, related_name='transports_communs'
    )


class Trajet(models.Model):
    class Types(models.TextChoices):
        MEDIC = 'medic', 'Médico-thérapeutique'
        PARTICIP = 'particip', 'Participatif-intégratif'
        EMPLETTES = 'emplettes', 'Emplettes à deux'
        ENFANTS = 'enfants', 'Enfants'

    transport = models.ForeignKey(Transport, related_name="trajets", on_delete=models.CASCADE)
    typ = models.CharField("Type de déplacement", max_length=10, choices=Types.choices)
    heure_depart = models.DateTimeField("Heure de départ")
    origine_domicile = models.BooleanField(default=True)
    origine_adr = models.ForeignKey(Adresse, null=True, blank=True, on_delete=models.PROTECT, related_name="+")
    destination_domicile = models.BooleanField(default=False)
    destination_adr = models.ForeignKey(Adresse, null=True, blank=True, on_delete=models.PROTECT, related_name="+")
    destination_princ = models.BooleanField("Destination principale", default=False)
    duree_calc = models.DurationField("Durée estimée du trajet", null=True, blank=True)
    dist_calc = models.IntegerField("Distance estimée du trajet [m]", blank=True, null=True)
    commun = models.ForeignKey(TrajetCommun, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return (
            f"Trajet depuis «{self.origine}» jusqu’à «{self.destination}» "
            f"pour «{self.transport.client if self.transport else '<Transport à définir>'}» le «{self.heure_depart}»"
        )

    @property
    def origine(self):
        return AdresseClient(self.transport.client) if self.origine_domicile else self.origine_adr

    @property
    def destination(self):
        return AdresseClient(self.transport.client) if self.destination_domicile else self.destination_adr

    def get_duree_calc(self):
        if self.duree_calc is None:
            self.calc_trajet()
        return self.duree_calc

    def get_dist_calc(self):
        if self.dist_calc is None:
            self.calc_trajet()
        return self.dist_calc

    @property
    def heure_arrivee(self):
        duree = self.get_duree_calc()
        if duree is None:
            return None
        return timezone.localtime(self.heure_depart) + self.duree_calc

    def calc_trajet(self):
        try:
            dist_real = distance_real(self.origine.empl_geo, self.destination.empl_geo)
        except ORSUnavailable:
            return False
        if dist_real is not None and 'duration' in dist_real:
            self.duree_calc = timedelta(seconds=round(dist_real['duration']))
            self.dist_calc = round(dist_real['distance'])
            self.save(update_fields=['duree_calc', 'dist_calc'])
            return True
        return False

    @classmethod
    def distance_entre(cls, client, origine, destination):
        """Recherche de la distance d'un trajet en cherchant d'abord un trajet existant récent dans la BD."""
        filtre = {}
        if isinstance(origine, Adresse):
            filtre['origine_adr'] = origine
        else:
            filtre['origine_domicile'] = True
        if isinstance(destination, Adresse):
            filtre['destination_adr'] = destination
        else:
            filtre['destination_domicile'] = True
        existing = Trajet.objects.annotate(month=TruncMonth('heure_depart')).filter(
            transport__client=client, dist_calc__isnull=False, heure_depart__date__gt=date.today() - timedelta(days=40)
        ).filter(**filtre)
        if existing:
            return existing[0].dist_calc

        # Query the distance service
        dist = distance_real(origine.empl_geo, destination.empl_geo)
        if dist is not None and 'duration' in dist:
            return round(dist['distance'])
        return None

    def to_date(self, dt):
        initial_offset = timezone.localtime(self.heure_depart).utcoffset()
        # Adjust start/end to consider timezone change from initial event timezone
        heure = self.heure_depart.replace(year=dt.year, month=dt.month, day=dt.day)
        self.heure_depart = fix_occurrence_date(heure, initial_offset, reverse=False)


class JournalTransport(models.Model):
    transport = models.ForeignKey(
        Transport, on_delete=models.CASCADE, verbose_name='Transport', related_name='journaux'
    )
    description = models.TextField()
    quand = models.DateTimeField()
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        get_latest_by = "quand"

    def __str__(self):
        return f"{self.quand}: {self.description}"


class Refus(models.Model):
    transport = models.ForeignKey(Transport, related_name="refus", on_delete=models.CASCADE)
    chauffeur = chauffeur = models.ForeignKey(
        Benevole, blank=True, null=True, on_delete=models.PROTECT, related_name='+'
    )

    def __str__(self):
        return f"Refus du transport {self.transport} par {self.chauffeur}"


class Frais(models.Model):
    class TypeFrais(models.TextChoices):
        REPAS = 'repas', "Frais de repas"
        PARKING = 'parking', "Frais de parking"

    transport = models.ForeignKey(Transport, related_name="frais", on_delete=models.CASCADE)
    descriptif = models.TextField("Descriptif", blank=True)
    cout = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Coût")
    typ = models.CharField("Type de frais", max_length=7, choices=TypeFrais.choices)
    justif = models.FileField(upload_to="justificatifs", blank=True, verbose_name="Justificatif")

    def __str__(self):
        return f"Frais ({self.cout}) pour le transport {self.transport}"

    def description(self):
        return self.get_typ_display()

    @property
    def non_conforme(self):
        return self.typ != 'repas' and self.cout >= 7


class Preference(models.Model):
    PREF_TYPE = (
        ('pref', '😀 Préféré'),
        ('incomp', '😡 Incompatible'),
    )
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="preferences")
    chauffeur = models.ForeignKey(Benevole, on_delete=models.CASCADE, related_name="preferences")
    typ = models.CharField("Type de lien", max_length=10, choices=PREF_TYPE)
    remarque = models.TextField(blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='client_chauffeur_unique', fields=['client', 'chauffeur']),
        ]

    def __str__(self):
        return f"Lien «{self.typ}» entre {self.client} et {self.chauffeur}"


class Facture(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="factures_transports")
    mois_facture = models.DateField("Mois facturé")
    no = models.BigIntegerField("N° de facture")
    date_facture = models.DateField("Date de facture")
    autre_debiteur = models.ForeignKey(
        Referent, on_delete=models.PROTECT, null=True, blank=True, verbose_name="Autre débiteur"
    )
    nb_transp = models.PositiveSmallIntegerField("Nb de transports")
    montant_total = models.DecimalField("Montant total", max_digits=6, decimal_places=2)
    fichier_pdf = models.FileField("Facture PDF", upload_to="transports/factures")
    annulee = models.BooleanField(default=False)
    exporte = models.DateTimeField("Exporté vers compta", blank=True, null=True)
    export_err = models.TextField("Erreur d’exportation", blank=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='no_facture_unique', fields=['no']),
        ]

    def __str__(self):
        return f"Facture de {self.mois_facture.strftime('%m %Y')} pour {self.client}"

    @property
    def no_debiteur(self):
        return self.autre_debiteur.no_debiteur if self.autre_debiteur_id is not None else self.client.no_debiteur

    @classmethod
    def next_no(cls):
        no = (cls.objects.aggregate(no_max=Max('no'))['no_max'] or 1000000) + 1
        # Try to never finish by 1 (https://gitlab.com/croixrouge/transports/-/issues/21)
        if no % 10 == 1:
            no = no + 1
        return no

    @classmethod
    def esr_num(self, no_fact, no_debiteur, with_spaces=False):
        num = str(no_debiteur).zfill(14) + str(no_fact).zfill(12)
        num += esr.calc_check_digit(num)
        if with_spaces:
            return esr.format(num)
        return num

    def match(self, referent):
        """Renvoie la priorité de prise en charge de la facture si le référent est débiteur."""
        return 1 if 'transp' in referent.facturation_pour else 0

    def get_transports(self):
        return Transport.objects.a_facturer_pour(self.mois_facture, client=self.client).order_by('heure_rdv')

    def fs_path(self, check_exists=False):
        path = settings.MEDIA_ROOT.joinpath(
            'transports', 'factures',
            f'fact_{self.client.pk}_{self.mois_facture.year}_{self.mois_facture.month}.pdf'
        )
        if check_exists and not path.exists():
            return None
        return path


class Dispo(OccurrenceMixin, models.Model):
    class CategChoices(models.TextChoices):
        DISPO = 'dispo', 'Disponibilité'
        ABSENCE = 'absence', 'Absence'

    chauffeur = models.ForeignKey(Benevole, on_delete=models.CASCADE, related_name='dispos')
    debut = models.DateTimeField("Début", db_index=True)
    fin = models.DateTimeField("Fin", db_index=True, null=True, blank=True)
    description = models.TextField("Description", blank=True)
    categorie = models.CharField("Catégorie", max_length=10, choices=CategChoices.choices, default='dispo')
    regle = models.ForeignKey(
        Regle, on_delete=models.CASCADE, null=True, blank=True, verbose_name="Règle",
    )
    fin_recurrence = models.DateTimeField(
        "Dernière occurrence", null=True, blank=True, db_index=True,
    )

    cancelled = False

    def __lt__(self, other):
        return self.debut < other.debut

    def __repr__(self):
        return f"Dispo(debut={self.debut}, fin={self.fin})"

    @property
    def ev_category(self):
        return self.categorie

    def spans(self):
        """Return number of 15min slices it occupies."""
        return round((self.fin - self.debut).seconds / 60 / 15, 1)

    def can_edit(self, user):
        return getattr(user, 'benevole', None) == self.chauffeur or user.has_perm('transport.change_dispo')

    def cancel_for(self, day):
        initial_offset = timezone.localtime(self.debut).utcoffset()
        day_start = fix_occurrence_date(
            datetime.combine(day, self.debut.time(), tzinfo=self.debut.tzinfo),
            initial_offset, reverse=False
        )
        Occurrence.objects.create(dispo=self, orig_start=day_start, annule=True)

    @classmethod
    def _select_dispos(cls, qs, start, end, categ=None):
        """Return dispos that at least overlap the start-end interval."""
        start = start.astimezone(tz.utc)
        end = end.astimezone(tz.utc)
        unique_dispos = qs.filter(
            regle__isnull=True, fin__gte=start, debut__lt=end
        ).select_related('chauffeur')
        if categ is not None:
            unique_dispos = unique_dispos.filter(categorie=categ)
        recur_dispos = qs.filter(
            regle__isnull=False
        ).filter(
            models.Q(fin_recurrence__gte=start) | models.Q(fin_recurrence__isnull=True)
        ).select_related('regle', 'chauffeur').prefetch_related('exceptions')
        if categ is not None:
            recur_dispos = recur_dispos.filter(categorie=categ)
        dispos = list(unique_dispos)
        for ev in recur_dispos:
            dispos.extend([occ for occ in ev.get_occurrences(start, end)])
        return sorted(dispos)

    @classmethod
    def get_for_person(cls, benev, start, end, exclude=None, categ=None):
        # categ=None means all categs
        qs = benev.dispos
        if exclude:
            qs = qs.exclude(pk__in=[ev.pk for ev in exclude])
        return cls._select_dispos(qs, start, end, categ=categ)

    @classmethod
    def get_for_period(cls, start, end):
        return cls._select_dispos(cls.objects, start, end)

    def get_exceptions(self):
        return {occ.orig_start: occ for occ in self.exceptions.all()} if self.pk else {}

    def memory_instance(self, occ_start):
        duration = self.fin - self.debut
        return MemOccurrence(
            id=self.id, debut=occ_start, fin=occ_start + duration,
            description=self.description, categorie=self.categorie,
            chauffeur=self.chauffeur, excepts=self._exceptions,
        )


class MemOccurrence(Dispo):
    class Meta:
        proxy = True

    is_recurring = True

    def __init__(self, *args, excepts={}, **kwargs):
        super().__init__(*args, **kwargs)
        if self.debut in excepts and excepts[self.debut].annule:
            self.cancelled = True

    def __repr__(self):
        return f"MemOccurrence(debut={self.debut}, fin={self.fin})"


class Occurrence(models.Model):
    """Recurring event exceptions."""
    dispo = models.ForeignKey(Dispo, on_delete=models.CASCADE, related_name='exceptions')
    orig_start = models.DateTimeField("Début d’origine")
    annule = models.BooleanField(default=False)

    def __str__(self):
        return "Exception pour l’événement «%s»" % self.dispo


def fix_occurrence_date(dt, initial_offset, reverse=False):
    if reverse:
        tz_diff = initial_offset - timezone.localtime(dt).utcoffset()
    else:
        tz_diff = timezone.localtime(dt).utcoffset() - initial_offset
    if tz_diff:
        return dt - tz_diff
    return dt


def color_is_dark(c):
    if c == 'black':
        return True
    elif c == 'white':
        return False
    # Assume a rgb '#aabbcc' syntax
    rgb = int(c.strip('#'), 16)
    red = rgb >> 16 & 0xff
    green = rgb >> 8 & 0xff
    blue = rgb >> 0 & 0xff
    luma = 0.2126 * red + 0.7152 * green + 0.0722 * blue  # per ITU-R BT.709
    return luma < 120
