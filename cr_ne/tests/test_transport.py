from datetime import date, datetime, time
from decimal import Decimal
from unittest import mock

from django.test import TestCase, tag
from django.urls import reverse
from django.utils.timezone import get_current_timezone

from benevole.models import Activites, Benevole, NoteFrais
from client.models import Client, Referent
from common.test_utils import mocked_httpx
from transport.models import Facture, Frais, Transport
from transport.tests.tests import DataMixin
from ..views import get_api


class NETransportTests(DataMixin, TestCase):
    @tag("factures")
    def test_envoi_facture_mensuelle(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date(2023, 7, 12), time(11, 30), tzinfo=get_current_timezone()
            ),
            destination=self.hne_ne, retour=False, km=30,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=None,
        )
        facture = Facture.objects.create(
            client=self.trclient, mois_facture=date(2023, 7, 1), no='111', date_facture=date(2023, 8, 5),
            nb_transp=4, montant_total=Decimal('27.35'),
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            msg = 'Impossible d’envoyer une facture si le client n’est pas lié à un partenaire CID'
            with self.assertRaisesMessage(ValueError, msg):
                result = api.create_invoice('transport', [facture])
            self.trclient.id_externe = 42
            self.trclient.save()
            result = api.create_invoice('transport', [facture])
            sent_data = mock_response.call_args[1]
        self.maxDiff = None
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 42, 'beneficiaryid': 42,
                'invoicedate': '2023-08-05', 'period': '07/2023',
                'journal': 'F510',
                'lines': [{
                    'productref': '51.100', 'description': 'Transport du 12.07.2023 11:30',
                    'quantity': 25.4, 'unitamount': 0.9,
                }, {
                    'productref': '51.120', 'description': 'Forfait aller simple',
                    'quantity': 1.0, 'unitamount': 4.5,
                }],
            }
        )

    def test_autre_adresse_facturation(self):
        facture = Facture.objects.create(
            client=self.trclient, mois_facture=date(2023, 7, 1), no='111', date_facture=date(2023, 8, 5),
            nb_transp=4, montant_total=Decimal('27.35'),
        )
        self.assertEqual(self.trclient.adresse_facturation(facture), self.trclient)
        referent = Referent.objects.create(
            salutation='M', nom='Meier', prenom='Aristide', npa='2000', localite='Neuchâtel', rue='Fleurs 5',
            courriel='ad@example.com', facturation_pour=['transp'], client=self.trclient, no_debiteur='4321',
        )
        self.assertEqual(self.trclient.adresse_facturation(facture), referent)

    def test_envoi_note_frais(self):
        benev = Benevole.objects.create(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[Activites.TRANSPORT]
        )
        NoteFrais.objects.create(
            benevole=benev,
            service='transport',
            mois=date(2023, 4, 1),
            kms=5999,
        )
        client = Client.objects.create(nom='Donzé', prenom='Léa', type_client=['transport'])
        a_1130 = datetime.combine(
            date(2023, 5, 8), time(11, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=client,
            heure_rdv=a_1130,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=benev,
        )
        Frais.objects.create(transport=transport, typ='repas', cout='17.5')
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('notesfrais-transmission', args=[2023, 5]), data={'service': 'transport'}
            )
            sent_data = mock_response.call_args[1]
        note = benev.notefrais_set.get(mois=date(2023, 5, 1))
        self.assertEqual(note.kms, 4)
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'Note de frais transport',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 17.5},
                {'productref': 'NF-B10', 'quantity': 1.0, 'unitamount': 0.7}, # 1km * 0.70
                {'productref': 'NF-B11', 'quantity': 3.0, 'unitamount': 0.55}, # 3km * 0.55
            ],
        })
