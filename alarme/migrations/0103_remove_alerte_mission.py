from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0102_migrate_alerte_mission'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alerte',
            name='mission',
        ),
    ]
