from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0020_init_destination_princ"),
    ]

    operations = [
        migrations.AddField(
            model_name="adresse",
            name="date_archive",
            field=models.DateField(blank=True, null=True, verbose_name="Archivée le"),
        ),
        migrations.AddConstraint(
            model_name="adresse",
            constraint=models.UniqueConstraint(
                condition=models.Q(("date_archive__isnull", True)),
                fields=("nom",),
                name="nom_non_archive_unique",
            ),
        ),
    ]
