import math
from collections import defaultdict
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from io import BytesIO
from itertools import chain, groupby
from operator import attrgetter
from pathlib import Path

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import RedirectURLMixin
from django.core.cache import cache
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.db import transaction
from django.db.models import Count, F, Min, Prefetch, Q, Sum
from django.db.models.functions import Round, TruncMonth
from django.http import FileResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.dates import MONTHS
from django.utils.safestring import SafeString
from django.utils.text import slugify
from django.views.generic import (
    CreateView, DeleteView, FormView, TemplateView, UpdateView, View
)

import httpx
from openpyxl.styles import Alignment
from openpyxl.utils import get_column_letter
from workalendar.europe import Neuchatel

from benevole.models import Activites, Benevole, NoteFrais
from benevole.views import BenevoleListView
from client.models import Client, Journal
from client.views import ClientEditViewBase, ClientListViewBase
from common.distance import ORSUnavailable, distance_real
from common.export import CSVExport, OpenXMLExport
from common.forms import MoisForm
from common.models import DistanceFigee
from common.templatetags.common_utils import format_duree
from common.utils import arrondi_5
from common.views import (
    BasePDFView, CreateUpdateView, FilterFormMixin, GeoAddressMixin, JournalMixin,
    ListView
)
from . import forms
from .factures import FacturePDF, RemboursementPDF
from .models import (
    Adresse, Dispo, Facture, JournalTransport, Occurrence, OccurrenceExistError,
    Preference, Refus, Trajet, TrajetCommun, Transport, TransportModel
)
from .pdf import TransportsPDF
from .utils import generer_notes_frais

HOLIDAYS_TRANS = {
    'New year': 'Nouvel an',
    "Berchtold's Day": 'Deux de l’an',
    'Republic Day': 'Instauration de la République',
    'Good Friday': 'Vendredi saint',
    'Easter Monday': 'Lundi de Pâques',
    'Labour Day': 'Fête du Travail',
    'Ascension Thursday': 'Ascension',
    'Whit Monday': 'Lundi de Pentecôte',
    'National Holiday': 'Fête nationale',
    'Federal Thanksgiving Monday': 'Jeûne Fédéral',
    'Christmas Day': 'Noël',
    'Boxing Day': 'Lendemain de Noël',
}


class TransportJournalMixin(JournalMixin):
    is_create = False
    journal_add_message = "Création du transport"
    journal_edit_message = "Modification du transport: {fields}"

    def _create_instance(self, **kwargs):
        JournalTransport.objects.create(
            transport=self.object, **kwargs
        )


class AccueilTransportView(PermissionRequiredMixin, TemplateView):
    """Accueil pour transports."""
    template_name = 'transport/home.html'
    permission_required = 'transport.view_transport'


class GestionTransportView(PermissionRequiredMixin, TemplateView):
    """Aperçu pour gestion transports."""
    template_name = 'transport/gestion.html'
    permission_required = 'transport.view_transport'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        Transport.check_effectues()

        transp_query = Transport.objects.exclude(
            Q(date_facture__isnull=False) |
            (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur__isnull=False)) |
            (Q(statut=Transport.StatutChoices.CONTROLE) & Q(statut_fact=Transport.FacturationChoices.PAS_FACTURER))
        ).order_by('statut')
        counts = {
            line['statut']: line['tot']
            for line in transp_query.values('statut').annotate(tot=Count('id'))
        }
        if Transport.StatutChoices.RAPPORTE in counts and Transport.StatutChoices.ANNULE in counts:
            counts[Transport.StatutChoices.RAPPORTE] += counts[Transport.StatutChoices.ANNULE]

        context.update({
            'tabs': [
                (stat.label, stat.value, counts.get(stat.value, 0), reverse('transports-by-status', args=[stat.value]))
                for stat in Transport.StatutChoices
                if stat.value <= Transport.StatutChoices.CONTROLE
            ],
            'current_tab': self.kwargs.get('num', 1),
        })
        return context


class TransportByStatusView(TemplateView):
    """
    Liste 'brute' de transports par statut, généralement affichée dynamiquement
    sous un onglet.
    """
    template_name = 'transport/transports_list.html'
    line_compl_map = {
        Transport.StatutChoices.SAISI: '-date,-chauffeur',
        Transport.StatutChoices.ATTRIBUE: 'confirmer',
    }

    def dispatch(self, request, *args, **kwargs):
        if not self.request.headers.get('X-Requested-With'):
            return HttpResponseRedirect(reverse('gestion-with-tab', args=[kwargs['num']]))
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if request.GET:
            filter_data = request.GET
            if any(request.GET.values()):
                self.request.session['transport_form_data'] = filter_data
            else:
                filter_data = None
                self.request.session.pop('transport_form_data', None)
        else:
            filter_data = self.request.session.get('transport_form_data', None)
            if (
                filter_data and self.kwargs['num'] == Transport.StatutChoices.SAISI and
                [k for k, v in filter_data.items() if v] == ['chauffeur']
            ):
                filter_data = None
        self.filter_form = forms.TransportFilterForm(statut=self.kwargs['num'], data=filter_data)
        self.filtered = False
        return super().get(request, *args, **kwargs)

    def get_template_names(self):
        return (
            ['transport/transports_saisis.html'] if self.kwargs['num'] == Transport.StatutChoices.SAISI
            else self.template_name
        )

    def get_queryset(self):
        if self.kwargs['num'] == Transport.StatutChoices.RAPPORTE:
            transports = Transport.objects.filter(
                Q(statut=Transport.StatutChoices.RAPPORTE) |
                (Q(statut=Transport.StatutChoices.ANNULE) & Q(defrayer_chauffeur__isnull=True))
            )
        else:
            transports = Transport.objects.filter(statut=self.kwargs['num'], date_facture__isnull=True)
            if self.kwargs['num'] == Transport.StatutChoices.CONTROLE:
                transports = transports.exclude(statut_fact=Transport.FacturationChoices.PAS_FACTURER)
        limit_date_to = None
        if 'date' in self.kwargs:
            limit_date_to = datetime.strptime(self.kwargs['date'], '%Y-%m-%d').date()
            transports = transports.filter(date=limit_date_to)

        transports = transports.annotate(depart=Min('trajets__heure_depart')
        ).select_related(
            *(['client'] if self.kwargs['num'] == Transport.StatutChoices.SAISI else ['client', 'chauffeur'])
        ).prefetch_related(
            Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
            Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr'))
        ).order_by('depart')

        if self.filter_form.is_bound and self.filter_form.is_valid():
            transports = self.filter_form.filter(transports)
            self.filtered = True

        if self.kwargs['num'] == Transport.StatutChoices.SAISI:
            # Ajouter dans les transports saisis les occurrences des transports répétitifs
            if self.filter_form.is_bound and self.filter_form.is_valid():
                filter_func = self.filter_form.filter_occurrences
            elif limit_date_to:
                filter_func = lambda occs: [occ for occ in occs if occ.date == limit_date_to]
            else:
                filter_func = None
            occs = TransportModel.toutes_occurrences(filter_func=filter_func, prefetch_trajets=True)
            transports = sorted(list(transports) + occs)
        return transports

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'filter_form': self.filter_form,
            'statut': self.kwargs['num'],
            'line_compl': self.line_compl_map.get(self.kwargs['num'], ''),
        })
        transports = self.get_queryset()
        # Mise en forme: {<date>: [<transports>], ...}
        if self.filtered:
            context['object_list'] = {
                dt: {'transports': list(transps)} for dt, transps in groupby(transports, key=attrgetter('date'))
            }
            for line in context['object_list'].values():
                line['len'] = len(line['transports'])
        else:
            # Seule la première date contient les transports, les autres seront chargés dynamiquement
            transp_iter = groupby(transports, key=attrgetter('date'))
            try:
                dt, transps = next(transp_iter)
            except StopIteration:
                context['object_list'] = {}
            else:
                context['object_list'] = {dt: {'transports': list(transps)}}
                context['object_list'][dt]['len'] = len(context['object_list'][dt]['transports'])
                context['object_list'].update({
                    dt: {'len': len(list(transps)), 'transports': []} for dt, transps in transp_iter
                })
        return context


class TransportByDayView(TemplateView):
    template_name = 'transport/par_jour.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        jour = date(self.kwargs['year'], self.kwargs['month'], self.kwargs['day'])
        transports = Transport.objects.filter(date=jour).exclude(statut=Transport.StatutChoices.ANNULE)
        avec_chauffeurs = transports.filter(chauffeur__isnull=False).select_related('chauffeur').order_by('chauffeur')
        chauffeurs = {
            ch: {'transports': list(transps), 'dispos': []}
            for ch, transps in groupby(avec_chauffeurs, key=lambda t: t.chauffeur)
        }
        # Compléter par chauffeurs avec dispos
        jour_dispos = Dispo.get_for_period(
            datetime.combine(jour, time(0, 0)), datetime.combine(jour, time(23, 59))
        )
        chauffeurs_dispos = {}
        absences = defaultdict(list)
        for dispo in jour_dispos:
            if dispo.cancelled:
                continue
            if dispo.categorie == Dispo.CategChoices.ABSENCE:
                absences[dispo.chauffeur].append(dispo)
            else:
                if dispo.chauffeur in chauffeurs:
                    chauffeurs[dispo.chauffeur]['dispos'].append(dispo)
                else:
                    if dispo.chauffeur in chauffeurs_dispos:
                        chauffeurs_dispos[dispo.chauffeur]['dispos'].append(dispo)
                    else:
                        chauffeurs_dispos[dispo.chauffeur] = {'transports': [], 'dispos': [dispo]}

        def dispo_overlap(dispo, absences):
            return any(dispo.debut < abs_.fin and dispo.fin > abs_.debut for abs_ in absences)

        # Filtrer les dispos selon les absences
        for chauff, abslist in absences.items():
            if chauff in chauffeurs:
                chauffeurs[chauff]['dispos'] = [
                    dispo for dispo in chauffeurs[chauff]['dispos'] if not dispo_overlap(dispo, abslist)
                ]
            if chauff in chauffeurs_dispos:
                chauffeurs_dispos[chauff]['dispos'] = [
                    dispo for dispo in chauffeurs_dispos[chauff]['dispos'] if not dispo_overlap(dispo, abslist)
                ]
                if not chauffeurs_dispos[chauff]['dispos']:
                    del chauffeurs_dispos[chauff]
        context.update({
            'jour': jour,
            'non_attribues': transports.filter(chauffeur__isnull=True),
            'chauffeurs': chauffeurs,
            'chauffeurs_dispos': chauffeurs_dispos,
        })
        return context


class TransportSaisieView(TransportJournalMixin, CreateView):
    template_name = 'transport/saisie.html'
    form_class = forms.SaisieTransportForm
    is_create = True

    def get_initial(self):
        initial = {**super().get_initial(), 'typ': Trajet.Types.MEDIC}
        if self.request.GET.get('client'):
            try:
                client = Client.objects.get(pk=self.request.GET.get('client'))
            except Client.DoesNotExist:
                pass
            else:
                initial['client'] = client.pk
                initial['client_select'] = str(client)
        return initial

    def get_success_url(self):
        return reverse('transport-client-detail', args=[self.object.client_id])

    def form_valid(self, form):
        response = super().form_valid(form)
        if 'transport' not in self.object.client.type_client:
            self.object.client.type_client.append('transport')
            self.object.client.save()
        self.journalize(form)
        return response


class TransportCalculerDepart(View):
    def post(self, request, *args, **kwargs):
        heure_rdv = datetime.combine(
            date.today(),
            datetime.strptime(request.POST['heure_rdv'], '%H:%M').time(),
            tzinfo=timezone.get_current_timezone()
        )
        orig = (
            get_object_or_404(Adresse, pk=request.POST['origine'])
            if (request.POST['origine'] and request.POST['origine'] != 'null')
            else None
        )
        client = get_object_or_404(Client, pk=request.POST['client']) if request.POST['client'] else None
        if orig:
            orig_geo = orig.empl_geo
        else:
            orig_geo = client.empl_geo
        dests_geo = []
        for value in request.POST.getlist('destination'):
            # Toutes les destinations jusqu'à la destination principale
            if value == "-1" and client:
                dests_geo.append(client.empl_geo)
            elif value:
                dests_geo.append(get_object_or_404(Adresse, pk=value).empl_geo)
        if not orig_geo or not dests_geo:
            return JsonResponse({'result': 'error', 'reason': "Désolé, certaines infos ne sont pas géolocalisées."})
        cur_orig = orig_geo
        duree_transport = timedelta(0)
        try:
            for dest in dests_geo:
                if cur_orig == dest:
                    return JsonResponse(
                        {'result': 'error', 'reason': "Origine et destination doivent être différentes."}
                    )
                duree_transport += timedelta(seconds=distance_real(cur_orig, dest)['duration'])
                cur_orig = dest
        except ORSUnavailable:
            return JsonResponse({
                'result': 'error',
                'reason': (
                    "Le service Openrouteservice n’est actuellement pas disponible "
                    "pour calculer automatiquement l’heure de départ"
                ),
            })
        # Ajouter 10% de marge à la durée calculée, mais au minimum 10 min.
        # Pas de duree calculée au-dessous de 15 min.
        depart = heure_rdv - max(
            duree_transport + max(duree_transport * 0.1, timedelta(minutes=10)), timedelta(minutes=15)
        )
        round_min = round(depart.minute / 5) * 5
        if round_min == 60:
            depart = depart.replace(hour=depart.hour + 1, minute=0)
        else:
            depart = depart.replace(minute=round_min)
        return JsonResponse({
            'result': 'OK',
            'depart': depart.strftime('%H:%M'),
            'duree_trajet': format_duree(duree_transport),
        })


class FromModeleMixin:
    """
    Get Transport either from plain id, or from a generated instance from
    TransportModel if urlpattern has moddate.
    """
    def get_object(self):
        if 'moddate' not in self.kwargs:
            return super().get_object()
        else:
            pk = self.kwargs.get('transport_pk') or self.kwargs['pk']
            modele = get_object_or_404(TransportModel, pk=pk)
            dt = datetime.strptime(str(self.kwargs['moddate']), '%Y%m%d')
            return modele.get_occurrence(dt)


class TransportEditView(TransportJournalMixin, RedirectURLMixin, FromModeleMixin, UpdateView):
    template_name = 'transport/saisie.html'
    model = Transport
    form_class = forms.SaisieTransportForm
    success_url = reverse_lazy('gestion')
    next_page = success_url

    def get_initial(self):
        premier_trajet = self.object.trajets_tries[0]
        return {
            **super().get_initial(),
            'origine_adr': premier_trajet.origine_adr,
            'heure_depart': self.object.heure_depart.time(),
            'typ': premier_trajet.typ,
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'depart_retour': (
                self.object.heure_depart_retour.time()
                if self.object.retour and self.object.heure_depart_retour else '--:--'
            ),
            'arrivee_retour': (
                self.object.heure_arrivee.time()
                if self.object.retour else '--:--'
            ),
        }

    def form_valid(self, form):
        with transaction.atomic():
            if 'retour' in form.changed_data and form.cleaned_data['retour'] is False and form.instance.temps_attente:
                # Un transport aller-retour modifié en aller simple ne peut plus contenir de temps d'attente
                form.instance.temps_attente = None
            response = super().form_valid(form)
            self.journalize(form)
            if form.instance.modele and 'date' in form.changed_data:
                # Annuler l'occurrence à la date originale
                occ = form.instance.modele.get_occurrence(form.initial['date'])
                if occ:
                    occ.statut = Transport.StatutChoices.ANNULE
                    occ.raison_annulation = "Report de cette occurrence à une autre date."
                    occ.date_annulation = timezone.now()
                    occ.statut_fact = Transport.FacturationChoices.PAS_FACTURER
                    occ.defrayer_chauffeur = False
                    occ.concretiser()
        return response


class TransportModelEditView(UpdateView):
    template_name = 'transport/modele_edit.html'
    model = TransportModel
    form_class = forms.TransportModelForm
    success_url = reverse_lazy('gestion')

    def get_initial(self):
        initial = super().get_initial()
        if self.object.regle:
            if self.object.regle.nom == 'Chaque jour, du lundi au vendredi':
                initial['repetition'] = 'DAILY-open'
            else:
                initial['repetition'] = self.object.regle.frequence
            if self.object.fin_recurrence:
                initial['fin_recurrence'] = self.object.fin_recurrence.date()
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['first_instance'] = self.object.transports.order_by('heure_rdv').first()
        context['concrete_future_occs'] = self.object.transports.filter(date__gte=date.today())
        return context


class TransportCancelView(TransportJournalMixin, FromModeleMixin, UpdateView):
    model = Transport
    form_class = forms.CancelForm
    template_name = 'transport/transport_cancel_form.html'
    success_url = reverse_lazy('gestion')
    journal_edit_message = "Annulation du transport"

    def get_success_url(self):
        return reverse('transport-client-detail', args=[self.object.client.pk])

    def form_valid(self, form):
        uncancel = (
            self.object.statut == Transport.StatutChoices.ANNULE and
            'annule' in form.fields and not form.cleaned_data.get('annule')
        )
        with transaction.atomic():
            super().form_valid(form)
            if uncancel:
                if self.object.est_passe:
                    self.object.statut = Transport.StatutChoices.EFFECTUE
                else:
                    if self.object.chauffeur:
                        self.object.statut = Transport.StatutChoices.ATTRIBUE
                    else:
                        self.object.statut = Transport.StatutChoices.SAISI
                self.object.date_annulation = None
                self.object.raison_annulation = ''
                self.object.statut_fact = Transport.FacturationChoices.FACTURER
                self.object.defrayer_chauffeur = None
                msg = "Le transport qui était annulé a été réactivé"
                self.object.save()
                JournalTransport.objects.create(
                    transport=self.object, description=msg,
                    quand=timezone.now(), qui=self.request.user
                )
            else:
                self.object.statut = Transport.StatutChoices.ANNULE
                self.object.date_annulation = timezone.now()
                msg = "Le transport a bien été annulé"
                self.object.save()
                communs = list(TrajetCommun.objects.filter(trajet__in=self.object.trajets.all()))
                self.object.trajets.filter(commun__isnull=False).update(commun=None)
                # Delete TrajetCommun if only one trajet remaining
                for commun in communs:
                    if commun.trajet_set.count() < 2:
                        commun.delete()
                self.journalize(form)
        messages.success(self.request, msg)
        return JsonResponse({'result': 'OK', 'reload': self.get_success_url()})


class TransportAutoAttribView(View):
    def post(self, request, *args, **kwargs):
        dt = date(kwargs['year'], kwargs['month'], kwargs['day'])
        transports = Transport.objects.filter(date=dt, statut=Transport.StatutChoices.SAISI)
        model_occs = TransportModel.toutes_occurrences(
            filter_func=lambda occs: [occ for occ in occs if occ.date == dt],
            prefetch_trajets=True
        )
        successes = failures = 0
        for transp in chain(transports, model_occs):
            chauffeurs = [ch for ch in transp.chauffeurs_potentiels() if ch.priorite_dispo == 1]
            if not chauffeurs:
                failures += 1
            else:
                attrib_form = forms.AttributionForm(instance=transp, data={'chauffeur': chauffeurs[0]})
                if attrib_form.is_valid():
                    transport = attrib_form.save()
                    JournalTransport.objects.create(
                        transport=transport,
                        description=f"Attribution automatique d’un chauffeur: {transp.chauffeur}",
                        quand=timezone.now(), qui=request.user
                    )
                    successes += 1
                else:
                    failures += 1
        messages.success(
            request,
            f"{successes} transports ont été attribués pour le {dt.strftime('%d.%m.%Y')}. "
            f"{failures} transports n’ont pas pu être attribués."
        )
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))


class TransportAttribView(TransportJournalMixin, FromModeleMixin, UpdateView):
    model = Transport
    form_class = forms.AttributionForm
    template_name = 'transport/attribution.html'
    success_url = reverse_lazy('gestion')
    journal_edit_message = "Attribution d’un chauffeur: {obj.chauffeur}"

    def get_initial(self):
        initial = super().get_initial()
        initial['page_origine'] = self.request.headers.get('Referer', reverse('gestion'))
        return initial

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'chauffeurs': self.object.chauffeurs_potentiels(),
            'transports_prec': self.object.client.transports.filter(
                chauffeur__isnull=False).exclude(statut=Transport.StatutChoices.ANNULE
            ).order_by('-heure_rdv')[:6],
        }

    def form_valid(self, form):
        with transaction.atomic():
            try:
                super().form_valid(form)
            except OccurrenceExistError:
                raise SuspiciousOperation("Le formulaire a déjà été envoyé pour cette occurrence.")
            self.journalize(form)
        return HttpResponseRedirect(form.cleaned_data['page_origine'] or reverse('gestion'))


class TransportAttribRemoveView(View):
    model = Transport
    journal_edit_message = "Suppression du chauffeur"

    def post(self, request, *args, **kwargs):
        transport = get_object_or_404(self.model, pk=kwargs['pk'])
        transport.chauffeur = None
        transport.statut = Transport.StatutChoices.SAISI
        with transaction.atomic():
            transport.save()
            JournalTransport.objects.create(
                transport=transport, description=self.journal_edit_message, quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(reverse('gestion'))


class TransportRefuseView(View):
    def post(self, request, *args, **kwargs):
        transport = get_object_or_404(Transport, pk=kwargs['pk'])
        chauffeur = get_object_or_404(Benevole, pk=request.POST.get('chauffeur'))
        with transaction.atomic():
            Refus.objects.create(transport=transport, chauffeur=chauffeur)
            JournalTransport.objects.create(
                transport=transport, description=f"{chauffeur} a refusé ce transport",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK'})


class TransportConfirmView(TransportJournalMixin, FormView):
    form_class = forms.ConfirmationForm
    journal_edit_message = "Confirmation du transport"

    def form_invalid(self, form):
        messages.error(self.request, "Vous n’avez pas sélectionné de transport")
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))

    def form_valid(self, form):
        for transport in form.cleaned_data['selection']:
            transport.statut = transport.StatutChoices.CONFIRME
            transport.save()
            # Simulate some variables for journalize
            JournalTransport.objects.create(
                transport=transport, description=self.journal_edit_message, quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.CONFIRME]))


class TransportRapportView(PermissionRequiredMixin, TransportJournalMixin, RedirectURLMixin, UpdateView):
    model = Transport
    permission_required = 'transport.view_transport'
    form_class = forms.RapportForm
    base_template = 'transport/base.html'
    template_name = 'transport/rapport.html'
    next_page = reverse_lazy('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE])
    par_chauffeur = False
    journal_add_message = "Rapport initial du transport"
    journal_edit_message = "Modification du rapport: {fields}"

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'user': self.request.user}

    def form_valid(self, form):
        self.is_create = form.initial['km'] is None  # So add_message is used
        transport = form.save()
        if self.is_create and transport.statut == Transport.StatutChoices.ANNULE:
            add_message = "Rapport initial du transport (marqué comme annulé)"
            transport.statut_fact = Transport.FacturationChoices.FACTURER_ANNUL_KM
            transport.save()
        else:
            add_message = self.journal_add_message
        self.journalize(form, changed_values=True, add_message=add_message)
        return HttpResponseRedirect(self.get_success_url())


class TransportCheckView(TransportJournalMixin, View):
    def post(self, request, *args, **kwargs):
        transport = get_object_or_404(Transport, pk=self.kwargs['pk'])
        transport.statut = transport.StatutChoices.CONTROLE
        transport.save()
        JournalTransport.objects.create(
            transport=transport, description="Validation finale du transport", quand=timezone.now(), qui=request.user
        )
        return JsonResponse({'result': 'OK'})


class FinancesHomeView(PermissionRequiredMixin, TemplateView):
    permission_required = 'transport.gestion_finances'
    template_name = 'transport/finances.html'

    def get(self, request, *args, **kwargs):
        default_month = date.today().replace(day=1) - timedelta(days=10)
        form_data = {
            'year': request.GET.get('year', str(default_month.year)),
            'month': request.GET.get('month', str(default_month.month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'year': self.un_du_mois.year,
            'month': self.un_du_mois.month,
            'month_str': MONTHS[int(self.un_du_mois.month)],
            'date_form': self.form,
            'a_facturer': Transport.objects.a_facturer_pour(
                self.un_du_mois, billed=False
            ).exists(),
        }


class TransportFactureView(PermissionRequiredMixin, TransportJournalMixin, FormView):
    permission_required = 'transport.gestion_finances'
    form_class = forms.FacturerForm
    template_name = 'transport/facturer_form.html'
    journal_edit_message = "Facturation du transport"

    def get_initial(self):
        return {**super().get_initial(), 'date_facture': date.today()}

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'refacturer': 'fact_pk' in self.kwargs}

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'form_classes': 'unmonitor normal_submit',
        }
        if 'fact_pk' not in self.kwargs:
            # Facturation mensuelle, contrôler si transports non contrôlés
            context['non_controles'] = Transport.objects.filter(
                date__lt=date.today().replace(day=1),
                statut__in=[Transport.StatutChoices.EFFECTUE, Transport.StatutChoices.RAPPORTE]
            ).count()
        return context

    def form_valid(self, form):
        date_facture = form.cleaned_data['date_facture']
        if form.cleaned_data.get('choix_refact') == 'annuler':
            # Refaire une facture en conservant la précédente
            facture = get_object_or_404(Facture, pk=self.kwargs['fact_pk'])
            facture.annulee = True
            facture.save()
            _transports = facture.get_transports()
            if not _transports:
                messages.success(
                    self.request,
                    "La facture a été annulée, mais aucune autre facture ne la remplace "
                    "car il n'y a plus de transport à facturer"
                )
                return HttpResponseRedirect(reverse('client-transports-factures', args=[facture.client_id]))
            next_no = Facture.next_no()
            facture = Facture(
                client=facture.client,
                mois_facture=facture.mois_facture,
                no=next_no,
            )
            self.facturer_client(facture, _transports, date_facture, ecraser=False)
            response = FileResponse(
                facture.fichier_pdf, as_attachment=True,
                filename=facture.fichier_pdf.name
            )

        elif form.cleaned_data.get('choix_refact') == 'remplacer':
            # Refaire une facture précise en l'écrasant
            facture = get_object_or_404(Facture, pk=self.kwargs['fact_pk'])
            _transports = Transport.objects.a_facturer_pour(facture.mois_facture, client=facture.client)
            if not _transports:
                facture.delete()
                messages.success(
                    self.request,
                    "La facture a été effacée, mais aucune autre facture ne la remplace "
                    "car il n'y a plus de transport à facturer"
                )
                return HttpResponseRedirect(reverse('client-transports-factures', args=[facture.client_id]))
            else:
                self.facturer_client(facture, _transports, date_facture, ecraser=True)
                response = FileResponse(
                    facture.fichier_pdf, as_attachment=True,
                    filename=facture.fichier_pdf.name
                )

        else:
            from pypdf import PdfMerger

            # Facturer mois précédent pour tous les clients
            merger = PdfMerger(strict=False)
            un_du_mois_prec = (date.today().replace(day=1) - timedelta(days=2)).replace(day=1)
            _transports = Transport.objects.a_facturer_pour(un_du_mois_prec, client=None, billed=False)
            if not _transports:
                messages.warning(self.request, "Aucun transport à facturer pour ce mois")
                return HttpResponseRedirect(reverse('gestion-with-tab', args=[Transport.StatutChoices.CONTROLE]))
            for client, transports in groupby(_transports, key=lambda t: t.client):
                transports = list(transports)
                next_no = Facture.next_no()
                facture = Facture(
                    client=client,
                    mois_facture=un_du_mois_prec,
                    no=next_no,
                )
                self.facturer_client(facture, transports, date_facture, ecraser=False, merger=merger)
            # Renvoie un gros PDF avec toutes les factures
            temp = BytesIO()
            merger.write(temp)
            merger.close()
            temp.seek(0)
            response = FileResponse(
                temp, as_attachment=True,
                filename=f'factures_transports_{un_du_mois_prec.month}_{un_du_mois_prec.year}.pdf'
            )
        _transports.update(date_facture=date_facture)
        return response

    def facturer_client(self, facture, transports, date_fact, ecraser=True, merger=None):
        temp = BytesIO()
        facture_pdf = FacturePDF(temp)
        fact_result = facture_pdf.produce(transports, facture.no, date_fact)
        if not fact_result:
            return None
        # Écrire le fichier PDF sur le système de fichier
        if ecraser:
            fact_path = Path(facture.fichier_pdf.path)
        else:
            fact_path = facture.fs_path(check_exists=False)
            while fact_path.exists():
                # Suffix to not overwrite (can stack: ..._v2_v2_v2.pdf)
                fact_path = fact_path.with_name(fact_path.stem + '_v2' + fact_path.suffix)
            fact_path.parent.mkdir(parents=True, exist_ok=True)
            facture.fichier_pdf.name = str(fact_path.relative_to(settings.MEDIA_ROOT))
        fact_path.write_bytes(temp.getvalue())
        if merger is not None:
            temp.seek(0)
            merger.append(temp)

        if fact_result['debiteur'] != facture.client:
            facture.autre_debiteur = fact_result['debiteur']
        facture.date_facture = date_fact
        facture.nb_transp = fact_result['nb_transp']
        facture.montant_total = fact_result['total']
        facture.save()
        return fact_result


class FacturesDuMois(PermissionRequiredMixin, View):
    """Joindre toutes les factures du mois indiqué dans un gros PDF."""
    permission_required = 'transport.gestion_finances'

    def get(self, request, *args, **kwargs):
        from pypdf import PdfWriter

        mois = date(kwargs['year'], kwargs['month'], 1)
        merger = PdfWriter()
        for fact in Facture.objects.filter(mois_facture=mois, annulee=False):
            merger.append(fact.fichier_pdf.path)
        temp = BytesIO()
        merger.write(temp)
        merger.close()
        temp.seek(0)
        return FileResponse(
            temp, as_attachment=True,
            filename=f'factures_transports_{mois.month}_{mois.year}.pdf'
        )


class ComptaExportView(PermissionRequiredMixin, View):
    permission_required = 'transport.gestion_finances'
    headers = [
        'DocumentCode', 'Patient', 'CustomerNumber', 'Number', 'AccountReceivableDate',
        'DispositionDate', 'Currency', 'Amount', 'PaymentReferenceLineType',
        'PaymentReferenceLine', 'CollectiveAccount', 'External_Id', 'Name', 'Line1', 'Line2',
        'Country', 'ZIP', 'CITY', 'CreditAccount', 'CreditCostCentre1', 'CreditCostCentre2',
        'TaxIncluded', 'TaxAmount', 'TreatmentDate', 'Treatment', 'TreatmentRealDuration',
        'TreatmentInvoicedDuration', 'InvoicedAmountForTreatment', 'Position', 'Distance',
        'Frais', 'Repas', "Temps d'attente", 'Montant'
    ]

    def post(self, request, *args, **kwargs):
        try:
            export, filename = self.export_as_csv()
        except ValueError as err:
            messages.error(request, str(err))
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', ''))
        return export.get_http_response(filename)

    def export_as_csv(self):
        if 'fact_pk' in self.kwargs:
            # Exporter pour un client unique
            facture = get_object_or_404(Facture, pk=self.kwargs['fact_pk'])
            un_du_mois_prec = facture.mois_facture
            factures = [facture]
            client = facture.client
        else:
            # Exporter pour tous les clients du mois en paramètre
            client = None
            un_du_mois_prec = date(year=self.kwargs['year'], month=self.kwargs['month'], day=1)
            factures = Facture.objects.filter(
                mois_facture=un_du_mois_prec, annulee=False
            ).select_related('client').order_by('client__nom', 'client__prenom')
        der_du_mois = (un_du_mois_prec + timedelta(days=40)).replace(day=1) - timedelta(days=1)
        export = CSVExport(delimiter=';')
        # Pas d'en-tête (#206)
        # export.write_line(self.headers, bold=True)
        for facture in factures:
            fact_code = 'NC' if facture.annulee else 'FA'
            export.write_line([
                fact_code, f'Transports {facture.client.nom.upper()} {facture.client.prenom}', '', facture.no,
                der_du_mois.strftime('%d.%m.%Y'), facture.date_facture.strftime('%d.%m.%Y'),
                '', '', '', facture.esr_num(facture.no, facture.client.no_debiteur, with_spaces=True),
                '', facture.no_debiteur,
                '', '', '', '', '', '', '340700', '6100', '', '', '', un_du_mois_prec.strftime('%d.%m.%Y'),
                '', '', '', facture.montant_total, facture.nb_transp, '', '', '', '', ''
            ])
        file_name = f'compta_export_{un_du_mois_prec.year}_{un_du_mois_prec.month:02}.csv'
        if client:
            file_name = f'client_{client.pk}_{file_name}'
        return export, file_name


class DefraiementView(PermissionRequiredMixin, ListView):
    template_name = 'transport/defraiements.html'
    permission_required = 'transport.gestion_finances'

    def get(self, request, *args, **kwargs):
        form_data = {
            'year': request.GET.get('year', str(date.today().year)),
            'month': request.GET.get('month', str(date.today().month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        # Modèles NoteFrais ou calcul dynamique
        self.dynamic = False
        notes = NoteFrais.objects.filter(
            service='transport', mois=self.un_du_mois
        ).annotate(
            montant_total=Sum(Round(F('lignes__quantite') * F('lignes__montant_unit') * 20) / 20)
        ).prefetch_related('lignes').order_by('benevole__nom')
        # Pour l'instant, il n'existe pas encore de NoteFrais pour les transports.
        if not notes.exists():
            # Calcul dynamique des frais
            self.dynamic = True
            chauffeurs_data = Transport.frais_mensuels(self.un_du_mois)
            chauffeurs = sorted(chauffeurs_data.keys(), key=attrgetter('nom'))
            results = []
            for chauff in chauffeurs:
                values = chauffeurs_data[chauff]
                values['benevole'] = chauff
                results.append(values)
            return results
        return notes

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        totaux = {key: Decimal('0') for key in [
            'frais_repas', 'frais_divers', 'frais_attente', 'frais_plusieurs',
            'kms', 'frais_kms', 'montant_total'
        ]}
        for line in context['object_list']:
            if self.dynamic:
                line['frais_kms'] = arrondi_5(line['kms'] * settings.TARIF_CHAUFFEUR[0])
                line['montant_total'] = sum([
                    line['frais_repas'], line['frais_divers'],  line['frais_attente'],
                    line['frais_plusieurs'], line['frais_kms']
                ])
                for key in totaux.keys():
                    totaux[key] += line[key]
            else:
                lignes = [ligne for ligne in line.lignes.all()]
                def sum_by_codes(codes):
                    return sum([l.montant for l in lignes if l.libelle.no in codes])
                line.frais_kms = sum_by_codes(['NF-B10', 'NF-B11'])
                line.frais_repas = sum_by_codes(['NF-B07'])
                line.frais_divers = sum_by_codes(['NF-B06'])
                line.frais_attente = sum_by_codes(['NF-B12'])
                line.frais_plusieurs = sum_by_codes(['NF-B13'])
                for key in totaux.keys():
                    totaux[key] += getattr(line, key)
        context.update({
            'date_form': self.form,
            'totaux': totaux,
            'mois_passe': (self.un_du_mois + timedelta(days=32)).replace(day=1) <= date.today(),
        })
        if settings.NOTESFRAIS_TRANSMISSION_URLNAME:
            context['transmission_url'] = reverse(
                settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[self.un_du_mois.year, self.un_du_mois.month]
            )
        return context


class DefraiementExport(OpenXMLExport):
    default_font_size = 16


class ChauffeursDefraiementExportView(PermissionRequiredMixin, View):
    permission_required = 'transport.gestion_finances'
    headers = [
        'Chauffeurs', 'Mois de la prestation',
        '602 - Nb km', '603 - Nb km >6000',
        "604 - Frais durant Temps d'attente",
        '605 - Transport de plusieurs personnes (nb)', 'Montant = Transport de plusieurs personnes (CHF)',
        '606 - Indemnités effectives - Frais de repas (CHF)',
        '606 - Indemnités effectives - Frais divers (CHF)',
        'Montant à défrayer (y.c. frais + km)',
    ]
    DEFR_PERS_SUPPL = 5

    def post(self, request, *args, **kwargs):
        un_du_mois = date(year=kwargs['year'], month=kwargs['month'], day=1)

        if not NoteFrais.objects.filter(service='transport', mois=un_du_mois).exists():
            generer_notes_frais(un_du_mois)

        export = DefraiementExport(col_widths=[43, 12, 10, 10, 8, 8, 8, 8, 8, 15])
        export.write_line(self.headers, bold=True, alignment=Alignment(text_rotation=90), height=325, borders='all')
        export.ws.freeze_panes = export.ws['B2']
        export.ws.print_title_rows = '1:1'
        export.ws.page_setup.fitToPage = True
        export.ws.page_setup.fitToWidth = 1
        export.ws.page_setup.fitToHeight = 2
        export.ws.set_printer_settings(paper_size=export.ws.PAPERSIZE_A4, orientation='portrait')

        chauffeurs = Transport.frais_mensuels(un_du_mois)
        liste_notes = NoteFrais.objects.filter(
            service='transport', mois=un_du_mois
        ).prefetch_related('lignes').order_by('benevole__nom', 'benevole__prenom')

        num_lines = len(liste_notes)
        if not num_lines:
            messages.error(request, "Aucun frais n’a été enregistré pour ce mois")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('finances')))

        num_formats = {
            3: '0.00', 4: '0.00', 5: '0.00', 6: '0.00', 7: '0.00', 8: '0.00',
            9: '0.00', 10: "0.00 [$CHF]",
        }
        for idx, note in enumerate(liste_notes, start=2):
            # frais_km + frais_attente + frais_plusieurs + frais_repas + frais_divers
            total_formula = (
                f'=(ROUND(C{idx}*{settings.TARIF_CHAUFFEUR[0]}*20, 0) / 20)+'
                f'(ROUND(D{idx}*{settings.TARIF_CHAUFFEUR[6001]}*20, 0) / 20)+'
                f'E{idx}+G{idx}+H{idx}+I{idx}'
            )
            frais_attente = note.somme_pour('NF-B12')
            frais_repas = note.somme_pour('NF-B07')
            frais_divers = note.somme_pour('NF-B06')
            frais_plusieurs = note.somme_pour('NF-B13')
            kms_below_6000 = note.quantite_pour('NF-B10')
            kms_over_6000 = note.quantite_pour('NF-B11')
            export.write_line(
                [f'Transports {note.benevole.nom.upper()} {note.benevole.prenom}', un_du_mois,
                 kms_below_6000, kms_over_6000, frais_attente, frais_plusieurs // self.DEFR_PERS_SUPPL,
                 f'=F{idx}*{self.DEFR_PERS_SUPPL}', frais_repas, frais_divers, total_formula
                ],
                bold=[1],
                number_formats=num_formats,
                height=27,
                borders='all'
            )
        export.write_line(
            ['Totaux', ''] + [
                f'=SUM({get_column_letter(col)}2:{get_column_letter(col)}{num_lines + 1})' for col in range(3, 11)
            ],
            bold=True,
            number_formats=num_formats,
            height=27,
            borders='all'
        )
        return export.get_http_response(f'defraiements_{un_du_mois.year}_{un_du_mois.month:02}.xlsx')


class TrajetsDetailView(TemplateView):
    template_name = 'transport/trajet.html'

    def get_context_data(self, **kwargs):
        transport = get_object_or_404(Transport, pk=self.kwargs['pk'])
        trajets = []
        for trajet in transport.trajets_tries:
            route = distance_real(trajet.origine.empl_geo, trajet.destination.empl_geo, with_geom=True)
            trajets.append({
                'trajet': trajet,
                'route': route,  # line in 'linestring' key
            })
        return {
            **super().get_context_data(**kwargs),
            'trajets': trajets,
        }


class TrajetSetDistanceView(View):
    fixed = False

    def post(self, request, *args, **kwargs):
        trajet = get_object_or_404(Trajet, pk=self.kwargs['pk'])
        try:
            km = Decimal(request.POST.get('distance_km'))
        except TypeError:
            messages.error(request, "Impossible de considérer «{km}» comme une distance en km.")
        else:
            trajet.dist_calc = int(km * 1000)
            with transaction.atomic():
                trajet.save()
                JournalTransport.objects.create(
                    transport=trajet.transport, description="Forcer un trajet à {km}km",
                    quand=timezone.now(), qui=request.user,
                )
                if self.fixed:
                    dist, created = DistanceFigee.objects.get_or_create(
                        empl_geo_dep=trajet.origine.empl_geo,
                        empl_geo_arr=trajet.destination.empl_geo,
                        defaults={'distance': km},
                    )
                    if not created:
                        dist.distance = km
                        dist.save()
                    # Delete any cache entry set in distance_real
                    cache_key = str(hash(tuple(trajet.origine.empl_geo + trajet.destination.empl_geo)))
                    cache.delete(cache_key)

        return HttpResponseRedirect(reverse('trajets-details', args=[trajet.transport.pk]))


class TrajetAPlusieursView(FormView):
    template_name = 'transport/trajet_a_plusieurs.html'
    form_class = forms.TrajetAPlusieursForm

    def dispatch(self, *args, **kwargs):
        self.trajet = get_object_or_404(Trajet.objects.select_related('transport'), pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'trajet': self.trajet}

    def form_valid(self, form):
        if not form.cleaned_data['trajets']:
            if self.trajet.commun:
                self.trajet.commun.delete()
        else:
            if not self.trajet.commun:
                self.trajet.commun = TrajetCommun.objects.create(
                    date=self.trajet.heure_depart.date(), chauffeur=self.trajet.transport.chauffeur
                )
                self.trajet.save(update_fields=['commun'])
            else:
                self.trajet.commun.trajet_set.exclude(pk=self.trajet.pk).update(commun=None)
            for traj in form.cleaned_data['trajets']:
                traj.commun = self.trajet.commun
                traj.save(update_fields=['commun'])
        return JsonResponse({
            'result': 'OK',
            'reload': 'page',
        })


class ClientListView(ClientListViewBase):
    template_name = 'transport/client_list.html'
    filter_formclass = forms.ClientFilterForm
    return_all_if_unbound = False
    client_types = ['transport']

    def get_queryset(self):
        if self.is_archive:
            return super(ClientListViewBase, self).get_queryset().annotate(
                num_transp=Count('transports')
            ).filter(
                Q(type_client__contains=self.client_types, archive_le__isnull=False) |
                Q(~Q(type_client__contains=self.client_types) & Q(num_transp__gt=0))
            ).order_by('nom', 'prenom')
        return super().get_queryset()

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "transport/base.html",
            'home_url': reverse('home'),
            'client_url_name': 'transport-client-detail',
        }


class ClientEditView(ClientEditViewBase):
    template_name = 'transport/client_edit.html'
    form_class = forms.ClientEditForm
    client_type = 'transport'

    def get_success_url(self):
        return reverse('transport-client-detail', args=[self.object.pk])

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "transport/base.html",
        }

    def form_valid(self, form):
        if not form.instance.pk:
            form.instance.type_client = ['transport']
        with transaction.atomic():
            response = super().form_valid(form)
            if 'date_deces' in form.changed_data:
                self.object.archiver(self.request.user, 'transport')
        return response


class ClientDetailView(TemplateView):
    template_name = 'transport/client_detail.html'

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        prochains = self.client.transports.filter(date__gte=date.today())
        occs = TransportModel.toutes_occurrences(client=self.client, prefetch_trajets=False)
        context.update({
            'client': self.client,
            'prochains_transports': sorted(list(prochains) + occs),
            'preferences': self.client.preferences.filter(
                chauffeur__archive_le__isnull=True,
            ).select_related('chauffeur'),
        })
        return context


class ClientPrefEditView(CreateUpdateView):
    model = Preference
    form_class = forms.PreferenceEditForm
    template_name = 'transport/preference_edit.html'
    pk_url_kwarg = 'pk_pref'
    json_response = True

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'client': get_object_or_404(Client, pk=self.kwargs['pk']),
        }

    def form_valid(self, form):
        with transaction.atomic():
            # TODO: edit only remarque field
            if not form.instance.client_id:
                form.instance.client = form.client
                if form.cleaned_data['typ'] == 'pref':
                    add_message = f"Ajout préférence pour chauffeur {form.cleaned_data['chauffeur']}"
                else:
                    add_message = f"Ajout incompatibilité avec chauffeur {form.cleaned_data['chauffeur']}"
                Journal.objects.create(
                    client=form.instance.client, description=add_message,
                    quand=timezone.now(), qui=self.request.user
                )
            super().form_valid(form)
        return JsonResponse({
            'result': 'OK',
            'reload': 'page',
        })


class ClientPrefDeleteView(DeleteView):
    model = Preference

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer cette préférence.")
        with transaction.atomic():
            self.object.delete()
            if self.object.typ == 'pref':
                del_message = f"Suppression préférence pour chauffeur {self.object.chauffeur}"
            else:
                del_message = f"Suppression incompatibilité avec chauffeur {self.object.chauffeur}"
            Journal.objects.create(
                client=self.object.client, description=del_message,
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientTransportsView(PermissionRequiredMixin, FilterFormMixin, ListView):
    model = Transport
    permission_required = 'transport.view_transport'
    filter_formclass = forms.TransportArchivesFilterForm
    paginate_by = 25
    template_name = "transport/par_client_archives.html"
    passes = True  # Utilisé uniquement pour les archives pour le moment

    def get(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        if self.passes:
            qs = self.client.transports.filter(
                date__lt=date.today()
            ).prefetch_related(
                Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
                Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr')),
            ).order_by('-heure_rdv')
            # super() will apply filtering
            return super().get_queryset(base_qs=qs)
        else:
            return self.client.transports.filter(date__gte=date.today()).order_by('heure_rdv')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'filter_form': self.filter_form,
        }


class ClientListeFacturesView(PermissionRequiredMixin, TemplateView):
    permission_required = 'transport.view_transport'
    template_name = 'transport/client_factures.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'client': client,
            'factures': client.factures_transports.order_by('-mois_facture'),
            'trans_url': settings.FACTURES_TRANSMISSION_URL,
        }


class ClientDerniersTransportsView(PermissionRequiredMixin, TemplateView):
    """Affichage dynamique des derniers transports du client dans certaines pages."""
    template_name = 'transport/client_derniers.html'
    permission_required = 'transport.view_transport'

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.request.GET.get('client'))
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'transports': self.client.transports.order_by('-heure_rdv')[:5],
        }


class ClientAdresseJsonView(View):
    """
    Détails du client demandés par le formulaire de saisie lors de la sélection d’un client.
    """
    def get(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=request.GET.get('id'))
        return JsonResponse({
            'rue': client.rue, 'npa': client.npa, 'localite': client.localite,
            'fonds_transport': client.fonds_transport,
            'remarques': '<br>'.join(
                rem for rem in [client.remarques_int_transport, client.remarques_ext] if rem
            ),
            'geolocalise': client.empl_geo is not None,
        })


class AdresseListeView(FilterFormMixin, ListView):
    model = Adresse
    filter_formclass = forms.AdresseFilterForm
    paginate_by = 30
    template_name = 'transport/adresse_list.html'

    def get_queryset(self):
        return super().get_queryset(base_qs=Adresse.objects.actives().order_by('nom'))


class AdresseEditView(GeoAddressMixin, PermissionRequiredMixin, CreateUpdateView):
    template_name = 'transport/adresse_edit.html'
    model = Adresse
    permission_required = 'transport.change_adresse'
    form_class = forms.AdresseEditForm
    json_response = True

    def get_initial(self):
        return {**super().get_initial(), 'target': self.request.GET.get('target')}

    def get_success_url(self):
        return None

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'futurs': Transport.objects.filter(
                Q(date__gte=date.today()) & (Q(trajets__origine_adr=self.object) |
                Q(trajets__destination_adr=self.object))
            ).distinct().order_by('date'),
        }
        return context

    def form_valid(self, form):
        super().form_valid(form)
        adresse = self.object
        return JsonResponse({
            'result': 'OK', 'reload': 'populateAC' if self.is_create else 'page',
            'target': form.cleaned_data.get('target'),
            'object': {'value': adresse.pk, 'label': str(adresse)},
        })


class AdresseArchiveView(PermissionRequiredMixin, DeleteView):
    model = Adresse
    permission_required = 'transport.delete_adresse'

    def form_valid(self, form):
        self.object.date_archive = date.today()
        self.object.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class AdresseAutocompleteView(View):
    """Endpoint for autocomplete search for Adresse."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        results = [
            {'label': str(adr), 'value': adr.pk}
            for adr in Adresse.objects.actives().filter(
                Q(nom__unaccent__icontains=term) |
                Q(localite__unaccent__icontains=term) |
                Q(rue__unaccent__icontains=term)
            )[:20]
        ]
        if 'domicile du client'.startswith(term.lower()):
            results.insert(0, {'label': "Domicile du client", 'value': '-1'})
        return JsonResponse(results, safe=False)


class SearchChAutocompleteView(View):
    """Search on search.ch for new Adresse proposals."""
    # https://tel.search.ch/api/help
    api_base = 'https://tel.search.ch/api/'

    def get(self, request, *args, **kwargs):
        import feedparser
        term = request.GET.get('q')
        # Limiter d'abord à NE…
        resp = httpx.get(self.api_base, params={'q': term, 'wo': 'ne', 'key': settings.SEARCH_CH_API_KEY})
        feed = feedparser.parse(resp.text)
        if len(feed.entries) == 0:
            # … puis étendre si on n'obtient pas de résultats
            resp = httpx.get(self.api_base, params={'q': term, 'key': settings.SEARCH_CH_API_KEY})
            feed = feedparser.parse(resp.text)
        results = []
        for line in feed.entries:
            nom = ' '.join([txt for txt in [line.get('tel_name'), line.get('tel_firstname')] if txt])
            rue = ' '.join([item for item in [line.get('tel_street'), line.get('tel_streetno')] if item])
            results.append({
                'label': f"{nom}, {rue}, "
                         f"{line.get('tel_zip')} {line.get('tel_city')}",
                'value': line.id,
                'details': {
                    'nom': nom,
                    'npa': line.get('tel_zip'),
                    'localite': line.get('tel_city'),
                    'rue': rue,
                    'tel': line.get('tel_phone'),
                }
            })
        return JsonResponse(results, safe=False)


class ChauffeurListView(BenevoleListView):
    template_name = 'transport/chauffeur_list.html'
    filter_formclass = forms.ChauffeurFilterForm


class ChauffeurTransportsView(FilterFormMixin, ListView):
    model = Transport
    filter_formclass = forms.TransportArchivesFilterForm
    paginate_by = 25
    passes = False

    def get(self, *args, **kwargs):
        self.chauffeur = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_template_names(self):
        return ['transport/par_chauffeur_archives.html'] if self.passes else ['transport/par_chauffeur.html']

    def get_queryset(self):
        if self.passes:
            qs = self.chauffeur.transports.filter(
                date__lt=date.today()
            ).select_related('client').prefetch_related(
                'frais',
                Prefetch('journaux', queryset=JournalTransport.objects.select_related('qui')),
                Prefetch('trajets', queryset=Trajet.objects.select_related('origine_adr', 'destination_adr')),
            ).order_by('-heure_rdv')
            # super() will apply filtering
            return super().get_queryset(base_qs=qs)
        else:
            return self.chauffeur.transports.filter(date__gte=date.today()).order_by('heure_rdv')

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'benevole': self.chauffeur, 'chauffeur': self.chauffeur,
            'activite': Activites.TRANSPORT,
            'filter_form': self.filter_form,
        }
        if self.passes:
            liste_mois = sorted(
                self.chauffeur.transports.annotate(
                    month=TruncMonth('date')).values_list('month', flat=True
                ).distinct(), reverse=True
            )
            if liste_mois and liste_mois[0] == date.today().replace(day=1):
                liste_mois = liste_mois[1:]
            context['remboursements'] = liste_mois
        return context


class ChauffeurTransportsPDFView(FormView):
    form_class = forms.DateRangeSelectionForm
    template_name = 'transport/selection_date_transports.html'

    def dispatch(self, *args, **kwargs):
        self.chauffeur = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        return {'date_de': date.today(), 'date_a': date.today() + timedelta(days=1)}

    def form_valid(self, form):
        transports = self.chauffeur.transports.filter(
            date__range=(form.cleaned_data["date_de"], form.cleaned_data["date_a"])
        ).exclude(statut=Transport.StatutChoices.ANNULE).order_by('heure_rdv')
        if not transports:
            messages.error(self.request, "Aucun transport dans la plage de dates indiquée")
            return HttpResponseRedirect(reverse('benevole-transports', args=[self.chauffeur.pk]))
        temp = BytesIO()
        pdf = TransportsPDF(temp)
        pdf.produce(
            transports[0].chauffeur, transports, form.cleaned_data["date_de"], form.cleaned_data["date_a"]
        )
        temp.seek(0)
        return FileResponse(
            temp, as_attachment=True,
            filename=(f'transports_{slugify(" ".join([self.chauffeur.nom, self.chauffeur.prenom]))}'
                      f'_{form.cleaned_data["date_de"]}_{form.cleaned_data["date_a"]}.pdf')
        )


class ChauffeurRembPDFView(PermissionRequiredMixin, BasePDFView):
    permission_required = 'transport.view_transport'
    obj_class = Benevole
    pdf_class = RemboursementPDF

    def get(self, request, *args, **kwargs):
        chauffeur = self.get_object()
        mois = date(self.kwargs["year"], self.kwargs["month"], 1)
        self.produce_kwargs = {
            'month': mois,
            'transports': chauffeur.transports.filter(
                date__year=mois.year, date__month=mois.month
            ).exclude(
                defrayer_chauffeur=False
            ).prefetch_related('trajets', 'frais').order_by('heure_rdv'),
        }
        return super().get(request, *args, **kwargs)


class CRHolidays(Neuchatel):
    extra_days = {
        2022: [
            (date(2022, 2, 28), "Congé supplémentaire"),
            (date(2022, 12, 27), "Congé supplémentaire"),
            (date(2022, 12, 28), "Congé supplémentaire"),
        ],
    }

    def has_berchtolds_day(self, year):
        return True

    def get_variable_days(self, year):
        days = super().get_variable_days(year)
        # 26.12 is conditional in official calendar, add it inconditionally.
        if date(year, 12, 26) not in [day for day, _ in days]:
            days.append((date(year, 12, 26), self.boxing_day_label))
        days.append((date(year, 12, 31), 'St-Sylvestre'))
        return days


class AgendaMixin:
    START_HOUR = 8
    END_HOUR = 19
    NUM_DAYS = 7

    def events_by_day(self, events, week_start, num_days=NUM_DAYS):
        """Return a dict {day: {'time(8, 30)': [events], ...}."""
        monday = week_start.date() if isinstance(week_start, datetime) else week_start
        week_days = [
            monday + timedelta(days=i)
            for i in range(num_days)
        ]
        years = list({week_days[0].year, week_days[-1].year})
        holidays = dict([item for sublist in [CRHolidays().holidays(year) for year in years] for item in sublist])
        week_events = {
            dt: {'events': [], 'cols': 0, 'holiday': HOLIDAYS_TRANS.get(holidays.get(dt))} for dt in week_days
        }
        # Start by putting each event in its week day
        for event in events:
            try:
                week_events[timezone.localtime(event.debut).date()]['events'].append(event)
            except KeyError:
                pass
        # Checking events overlap
        for day in week_events:
            num_cols = day_overlap(week_events[day]['events'])
            week_events[day]['cols'] = num_cols
            week_events[day]['col_width'] = round(100.0 / num_cols, 2)
        # Putting events as a dict with the hour quarter as key
        for day in week_events:
            events = week_events[day]['events']
            week_events[day]['events'] = {}
            for event in events:
                hour = round_quarter(timezone.localtime(event.debut))
                week_events[day]['events'].setdefault(hour, []).append(event)
        return week_events

    def get_day_divs(self, start_hour, end_hour):
        return [time(h, m) for h in range(start_hour, end_hour + 1) for m in range(0, 60, 15)]


class ChauffeurAgendaView(AgendaMixin, TemplateView):
    template_name = "agenda-week.html"

    def get_chauffeur(self):
        return get_object_or_404(Benevole, pk=self.kwargs['pk'])

    def get_start_end_dates(self):
        if self.kwargs['year'] == 0:  # current week
            year, week_num, _ = date.today().isocalendar()
        else:
            year, week_num = self.kwargs['year'], self.kwargs['week']
        week_start = timezone.make_aware(
            datetime.strptime(f'{year}-{week_num}-1', "%G-%V-%w")
        )
        return week_start, week_start + timedelta(days=self.NUM_DAYS)

    def filter_transports(self, queryset):
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chauffeur = self.get_chauffeur()
        ag_start, ag_end = self.get_start_end_dates()
        dispos = list(Dispo.get_for_person(chauffeur, ag_start, ag_end))
        transports = list(self.filter_transports(
            Transport.get_for_person(chauffeur, ag_start, ag_end).exclude(statut=Transport.StatutChoices.ANNULE)
        ))
        prev_url, next_url = self.prev_next_urls(chauffeur, ag_start, ag_end)
        start_hour = min(self.START_HOUR, *(
            [self.START_HOUR] + [timezone.localtime(d.debut).hour for d in dispos] +
            [t.heure_depart.hour for t in transports]
        ))
        end_hour = max(self.END_HOUR, *(
            [self.END_HOUR] + [timezone.localtime(d.fin).hour for d in dispos] +
            [t.heure_arrivee.hour for t in transports]
        ))

        context.update({
            'benevole': chauffeur,
            'activite': Activites.TRANSPORT,
            'base': 'transport/base.html',
            'show_tabs': True,
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agendas': [{
                'title': f'Agenda pour {chauffeur}',
                'chauffeur': chauffeur,
                'week_events': self.events_by_day(dispos + transports, ag_start, num_days=self.NUM_DAYS),
                'prev_events': prev_url,
                'next_events': next_url,
                'add_dispo_url': reverse('benevole-dispo-add', args=[chauffeur.pk]),
                'add_absence_url': reverse('benevole-absence-add', args=[chauffeur.pk]),
                'classes': 'week-grid' if (ag_end - ag_start).days >= 5 else 'day-grid',
            }],
        })
        return context

    def prev_next_urls(self, benev, start, end):
        # Prev/next links
        prev_iso = (start - timedelta(days=self.NUM_DAYS)).isocalendar()
        next_iso = end.isocalendar()
        return (
            reverse('benevole-agenda', args=[benev.pk, prev_iso.year, prev_iso.week]),
            reverse('benevole-agenda', args=[benev.pk, next_iso.year, next_iso.week]),
        )


class ChauffeurDispoEditView(CreateUpdateView):
    model = Dispo
    categ = 'dispo'
    form_class = forms.DispoEditForm
    template_name = 'transport/chauffeur-dispo-edit.html'
    json_response = True

    def get_chauffeur(self):
        if self.object and self.object.chauffeur_id:
            return self.object.chauffeur
        elif 'chauffeur_pk' in self.kwargs:
            return Benevole.objects.get(pk=self.kwargs['chauffeur_pk'])
        else:
            return self.request.user.benevole

    def get_initial(self):
        initial = super().get_initial()
        if not self.is_create:
            initial['date'] = timezone.localtime(self.object.debut).date()
            initial['heure_de'] = timezone.localtime(self.object.debut).time()
            initial['heure_a'] = timezone.localtime(self.object.fin).time()
            if self.object.regle:
                if self.object.regle.nom == 'Chaque jour, du lundi au vendredi':
                    initial['repetition'] = 'DAILY-open'
                else:
                    initial['repetition'] = self.object.regle.frequence
                if self.object.fin_recurrence:
                    initial['repetition_fin'] = self.object.fin_recurrence.date()
        elif 'date' in self.request.GET:
            try:
                initial['date'] = datetime.strptime(self.request.GET['date'], '%Y-%m-%d').date()
            except ValueError:
                pass
        return initial

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'chauffeur': self.get_chauffeur(),
            'categ': (self.object.categorie if self.object else None) or self.categ,
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_benev_app = '/app/' in self.request.path
        if self.object and self.object.debut.date() > date.today():
            context['delete_url'] = (
                reverse('benevole-self-dispo-delete', args=[self.object.pk]) if is_benev_app
                else reverse('benevole-dispo-delete', args=[self.object.pk])
            )
        if 'occ' in self.request.GET:
            occ_date = datetime.fromisoformat(self.request.GET['occ'])
            context.update({
                'occ_date': occ_date,
                'occ_obj': self.object.exceptions.filter(orig_start=occ_date).first(),
                'toggle_url': (
                    reverse('benevole-self-dispo-toggle', args=[self.object.pk]) if is_benev_app
                    else reverse('benevole-dispo-toggle', args=[self.object.pk])
                )
            })
        return context

    def form_valid(self, form):
        if self.is_create:
            form.instance.categorie = self.categ
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDispoToggleView(View):
    def post(self, request, *args, **kwargs):
        dispo = get_object_or_404(Dispo, pk=self.kwargs['pk'])
        if not dispo.can_edit(request.user):
            raise PermissionDenied()
        occ_date = datetime.fromisoformat(request.POST['occ_date']).date()
        with transaction.atomic():
            try:
                occ = dispo.exceptions.select_for_update().get(orig_start__date=occ_date)
                occ.delete()
            except Occurrence.DoesNotExist:
                dispo.cancel_for(occ_date)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDispoDeleteView(DeleteView):
    model = Dispo

    def form_valid(self, form):
        if not self.object.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer cette disponibilité.")
        # Check transports
        transports_lies = []
        transports_futurs = list(self.object.chauffeur.transports.filter(date__gte=date.today()))
        if transports_futurs:
            date_min = min(t.heure_rdv for t in transports_futurs)
            date_max = max(t.date for t in transports_futurs)
            occs = self.object.get_occurrences(
                date_min, datetime.combine(date_max, time(23, 59), tzinfo=date_min.tzinfo)
            )
            for transp in transports_futurs:
                if any((transp.debut > occ.debut and transp.fin < occ.fin) for occ in occs):
                    transports_lies.append(transp)
        if not transports_lies:
            self.object.delete()
        else:
            msg = "Impossible de supprimer cette disponibilité car les transports suivants sont déjà attribués:<br>"
            msg += "<br>".join(str(t) for t in transports_lies)
            messages.error(self.request, SafeString(msg))
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ChauffeurDayView(AgendaMixin, FromModeleMixin, TemplateView):
    """
    Affichage dispos et transports d'un chauffeur pour le jour d'un transport à
    placer (pour l'attribution)
    """
    template_name = "agenda_grid.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        chauffeur = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        if 'moddate' in self.kwargs:
            transport = self.get_object()
        else:
            transport = get_object_or_404(Transport, pk=self.kwargs['transport_pk'])
        transport.ev_category = 'nouveau-transport'
        jour = transport.heure_rdv.replace(hour=0, minute=0)
        dispos = Dispo.get_for_person(chauffeur, jour, jour + timedelta(days=1))
        transports = Transport.get_for_person(
            chauffeur, jour, jour + timedelta(days=1)
        ).exclude(statut=Transport.StatutChoices.ANNULE)
        start_hour = min(*(
            [self.START_HOUR, transport.heure_depart.hour] +
            [timezone.localtime(d.debut).hour for d in dispos] +
            [t.heure_depart.hour for t in transports]
        ))
        end_hour = max(*(
            [self.END_HOUR, transport.heure_arrivee.hour] +
            [timezone.localtime(d.fin).hour for d in dispos if d.fin] +
            [t.heure_arrivee.hour for t in transports]
        ))
        context.update({
            'start_hour': start_hour,
            'end_hour': end_hour,
            'day_divs': self.get_day_divs(start_hour, end_hour),
            'agenda': {
                'week_events': self.events_by_day(list(dispos) + list(transports) + [transport], jour, num_days=1),
            }
        })
        return context


def round_quarter(time_):
    return time(time_.hour, math.floor(time_.minute / 15) * 15)


def day_overlap(event_list):
    """
    Attach a column attribute to each event in event_list, so as no events overlap
    """
    event_list = [ev for ev in event_list if not isinstance(ev, Dispo)]
    cols = [[]]

    def overlaps(ev, other):
        return ev.debut < other.fin and ev.fin > other.debut

    def find_column_for_event(ev):
        for idx, evts in enumerate(cols):
            overlapping = [other for other in evts if overlaps(ev, other)]
            if overlapping:
                for e in overlapping:
                    e.overlaps = True
                ev.overlaps = True
                continue
            else:
                # Add event to this col
                cols[idx].append(ev)
                ev.column = idx
                break
        else:
            # Add event to a new col
            cols.append([ev])
            ev.column = len(cols) - 1

    # Set column for each rzv event related to its dispo
    for ev in event_list:
        find_column_for_event(ev)

    for ev in event_list:
        ev.leftoffset = ev.column * (100.0 / len(cols))
        if ev.leftoffset != 0:
            ev.leftoffset = f'{ev.leftoffset}%'
    return len(cols)
