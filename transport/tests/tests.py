from datetime import date, datetime, time, timedelta, timezone
from decimal import Decimal
from io import BytesIO
from unittest.mock import patch
from urllib.parse import quote

import httpx
from dateutil import relativedelta
from freezegun import freeze_time
from openpyxl import load_workbook
from pypdf import PdfReader

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import F
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils.timezone import get_current_timezone, localtime, now

from benevole.models import Activites, NoteFrais, TypeFrais
from client.models import Alerte, Client, ClientType, Referent
from common.forms import HMDurationField
from common.distance import ORSUnavailable, distance_real
from common.models import DistanceFigee, Utilisateur
from common.export import openxml_contenttype
from common.test_utils import TempMediaRootMixin, mocked_httpx
from transport.forms import (
    AdresseEditForm, CancelForm, DispoEditForm,  RapportForm, SaisieTransportForm
)
from transport.models import (
    Adresse, AdresseClient, Benevole, Dispo, Frais, Preference, Regle, Trajet,
    TrajetCommun, Transport, TransportModel
)

GEOADMIN_API_SEARCH = "https://api3.geo.admin.ch/rest/services/api/SearchServer"
MOCKED_DISTANCE = 12701.5


class FakeORSClient:
    def __init__(self, key, **kwargs):
        pass

    def directions(self, *args, **kwargs):
        response = {'routes': [{
            'bbox': [6.820271, 46.996063, 6.944324, 47.095443],
            'summary': {'distance': MOCKED_DISTANCE, 'duration': 980.1}
        }]}
        if kwargs.get('geometry') is True:
            response['routes'][0]['geometry'] = (
                '}te~Gcseh@u@yAQASNEFCHE`@@J@J^~@CDi@aAGCE@BBv@|Aj@hAqAa@_@Q]YgAcA[l@WW_@YiB'
                'aAwCoCIKGNMXCHM\\ITGECCC?MQSYEGqAmDCEEMaBoEQ_@S]KOS[CEgAeBQ_@Wo@g@wACKqA}DG'
                'O_@y@_@w@{@kBCEWw@Sm@]sAk@uB_@uAIWM]IQMWUc@CE[c@_@c@w@}@[_@e@i@_@c@[]OSAAY_'
                '@KQU[Ye@g@gAMWGQa@iA_@mAg@iBOm@YuAQw@E[BE@G?ICGIKEAE?CBIO}@mB_@y@u@yAeFcK_@'
                'k@Y]u@o@[OgA_@cBa@]Q]Qg@c@W[q@cAYm@c@yA[cBmCeSgAyHq@cDaAyDe@_B[s@aBuCcFgIm@e'
                'A_AiByVok@qA{Cg@uAe@}A_@uBOaBKgC?gE@gAJ_AJKHODU?UCUISKKYSWWGSS{@k@gBa@}@{@g'
                'Be@iAuAgFgAyEo@gCSs@W_AFGDUEUCGGEM?GBGQg@oAc@_Ae@_AyByEuEsKUq@UcAMiAKmAA]C_@'
                'IaBEs@Iq@COCQe@mBAIIQa@}@[k@m@}@QWQWg@s@o@aAg@o@_@m@m@kADGBUAKCICGMCE?GBGLMU'
                '{B}Dq@mAKQmAwBUe@S]Uc@kIuPMUMYyAsCYk@y@}Ai@kAQ]q@uA[k@OYBGBQCQEGECK@EBCFCP@H'
                'UPu@f@e@d@QRILKNIJkEpFKLSVGHSXMYgHuNS_@GOcDsGKSU\\GH{@tA_@n@OTQVm@~@u@lAe@v@'
                'MRMUg@{@gAkBi@_AIMCE?AGIcDwFIOcBuCOWMUuEcIs@oA}AmCk@aAgAmBe@y@Ua@U_@OTIPeAdBc'
                'A~A}@{Am@cAuBoDYe@s@uAWOi@YMC[B[HULu@\\GBWLm@VMF}@^WLQJ]NQBQB]GSCMC}AWCASCiAK'
                'YCOAuABWHUPMUQ[qEuHGMIO'
            )
        return response


class DataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.patcher = patch('common.distance.Client', new=FakeORSClient)
        cls.patcher.start()
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            Permission.objects.get(content_type__model='client', codename='change_client'),
            Permission.objects.get(codename='view_transport'),
            Permission.objects.get(codename='change_transport'),
            Permission.objects.get(codename='view_benevole'),
            Permission.objects.get(codename='gestion_finances'),
        )
        cls.trclient = Client.objects.create(
            type_client=['transport'], nom='Donzé', prenom='Léa', no_debiteur=999,
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.820, 47.094]
        )
        cls.hne_ne = Adresse.objects.create(
            nom='Hôpital neuchâtelois', rue='Rue de la Maladière 45', npa='2000', localite='Neuchâtel',
            empl_geo=[6.943, 46.996]
        )
        cls.demain_1130 = datetime.combine(
            date.today() + timedelta(days=1), time(11, 30), tzinfo=get_current_timezone()
        )
        TypeFrais.objects.bulk_create([
            TypeFrais(no='NF-B06', services=['alarme', 'transport']),
            TypeFrais(no='NF-B07', services=['alarme', 'transport']),
            TypeFrais(no='NF-B10', services=['transport']),
            TypeFrais(no='NF-B11', services=['transport']),
            TypeFrais(no='NF-B12', services=['transport']),
            TypeFrais(no='NF-B13', services=['transport']),
        ])


    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        cls.patcher.stop()

    @classmethod
    def create_transport(cls, adr_interm_aller=None, adr_interm_retour=None, **kwargs):
        origine = kwargs.pop('origine', None)
        destination = kwargs.pop('destination', None)
        heure_depart = kwargs.pop('heure_depart', kwargs['heure_rdv'] - timedelta(seconds=1525))
        kwargs['date'] = kwargs['heure_rdv'].date()
        transport = Transport.objects.create(**kwargs)
        destination_domicile = not transport.retour and origine is not None
        if adr_interm_aller:
            tr = Trajet.objects.create(
                transport=transport, typ='medic',
                heure_depart=heure_depart,
                origine_domicile=origine is None, origine_adr=origine,
                destination_domicile=False, destination_adr=adr_interm_aller,
                destination_princ=False,
            )
            tr.calc_trajet()
            origine = adr_interm_aller
            heure_depart += timedelta(seconds=600)
        aller = Trajet.objects.create(
            transport=transport, typ='medic',
            heure_depart=heure_depart,
            origine_domicile=origine is None,
            origine_adr=origine,
            destination_domicile=destination_domicile,
            destination_adr=destination,
            destination_princ=True,
        )
        aller.calc_trajet()
        if transport.retour:
            heure_depart = transport.heure_rdv + transport.duree_rdv
            origine_adr = aller.destination
            if adr_interm_retour:
                tr = Trajet.objects.create(
                    transport=transport, typ='medic',
                    heure_depart=heure_depart,
                    origine_domicile=False, origine_adr=origine_adr,
                    destination_domicile=False, destination_adr=adr_interm_retour,
                    destination_princ=False,
                )
                tr.calc_trajet()
                heure_depart += timedelta(hours=1)
                origine_adr = adr_interm_retour
            tr = Trajet.objects.create(
                transport=transport, typ='medic',
                heure_depart=heure_depart,
                origine_domicile=False, origine_adr=origine_adr,
                destination_domicile=True,
                destination_adr=None,
                destination_princ=False,
            )
            tr.calc_trajet()
        return transport

    @classmethod
    def create_modele(cls, **kwargs):
        kwargs.setdefault('typ', 'medic')
        dest = kwargs.pop('destination', None)
        kwargs.update({
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': dest or '-1',
            'trajets-0-destination_adr_select': '',
            'trajets-0-destination_princ': 'True',
        })
        if kwargs['retour'] == 'True':
            kwargs.update({
                'trajets-TOTAL_FORMS': '2',
                'trajets-1-id': '',
                'trajets-1-ORDER': '1',
                'trajets-1-destination_princ': 'False',
            })
        form = SaisieTransportForm(data={**kwargs, 'client_select': 'any'}, instance=None)
        if not form.is_valid():
            cls.fail(cls, [form.errors] + form.formset.errors)
        transp = form.save()
        return transp.modele


class TransportTests(TempMediaRootMixin, DataMixin, TestCase):

    def test_est_passe(self):
        transport = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True
        )
        self.assertFalse(transport.est_passe)
        hier = date.today() - timedelta(days=1)
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(hier, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True
        )
        self.assertTrue(transport.est_passe)

    def test_saisie_transport(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-saisie'))
        self.assertContains(response, "Saisir un nouveau transport")
        in_5_days = date.today() + timedelta(days=5)
        form_data = {
            'client_select': 'Don',
            'client': self.trclient.pk,
            'depart_autre': '',
            'typ': 'medic',
            'retour': 'True',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-ORDER': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
            'trajets-1-id': '',
            'trajets-1-ORDER': '',
            'trajets-1-destination_adr': '',
            'trajets-1-destination_adr_select': '',
            'trajets-1-destination_princ': 'False',
        }
        response = self.client.post(reverse('transport-saisie'), data=form_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(
            transp.heure_rdv.astimezone(get_current_timezone()),
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 10, 30, tzinfo=get_current_timezone())
        )
        self.assertEqual(localtime(transp.trajets_tries[0].heure_depart).time(), time(10, 15))
        self.assertEqual(localtime(transp.trajets_tries[1].heure_depart).time(), time(11, 15))
        # Distance et durée sont calculées
        self.assertEqual(transp.duree_calc, timedelta(seconds=980) * 2 + timedelta(minutes=45))
        self.assertEqual(transp.dist_calc, 12702 * 2)
        self.assertEqual(
            transp.heure_depart_retour,
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 11, 15, tzinfo=get_current_timezone())
        )
        self.assertEqual(
            transp.heure_arrivee,
            datetime(in_5_days.year, in_5_days.month, in_5_days.day, 11, 31, tzinfo=get_current_timezone())
        )
        self.assertTrue(transp.trajets_tries[0].destination_princ)
        self.assertFalse(transp.trajets_tries[1].destination_princ)

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    def test_saisie_transport_ors_unavailable(self, mocked):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:05',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
        })
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))

    def test_saisie_transport_aller(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:05',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
        })
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertFalse(transp.retour)
        self.assertEqual(len(transp.trajets.all()), 1)
        self.assertEqual(transp.heure_arrivee.time(), time(10, 30))

    def test_saisie_transport_aller_vers_domicile(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': 'any',
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
            'trajets-0-destination_princ': 'True',
        })
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(transp.heure_arrivee.time(), time(10, 36))
        self.assertEqual(len(transp.trajets.all()), 1)
        trajet = transp.trajets.first()
        self.assertEqual(trajet.origine_domicile, False)
        self.assertEqual(trajet.origine_adr, self.hne_ne)
        self.assertEqual(trajet.destination_domicile, True)
        self.assertIsNone(trajet.destination_adr)
        self.assertTrue(transp.trajets_tries[0].destination_princ)

    def test_saisie_transport_aller_vers_domicile_et_retour(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': 'any',
            'typ': 'medic',
            'retour': 'True',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:30',
            'duree_rdv': '02:00',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
            'trajets-0-destination_princ': 'True',
            'trajets-1-id': '',
            'trajets-1-destination_adr': self.hne_ne.pk,
            'trajets-1-destination_adr_select': 'any',
            'trajets-1-destination_princ': 'False',
        })
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        traj1, traj2 = transp.trajets_tries
        self.assertEqual(traj1.origine_domicile, False)
        self.assertEqual(traj1.destination_domicile, True)
        self.assertEqual(traj2.origine_domicile, True)
        self.assertEqual(traj2.destination_domicile, False)

    def test_saisie_transport_domicile_vers_domicile(self):
        """Transport avec trajet domicile-domicile n'est pas autorisé."""
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'origine_adr': '',
            'origine_adr_select': '',
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': -1,
            'trajets-0-destination_adr_select': 'Domicile',
            'trajets-0-destination_princ': 'True',
        })
        self.assertContains(response, "L’origine et la destination ne peuvent pas être les deux le domicile")

    def test_saisie_transport_trajet_intermediaire(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651]
        )
        in_5_days = date.today() + timedelta(days=5)
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'depart_autre': '',
            'typ': 'medic',
            'destination': self.hne_ne.pk,
            'destination_select': 'HNE',
            'retour': 'True',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-0-destination_princ': 'True',
            'trajets-1-id': '',
            'trajets-1-ORDER': '2',
            'trajets-1-destination_princ': 'False',
            'trajets-1-destination_adr': '',
            'trajets-1-destination_adr_select': '',
            # Trajet intermédiaire (au retour)
            'trajets-2-id': '',
            'trajets-2-ORDER': '1',
            'trajets-2-destination_adr': pharma.pk,
            'trajets-2-destination_adr_select': 'Pharma',
            'trajets-2-destination_princ': 'False',
        })
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(len(transp.trajets_tries), 3)
        self.assertIs(transp.trajets_tries[0].destination_princ, True)
        self.assertIs(transp.trajets_tries[1].destination_princ, False)
        # Distance et durée sont calculées
        self.assertEqual(transp.duree_calc, timedelta(seconds=980) * 3 + timedelta(minutes=45))
        self.assertEqual(transp.dist_calc, 12702 * 3)

    def test_saisie_transport_trajet_intermediaire_aller_simple(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651]
        )
        in_5_days = date.today() + timedelta(days=5)
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'depart_autre': '',
            'typ': 'medic',
            'destination': self.hne_ne.pk,
            'destination_select': 'HNE',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '11:40',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': '',
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'RHNe',
            'trajets-0-destination_princ': 'True',
            'trajets-2-id': '',
            'trajets-2-ORDER': '0',
            'trajets-2-destination_adr': pharma.pk,
            'trajets-2-destination_adr_select': 'Pharma',
            'trajets-2-destination_princ': 'False',
            'trajets-1-id': '',
            'trajets-1-ORDER': '2',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('transport-client-detail', args=[self.trclient.pk]))
        transp = self.trclient.transports.first()
        self.assertEqual(len(transp.trajets_tries), 2)
        self.assertIs(transp.trajets_tries[0].destination_princ, False)
        self.assertIs(transp.trajets_tries[1].destination_princ, True)

    def test_saisie_depart_egal_destination(self):
        form = SaisieTransportForm(data={
            'client': self.trclient.pk,
            'client_select': 'any',
            'origine_adr': self.hne_ne.pk,
            'origine_adr_select': '',
            'date': now().strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '00:45',
            'typ': 'medic',
            'retour': 'on',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': '',
        }, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['__all__'],
            ["L’origine et la destination d’un trajet doivent être différents"] * 2
        )

    def test_saisie_transport_client_initial(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-saisie') + f'?client={self.trclient.pk}')
        self.assertContains(
            response,
            f'<input type="hidden" name="client" value="{self.trclient.pk}" class="form-control" id="id_client">',
            html=True
        )
        self.assertContains(response, f'value="{str(self.trclient)}"')

    def test_saisie_transport_sans_destination(self):
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': now().strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '',
            'duree_rdv': '00:45',
            'typ': 'medic',
            'retour': 'on',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-destination_adr': '',
            'trajets-0-destination_adr_select': '',
        }
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'heure_rdv': ['Ce champ est obligatoire.']})
        form_data['heure_rdv'] = '12:00'
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.formset.errors, [{'destination_adr': ['Ce champ est obligatoire.']}, {}])

    def test_saisie_transport_sans_heure_depart(self):
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': now().strftime('%Y-%m-%d'),
            'heure_rdv': '12:00',
            'duree_rdv': '00:45',
            'typ': 'medic',
            'retour': 'on',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': '',
        }
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'heure_depart': ['Ce champ est obligatoire.']})

    def test_saisie_transport_sans_duree_rdv(self):
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': now().strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '12:00',
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'on',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': '',
        }
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'duree_rdv': ['La durée de rendez-vous est obligatoire.']})
        form_data['retour'] = ''
        form = SaisieTransportForm(data=form_data, instance=None)
        self.assertTrue(form.is_valid())

    def test_client_derniers_transports(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-client-derniers') + f'?client={self.trclient.pk}')
        self.assertContains(response, f"{self.demain_1130.strftime('%d.%m.%Y')} à 11:10 (Rdv. 11:30)")

    @patch('httpx.get', side_effect=mocked_httpx)
    def test_adresse_ajout(self, mock):
        self.user.user_permissions.add(
            Permission.objects.get(codename='change_adresse'),
        )
        self.client.force_login(self.user)
        form_data = {
            'nom': 'Dentiste Toutdoux',
            'rue': 'Seyon 10',
            'npalocalite': '2000 Neuchâtel',
            'tel': '+41321234567'
        }
        response = self.client.post(reverse('transport-address-add'), data=form_data)
        self.assertEqual(response.json()['result'], "OK")
        toutdoux = Adresse.objects.get(nom="Dentiste Toutdoux")
        self.assertEqual(toutdoux.tel, "032 123 45 67")
        # Contrôles unicité sur le nom (ajout + édition)
        response = self.client.post(reverse('transport-address-add'), data=form_data)
        self.assertContains(response, "Il existe déjà une adresse avec ce nom")
        adresse = Adresse.objects.create(
            nom="Dentiste", rue='Seyon 10', npa='2000', localite='Neuchâtel', empl_geo=[6.9355, 46.99651]
        )
        response = self.client.post(reverse('transport-address-edit', args=[adresse.pk]), data=form_data)
        self.assertContains(response, "Il existe déjà une adresse avec ce nom")
        # Si ancienne archivée, nouvelle OK
        toutdoux.date_archive = date.today()
        toutdoux.save(update_fields=['date_archive'])
        response = self.client.post(reverse('transport-address-add'), data=form_data)
        self.assertEqual(response.json()['result'], "OK")

    def test_adresse_liste(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel', tel='032 222 11 00',
            empl_geo=[6.9355, 46.99651]
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-addresses'))
        self.assertContains(response, '<th>Nom</th><th>Adresse</th>')
        self.assertQuerySetEqual(response.context['object_list'], [self.hne_ne, pharma])
        # Filtrage
        response = self.client.get(reverse('transport-addresses') + '?nom=pharma')
        self.assertQuerySetEqual(response.context['object_list'], [pharma])
        response = self.client.get(reverse('transport-addresses') + '?tel=211')
        self.assertQuerySetEqual(response.context['object_list'], [pharma])
        response = self.client.get(reverse('transport-addresses') + '?npalocalite=neuchat')
        self.assertQuerySetEqual(response.context['object_list'], [self.hne_ne, pharma])

    def test_adresse_archive(self):
        adr = Adresse.objects.create(
            nom='Hôpital neuchâtelois à archiver', rue='Rue de la Maladière 45', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9428534507751465, 46.99623107910156]
        )
        self.user.user_permissions.add(
            Permission.objects.get(codename='delete_adresse'),
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-address-archive', args=[adr.pk]), data={})
        self.assertEqual(response.json()['result'], "OK")
        adr.refresh_from_db()
        self.assertEqual(adr.date_archive, date.today())

    def test_calcul_depart(self):
        self.client.force_login(self.user)
        form_data = {
            'client': self.trclient.pk,
            'origine': '',
            'destination': [self.hne_ne.pk],
            'heure_rdv': '10:00',
        }
        calcul_url = reverse('transport-calculer-depart')
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:16', 'depart': '09:35'})
        # Tester l'arrondi aux alentours de l'heure
        form_data['heure_rdv'] = '10:25'
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:16', 'depart': '10:00'})
        # Retour vers domicile
        form_data['origine'] = self.hne_ne.pk
        form_data['destination'] = '-1'
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:16', 'depart': '10:00'})
        # Ne pas crasher si origine manque
        form_data['origine'] = ''
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(
            response.json(),
            {'result': 'error', 'reason': 'Origine et destination doivent être différentes.'}
        )
        # Plusieurs destinations
        adr2 = Adresse.objects.create(
            nom='Permanence Volta', rue='Rue Numa-Droz 187', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.8145, 47.095]
        )
        form_data['destination'] = [adr2.pk, self.hne_ne.pk]
        response = self.client.post(calcul_url, data=form_data)
        self.assertEqual(response.json(), {'result': 'OK', 'duree_trajet': '00:32', 'depart': '09:40'})

    def test_edit_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(
            response,
            f'<input type="date" name="date" value="{self.demain_1130.date()}" '
            'class="form-control" required="" id="id_date">',
            html=True
        )
        self.assertContains(response, 'value="11:10"')  # Heure de départ
        # Now POSTing (changed: typ, remarques, heure_depart, heure_rdv, duree_rdv)
        form_data = {
            'typ': 'emplettes',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '15:40',
            'heure_rdv': '16:00',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '1',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '2',
        }
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(transport.journaux.count(), 1)
        trajet1, trajet2 = transport.trajets_tries
        self.assertTrue(trajet1.destination_princ)
        self.assertEqual(localtime(trajet1.heure_depart).time(), time(15, 40))
        self.assertEqual(trajet1.typ, 'emplettes')
        self.assertEqual(localtime(trajet2.heure_depart).time(), time(16, 45))
        self.assertEqual(trajet2.typ, 'emplettes')
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(response, '<option value="emplettes" selected>Emplettes à deux</option>', html=True)
        # Change only typ:
        form_data.update({
            'trajets-0-id': trajet1.pk,
            'trajets-1-id': trajet2.pk,
            'typ': 'medic',
        })
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        transport.refresh_from_db()
        trajet1, trajet2 = list(transport.trajets.all())
        self.assertEqual(trajet1.typ, 'medic')
        self.assertEqual(trajet2.typ, 'medic')

    def test_edit_transport_aller(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        # L'adresse retour est masquée, mais elle doit contenir l'adresse de départ pour le cas
        # où le transport est transformé en aller-retour.
        self.assertContains(
            response,
            '<p id="adresse_retour" class="fst-italic">Rue des Crêtets 92, 2300 La Chaux-de-Fonds</p>',
            html=True
        )

    def test_edit_transport_intermediaire_aller(self):
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            adr_interm_aller=pharma,
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.assertEqual(transport.destination, self.hne_ne)
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertContains(response, 'Pharmacie')
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data={
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:40',  # modifié
            'heure_rdv': '12:00',  # modifié
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'origine_adr': '',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '3',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': pharma.pk,
            'trajets-0-destination_adr_select': 'Pharma',
            'trajets-0-destination_princ': 'False',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-ORDER': '2',
            'trajets-1-destination_adr': self.hne_ne.pk,
            'trajets-1-destination_adr_select': 'HNE',
            'trajets-1-destination_princ': 'True',
            'trajets-2-id': '',
            'trajets-2-ORDER': '',
            'trajets-2-destination_princ': 'False',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(len(transport.trajets_tries), 2)
        self.assertEqual(transport.destination, self.hne_ne)

    def test_edit_transport_changed_data(self):
        """
        Enregistrer un transport sans modifier les champs, changed_data doit être vide.
        """
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        premier_trajet = transport.trajets_tries[0]
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
        }
        form = SaisieTransportForm(data=form_data, instance=transport, initial={'typ': premier_trajet.typ})
        self.assertEqual(form.changed_data, [])

    def test_edit_transport_date(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        premier_trajet = transport.trajets_tries[0]
        dans4jours = (self.demain_1130 + timedelta(days=4)).date()
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': dans4jours.strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': self.hne_ne.pk,
        }
        form = SaisieTransportForm(data=form_data, instance=transport, initial={'typ': premier_trajet.typ})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.changed_data, ['date'])
        form.save()
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(transport.date, dans4jours)
        self.assertEqual([traj.heure_depart.date() for traj in transport.trajets_tries], [dans4jours])

    def test_edit_transport_dest_avec_chauffeur(self):
        """
        Les champs chauff_aller_dist/chauff_retour_dist doivent être recalculés si les
        trajets changent et que le chauffeur est déjà attribué.
        """
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE,
            chauffeur=chauffeur,
        )
        transport.chauff_aller_dist = transport.chauff_retour_dist = 100
        transport.save()
        autre_dest = Adresse.objects.create(
            nom='Pharmacie', rue='Place de la Gare 1', npa='2000', localite='Neuchâtel',
            empl_geo=[6.9355, 46.99651]
        )
        premier_trajet = transport.trajets_tries[0]
        form_data = {
            'client': self.trclient.pk,
            'client_select': 'any',
            'date': self.demain_1130.strftime('%Y-%m-%d'),
            'heure_depart': transport.heure_depart.strftime('%H:%M'),
            'heure_rdv': transport.heure_rdv.strftime('%H:%M'),
            'duree_rdv': '',
            'typ': 'medic',
            'retour': 'False',
            'remarques': '',
            'trajets-TOTAL_FORMS': '1',
            'trajets-INITIAL_FORMS': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-id': premier_trajet.pk,
            'trajets-0-ORDER': '1',
            'trajets-0-destination_adr': autre_dest.pk,
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport.refresh_from_db()
        self.assertEqual(transport.chauff_aller_dist, int(MOCKED_DISTANCE))
        self.assertEqual(transport.chauff_retour_dist, int(MOCKED_DISTANCE))

    def test_edit_transport_rapporte_ar_vers_aller_simple(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            duree_eff=timedelta(hours=1), km=Decimal(10.5), temps_attente=timedelta(hours=3),
            statut=Transport.StatutChoices.RAPPORTE,
        )
        form_data = {
            'typ': 'medic',
            'remarques': 'Aller-retour vers aller simple',
            'retour': 'False',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '01:10',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '1',
            'trajets-1-id': transport.trajets_tries[1].pk,
            'trajets-1-destination_adr': '-1',
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '2',
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(len(transport.trajets_tries), 1)
        self.assertFalse(transport.retour)
        self.assertIsNone(transport.temps_attente)

    def test_edit_transport_simple_vers_allerretour(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        form_data = {
            'typ': 'medic',
            'remarques': 'Aller simple vers aller-retour',
            'retour': 'True',
            'date': self.demain_1130.date().strftime('%Y-%m-%d'),
            'heure_depart': '11:10',
            'heure_rdv': '11:30',
            'duree_rdv': '',  # manquant
            'trajets-INITIAL_FORMS': '1',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': transport.trajets_tries[0].pk,
            'trajets-0-transport': transport.pk,
            'trajets-0-destination_adr': transport.destination.pk,
            'trajets-0-destination_princ': 'True',
            'trajets-0-ORDER': '0',
            'trajets-1-id': '',
            'trajets-1-destination_adr': '-1',
            'trajets-1-transport': transport.pk,
            'trajets-1-ORDER': '1',
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-edit', args=[transport.pk]), data=form_data)
        self.assertEqual(
            response.context['form'].errors,
            {'duree_rdv': ['La durée de rendez-vous est obligatoire.']}
        )

    def test_edit_transport_retour_dom(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130, heure_depart=self.demain_1130 - timedelta(minutes=20),
            duree_rdv=timedelta(minutes=70),
            origine=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-edit', args=[transport.pk]))
        self.assertEqual(
            response.context['form'].formset.forms[0].instance,
            transport.trajets_tries[0]
        )
        self.assertContains(
            response,
            f'<input type="hidden" name="origine_adr" value="{self.hne_ne.pk}" '
            'class="form-control" id="id_origine_adr">',
            html=True
        )
        self.assertContains(
            response,
            '<p class="fst-italic" id="adresse_depart">Hôpital neuchâtelois, '
            'Rue de la Maladière 45, 2000 Neuchâtel</p>',
            html=True
        )

    def test_attribution_transport(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        allerretour = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        allersimple = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-attrib', args=[allerretour.pk]))
        self.assertEqual(len(response.context['chauffeurs']), 1)
        self.assertContains(response, 'Pour <b>Donzé Léa</b>')
        response = self.client.get(reverse('transport-attrib', args=[allersimple.pk]))
        self.assertContains(response, 'Pour <b>Donzé Léa</b>')

        response = self.client.post(reverse('transport-attrib', args=[allerretour.pk]), data={
            'chauffeur': chauffeur.pk,
        })
        self.assertRedirects(response, reverse('gestion'))
        allerretour.refresh_from_db()
        self.assertEqual(allerretour.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(allerretour.chauff_aller_dist, 12701)
        self.assertEqual(allerretour.chauff_retour_dist, 12701)

    def test_desattribution_transport(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-attrib-remove', args=[transport.pk]))
        self.assertRedirects(response, reverse('gestion'))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(transport.chauffeur)

    def test_autoattribution_transport(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        modele = self.create_modele(
            client=self.trclient,
            date=self.demain_1130 - timedelta(days=4),
            heure_depart='15:15',
            heure_rdv='15:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='False',
            repetition='DAILY',
        )

        self.client.force_login(self.user)
        auto_url = reverse(
            'transport-autoattrib',
            args=[self.demain_1130.year, self.demain_1130.month, self.demain_1130.day]
        )
        response = self.client.post(auto_url, data={})
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.ATTRIBUE]))
        transport.refresh_from_db()
        # Chauffeur sans dispo, pas d'attribution auto
        self.assertIsNone(transport.chauffeur)

        Dispo.objects.create(
            chauffeur=chauffeur,
            debut=self.demain_1130 - timedelta(days=7, hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=6),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        response = self.client.post(auto_url, data={}, follow=True)
        self.assertContains(response, "2 transports ont été attribués pour le ")
        self.assertEqual(chauffeur.transports.count(), 2)
        transport.refresh_from_db()
        self.assertEqual(transport.chauffeur, chauffeur)
        self.assertEqual(transport.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(len(transport.trajets_tries), 2)
        transport2 = modele.transports.get(date=self.demain_1130.date())
        self.assertEqual(transport2.chauffeur, chauffeur)
        self.assertEqual(transport2.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(len(transport2.trajets_tries), 1)

    def test_trajets_en_commun(self):
        chauffeur = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT])
        client2 = Client.objects.create(
            type_client=['transport'], nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        transport1 = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(minutes=10),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        transport2 = self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        self.client.force_login(self.user)
        url = reverse('trajet-a-plusieurs', args=[transport1.trajets_tries[0].pk])
        response = self.client.get(url)
        self.assertContains(response, '<input type="checkbox"', count=2)
        response = self.client.post(url, data={'trajets': [transport2.trajets_tries[0].pk]})
        self.assertEqual(response.json()['result'], 'OK')
        commun = TrajetCommun.objects.first()
        self.assertQuerySetEqual(
            commun.trajet_set.all().order_by('pk'),
            [transport1.trajets_tries[0], transport2.trajets_tries[0]]
        )
        # Édition
        response = self.client.get(url)
        self.assertContains(
            response,
            f'<input type="checkbox" name="trajets" value="{transport2.trajets_tries[0].pk}" '
            'id="id_trajets_1" class="form-check-input" checked>',
            html=True
        )
        response = self.client.post(url, data={'trajets': [transport2.trajets_tries[1].pk]})
        self.assertEqual(response.json()['result'], 'OK')
        commun.refresh_from_db()
        self.assertQuerySetEqual(
            commun.trajet_set.all().order_by('pk'),
            [transport1.trajets_tries[0], transport2.trajets_tries[1]]
        )
        # Suppression
        response = self.client.post(url, data={'trajets': []})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(TrajetCommun.objects.all().count(), 0)

    def test_trajets_communs_annulation(self):
        chauffeur = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT])
        client2 = Client.objects.create(
            type_client=['transport'], nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        client3 = Client.objects.create(
            type_client=['transport'], nom='Schmid', prenom='Germain',
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23), empl_geo=[6.820, 47.094]
        )
        transport1 = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(minutes=10),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        transport2 = self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        transport3 = self.create_transport(
            client=client3,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        tc = TrajetCommun.objects.create(chauffeur=chauffeur, date=self.demain_1130.date())
        transport1.trajets_tries[0].commun = tc
        transport1.trajets_tries[0].save()
        transport2.trajets_tries[0].commun = tc
        transport2.trajets_tries[0].save()
        transport3.trajets_tries[0].commun = tc
        transport3.trajets_tries[0].save()
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-cancel', args=[transport3.pk]), {
            'raison_annulation': "Trouvé autre moyen",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(tc.trajet_set.count(), 2)
        response = self.client.post(reverse('transport-cancel', args=[transport2.pk]), {
            'raison_annulation': "Trouvé autre moyen",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(TrajetCommun.objects.count(), 0)

    def test_annulation_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('transport-cancel', args=[transport.pk]), {
            'raison_annulation': "Trouvé autre moyen",
            'km': '4',
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'true',
        })
        self.assertEqual(response.json()['result'], 'OK')
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)
        self.assertEqual(transport.date_annulation.date(), date.today())
        self.assertEqual(transport.journaux.latest().description, "Annulation du transport")
        # Annuler l'annulation!
        response = self.client.post(reverse('transport-cancel', args=[transport.pk]), {
            'annule': '',
            'raison_annulation': "Trouvé autre moyen",
            'km': '4',
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'true',
        })
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)
        self.assertIsNone(transport.date_annulation)
        self.assertEqual(transport.raison_annulation, '')
        self.assertEqual(transport.statut_fact, Transport.FacturationChoices.FACTURER)
        self.assertIsNone(transport.defrayer_chauffeur)
        self.assertEqual(
            transport.journaux.latest().description,
            "Le transport qui était annulé a été réactivé"
        )

    def test_annulation_transport_erreurs(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        form = CancelForm(instance=transport, data={
            'raison_annulation': "Va savoir",
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': True,
            'km': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['km'], ["Vous devez indiquer les kilomètres pour défrayer le chauffeur."])
        form = CancelForm(instance=transport, data={
            'raison_annulation': "Va savoir",
            'statut_fact': Transport.FacturationChoices.FACTURER_ANNUL_KM,
            'defrayer_chauffeur': False,
            'km': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['km'], ["Vous devez indiquer les kilomètres pour facturer les km du chauffeur."])

    def test_confirmation_transport(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        self.client.force_login(self.user)
        confirm_url = reverse('transport-confirm')
        response = self.client.post(confirm_url, data={'selection': []}, follow=True)
        self.assertContains(response, "Vous n’avez pas sélectionné de transport")
        response = self.client.post(confirm_url, data={'selection': [str(transport.pk)]}, follow=True)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.CONFIRME]))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.CONFIRME)

    def test_liste_transports_vide(self):
        self.client.force_login(self.user)
        saisi_url = reverse('transports-by-status', args=[Transport.StatutChoices.SAISI])
        response = self.client.get(saisi_url, HTTP_X_REQUESTED_WITH='fetch')
        # Django 4.2:
        #response = self.client.get(saisi_url, headers={'X-Requested-With': 'fetch'})
        self.assertContains(response, "Cette liste est actuellement vide.")

    def test_liste_transports_saisis_par_jour(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_modele(
            client=self.trclient,
            date=self.demain_1130 - timedelta(days=4),#.strftime('%Y-%m-%d'),
            heure_depart='15:15',
            heure_rdv='15:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='False',
            repetition='DAILY',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse(
            'transports-by-status-and-day',
            args=[Transport.StatutChoices.SAISI, self.demain_1130.strftime('%Y-%m-%d')]
        ), HTTP_X_REQUESTED_WITH='fetch')
        # Tester présence transport unique + occurrence modèle pour ce jour
        self.assertEqual(
            [tr.duree_rdv for tr in response.context['object_list'][self.demain_1130.date()]['transports']],
            [timedelta(seconds=4200), timedelta(seconds=2700)]
        )

    def test_liste_transports_filtres(self):
        chauffeur = Benevole.objects.create(nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT])
        client2 = Client.objects.create(
            type_client=['transport'], nom='Müller', prenom='Hans',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        adr2 = Adresse.objects.create(
            nom='Permanence Volta', rue='Rue Numa-Droz 187', npa='2300', localite='La Chaux-de-Fonds',
            empl_geo=[6.8145, 47.095]
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=adr2, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130 + timedelta(seconds=7200),
            origine=adr2, retour=False,
            statut=Transport.StatutChoices.SAISI,
        )
        self.create_transport(
            client=client2,
            heure_rdv=self.demain_1130 + timedelta(seconds=14400),
            duree_rdv=timedelta(minutes=70), destination=adr2, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur,
        )
        # Modèle aller-retour vers NE
        self.create_modele(
            client=self.trclient,
            date=self.demain_1130.strftime('%Y-%m-%d'),
            heure_depart='15:15',
            heure_rdv='15:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
        )
        # Modèle pour aller simple retour domicile CdF (pendant 15j = 2x)
        self.create_modele(
            client=self.trclient,
            date=self.demain_1130.strftime('%Y-%m-%d'),
            heure_depart='15:15',
            heure_rdv='15:30',
            duree_rdv='00:45',
            origine_adr=self.hne_ne.pk,
            retour='False',
            repetition='WEEKLY',
            repetition_fin=(self.demain_1130 + timedelta(days=15)).strftime('%Y-%m-%d'),
        )

        def count_transports(response):
            return sum(val['len'] for val in response.context['object_list'].values())

        self.client.force_login(self.user)
        saisi_url = reverse('transports-by-status', args=[Transport.StatutChoices.SAISI])
        response = self.client.get(saisi_url, HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(count_transports(response), 15)
        self.assertNotContains(response, "Défraiements du mois précédent")
        response = self.client.get(
            f'{saisi_url}?date={self.demain_1130.strftime("%Y-%m-%d")}', HTTP_X_REQUESTED_WITH='fetch'
        )
        self.assertEqual(count_transports(response), 5)
        response = self.client.get(f'{saisi_url}?client=mûll', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(count_transports(response), 2)
        response = self.client.get(f'{saisi_url}?destination=chaux', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(
            {dt: [tr.client for tr in lst['transports']] for dt, lst in response.context['object_list'].items()},
            {
                self.demain_1130.date(): [client2],
                self.demain_1130.date() + timedelta(days=7): [self.trclient],
                self.demain_1130.date() + timedelta(days=14): [self.trclient],
            }
        )

        attr_url = reverse('transports-by-status', args=[Transport.StatutChoices.ATTRIBUE])
        response = self.client.get(f'{attr_url}?chauffeur=dupl', HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_liste_transports_controles(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT], empl_geo=[6.81, 47.11]
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE, chauffeur=chauffeur,
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
        )
        # Transport facturé
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
            date_facture=date(2023, 2, 10),
        )
        # Transport non facturé mais ne doit pas l'être
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=chauffeur,
            date_facture=None, statut_fact=Transport.FacturationChoices.PAS_FACTURER,
        )
        self.client.force_login(self.user)
        url = reverse('transports-by-status', args=[Transport.StatutChoices.CONTROLE])
        response = self.client.get(url, HTTP_X_REQUESTED_WITH='fetch')
        self.assertEqual(
            sum(val['len'] for val in response.context['object_list'].values()),
            1
        )

    def test_transports_par_jour(self):
        # Test minimal pour le moment
        # Chauffeur avec disponibilité et transport
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT], empl_geo=[6.81, 47.11]
        )
        Dispo.objects.create(
            chauffeur=chauffeur,
            debut=self.demain_1130 - timedelta(hours=5),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=chauffeur
        )
        # Chauffeur avec disponibilité annulée
        chauffeur_abs = Benevole.objects.create(
            nom="Absente", prenom="Julie", activites=[Activites.TRANSPORT], empl_geo=[6.83, 47.01]
        )
        dispo_a_annuler = Dispo.objects.create(
            chauffeur=chauffeur_abs,
            debut=self.demain_1130 - timedelta(days=7, hours=5),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=1),
        )
        dispo_a_annuler.cancel_for(self.demain_1130.date())
        # Chauffeur avec dispo, mais en vacances
        chauffeur_vac = Benevole.objects.create(
            nom="Vacances", prenom="Lise", activites=[Activites.TRANSPORT],
        )
        Dispo.objects.create(
            chauffeur=chauffeur_vac,
            debut=self.demain_1130 - timedelta(hours=5),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        Dispo.objects.create(
            chauffeur=chauffeur_vac, categorie='absence',
            debut=now().replace(hour=6, minute=0),
            fin=now().replace(hour=22, minute=0),
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence=now() + timedelta(days=7),
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('transports-by-day', args=[
            self.demain_1130.year, self.demain_1130.month, self.demain_1130.day
        ]))
        self.assertEqual(list(response.context['chauffeurs'].keys()), [chauffeur])
        self.assertEqual(list(response.context['chauffeurs_dispos'].keys()), [])
        self.assertContains(response, "Duplain Irma")


class TransportRapportTests(TempMediaRootMixin, DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = Benevole.objects.create(
            nom="Dupond", prenom="Lily3", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        cls.chauffeur.utilisateur = Utilisateur.objects.create_user(
            'benev@example.org', '1234', first_name="Dupond", last_name="Lily3",
        )

    def test_rapport_conforme(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        Frais.objects.create(transport=transport, typ='repas', cout='17.5')
        fr_park = Frais.objects.create(
            transport=transport, typ=Frais.TypeFrais.PARKING, cout='4.5'
        )
        self.assertFalse(transport.rapport_complet)
        self.assertFalse(transport.rapport_conforme)
        transport.km = (transport.dist_calc_chauffeur * 1.03) / 1000
        transport.duree_eff = timedelta(hours=3)
        transport.temps_attente = timedelta(hours=2)
        self.assertTrue(transport.rapport_conforme)
        # Tester divers critères qui rend le rapport non conforme
        transport.km = (transport.dist_calc_chauffeur * 1.06) / 1000
        self.assertFalse(transport.rapport_conforme)
        transport.km = (transport.dist_calc_chauffeur * 1.03) / 1000
        transport.temps_attente = timedelta(hours=4)
        self.assertFalse(transport.rapport_conforme)
        transport.temps_attente = timedelta(hours=2)
        transport.rapport_chauffeur = "Aïe aïe."
        self.assertFalse(transport.rapport_conforme)
        transport.rapport_chauffeur = ""
        fr_park.cout = 8
        fr_park.save()
        self.assertFalse(transport.rapport_conforme)
        fr_park.cout = 4.5
        fr_park.save()
        self.assertTrue(transport.rapport_conforme)

    def test_rapport_transport(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': '',
            'km': '28.5',
            'duree_eff': "00:42",
            'temps_attente': "00:30",
            'rapport_chauffeur': "Tout est OK.",
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': transport.pk,
            'frais-0-cout': '14.5',
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png'),
        }
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.journaux.latest().description, "Rapport initial du transport")
        self.assertEqual(transport.statut, Transport.StatutChoices.RAPPORTE)
        self.assertEqual(transport.frais.count(), 1)
        self.assertEqual(transport.frais.first().justif.name, 'justificatifs/test.png')
        # Resending same data twice should not duplicating frais formsets
        rapport_data['frais-0-justif'] = SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png')
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        transport.refresh_from_db()
        self.assertEqual(transport.frais.count(), 1)

        # Second edition
        rapport_data.update({
            'frais-INITIAL_FORMS': '1',
            'temps_attente': "00:35",
            'frais-0-id': transport.frais.first().pk,
            'frais-0-justif': '',
        })
        response = self.client.post(
            reverse('transport-rapport', args=[transport.pk]), data=rapport_data,
        )
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        self.assertEqual(
            transport.journaux.latest().description,
            "Modification du rapport: temps d’attente (de «00:30» à «00:35»)"
        )

    def test_rapport_transport_conforme(self):
        """Le rapport est conforme et passe directement en "contrôlé"."""
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': '',
            'km': int(transport.dist_calc / 1000) + 0.5,
            'duree_eff': "00:42",
            'temps_attente': "00:30",
            'rapport_chauffeur': "",
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': transport.pk,
            'frais-0-cout': '14.5',
            'frais-0-justif': '',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=rapport_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.CONTROLE)

    def test_rapport_frais_repas(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        rapport_data = {
            'km': '28.5',
            'duree_eff': "00:42",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '1',
            'frais-0-id': '',
            'frais-0-typ': 'repas',
            'frais-0-transport': '',
            'frais-0-cout': '',
        }
        form = RapportForm(instance=transport, data=rapport_data, files={
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png'),
        }, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.formset.errors,
            [{'cout': ['Ce champ est obligatoire.']}]
        )
        rapport_data['frais-0-cout'] = '22.60'
        form = RapportForm(instance=transport, data=rapport_data, files={
            'frais-0-justif': SimpleUploadedFile("test.png", b"x4390Df3", content_type='image/png'),
        }, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.formset.errors,
            [{'cout': ['Les frais de repas ne sont remboursés que jusqu’à 20 CHF.']}]
        )

    def test_rapport_km_durees(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130, duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        form_data = {
            'km': '0', 'duree_eff': "00:42",
            'frais-INITIAL_FORMS': '0', 'frais-TOTAL_FORMS': '0',
        }
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertTrue(form.is_valid())
        form_data['km'] = "-10"
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'km': ["La saisie de km négatifs n’est pas autorisée."]})
        form_data['km'] = "500.5"
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'km': ["La saisie de plus de 500km n’est pas autorisée."]})
        form_data.update({'km': '12', 'duree_eff': "21h00", 'temps_attente': "15h42"})
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'duree_eff': ["La durée effective ne peut pas dépasser 20 heures."],
            'temps_attente': ["Le temps d'attente ne peut pas dépasser 15 heures."],
        })

    def test_rapport_attente_plus_de_3h(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130, duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        form_data = {
            'km': '20', 'duree_eff': "02:50", 'temps_attente': "3:15",
            'frais-INITIAL_FORMS': '0', 'frais-TOTAL_FORMS': '0',
        }
        form = RapportForm(instance=transport, data=form_data, user=self.chauffeur.utilisateur)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'rapport_chauffeur': ["Pour un temps d’attente de plus de 3 heures, merci d'indiquer un commentaire."],
            'duree_eff': ['La durée effective doit être plus élevée que le temps d’attente.'],
        })

    def test_rapport_transport_aller(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), origine=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-rapport', args=[transport.pk]))
        # Le temps d'attente est tout de même visible pour les admins
        self.assertIn('temps_attente', response.context['form'].fields)
        self.assertNotContains(response, "Frais de repas")
        self.assertEqual(len(response.context['form'].formset.forms), 1)
        post_data = {
            'km': '25.0',
            'duree_eff': '00:45',
            'temps_attente': '',
            'rapport_chauffeur': '',
            'statut_fact': '1',
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=post_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        # Correction
        post_data['km'] = '25.4'
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=post_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))

    def test_rapport_transport_annule(self):
        transport = self.create_transport(
            client=self.trclient, chauffeur=self.chauffeur,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.client.force_login(self.user)
        rapport_data = {
            'annule': 'on',
            'km': '',
            'duree_eff': "",
            'temps_attente': "",
            'statut_fact': '1',
            'rapport_chauffeur': "J'ai personne vu!",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        }
        response = self.client.post(reverse('transport-rapport', args=[transport.pk]), data=rapport_data)
        self.assertRedirects(response, reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        transport.refresh_from_db()
        self.assertEqual(transport.journaux.latest().description, "Rapport initial du transport (marqué comme annulé)")
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)
        self.assertIsNone(transport.duree_eff)
        self.assertIsNone(transport.defrayer_chauffeur)
        self.assertEqual(transport.statut_fact, Transport.FacturationChoices.FACTURER_ANNUL_KM)
        # Apparaît dans les transports rapportés (même si annulé)
        response = self.client.get(
            reverse('transports-by-status', args=[Transport.StatutChoices.RAPPORTE]),
            HTTP_X_REQUESTED_WITH='fetch'
        )
        self.assertIn(transport, response.context['object_list'][self.demain_1130.date()]['transports'])


class ChauffeurTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=[Activites.TRANSPORT], empl_geo=[6.82, 47.087]
        )

    def test_liste_chauffeurs(self):
        benev = Benevole.objects.create(
            nom="Doe", prenom="Jill", activites=[Activites.TRANSPORT],
            macaron_depuis=date.today() - timedelta(days=364)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['transport']))
        self.assertContains(response, 'Liste des chauffeurs')
        response = self.client.get(reverse('benevoles', args=['transport']) + '?macaron_echu=on')
        self.assertQuerySetEqual(response.context['object_list'], [benev])

    def test_chauffeur_transports(self):
        """Liste des transports à venir, par chauffeur."""
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=self.chauffeur,
            remarques="Remarque OK"
        )
        annule = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ANNULE, chauffeur=self.chauffeur,
            remarques="Annulation il y a 3 jour",
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-transports', args=[self.chauffeur.pk]))
        self.assertContains(response, '<h4>Prochains transports de Duplain Irma</h4>')
        self.assertQuerySetEqual(response.context['object_list'], [transport, annule])
        client_url = reverse('transport-client-detail', args=[self.trclient.pk])
        self.assertContains(response, f'<a href="{client_url}">Donzé Léa</a>', html=True)

        # Test PDF list
        response = self.client.post(reverse('benevole-transports-pdf', args=[self.chauffeur.pk]), data={
            'date_de': date.today(),
            'date_a': date.today() + timedelta(days=5),
        })
        self.assertEqual(response.headers['Content-Type'], 'application/pdf')
        fact_text = pdf_text(response, pages=[0])[0]
        self.assertIn("Remarque OK", fact_text)
        self.assertNotIn("Annulation", fact_text)

        # Pas de transports dans cet intervalle.
        response = self.client.post(reverse('benevole-transports-pdf', args=[self.chauffeur.pk]), data={
            'date_de': date.today(),
            'date_a': date.today(),
        }, follow=True)
        self.assertContains(response, "Aucun transport dans la plage de dates indiquée")

    def test_chauffeur_transports_archives(self):
        """Liste des transports passés, par chauffeur."""
        hier_1130 = datetime.combine(date.today() - timedelta(days=1), time(11, 30), tzinfo=get_current_timezone())
        transport1 = self.create_transport(
            client=self.trclient,
            heure_rdv=hier_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE, chauffeur=self.chauffeur,
        )
        transport2 = self.create_transport(
            client=self.trclient,
            heure_rdv=hier_1130 - timedelta(days=4),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE, chauffeur=self.chauffeur,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-transports-archives', args=[self.chauffeur.pk]))
        self.assertContains(response, '<h4>Transports passés de Duplain Irma</h4>')
        self.assertQuerySetEqual(response.context['object_list'], [transport1, transport2])
        # Avec filtre
        response = self.client.get(
            reverse('benevole-transports-archives', args=[self.chauffeur.pk]) +
            f'?statut={Transport.StatutChoices.CONTROLE}'
        )
        self.assertQuerySetEqual(response.context['object_list'], [transport2])

    def test_ajout_preference(self):
        self.client.force_login(self.user)
        new_pref_url = reverse('transport-client-newpref', args=[self.trclient.pk])
        response = self.client.get(new_pref_url)
        self.assertContains(
            response,
            '<label for="id_typ_0"><input type="radio" name="typ" value="pref" required id="id_typ_0"'
            ' class="form-check-input">😀 Préféré</label>',
            html=True
        )
        post_data = {
            'chauffeur': self.chauffeur.pk, 'typ': "pref", 'remarque': "Top!",
        }
        response = self.client.post(new_pref_url, data=post_data)
        self.assertEqual(response.json()['result'], "OK")
        response = self.client.post(new_pref_url, data=post_data)
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ["Il existe déjà une préférence ou incompatibilité entre ce client et ce chauffeur"]
        )

    def test_chauffeur_create_dispo(self):
        self.client.force_login(self.user)
        form_data = {
            'date': '2022-11-01',
            'heure_de': '10:30',
            'heure_a': '12:15',
            'repetition': 'WEEKLY',
            'repetition_fin': '2023-05-20',
        }
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        response = self.client.get(reverse('benevole-agenda', args=[self.chauffeur.pk, 2022, 45]))
        self.assertContains(response, "10:30 - 12:15")
        # Cannot add overlapping dispo
        form_data['date'] = '2022-11-08'
        form_data['heure_de'] = '11:30'
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(
            response.context['form'].errors,
            {'__all__': ['Cette disponibilité se chevauche avec une autre déjà existante.']}
        )
        # Si l'occurrence en conflit est annulée, autorisation d'ajout par dessus.
        dispo = self.chauffeur.dispos.first()
        dispo.cancel_for(date(2022, 11, 8))
        response = self.client.post(reverse('benevole-dispo-add', args=[self.chauffeur.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')

    def test_chauffeur_create_absence(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        self.client.force_login(self.user)
        form_data = {
            'date': self.demain_1130.date(),
            'heure_de': '06:00',
            'heure_a': '22:00',
            'repetition': '',
        }
        response = self.client.post(reverse('benevole-absence-add', args=[self.chauffeur.pk]), data=form_data)
        if response.headers['Content-Type'].startswith('text/html'):
            self.fail(response.context['form'].errors)
        self.assertEqual(response.json()['result'], 'OK')

    def test_chauffeur_dispo_form(self):
        # Heures sans séparateur
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=None, data={
            'date': '2022-11-01',
            'heure_de': '1430',
            'heure_a': '1515',
        })
        self.assertTrue(form.is_valid())
        # Erreur format heure
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=None, data={
            'date': '2022-11-01',
            'heure_de': '14x30',
            'heure_a': '1515',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'heure_de': ['Saisissez une heure valide.']}
        )

    def test_chauffeur_dispo_form_passe(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence = self.demain_1130 - timedelta(days=2)
        )
        form = DispoEditForm(chauffeur=self.chauffeur, categ='dispo', instance=dispo, data={
            'date': dispo.debut.date(),
            'heure_de': '10:30',
            'heure_a': '14:45',
            'repetition': 'DAILY',
            'repetition-fin': dispo.fin_recurrence
        })
        self.assertTrue(form.is_valid(), form.errors)

    def test_chauffeur_delete_dispo(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=self.chauffeur,
        )
        self.user.user_permissions.add(Permission.objects.get(codename='change_dispo'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-dispo-delete', args=[dispo.pk]), {})
        # Pas de suppression car transport existant sur dispo
        self.assertEqual(response.json()['result'], 'OK')
        self.assertTrue(Dispo.objects.filter(pk=dispo.pk).exists())
        response = self.client.get(reverse('gestion'))
        self.assertContains(
            response,
            "Impossible de supprimer cette disponibilité car les transports suivants sont déjà attribués"
        )
        transport.delete()
        response = self.client.post(reverse('benevole-dispo-delete', args=[dispo.pk]), {})
        self.assertFalse(Dispo.objects.filter(pk=dispo.pk).exists())

    def test_chauffeur_toggle_occurrence(self):
        dispo = Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        self.user.user_permissions.add(Permission.objects.get(codename='change_dispo'))
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('benevole-dispo-edit', args=[dispo.pk]) + f'?occ={quote(self.demain_1130.isoformat())}'
        )
        self.assertContains(response, "Annuler l’occurrence du ")
        toggle_url = reverse('benevole-dispo-toggle', args=[dispo.pk])
        response = self.client.post(toggle_url, data={'occ_date': self.demain_1130.isoformat()})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(dispo.exceptions.filter(annule=True).count(), 1)
        response = self.client.post(toggle_url, data={'occ_date': self.demain_1130.isoformat()})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(dispo.exceptions.filter(annule=True).count(), 0)

    def test_chauffeur_day_view(self):
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=self.chauffeur,
        )
        self.client.force_login(self.user)
        demain = self.demain_1130.date()
        response = self.client.get(reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transport.pk]))
        self.assertEqual(response.context['start_hour'], response.context['view'].START_HOUR)
        self.assertContains(
            response, f'<div class="event-cell hour" data-day="{demain}" data-hour="10:00">',
            html=True
        )
        Dispo.objects.create(
            chauffeur=self.chauffeur,
            debut=self.demain_1130 - timedelta(hours=5),
            fin=self.demain_1130 + timedelta(hours=1),
        )
        response = self.client.get(reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transport.pk]))
        self.assertEqual(response.context['start_hour'], 6)
        transp_non_attrib = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(hours=6),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=None,
        )
        response = self.client.get(
            reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transp_non_attrib.pk])
        )
        self.assertEqual(response.context['start_hour'], 5)

    def test_chauffeur_day_overlapping(self):
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE, chauffeur=self.chauffeur,
        )
        transp_non_attrib = self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130 - timedelta(hours=1),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI, chauffeur=None,
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('benevole-dispo-transport', args=[self.chauffeur.pk, transp_non_attrib.pk])
        )
        self.assertContains(response, "width: calc(50.0% - 22px);")


class ChoixChauffeurTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.chauffeur1 = Benevole.objects.create(
            nom="Dupond", prenom="Lily1", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        cls.chauffeur2 = Benevole.objects.create(
            nom="Duplain", prenom="Irma2", activites=['transport'], empl_geo=[6.85, 47.013]  # Genev.-sur
        )
        cls.chauffeur3 = Benevole.objects.create(
            nom="Smith", prenom="John3", activites=['transport'], empl_geo=[6.885, 46.985]  # Peseux
        )
        benev = Benevole.objects.create(nom="Temp", activites=['transport'], empl_geo=[6.885, 46.985])
        benev.activite_set.all().update(inactif=True)
        cls.allerretour = cls.create_transport(
            client=cls.trclient,  # habitant CdF
            heure_rdv=cls.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=cls.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI,
        )

    def test_chauffeurs_potentiels_distance(self):
        """Tri sur critère distance uniquement."""
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur2, self.chauffeur3])

    def test_chauffeurs_potentiels_distance0(self):
        """Le chauffeur habite même immeuble que le client."""
        chauffeur4 = Benevole.objects.create(
            nom="Schmid", prenom="Hans4", activites=['transport'], empl_geo=self.trclient.empl_geo
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [chauffeur4, self.chauffeur1, self.chauffeur2, self.chauffeur3])
        self.assertEqual(chauffeurs[0].distance_vo, 0)

    def test_chauffeurs_potentiels_dispo(self):
        """
        Chauffeur avec dispo reçoit la priorité (1 ou 2 selon matching complet
        ou partiel avec la dispo), quelle que soit la distance,
        mais reçoit une priorité négative si la dispo est annulée.
        """
        Dispo.objects.create(
            chauffeur=self.chauffeur3,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        # Chauffeur2 a une dispo, mais annulee
        dispo2 = Dispo.objects.create(
            chauffeur=self.chauffeur2,
            debut=self.demain_1130 - timedelta(days=7) - timedelta(hours=1),
            fin=self.demain_1130 - timedelta(days=7) + timedelta(hours=3),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        dispo2.refresh_from_db()
        dispo2.cancel_for(self.demain_1130.date())
        dispos2 = Dispo.get_for_person(self.chauffeur2, now(), now() + timedelta(days=4))
        self.assertTrue(dispos2[0].cancelled)
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur1, self.chauffeur2])
        self.assertEqual([c.priorite_dispo for c in chauffeurs], [1, 3, 4])

        # Autre transport attribué au chauffeur3, il sera donc déja occupé pour le transport 'allerretour'
        autre_client = Client.objects.create(
            nom='Donzé', prenom='Georges', no_debiteur=9991,
            rue='Rue des Crêtets 90', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.81, 7.0]
        )
        self.create_transport(
            client=autre_client,
            chauffeur=self.chauffeur3,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ATTRIBUE,
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur3, self.chauffeur2])
        self.assertEqual([c.priorite_dispo for c in chauffeurs], [3, 3, 4])

    def test_chauffeurs_potentiels_absence(self):
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur1, self.chauffeur2, self.chauffeur3])
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=self.demain_1130 - timedelta(hours=1),
            fin=self.demain_1130 + timedelta(hours=3),
        )
        # Absence 2 jours avant, jusqu'à 2 jours après
        Dispo.objects.create(
            chauffeur=self.chauffeur1,
            debut=datetime.combine(
                date.today() - timedelta(days=2), time(6, 0), tzinfo=get_current_timezone()
            ),
            fin=datetime.combine(
                date.today() - timedelta(days=2), time(22, 0), tzinfo=get_current_timezone()
            ),
            categorie=Dispo.CategChoices.ABSENCE,
            regle=Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')[0],
            fin_recurrence=datetime.combine(
                date.today() + timedelta(days=2), time(22, 0), tzinfo=get_current_timezone()
            ),
        )
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])


    def test_chauffeurs_potentiels_preference(self):
        Preference.objects.create(client=self.trclient, chauffeur=self.chauffeur3, typ='pref')
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur1, self.chauffeur2])
        Preference.objects.create(client=self.trclient, chauffeur=self.chauffeur1, typ='incomp')
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur3, self.chauffeur2, self.chauffeur1])

    def test_chauffeurs_potentiels_incompats(self):
        self.trclient.handicaps = ['deamb', 'cannes']
        self.trclient.save()
        self.chauffeur1.incompats = ['poids', 'deamb']
        self.chauffeur1.save()
        self.chauffeur2.incompats = ['poids']
        self.chauffeur2.save()
        chauffeurs = self.allerretour.chauffeurs_potentiels()
        self.assertEqual(chauffeurs, [self.chauffeur2, self.chauffeur3, self.chauffeur1])


class TransportModeleTests(DataMixin, TestCase):
    def test_model_fin(self):
        modele = self.create_modele(
            client=self.trclient,
            date=now().strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='DAILY',
            repetition_fin=(now() + timedelta(days=4)).strftime('%Y-%m-%d'),
        )
        occs = modele.generer_jusqua(now() + timedelta(days=20))
        self.assertEqual(len(occs), 4)

    def test_model_suspension(self):
        modele = self.create_modele(
            client=self.trclient,
            date=(now()).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='DAILY',
            repetition_fin='',
        )
        modele.suspension_depuis = date.today() + timedelta(days=4)
        modele.save()
        occs = modele.generer_jusqua(now() + timedelta(days=20))
        self.assertEqual(len(occs), 3)

    def test_saisie_transport_modele(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'True',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '00:45',
            'repetition': 'WEEKLY',
            'repetition_fin': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': '',
            'trajets-1-ORDER': '1',
            'trajets-1-destination_adr': '',
        })
        modele = TransportModel.objects.first()
        self.assertEqual(modele.regle.frequence, 'WEEKLY')
        transport = Transport.objects.get(client=self.trclient)
        self.assertEqual(transport.modele, modele)
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 30))
        self.assertEqual(modele.heure_depart, time(10, 15))

    def test_saisie_transport_modele_aller_simple(self):
        self.client.force_login(self.user)
        in_5_days = date.today() + timedelta(days=5)
        self.client.post(reverse('transport-saisie'), data={
            'client_select': 'Don',
            'client': self.trclient.pk,
            'typ': 'medic',
            'retour': 'False',
            'date': in_5_days.strftime('%Y-%m-%d'),
            'heure_depart': '10:15',
            'heure_rdv': '10:30',
            'duree_rdv': '',
            'repetition': 'WEEKLY',
            'repetition_fin': '',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': '',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
        })
        modele = TransportModel.objects.first()
        self.assertEqual(modele.regle.frequence, 'WEEKLY')
        transport = Transport.objects.get(client=self.trclient)
        self.assertEqual(transport.modele, modele)
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 30))

    def test_edit_transport_modele(self):
        """Édition du modèle lui-même."""
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=70)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        transport = modele.get_occurrence(date.today() + timedelta(days=7))
        transport.concretiser()

        self.client.force_login(self.user)
        response = self.client.get(reverse('transportmodel-edit', args=[modele.pk]))
        self.assertContains(
            response,
            '<input type="time" name="heure_rdv" value="10:30" class="form-control" required="" id="id_heure_rdv">',
            html=True
        )
        self.assertContains(response, "Occurrences futures")
        response = self.client.post(reverse('transportmodel-edit', args=[modele.pk]), data={
            'heure_depart': '10:10',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'remarques': 'Rdv plus tard',
            'suspension_depuis': '',
            'fin_recurrence': '',
        })
        self.assertRedirects(response, reverse('gestion'))
        modele.refresh_from_db()
        self.assertEqual(localtime(modele.heure_rdv).time(), time(10, 45))
        self.assertEqual(modele.heure_depart, time(10, 10))

    def test_liste_transports_avec_occurrences(self):
        # Passé
        self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin=(now() - timedelta(days=1)).strftime('%Y-%m-%d'),
        )
        self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('transports-by-status', args=[Transport.StatutChoices.SAISI]),
            HTTP_X_REQUESTED_WITH='fetch'
        )
        # 2 from test data + 9 new from modele
        self.assertEqual(
            sum(val['len'] for val in response.context['object_list'].values()),
            11
        )

    def test_liste_transports_par_client(self):
        """Les futures occurences apparaissent dans la liste des prochains transports du client."""
        self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('transport-client-detail', args=[self.trclient.pk]))
        self.assertEqual(len(response.context['prochains_transports']), 9)

    def test_attrib_transport_from_models(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        # Si heure de départ modifiée a posteriori, elle est respectée au moment
        # de concrétiser un transport
        modele.heure_depart = '10:10'
        modele.save()
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-attrib', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, f'Pour <b>{self.trclient}</b>')
        self.assertContains(
            response, '<div class="col-sm-3 horaire">Dép: 10:10</div>', html=True
        )

        response = self.client.post(url, data={'chauffeur': chauffeur.pk})
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.ATTRIBUE)
        self.assertEqual(transport.chauffeur, chauffeur)
        self.assertEqual(transport.heure_depart.time(), time(10, 10))

        # D'autres envois du même formulaire ne doivent pas créer plus de transports
        with self.assertLogs("django.security", "WARNING"), self.assertLogs("django.request", "WARNING"):
            response = self.client.post(url, data={'chauffeur': chauffeur.pk})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(Transport.objects.filter(modele=modele, date=next_occ_date).count(), 1)

    def test_cancel_transport_from_models(self):
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-cancel', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, f'<h3>Transport pour {self.trclient}')

        response = self.client.post(url, {
            'raison_annulation': "Trouvé autre moyen",
            'km': '',
            'statut_fact': Transport.FacturationChoices.PAS_FACTURER,
            'defrayer_chauffeur': 'false',
        })
        self.assertEqual(response.json()['result'], 'OK')
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.ANNULE)

    def test_edit_transport_from_models(self):
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.get(url)
        self.assertContains(response, '<h2>Édition d’un transport</h2>', html=True)
        self.assertContains(response, 'Pour: <span class="fw-bold">Donzé Léa</span>')
        self.assertContains(
            response,
            '<p class="fst-italic" id="adresse_depart">Rue des Crêtets 92, 2300 La Chaux-de-Fonds</p>',
            html=True
        )
        self.assertContains(
            response,
            '<div class="destination_actuelle">Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel</div>',
            html=True
        )
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': next_occ_date.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '0',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': '',
            'trajets-0-ORDER': '0',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': '',
            'trajets-1-ORDER': '1',
            'trajets-1-destination_adr': '',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = Transport.objects.get(modele=modele, date=next_occ_date)
        self.assertEqual(transport.statut, Transport.StatutChoices.SAISI)

    def test_edit_transport_from_models_retour_domicile(self):
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:30',
            heure_rdv='10:30',
            duree_rdv='',
            origine_adr=self.hne_ne,
            destination=None,
            retour='False',
            repetition='WEEKLY',
            repetition_fin='',
        )
        self.client.force_login(self.user)
        next_occ_date = (now() + timedelta(days=3)).date()
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'False',
            'date': next_occ_date.strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'origine_adr': self.hne_ne.pk,
            'trajets-INITIAL_FORMS': '1',
            'trajets-TOTAL_FORMS': '1',
            'trajets-0-id': modele.transports.first().trajets_tries[0].pk,
            'trajets-0-destination_adr': '-1',
            'trajets-0-destination_adr_select': '',
            'trajets-0-destination_princ': 'True',
        })
        self.assertRedirects(response, reverse('gestion'))
        transport = modele.transports.get(date=next_occ_date)
        self.assertEqual(transport.origine, self.hne_ne)
        self.assertEqual(transport.destination.empl_geo, self.trclient.empl_geo)

    def test_edit_occurrence_autre_date(self):
        """
        Si la date d'une occurrence est modifiée, une annulation est créée pour
        la date d'origine pour que la répétition ne la recrée pas.
        """
        modele = self.create_modele(
            client=self.trclient,
            date=(now() - timedelta(days=60)).strftime('%Y-%m-%d'),
            heure_depart='10:15',
            heure_rdv='10:30',
            duree_rdv='00:45',
            destination=self.hne_ne.pk,
            retour='True',
            repetition='WEEKLY',
            repetition_fin='',
        )
        modele._check_trajets()
        next_occ_date = (now() + timedelta(days=3)).date()
        self.client.force_login(self.user)
        url = reverse('transport-model-edit', args=[modele.pk, next_occ_date.strftime('%Y%m%d')])
        response = self.client.post(url, {
            'typ': 'medic',
            'remarques': 'Déplacé après tél.',
            'retour': 'True',
            'date': (next_occ_date + timedelta(days=1)).strftime('%Y-%m-%d'),
            'heure_depart': '10:20',
            'heure_rdv': '10:45',
            'duree_rdv': '00:45',
            'trajets-INITIAL_FORMS': '2',
            'trajets-TOTAL_FORMS': '2',
            'trajets-0-id': str(modele.trajets_tries[0].pk),
            'trajets-0-ORDER': '1',
            'trajets-0-destination_princ': 'True',
            'trajets-0-destination_adr': self.hne_ne.pk,
            'trajets-0-destination_adr_select': 'HNE',
            'trajets-1-id': str(modele.trajets_tries[1].pk),
            'trajets-1-ORDER': '2',
            'trajets-1-destination_princ': 'False',
            'trajets-1-destination_adr': '-1',
        })
        self.assertRedirects(response, reverse('gestion'))
        transps = modele.transports.filter(date__gt=date.today()).order_by('date')
        self.assertEqual(len(transps), 2)
        self.assertEqual(transps[0].statut, Transport.StatutChoices.ANNULE)
        self.assertEqual(transps[0].date, next_occ_date)
        self.assertEqual(transps[0].raison_annulation, "Report de cette occurrence à une autre date.")
        self.assertEqual(transps[0].statut_fact, Transport.FacturationChoices.PAS_FACTURER)
        self.assertIs(transps[0].defrayer_chauffeur, False)
        self.assertGreater(len(transps[0].trajets_tries), 0)
        self.assertEqual(transps[1].statut, Transport.StatutChoices.SAISI)
        self.assertEqual(transps[1].date, next_occ_date + timedelta(days=1))
        self.assertGreater(len(transps[1].trajets_tries), 0)


class FacturesTests(DataMixin, TempMediaRootMixin, TestCase):
    def test_cout_annulation(self):
        """Les frais d'annulation dépendent de la date du transport."""
        transport = Transport(date=date(2020, 4, 3))
        self.assertEqual(transport.cout_annulation(), Decimal('10'))
        transport = Transport(date=date(2024, 1, 3))
        self.assertEqual(transport.cout_annulation(), Decimal('25'))

    def test_facturer_transport(self):
        self.maxDiff = None
        client2 = Client.objects.create(
            type_client=['transport'], nom='Schmid', prenom='Germain', no_debiteur=999,
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23), empl_geo=[6.820, 47.094]
        )
        day_in_passed_month = date.today() - relativedelta.relativedelta(months=1)
        tr1 = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(day_in_passed_month, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True, km=15,
            statut=Transport.StatutChoices.CONTROLE,
        )
        Frais.objects.create(transport=tr1, typ=Frais.TypeFrais.REPAS, cout=Decimal("17.90"))
        Frais.objects.create(transport=tr1, typ=Frais.TypeFrais.PARKING, cout=Decimal("3.45"))
        self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(day_in_passed_month, time(15, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.EFFECTUE,
        )
        self.create_transport(
            client=client2,
            heure_rdv=datetime.combine(day_in_passed_month, time(15, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
        )
        self.assertEqual(
            tr1.donnees_facturation(),
            {
                'no': tr1.pk,
                'avs': True,
                'rendez-vous': localtime(tr1.heure_rdv),
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'km': Decimal('25.4'),
                'cout_km': Decimal('22.86'),  # 25.4 * 0.9
                'cout_forfait': Decimal('9.00'),
                'frais': [
                    {'descr': "Facturation des frais de repas", 'cout': Decimal("17.90")},
                    {'descr': "Facturation des frais", 'cout': Decimal("3.45")}
                ],
                'total': Decimal('53.21'),
            }
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('facturer-transports'))
        self.assertContains(
            response,
            "Attention, il semble qu’il reste un certain nombre de transports non contrôlés (1)."
        )
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')
        fact_text = pdf_text(response, pages=[0])[0]
        self.assertIn("11:30", fact_text)
        self.assertIn("CHF 0.90/km", fact_text)
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        self.assertEqual(self.trclient.factures_transports.first().montant_total, Decimal('57.30'))
        self.assertEqual(client2.factures_transports.count(), 1)
        self.assertEqual(client2.factures_transports.first().montant_total, Decimal('29.45'))
        # Un deuxième envoi ne doit pas générer les mêmes factures à double
        response = self.client.post(
            reverse('facturer-transports'), data={'date_facture': date.today()}, follow=True
        )
        self.assertIn('text/html', response['Content-Type'])
        self.assertContains(response, "Aucun transport à facturer pour ce mois")
        # Test de toutes les factures PDF du mois
        response = self.client.get(
            reverse('factures-du-mois', args=[day_in_passed_month.year, day_in_passed_month.month])
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_facturation_minimale(self):
        """Un minimum de kilomètres sont facturés"""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE,
        )
        tr.trajets_tries[0].dist_calc = 1200
        tr.trajets_tries[0].save()
        tr.trajets_tries[1].dist_calc = 1200
        tr.trajets_tries[1].save()
        donnees_fact = tr.donnees_facturation()
        self.assertEqual(donnees_fact['km'], Decimal('4.0'))
        self.assertEqual(donnees_fact['cout_km'], Decimal('3.60'))

    def test_facturation_aller_simple(self):
        """Les km retour sont aussi facturés pour un aller simple."""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
        )
        self.assertEqual(sum(traj.dist_calc for traj in tr.trajets_tries), 12702)
        donnees_fact = tr.donnees_facturation()
        self.assertEqual(donnees_fact['km'], round(Decimal(MOCKED_DISTANCE) / 1000, 2) * 2)
        self.assertEqual(donnees_fact['cout_km'], Decimal('22.86'))

    def test_non_facturation(self):
        """Un transport peut être marqué comme à ne pas facturer"""
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.CONTROLE,
            statut_fact=Transport.FacturationChoices.PAS_FACTURER,
        )
        self.assertIsNone(tr.donnees_facturation())

    def test_facturer_transport_annule(self):
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.ANNULE,
            statut_fact=Transport.FacturationChoices.PAS_FACTURER,
            km=Decimal(12),
        )
        self.assertIsNone(tr.donnees_facturation())
        un_du_mois_prec = (date.today().replace(day=1) - timedelta(days=2)).replace(day=1)
        self.assertQuerySetEqual(
            Transport.objects.a_facturer_pour(un_du_mois_prec, billed=False),
            []
        )

        tr.statut_fact = Transport.FacturationChoices.FACTURER_ANNUL
        tr.save()
        self.assertEqual(
            tr.donnees_facturation(),
            {
                'no': tr.pk,
                'avs': True,
                'rendez-vous': tr.heure_rdv,
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'annulation': Decimal('10.00'),
                'km': Decimal(0),
                'cout_km': Decimal(0),
                'total': Decimal('10.00'),
            }
        )
        tr.statut_fact = Transport.FacturationChoices.FACTURER_ANNUL_KM
        tr.save()
        self.assertEqual(
            tr.donnees_facturation(),
            {
                'no': tr.pk,
                'avs': True,
                'rendez-vous': tr.heure_rdv,
                'type': 'Médico-thérapeutique',
                'depart': 'Rue des Crêtets 92, 2300 La Chaux-de-Fonds',
                'destination': 'Hôpital neuchâtelois, Rue de la Maladière 45, 2000 Neuchâtel',
                'annulation': Decimal('10.00'),
                'km': Decimal(12),
                'cout_km': Decimal('10.80'),
                'total': Decimal('20.80'),
            }
        )
        self.assertQuerySetEqual(
            Transport.objects.a_facturer_pour(un_du_mois_prec, billed=False),
            [tr]
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_facturer_transport_avec_attente(self):
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        test_data = [
            (70, 0),
            (100, 0),
            (125, Decimal("5.00")),
            (180, Decimal("15.00")),  # 3 * 5
        ]
        for minutes, cost in test_data:
            tr.temps_attente = timedelta(minutes=minutes)
            if cost == 0:
                self.assertNotIn('cout_attente', tr.donnees_facturation())
            else:
                self.assertEqual(tr.donnees_facturation()['cout_attente'], cost)
        tr.save()
        self.assertEqual(tr.donnees_facturation()['total'], Decimal('46.86'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')

    def test_tarif_avs_acquis(self):
        self.trclient.date_naissance = date(date.today().year - 62, 2, 10)
        self.trclient.save()
        tr = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(
                date.today() - relativedelta.relativedelta(months=1), time(15, 30),
                tzinfo=get_current_timezone()
            ),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.RAPPORTE
        )
        self.assertEqual(tr.tarif_km, settings.TARIF_NON_AVS)
        self.trclient.tarif_avs_acquis = True
        self.trclient.save()
        self.assertEqual(tr.tarif_km, settings.TARIF_AVS)

    def test_autre_debiteur(self):
        referent = Referent.objects.create(
            client=self.trclient, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            complement="Avocat conseil", no_debiteur="12345",
            facturation_pour=['transp'],
        )
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')
        text1, text_qr = pdf_text(response, pages=[0, -1])
        self.assertIn("Avocat conseil", text1)
        self.assertIn("Debit Arthur", text_qr)
        facture = self.trclient.factures_transports.first()
        self.assertEqual(facture.autre_debiteur, referent)
        self.assertEqual(facture.no_debiteur, referent.no_debiteur)

    def test_refacturation(self):
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')
        facture = self.trclient.factures_transports.first()
        # Régénérer facture en remplaçant l'ancienne
        response = self.client.post(
            reverse('client-editer-facture', args=[facture.pk]),
            data={
                'date_facture': date.today(), 'choix_refact': 'remplacer',
            }
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        # Régénérer facture en conservant l'ancienne (qui sera annulée)
        response = self.client.post(
            reverse('client-editer-facture', args=[facture.pk]),
            data={
                'date_facture': date.today(), 'choix_refact': 'annuler',
            }
        )
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(self.trclient.factures_transports.count(), 2)
        facture.refresh_from_db()
        self.assertTrue(facture.annulee)
        facture2 = self.trclient.factures_transports.filter(annulee=False).first()
        self.assertNotEqual(facture.fichier_pdf.path, facture2.fichier_pdf.path)
        self.assertTrue(facture2.fichier_pdf.name.endswith('v2.pdf'))

    def test_refacturation_remplacer_plus_de_transport(self):
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')
        facture = self.trclient.factures_transports.first()
        transport.statut_fact = Transport.FacturationChoices.PAS_FACTURER
        transport.save()
        # Régénérer facture en remplaçant l'ancienne -> pas de nouvelle facture
        response = self.client.post(
            reverse('client-editer-facture', args=[facture.pk]),
            data={
                'date_facture': date.today(), 'choix_refact': 'remplacer',
            }
        )
        self.assertRedirects(response, reverse('client-transports-factures', args=[self.trclient.pk]))
        self.assertEqual(self.trclient.factures_transports.count(), 0)

    def test_refacturation_annuler_plus_de_transport(self):
        heure = datetime.combine(
            date.today() - relativedelta.relativedelta(months=1), time(15, 30), tzinfo=get_current_timezone()
        )
        transport = self.create_transport(
            client=self.trclient,
            date=heure.date(),
            heure_rdv=heure,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('facturer-transports'), data={'date_facture': date.today()})
        self.assertEqual(response['Content-Type'], 'application/pdf')
        facture = self.trclient.factures_transports.first()
        transport.statut_fact = Transport.FacturationChoices.PAS_FACTURER
        transport.save()
        # Régénérer facture en conservant l'ancienne (qui sera annulée) -> pas de nouvelle facture
        response = self.client.post(
            reverse('client-editer-facture', args=[facture.pk]),
            data={
                'date_facture': date.today(), 'choix_refact': 'annuler',
            }
        )
        self.assertRedirects(response, reverse('client-transports-factures', args=[self.trclient.pk]))
        self.assertEqual(self.trclient.factures_transports.count(), 1)
        facture.refresh_from_db()
        self.assertTrue(facture.annulee)

    @freeze_time("2023-04-06")
    def test_export_compta(self):
        chauffeur1 = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        heure_rdv=datetime(2023, 3, 12, 16, 0, tzinfo=get_current_timezone())
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            date=heure_rdv.date(),
            heure_rdv=heure_rdv,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE,
        )
        heure_rdv=datetime(2023, 3, 14, 16, 0, tzinfo=get_current_timezone())
        self.create_transport(
            client=self.trclient,
            date=heure_rdv.date(),
            heure_rdv=heure_rdv,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE,
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('facturer-transports'), data={'date_facture': '2023-04-10'}
        )
        response = self.client.post(reverse('exporter-compta', args=[2023, 3]), data={})
        lines = response.content.decode('cp1252').split('\r\n')
        fact = self.trclient.factures_transports.first()
        self.assertEqual(
            lines[0],
            f'FA;Transports DONZÉ Léa;;{fact.no};31.03.2023;10.04.2023;;;;'
            '00 00000 00009 99000 00100 00027;;999;;;;;;;'
            f'340700;6100;;;;01.03.2023;;;;68.65;2;;;;;'
        )
        fact.annulee = True
        fact.save()
        response = self.client.post(reverse('client-exporter-compta', args=[fact.pk]), data={})
        lines = response.content.decode('cp1252').split('\r\n')
        self.assertEqual(
            lines[0],
            f'NC;Transports DONZÉ Léa;;{fact.no};31.03.2023;10.04.2023;;;;'
            '00 00000 00009 99000 00100 00027;;999;;;;;;;'
            f'340700;6100;;;;01.03.2023;;;;68.65;2;;;;;'
        )

    def test_defrayer_chauffeurs(self):
        chauffeur1 = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        chauffeur2 = Benevole.objects.create(
            nom="Plot", prenom="John", activites=['transport'], empl_geo=[6.73, 46.94]
        )
        NoteFrais.objects.create(
            benevole=chauffeur1,
            service='transport',
            mois=date(2022, 1, 1),
            kms=6800,
        )
        # Pas contrôlé => non pris en compte
        tr0 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=datetime(2022, 3, 12, 14, 30, tzinfo=get_current_timezone()),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.RAPPORTE
        )
        a16h = datetime(2022, 3, 12, 16, 0, tzinfo=get_current_timezone())
        tr1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur1,
            heure_rdv=a16h,
            km=Decimal('23.4'), temps_attente=timedelta(minutes=45),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        # Transports 2eme chauffeur
        tr_ch2_0 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('19.2'), temps_attente=timedelta(minutes=125),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE
        )
        tr_ch2_1 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('59'), temps_attente=timedelta(minutes=190),
            destination=self.hne_ne, retour=True, duree_rdv=timedelta(minutes=180),
            statut=Transport.StatutChoices.CONTROLE
        )
        tr_ch2_2 = self.create_transport(
            client=self.trclient, chauffeur=chauffeur2,
            heure_rdv=a16h, km=Decimal('1.5'),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.ANNULE,
            defrayer_chauffeur=True,
        )
        # 3 trajets de chauffeur2 ont été transportés ensemble
        commun = TrajetCommun.objects.create(chauffeur=chauffeur2, date=a16h.date())
        tr_ch2_0.trajets_tries[0].commun = commun
        tr_ch2_0.trajets_tries[0].save()
        tr_ch2_1.trajets_tries[0].commun = commun
        tr_ch2_1.trajets_tries[0].save()
        tr_ch2_2.trajets_tries[0].commun = commun
        tr_ch2_2.trajets_tries[0].save()
        Frais.objects.bulk_create([
            Frais(transport=tr0, typ=Frais.TypeFrais.PARKING, cout=Decimal('7.35')),  # Non contrôlé
            Frais(transport=tr1, typ=Frais.TypeFrais.REPAS, cout=Decimal('16.55')),
            Frais(transport=tr1, typ=Frais.TypeFrais.PARKING, cout=Decimal('2.4')),
            Frais(transport=tr_ch2_0, typ=Frais.TypeFrais.REPAS, cout=Decimal('15.50')),
        ])
        self.client.force_login(self.user)
        response = self.client.get(reverse('defraiements') + '?year=2022&month=3')
        expected_john_line = (
            f'<tr><td><a href="/benevoles/transport/{chauffeur2.pk}/edition/">Plot John</a></td>'
            '<td class="text-end">15.50</td><td class="text-end">0</td><td class="text-end">20.00</td>'
            '<td class="text-end">10.00</td><td class="text-end">82,2</td><td class="text-end">57.55</td>'
            '<td class="text-end">103.05</td><td>-</td></tr>'
        )
        self.assertContains(response, expected_john_line, html=True)
        # Exportation OpenXML
        response = self.client.post(reverse('defrayer-chauffeurs', args=[2022, 3]), data={})
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        content = read_xlsx(BytesIO(response.content))
        un_du_mois = datetime(2022, 3, 1, 0, 0)
        self.assertEqual(content[0][0], 'Chauffeurs')
        self.assertEqual(
            content[1], [
                'Transports DUPLAIN Irma', un_du_mois, 0, 23.4, 0, 0, '=F2*5', 16.55, 2.4,
                '=(ROUND(C2*0.70*20, 0) / 20)+(ROUND(D2*0.55*20, 0) / 20)+E2+G2+H2+I2'
            ]
        )
        self.assertEqual(
            content[2], [
                'Transports PLOT John', un_du_mois, 82.2, 0, 20, 2, '=F3*5', 15.5, 0,
                '=(ROUND(C3*0.70*20, 0) / 20)+(ROUND(D3*0.55*20, 0) / 20)+E3+G3+H3+I3'
            ]
        )
        note_frais = chauffeur1.notefrais_set.get(mois=date(2022, 3, 1))
        self.assertFalse(note_frais.lignes.filter(libelle__no='NF-B10').exists())
        # Tarif > 6000km
        self.assertEqual(note_frais.lignes.get(libelle__no='NF-B11').montant, Decimal('12.85'))
        # Après exportation, les notes de frais statiques ont été générées, les résultats devraient être les mêmes
        response = self.client.get(reverse('defraiements') + '?year=2022&month=3')
        self.assertContains(response, expected_john_line, html=True)
        # Fiche récap. des remboursements par chauffeur
        response = self.client.get(reverse('benevole-transports-rembpdf', args=[chauffeur2.pk, 2022, 3]))
        self.assertEqual(response.headers['Content-Type'], 'application/pdf')


class ClientTests(DataMixin, TestCase):
    def test_ajout_client(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Donzé', 'prenom': 'Léa',
            'rue': 'Rue des Crêtets 92', 'npalocalite': '2300 La Chaux-de-Fonds',
            'date_naissance': '1952-10-23',
        })
        client = Client.objects.get(date_naissance=date(1952, 10, 23))
        self.assertRedirects(response, reverse('transport-client-detail', args=[client.pk]))
        self.assertEqual(client.type_client, ['transport'])

    @freeze_time("2023-03-22")
    def test_client_avs(self):
        clients = Client.objects.bulk_create([
            # AVS
            Client(nom='01', genre='M', date_naissance=date(2023 - 65, 12, 3)),
            Client(nom='02', genre='F', date_naissance=date(2023 - 64, 12, 3)),
            Client(nom='03', genre='M', date_naissance=date(2023 - 61, 12, 3), tarif_avs_acquis=True),
            # Non AVS
            Client(nom='04', genre='M', date_naissance=date(2023 - 64, 12, 3)),
        ])
        self.assertIs(clients[0].is_avs(date.today()), True)
        self.assertIs(clients[1].is_avs(date.today()), True)
        self.assertIs(clients[2].is_avs(date.today()), True)
        self.assertIs(clients[3].is_avs(date.today()), False)

        # Fonction is_ofas en base de données
        from transport.views_stats import IsOFAS

        for cl in clients:
            self.create_transport(
                client=cl, heure_rdv=self.demain_1130 - timedelta(days=4),
                duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
                statut=Transport.StatutChoices.CONTROLE
            )
        self.assertQuerySetEqual(
            Transport.objects.annotate(is_ofas=IsOFAS(
                F('id'), F('date'), F('client__tarif_avs_acquis'), F('client__date_naissance'), F('client__genre')
            )).values_list('client__nom', 'is_ofas').order_by('client__nom'),
            [('01', True), ('02', True), ('03', True), ('04', False)]
        )

    def test_edit_client(self):
        """L'édition d'un client alarme crée une alerte pour l'alarme."""
        client_data = {
            'nom': 'Donzé', 'prenom': 'Léa',
            'rue': 'Rue des Crêtets 92', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-23',
        }
        client = Client.objects.create(**{
            **client_data,
            'type_client': ['alarme', 'transport'],
            'empl_geo': [6.82058572769165, 47.093910217285156],
        })
        self.client.force_login(self.user)
        client_data.update({
            'tel_1': '077 888 88 88',
            'nom': 'Jobin',
            'npalocalite': '2300 La Chaux-de-Fonds',
            'handicaps': ['deamb'],
        })
        response = self.client.post(reverse('transport-client-edit', args=[client.pk]), data=client_data)
        self.assertRedirects(response, reverse('transport-client-detail', args=[client.pk]))
        self.assertEqual(client.alertes.count(), 1)
        alerte = client.alertes.first()
        self.assertEqual(
            alerte.alerte,
            "Modifications depuis l’application «transport»: nom (de «Donzé» à «Jobin»), "
            "ajout de tél. principal («077 888 88 88»), ajout de handicaps («Déambulateur»)"
        )
        # Si le client n'est que 'transport', il peut être archivé.
        client.type_client = ['transport']
        client.save()
        response = self.client.get(reverse('transport-client-edit', args=[client.pk]))
        self.assertContains(response, "Archiver")

    def test_edit_referent(self):
        ref = Referent.objects.create(
            client=self.trclient, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            facturation_pour=['transp'],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-referent-edit', args=[self.trclient.pk, ref.pk]))
        self.assertContains(response, "Arthur")
        self.assertNotContains(response, "courrier initial")

    def test_client_archive_transports(self):
        chauffeur = Benevole.objects.create(
            nom="Duplain", prenom="Irma", activites=['transport'], empl_geo=[6.83, 47.01]
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=self.demain_1130 - timedelta(days=4),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.CONTROLE
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur,
            heure_rdv=self.demain_1130 - timedelta(days=2),
            destination=self.hne_ne, retour=False,
            statut=Transport.StatutChoices.EFFECTUE
        )
        self.create_transport(
            client=self.trclient,
            heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne, retour=True,
            statut=Transport.StatutChoices.SAISI
        )
        self.client.force_login(self.user)
        url = reverse('client-transports-archives', args=[self.trclient.pk])
        response = self.client.get(url)
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(f"{url}?statut={Transport.StatutChoices.CONTROLE}")
        self.assertEqual(len(response.context['object_list']), 1)

    def test_client_factures(self):
        self.client.force_login(self.user)
        url = reverse('client-transports-factures', args=[self.trclient.pk])
        response = self.client.get(url)
        self.assertEqual(self.trclient.factures_transports.count(), 0)
        self.assertContains(response, "Aucune facture à ce jour")

    def test_client_adresse_json(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-address') + f'?id={self.trclient.pk}')
        self.assertEqual(
            response.json(),
            {'localite': 'La Chaux-de-Fonds', 'npa': '2300', 'rue': 'Rue des Crêtets 92',
             'fonds_transport': False, 'remarques': '', 'geolocalise': True}
        )

    def test_client_liste_archive(self):
        cl1 = Client.objects.create(
            type_client=['transport'], nom='Donzé1', prenom='Léa1', archive_le=date.today(),
        )
        # Afficher aussi les clients archivés pour les transports (=ayant eu un transport dans le passé),
        # mais actifs dans un autre service
        cl2 = Client.objects.create(
            type_client=['alarme'], nom='Donzé2', prenom='Léa2', archive_le=None,
        )
        self.create_transport(
            client=cl2, heure_rdv=self.demain_1130 - timedelta(days=30),
            destination=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-archives') + '?nom_prenom=don')
        self.assertQuerySetEqual(response.context['object_list'], [cl1, cl2])

    def test_client_list_filter(self):
        client = Client.objects.create(
            type_client=['transport'], nom='Donzé1', prenom='Léa1',
        )
        Referent.objects.create(
            client=client, nom="Debit", prenom="Arthur", npa="2000", localite="Neuchâtel",
            facturation_pour=['transp'],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients') + '?listes=autres_debit')
        self.assertContains(response, "Clients avec autre débiteur")
        self.assertEqual(len(response.context['object_list']), 1)

    def test_client_list_alertes(self):
        client = Client.objects.create(
            type_client=['transport'], nom='Donzé1', prenom='Léa1',
        )
        al = Alerte.objects.create(
            client=client, cible=ClientType.TRANSPORT, alerte="Test", recu_le=now()
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients-alertes'))
        self.assertQuerySetEqual(response.context['object_list'], [al])

    def test_client_decede(self):
        client_tr = self.trclient
        client_altr = Client.objects.create(
            type_client=['alarme', 'transport'], nom='Donzé1', prenom='Léa1',
            rue='Rue des Crêtets 94', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 24),
            empl_geo=[6.8205, 47.094]
        )
        self.client.force_login(self.user)
        self.client.post(reverse('transport-client-edit', args=[client_tr.pk]), data={
            'nom': 'Donzé', 'prenom': 'Léa',
            'rue': 'Rue des Crêtets 92', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-23', 'npalocalite': '2300 La Chaux-de-Fonds',
            'date_deces': '2022-11-11',
        })
        client_tr.refresh_from_db()
        # Archivage direct si le client n'est client que de l'application courante
        self.assertEqual(client_tr.archive_le, date.today())

        self.client.post(reverse('transport-client-edit', args=[client_altr.pk]), data={
            'nom': 'Donzé1', 'prenom': 'Léa1',
            'rue': 'Rue des Crêtets 94', 'npa': '2300', 'localite': 'La Chaux-de-Fonds',
            'date_naissance': '1950-05-24', 'npalocalite': '2300 La Chaux-de-Fonds',
            'date_deces': '2022-11-11',
        })
        client_altr.refresh_from_db()
        self.assertIsNone(client_altr.archive_le)
        self.assertEqual(client_altr.type_client, ['alarme'])
        self.assertEqual(
            client_altr.alertes.first().alerte,
            "L’équipe «transport» a marqué cette personne comme décédée"
        )


class StatsTests(DataMixin, TestCase):
    def test_stats_index(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-index'))
        self.assertContains(response, '<td>Nbre de clients uniques</td>')


class OtherTests(DataMixin, TestCase):
    def test_date_transp_tag(self):
        from transport.templatetags.transport_utils import date_transp

        self.assertEqual(
            date_transp(datetime(2023, 3, 12, 12, 0, tzinfo=get_current_timezone())),
            'DI 12.03.2023'
        )
        self.assertEqual(
            date_transp(datetime(2023, 3, 11, 23, 30, tzinfo=timezone.utc)),
            'DI 12.03.2023'
        )
        self.assertEqual(
            date_transp(date(2023, 3, 12)),
            'DI 12.03.2023'
        )

    @freeze_time("2023-03-22")
    def test_cancel_dispo_through_dst(self):
        dispo = Dispo.objects.create(
            chauffeur=Benevole.objects.create(nom="Duplain", prenom="Irma", activites=['transport']),
            debut=now().replace(hour=14, minute=0, microsecond=0),
            fin=now().replace(hour=17, minute=30, microsecond=0),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        dispo.cancel_for((dispo.debut + timedelta(days=7)).date())
        start = now().replace(hour=1)
        end = now().replace(hour=1) + timedelta(days=15)
        occs = dispo.get_occurrences(start, end)
        self.assertEqual(
            [o.debut.date() for o in occs if not o.cancelled],
            [date(2023, 3, 22), date(2023, 4, 5)]
        )

    def test_search_ch_view(self):
        self.client.force_login(self.user)
        with patch('httpx.get', side_effect=mocked_httpx):
            response = self.client.get(reverse('searchch-autocomplete') + '?q=john+meier')
        results = response.json()
        self.assertEqual(len(results), 2)
        self.assertEqual(results[0], {
            'label': 'Meier John, Marienfeldstrasse 92, 8252 Schlatt', 'value': 'urn:uuid:b4f420fda52419f2',
            'details': {
                'nom': 'Meier John', 'npa': '8252', 'localite': 'Schlatt',
                'rue': 'Marienfeldstrasse 92', 'tel': '+41526544230',
            }
        })
        self.assertEqual(results[1]['label'], "John Meier IT Consulting, Unterdorfstrasse 22, 4143 Dornach")

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    def test_openrouteservice_unavailable(self, mocked):
        with self.assertRaises(ORSUnavailable):
            distance_real((1, 1), (2, 2))

    def test_distance_figee(self):
        dep = [6.94, 46.995]
        arr = [6.82, 47.096]
        DistanceFigee.objects.create(
            empl_geo_dep=dep, empl_geo_arr=arr, distance=Decimal('7.7')
        )
        patcher = patch('common.distance.Client', new=FakeORSClient)
        patcher.start()
        result = distance_real(dep, arr)
        patcher.stop()
        self.assertEqual(result['distance_calc'], 12701.5)
        self.assertEqual(result['distance'], 7700)

    def test_trajet_force_distance(self):
        transport = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, duree_rdv=timedelta(minutes=70),
            retour=True
        )
        trajet = transport.trajets_tries[0]
        self.assertEqual(trajet.dist_calc, 12702)
        self.client.force_login(self.user)
        response = self.client.post(reverse('trajet-set-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })
        self.assertRedirects(response, reverse('trajets-details', args=[transport.pk]))
        trajet.refresh_from_db()
        self.assertEqual(trajet.dist_calc, 10400)
        # Figer la distance, applicable pour tout nouveau transport similaire
        response = self.client.post(reverse('trajet-set-fixed-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })
        self.assertRedirects(response, reverse('trajets-details', args=[transport.pk]))
        nouveau = self.create_transport(
            client=self.trclient, heure_rdv=self.demain_1130,
            destination=self.hne_ne, duree_rdv=timedelta(minutes=70),
            retour=True
        )
        self.assertEqual(nouveau.trajets_tries[0].dist_calc, 10400)
        # Nouvelle requête pour même trajet dne doit pas planter
        response = self.client.post(reverse('trajet-set-fixed-distance', args=[trajet.pk]), data={
            'distance_km': "10.4",
        })

    def test_duration_field(self):
        field = HMDurationField()
        good_values = (
            ('12:30', timedelta(hours=12, minutes=30)),
            ('12h30', timedelta(hours=12, minutes=30)),
            ('12.30', timedelta(hours=12, minutes=30)),
            ('0045', timedelta(minutes=45)),
            ('2:0', timedelta(hours=2)),
            ('2h', timedelta(hours=2)),
        )
        for in_value, expected in good_values:
            with self.subTest(in_value):
                self.assertEqual(field.to_python(in_value), expected)
        bad_values = (
            'ah30', '2',
        )
        for in_value in bad_values:
            with self.subTest(in_value):
                with self.assertRaises(ValidationError):
                    field.to_python(in_value)

    @override_settings(QUERY_GEOADMIN_FOR_ADDRESS=False)
    def test_phonenumber_normalization(self):
        form = AdresseEditForm(instance=None, data={
            'nom': "Dr Schmid", 'rue': "Rue du Musée 2", 'npalocalite': "2000 Neuchâtel",
            'tel': "+41325556677"
        })
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Adresse.objects.get(nom="Dr Schmid").tel, "032 555 66 77")
        form = AdresseEditForm(instance=None, data={
            'nom': "Dr Akena", 'rue': "10 Place de la Gare", 'npalocalite': "25500 Morteau",
            'tel': "+33388660000"
        })
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(Adresse.objects.get(nom="Dr Akena").tel, "+33 3 88 66 00 00")

    @patch('common.distance.Client', side_effect=httpx.HTTPError("HTTP Error"))
    def test_trajet_distance_entre(self, mocked):
        client = Client.objects.create(
            type_client=['transport'], nom='Donzé', prenom='Léa', no_debiteur=999,
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.820, 47.094]
        )
        cache.clear()
        with self.assertRaises(ORSUnavailable):
            Trajet.distance_entre(client, AdresseClient(client), self.hne_ne)
        with self.assertRaises(ORSUnavailable):
            Trajet.distance_entre(client, self.hne_ne, AdresseClient(client))

        heure = now() - timedelta(days=10)
        transp = Transport.objects.create(client=client, date=heure.date(), heure_rdv=heure)
        Trajet.objects.create(
            transport=transp,
            heure_depart=heure,
            origine_domicile=True, destination_adr=self.hne_ne, dist_calc=20120
        )
        Trajet.objects.create(
            transport=transp,
            heure_depart=heure,
            origine_adr=self.hne_ne, destination_domicile=True, dist_calc=20140
        )
        self.assertEqual(Trajet.distance_entre(client, AdresseClient(client), self.hne_ne), 20120)
        self.assertEqual(Trajet.distance_entre(client, self.hne_ne, AdresseClient(client)), 20140)


def read_xlsx(raw):
    wb = load_workbook(raw, read_only=True)
    ws = wb.active
    result = []
    for line in ws.rows:
        result.append([cell.value for cell in line])
    return result


def pdf_text(response, pages=(0,)):
    reader = PdfReader(BytesIO(response.getvalue()))
    return [reader.pages[idx_page].extract_text() for idx_page in pages]
