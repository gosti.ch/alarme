import time
from pathlib import Path
from unittest import skipIf

from django.conf import settings
from django.contrib.auth.models import Permission
from django.urls import reverse

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
# selenium drag_and_drop is known to be buggy
from seletools.actions import drag_and_drop

from common.models import Utilisateur
from client.models import (
    AdresseClient, Client, Professionnel, ProfClient, Referent,
)
from common.test_utils import LiveTestBase


class LiveTests(LiveTestBase):
    def setUp(self):
        self.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        self.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_client', 'change_client', 'view_alarme', 'change_alarme', 'view_facture',
                'change_professionnel',
            ]))
        )

    def test_new_client_duplicate(self):
        Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['transport'])
        self.login()
        self.goto(reverse('client-new'))
        self.selenium.find_element(By.ID, 'id_nom').send_keys('Dupo')
        doublons = WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "doublons"))
        )
        doublons.find_element(By.TAG_NAME, 'a').click()
        self.selenium.switch_to.alert.accept()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "alert-danger"))
        )
        self.assertIn("Ce client n’est pas encore identifié comme client «alarme».", self.selenium.page_source)

    def test_adresse_add(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme'])
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.XPATH, '//button[@id="new_adr_button"]')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(
            By.XPATH, value='//form[@id="standalone-form"]//input[@id="id_npalocalite"]'
        ).send_keys('2000 Neuchâtel')
        self.selenium.find_element(By.XPATH, value='//form[@id="standalone-form"]').click()
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-save"]').click()
        adr_items = self.selenium.find_elements(By.XPATH, value='//div[@id="client-addresses"]//a')
        self.assertEqual(len(adr_items), 1)
        self.assertEqual(adr_items[0].text, "2000 Neuchâtel")

    def test_adresse_edit(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        AdresseClient.objects.create(client=client, npa='1234', localite='Quelquepart')
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        link = self.find_and_scroll_to(By.XPATH, '//div[@id="client-addresses"]//a')
        link.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        # Test wrong npa value
        npaloc = self.selenium.find_element(By.XPATH, value='//form[@id="standalone-form"]//input[@id="id_npalocalite"]')
        npaloc.clear()
        npaloc.send_keys('abcd ICI')
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-save"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "formfield-error"))
        )
        self.assertIn(
            "Le numéro postal doit être uniquement composé de chiffres.",
            self.selenium.find_element(By.XPATH, value='//form[@id="standalone-form"]').text
        )
        npaloc = self.selenium.find_element(By.XPATH, value='//form[@id="standalone-form"]//input[@id="id_npalocalite"]')
        npaloc.clear()
        npaloc.send_keys('4321 Quelquepart')
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-save"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.invisibility_of_element_located((By.ID, 'siteModal'))
        )
        self.assertEqual(
            self.selenium.find_element(By.XPATH, value='//div[@id="client-addresses"]//a').text,
            "4321 Quelquepart"
        )

    def test_adresse_delete(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        AdresseClient.objects.create(client=client, npa='1234', localite='Quelquepart')
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        self.assertNotEqual(self.selenium.find_element(By.XPATH, value='//div[@id="client-addresses"]').text, '')
        link = self.find_and_scroll_to(By.XPATH, '//div[@id="client-addresses"]//a')
        link.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-delete"]').click()
        self.selenium.switch_to.alert.accept()
        time.sleep(0.2)
        self.assertEqual(self.selenium.find_element(By.XPATH, value='//div[@id="client-addresses"]').text, '')

    def test_professionnel_add_and_delete(self):
        from selenium.common.exceptions import StaleElementReferenceException

        def text_to_be_equal_in_element(locator, text_):
            def _predicate(driver):
                try:
                    element_text = driver.find_element(*locator).text
                    return text_ == element_text
                except StaleElementReferenceException:
                    return False

            return _predicate

        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        Professionnel.objects.create(nom="Centre médical des Terreaux", npa="2000", localite="Neuchâtel")
        Professionnel.objects.create(nom="Dr Jean Valjean", npa="2000", localite="Neuchâtel")
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.XPATH, '//button[@id="new_prof_button"]')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(
            By.XPATH, value='//form[@id="prof_select_form"]//input[@id="id_professionnel"]'
        ).send_keys('val')
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "dropdown-item"))
        )
        self.selenium.find_element(By.XPATH, value='//button[@class="dropdown-item"]').click()
        self.selenium.find_element(By.XPATH, value='//form[@id="prof_select_form"]//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.invisibility_of_element_located((By.ID, 'siteModal'))
        )
        self.assertEqual(client.professionnels.count(), 1)

        # Deletion
        self.selenium.find_element(By.XPATH, value='//div[@id="client-professionnels"]//a').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-delete"]').click()
        self.selenium.switch_to.alert.accept()
        WebDriverWait(self.selenium, 2).until(
            text_to_be_equal_in_element((By.ID, "client-professionnels"), '')
        )
        client = Client.objects.get(pk=client.pk)
        self.assertEqual(client.professionnels.count(), 0, client.professionnels.all())

    def test_referent_add(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.XPATH, '//button[@id="new_contact_button"]')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(
            By.CSS_SELECTOR, value='form#standalone-form input#id_nom'
        ).send_keys('Robert')
        self.selenium.find_element(
            By.CSS_SELECTOR, value='form#standalone-form input#id_npalocalite'
        ).send_keys('2000 Neuchâtel')
        self.selenium.find_element(By.ID, "id_est_repondant").click()
        self.selenium.find_element(By.ID, "id_referenttel_set-0-tel").send_keys('032 111 11 11')
        self.selenium.find_element(By.ID, "id_referenttel_set-1-tel").send_keys('0799999999')
        # Ajout d'un no tél suppl.
        self.selenium.find_element(By.CSS_SELECTOR, value='button.add_more').click()
        self.selenium.find_element(By.ID, "id_referenttel_set-2-tel")
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-save"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, 'div.contact-list-orderable div a'))
        )
        contact_items = self.selenium.find_elements(By.CSS_SELECTOR, value='div.contact-list-orderable div')
        self.assertEqual(len(contact_items), 3)
        self.assertEqual(contact_items[0].text, "1. Robert, 2000 Neuchâtel\n032 111 11 11 079 999 99 99")
        # Suppr. tél
        self.selenium.find_element(By.CSS_SELECTOR, 'a[title="Édition du contact"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(By.CLASS_NAME, 'delete-formset-link').click()
        self.selenium.switch_to.alert.accept()
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-save"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, 'reloading'))
        )
        contact_items = self.selenium.find_elements(By.CSS_SELECTOR, value='div.contact-list-orderable div')
        self.assertEqual(contact_items[0].text, "1. Robert, 2000 Neuchâtel\n079 999 99 99")

    def test_referent_add_copy_of_existent(self):
        client0 = Client.objects.create(nom="Doe", prenom="John")
        Referent.objects.create(
            client=client0, nom="SUVA", npa="2300", localite="La Chaux-de-Fonds",
            no_debiteur="5341", facturation_pour=['al-tout'],
        )
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.XPATH, '//button[@id="new_contact_button"]')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(By.ID, 'id_contact_existant').send_keys('SUV')
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "dropdown-item"))
        )
        self.selenium.find_element(By.XPATH, value='//button[@class="dropdown-item"]').click()
        nom = WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, "form#standalone-form #id_nom"))
        )
        self.assertEqual(nom.get_attribute('value'), "SUVA")

    def test_drag_and_drop_referent_repondant(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referents = Referent.objects.bulk_create([
            Referent(nom="Referent1", client=client, referent=1),
            Referent(nom="Referent2", client=client, referent=2),
            Referent(nom="Repondant1", client=client, repondant=1),
            Referent(nom="Repondant2", client=client, repondant=2),
            Referent(nom="Repondant3", client=client, repondant=3),
        ])
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        self.selenium.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(0.5)
        rep1 = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][1]/div[1]")
        rep3 = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][1]/div[3]")
        drag_and_drop(self.selenium, rep3, rep1)
        self.selenium.switch_to.alert.accept()
        ref1 = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][2]/div[1]")
        ref2 = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][2]/div[2]")
        drag_and_drop(self.selenium, ref2, ref1)
        self.selenium.switch_to.alert.accept()
        time.sleep(0.5)
        referents[0].refresh_from_db()
        self.assertEqual(referents[0].referent, 2)
        referents[1].refresh_from_db()
        self.assertEqual(referents[1].referent, 1)
        referents[2].refresh_from_db()
        self.assertEqual(referents[2].repondant, 2)
        referents[3].refresh_from_db()
        self.assertEqual(referents[3].repondant, 3)
        referents[4].refresh_from_db()
        self.assertEqual(referents[4].repondant, 1)
        self.assertEqual(
            client.journaux.latest().description,
            "Changement de priorité des référents.<br>"
            "<i>Avant:</i> 1. Referent1, 2. Referent2.<br><i>Après:</i> 1. Referent2, 2. Referent1"
        )

    @skipIf(settings.CANTON_APP == 'cr_ju', "Jura is not using the Samaritains capability")
    def test_set_unset_samaritains(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        Referent.objects.bulk_create([
            Referent(nom="Referent1", client=client, repondant=1),
            Referent(nom="Referent2", client=client, repondant=2),
        ])
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        self.selenium.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(0.5)
        self.selenium.find_element(By.ID, 'id_samaritains').click()
        self.selenium.switch_to.alert.accept()
        time.sleep(0.5)
        self.assertIn('<i>Service Samaritains</i>', self.selenium.page_source)
        ref1 = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][1]/div[1]")
        sama = self.selenium.find_element(By.XPATH, value="//div[@class='list-group contact-list-orderable'][1]/div[3]")
        drag_and_drop(self.selenium, sama, ref1)
        self.selenium.switch_to.alert.accept()
        time.sleep(0.5)
        client.refresh_from_db()
        self.assertEqual(client.samaritains, 1)
        self.assertEqual(
            client.journaux.latest().description,
            "Changement de priorité des répondants.<br>"
            "<i>Avant:</i> 1. Referent1, 2. Referent2, 3. Samaritains.<br>"
            "<i>Après:</i> 1. Samaritains, 2. Referent1, 3. Referent2"
        )
        self.selenium.find_element(By.ID, value="remove_samas").click()
        self.selenium.switch_to.alert.accept()
        WebDriverWait(self.selenium, 2).until(
            EC.presence_of_element_located((By.ID, "id_samaritains"))
        )
        client.refresh_from_db()
        self.assertIsNone(client.samaritains)

    @skipIf(settings.CANTON_APP == 'cr_ju', "Jura is not using the Samaritains capability")
    def test_add_delete_reorder_samaritains(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", samaritains=1, type_client=['alarme'])
        pro1 = Professionnel.objects.create(nom="Saint", prenom="Bernard", type_pro='Sama')
        ProfClient.objects.create(client=client, professionnel=pro1, priorite=1)
        Professionnel.objects.create(nom="Darc", prenom="Jeanne", type_pro='Sama')
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        self.selenium.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(0.5)
        self.selenium.find_element(By.CLASS_NAME, 'samas-count').click()
        self.selenium.find_element(By.CLASS_NAME, 'add-sama').click()
        self.selenium.switch_to.alert.accept()
        time.sleep(0.5)
        client.refresh_from_db()
        self.assertEqual(client.profclient_set.filter(professionnel__type_pro='Sama').count(), 2)
        self.assertEqual(client.profclient_set.get(professionnel__nom='Darc').priorite, 2)
        # Switch priority
        sama1 = self.selenium.find_element(By.XPATH, value="//ul[@class='list-group-numbered contact-list-orderable']/li[1]")
        sama2 = self.selenium.find_element(By.XPATH, value="//ul[@class='list-group-numbered contact-list-orderable']/li[2]")
        drag_and_drop(self.selenium, sama2, sama1)
        self.selenium.switch_to.alert.accept()
        time.sleep(0.2)
        client.refresh_from_db()
        self.assertEqual(client.profclient_set.get(professionnel__nom='Darc').priorite, 1)
        self.assertEqual(client.profclient_set.get(professionnel__nom='Saint').priorite, 2)
        # Remove
        self.selenium.find_elements(By.CLASS_NAME, 'remove-sama')[0].click()
        self.selenium.switch_to.alert.accept()
        self.selenium.find_element(By.CLASS_NAME, value='btn-close').click()
        client.refresh_from_db()
        self.assertEqual(client.profclient_set.filter(professionnel__type_pro='Sama').count(), 1)

    def test_upload_delete_client_file(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.login()
        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.ID, 'btn-add-file')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(By.ID, "id_titre").send_keys('Titre fichier')
        self.selenium.find_element(By.ID, "id_fichier").send_keys(str(Path(__file__)))
        self.selenium.find_element(By.XPATH, value='//button[@type="submit" and @form="standalone-form"]').click()
        WebDriverWait(self.selenium, 2).until(
            EC.invisibility_of_element_located((By.ID, 'siteModal'))
        )
        self.assertIn('Titre fichier', self.selenium.page_source)
        self.selenium.find_element(By.CLASS_NAME, 'delete-link').click()
        self.selenium.switch_to.alert.accept()
        time.sleep(0.2)
        self.assertNotIn('Titre fichier', self.selenium.page_source)

    def test_changed_form_warning(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        self.login()

        self.goto(reverse('client-edit', args=[client.pk]))
        btn = self.find_and_scroll_to(By.XPATH, '//button[@id="new_contact_button"]')
        btn.click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, "siteModal"))
        )
        self.selenium.find_element(
            By.XPATH, value='//form[@id="standalone-form"]//input[@id="id_nom"]'
        ).send_keys('Untel')
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-cancel"]').click()
        self.selenium.switch_to.alert.dismiss()
        self.selenium.find_element(By.XPATH, value='//button[@id="modal-cancel"]').click()
        self.selenium.switch_to.alert.accept()
