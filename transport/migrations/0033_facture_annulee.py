from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0032_facture_nb_transp_no_debiteur"),
    ]

    operations = [
        migrations.AddField(
            model_name="facture",
            name="annulee",
            field=models.BooleanField(default=False),
        ),
    ]
