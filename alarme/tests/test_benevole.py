from django.contrib.auth.models import Permission
from datetime import date, timedelta
from decimal import Decimal
from unittest import expectedFailure

from django.conf import settings
from django.core import mail
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.timezone import now

from alarme.models import Alarme, Frais, Mission, TypeMission
from benevole.models import Activites, Benevole
from client.models import AdresseClient, AdressePresence, Client
from common.models import Utilisateur
from common.utils import canton_abrev

from .test_general import TestUtils

today = date.today()


class BenevoleTests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Jeanne', last_name='d’Arc',
        )
        user_benev.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_alarme',
            ]))
        )
        cls.benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", utilisateur=user_benev,
            activites=[Activites.INSTALLATION],
        )

    def test_has_intervention_on(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna')
        self.assertFalse(self.benev.has_intervention_on(cl))
        Mission.objects.create(
            client=cl, benevole=self.benev, type_mission=self.typem, delai=today,
            effectuee=today - timedelta(days=30),
        )
        self.assertFalse(self.benev.has_intervention_on(cl))
        Mission.objects.create(
            client=cl, benevole=self.benev, type_mission=self.typem, delai=today,
            effectuee=today - timedelta(days=3),
        )
        self.assertTrue(self.benev.has_intervention_on(cl))
        self.assertTrue(self.benev.has_intervention_on(cl, types=['NEW']))
        self.assertFalse(self.benev.has_intervention_on(cl, types=['VISITE']))

    def test_acces_benevole_accueil(self):
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home'), follow=True)
        self.assertTemplateUsed(response, 'benevoles/index.html')

    def test_mission_client_autre_adresse(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna')
        adr = AdresseClient.objects.create(client=cl, hospitalisation=True)
        AdressePresence.objects.create(adresse=adr, depuis=date.today() - timedelta(days=10))
        Mission.objects.create(
            client=cl, benevole=self.benev,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        self.assertContains(response, "Actuellement à l’hôpital")

    def test_acces_benevole_au_client(self):
        cl = Client.objects.create(nom='Schmid', prenom='Léna')
        self.client.force_login(self.benev.utilisateur)
        edit_url = reverse('benevole-client-edit', args=[cl.pk])
        response = self.client.get(edit_url)
        # Pas d'accès sans intervention
        self.assertEqual(response.status_code, 403)

        Mission.objects.create(
            client=cl, benevole=self.benev,
            type_mission=TypeMission.objects.create(nom='Visite', code='VISITE'),
            delai=today, effectuee=None,
        )
        response = self.client.get(edit_url)
        self.assertTrue(response.context['form'].readonly)

        # Le bénévole peut modifier le client s'il a une mission d’installation pour lui
        Mission.objects.create(
            client=cl, benevole=self.benev,
            type_mission=TypeMission.objects.create(nom='Install', code='NEW'),
            delai=today, effectuee=None,
        )
        response = self.client.get(edit_url)
        self.assertFalse(response.context['form'].readonly)
        # Accès listes liées au client
        for url_name in ['client-referent-list', 'client-adresse-list', 'client-fichier-list']:
            response = self.client.get(reverse(url_name, args=[cl.pk]))
            self.assertEqual(response.status_code, 200)
        # Accès également au questionnaire pour signature
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 200)

    def test_benevole_attribution(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation'),
            delai=today + timedelta(days=10),
        )
        autre_benev = Benevole.objects.create(
            nom="Duschnock", prenom="Jules", courriel='ju@example.org',
            utilisateur=Utilisateur.objects.create_user(
                'ju@example.org', 'jupassword', first_name='Ju', last_name='Ju'
            ),
            activites=[Activites.INSTALLATION],
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "L’intervention chez Schmid Léna vous a bien été attribuée, merci !"
        )
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "Cette intervention vous a déjà été attribuée."
        )
        interv.benevole = autre_benev
        interv.save()
        response = self.client.post(reverse('benevole-attribuer', args=[interv.pk]), data={}, follow=True)
        self.assertContains(
            response,
            "Désolé, cette intervention vient d’être attribuée à une autre personne."
        )

    @expectedFailure
    @override_settings(TEST_INSTANCE=False)
    def test_benevole_mission_planifiee(self):
        """
        Quand la date de planification est fixée par un bénévole, un message
        avec les infos client et alarme est envoyé à la centrale.
        """
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), benevole=self.benev,
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', no_serie='10131839', carte_sim='+467191111111111',
            chez_benevole=self.benev,
        )
        self.client.force_login(self.benev.utilisateur)
        date_planif = today + timedelta(days=4)
        post_data = {
            'alarme': alarme1.pk,
            'planifiee_0': date_planif.strftime('%d.%m.%Y'),
            'planifiee_1': '14:30',
            'effectuee': '',
            'duree_client': '',
            'duree_seul': '',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        url = reverse('benevole-mission-edit', args=[interv.pk])
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(url, data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(mail.outbox[0].recipients(), [settings.CENTRALE_EMAIL])
        self.assertEqual(mail.outbox[0].subject, f"[Croix-Rouge {canton_abrev()}] Réservation d'installation")
        self.assertIn("Appareil: Neat NOVO 4G, SIM +467191111111111, SN 10131839", mail.outbox[0].body)
        self.assertIn(f"Installation planifiée le: {date_planif.strftime('%d.%m.%Y')} 14:30", mail.outbox[0].body)
        self.assertJournalMessage(
            interv.client, "Message d'intervention planifiée envoyé à la centrale", user=self.benev.utilisateur
        )

    def test_benevole_mission_install(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), benevole=self.benev,
        )
        alarme1 = Alarme.objects.create(
            modele=self.modele, no_appareil='123456', chez_benevole=self.benev
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-mission-edit', args=[interv.pk]))
        self.assertEqual(response.status_code, 200)
        # Si l'abonnement n'est pas encore défini, il apparaît dans le formulaire
        self.assertContains(response, 'id="id_abonnement"')
        post_data = {
            'alarme': alarme1.pk,
            'effectuee': today,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        }
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.post(url, data=post_data)
        self.assertEqual(
            response.context['form'].errors, {
                'abonnement': ['Vous devez indiquer l’abonnement choisi.'],
            }
        )
        post_data.update({'km': '25', 'abonnement': self.abo.pk})
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(interv.client.alertes.count(), 1)
        self.assertEqual(interv.client.alertes.first().alerte, "Intervention effectuée: Installation")

    def test_benevole_mission_install_abo_defini(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today + timedelta(days=10), benevole=self.benev,
            abonnement=self.abo
        )
        alarme1 = Alarme.objects.create(modele=self.modele, no_appareil='123456', chez_benevole=self.benev)
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-mission-edit', args=[interv.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'id="id_abonnement"')
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.post(url, data={
            'alarme': alarme1.pk,
            'effectuee': today,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '0',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')

    def test_mission_archive_edition(self):
        interv = Mission.objects.create(
            client=Client.objects.create(nom='Schmid', prenom='Léna'),
            type_mission=TypeMission.objects.create(nom='Installation', code='NEW'),
            delai=today, effectuee=today.replace(day=1),
            benevole=self.benev, abonnement=self.abo, km=6
        )
        self.client.force_login(self.benev.utilisateur)
        url = reverse('benevole-mission-edit', args=[interv.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Permissions insuffisantes")
        self.assertEqual(
            list(response.context['form'].fields.keys()),
            ['planifiee', 'effectuee', 'rapport', 'duree_client', 'duree_seul', 'km']
        )
        self.assertIsNotNone(response.context['form'].formset)
        response = self.client.post(url, data={
            'effectuee': interv.effectuee,
            'duree_client': '01:30',
            'duree_seul': '00:00',
            'km': '8',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')
        interv.refresh_from_db()
        self.assertEqual(interv.km, 8)

    def test_benevole_nouvelle_visite(self):
        TypeMission.objects.create(nom='Visite', code='VISITE')
        # Client sans installation
        Client.objects.create(
            nom='Schmid', prenom='Léna', type_client=['alarme'], visiteur=self.benev
        )
        install, install_resil, install_resil_passee = self.create_client_with_installations(
            3, client_data=[{'visiteur': self.benev}] * 3
        )
        # Client avec résiliation en cours
        install_resil.date_fin_abo = date.today() + timedelta(days=4)
        install_resil.save()
        install_resil_passee.date_fin_abo = date.today() - timedelta(days=4)
        install_resil_passee.save()
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home-app'))
        self.assertEqual(response.context['visites'], [install.client, install_resil.client])
        self.assertEqual(response.context['visites_a_faire'], 1)
        self.assertContains(response, "Résiliation pour le")
        response = self.client.post(reverse('benevole-visite-new', args=[install_resil.client.pk]), {
            'planifiee_0': date.today() + timedelta(days=10),
            'planifiee_1': '13:45',
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertIsNotNone(install_resil.client.mission_set.first().planifiee)
        response = self.client.get(reverse('home-app'))
        # L'appareil est affiché
        self.assertContains(response, '<div class="col-8">Neat NOVO 4G (-) (standard)</div>', html=True)

    def test_benevole_mission_sans_client_par_admin(self):
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-intervention-new', args=[self.benev.pk]), data={
            'type_mission': typem.pk,
            'description': "Réparation urgente",
            'effectuee': date.today(),
            'duree_seul': '1:30',
            'km': '6',
            'frais-TOTAL_FORMS': 1,
            'frais-INITIAL_FORMS': 0,
            'frais-0-id': '',
            'frais-0-mission': '',
            'frais-0-descriptif': 'Achat fil',
            'frais-0-cout': '12.60',
            'frais-0-justif': '',
        })
        self.assertEqual(response.json()['result'], 'OK')
        mission = Mission.objects.get(description="Réparation urgente")
        self.assertEqual(mission.benevole, self.benev)

    def test_benevole_mission_sans_client_par_benevole(self):
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)
        self.client.force_login(self.benev.utilisateur)
        response = self.client.post(reverse('benevole-mission-new'), data={
            'type_mission': typem.pk,
            'description': "Réparation urgente",
            'effectuee': '',
            'duree_seul': '',
            'km': '',
            'frais-TOTAL_FORMS': 0,
            'frais-INITIAL_FORMS': 0,
        })
        self.assertEqual(response.json()['result'], 'OK')
        mission = Mission.objects.get(description="Réparation urgente")
        self.assertEqual(mission.benevole, self.benev)
        # Edition
        url = reverse('benevole-mission-edit', args=[mission.pk])
        response = self.client.get(url)
        self.assertNotIn('duree_client', response.context['form'].fields)
        response = self.client.post(url, data={
            'planifiee_0': '',
            'planifiee_1': '',
            'effectuee': date.today(),
            'duree_seul': '1:30',
            'frais': '0',
            'km': '8',
            'frais-TOTAL_FORMS': 1,
            'frais-INITIAL_FORMS': 0,
            'frais-0-id': '',
            'frais-0-mission': mission.pk,
            'frais-0-descriptif': 'Achat fil',
            'frais-0-cout': '12.80',
            'frais-0-justif': '',
        })
        self.assertEqual(response.json()['result'], 'OK')
        self.assertEqual(mission.frais.first().cout, Decimal('12.80'))

    def test_benevole_stock(self):
        """Onglet 'Stock' de la fiche Bénévole."""
        Alarme.objects.create(modele=self.modele, no_serie='111111')
        alarme_stock = Alarme.objects.create(modele=self.modele, no_serie='111222', chez_benevole=self.benev)
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-stock', args=[self.benev.pk]))
        self.assertQuerySetEqual(response.context['appareils'], [alarme_stock])

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-alarme-search') + '?q=111')
        self.assertEqual(response.json(), [{'label': 'Neat NOVO 4G (111222)', 'value': alarme_stock.pk}])

    def test_benevole_visites(self):
        """Onglet 'Visites' de la fiche Bénévole."""
        self.client.force_login(self.user)
        client = Client.objects.create(
            nom='Schmid', prenom='Léa', rue="Fleurs 2", npa="2800", localite="Delémont",
            type_client=['alarme'], visiteur=self.benev,
        )
        self.nouvelle_install_par_mission(client=client)
        # Pas encore d'install, ne doit pas apparaître
        Client.objects.create(
            nom='Schmid', prenom='Léa', rue="Fleurs 432", npa="2800", localite="Delémont",
            type_client=['alarme'], visiteur=self.benev,
        )
        # Archivé, ne doit pas apparaître
        Client.objects.create(
            nom='Marchand', prenom='Dage', rue="Grand-Rue 2",
            npa="2000", localite="Neuchâtel",
            visiteur=self.benev, archive_le=date.today()
        )
        response = self.client.get(reverse('benevole-visites', args=[self.benev.pk]))
        self.assertQuerySetEqual(response.context['a_visiter'], [client])

    def test_archives_mois(self):
        time_in_last_month = now().replace(day=1, hour=10, minute=0) - timedelta(days=15)
        client = Client.objects.create(nom='Schmid', prenom='Léna')
        typem = TypeMission.objects.create(nom='Atelier', avec_client=False)

        def create_mission(effectuee=time_in_last_month, **kwargs):
            return Mission.objects.create(
                client=client, type_mission=typem, delai=None, effectuee=effectuee,
                benevole=self.benev, duree_client=timedelta(hours=1), km=6
            )

        # Ne doivent pas apparaître:
        create_mission(effectuee=time_in_last_month - timedelta(days=20))  # trop tôt
        create_mission(effectuee=None)  # pas terminée
        # Doivent apparaître
        m1 = create_mission()
        m2 = create_mission()
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(
            reverse('benevole-archives-month', args=[time_in_last_month.year, time_in_last_month.month])
        )
        self.assertEqual(response.context['days'], {time_in_last_month.date(): [m1, m2]})
        self.assertEqual(response.context['sums'], {
            'total_missions': 2, 'total_km': 12, 'total_frais': Decimal('4.50'),
            'total_duree_client': timedelta(hours=2), 'total_duree_seul': None,
        })
