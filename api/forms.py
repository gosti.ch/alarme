from django import forms

from common.forms import ModelForm
from client.forms import ClientEditFormBase
from client.models import Client, Referent
from benevole.models import Benevole


class ClientForm(ClientEditFormBase):
    class Meta(ClientEditFormBase.Meta):
        fields = [
            'nom', 'prenom', 'genre', 'rue', 'npa', 'localite', 'tel_1', 'tel_2',
            'courriel', 'date_naissance', 'date_deces',
        ]

    def clean(self):
        data = super().clean()
        self.doublons = None
        if not self.instance.pk and not self.errors:
            # Détection de doublons
            self.doublons = Client.objects.filter(
                nom=self.cleaned_data['nom'],
                localite=self.cleaned_data['localite'],
                date_naissance=self.cleaned_data['date_naissance']
            )
            if self.doublons:
                raise forms.ValidationError("Ce client existe déjà dans la base")
        return data


class ReferentForm(ModelForm):
    class Meta:
        model = Referent
        fields = [
            'nom', 'prenom', 'rue', 'npa', 'localite', 'courriel',
        ]


class BenevoleForm(ModelForm):
    class Meta:
        model = Benevole
        fields = [
            'nom', 'prenom', 'rue', 'npa', 'localite', 'tel_prive', 'tel_mobile',
            'courriel', 'date_naissance'
        ]

    def clean(self):
        data = super().clean()
        self.doublon = None
        if not self.instance.pk:
            # Détection de doublons
            self.doublons = Benevole.objects.filter(
                nom=self.cleaned_data['nom'],
                courriel=self.cleaned_data['courriel'],
            )
            if self.doublons:
                raise forms.ValidationError("Ce bénévole existe déjà dans la base")
        return data
