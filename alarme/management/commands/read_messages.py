import re
import ssl
import email
from datetime import datetime
from io import BytesIO

from imapclient import IMAPClient
from pypdf import PdfReader

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.mail import mail_admins
from django.core.management.base import BaseCommand
from django.utils.timezone import now

from client.models import Alerte, Client, ClientType, Journal
from alarme.models import ArticleFacture, Facture


class Command(BaseCommand):
    """
    Commande vérifiant les arrivées de messages AlarmPost de la centrale.
    Généralement exécutée par un job cron toutes les heures.
    """
    def handle(self, **options):
        context = ssl.create_default_context()
        server = IMAPClient(settings.ALARMPOST_EMAIL_HOST, ssl_context=context)
        server.login(settings.ALARMPOST_EMAIL, settings.ALARMPOST_EMAIL_PASSWORD)
        select_info = server.select_folder('INBOX')
        num_messages = select_info[b'EXISTS']

        message_ids = server.search()
        messages = server.fetch(message_ids, data=['ENVELOPE', 'RFC822'])
        successes = 0
        for mid, content in messages.items():
            envelope = content[b'ENVELOPE']
            raw = email.message_from_bytes(content[b'RFC822'])
            if '@jotform.com>' in raw['From']:
                try:
                    self.handle_jot_message(raw.get_payload())
                except Exception as exc:
                    mail_admins(
                        "[Alarmes] Erreur de traitement de courriel",
                        f"Erreur lors du traitement du courriel jotform.\n"
                        f"L'erreur est {exc}."
                    )
                else:
                    server.delete_messages([mid])
                    successes += 1
                continue
            try:
                get_attachments(raw)
            except Exception as exc:
                # Uncomment to exceptionnaly delete some message.
                # server.delete_messages([mid])
                mail_admins(
                    "[Alarmes] Courriel non traité",
                    f"Impossible de lire le courriel avec le sujet «{envelope.subject.decode()}».\n"
                    f"L'erreur est {exc}."
                )
            else:
                server.delete_messages([mid])
                successes += 1
        server.close_folder()  # also expunges deleted messages
        server.logout()
        if options.get('verbosity') > 0:
            return f"{num_messages} handled, {successes} with success"

    def handle_jot_message(self, payload):
        """Handle message received by JotForm (samaritains)."""
        from bs4 import BeautifulSoup

        def clean_text(txt):
            return txt.strip().replace("\r\n", " ").replace("\n", " ")

        doc = BeautifulSoup(payload, 'html.parser')
        day = doc.find(id="value_6").text
        day = datetime.strptime(day, '%d-%m-%Y').date()
        heure_al = clean_text(doc.find(id="value_12").text)
        nom_sama = clean_text(doc.find(id="value_4").text)
        descriptif = clean_text(doc.find(id="value_20").text)
        client_parts = clean_text(doc.find(id="value_5").text).split(",")
        parsed_nom = client_parts[0].strip().split(' ')[0]
        parsed_prenom = client_parts[0].strip().split(' ')[-1]
        parsed_npa = client_parts[-1].strip().split(' ')[0]
        clients = Client.objects.filter(
            nom__istartswith=parsed_nom, npa=parsed_npa,
            type_client__contains=['alarme'], archive_le=None,
        )
        if len(clients) == 1:
            client = clients[0]
        elif len(clients) == 0:
            raise ValueError(
                f"Impossible de trouver un client dont le nom commence par {parsed_nom} et le NPA est {parsed_npa}"
            )
        else:
            clients = clients.filter(prenom__icontains=parsed_prenom)
            if len(clients) == 1:
                client = clients[0]
            else:
                raise ValueError(
                    "Plusieurs correspondances pour un client dont le nom commence "
                    f"par {parsed_nom} et le NPA est {parsed_npa}"
                )
        article = ArticleFacture.objects.get(code=settings.CODE_ARTICLE_INTERV_SAMA)
        libelle = f"Intervention samaritain du {day.strftime('%d.%m.%Y')} ({heure_al})"
        # Plusieurs messages peuvent arriver pour la même intervention, ne pas recréer
        # la facture pour même jour/heure
        if not Facture.objects.filter(client=client, libelle=libelle).exists():
            Facture.objects.create(
                client=client, date_facture=day, mois_facture=day,
                libelle=libelle, article=article, montant=article.prix,
            )
            Journal.objects.create(
                client=client,
                description=(
                    f"Intervention samaritain facturée. Samaritain: {nom_sama}. Description: {descriptif}"
                ),
                quand=now(), qui=None
            )
        return True


def get_attachments(msg):
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue
        file_name = part.get_filename()  # Get the filename

        if file_name and 'pdf' in part.get('Content-Type').lower():
            payload = part.get_payload(decode=True)
            save_payload(payload)
            return True
    raise ValueError("No valid PDF attachment found in the message content.")


def save_payload(pdf_payload):
    """
    Extract client from PDF payload and create Journal/Alerte instances.
    """
    date_re = r'\d{1,2}\.\d{1,2}\.\d{4}\s+\d{2}:\d{2}:\d{2}'
    lines = [line.strip() for line in get_pdf_text(BytesIO(pdf_payload)).split('\n')]

    def get_line_content(header):
        content = next(line for line in lines if line.startswith(header))
        if content == header:
            content = lines[lines.index(header) + 1]  # Next line
        else:
            content = content.replace(header, '')
        for parasite in ('Gerätetyp:', 'Tel. Wohnung:'):
            if parasite in content:
                content = content.split(parasite)[0]
        return content.strip()

    # -> '12.06.2022 10:14:22'
    date_m = re.search(date_re, get_line_content('Eingegangen am:'))
    if not date_m:
        raise ValueError("Unable to find a date for the alert")
    date_obj = datetime.strptime(date_m.group(), '%d.%m.%Y %H:%M:%S').astimezone()
    ignore_words = ["Monsieur", "Madame", "Famille", "Familie", "Signora", "Herr", "Frau"]
    # -> 'Monsieur Valentin a Marca'
    name_words = [word for word in get_line_content('Name:').split() if word not in ignore_words]
    npa_loc = get_line_content('PLZ | Ort:').split(maxsplit=1)
    client = match_client(name_words, npa_loc)
    # Save PDF to client journal
    remarque = get_line_content('Bemerkung:')
    alerte = Alerte.objects.create(client=client, cible=ClientType.ALARME, alerte=remarque, recu_le=date_obj)
    alerte.fichier.save(f'alerte_{alerte.pk}.pdf', ContentFile(pdf_payload))
    Journal.objects.create(
        client=client, description=f'Alerte Curena: {remarque} (<a href="{alerte.fichier.url}">pdf</a>)',
        quand=date_obj, qui=None,
    )


def match_client(name_words, npa_loc):
    last_name = name_words[-1]
    # Find client by name/location
    try:
        client = Client.objects.get(nom__icontains=last_name, npa=npa_loc[0], localite=npa_loc[1])
    except Client.DoesNotExist:
        try:
            client = Client.objects.get(nom_part__icontains=last_name, npa=npa_loc[0], localite=npa_loc[1])
        except Client.DoesNotExist:
            raise ValueError(
                f"Unable to find client with name:{last_name}, npa:{npa_loc[0]}, localite:{npa_loc[1]}"
            )
    except Client.MultipleObjectsReturned:
        first_name = name_words[0]
        try:
            client = Client.objects.get(
                nom__icontains=last_name, prenom__icontains=first_name,
                type_client__contains=['alarme'],
                npa=npa_loc[0], localite=npa_loc[1]
            )
        except (Client.DoesNotExist, Client.MultipleObjectsReturned):
            raise ValueError(
                f"Unable to distinguished alarme client with name:{last_name}, firstname:{first_name}, "
                f"npa:{npa_loc[0]}, localite:{npa_loc[1]}"
            )
    return client


def get_pdf_text(pdf):
    read_pdf = PdfReader(pdf)
    page = read_pdf.pages[0]
    return page.extract_text()
