from django.db import migrations, models


def migrate_autre_to_parking(apps, schema_editor):
    Frais = apps.get_model('transport', 'Frais')
    Frais.objects.filter(typ='autre').update(typ='parking')


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0041_rename_mois_facture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='frais',
            name='typ',
            field=models.CharField(choices=[('repas', 'Frais de repas'), ('parking', 'Frais de parking')], max_length=7, verbose_name='Type de frais'),
        ),
        migrations.AlterField(
            model_name='frais',
            name='descriptif',
            field=models.TextField(blank=True, verbose_name='Descriptif'),
        ),
        migrations.RunPython(migrate_autre_to_parking),
    ]
