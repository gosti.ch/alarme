from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0013_typefrais_lignefrais'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lignefrais',
            name='montant',
        ),
        migrations.AddField(
            model_name='lignefrais',
            name='montant_unit',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=7),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='lignefrais',
            name='quantite',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=7),
            preserve_default=False,
        ),
    ]
