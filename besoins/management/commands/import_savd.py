import argparse
from datetime import date, datetime
from string import capwords

from openpyxl import load_workbook

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from client.models import Client, Journal


class Command(BaseCommand):
    lang_map = {
        'Allemande': ['de'],
        'Italienne': ['it'],
        'Turque': ['tr'],
    }

    def add_arguments(self, parser):
        parser.add_argument('fichier', type=argparse.FileType('rb'))

    def handle(self, **options):
        wb = load_workbook(options['fichier'])
        ws = wb.active
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                if values['Actif'] != 'Oui' or values['NOM'] in ['ENTRETIEN ADV', 'Z-COLLOQUE', 'Z-FORMATION']:
                    continue
                self.import_client(values)
            #raise Exception("Importation en phase de test")

    def import_client(self, values):
        code_postal, localite = values['NP / Ville'].split(maxsplit=1)
        no_nav = clean(values['Id externe'])
        try:
            client = Client.objects.get(
                nom__unaccent__iexact=values['NOM'], prenom__unaccent__iexact=values['PRENOM'],
                npa=code_postal
            )
        except Client.DoesNotExist:
            pass
        else:
            self.stdout.write(f"Le client {client} existe déjà")
            if client.archive_le:
                self.stdout.write(f"ATTENTION: ce client est archivé")
            data_local = f'{client.rue}/{client.npa}/{client.localite}/NAV:{client.no_debiteur}'.lower()
            data_import = f"{values['Rue']}/{code_postal}/{localite}/NAV:{no_nav}".lower()
            if data_local != data_import:
                self.stdout.write(f"{client}: Différence entre «{data_local}» et «{data_import}»")
            # Compléter avec nouvelles valeurs
            if 'savd' not in client.type_client:
                client.type_client.append('savd')
                Journal.objects.create(
                    client=client,
                    description="Ajout comme client SAVD par script d’importation",
                    quand=timezone.now()
                )
            if not client.no_debiteur:
                self.stdout.write(f"Le client {client} a reçu un nouveau no débiteur: {no_nav}")
                client.no_debiteur = no_nav
            client.save(update_fields=['type_client', 'no_debiteur'])
            return

        tel1 = clean_tel(values['Téléphone'])
        tel2 = clean_tel(values['Portable'])
        langues = self.lang_map.get(values['Langue parlée'], ['fr'])
        client = Client(
            nom=capwords(values['NOM']), prenom=values['PRENOM'], langues=langues, type_client=['savd'],
            genre=values['Sexe'] == "Féminin" and 'F' or 'M',
            date_naissance=read_date(values['NE(E) LE']),
            rue=values['Rue'],  npa=code_postal, localite=localite,
            tel_1=tel1 or tel2, tel_2=tel2 if tel1 and tel2 else '',
            courriel=clean(values['Email']),
            no_debiteur=no_nav, remarques_ext=values['Réseau primaire'] or '',
        )
        try:
            client.full_clean()
        except ValidationError as err:
            print(f"Client {client}: {err}")
        self.stdout.write(f"Création du client {client}")
        client.save()
        Journal.objects.create(
            client=client,
            description="Création client par script d’importation (SAVD)",
            quand=timezone.now()
        )


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val


def clean_tel(val):
    if val is None:
        return ''
    return "".join([c for c in val if c.isdigit() or c in [' ', '+']]).strip()


def read_date(dt):
    if not dt or dt in {'00.00.0000', '- - -'}:
        return None
    if isinstance(dt, datetime):
        return dt.date()
    return date(*reversed([int(part) for part in dt.split('.')]))

