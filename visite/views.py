from datetime import date, timedelta

from dateutil.relativedelta import relativedelta

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Count, Prefetch, Q, Sum
from django.db.models.functions import TruncMonth
from django.http import Http404, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DeleteView, ListView

from benevole.models import Benevole
from benevole.views import (
    BenevoleEditView as BenevoleEditViewBase, BenevoleListView as BenevoleListViewBase
)
from common.views import CreateUpdateView, FilterFormMixin
from client.models import Client
from client.views import ClientEditViewBase, ClientListViewBase

from . import forms
from .models import Attribution, Visite


def home(request):
    return render(request, 'visite/home.html', context={})


class ClientListView(ClientListViewBase):
    template_name = 'visite/client_list.html'
    return_all_if_unbound = True
    client_types = ['visite']

    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch('attributions', queryset=Attribution.objects.select_related('visiteur'))
        )

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "visite/base.html",
            'home_url': reverse('home'),
            'client_url_name': 'client-edit',
        }


class ClientEditView(ClientEditViewBase):
    template_name = 'visite/client_edit.html'
    base_template = 'visite/base.html'
    form_class = forms.ClientEditForm
    client_type = 'visite'


class ClientsAttenteView(ClientListView):
    template_name = 'visite/client_attente.html'
    title = "Clients en attente de visiteurs"
    filter_formclass = None

    def get_queryset(self):
        return Client.objects.filter(
            type_client__contains=['visite'], archive_le__isnull=True
        ).annotate(
            num_attr=Count('attributions'),
        ).filter(num_attr=0).order_by('nom')


class BenevoleListView(BenevoleListViewBase):
    template_name = 'visite/benevole_list.html'
    filter_formclass = forms.BenevoleFilterForm

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Benevole.objects.annotate(
                num_visites=Count('attribution', filter=Q(attribution__fin__isnull=True))
            )
        )


class BenevoleEditView(BenevoleEditViewBase):
    template_name = 'visite/benevole.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'visites_en_cours': self.object.attribution_set.filter(fin__isnull=True),
        }


class AttributionsView(PermissionRequiredMixin, ListView):
    permission_required = 'visite.view_attribution'
    model = Attribution
    template_name = 'visite/attributions.html'

    def get_queryset(self):
        qs = super().get_queryset().select_related('client', 'visiteur')
        if self.kwargs['status'] == 'encours':
            return qs.filter(fin__isnull=True).order_by('client__nom')
        if self.kwargs['status'] == 'terminees':
            return qs.filter(fin__isnull=False).order_by('-fin')
        raise Http404("Unknown status")

    def get_context_data(self, **kwargs):
        context = {**super().get_context_data(**kwargs), 'active': self.kwargs['status']}
        if self.kwargs['status'] == 'encours':
            last_day_of_last_month = date.today().replace(day=1) - timedelta(days=1)
            total_durees = {
                f"{line['mois'].strftime('%Y-%m-%d')}-{line['client']}-{line['visiteur']}": line['subtot']
                for line in Visite.objects.filter(
                    debut__date__range=(
                        (last_day_of_last_month.replace(day=1) - timedelta(days=100)).replace(day=1),
                        last_day_of_last_month
                    )
                ).annotate(mois=TruncMonth('debut')).values(
                    'mois', 'client', 'visiteur'
                ).annotate(subtot=Sum('duree'))
            }
            lastm = last_day_of_last_month.replace(day=1)
            context.update({
                'total_durees': total_durees,
                'mois': [lastm - relativedelta(months=2), lastm - relativedelta(months=1), lastm],
            })
        return context


class AttributionEditView(PermissionRequiredMixin, CreateUpdateView):
    permission_required = 'visite.add_attribution'
    model = Attribution
    form_class = forms.AttributionForm
    template_name = 'alarme/general_edit.html'
    json_response = True

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VisitesView(PermissionRequiredMixin, FilterFormMixin, ListView):
    permission_required = 'visite.view_visite'
    model = Visite
    filter_formclass = forms.VisiteFilterForm
    template_name = 'visite/visites.html'
    paginate_by = 30

    def get_queryset(self):
        return super().get_queryset().select_related('client', 'visiteur').order_by('-debut')


class VisiteEditView(PermissionRequiredMixin, CreateUpdateView):
    permission_required = 'visite.add_visite'
    model = Visite
    form_class = forms.VisiteForm
    template_name = 'visite/visite_edit.html'
    json_response = True

    def get_initial(self):
        if self.is_create:
            return {
                **super().get_initial(),
                'client': self.request.GET.get('client'),
                'visiteur': self.request.GET.get('benevole'),
                'debut': (timezone.now().replace(day=1) - timedelta(days=1)).replace(day=25, hour=12, minute=0),
            }
        else:
            return super().get_initial()

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class VisiteDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'visite.delete_visite'
    model = Visite

    def form_valid(self, form):
        self.object.delete()
        return JsonResponse({'result': 'OK', 'reload': 'page'})
