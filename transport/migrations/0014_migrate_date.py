from django.db import migrations


def complete_date(apps, schema_editor):
    Transport = apps.get_model('transport', 'Transport')
    for transp in Transport.objects.all():
        transp.date = transp.heure_rdv.date()
        transp.save()


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0013_transport_date"),
    ]

    operations = [migrations.RunPython(complete_date)]
