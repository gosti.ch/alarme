import csv
from datetime import date, timedelta
from io import StringIO
from tempfile import NamedTemporaryFile

from openpyxl import Workbook
from openpyxl.styles import Border, DEFAULT_FONT, Font, Side
from openpyxl.utils import get_column_letter

from django.http import HttpResponse

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class OpenXMLExport:
    default_font_size = 11

    def __init__(self, sheet_title=None, col_widths=None):
        DEFAULT_FONT.sz = self.default_font_size
        self.wb = Workbook()
        self.ws = self.wb.active
        if sheet_title:
            self.ws.title = sheet_title
        self.col_widths = col_widths
        self.bold = Font(name='Calibri', bold=True, sz=self.default_font_size)
        self.row_idx = 1

    def write_line(self, values, bold=False, alignment=None, height=None, col_widths=(), number_formats={}, borders=None):
        col_widths = col_widths or self.col_widths
        if borders == 'all':
            border_style = Border(
                left=Side(style='thin', color='FF000000'), right=Side(style='thin', color='FF000000'),
                top=Side(style='thin', color='FF000000'), bottom=Side(style='thin', color='FF000000')
            )
        else:
            border_style = None
        max_lines = 1
        for col_idx, value in enumerate(values, start=1):
            cell = self.ws.cell(row=self.row_idx, column=col_idx)
            if isinstance(value, timedelta):
                cell.number_format = '[h]:mm;@'
            if isinstance(value, date):
                cell.number_format = 'dd.mm.yyyy;@'
            if col_idx in number_formats:
                cell.number_format = number_formats[col_idx]
            cell.value = value
            if bold is True or (bold is not False and col_idx in bold):
                cell.font = self.bold
            if alignment:
                cell.alignment = alignment
            if border_style:
                cell.border = border_style
            if col_widths and len(col_widths) >= col_idx:
                self.ws.column_dimensions[get_column_letter(col_idx)].width = col_widths[col_idx - 1]
            if isinstance(value, str):
                max_lines = max(max_lines, value.count("\n") + 1)
        if height:
            self.ws.row_dimensions[self.row_idx].height = height
        elif max_lines > 1:
            self.ws.row_dimensions[self.row_idx].height = 14 * max_lines
        self.row_idx += 1

    def fill_data(self, generator):
        for row in generator:
            if row and row[0] == 'BOLD':
                self.write_line(row[1:], bold=True, col_widths=self.col_widths)
            else:
                self.write_line(row, col_widths=self.col_widths)

    def add_sheet(self, title):
        self.wb.create_sheet(title)
        self.ws = self.wb[title]
        self.row_idx = 1

    def get_http_response(self, filename):
        with NamedTemporaryFile() as tmp:
            self.wb.save(tmp.name)
            tmp.seek(0)
            response = HttpResponse(tmp, content_type=openxml_contenttype)
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response


class CSVExport:
    def __init__(self, **kwargs):
        self.buff = StringIO()
        self.writer = csv.writer(self.buff, **kwargs)

    def write_line(self, values, **kwargs):
        self.writer.writerow(values)

    def get_http_response(self, filename):
        assert filename.endswith(".csv")
        response = HttpResponse(content=self.buff.getvalue(), content_type='text/csv', charset='cp1252')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response
