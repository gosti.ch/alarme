from django.urls import path

from . import views

urlpatterns = [
    path('partners', views.ClientUpdateView.as_view()),
    path('employees', views.BenevoleUpdateView.as_view()),
]
