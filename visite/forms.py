import calendar
from datetime import date

from dateutil.relativedelta import relativedelta

from django import forms
from django.db.models import Q
from django.utils.dates import MONTHS

from benevole.forms import BenevoleFilterForm as BenevoleFilterFormBase
from benevole.models import Benevole
from client.forms import ClientEditFormBase
from client.models import Client
from common.forms import (
    BootstrapMixin, HMDurationField, ModelForm, NPALocaliteMixin, SplitDateTimeWidget
)
from .models import Attribution, Visite


class ClientEditForm(NPALocaliteMixin, ClientEditFormBase):
    class Meta(ClientEditFormBase.Meta):
        fields = [
            'nom', 'prenom', 'genre', 'date_naissance', 'c_o', 'rue', 'case_postale',
            'npa', 'localite', 'tel_1', 'tel_2', 'courriel', 'langues', 'handicaps',
            'type_logement', 'type_logement_info', 'etage', 'nb_pieces', 'ascenseur',
            'code_entree', 'date_deces', 'remarques_int_visite', 'remarques_ext',
        ]

    benev_hidden_fields = {'code_entree', 'remarques_int_visite'}
    field_order = [
        'nom', 'prenom', 'genre', 'date_naissance', 'rue', 'npalocalite',
    ]


class BenevoleFilterForm(BenevoleFilterFormBase):
    sans_visite = forms.BooleanField(label="Sans visite", required=False)

    def filter(self, benevs):
        benevs = super().filter(benevs)
        if self.cleaned_data['sans_visite']:
            benevs = benevs.filter(num_visites=0)
        return benevs


class AttributionForm(BootstrapMixin, ModelForm):
    client = forms.ModelChoiceField(
        queryset=Client.objects.filter(type_client__contains=['visite'], archive_le__isnull=True).order_by('nom'),
    )
    visiteur = forms.ModelChoiceField(
        queryset=Benevole.objects.par_domaine('visite'),
    )

    class Meta(BootstrapMixin.Meta):
        model = Attribution
        fields = ['client', 'visiteur', 'debut', 'fin', 'motif_fin', 'en_pause', 'remarques']


class VisiteForm(BootstrapMixin, ModelForm):
    client = forms.ModelChoiceField(
        queryset=Client.actifs.filter(type_client__contains=['visite']).order_by('nom')
    )
    visiteur = forms.ModelChoiceField(
        queryset=Benevole.objects.par_domaine('visite'),
    )
    duree = HMDurationField(label="Durée")

    class Meta(BootstrapMixin.Meta):
        model = Visite
        fields = ['client', 'visiteur', 'debut', 'duree', 'compte_rendu']
        widgets = {
            'debut': SplitDateTimeWidget,
        }
        field_classes = {
            'debut': forms.SplitDateTimeField,
        }


class VisiteFilterForm(BootstrapMixin, forms.Form):
    nom_client = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}
        ),
        required=False,
    )
    visiteur = forms.ModelChoiceField(
        queryset=Benevole.objects.par_domaine('visite'),
        required=False,
    )
    mois = forms.ChoiceField(required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['mois'].choices = [('', '--------')] + [
            (f"{dt.month}.{dt.year}", f"{MONTHS[dt.month]} {dt.year}") for dt in [
                date.today() - relativedelta(months=i) for i in range(1, 12)
            ]
        ]

    def filter(self, visites):
        if self.cleaned_data['nom_client']:
            term = self.cleaned_data['nom_client'].split()[0]
            visites = visites.filter(
                Q(client__nom__unaccent__icontains=term) | Q(client__prenom__unaccent__icontains=term)
            )
        if self.cleaned_data['visiteur']:
            visites = visites.filter(visiteur=self.cleaned_data['visiteur'])
        if self.cleaned_data['mois']:
            mois_num, year = [int(part) for part in self.cleaned_data['mois'].split('.')]
            last_day_of_month = calendar.monthrange(year, mois_num)[1]
            visites = visites.filter(
                debut__date__range=(date(year, mois_num, 1), date(year, mois_num, last_day_of_month))
            )
        return visites
