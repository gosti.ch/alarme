from django.apps import apps
from django.contrib import admin
from django.urls import include, path

from two_factor.urls import urlpatterns as tf_urls

from common import views

handler500 = 'common.views.error_view'

urlpatterns = [
    # Overwrite two factor view to redirect authenticated users
    path('account/login/', views.LoginView.as_view(), name='login'),
    path('', include(tf_urls)),

    path('admin/', admin.site.urls),

    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/list/', views.UtilisateurListView.as_view(),
         name='utilisateur-list'),
    path('accounts/list/inactive/', views.UtilisateurListView.as_view(is_active=False),
         name='utilisateur-desactive-list'),
    path('accounts/new/', views.UtilisateurUpdateView.as_view(is_create=True),
         name='utilisateur-add'),
    path('accounts/<int:pk>/edit/', views.UtilisateurUpdateView.as_view(),
         name='utilisateur-edit'),
    path('accounts/<int:pk>/otp-reinit/', views.UtilisateurOtpReinitView.as_view(),
         name='utilisateur-otp-reinit'),

    path('benevoles/', include('benevole.urls')),

    path('media/<path:path>', views.MediaServeView.as_view(), name='media'),

    path('npa/autocomplete/', views.NPALocaliteAutocomplete.as_view(), name='npalocalite-search'),
    path('referent/autocomplete/', views.ReferentFactAutocomplete.as_view(), name='referent-fact-search'),
]

if apps.is_installed('api'):
    urlpatterns.append(path('api/', include('api.urls')))

if apps.is_installed('debug_toolbar'):
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
