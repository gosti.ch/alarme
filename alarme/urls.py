from django.apps import apps
from django.conf import settings
from django.urls import include, path
from django.views.decorators.cache import cache_page, never_cache

from alarme import views, views_stats
from common.views import ManifestView

handler500 = 'common.views.error_view'

urlpatterns = [
    path('', include('common.urls')),
    path('', include('client.urls')),

    path('', views.home, name='home'),
    path('tab-<tabname>', views.home, name='home-with-tab'),
    path('controles/', views.controles, name='controles'),
    path('clients/', views.ClientListView.as_view(), name='clients'),
    path('clients/archives/', views.ClientListView.as_view(is_archive=True), name='clients-archives'),
    path('clients/nouveau/', views.ClientEditView.as_view(is_create=True), name='client-new'),
    path('clients/<int:pk>/edition/', views.ClientEditView.as_view(is_create=False), name='client-edit'),
    path('clients/<int:pk>/donneesmedicales/', views.ClientDonneesMedicView.as_view(), name='client-donneesmedicales'),
    path('clients/<int:pk>/contrat/', views.ClientContratView.as_view(), name='client-contrat'),
    path('clients/<int:pk>/contrat_a_signer/', views.ClientContratSignatureView.as_view(), name='client-contrat-signer'),
    path('clients/<int:pk>/questionnaire/', views.ClientQuestionnaireView.as_view(), name='client-questionnaire'),
    path('clients/<int:pk>/questionnaire_a_signer/', views.ClientQuestionnaireSignatureView.as_view(),
        name='client-questionnaire-signer'),
    path('clients/<int:pk>/courrier/', views.ClientCourrierView.as_view(), name='client-courrier'),
    path('clients/<int:pk>/referent/<int:pk_ref>/courrier/', views.ReferentCourrierView.as_view(),
        name='client-referent-courrier'),

    path('clients/<int:pk>/interventions/', views.ClientMissionsView.as_view(), name='client-interventions'),
    path('clients/<int:pk>/envoicentrale/', views.ClientEnvoiCentraleView.as_view(), name='client-envoicentrale'),
    # Factures des clients
    path('clients/<int:pk>/factures/', views.ClientFacturesView.as_view(), name='client-factures'),
    path('clients/<int:pk>/factures/nouvelle/', views.FactureEditView.as_view(is_create=True), name='facture-new'),
    path('clients/<int:pk>/factures/<int:pk_fact>/edition/', views.FactureEditView.as_view(is_create=False),
        name='facture-edit'),
    path('clients/<int:pk>/factures/<int:pk_fact>/supprimer/', views.FactureDeleteView.as_view(),
        name='facture-delete'),

    path('installation/nouvelle/', views.InstallationEditView.as_view(is_create=True), name='install-new'),
    path('installation/<int:pk>/edition/', views.InstallationEditView.as_view(is_create=False), name='install-edit'),
    path('installation/<int:pk>/close/', views.InstallationCloseView.as_view(), name='install-close'),
    path('installation/<int:pk>/close/cancel/', views.InstallationCloseCancelView.as_view(),
        name='install-close-cancel'),
    path('clients/<int:client_pk>/materielclient/nouveau/', views.MaterielClientEditView.as_view(is_create=True), name='materielclient-new'),
    path('materielclient/<int:pk>/edition/', views.MaterielClientEditView.as_view(is_create=False), name='materielclient-edit'),

    path('clients/<int:pk>/mission/nouvelle/', views.MissionEditView.as_view(is_create=True), name='mission-new'),
    path('mission/<int:pk>/supprimer/', views.MissionDeleteView.as_view(), name='mission-delete'),
    path('mission/<int:pk>/edition/', views.MissionEditView.as_view(is_create=False), name='mission-edit'),

    # Facturation
    path('facturation/', views.FacturationView.as_view(), name='facturation'),
    path('facturation/articles/', views.ArticlesView.as_view(), name='articles'),
    path('facturation/mensuelles/<int:year>/<int:month>/', views.FacturerMoisView.as_view(), name='facturation-mensuelle'),
    path('facturation/factures/', views.FactureListView.as_view(), name='factures'),
    path('facturation/factures/<int:year>/<int:month>/', views.FacturesMoisView.as_view(), name='factures-mois'),
    path('facturation/non_transmises/', views.FacturesNonTransmisesView.as_view(), name='factures-non-transmises'),
    path('facturation/defraiements/', never_cache(views.DefraiementView.as_view()), name='defraiements'),
    path('facturation/defraiements/export/<int:year>/<int:month>/', never_cache(views.DefraiementExportView.as_view()),
        name='defraiements-export'),

    # Bénévoles
    path('benevole/<int:pk>/interventions/', views.BenevoleMissionsView.as_view(), name='benevole-interventions'),
    path('benevole/<int:pk>/interventions/nouvelle/', views.BenevoleMissionEditView.as_view(is_create=True),
         name='benevole-intervention-new'),
    path('benevole/intervention/<int:pk>/edition/', views.BenevoleMissionEditView.as_view(is_create=False),
         name='benevole-intervention-edit'),
    path('benevole/<int:pk>/stock/', views.BenevoleStockView.as_view(), name='benevole-stock'),
    path('benevole/<int:pk>/visites/', views.BenevoleVisitesView.as_view(), name='benevole-visites'),

    # Alarmes et matériel
    path('materiel/', views.MaterielIndexView.as_view(), name='materiel'),
    path('alarmes/', views.AlarmeListView.as_view(), name='alarmes'),
    path('alarmes/nouvelle/', views.AlarmeEditView.as_view(is_create=True), name='alarme-new'),
    path('alarmes/<int:pk>/edition/', views.AlarmeEditView.as_view(is_create=False), name='alarme-edit'),
    path('alarmes/<int:pk>/archive/', views.AlarmeArchiveView.as_view(), name='alarme-archive'),
    path('alarmes/<int:pk>/journal/', views.AlarmeJournalView.as_view(), name='alarme-journal'),
    path('alarmes/search/', views.AlarmeSearchView.as_view(), name='alarme-search'),
    path('alarmes/json/', views.AlarmeJSONView.as_view(), name='alarme-json'),
    path('modelealarme/nouveau/', views.ModeleAlarmeEditView.as_view(is_create=True), name='modelealarme-new'),

    path('materiel/liste/', views.MaterielListView.as_view(), name='materiel-liste'),
    path('materiel/nouvelle/', views.MaterielEditView.as_view(is_create=True), name='materiel-new'),
    path('materiel/<int:pk>/edition/', views.MaterielEditView.as_view(is_create=False), name='materiel-edit'),
    path('materiel/<int:pk>/archive/', views.MaterielArchiveView.as_view(), name='materiel-archive'),
    path('typemateriel/nouveau/', views.TypeMaterielEditView.as_view(is_create=True), name='typemat-new'),
    path('materiel/search/', views.MaterielSearchView.as_view(), name='materiel-search'),

    path('refresh-select/<str:modelname>/', views.RefreshSelectView.as_view(), name='refresh-select'),

    # Statistiques
    path('stats/', views_stats.StatsInstallView.as_view(), name='stats-index'),
    path('stats/samas/<int:year>/<int:month>/', views_stats.ClientsSamaView.as_view(), name='stats-list-samas'),
    path('stats/installs/<int:year>/<int:month>/', views_stats.NewInstallsView.as_view(), name='stats-list-installs'),
    path('stats/uninstalls/<int:year>/<int:month>/', views_stats.UninstallsView.as_view(), name='stats-list-uninstalls'),
    path('stats/benevoles/', views_stats.StatsBenevView.as_view(), name='stats-benevoles'),

    path('', include(f'{settings.CANTON_APP}.urls')),

    path('app/', views.home_benevoles, name='home-app'),
    path('app/manifest.json',
         cache_page(3600 * 24)(ManifestView.as_view(template_name="alarme/manifest.json")),
         name='manifest'
    ),
    path('app/', include('client.urls')),
    path('app/clients/<int:pk>/edition/', views.ClientEditView.as_view(is_create=False), name='benevole-client-edit'),
    path('app/attribuer/<int:interv_pk>/', views.BenevoleAttributionView.as_view(), name='benevole-attribuer'),
    path('app/mission/sansclient/nouvelle/', views.BenevoleMissionEditView.as_view(is_create=True, par_benevole=True),
         name='benevole-mission-new'),
    path('app/mission/sansclient/<int:pk>/edition/',
         views.BenevoleMissionEditView.as_view(is_create=False, par_benevole=True),
         name='benevole-mission-sans-client-edit'),
    path('app/mission/<int:pk>/edition/', views.MissionEditView.as_view(is_create=False, par_benevole=True),
         name='benevole-mission-edit'),
    path('app/alarmes/search/', views.AlarmeSearchView.as_view(from_benev_stock=True), name='benevole-alarme-search'),
    path('app/clients/<int:pk>/visite/nouvelle/', views.BenevoleVisiteNew.as_view(), name='benevole-visite-new'),
    path('app/archives/', views.BenevoleArchivesView.as_view(), name='benevole-archives'),
    path('app/archives/<int:year>/<int:month>/', views.BenevoleArchivesMonthView.as_view(),
        name='benevole-archives-month'),
]

if apps.is_installed('besoins'):
    urlpatterns.append(
        path('besoins/', include('besoins.urls_app'))
    )
