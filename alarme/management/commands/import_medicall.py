from datetime import date
from pathlib import Path

from openpyxl import load_workbook

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from alarme.models import (
    Alarme, Client, Installation, ModeleAlarme,
    Professionnel, Referent, ReferentTel,
)

LANG_MAPPING = {
    'Französisch': 'fr',
    'Français': 'fr',
    'Francese': 'fr',
    'Deutsch': 'de',
    'Allemand': 'de',
    'Italienisch': 'it',
    'Italien': 'it',
    'Italiano': 'it',
    'Spanisch': 'es',
    'Englisch': 'en',
}


class Command(BaseCommand):
    """A command to read xlsx file of Medicall export data."""
    def add_arguments(self, parser):
        parser.add_argument('path')

    def handle(self, **options):
        file_path = Path(options['path'])
        if not file_path.exists():
            raise CommandError(f"File {file_path} does not exist.")
        wb = load_workbook(file_path)
        ws = wb.active
        headers = []
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                self.import_client(values, idx - 1)
        self.stdout.write(f"{idx} clients importés")

    def import_client(self, line, idx):
        # Ignored? Teilnehmer: Schlüsselnummer
        client = Client(
            no_centrale=f'MC{idx}',  # ask for number in file?
            nom=line['Teilnehmer: Name'], prenom=line['Teilnehmer: Vorname'],
            rue=line['Teilnehmer: Straße'],
            npa=line['Teilnehmer: PLZ'], localite=line['Teilnehmer: Wohnort'],
            genre=read_genre(line['Teilnehmer: Geschlecht']),
            tel_1=line['Teilnehmer: Telefonnummer1'],
            tel_2=line['Teilnehmer: Telefonnummer2'],
            date_naissance=line['Teilnehmer: Geburtsdatum'],
            situation_vie=line['Teilnehmer: Familienstand'],
            cles=line['Teilnehmer: Schlüsselaufbewahrung'],
            remarques=line['Teilnehmer: Bemerkung'],
            etage=line['Teilnehmer: Zugang zur Wohnung'],
            type_logement=line['Teilnehmer: Bemerkung Wohnung'],
            langues=read_langs(line['Teilnehmer: Sprache']),
        )
        try:
            client.full_clean()
        except ValidationError as err:
            print(err)
            print(client.__dict__)
            import pdb; pdb.set_trace()
            return
        client.save()

        model, _ = ModeleAlarme.objects.get_or_create(nom=line['Gerät1Gerät: Gerätetyp'])
        alarme = Alarme(
            modele=model, centrale='medicall',
            no_appareil=line['Gerät1Gerät: Identnummer'],
            no_serie=line['Gerät1Gerät: Seriennnummer'] or '',
            carte_sim=line['Gerät1Gerät: Telefonnummer'],
        )
        alarme.full_clean()
        alarme.save()
        Installation.objects.create(client=client, alarme=alarme, date_debut=date(2000, 1, 1))

        for no in range(1, 7):
            if not line[f'Bezugspersonen{no}: Name']:
                continue
            referent = Referent(
                client=client,
                nom=line[f'Bezugspersonen{no}: Name'],
                rue=line[f'Bezugspersonen{no}: Straße'],
                npa=str((line[f'Bezugspersonen{no}: PLZ'] or '')).replace('F-', ''),
                localite=line[f'Bezugspersonen{no}: Wohnort'],
                relation=line[f'Bezugspersonen{no}: Verhältnis'],
                remarque=line[f'Bezugspersonen{no}: Bemerkung'],
            )
            type_aide = line[f'Bezugspersonen{no}: Art der Hilfe']
            if 'Notfallinfo' in type_aide or '7h00 - 22h' in type_aide:
                referent.referent = int(line[f'Bezugspersonen{no}: Reihenfolge'])
            if 'KP' in type_aide or 'Kontaktperson' in type_aide:
                referent.repondant = int(line[f'Bezugspersonen{no}: Reihenfolge'])
            if 'Samaritains' in type_aide:
                client.samaritains = True
                client.save()
            try:
                referent.full_clean()
            except ValidationError:
                import pdb; pdb.set_trace()
            referent.save()
            for tel_pos in range(1, 7):
                no_tel = line[f'Bezugspersonen{no}: Telefon{tel_pos}']
                if not no_tel:
                    continue
                ReferentTel.objects.create(
                    referent=referent,
                    tel=no_tel,
                    priorite=tel_pos,
                )

        for no in range(1, 10):
            typ = line[f'Prof. Hilfspersonen{no}: Art der Hilfe']
            if typ != 'Arzt / Ärztin':
                continue
            prof, _ = Professionnel.objects.get_or_create(
                nom=line[f'Prof. Hilfspersonen{no}: Name'],
                defaults={
                    'type_pro': 'Médecin',
                    'rue': line[f'Prof. Hilfspersonen{no}: Straße'],
                    'npa': line[f'Prof. Hilfspersonen{no}: PLZ'],
                    'localite': line[f'Prof. Hilfspersonen{no}: Wohnort'],
                    'tel': line[f'Prof. Hilfspersonen{no}: Telefon1'],
                    'remarque': line[f'Prof. Hilfspersonen{no}: Bemerkung'],
                }
            )
            client.professionnels.add(prof)


def read_genre(genre):
    return {'W': 'F', 'M': 'M'}.get(genre, '')


def read_langs(langs):
    return [code for key, code in LANG_MAPPING.items() if key in langs]
