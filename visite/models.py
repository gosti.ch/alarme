from django.db import models

from client.models import Client
from benevole.models import Benevole


class Attribution(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='attributions')
    visiteur = models.ForeignKey(Benevole, on_delete=models.PROTECT)
    debut = models.DateField("Date début des visites")
    fin = models.DateField(null=True, blank=True)
    en_pause = models.BooleanField("En pause", default=False)
    motif_fin = models.TextField("Motif d’arrêt", blank=True)
    remarques = models.TextField("Remarques", blank=True)

    def __str__(self):
        return f"{self.visiteur} est visiteur/euse pour {self.client}"


class Visite(models.Model):
    client = models.ForeignKey(Client, on_delete=models.PROTECT, related_name='visites')
    visiteur = models.ForeignKey(Benevole, on_delete=models.PROTECT)
    debut = models.DateTimeField("Début")
    duree = models.DurationField("Durée")
    compte_rendu = models.TextField("Compte-rendu", blank=True)

    def __str__(self):
        return f"Visite de {self.visiteur} à {self.client}, le {self.debut.strftime('%d.%m.%Y')}"
