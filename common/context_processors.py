from django.apps import apps
from django.conf import settings

from .utils import current_app

besoins_installed = apps.is_installed('besoins')

def common(request):
    return {
        'current_app': current_app(),
        'besoins_installed': besoins_installed,
        'main_logo': settings.MAIN_LOGO,
        'app_logo': settings.APP_LOGO,
        'test_instance': settings.TEST_INSTANCE,
    }
