from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0010_alerte'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='contact_visites',
            field=models.BooleanField(default=False, verbose_name='Contact pour les visites'),
        ),
    ]
