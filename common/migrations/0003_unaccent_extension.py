from django.db import migrations
from django.contrib.postgres.operations import UnaccentExtension


class Migration(migrations.Migration):

    dependencies = [
        ("common", "0002_rename_utilisateur_table"),
    ]

    operations = [UnaccentExtension()]
