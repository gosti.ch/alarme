from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '__first__'),
        ('transport', '0042_alter_frais_typ'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrajetCommun',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chauffeur', models.ForeignKey(
                    blank=True, null=True, on_delete=models.deletion.PROTECT,
                    related_name='transports_communs', to='benevole.benevole'
                )),
                ('date', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='trajet',
            name='commun',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=models.deletion.SET_NULL, to='transport.trajetcommun'
            ),
        ),
    ]
