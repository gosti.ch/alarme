from datetime import date, datetime, timedelta
from decimal import Decimal
from functools import partial
from io import BytesIO
from itertools import groupby
from operator import attrgetter

from dateutil import relativedelta
from openpyxl.styles import Alignment

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.views import RedirectURLMixin
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.mail import send_mail
from django.core.serializers import serialize
from django.db import transaction
from django.db.models import Count, DateField, F, Min, Prefetch, Q, Sum
from django.db.models.functions import Trunc, TruncMonth
from django.http import (
    HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
)
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.utils.module_loading import import_string
from django.utils.text import slugify
from django.views.generic import (
    CreateView, DeleteView, FormView, TemplateView, UpdateView, View,
)

from benevole.models import Benevole, NoteFrais
from client.models import (
    AdressePresence, Alerte, Client, ClientType, Journal, Referent
)
from client.views import (
    ClientAccessCheckMixin, ClientEditViewBase, ClientJournalMixin, ClientListViewBase
)
from common.export import OpenXMLExport
from common.forms import MoisForm
from common.models import Fichier
from common.utils import arrondi_5, canton_abrev
from common.views import (
    BasePDFView, BenevoleMixin, CreateUpdateView, ExportableMixin, FilterFormMixin,
    JournalMixin, ListView
)
from . import forms
from . import models
from .pdf import QuestionnairePDF


def home(request, tabname=None):
    if request.user.is_benevole:
        return home_benevoles(request)
    if not request.user.has_perm('alarme.view_alarme'):
        raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour voir cette page.")
    context = {
        'nouveaux': {
            'clients': Client.objects.annotate(
                num_interv=Count('mission'), num_installs=Count('installation'),
            ).filter(
                type_client__contains=['alarme'], archive_le__isnull=True, num_interv=0, num_installs=0
            ),
            'missions': models.Mission.objects.filter(
                client__isnull=False, effectuee__isnull=True, type_mission__code='NEW',
            ).order_by('delai').select_related('client', 'type_mission', 'benevole'),
        },
        'missions': models.Mission.objects.filter(
            client__isnull=False, effectuee__isnull=True
        ).exclude(type_mission__code='NEW').order_by('delai'
        ).select_related('client', 'type_mission', 'benevole'),
        'mat_en_attente': models.Installation.objects.filter(
            date_fin_abo__lte=date.today(), retour_mat__isnull=True
        ).select_related('client').order_by('date_fin_abo'),
        'alertes': list(Alerte.objects.filter(
            cible='alarme', traite_le__isnull=True
        ).select_related('client').order_by('recu_le')),
        'absences': AdressePresence.objects.filter(
            Q(adresse__client__type_client__contains=['alarme']) &
            Q(depuis__lte=date.today()) &
            (Q(jusqua__isnull=True) | Q(jusqua__gt=date.today()))
        ).order_by('dernier_contact'
        ).select_related('adresse__client'),
        'current_tab': tabname if tabname in ('interventions', 'alertes', 'absences', 'materiel') else 'interventions',
    }
    hospits = Client.actifs.all().hospitalises().values_list('pk', flat=True)
    for alerte in context['alertes']:
        alerte.client_hospitalise = alerte.client_id in hospits
    context['alertes_has_rems'] = any(al.remarque for al in context['alertes'])
    context['nbre_nouveaux'] = len(context['nouveaux']['clients']) + len(context['nouveaux']['missions'])
    if request.user.has_perm('alarme.view_facture'):
        context['factures'] = models.Facture.non_transmises().count()
    return render(request, 'index.html', context=context)


def home_benevoles(request):
    benevole = request.user.benevole
    visites = benevole.clients_a_visiter().prefetch_related(
        Prefetch('mission_set', to_attr='interv_en_cours', queryset=models.Mission.objects.filter(effectuee__isnull=True)),
        Prefetch('installation_set', queryset=models.Installation.objects.select_related(
            'alarme', 'alarme__modele', 'abonnement'
        )),
    ).order_by(F('derniere').asc(nulls_first=True), 'nom', 'prenom')
    clients_a_visiter = []
    afaire = 0
    for client in visites:
        typ_en_cours = set([interv.type_mission.code for interv in client.interv_en_cours])
        if not client.interv_en_cours:
            client.href = reverse('benevole-visite-new', args=[client.pk])
        elif typ_en_cours != {'VISITE'}:
            # Une autre intervention est pendante, ignorer ce client.
            continue
        elif client.interv_en_cours[0].benevole != benevole:
            # Une visite est planifiée avec un autre bénévole
            continue
        else:
            client.planifiee_le = client.interv_en_cours[0].planifiee
            client.href = reverse('benevole-mission-edit', args=[client.interv_en_cours[0].pk])
        if (
            not client.date_resiliation() and (
                client.derniere is None or
                (date.today() - client.derniere) > timedelta(days=settings.DELAI_ENTRE_VISITES)
            )
        ):
            client.afaire = True
            afaire += 1
        else:
            client.afaire = False
        client.next_interv = client.derniere + timedelta(days=182) if client.derniere else None
        clients_a_visiter.append(client)
    context = {
        'missions_ouvertes': models.Mission.objects.filter(
            effectuee__isnull=True, benevole__isnull=True).order_by('delai'
        ).select_related('client', 'type_mission'),
        'mes_missions': benevole.mission_set.filter(effectuee=None),
        'visites': clients_a_visiter,
        'visites_a_faire': afaire,
    }
    return render(request, 'benevoles/index.html', context=context)


def controles(request):
    context = {
        'clients_sans_visiteurs': Client.objects.actifs_entre(
            date.today(), date.today() + timedelta(days=200)
        ).filter(visiteur=None).order_by('nom'),
        'courriers_non_envoyes': Client.objects.annotate(
            non_envoi=Count('referent', filter=Q(
                referent__repondant__isnull=False, referent__courrier_envoye__isnull=True,
                referent__date_archive__isnull=True,
            )),
            num_installs=Count('installation'),
        ).filter(
            type_client__contains=['alarme'], archive_le=None, non_envoi__gt=0, num_installs__gt=0,
        ).order_by('nom'),
    }
    return render(request, 'alarme/controles.html', context=context)


class RefreshSelectView(View):
    def get(self, *args, **kwargs):
        model = {
            'ModeleAlarme': models.ModeleAlarme,
            'TypeMateriel': models.TypeMateriel,
        }.get(kwargs['modelname'])
        return HttpResponse('\n'.join(
            ['<option value="" selected>---------</option>'] +
            [f'<option value="{item.pk}">{item.nom}</option>' for item in model.objects.all().order_by('nom')]
        ))


class ClientListView(ClientListViewBase):
    template_name = 'alarme/client_list.html'
    filter_formclass = forms.ClientFilterForm
    client_types = ['alarme']
    return_all_if_unbound = False

    def get_queryset(self):
        clients = super().get_queryset()
        return clients.annotate(
            date_tout_debut=Min('installation__date_debut')
        ).prefetch_related(Prefetch(
            'installation_set', to_attr='installations',
            queryset=models.Installation.objects.filter(date_fin_abo__isnull=True).select_related(
                'alarme', 'alarme__modele', 'abonnement'
            )
        ))

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "base.html",
            'home_url': reverse('home'),
            'client_url_name': 'client-edit',
        }


class ClientEditView(ClientEditViewBase):
    form_class = forms.ClientForm
    template_name = 'alarme/client.html'
    client_type = 'alarme'

    def get_queryset(self):
        return self.model._default_manager.all().annotate(
            date_tout_debut=Min('installation__date_debut')
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='base.html')
        if not self.is_create:
            context.update({
                'install': self.object.derniere_alarme(),
                'materiels': self.object.materiel_actuel(),
                'missions': self.object.mission_set.filter(effectuee__isnull=True),
            })
        return context


class ClientDonneesMedicView(ClientAccessCheckMixin, ClientJournalMixin, UpdateView):
    model = Client
    form_class = forms.DonneesMedicForm
    template_name = 'alarme/general_edit.html'
    journal_edit_message = "Modification des données médicales ({fields})"
    is_create = False

    def get_client(self):
        return self.object

    def form_valid(self, form):
        with transaction.atomic():
            super().form_valid(form)
            self.journalize(form, changed_values=True)
        return JsonResponse({'result': 'OK', 'reload': ''})


def get_appareil_abo(client):
    """Renvoi d'un tuple (appareil, abonnement) pour le client, ou (None, None)."""
    # Utiliser installation actuelle, le cas échéant
    install = client.alarme_actuelle()
    if install:
        return (install.alarme, install.abonnement)
    # Sinon, chercher info dans mission d'installation
    mission = client.mission_set.filter(type_mission__code='NEW').latest('delai')
    if mission:
        return (mission.alarme, mission.abonnement)
    # Sinon, renvoyer None
    return (None, None)


class ClientContratView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = '%(CANTON_APP)s.pdf.ContratPDF'

    def get(self, request, *args, **kwargs):
        appareil, abo = get_appareil_abo(self.client)
        if appareil is None:
            messages.error(request, "Il n'y a pas encore d’appareil attribué à ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        self.produce_kwargs = {'appareil': appareil, 'abo': abo}
        if request.GET.get('forsign') == 'yes':
            self.produce_kwargs['pour_signature'] = True
        return super().get(request, *args, as_attachment=request.GET.get('attach') != 'no', **kwargs)


class ClientContratSignatureView(ClientAccessCheckMixin, TemplateView):
    pdf_class = '%(CANTON_APP)s.pdf.ContratPDF'
    template_name = 'alarme/contrat_a_signer.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        self.appareil, self.abo = get_appareil_abo(self.client)
        if self.appareil is None:
            messages.error(request, "Il n'y a pas encore d’appareil attribué à ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.read_only:
            messages.error(request, "Vous n'avez pas les droits d'édition pour ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        sig_form = forms.ContratForm(data=request.POST)
        if not sig_form.is_valid():
            messages.error(request, "Une erreur a été détectée dans l’envoi de la signature, veuillez réessayer")
            return self.render_to_response(self.get_context_data())
        # Produire le contrat PDF avec la signature PNG dans sig_data.
        ContratPDF = import_string(self.pdf_class % {'CANTON_APP': settings.CANTON_APP})
        pdf_temp = BytesIO()
        pdf = ContratPDF(pdf_temp)
        pdf.produce(
            self.client, appareil=self.appareil, abo=self.abo,
            signature_client=sig_form.cleaned_data['sig_data_cl'],
            signature_cr=sig_form.cleaned_data['sig_data_cr'],
            signature_lieu=sig_form.cleaned_data['lieu'],
            signature_cr_nom=f"{request.user.last_name} {request.user.first_name}",
        )
        # Enregistrer le contrat signé dans media/...
        saved_name = default_storage.save(
            f"clients/contrat_{slugify(self.client.nom_prenom)}_{date.today().strftime('%Y_%m_%d')}.pdf",
            pdf_temp
        )
        # Lier au client par un fichier
        Fichier.objects.create(
            content_object=self.client, titre=f"Contrat signé du {date.today().strftime('%d.%m.%Y')}",
            typ='contrat', fichier=saved_name, qui=request.user, quand=timezone.now()
        )
        messages.success(request, "Le contrat a bien été signé et enregistré")
        return HttpResponseRedirect(self.client.get_absolute_url())

    def get_context_data(self, **kwargs):
        return super().get_context_data(client=self.client, **kwargs)


class ClientQuestionnaireView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = 'alarme.pdf.QuestionnairePDF'

    def get(self, request, *args, **kwargs):
        client = self.get_object()
        try:
            install_mission = client.mission_set.filter(type_mission__code='NEW').latest('delai')
        except models.Mission.DoesNotExist:
            install_mission = None
        self.produce_kwargs = {
            'install_mission': install_mission,
            'pour_signature': request.GET.get('forsign') == 'yes',
        }
        return super().get(request, *args, as_attachment=request.GET.get('attach') != 'no', **kwargs)


class ClientQuestionnaireSignatureView(ClientAccessCheckMixin, TemplateView):
    template_name = 'alarme/questionnaire_a_signer.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        try:
            self.install_mission = self.client.mission_set.filter(type_mission__code='NEW').latest('delai')
        except models.Mission.DoesNotExist:
            messages.error(request, "Il n'y a pas encore d’intervention d’installation prévue pour ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.read_only:
            messages.error(request, "Vous n'avez pas les droits d'édition pour ce client")
            return HttpResponseRedirect(self.client.get_absolute_url())
        sig_form = forms.QuestionnaireForm(data=request.POST)
        if not sig_form.is_valid():
            messages.error(request, "Une erreur a été détectée dans l’envoi de la signature, veuillez réessayer")
            return self.render_to_response(self.get_context_data())
        # Produire le questionnaire PDF avec la signature PNG dans sig_data.
        pdf_temp = BytesIO()
        pdf = QuestionnairePDF(pdf_temp)
        pdf.produce(
            self.client, install_mission=self.install_mission,
            signature_file=sig_form.cleaned_data['sig_data'],
            signature_lieu=sig_form.cleaned_data['lieu'],
        )
        # Enregistrer le questionnaire signé dans media/...
        saved_name = default_storage.save(
            f"clients/questionnaire_{slugify(self.client.nom_prenom)}_{date.today().strftime('%Y_%m_%d')}.pdf",
            pdf_temp
        )
        # Lier au client par un fichier
        Fichier.objects.create(
            content_object=self.client, titre=f"Questionnaire signé du {date.today().strftime('%d.%m.%Y')}",
            typ='quest', fichier=saved_name, qui=request.user, quand=timezone.now()
        )
        messages.success(request, "Le questionnaire a bien été signé et enregistré")
        return HttpResponseRedirect(self.client.get_absolute_url())

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            client=self.client,
            cr_canton={
                'JU': 'Croix-Rouge suisse Canton du Jura',
                'NE': 'Croix-Rouge neuchâteloise',
            }.get(canton_abrev()),
            **kwargs
        )


class ClientCourrierView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Client
    pdf_class = 'alarme.pdf.CourrierPDF'


class ReferentCourrierView(ClientAccessCheckMixin, BasePDFView):
    obj_class = Referent
    pk_url_kwarg = 'pk_ref'
    pdf_class = 'alarme.pdf.CourrierReferentPDF'


class ClientMissionsView(ListView):
    model = models.Mission
    template_name = 'alarme/client_missions.html'
    paginate_by = 25

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(
            client=self.client
        ).annotate(frais_total=Sum('frais__cout')).order_by('-effectuee', '-delai')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientEnvoiCentraleView(FormView):
    form_class = forms.EnvoiCentraleForm
    template_name = 'alarme/apercu_envoi.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour envoyer des données.")
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        install = self.client.derniere_alarme()
        try:
            mission = self.client.mission_set.filter(effectuee=None).earliest('delai')
        except models.Mission.DoesNotExist:
            mission = None
        if install:
            alarme = install.alarme
        elif mission:
            alarme = mission.alarme
        else:
            alarme = None
        return default_centrale_message(self.client, alarme, mission)

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'recipient': settings.CENTRALE_EMAIL,
        }

    def form_valid(self, form):
        envoi_centrale(self.request, form.cleaned_data['subject'], form.cleaned_data['body'])
        Journal.objects.create(
            client=self.client, description=f"Envoi des données du jour à {settings.CENTRALE_EMAIL}",
            details=(
                f"<b>Destinataire:</b> {settings.CENTRALE_EMAIL}\n"
                f"<b>Sujet:</b> {form.cleaned_data['subject']}\n"
                f"<b>Message:</b>\n{form.cleaned_data['body']}"
            ),
            quand=timezone.now(), qui=self.request.user
        )
        messages.success(self.request, f"Le message a bien été envoyé à {settings.CENTRALE_EMAIL}")
        return HttpResponseRedirect(reverse('client-edit', args=[self.client.pk]))


class ClientFacturesView(ClientAccessCheckMixin, ListView):
    model = models.Facture
    paginate_by = 25
    template_name = 'alarme/client_factures.html'

    def get_queryset(self):
        return super().get_queryset().filter(client=self.client).order_by('-date_facture')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'trans_url': settings.FACTURES_TRANSMISSION_URL,
        }


class FactureEditView(ClientJournalMixin, CreateUpdateView):
    model = models.Facture
    form_class = forms.FactureForm
    pk_url_kwarg = 'pk_fact'
    template_name = 'alarme/facture_edit.html'
    journal_edit_message = "Modification d’une facture ({fields})"
    json_response = True

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_client(self):
        return self.client

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }

    def form_valid(self, form):
        form.instance.client = self.client
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})

    def journalize(self, form, add_message=None, **kwargs):
        add_msg = (
            f"Création d’une facture de CHF {form.instance.montant} "
            f"du {form.instance.date_facture.strftime('%d.%m.%Y')} ({form.instance.libelle})"
        )
        super().journalize(form, add_message=add_msg, **kwargs)


class FactureDeleteView(DeleteView):
    model = models.Facture
    pk_url_kwarg = 'pk_fact'

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.object.delete()
        Journal.objects.create(
            client=self.object.client,
            description=f"Suppression de la facture du {self.object.date_facture} ({ self.object.montant})",
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class FacturationView(PermissionRequiredMixin, TemplateView):
    template_name = 'alarme/facturation.html'
    permission_required = 'alarme.view_facture'

    def get_context_data(self, **kwargs):
        mois_factures = models.Facture.objects.filter(
            mois_facture__isnull=False
        ).annotate(
            month=TruncMonth('mois_facture'),
        ).values('month').annotate(num_fact=Count('id')).order_by('-month')
        mois_a_facturer = None
        if settings.FACTURES_TRIMESTRIELLES:
            if date.today().month in (3, 6, 9, 12):
                # filtrer sur nbre de facture > 50 pour détecter un mois complet facturé
                last_mois = mois_factures.values('month').annotate(num_fact=Count('id')).filter(num_fact__gt=50).order_by('-month').first()
                mois_suivant = date.today().replace(day=1) + relativedelta.relativedelta(months=1)
                if last_mois and mois_suivant > last_mois['month']:
                    mois_a_facturer = mois_suivant
        else:
            # Mois précédent
            mois_a_facturer = date.today().replace(day=1) - relativedelta.relativedelta(months=1)
        return {
            **super().get_context_data(**kwargs),
            'mois_factures': mois_factures.values_list('month', flat=True).order_by('-month'),
            'mois_a_facturer': mois_a_facturer,
            'fact_trimestre': settings.FACTURES_TRIMESTRIELLES,
            'non_transmises': models.Facture.non_transmises().count(),
        }


class ArticlesView(TemplateView):
    template_name = 'alarme/articles.html'

    def get_context_data(self, **kwargs):
        return {'articles': models.ArticleFacture.objects.all().order_by('code')}


class FacturerMoisView(PermissionRequiredMixin, FormView):
    form_class = forms.FacturerForm
    template_name = 'alarme/general_edit.html'
    permission_required = 'alarme.add_facture'

    def get_initial(self):
        return {**super().get_initial(), 'date_facture': date.today()}

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'form_classes': 'unmonitor normal_submit'}

    def form_valid(self, form):
        dt = form.cleaned_data['date_facture']
        # Existing factures not sent to ERP, change date_facture to dt
        models.Facture.objects.filter(exporte__isnull=True).update(date_facture=dt)
        mois = date(self.kwargs['year'], self.kwargs['month'], 1)
        try:
            with transaction.atomic():
                num_fact = models.Installation.generer_factures(mois, date_factures=dt)
                num_fact += models.MaterielClient.generer_factures(mois, date_factures=dt)
        except Exception as err:
            messages.error(self.request, f"Désolé, une erreur s’est produite durant la facturation: {err}")
        else:
            messages.success(self.request, f"{num_fact} factures ont été générées pour le mois de {django_format(mois, 'F Y')}")
        return HttpResponseRedirect(reverse('facturation'))


class FactureListView(PermissionRequiredMixin, FilterFormMixin, ExportableMixin, ListView):
    model = models.Facture
    filter_formclass = forms.FactureFilterForm
    paginate_by = 25
    template_name = 'alarme/factures.html'
    permission_required = 'alarme.view_facture'
    col_widths = [18, 45, 50, 40, 10]

    def get_queryset(self, **kwargs):
        return super().get_queryset(**kwargs).select_related('client', 'install__abonnement'
            ).order_by('-date_facture', 'client__nom')

    def export_lines(self, context):
        yield ['BOLD', 'Date de facture', 'Client', 'Libellé', 'Article', 'Montant']
        for facture in self.get_queryset():
            yield [
                facture.date_facture, str(facture.client), facture.libelle,
                str(facture.article), facture.montant
            ]

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'title': 'Factures',
        }


class FacturesNonTransmisesView(FactureListView):
    def get_queryset(self):
        return super().get_queryset(base_qs=self.model.non_transmises())

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'csv_export': settings.FACTURES_EXPORT_URL,
            'api_export': settings.FACTURES_TRANSMISSION_URL,
            'title': 'Factures non transmises à la compta',
        }


class FacturesMoisView(FactureListView):
    def get_queryset(self):
        return super().get_queryset().annotate(
            month=Trunc('mois_facture', 'month', output_field=DateField())
        ).filter(
            month=date(self.kwargs['year'], self.kwargs['month'], 1)
        ).order_by('client__nom')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'title': 'Factures mensuelles de ' + django_format(date(self.kwargs['year'], self.kwargs['month'], 1), 'F Y'),
        }


class DefraiementView(PermissionRequiredMixin, ListView):
    template_name = 'alarme/defraiements.html'
    permission_required = 'alarme.view_facture'

    def get(self, request, *args, **kwargs):
        form_data = {
            'year': request.GET.get('year', str(date.today().year)),
            'month': request.GET.get('month', str(date.today().month)),
        }
        self.form = MoisForm(data=form_data)
        self.form.full_clean()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        self.un_du_mois = date(int(self.form.cleaned_data['year']), int(self.form.cleaned_data['month']), 1)
        return models.Mission.frais_mensuels(self.un_du_mois)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        totaux = {'frais_sum': Decimal('0'), 'kms': Decimal('0'), 'num_visites': 0, 'montant_total': Decimal('0')}
        notes_frais = {
            nt.benevole_id: nt for nt in NoteFrais.objects.filter(service='alarme', mois=self.un_du_mois)
        }
        for line in context['object_list']:
            line['montant_total'] = (
                line['frais_repas'] + line['frais_autre'] + (
                    arrondi_5(line['kms'] * settings.TARIF_CHAUFFEUR[0])
                ) + models.Frais.PART_FRAIS_TEL
            )
            line['note_frais'] = notes_frais.get(line['benevole__id'])
            line['frais_sum'] = line['frais_repas'] + line['frais_autre']
            totaux['frais_sum'] += line['frais_sum']
            totaux['kms'] += line['kms']
            totaux['num_visites'] += line['num_visites']
            totaux['montant_total'] += line['montant_total']
        context.update({
            'date_form': self.form,
            'totaux': totaux,
        })
        if settings.NOTESFRAIS_TRANSMISSION_URLNAME:
            context['transmission_url'] = reverse(
                settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[self.un_du_mois.year, self.un_du_mois.month]
            )
        return context


class DefraiementExport(OpenXMLExport):
    default_font_size = 12


class DefraiementExportView(View):
    headers = [
        'Bénévole',
        'Mois de la prestation',
        '606 - Indemnités effectives - Frais divers (CHF)',
        '621 Frais de déplacements – Nb km',
        '633 Participation aux frais téléphone bénévole',
        'Montant à défrayer (y.c. frais + km)',
    ]

    def post(self, request, *args, **kwargs):
        tarif_km = settings.TARIF_CHAUFFEUR[0]
        un_du_mois = date(year=kwargs['year'], month=kwargs['month'], day=1)
        export = DefraiementExport(col_widths=[43, 12, 8, 8, 8, 15])
        export.write_line(self.headers, bold=True, alignment=Alignment(text_rotation=90), height=270, borders='all')
        export.ws.freeze_panes = export.ws['B2']
        export.ws.print_title_rows = '1:1'
        frais_par_benevole = models.Mission.frais_mensuels(un_du_mois)

        for idx, line in enumerate(frais_par_benevole, start=2):
            total_formula = f'=C{idx}+(ROUND(D{idx}*{tarif_km}*20, 0) / 20)+E{idx}'
            export.write_line([
                f"{line['benevole__nom']} {line['benevole__prenom']}", un_du_mois,
                line['frais_repas'] + line['frais_autre'], line['kms'], models.Frais.PART_FRAIS_TEL,
                total_formula
            ])
        num_lines = len(frais_par_benevole) + 1
        export.write_line(
            ['Totaux', ''] + [
                f'=SUM(C2:C{num_lines})', f'=SUM(D2:D{num_lines})', f'=SUM(E2:E{num_lines})', f'=SUM(F2:F{num_lines})'
            ],
            bold=True,
            number_formats={3: '0.00', 4: '0.00', 5: '0.00', 6: "0.00 [$CHF]"},
            height=17,
            borders='all'
        )
        return export.get_http_response(f'defraiements_{un_du_mois.year}_{un_du_mois.month:02}.xlsx')


class InstallationEditView(ClientJournalMixin, CreateUpdateView):
    """
    A priori, ne devrait être utilisée que pour modifier une installation existante,
    la création étant gérée par le moyen des interventions.
    """
    model = models.Installation
    form_class = forms.InstallationForm
    template_name = 'alarme/install_edit.html'
    journal_add_message = "Installation d’un nouvel appareil ({obj})"
    journal_edit_message = "Modification de l’installation alarme ({fields})"
    json_response = True

    def get_client(self):
        client = None
        if self.object:
            client = self.object.client
        elif 'client' in self.request.GET:
            client = Client.objects.get(pk=self.request.GET['client'])
        if client and not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_initial(self):
        initial = super().get_initial()
        if 'client' in self.request.GET:
            initial['client'] = Client.objects.get(pk=self.request.GET['client'])
        if 'alarme' in self.request.GET:
            initial['alarme'] = models.Alarme.objects.get(pk=self.request.GET['alarme'])
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['alarme'] = context['form'].instance.alarme if context['form'].instance.alarme_id else context['form'].initial.get('alarme')
        return context

    def journalize(self, form, **kwargs):
        if form.cleaned_data.get('nouvelle_alarme'):
            journal_msg = f"Changement d’appareil, ancien: {form.instance.alarme}, nouveau: {form.cleaned_data['nouvelle_alarme']}"
            Journal.objects.create(
                client=self.get_client(), description=journal_msg,
                quand=timezone.now(), qui=self.request.user
            )
        else:
            super().journalize(form, **kwargs)

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class InstallationCloseView(TemplateView):
    """Formulaire de résiliation"""
    template_name = 'alarme/uninstall.html'

    def dispatch(self, *args, **kwargs):
        self.install = get_object_or_404(models.Installation, pk=kwargs['pk'])
        if not self.install.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.edit_mode = self.install.date_fin_abo is not None
        self.uninstall_tm = models.TypeMission.objects.filter(code='UNINSTALL').first()
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(**{**kwargs,
            'client': self.install.client,
            'install_form': forms.UninstallForm(instance=self.install),
            'mission_form': forms.MissionSimpleCreateForm(
                initial={'type_mission': self.uninstall_tm}
            ) if not self.edit_mode else None,
        })

    def post(self, request, *args, **kwargs):
        install_form = forms.UninstallForm(instance=self.install, data=request.POST)
        frms = [install_form]
        if not self.edit_mode:
            mission_form = forms.MissionSimpleCreateForm(
                data=request.POST, initial={'type_mission': self.uninstall_tm}
            )
            if mission_form.has_changed():
                frms.append(mission_form)
                mission_form.instance.client = self.install.client
        else:
            mission_form = None
        if all([form.is_valid() for form in frms]):
            with transaction.atomic():
                for form in frms:
                    form.save()
                date_fin_abo = install_form.cleaned_data['date_fin_abo']
                if install_form.cleaned_data['mat_achete']:
                    self.install.alarme.date_archive = date_fin_abo
                    self.install.alarme.save()
                if self.edit_mode:
                    message = f"Résiliation modifiée: {form.get_changed_string()}"
                else:
                    message = (
                        f"Fin de l’abonnement le {django_format(date_fin_abo, 'd.m.Y')},"
                        f" motif {self.install.get_motif_fin_display()}"
                    )
                    if self.install.remarques:
                        message += f", {self.install.remarques}"
                Journal.objects.create(
                    client=self.install.client,
                    description=message,
                    quand=timezone.now(), qui=request.user
                )
            return JsonResponse({'result': 'OK', 'reload': 'page'})
        else:
            return self.render_to_response(self.get_context_data(
                install_form=install_form, mission_form=mission_form
            ))


class InstallationCloseCancelView(FormView):
    """Formulaire d’anulation de résiliation"""
    template_name = 'alarme/general_edit.html'
    form_class = forms.CancelWithCommentForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].intro_text = (
            "Voulez-vous vraiment annuler cette résiliation ? "
            "Le cas échéant, vous devez aussi aller annuler l’intervention correspondante. "
            "La remarque sera ajoutée dans le journal."
        )
        return context

    def form_valid(self, form):
        install = get_object_or_404(models.Installation, pk=self.kwargs['pk'])
        with transaction.atomic():
            install.date_fin_abo = None
            install.motif_fin = ''
            install.save()
            Journal.objects.create(
                client=install.client,
                description=f"La résiliation a été annulée: {form.cleaned_data['comment']}",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class MaterielClientEditView(ClientJournalMixin, CreateUpdateView):
    model = models.MaterielClient
    form_class = forms.MaterielClientForm
    template_name = "alarme/client_materiel.html"
    journal_add_message = "Installation d’un nouveau matériel ({obj})"
    journal_edit_message = "Modification du matériel installé ({fields})"
    json_response = True

    def get_client(self):
        if self.object:
            client = self.object.client
        else:
            client = get_object_or_404(Client, pk=self.kwargs['client_pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_initial(self):
        initial = super().get_initial()
        if 'client' in self.request.GET:
            initial['client'] = Client.objects.get(pk=self.request.GET['client'])
        if 'materiel' in self.request.GET:
            initial['materiel'] = models.Materiel.objects.get(pk=self.request.GET['materiel'])
        return initial

    def form_valid(self, form):
        if not form.instance.client_id:
            form.instance.client = self.get_client()
        with transaction.atomic():
            is_new = not form.instance.pk
            super().form_valid(form)
            matclient = self.object
            mat = matclient.type_mat or matclient.materiel.type_mat
            if is_new and mat.article_achat:
                article = mat.article_achat
                models.Facture.objects.create(
                    client=form.instance.client,
                    mois_facture=matclient.date_debut, date_facture=date.today(),
                    article=article,
                    install=None, materiel=matclient,
                    libelle=f"{article.designation} (installé le {matclient.date_debut.strftime('%d.%m.%Y')})",
                    montant=article.prix, exporte=None
                )
                messages.info(self.request, "Une facture a été créée pour l’achat de ce matériel par le client.")
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class MissionEditView(ClientJournalMixin, CreateUpdateView):
    model = models.Mission
    template_name = 'alarme/mission_edit.html'
    par_benevole = False
    journal_add_message = "Nouvelle intervention: {obj}"
    journal_edit_message = "Modification d’une intervention ({fields})"
    json_response = True

    def get_client(self):
        client = self.object.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if self.object and not self.object.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier cette intervention.")
        return client

    def get_form_class(self):
        return forms.BenevoleMissionForm if self.par_benevole else forms.MissionForm

    def get_initial(self):
        initial = super().get_initial()
        install = self.object.client.alarme_actuelle() if (self.object and self.object.client) else None
        if install and install.abonnement:
            initial['abonnement'] = install.abonnement
        if self.object and self.object.pk and self.object.alarme:
            initial['alarme_select'] = str(self.object.alarme)
        return initial

    def journalize(self, form, **kwargs):
        if self.object.type_mission.code == 'CHANGE' and self.object.effectuee and 'effectuee' in form.changed_data:
            old_inst = self.object.client.alarme_actuelle()
            if old_inst:
                message = (
                    f"Changement d’appareil, ancien: {old_inst.alarme}, "
                    f"nouveau: {form.cleaned_data['alarme']}"
                )
            else:
                message = f"Nouvel appareil: {form.cleaned_data['alarme']}"
            if 'abonnement' in form.changed_data:
                message += (
                    f"\nChangement d’abonnement, ancien: {old_inst.abonnement}, "
                    f"nouveau: {form.cleaned_data['abonnement']}"
                )
            dt = self.object.effectuee
            Journal.objects.create(
                client=self.get_client(), description=message,
                quand=timezone.make_aware(datetime(dt.year, dt.month, dt.day, 12, 0)),
                qui=self.request.user
            )
        else:
            return super().journalize(form, **kwargs)

    def form_valid(self, form):
        client = self.get_client()
        if self.is_create:
            form.instance.client = client
        with transaction.atomic():
            if (
                form.cleaned_data.get('type_mission') and form.cleaned_data.get('type_mission').code == 'NEW'
                and settings.DUREE_SANS_CLIENT_NEW_DEFAULT
            ):
                form.instance.duree_seul = settings.DUREE_SANS_CLIENT_NEW_DEFAULT
            super().form_valid(form)
            new_or_change = self.object.type_mission.code in {'NEW', 'CHANGE'}
            # Temporairement désactivé
            if False and new_or_change and self.object.planifiee and 'planifiee' in form.changed_data and self.par_benevole:
                # Envoi à la centrale des informations d'installation planifiée
                msg_body = default_centrale_message(
                    client, alarme=self.object.alarme, mission=self.object,
                )['body']
                msg_subject = f"[Croix-Rouge {canton_abrev()}] Réservation d'installation"
                transaction.on_commit(partial(
                    envoi_centrale, self.request, msg_subject, msg_body
                ))
                Journal.objects.create(
                    client=client, description="Message d'intervention planifiée envoyé à la centrale",
                    details=(
                        f"<b>Destinataire:</b> {settings.CENTRALE_EMAIL}\n"
                        f"<b>Sujet:</b> {msg_subject}\n"
                        f"<b>Message:</b>\n{msg_body}"
                    ),
                    quand=timezone.now(),
                    qui=self.request.user
                )
                messages.success(
                    self.request,
                    "Un message avec les informations d’installation (client/appareil) "
                    "a été envoyé à la centrale d’alarme."
                )

            if client and self.object.effectuee and 'effectuee' in form.changed_data and self.par_benevole:
                # Ajout d'une alerte pour que les gestionnaires de l'alarme soient informés de la fin d'intervention.
                text = f"Intervention effectuée: {str(self.object)}"
                Alerte.objects.create(
                    client=client, cible=ClientType.ALARME, alerte=text,
                    modele_lie=self.object, recu_le=timezone.now(),
                )

            nouvel_app = form.cleaned_data.get('alarme')
            if nouvel_app and self.object.effectuee:
                if self.object.type_mission.code == 'NEW':
                    # Nouvelle installation effectuée
                    abo = form.cleaned_data.get('abonnement') or self.object.abonnement
                    assert abo is not None
                    models.Installation.objects.create(
                        client=self.object.client, alarme=nouvel_app,
                        abonnement=abo, date_debut=self.object.effectuee
                    )
                    Journal.objects.create(
                        client=client,
                        description=f"Installation d’un nouvel appareil ({nouvel_app}, {abo})",
                        quand=timezone.now(), qui=self.request.user
                    )
                    if nouvel_app.chez_benevole:
                        nouvel_app.chez_benevole = None
                        nouvel_app.save()
                elif self.object.type_mission.code == 'CHANGE':
                    # Changement d'installation effectue
                    current_install = self.object.client.alarme_actuelle()
                    if not current_install.date_fin_abo:
                        current_install.date_fin_abo = self.object.effectuee - timedelta(days=1)
                        current_install.retour_mat = self.object.effectuee
                        current_install.save(is_change=True)
                    if self.object.benevole:
                        current_install.alarme.chez_benevole = self.object.benevole
                        current_install.alarme.save()
                    # Create new install
                    models.Installation.objects.create(
                        client=self.object.client, date_debut=self.object.effectuee,
                        alarme=nouvel_app,
                        abonnement=form.cleaned_data.get('abonnement'),
                        remarques=current_install.remarques,
                    )
                    if nouvel_app.chez_benevole:
                        nouvel_app.chez_benevole = None
                        nouvel_app.save()
            elif self.object.effectuee and self.object.type_mission.code == 'UNINSTALL':
                # Désinstallation effectuée par le bénévole, le matériel revient dans son stock
                current_install = self.object.client.derniere_alarme()
                current_install.retour_mat = self.object.effectuee
                current_install.save()
                if self.object.benevole:
                    current_install.alarme.chez_benevole = self.object.benevole
                    current_install.alarme.save()

        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleVisiteNew(CreateView):
    """Création d'une nouvelle visite planifiée par un bénévole."""
    model = models.Mission
    form_class = forms.VisiteCreateForm
    template_name = 'alarme/mission_edit.html'

    def form_valid(self, form):
        form.instance.type_mission = models.TypeMission.objects.filter(code='VISITE').first()
        form.instance.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        form.instance.benevole = self.request.user.benevole
        form.instance.delai = form.cleaned_data.get('planifiee')
        form.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class MissionDeleteView(DeleteView):
    model = models.Mission

    def form_valid(self, form):
        if not self.request.user.has_perm('alarme.delete_mission') or self.object.effectuee:
            raise PermissionDenied("Vous n’avez pas la permission d’annuler cette intervention")
        avec_client = self.object.client is not None
        self.object.delete()
        if avec_client:
            Journal.objects.create(
                client=self.object.client, description=f"Annulation de l’intervention «{self.object}»",
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleMissionsView(ListView):
    """Liste des missions attribuées à un bénévole."""
    model = models.Mission
    template_name = 'alarme/benevole_missions.html'
    paginate_by = 25

    def dispatch(self, request, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(
            benevole=self.benevole
        ).prefetch_related('frais').order_by('-effectuee', '-delai')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
            'activite': 'alarme',
        }


class BenevoleMissionEditView(MissionEditView):
    """Création/Édition d'une mission non liée à un client."""
    def get_client(self):
        return None

    def get_form_class(self):
        return forms.MissionSansClientForm

    def get_initial(self):
        return super(MissionEditView, self).get_initial()

    def form_valid(self, form):
        if self.is_create:
            if self.request.user.is_benevole:
                form.instance.benevole = self.request.user.benevole
            else:
                form.instance.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        form.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class BenevoleStockView(TemplateView):
    template_name = 'alarme/benevole_stock.html'

    def get_context_data(self, **kwargs):
        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'benevole': benevole,
            'activite': 'alarme',
            'appareils': benevole.alarme_set.all(),
            'materiels': benevole.materiel_set.all(),
        }


class BenevoleVisitesView(TemplateView):
    template_name = 'alarme/benevole_visites.html'

    def get_context_data(self, **kwargs):
        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'benevole': benevole,
            'activite': 'alarme',
            'a_visiter': benevole.clients_a_visiter().order_by('nom', 'prenom'),
        }


class BenevoleAttributionView(View):
    """Le bénévole s'attribue une intervention non encore attribuée."""
    def post(self, request, *args, **kwargs):
        benevole = request.user.benevole if hasattr(request.user, 'benevole') else None
        with transaction.atomic():
            intervention = models.Mission.objects.select_for_update().get(pk=kwargs['interv_pk'])
            if intervention.benevole is not None:
                if intervention.benevole == benevole:
                    messages.error(request, "Cette intervention vous a déjà été attribuée.")
                else:
                    messages.error(request, "Désolé, cette intervention vient d’être attribuée à une autre personne.")
            elif benevole and intervention.benevole is None:
                intervention.benevole = benevole
                intervention.save()
                messages.success(request, f"L’intervention chez {intervention.client.nom_prenom} vous a bien été attribuée, merci !")
        return HttpResponseRedirect(reverse('home'))


class BenevoleArchivesView(BenevoleMixin, TemplateView):
    """Liste des mois d'archives"""
    template_name = 'benevoles/archives-mois.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='benevoles/base.html')
        context['months'] = self.benev.mission_set.filter(effectuee__isnull=False).annotate(
            month=TruncMonth('effectuee')
        ).values_list('month', flat=True).distinct().order_by('-month')
        return context


class BenevoleArchivesMonthView(BenevoleMixin, TemplateView):
    template_name = 'benevoles/archives-mois-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = date(self.kwargs['year'], self.kwargs['month'], 1)
        end = (start + timedelta(days=32)).replace(day=1) - timedelta(days=1)
        missions = self.benev.mission_set.filter(
            effectuee__range=[start, end],
        ).order_by('-effectuee', 'pk')
        context['days'] = {
            dt: list(miss) for dt, miss in groupby(missions, key=attrgetter('effectuee'))
        }
        context['sums'] = missions.aggregate(
            total_missions=Count('pk'), total_km=Sum('km'), total_frais=Sum('frais__cout'),
            total_duree_client=Sum('duree_client'), total_duree_seul=Sum('duree_seul'),
        )
        return context


class MaterielIndexView(PermissionRequiredMixin, TemplateView):
    template_name = 'alarme/materiel_index.html'
    permission_required = 'alarme.view_alarme'

    def get_context_data(self, **kwargs):
        alarmes_installees = models.Alarme.objects.installees()
        alarmes_non_installees = models.Alarme.objects.non_installees()
        alarmes_a_reviser = models.Alarme.objects.a_reviser()
        alarmes_en_reparation = models.Alarme.objects.non_installees().filter(en_rep_depuis__isnull=False)
        materiels_installes = models.Materiel.objects.installes()
        materiels_non_installes = models.Materiel.objects.non_installes()
        context = {
            **super().get_context_data(**kwargs),
            'types_app': models.ModeleAlarme.objects.all().annotate(
                stock=Count('alarme', filter=
                    Q(alarme__id__in=alarmes_non_installees.values('id')) &
                    ~Q(alarme__id__in=alarmes_en_reparation.values('id'))
                ),
                installes=Count('alarme', filter=Q(alarme__id__in=alarmes_installees.values('id'))),
                revision=Count('alarme', filter=Q(alarme__id__in=alarmes_a_reviser.values('id'))),
                reparation=Count('alarme', filter=Q(alarme__id__in=alarmes_en_reparation.values('id'))),
            ).order_by('nom'),
            'types_mat': models.TypeMateriel.objects.all().annotate(
                stock=Count('materiel', filter=Q(materiel__id__in=materiels_non_installes.values('id'))),
                installes=Count('materiel', filter=Q(materiel__id__in=materiels_installes.values('id')))
            ).order_by('nom'),
        }
        context['totaux_app'] = {
            'stock': sum([l.stock for l in context['types_app']]),
            'reparation': sum([l.reparation for l in context['types_app']]),
            'revision': sum([l.revision for l in context['types_app']]),
            'installes': sum([l.installes for l in context['types_app']]),
            'total': sum([l.stock + l.installes for l in context['types_app']]),
        }
        return context


class AlarmeListView(PermissionRequiredMixin, FilterFormMixin, ExportableMixin, ListView):
    model = models.Alarme
    permission_required = 'alarme.view_alarme'
    filter_formclass = forms.AlarmeFilterForm
    paginate_by = 25
    col_widths = [20] * 8 + [40]

    def get_queryset(self):
        if self.filter_form.is_bound and self.filter_form.is_valid() and self.filter_form.cleaned_data['archived']:
            alarmes = super().get_queryset().filter(date_archive__isnull=False)
        else:
            alarmes = super().get_queryset().filter(date_archive=None)
        return alarmes.prefetch_related(
            Prefetch(
                'installation_set', to_attr='installations',
                queryset=models.Installation.objects.all().select_related(
                    'client', 'alarme', 'alarme__modele', 'abonnement'
                )
            )
        ).select_related('chez_benevole').order_by('modele', 'no_serie')

    def export_lines(self, context):
        fields = [
            self.model._meta.get_field(fname) for fname in [
                'modele', 'no_appareil', 'no_serie', 'carte_sim', 'date_achat', 'date_revision',
                'en_rep_depuis', 'remarques'
            ]
        ]
        yield ['BOLD'] + [f.verbose_name for f in fields] + ['Stock']
        for appareil in self.get_queryset():
            if appareil.date_archive or appareil.en_rep_depuis:
                stock_str = ''
            else:
                stock_str = 'En stock' + f", chez {appareil.chez_benevole}" if appareil.chez_benevole else ""
            yield [self._serialize(getattr(appareil, f.name)) for f in fields] + [stock_str]


class AlarmeEditView(PermissionRequiredMixin, RedirectURLMixin, JournalMixin, CreateUpdateView):
    model = models.Alarme
    form_class = forms.AlarmeForm
    template_name = 'alarme/alarme.html'
    permission_required = 'alarme.change_alarme'
    next_page = reverse_lazy('alarmes')
    journal_add_message = "Création de l’appareil"
    journal_edit_message = "Modification de l’appareil: {fields}"

    def _create_instance(self, **kwargs):
        models.JournalAlarme.objects.create(
            alarme=self.object, **kwargs
        )

    def get_success_message(self, obj):
        if self.is_create:
            return "La création du nouvel appareil {obj} a réussi."
        else:
            return f"L’appareil «{obj}» a bien été modifié"

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'install': self.object.install_actuelle() if self.object else None,
            'next': self.get_redirect_url(),
        }


class AlarmeArchiveView(PermissionRequiredMixin, View):
    model = models.Alarme
    permission_required = 'alarme.change_alarme'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(self.model, pk=kwargs['pk'])
        obj.date_archive = timezone.now()
        obj.chez_benevole = None
        obj.save()
        messages.success(request, "L’appareil a bien été archivé.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


class AlarmeJournalView(ListView):
    model = models.JournalAlarme
    paginate_by = 20
    template_name = 'alarme/alarme_journal.html'

    def get(self, *args, **kwargs):
        self.alarme = get_object_or_404(models.Alarme, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        journaux = list((j.quand, j.qui, j.description) for j in self.alarme.journaux.order_by('-quand'))
        for install in self.alarme.installation_set.order_by('-date_debut'):
            journaux.append((install.date_debut, '', f'Installation chez {install.client}'))
            if install.retour_mat:
                journaux.append((install.retour_mat, '', f'Retour du matériel de chez {install.client}'))
        return list(reversed(journaux))

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'alarme': self.alarme,
        }


class ModeleAlarmeEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.ModeleAlarme
    permission_required = 'alarme.change_modelealarme'
    form_class = forms.ModeleAlarmeForm
    json_response = True

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'OK', 'reload': '#id_modele'})


class MaterielListView(PermissionRequiredMixin, FilterFormMixin, ListView):
    model = models.Materiel
    permission_required = 'alarme.view_alarme'
    filter_formclass = forms.MaterielFilterForm
    paginate_by = 25

    def get_queryset(self):
        if self.filter_form.is_bound and self.filter_form.is_valid() and self.filter_form.cleaned_data['archived']:
            materiel = super().get_queryset().filter(date_archive__isnull=False)
        else:
            materiel = super().get_queryset().filter(date_archive=None)
        return materiel.prefetch_related(
            Prefetch('materielclient_set', to_attr='installations',
                     queryset=models.MaterielClient.objects.filter(
                        Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
                     ).select_related('client'))
        ).order_by('type_mat')


class MaterielEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.Materiel
    form_class = forms.MaterielForm
    template_name = 'alarme/materiel.html'
    permission_required = 'alarme.change_alarme'
    success_url = reverse_lazy('materiel')

    def get_success_message(self, obj):
        if self.is_create:
            return "La création du nouveau materiel {obj} a réussi."
        else:
            return f"Le matériel «{obj}» a bien été modifié."

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'install': self.object.materielclient_set.filter(
                Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
            ).first() if self.object else None,
        }


class MaterielArchiveView(PermissionRequiredMixin, View):
    model = models.Materiel
    permission_required = 'alarme.change_alarme'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(self.model, pk=kwargs['pk'])
        obj.date_archive = timezone.now()
        obj.save()
        messages.success(request, "Le matériel a bien été archivé.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', ''))


class TypeMaterielEditView(PermissionRequiredMixin, CreateUpdateView):
    model = models.TypeMateriel
    permission_required = 'alarme.change_typemateriel'
    form_class = forms.TypeMaterielForm
    json_response = True

    def form_valid(self, form):
        form.save()
        return JsonResponse({'result': 'OK', 'reload': '#id_type_mat'})


class AlarmeSearchView(View):
    """
    Endpoint for autocomplete search for InstallationEditView.
    Therefore only search unlinked alarmes.
    """
    MAX_RESULTS = 15
    from_benev_stock = False

    def get_queryset(self, term):
        if self.from_benev_stock:
            base_qs = self.request.user.benevole.alarme_set.all()
        else:
            base_qs = models.Alarme.objects.non_installees().filter(
                en_rep_depuis__isnull=True
            ).exclude(
                id__in=models.Alarme.objects.a_reviser().values('id')
            )
        return base_qs.filter(
            Q(modele__nom__icontains=term) | Q(no_serie__icontains=term) | Q(carte_sim__icontains=term)
        )

    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        if term is None:
            return HttpResponseBadRequest("Un terme de recherche est obligatoire")
        results = [
            {'label': str(mat), 'value': mat.pk}
            for mat in self.get_queryset(term)[:self.MAX_RESULTS]
        ]
        return JsonResponse(results, safe=False)


class AlarmeJSONView(View):
    def get(self, *args, **kwargs):
        if not self.request.user.has_perm('alarme.view_alarme') and not self.request.user.is_benevole:
            raise PermissionDenied("Vous n’avez pas les droits pour voir cet appareil")
        alarme = get_object_or_404(models.Alarme, pk=self.request.GET.get('pk'))
        content = serialize("json", [alarme])
        return HttpResponse(content, content_type="application/json")


class MaterielSearchView(PermissionRequiredMixin, View):
    """
    Endpoint for autocomplete search for MaterielClientEditView.
    Therefore only search unlinked materiel.
    """
    permission_required = 'alarme.view_alarme'

    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        query = models.Materiel.objects.non_installes().filter(
            Q(type_mat__nom__icontains=term) | Q(no_ref__icontains=term)
        )
        results = [
            {'label': str(mat), 'value': mat.pk}
            for mat in query[:10]
        ]
        return JsonResponse(results, safe=False)


def default_centrale_message(client, alarme, mission, extra_context=None):
    template = loader.get_template('alarme/envoi_centrale.txt')
    canton = canton_abrev()
    medecins = client.profclient_set.exclude(professionnel__type_pro='Sama').select_related('professionnel')
    body_context = {
        'canton': canton,
        'client': client,
        'alarme': alarme,
        'mission': mission,
        'medecins': [", ".join([item for item in [
            med.professionnel.nom, med.professionnel.prenom, med.professionnel.rue,
            " ".join([med.professionnel.npa, med.professionnel.localite]), med.professionnel.tel_1, med.professionnel.tel_2,
            med.professionnel.remarque, f"({med.remarque})" if med.remarque else '',
        ] if item]) for med in medecins],
        'contacts': client.contacts_as_context(),
        'liste_samas': client.profclient_set.filter(
                professionnel__type_pro='Sama'
            ).select_related('professionnel').order_by('priorite') if client.samaritains else [],
        'historique': client.journaux.filter(
            quand__gte=timezone.now() - timedelta(days=30)
        ).order_by('-quand'),
    }
    body_context.update(extra_context or {})
    return {
        'subject': f"[Croix-Rouge {canton}] Mise à jour client {client.nom_prenom}",
        'body': template.render(body_context),
    }


def envoi_centrale(request, subject, body):
    from_email = request.user.email
    if not same_domain(from_email, settings.DEFAULT_FROM_EMAIL):
        # The server cannot send mail on behalf of any mail domain.
        from_email = settings.DEFAULT_FROM_EMAIL
        body = (
            f"[Message envoyé par {request.user.last_name} {request.user.first_name} <{request.user.email}>, "
            f"depuis https://{settings.ALLOWED_HOSTS[0]}]\n\n"
        ) + body
    if not settings.TEST_INSTANCE:
        send_mail(
            subject, body, from_email, [settings.CENTRALE_EMAIL]
        )


def same_domain(email1, email2):
    return email1.rsplit('@', 1)[-1] == email2.rsplit('@', 1)[-1]
