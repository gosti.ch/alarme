from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0018_transportmodel_duree_rdv_nullable"),
    ]

    operations = [
        migrations.AddField(
            model_name="trajet",
            name="destination_princ",
            field=models.BooleanField(
                default=False, verbose_name="Destination principale"
            ),
        ),
    ]
