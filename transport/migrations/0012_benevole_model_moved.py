from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0001_initial"),
        ("transport", "0011_preference_client_chauffeur"),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            state_operations=[
                migrations.AlterField(
                    model_name="dispo",
                    name="chauffeur",
                    field=models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="dispos",
                        to="benevole.benevole",
                    ),
                ),
                migrations.AlterField(
                    model_name="preference",
                    name="chauffeur",
                    field=models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="benevole.benevole",
                        related_name="preferences",
                    ),
                ),
                migrations.AlterField(
                    model_name="transport",
                    name="chauffeur",
                    field=models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="transports",
                        to="benevole.benevole",
                    ),
                ),
            ],
            database_operations=[],
        )
    ]
