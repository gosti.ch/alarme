from django.urls import path

from . import views

urlpatterns = [
    path('<str:app>/', views.BenevoleListView.as_view(), name='benevoles'),
    path('<str:app>/archives/', views.BenevoleListView.as_view(is_archive=True), name='benevoles-archives'),
    path('<str:app>/nouveau/', views.BenevoleEditView.as_view(is_create=True), name='benevole-new'),
    path('<str:app>/<int:pk>/edition/', views.BenevoleEditView.as_view(is_create=False), name='benevole-edit'),
    path('<str:app>/<int:pk>/creercompte/', views.BenevoleCreateAccountView.as_view(), name='benevole-new-account'),
    path('<str:app>/autocomplete/', views.BenevoleAutocompleteView.as_view(),
        name='benevole-autocomplete'),
    path('<str:app>/<int:pk>/archive/', views.BenevoleArchiveView.as_view(), name='benevole-archive'),
    path('<str:app>/<int:pk>/notesfrais/', views.BenevoleNotesFraisView.as_view(), name='benevole-notesfrais'),
    path('<str:app>/<int:pk>/journal/', views.BenevoleJournalView.as_view(), name='benevole-journal'),
]
