from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0021_adresse_date_archive_nom_unique"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="chauff_aller_dist",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="Distance chauffeur-départ [m]"
            ),
        ),
        migrations.AddField(
            model_name="transport",
            name="chauff_retour_dist",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="Distance arrivée-chauffeur [m]"
            ),
        ),
    ]
