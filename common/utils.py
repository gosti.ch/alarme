from decimal import Decimal, ROUND_HALF_UP
from pathlib import Path

from django.apps import apps
from django.conf import settings
from django.db.models import Q
from django.templatetags.static import static

canton_app = apps.get_app_config(settings.CANTON_APP)
IMAGE_EXTS = ['.jpg', '.jpeg', '.png', '.tif', '.tiff', '.gif']


def current_app():
    for app in ['transport', 'visite', 'alarme', 'besoins']:
        if apps.is_installed(app):
            return app
    raise RuntimeError("No current app detected")


def arrondi_5(montant):
    if isinstance(montant, Decimal):
        return round(montant * Decimal('20')) / Decimal('20')
    return round(montant * 20) / 20


def tva_et_arrondi(montant):
    tva = (montant * settings.TAUX_TVA).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
    diff_arrond = arrondi_5(montant + tva) - (montant + tva)
    return tva, diff_arrond


def canton_abrev():
    return canton_app.abrev


def icon_url(file_name):
    ext = Path(file_name).suffix.lower()
    icon = 'master'  # default
    if ext in IMAGE_EXTS:
        icon = 'image'
    elif ext == '.pdf':
        icon = 'pdf'
    elif ext in ('.xls', '.xlsx', '.ods', '.csv'):
        icon = 'tsv'
    elif ext == '.odt':
        icon = 'odt'
    elif ext in ('.doc', '.docx'):
        icon = 'docx'
    return static(f'ficons/{icon}.svg')


def same_month(date1, date2):
    return date1 and date2 and (date1.year, date1.month) == (date2.year, date2.month)


def format_HM(tdelta):
    seconds = round(tdelta.total_seconds())
    hours = seconds // 3600
    minutes = seconds % 3600 // 60
    return '{:02d}:{:02d}'.format(hours, minutes)


class RegionFinder:
    regions = {
        'Littoral Est': [range(2068, 2100), range(2523, 2526)],
        'Littoral Ouest': [range(2012, 2034)],
        'Montagnes': [range(2300, 2326), range(2400, 2420)],
        'Neuchâtel': [range(2000, 2012), range(2034, 2037), range(2067, 2068)],
        'Val-de-Ruz': [range(2037, 2066), range(2200, 2300)],
        'Val-de-Travers': [range(2100, 2200)],
    }

    @classmethod
    def get_region(cls, npa):
        if not npa:
            return 'Autre'
        npa = int(npa)
        for nom, ranges in cls.regions.items():
            if any(npa in rg for rg in ranges):
                return nom
        return 'Autre'

    @classmethod
    def get_region_filter(cls, field_name, reg_name):
        if reg_name == 'Autre':
            return ~Q(
                Q(**{f'{field_name}__range': ('2000', '2326')}) |
                Q(**{f'{field_name}__range': ('2400', '2420')}) |
                Q(**{f'{field_name}__range': ('2523', '2526')})
            )
        range1 = cls.regions[reg_name][0]
        flt = Q(**{f'{field_name}__range': (range1.start, range1.stop)})
        for rg in cls.regions[reg_name][1:]:
            flt |= Q(**{f'{field_name}__range': (rg.start, rg.stop)})
        return flt
