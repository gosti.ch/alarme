from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0017_champs_remarques"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transportmodel",
            name="duree_rdv",
            field=models.DurationField(
                blank=True, null=True, verbose_name="Durée du rendez-vous"
            ),
        ),
    ]
