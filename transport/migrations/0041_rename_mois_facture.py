from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0040_facture_exporte_id_externe'),
    ]

    operations = [
        migrations.RenameField(
            model_name='facture',
            old_name='mois',
            new_name='mois_facture',
        ),
    ]
