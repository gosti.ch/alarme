import argparse
from datetime import date, datetime

from openpyxl import load_workbook

from django.core.management.base import BaseCommand
from django.db import transaction

from benevole.models import Benevole


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('fichier', type=argparse.FileType('rb'))

    def handle(self, **options):
        wb = load_workbook(options['fichier'])
        ws = wb.active
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                self.import_chauffeur(values)
            #raise Exception("Importation en phase de test")

    def import_chauffeur(self, values):
        try:
            chauffeur = Benevole.objects.get(nom=values['Nom'], prenom=values['Prénom'])
        except Benevole.DoesNotExist:
            pass
        else:
            self.stdout.write(f"Le chauffeur {chauffeur} existe déjà")
            # Compléter avec nouvelles valeurs
            if 'transport' not in chauffeur.activites:
                chauffeur.activites.append('transport')
            chauffeur.genre = values['Sexe']
            chauffeur.date_naissance = read_date(values['Date de naissance'])
            chauffeur.no_plaques = clean(values['Numéro de plaque'])
            chauffeur.transport_depuis = read_date(values['Profil gültig von'])
            chauffeur.no_debiteur = clean(values['Navision ID'])
            chauffeur.save()
            return

        if not values['Code Postal'] or not values['Lieu']:
            self.stdout.write(
                f"Code postal et localité sont obligatoires, importation de {values['Nom']} {values['Prénom']} annulée"
            )
            return

        chauffeur = Benevole(
            nom=values['Nom'], prenom=values['Prénom'], langues=['fr'], activites=['transport'],
            genre=values['Sexe'], date_naissance=read_date(values['Date de naissance']),
            rue=" ".join([val for val in [values['Rue'], str(values['Numéro'])] if val]),
            npa=values['Code Postal'], localite=values['Lieu'], tel_prive=clean(values['privé Téléphone']),
            courriel=values['privé Courriel'] or values.get('entreprise Courriel', ''),
            no_plaques=clean(values['Numéro de plaque']),
            transport_depuis=read_date(values['Profil gültig von']),
            no_debiteur=clean(values['Navision ID']),
        )
        chauffeur.full_clean()
        chauffeur.save()


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val


def read_date(dt):
    if not dt or dt in {'00.00.0000', '- - -'}:
        return None
    if isinstance(dt, datetime):
        return dt.date()
    return date(*reversed([int(part) for part in dt.split('.')]))
