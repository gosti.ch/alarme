import re
from ipaddress import ip_address

from django.apps import apps
from django.conf import settings
from django.urls import reverse
from django.http import HttpResponseRedirect

EXEMPT_URLS = [
    re.compile(r'^account/*'),
    re.compile(r'^login/$'),
    re.compile(r'^logout/$'),
    re.compile(r'^jsi18n/$'),
    re.compile(r'^favicon.ico$'),
    re.compile(r'^api/*'),
]

# Liste d'URL autorisées pour l'accès des bénévoles (potentiellement sans 2FA)
BENEV_ALLOWLIST = [
    re.compile(r'^app/*'),
    re.compile(r'^alarmes/json/'),
]


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        path = request.path_info.lstrip('/')
        if any(m.match(path) for m in EXEMPT_URLS):
            # Short-circuit if path is auth-exempted.
            return self.get_response(request)

        path_2fa_exempted = any(m.match(path) for m in BENEV_ALLOWLIST)
        if request.user.is_authenticated and request.user.is_benevole and apps.is_installed('transport'):
            if path_2fa_exempted:
                return self.get_response(request)
            return HttpResponseRedirect(reverse('home-app'))
        # is_verified (coming from django-otp OPTMiddleware) checks 2FA auth
        if not request.user.is_verified():
            ip = ip_address(request.META.get('REMOTE_ADDR'))
            ip_2fa_exempted = any(ip in net for net in settings.EXEMPT_2FA_NETWORKS)
            if request.user.is_authenticated:
                if ip_2fa_exempted or request.user.email in settings.EXEMPT_2FA_USERS:
                    return self.get_response(request)
                return HttpResponseRedirect(reverse('two_factor:setup'))
            else:
                login_view = reverse('login') if (ip_2fa_exempted or path_2fa_exempted) else reverse('two_factor:login')
                return HttpResponseRedirect("{}?next={}".format(login_view, request.path))
        return self.get_response(request)
