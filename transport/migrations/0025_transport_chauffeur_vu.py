from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0024_transport_facturer"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="chauffeur_vu",
            field=models.BooleanField(default=False, verbose_name="Vu par chauffeur"),
        ),
    ]
