import calendar


def last_day_of_month(_date):
    return _date.replace(day=calendar.monthrange(_date.year, _date.month)[1])
