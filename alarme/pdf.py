from datetime import date
from functools import partial

from django.utils.dateformat import format as django_format

from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.colors import HexColor, black
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    KeepTogether, Image, ListFlowable, ListItem, PageBreak, Paragraph,
    Spacer, Table, TableStyle
)

from common.pdf import BaseCroixrougePDF, PageNumCanvas, VerticalText
from common.utils import canton_abrev


class QuestionnairePDF(BaseCroixrougePDF):
    title = 'Questionnaire Alarme Croix-Rouge'
    version = '2.11'
    top_margin = 3 * cm
    FONTSIZE = 10

    def define_styles(self):
        super().define_styles()
        self.bold11= ParagraphStyle(
            name='Bold 11', fontName='Helvetica-Bold', fontSize=11, leading=13
        )
        # Pour la case à cocher
        registerFont(TTFont('DejaVuSans', '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'))

    def draw_header_footer(self, canvas, doc):
        super().draw_header_footer(canvas, doc)
        canvas.saveState()
        canvas.setFont("Helvetica-Bold", 12)
        canvas.drawString(doc.leftMargin, A4[1] - 2.6 * cm, "Questionnaire Alarme Croix-Rouge")
        canvas.setFont("Helvetica", 9)
        canvas.drawString(doc.leftMargin, 2.8 * cm, f"Questionnaire v{self.version}")
        canvas.restoreState()

    def get_filename(self, client):
        return f'questionnaire_alarme_{client.nom}.pdf'

    def produce(self, client, install_mission=None, pour_signature=False, signature_file=None, signature_lieu=None):
        """
        Si pour_signature: la dernière page contenant la signature est omise, car elle sera affichée
            dans le HTML avec le composant de signature.
        Si signature_file/signature_lieu: les champs de signature sont complétés avec ce contenu.
        """
        install = client.alarme_actuelle()
        abonnement = install_mission.abonnement if install_mission else (install.abonnement if install else None)
        appareil = install_mission.alarme if install_mission else (install.alarme if install else None)
        P = partial(Paragraph, style=self.style_normal)
        self.story.append(Spacer(1, 3 * cm))
        self.story.append(P("Client-e Alarme Croix-Rouge"))
        cr_canton = {
            'JU': 'Croix-Rouge suisse Canton du Jura',
            'NE': 'Croix-Rouge neuchâteloise',
        }.get(canton_abrev())
        date_mission = (install_mission.effectuee or install_mission.planifiee) if install_mission else None
        table_data = [
            [Paragraph(f'AC CR {cr_canton}', style=self.bold11), ''],
            ['Nom client-e', client.nom, 'Prénom', client.prenom],
            ['Adresse', client.rue, 'NPA, localité', f'{client.npa} {client.localite}'],
            ['Tél. princ.', client.tel_1, 'Tél. sec.', client.tel_2],
            ['Courriel', client.courriel],
            ['Date de naissance',
             client.date_naissance.strftime('%d.%m.%Y') if client.date_naissance else '?',
             'Langue', client.get_langues_display()
            ],
            ['Responsable (installateur/trice)', '',
             install_mission.benevole.nom_prenom if install_mission and install_mission.benevole else '?'
            ],
            ['Date d’installation', '', date_mission.strftime('%d.%m.%Y') if date_mission else '?'],
        ]
        style_data = [
            ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ('LEFTPADDING', (0, 0), (-1, -1), 10),
            ('RIGHTPADDING', (0, 0), (-1, -1), 10),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ('TOPPADDING', (0, 0), (-1, -1), 8),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
            ('BOX', (0, 1), (1, 1), 0.25, black),
            ('BOX', (2, 1), (3, 1), 0.25, black),
            ('BOX', (0, 2), (1, 2), 0.25, black),
            ('BOX', (2, 2), (3, 2), 0.25, black),
            ('BOX', (0, 3), (1, 3), 0.25, black),
            ('BOX', (2, 3), (3, 3), 0.25, black),
            ('BOX', (0, 5), (1, 5), 0.25, black),
            ('BOX', (2, 5), (3, 5), 0.25, black),
            ('BOX', (0, 6), (1, 6), 0.25, black),
            ('BOX', (2, 6), (3, 6), 0.25, black),
            ('BOX', (0, 7), (1, 7), 0.25, black),
            ('BOX', (2, 7), (3, 7), 0.25, black),
            ('BACKGROUND', (0, 0),(-1, -1), HexColor(0xF3F3F3)),
            ('SPAN', (0, 0),(-1, 0)),
        ]
        if client.nom_part:
            table_data.extend([
                ['Nom Prénom client-e 2', '', f'{client.nom_part} {client.prenom_part}'],
                ['Date de naissance client-e 2', '',
                 client.date_naissance_part.strftime('%d.%m.%Y') if client.date_naissance_part else '?'],
            ])
            style_data.extend([
                ('BOX', (0, 8), (1, 8), 0.25, black),
                ('BOX', (2, 8), (3, 8), 0.25, black),
                ('BOX', (0, 9), (1, 9), 0.25, black),
                ('BOX', (2, 9), (3, 9), 0.25, black),
            ])

        self.story.append(Table(
            data=table_data, colWidths=[3* cm, 6 * cm, 3 * cm, 6 * cm],
            style=TableStyle(style_data),
        ))
        nom_abo = abonnement.nom.replace("Abo ", "") if abonnement else "?"
        table_data = [
            ['Prestation', 'Type d’appareil', 'ID appareil (carte SIM)', 'Numéro de série (SN)'],
            [P(f'{self.checked_box} {nom_abo}'),
             appareil.modele if appareil else '?',
             appareil.carte_sim if appareil else '?',
             appareil.no_serie if appareil else '?',
            ]
        ]
        style_data = [
            ('LEFTPADDING', (0, 0), (-1, -1), 10),
            ('RIGHTPADDING', (0, 0), (-1, -1), 10),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ('TOPPADDING', (0, 0), (-1, -1), 8),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
            ('BOX', (0, 0), (-1, -1), 0.25, black),
            ('BACKGROUND', (0, 0),(-1, -1), HexColor(0xF3F3F3)),
        ]
        no_lig = 2
        if client.nom_part:
            table_data.extend([
                [P(f'Accessoires<br/><br/>{self.checked_box} Bracelet émetteur supplémentaire'), '']
            ])
            style_data.append(
                ('SPAN', (0, no_lig), (-1, no_lig)),
            )
            no_lig += 1
        if client.boitier_cle:
            txt = {
                'c-r': "* Boîtier à clés Croix-Rouge",
                'prive': "Boîtier à clés privé",
            }[client.boitier_cle]
            table_data.extend([
                [P(f'{self.checked_box} {txt}'), '', P(f'Infos clés: {client.cles}')]
            ])
            style_data.extend([
                ('SPAN', (0, no_lig), (1, no_lig)),
                ('SPAN', (2, no_lig), (3, no_lig)),
            ])
        self.story.append(Table(
            data=table_data,
            colWidths=[4.5 * cm, 4.5 * cm, 4.5 * cm, 4.5 * cm],
            style=TableStyle(style_data),
        ))
        if client.boitier_cle == 'c-r':
            self.story.append(Spacer(1, 0.2 * cm))
            self.story.append(P(
                "*Avant l’installation du boîtier clé, le bénéficiaire ou son référent aura demandé "
                "à sa gérance l’installation du boîtier clé, pour autant que ce dernier soit locataire. "
                "Le bénéficiaire de l’alarme est propriétaire et ainsi responsable du boîtier clé. "
                "Le bénéficiaire autorise l’installateur-bénévole à le poser à l’endroit adapté à cet "
                f"effet. La {cr_canton} décline toutes responsabilités lors du retrait dudit "
                "boîtier en cas d’endommagements éventuels liés à cette action par le bénéficiaire ou "
                "ses répondants."
            ))
        self.story.append(PageBreak())
        # Page 2
        self.story.append(Paragraph("Données pour la prestation d’aide par la centrale d’alarme", style=self.bold11))
        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P("Situation de vie et conditions de logement"))
        situation_vie = {
            client.SITUATION_VIE_SEUL: 's',
            client.SITUATION_VIE_COUPLE: 'c'
        }.get(client.situation_vie, 'a')
        p_situation_vie = (
            f"{self.checked_box if situation_vie == 's' else self.unchecked_box} seul<br/><br/>"
            f" {self.checked_box if situation_vie == 'c' else self.unchecked_box} "
            "avec mon époux/épouse<br/>  ou compagnon/compagne<br/><br/>"
            f" {self.checked_box if situation_vie == 'a' else self.unchecked_box} "
            f"autre : {client.situation_vie if situation_vie == 'a' else ''}"
        )
        p_type_logement = (
            f"{self.checked_box if client.type_logement == 'villa' else self.unchecked_box} dans une villa<br/>"
            f"{self.checked_box if client.type_logement == 'appartement' else self.unchecked_box} "
            "dans un appartement<br/>"
            f"{self.checked_box if client.type_logement == 'resid' else self.unchecked_box} "
            "dans une institution de soins /<br/>     appartement avec encadrement<br/><br/>"
        )
        if client.type_logement_info:
            p_type_logement += f'Remarque : {client.type_logement_info}<br/>'
        if client.etage:
            p_type_logement += f'Étage/niveau : {client.etage}'

        self.story.append(Table(
            data=[
                [
                    P("Je vis…"), P(p_situation_vie),
                    P("J’habite…"), P(p_type_logement),
                ],
                [
                    P(f"Animal de compagnie : {self.checked_box if client.animal_comp else self.unchecked_box} Oui   "
                      f"{self.checked_box if not client.animal_comp else self.unchecked_box} Non"
                    ), "",
                    P("Ascenseur"),
                    P(f"{self.checked_box if client.ascenseur else self.unchecked_box} Oui   "
                      f"{self.checked_box if not client.ascenseur else self.unchecked_box} Non"
                    ),
                ],
                [
                    "", P(client.animal_comp),
                    P(f"Code entrée immeuble : {client.code_entree or '-'}"),
                ],
                [
                    "", "", P(f"Digicode (boîte à clé) : {client.cles or '-'}"),
                ],
                [
                    "", "", P(f"Nombre de pièces : {client.nb_pieces or '-'}"),
                ],
            ], colWidths=[2.5 * cm, 6.5 * cm, 2.5 * cm, 6.5 * cm],
            style=TableStyle([
                ('BOX', (0, 0), (1, -1), 0.25, black),
                ('BOX', (2, 0), (3, -1), 0.25, black),
                ('VALIGN', (0, 0), (-1, -1), "TOP"),
                ('SPAN', (0, 1), (1, 1)),
                ('SPAN', (2, 2), (3, 2)),
                ('SPAN', (2, 3), (3, 3)),
                ('SPAN', (2, 4), (3, 4)),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))

        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P(
            "Informations importantes à l’intention du personnel soignant / des ambulanciers"
        ))
        donnees_medic = client.donnees_medic or {}

        def medic_cb(key):
            box = self.checked_box if donnees_medic.get(key) else self.unchecked_box
            return P(f'{box} {client.DONNEES_MEDIC_MAP[key]}')

        def medic_cb_plus_text(key):
            return P(
                f'{self.checked_box} {client.DONNEES_MEDIC_MAP[key]} : {donnees_medic[key]}'
                if donnees_medic.get(key)
                else f'{self.unchecked_box} {client.DONNEES_MEDIC_MAP[key]}'
            )

        self.story.append(Table(
            data=[[
                medic_cb_plus_text('troubles_eloc'), medic_cb_plus_text('troubles_aud'),
            ], [
                medic_cb_plus_text('allergies')
            ], [
                medic_cb_plus_text('empl_medic')
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOX', (0, 2), (-1, 2), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
                ('SPAN', (0, 1),(-1, 1)),
                ('SPAN', (0, 2),(-1, 2)),
            ])
        ))
        self.story.append(Table(
            data=[[
                medic_cb('diabete'), medic_cb('troubles_cardio'), medic_cb('epilepsie'),
            ], [
                medic_cb('oxygene'), medic_cb('anticoag'),
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))
        self.story.append(Table(
            data=[[
                medic_cb_plus_text('directives'),
            ], [
                P(f"{client.DONNEES_MEDIC_MAP['autres']} : {donnees_medic.get('autres') or ''}")
            ]],
            style=TableStyle([
                ('BOX', (0, 0), (-1, 0), 0.25, black),
                ('BOX', (0, 1), (-1, 1), 0.25, black),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
            ])
        ))
        self.story.append(Spacer(1, 0.8 * cm))

        self.story.append(P("Médecin traitant"))
        medecins = client.profclient_set.exclude(professionnel__type_pro='Sama').select_related('professionnel')
        if medecins:
            tdata = []
            for med in medecins:
                tdata.append([
                    P(f"{med.professionnel.nom_prenom}"),
                    P(f"Tél : {med.professionnel.tel_1}"),
                    P(f"Remarque : {med.remarque}") if med.remarque else "",
                ])
            self.story.append(Table(
                data=tdata,
                style=TableStyle([
                    ('INNERGRID', (0, 0), (2, -1), 0.25, black),
                    ('BOX', (0, 0), (-1, -1), 0.25, black),
                    ('VALIGN', (0, 0), (-1, -1), 'TOP'),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 8),
                ]),
                colWidths=[6.7 * cm, 4 * cm, 6.7 * cm],
            ))
        else:
            self.story.append(P("<i>Non renseigné</i>"))
        self.story.append(PageBreak())

        # Page 3
        self.story.append(Paragraph("Personnes disposant de votre clé à contacter en cas d’urgence", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        self.story.append(P(
            "Veuillez indiquer, <u>par ordre de priorité</u>, au minimum <u>trois</u> répondants vivant "
            "dans votre voisinage immédiat et disposant d’une clé de votre appartement/maison ou du "
            "moins connaissant l’emplacement de celle-ci.<br/>"
            "Si les personnes mentionnées ne sont pas joignables en cas d’urgence, d’autres services "
            "d’intervention peuvent être mobilisés en fonction de la situation (sauveteurs, pompiers "
            "ou policiers), les coûts éventuels étant à votre charge."
        ))
        self.story.append(Spacer(1, 0.5 * cm))

        def print_rep_table(rep_list, left_label=None):
            for idx, rep in enumerate(rep_list, start=1):
                col1 = [f"{idx})", VerticalText(left_label), "", ""] if left_label else ['', '', '', '']
                if rep.sama:
                    # Impression des samaritains
                    samas = client.profclient_set.filter(
                        professionnel__type_pro='Sama'
                    ).select_related('professionnel').order_by('priorite')
                    samas_strs = []
                    for idx2, sama in enumerate(samas, start=1):
                        sama_str = f"{str(idx2)}. {sama.professionnel.nom} {sama.professionnel.prenom}"
                        if sama.professionnel.inactif:
                            sama_str = f"<i>{sama_str} (temporairement en inactivité)</i>"
                        samas_strs.append(sama_str)
                    self.story.append(Table(
                        data=[
                            [col1[0], "Les Samaritains"],
                            [col1[1], P("<br/>".join(samas_strs))],
                        ],
                        style=TableStyle([
                            ('VALIGN', (1, 0), (2, 2), 'TOP'),
                            ('BOX', (0, 0), (-1, -1), 0.25, black),
                            ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
                        ]),
                        colWidths=[1 * cm if left_label else 0 * cm, 16.4 * cm],
                    ))
                    continue

                tels = [tel.tel + (f"({tel.remarque})" if tel.remarque else "") for tel in rep.referenttel_set.all()]
                self.story.append(Table(
                    data=[[
                        col1[0], "Nom", rep.nom, "Prénom", rep.prenom,
                    ], [
                        col1[1], "Adresse", P(rep.rue), "Localité", f"{rep.npa} {rep.localite}"
                    ], [
                        col1[2], "Téléphones", P("<br/>".join(tels)), "Courriel", P(rep.courriel)
                    ], [
                        col1[3], f"Relation / lien de parenté : {rep.relation}"
                    ]],
                    style=TableStyle([
                        ('VALIGN', (1, 0), (4, 3), 'TOP'),
                        ('SPAN', (0, 1), (0, 3)),
                        ('BOX', (0, 0), (0, 0), 0.25, black),
                        ('BOX', (1, 0), (2, 0), 0.25, black),
                        ('BOX', (3, 0), (4, 0), 0.25, black),
                        ('BOX', (0, 1), (0, 3), 0.25, black),
                        ('BOX', (1, 1), (2, 1), 0.25, black),
                        ('BOX', (3, 1), (4, 1), 0.25, black),
                        ('BOX', (1, 2), (2, 2), 0.25, black),
                        ('BOX', (3, 2), (4, 2), 0.25, black),
                        ('BOX', (1, 3), (4, 3), 0.25, black),
                    ]),
                    colWidths=[1 * cm if left_label else 0 * cm, 2.5 * cm, 5.7 * cm, 2.5 * cm, 5.7 * cm],
                ))
                self.story.append(Spacer(1, 0.5 * cm))

        print_rep_table(client.repondants(), "Répondant")
        self.story.append(PageBreak())

        # Page 4
        self.story.append(Paragraph("Personnes à prévenir en cas d’urgence", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        self.story.append(P(
            "En cas d’urgence, les personnes de référence ne sont pas mobilisées pour la "
            "prestation d’aide, mais sont informées par la centrale d’alarme. Ce sont elles "
            "qui prennent des décisions à votre place si vous n’êtes plus apte à le faire."
        ))
        self.story.append(Spacer(1, 0.5 * cm))
        referents = [ref for ref in client.referents if ref.referent]
        print_rep_table(referents, "Pers. de référence")

        self.story.append(Paragraph("Données administratives", style=self.bold11))
        self.story.append(Spacer(1, 0.5 * cm))
        self.story.append(P(
            "Personne à contacter pour toute question d’ordre administratif ou "
            "technique (si différent du / de la client-e)"
        ))
        admin = [ref for ref in client.referents if ref.admin]
        if admin:
            self.story.append(Spacer(1, 0.5 * cm))
            print_rep_table(admin, None)
        else:
            self.story.append(P("<i>Aucune</i>"))

        self.story.append(P("Facture à adresser à (si différent du / de la client-e)"))
        factures = [ref for ref in client.referents if ref.facturation_pour]
        if factures:
            self.story.append(Spacer(1, 0.5 * cm))
            print_rep_table(factures, None)
        else:
            self.story.append(P("<i>Aucune</i>"))

        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(KeepTogether([
            P("Mode de paiement (mensuel)"),
            P(f"{self.checked_box if client.envoi_facture else self.unchecked_box} Facture"),
        ]))

        self.story.append(Spacer(1, 0.8 * cm))

        if not pour_signature:
            sigdata = [[P("Lieu/date"), P("Signature client-e")]]
            if signature_file:
                im = Image(signature_file, 2 * cm, 2 * cm)
                sigdata.append([P(f"{signature_lieu}, le {date.today().strftime('%d.%m.%Y')}"), im])

            tstyle = [
                ('INNERGRID', (0, 0), (-1, -1), 0.25, black),
                ('BOX', (0, 0), (-1, -1), 0.25, black),
                ('VALIGN', (0, 0), (-1, -1), "CENTER"),
            ]
            if not signature_file:
                tstyle.append(('BOTTOMPADDING', (0, 0), (-1, -1), 40))
            self.story.append(KeepTogether([
                Paragraph("Dispositions finales", style=self.bold11),
                Spacer(1, 0.3 * cm),
                P(
                    "Veuillez noter que tous les détails des prestations auxquelles vous recourez "
                    "sont régies par le contrat Alarme Croix-Rouge et les conventions de prestations "
                    "correspondantes."
                ),
                Spacer(1, 0.5 * cm),
                P(
                    "Accord<br/>"
                    "Je confirme avoir pris connaissance des informations techniques ci-dessus et "
                    "accepte que mes données personnelles soient enregistrées auprès de la "
                    f"centrale d’alarme et de la {cr_canton}."
                ),
                Spacer(1, 0.5 * cm),
                Table(
                    data=sigdata,
                    colWidths=[8 * cm, 8 * cm],
                    style=TableStyle(tstyle)
                ),
                Spacer(1, 0.5 * cm),
                P(
                    f"Veuillez remplir ce formulaire de manière complète et précise. La {cr_canton} "
                    "et la centrale d’alarme s’engagent à traiter de façon strictement "
                    "confidentielle toutes les données personnelles qui y figurent."
                ),
            ]))

        self.doc.build(
            self.story, onFirstPage=self.draw_header_footer, onLaterPages=self.draw_header_footer,
            canvasmaker=PageNumCanvas
        )


class CourrierPDF(BaseCroixrougePDF):
    title = 'Courrier Alarme Croix-Rouge'
    FONTSIZE = 11

    def get_filename(self, client):
        return f'courrier_bienvenue_{client.nom}.pdf'

    def produce(self, client):
        para_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE,
            alignment=TA_JUSTIFY, spaceAfter=0.3 * cm
        )
        P = partial(Paragraph, style=self.style_normal)
        self.story.append(Spacer(1, 2 * cm))
        self.story.append(P(client.titre))
        self.story.append(P(f"{client.noms} {client.prenoms}"))
        if client.rue:
            self.story.append(P(client.rue))
        self.story.append(P(f"{client.npa} {client.localite}"))
        self.story.append(Spacer(1, 1.8 * cm))
        self.story.append(P(f"Neuchâtel, le {django_format(date.today(), 'j F Y')}"))
        self.story.append(Spacer(1, 1.5 * cm))
        self.story.append(P("<b>Alarme Croix-Rouge</b>"))
        self.story.append(Spacer(1, 1 * cm))

        Pjust = partial(Paragraph, style=para_style)
        self.story.append(Pjust(f'{client.titre},'))
        paras = [
            "Par la présente, nous vous souhaitons la bienvenue parmi nos clients Alarme Croix-Rouge.",
            "Nous espérons que l’appareil et le service choisi vous offre entière satisfaction.",
            "Vous trouverez ci-joint votre contrat ainsi que votre questionnaire datés et signés.",
            "Pensez bien à nous avertir du moindre changement - adresse, numéro de téléphone, "
            "n° digicode, changement de répondant, etc. –  au <b>032 886 88 60 du lundi au vendredi "
            "de 8h30 à 11h30 et de 14h à 16h30</b> pour que nos prestations restent optimales.",
            "Nous attirons votre attention que nous ne délivrons pas d’attestation fiscale. "
            "De plus, seuls les montants payés durant la période fiscale concernée peuvent être "
            "déduits. Il est de votre responsabilité de vous assurer auprès de l’autorité fiscale "
            "que ces frais sont bien déductibles. Nous vous remercions en ce sens de conserver "
            "vos factures.",
            "Nous vous remercions de votre confiance en ayant choisi notre système Alarme.",
            "En restant à votre disposition pour toute demande d’informations complémentaires, "
            f"nous vous adressons, {client.titre}, nos cordiales salutations."
        ]
        for para in paras:
            self.story.append(Pjust(para))
        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P("Votre Croix-Rouge neuchâteloise"))
        self.story.append(Spacer(1, 1.0 * cm))
        self.story.append(P("Annexes : ment."))

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


class CourrierReferentPDF(BaseCroixrougePDF):
    title = 'Courrier Alarme Croix-Rouge'
    FONTSIZE = 11

    def get_filename(self, referent):
        return f'courrier_repondant_{referent.nom}.pdf'

    def produce(self, referent):
        para_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE,
            alignment=TA_JUSTIFY, spaceAfter=0.3 * cm
        )
        P = partial(Paragraph, style=self.style_normal)
        self.story.append(Spacer(1, 2 * cm))
        self.story.append(P(referent.get_salutation_display()))
        self.story.append(P(f"{referent.nom_prenom}"))
        if referent.rue:
            self.story.append(P(referent.rue))
        self.story.append(P(f"{referent.npa} {referent.localite}"))
        self.story.append(Spacer(1, 1.8 * cm))
        self.story.append(P(f"Neuchâtel, le {django_format(date.today(), 'j F Y')}"))
        self.story.append(Spacer(1, 1.3 * cm))
        self.story.append(P("<b>Alarme Croix-Rouge</b>"))
        self.story.append(Spacer(1, 1 * cm))

        Pjust = partial(Paragraph, style=para_style)
        titre = referent.get_salutation_display() if (
            referent.salutation and referent.salutation != 'E'
        ) else 'Madame, Monsieur'
        self.story.append(Pjust(f'{titre},'))
        client_nom = f"{referent.client.titre} {referent.client.prenoms} {referent.client.noms}"
        verbe = 'peuvent' if referent.client.nom_part else 'peut'

        paras = [
            "Par la présente, nous vous informons qu’un appareil d’alarme Croix-Rouge a été "
            f"installé récemment chez {client_nom}, qui {verbe} désormais envoyer un "
            "appel à l’aide 24 heures sur 24 à une centrale d’alarme, par simple pression "
            "sur une touche placée sur un bracelet.",
            "Vous avez accepté de lui porter secours en cas de problème et votre nom est de "
            "ce fait enregistré auprès de la centrale d’alarme en qualité de répondant-e.",
            "<b>La centrale d’alarme vous appelle avec ce numéro : 043 255 34 00.</b>",
            "Nous vous remercions de votre disponibilité et attirons votre attention sur les "
            "points suivants :",
        ]
        for para in paras:
            self.story.append(Pjust(para))
        bull_paras = [
            "Veuillez vérifier que vous avez bien reçu les clés vous permettant "
            f"d’accéder au logement de {client_nom}.",
            "Si une alarme est déclenchée, il se peut que la centrale vous prie de vous "
            "rendre à son domicile. Dès que vous vous trouvez sur place, veuillez vous mettre "
            "en contact avec le collaborateur de la centrale par l’intermédiaire du haut-parleur "
            f"installé chez {client_nom} ou par téléphone au <b>058 105 04 18</b>. Vous pourrez ainsi "
            "indiquer si vous êtes en mesure de fournir l’aide nécessaire ou s’il convient de "
            "faire appel à un médecin ou à une ambulance. Si la liaison avec la centrale est "
            "interrompue, vous pouvez la rétablir en appuyant une nouvelle fois sur la touche "
            "d’alarme.",
            "Si vous deviez vous absenter plus de trois jours ou que vous déménagiez, merci "
            f"d’en avertir {client_nom} ainsi que la centrale dès que possible.",
        ]
        self.story.append(ListFlowable(
            [ListItem(Pjust(para), leftIndent=35) for para in bull_paras],
            bulletType='bullet'
        ))
        self.story.append(Pjust(
            "En vous remerciant une fois encore d’accepter cette tâche et d’offrir ainsi "
            f"à {client_nom} un peu plus de sécurité, nous restons à disposition pour tout "
            f"complément d’information et vous adressons, {titre}, nos cordiales salutations."
        ))
        self.story.append(Spacer(1, 0.8 * cm))
        self.story.append(P("Votre Croix-Rouge neuchâteloise"))

        self.doc.build(self.story, onFirstPage=self.draw_header_footer)
