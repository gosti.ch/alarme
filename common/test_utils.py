import json
import shutil
import tempfile
from pathlib import Path
from unittest import mock

import chromedriver_autoinstaller
import geckodriver_autoinstaller
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test.testcases import LiveServerThread


class TempMediaRootMixin:
    @classmethod
    def setUpClass(cls):
        cls._temp_media = tempfile.mkdtemp()
        cls._overridden_settings = cls._overridden_settings or {}
        cls._overridden_settings['MEDIA_ROOT'] = Path(cls._temp_media)
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls._temp_media)
        super().tearDownClass()


class MockResponse:
    def __init__(self, data, status_code):
        self.data = data
        self.status_code = status_code

    def json(self):
        return self.data

    @property
    def text(self):
        return self.data

    @property
    def content(self):
        return json.dumps(self.data).encode('utf-8')


def mocked_httpx(url, **kwargs):
    """Mock a httpx GET or POST request."""
    data = None
    if url.endswith('/oauth2/token'):
        data = {'access_token': 'abcdef'}
    elif b'Errorum Navision' in kwargs.get('data', b''):
        return MockResponse([{'fr:': "Erreur API Navision"}], 540)
    elif kwargs.get('json', {}).get('beneficiaryid', '') == 98765:
        return MockResponse(
            {'error': {'code': 400, 'message': 'Field "journal" is required in JSON dict and must have a value'}},
            200
        )
    elif '/payments/invoice/' in url:  # Call to Navision
        data = [{'provInvoiceCreated': 'some date?'}]
    elif url.endswith('/invoices') or url.endswith('/expenses'):  # Call to CID
        data = {'id': 1234}
    elif 'swisspost.opendatasoft.com' in url:
        data = {'records': [{'fields': {'postleitzahl': '2400', 'ortbez27': 'Le Locle', 'plz_typ': 20}}]}
    elif 'tel.search.ch' in url:
        sample_response = Path('transport/tests/telsearch-api-response.xml')
        data = sample_response.read_text()
    elif 'geo.admin.ch' in url:
        data = {'results': [{'attrs': {'lon': 1, 'lat': 2}}]}
    return MockResponse(data, 200)


class CustomLiveServerThread(LiveServerThread):
    """Custom server thread to avoid httpx network calls during tests."""
    def run(self):
        with mock.patch('httpx.get', side_effect=mocked_httpx):
            super().run()


class LiveTestBase(TempMediaRootMixin, StaticLiveServerTestCase):
    server_thread_class = CustomLiveServerThread
    #driver = 'firefox'
    driver = 'chromium'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        if cls.driver == 'firefox':
            with mock.patch('geckodriver_autoinstaller.utils.get_latest_geckodriver_version') as mocked:
                # Shortcut to avoid a network request at each run. To be checked periodically.
                mocked.return_value = 'v0.31.0'
                geckodriver_autoinstaller.install()
            cls.selenium = WebDriver()
        elif cls.driver == 'chromium':
            from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver

            with mock.patch('chromedriver_autoinstaller.utils.get_matched_chromedriver_version') as mocked:
                # Shortcut to avoid a network request at each run. To be checked periodically.
                mocked.return_value = '118.0.5993.70'
                chromedriver_autoinstaller.install()
            cls.selenium = ChromeDriver()
        cls.selenium.implicitly_wait(5)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def goto(self, url):
        self.selenium.get(f'{self.live_server_url}{url}')

    def login(self):
        self.goto(reverse('login'))
        username_input = self.selenium.find_element(By.NAME, value="username")
        username_input.send_keys(self.user.email)
        password_input = self.selenium.find_element(By.NAME, value="password")
        password_input.send_keys('mepassword')
        self.selenium.find_element(By.XPATH, value='//button[@type="submit"]').click()

    def find_and_scroll_to(self, *args):
        element = self.selenium.find_element(*args)
        # scroll_to_element is buggy until Firefox 106 (https://github.com/SeleniumHQ/selenium/issues/11059)
        #ActionChains(self.selenium).scroll_to_element(element).perform()
        self.selenium.execute_script("window.scrollTo({left: 0, top: document.body.scrollHeight, behavior: 'instant'})")
        return element
