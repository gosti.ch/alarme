from django.apps import apps
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion

model_migration =  migrations.CreateModel(
    name='Alerte',
    fields=[
        ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
        ('fichier', models.FileField(blank=True, upload_to='alertes', verbose_name='Fichier')),
        ('alerte', models.TextField(blank=True)),
        ('cible', models.CharField(choices=[
            ('alarme', 'Alarme'), ('transport', 'Transport'), ('visite', 'Visite'), ('savd', 'SAVD')
        ], max_length=10)),
        ('recu_le', models.DateTimeField(verbose_name='Reçu le')),
        ('traite_le', models.DateTimeField(blank=True, null=True, verbose_name='Traité le')),
        ('remarque', models.TextField(blank=True)),
        ('rel_object_id', models.PositiveIntegerField(blank=True, null=True)),
        ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='alertes', to='client.client')),
        ('par', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
        ('rel_content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
    ],
    options={
        'db_table': 'alarme_alerte',
        'get_latest_by': ['recu_le'],
        'indexes': [models.Index(fields=['cible'], name='index_cible')],
    },
)


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0009_alter_referent_facturation_pour'),
    ]

    if apps.is_installed('alarme'):
        operations = [
            migrations.SeparateDatabaseAndState(
                state_operations=[model_migration],
                database_operations=[],
            ),
        ]
    else:
        operations = [model_migration]
