from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.platypus import KeepTogether, Paragraph, Spacer, Table, TableStyle

from django.utils.dateformat import format as django_format
from django.utils.timezone import localtime

from common.pdf import BaseCroixrougePDF
from common.templatetags.common_utils import format_duree


class TransportsPDF(BaseCroixrougePDF):
    FONTSIZE = 11
    title = "Liste de transports"

    def produce(self, chauffeur, transports, date_de, date_a):
        """
        `chauffeur` peut valoir None, ce qui fera imprimer le nom du chauffeur pour chaque transport.
        """
        line1_style = ParagraphStyle(
            name='line1', fontName='Helvetica', fontSize=self.FONTSIZE, leading=self.FONTSIZE + 1,
            borderWidth=0.5, borderColor='#000000', borderPadding=(2, 2, 2), spaceAfter=3
        )
        self.story.append(Spacer(1, 1 * cm))
        chauff_str = f"pour {chauffeur.nom} {chauffeur.prenom}" if chauffeur else ""
        self.story.append(Paragraph(
            f"Transports Croix-Rouge {chauff_str}<br/>"
            f"du {django_format(date_de, 'd.m.Y')} au {django_format(date_a, 'd.m.Y')}",
            style=self.style_title
        ))
        self.story.append(Spacer(1, 1 * cm))

        def format_adr(adresse):
            adr = ""
            if getattr(adresse, 'nom', ''):
                adr += f"{adresse.nom}<br/>"
            adr += f"{adresse.rue}<br/>"
            adr += f"{adresse.npa} {adresse.localite}"
            if getattr(adresse, 'tel', ''):
                adr += f" ({adresse.tel})"
            return adr

        tstyle = TableStyle([
            ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ('LEFTPADDING', (0, 0), (-1, -1), 0),
        ])
        for transp in transports:
            transp_story = []
            tels = "/".join([tel for tel in [transp.client.tel_1, transp.client.tel_2] if tel])
            transp_story.append(Paragraph(
                f"{django_format(transp.heure_depart, 'l d.m.Y')} pour <b>{transp.client}</b> ({tels})",
                line1_style
            ))
            if transp.client.remarques_ext:
                transp_story.append(Paragraph(transp.client.remarques_ext, self.style_normal))
            for handi in transp.client.handicaps_verbose:
                transp_story.append(Paragraph(f"☑ {handi}", self.style_normal))

            if not chauffeur:
                transp_story.append(
                    Paragraph(f"Chauffeur: {transp.chauffeur.nom} {transp.chauffeur.prenom}", self.style_normal)
                )
            dep_table = [[
                Paragraph(f"Dép. {django_format(transp.heure_depart, 'H:i')}", self.style_normal),
                Paragraph(format_adr(transp.adresse_depart()), self.style_normal)
            ]]
            transp_story.append(Table(data=dep_table, colWidths=[2.5 * cm, 15 * cm], style=tstyle))

            trajets = transp.trajets_tries
            for idx, trajet in enumerate(trajets):
                is_last = idx == len(trajets) - 1
                if trajet.destination_princ:
                    dest_table = [[
                        Paragraph(f"Rdv {django_format(localtime(transp.heure_rdv), 'H:i')}", self.style_normal),
                        Paragraph(format_adr(transp.destination), self.style_normal)
                    ]]
                    transp_story.append(Table(data=dest_table, colWidths=[2.5 * cm, 15 * cm], style=tstyle))
                    if transp.retour:
                        transp_story.append(Paragraph(
                            f"Attente prévue: {format_duree(transp.duree_rdv)}", self.style_normal
                        ))
                elif idx != 0 and not is_last:
                    dest_table = [[
                        Paragraph("En passant par", self.style_normal),
                        Paragraph(format_adr(trajet.destination), self.style_normal)
                    ]]
                    transp_story.append(Table(data=dest_table, colWidths=[2.5 * cm, 15 * cm], style=tstyle))
            if transp.retour:
                transp_story.append(Paragraph(
                    f"Retour vers {django_format(transp.heure_depart_retour, 'H:i')}", self.style_normal
                ))
            elif transp.vers_domicile:
                transp_story.append(Paragraph("RETOUR À DOMICILE", self.style_normal))
            else:
                transp_story.append(Paragraph("ALLER SIMPLE", self.style_normal))
            if transp.remarques:
                transp_story.append(Paragraph(transp.remarques, self.style_normal))

            self.story.append(KeepTogether(transp_story))
            self.story.append(Spacer(1, 0.8 * cm))

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer,
            onLaterPages=self.head_foot_class.draw_footer,
        )
