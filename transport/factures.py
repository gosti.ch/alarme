from decimal import Decimal
from functools import partial
from io import BytesIO, StringIO

from reportlab.lib.colors import black
from reportlab.lib.enums import TA_LEFT, TA_RIGHT
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.platypus import (
    BaseDocTemplate, Frame, Image, KeepTogether, NextPageTemplate, PageBreak,
    PageTemplate, Paragraph, Spacer, Table, TableStyle
)

from qrbill import QRBill
from svglib.svglib import svg2rlg

from django.conf import settings
from django.db.models import Sum
from django.utils.dateformat import format as django_format
from django.utils.text import slugify
from django.utils.timezone import localtime

from common.pdf import BaseCroixrougePDF
from common.utils import format_HM, tva_et_arrondi
from .models import Facture, Transport


class FactureDocTemplate(BaseDocTemplate):
    def handle_pageBegin(self):
        '''override base method to add a change of page template after the firstpage. (copied from SimpleDocTemplate)
        '''
        self._handle_pageBegin()
        self._handle_nextPageTemplate('Later')

    def build(self, flowables, onFirstPage=None, onLaterPages=None, **kwargs):
        self._calc()    #in case we changed margins sizes etc
        frameT = Frame(self.leftMargin, self.bottomMargin, self.width, self.height, id='normal')
        frameTLast = Frame(
            0, 0, self.pagesize[0], 300, id='nomargin',
            leftPadding=0, bottomPadding=0, rightPadding=0, topPadding=0
        )
        self.addPageTemplates([
            PageTemplate(id='First',frames=frameT, onPage=onFirstPage, pagesize=self.pagesize),
            PageTemplate(id='Later',frames=frameT, onPage=onLaterPages, pagesize=self.pagesize),
            PageTemplate(id='Last', frames=frameTLast, pagesize=self.pagesize),
        ])
        super().build(flowables)#, **kwargs)#canvasmaker=canvasmaker)


class FacturePDF(BaseCroixrougePDF):
    title = "Facture transports"
    left_margin = 2 * cm
    font_small = 7.5

    def init_doc(self, tampon):
        return FactureDocTemplate(
            tampon, title=self.title, pagesize=self.page_size,
            leftMargin=self.left_margin, rightMargin=self.right_margin,
            topMargin=self.top_margin, bottomMargin=self.bottom_margin,
            onFirstPage=self.draw_header_footer, onLaterPage=self.draw_later,
        )

    def _draw_top_line(self, canvas, doc):
        canvas.saveState()
        canvas.setFont("Helvetica", 8)
        canvas.drawString(doc.leftMargin, doc.height + 120, "Pour toute question, appeler le 032 886 82 32")

    def draw_header_footer(self, canvas, doc):
        self._draw_top_line(canvas, doc)
        super().draw_header_footer(canvas, doc)

    def draw_later(self, canvas, doc):
        self._draw_top_line(canvas, doc)
        self.head_foot_class.draw_footer(canvas, doc)

    def produce(self, transports, no_facture, date_facture):
        Pbold = partial(Paragraph, style=ParagraphStyle(
            name='bold', fontName='Helvetica-Bold', fontSize=8, leading=9
        ))
        Pboldr = partial(Paragraph, style=ParagraphStyle(
            name='boldr', fontName='Helvetica-Bold', fontSize=8, leading=9, alignment=TA_RIGHT,
        ))
        P = partial(Paragraph, style=ParagraphStyle(
            name='normal', fontName='Helvetica', fontSize=self.font_small, leading=self.font_small + 1,
        ))
        Pr = partial(Paragraph, style=ParagraphStyle(
            name='normalr', fontName='Helvetica', fontSize=self.font_small, leading=self.font_small + 1,
            alignment=TA_RIGHT
        ))
        client = transports[0].client
        debiteur = client.adresse_facturation(transports[0])
        self.story.append(Spacer(1, 2.6 * cm))
        lignes_adresse = [
            f'<b>{debiteur.nom} {debiteur.prenom}</b>',
            debiteur.rue,
            f'{debiteur.npa} {debiteur.localite}',
        ]
        if compl := getattr(debiteur, 'complement', ''):
            lignes_adresse.insert(1, compl)
        table_data = [[
            Paragraph('<br/>'.join(lignes_adresse), self.style_normal),
            Paragraph('<para align="right">%s</para>' % django_format(date_facture, 'd F Y'), self.style_normal),
        ]]
        self.story.append(Table(
            data=table_data, colWidths=[8.5 * cm, 8.5 * cm],
            style=TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ]),
        ))

        self.story.append(Spacer(1, 1.8 * cm))
        self.story.append(Paragraph("Service des transports CRS", ParagraphStyle(
            name='title', fontName='Helvetica', fontSize=self.FONTSIZE + 8,
        )))
        self.story.append(Spacer(1, 1 * cm))
        self.story.append(Paragraph(
            f'<para fontSize="12" autoleading="max">N° de facture {no_facture}</para>', self.style_normal
        ))
        self.story.append(Spacer(1, 0.3 * cm))
        self.story.append(Pbold(
            f"{client.no_debiteur} {client.nom} {client.prenom}, {client.rue}, {client.npa} {client.localite}"
        ))
        sous_total = Decimal('0')
        tva_str = str(float(settings.TAUX_TVA * 100))
        num_transp_factures = 0
        for transport in transports:
            fact = transport.donnees_facturation()
            if fact is None:
                continue
            num_transp_factures += 1
            self.story.append(Spacer(1, 0.3 * cm))
            aller_ret = Transport.aller_ret_map.get(transport.retour)
            sous_total += fact['total']
            table_data = [
                [fact['no'], django_format(fact['rendez-vous'], 'd.m.Y H:i'), fact['type'], '', ''],
                [aller_ret['label'], P(fact['depart']), P(fact['destination']), '', ''],
            ]
            if fact.get('km'):
                tarif_km = transport.tarif_km
                table_data.append([
                    '', 'Tarif au km (AVS)' if fact['avs'] else 'Tarif au km (Non AVS)',
                    f'CHF {tarif_km}/km',
                    f'{fact["km"]} km', fact['cout_km']
                ])
                if 'cout_forfait' in fact:
                    table_data.append([
                        '', f"Forfait {aller_ret['label'].lower()}", f"CHF {fact['cout_forfait']}",
                        '', fact['cout_forfait']
                    ])
            if 'annulation' in fact:
                table_data.append(
                    ['', 'Annulation de dernière minute', f"CHF {fact['annulation']}", '', fact['annulation']]
                )
            if 'cout_attente' in fact:
                table_data.append(
                    ['', 'Facturation de temps d’attente',
                     P(f"CHF {fact['cout_attente']}<br/>par demi-heure, à partir de 1h30"),
                     format_HM(transport.temps_attente), fact['cout_attente']
                    ]
                )
            for frais in fact.get('frais', []):
                table_data.append(
                    ['', frais['descr'], '', '', frais['cout']]
                )
            table_data.append(
                [Pbold('Montant total (Hors TVA)'), '', '', '', Pboldr(str(fact['total']))],
            )
            num_lines = len(table_data)
            self.story.append(KeepTogether([Table(
                data=table_data, colWidths=[3.2 * cm, 5.3 * cm, 5.3 * cm, 1.9 * cm, 1.2 * cm],
                style=TableStyle([
                    #('GRID', (0, 0), (-1, -1), 1, black),
                    ('LEFTPADDING', (0, 0), (-1, -1), 0),
                    ('RIGHTPADDING', (4, 0), (-1, -1), 0),
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
                    ('FONTSIZE', (0, 0), (-1, -1), self.font_small),
                    ('VALIGN', (0, 0), (-1, -1), "TOP"),
                    ('ALIGN', (4, 0), (4, -1), "RIGHT"),
                    ('SPAN', (0, -1), (1, -1)),
                    ('LINEABOVE', (0, num_lines - 1),(-1, num_lines - 1), 0.5, black),
                ]),
            )]))
            self.story.append(Spacer(1, 0.3 * cm))

        if sous_total == 0:
            return None
        self.story.append(Spacer(1, 0.5 * cm))
        total_tva, diff_arrond = tva_et_arrondi(sous_total)
        self.total_facture = sous_total + total_tva + diff_arrond
        table_data = [
            [Pbold('Sous-total'), '', Pboldr(str(sous_total))],
            [P(f'TVA {tva_str}% (N° IDE CHE - 108.099.592 TVA)'), '', Pr(str(total_tva))],
        ]
        if diff_arrond:
            table_data.append(
                [P('Différence d’arrondi'), '', Pr(str(diff_arrond))]
            )
        table_data.append(
            [Pbold('Total avec TVA'), '', Pboldr(str(self.total_facture))]
        )
        final_block = [
            Table(
                data=table_data, colWidths=[11.5 * cm, 2 * cm, 2 * cm],
                style=TableStyle([
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                    ('ALIGN', (1, 0), (1, -1), "RIGHT"),
                ])
            ),
            Spacer(1, 1.2 * cm),
            Pbold("Paiement à 30 jours net"),
        ]
        self.story.append(KeepTogether(final_block))

        self.story.append(NextPageTemplate('Last'))
        self.story.append(PageBreak())

        temp = StringIO()
        esr_num = Facture.esr_num(no_facture, client.no_debiteur)
        infos = "Facture transports %s" % django_format(transports[0].heure_rdv, 'F Y')
        if debiteur is not client:
            infos += f" - {client.nom_prenom}"
        bill = QRBill(
            account=settings.FACTURE_IBAN,
            creditor={
                'name': 'Croix-Rouge suisse du canton de Neuchâtel',
                'street': 'Avenue du Premier-Mars 2a',
                'pcode': '2000', 'city': 'Neuchâtel', 'country': 'CH',
            },
            debtor={
                'name': debiteur.nom_prenom, 'street': debiteur.rue,
                'pcode': debiteur.npa, 'city': debiteur.localite, 'country': 'CH',
            },
            amount=self.total_facture,
            reference_number=esr_num,
            additional_information=infos,
            language='fr',
        )
        bill.as_svg(temp)
        temp.seek(0)
        drawing = svg2rlg(BytesIO(temp.getvalue().encode('utf-8')))

        img = Image(drawing, width=round(drawing.width), height=round(drawing.height))
        self.story.append(img)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer,
            onLaterPages=self.head_foot_class.draw_footer,
            #canvasmaker=PageNumCanvas
        )
        return {
            'total': self.total_facture,
            'debiteur': debiteur,
            'nb_transp': num_transp_factures,
        }


class RemboursementPDF(FacturePDF):
    """Fiche de remboursement mensuel des frais par chauffeur."""
    title = "Remboursement de frais"

    def get_filename(self, chauffeur):
        return (
            f'remboursements_{slugify(" ".join([chauffeur.nom, chauffeur.prenom]))}'
            f'_{self.month.year}_{self.month.month}.pdf'
        )


    def produce(self, chauffeur, month=None, transports=None):
        self.month = month
        self.story.append(Spacer(1, 2.6 * cm))
        lignes_adresse = [
            f'<b>{chauffeur.nom} {chauffeur.prenom}</b>',
            chauffeur.rue,
            f'{chauffeur.npa} {chauffeur.localite}',
        ]
        table_data = [[
            Paragraph('<br/>'.join(lignes_adresse), self.style_normal),
            Paragraph('<para align="right">%s</para>' % django_format(month, 'F Y'), self.style_normal),
        ]]
        self.story.append(Table(
            data=table_data, colWidths=[8.5 * cm, 8.5 * cm],
            style=TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 0),
                ('RIGHTPADDING', (0, 0), (-1, -1), 0),
                ('VALIGN', (0, 0), (-1, -1), "TOP"),
            ]),
        ))
        self.story.append(Spacer(1, 2 * cm))

        style_h1 = ParagraphStyle(
            name='title', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 4, leading=self.FONTSIZE + 5,
            alignment=TA_LEFT
        )
        P = partial(Paragraph, style=ParagraphStyle(
            name='normal', fontName='Helvetica', fontSize=self.font_small, leading=self.font_small + 1,
        ))
        Pr = partial(Paragraph, style=ParagraphStyle(
            name='normalr', fontName='Helvetica', fontSize=self.font_small, leading=self.font_small + 1,
            alignment=TA_RIGHT
        ))
        Pbold = partial(Paragraph, style=ParagraphStyle(
            name='bold', fontName='Helvetica-Bold', fontSize=8, leading=9
        ))
        Pboldr = partial(Paragraph, style=ParagraphStyle(
            name='boldr', fontName='Helvetica-Bold', fontSize=8, leading=9, alignment=TA_RIGHT,
        ))
        self.story.append(Paragraph(f"{self.title} - {django_format(month, 'F Y')}", style_h1))
        self.story.append(Spacer(1, 0.5 * cm))

        sous_total = Decimal('0')
        total_km = Decimal('0')
        kms_sur_annee = chauffeur.notefrais_set.filter(
            mois__year=month.year
        ).aggregate(total_kms=Sum('kms'))['total_kms'] or Decimal('0')
        for transport in transports:
            tr_total = Decimal('0')
            aller_ret = Transport.aller_ret_map.get(transport.retour)
            date_heure = django_format(localtime(transport.heure_rdv), 'd.m.Y H:i')
            table_data = [
                [transport.pk, f'{date_heure}, pour {transport.client.nom_prenom}', '', '', ''],
                [aller_ret['label'], P(str(transport.adresse_depart())), P(str(transport.destination)), '', ''],
            ]
            if transport.km:
                km = max(transport.km, transport.MIN_KM_FACTURES)
                kms_sur_annee += km
                tarif_km = settings.TARIF_CHAUFFEUR[0] if kms_sur_annee <= 6000 else settings.TARIF_CHAUFFEUR[6001]
                cout_km = round(km * tarif_km, 2)
                tr_total += cout_km
                total_km += km
                table_data.append([
                    '', 'Indemnisation kilométrique',
                    f'CHF {tarif_km}/km TVA incluse 0%',
                    f'{km} km', cout_km
                ])
            _, cout_attente = transport.cout_attente()
            if cout_attente:
                tr_total += cout_attente
                table_data.append(
                    ['', 'Frais de temps d’attente',
                     P(f"CHF {Transport.COUT_ATTENTE_UNITE}<br/>par demi-heure, à partir de 1h30"),
                     format_HM(transport.temps_attente), cout_attente
                    ]
                )
            for frais in transport.frais.all():
                if not frais.cout:
                    continue
                tr_total += frais.cout
                table_data.append(
                    ['', frais.description(), '', '', frais.cout]
                )
            table_data.append(
                [Pbold('Montant total (Hors TVA)'), '', '', '', Pboldr(str(tr_total))],
            )
            sous_total += tr_total
            num_lines = len(table_data)
            self.story.append(KeepTogether([Table(
                data=table_data, colWidths=[3.2 * cm, 5.3 * cm, 5.3 * cm, 1.9 * cm, 1.2 * cm],
                style=TableStyle([
                    #('GRID', (0, 0), (-1, -1), 1, black),
                    ('LEFTPADDING', (0, 0), (-1, -1), 0),
                    ('RIGHTPADDING', (4, 0), (-1, -1), 0),
                    ('TOPPADDING', (0, 0), (-1, -1), 1),
                    ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
                    ('FONTSIZE', (0, 0), (-1, -1), self.font_small),
                    ('VALIGN', (0, 0), (-1, -1), "TOP"),
                    ('ALIGN', (4, 0), (4, -1), "RIGHT"),
                    ('SPAN', (0, -1), (1, -1)),
                    ('LINEABOVE', (0, num_lines - 1),(-1, num_lines - 1), 0.5, black),
                ]),
            )]))
            self.story.append(Spacer(1, 0.3 * cm))

        diff_arrond = (round(sous_total * Decimal(20)) / Decimal(20)) - sous_total
        total_facture = sous_total + diff_arrond
        table_data = [
            [P('Total km'), P(f'{total_km} km'), ''],
            [Pbold('Sous-total'), '', Pboldr(str(sous_total))],
            [P('TVA 0% (N° IDE CHE - 108.099.592 TVA)'), '', Pr('0.00')],
        ]
        if diff_arrond:
            table_data.append(
                [P('Différence d’arrondi'), '', Pr(str(diff_arrond))]
            )
        table_data.append(
            [Pbold('Total en votre faveur'), '', Pboldr(str(total_facture))]
        )
        final_block = Table(
            data=table_data, colWidths=[13.3 * cm, 2 * cm, 2 * cm],
            style=TableStyle([
                ('TOPPADDING', (0, 0), (-1, -1), 1),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
                ('ALIGN', (1, 0), (1, -1), "RIGHT"),
            ])
        )
        self.story.append(final_block)

        self.doc.build(
            self.story,
            onFirstPage=self.draw_header_footer,
            onLaterPages=self.head_foot_class.draw_footer,
        )
