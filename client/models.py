from datetime import date, timedelta
from operator import attrgetter

from django.apps import apps
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import models, transaction
from django.db.models import Count, Min, Max, OuterRef, Prefetch, Q, Subquery
from django.dispatch import Signal
from django.urls import NoReverseMatch, reverse
from django.utils import timezone

from common.choices import Genres, Handicaps, Languages
from common.fields import ChoiceArrayField, PhoneNumberField
from common.models import GeolocMixin, Fichier, TypeFichier, Utilisateur
from common.utils import canton_app, same_month

# Un signal similaire à post_save, mais envoyé par ReferentEditView, et où les
# instances ReferentTel sont enregistrées.
referent_saved = Signal()


class ClientQuerySet(models.QuerySet):
    def actifs_entre(self, date_deb, date_fin):
        from alarme.models import Installation
        # Sous-requête pour obtenir l'installation la plus récente
        installs = Installation.objects.filter(
            client=OuterRef('pk')
        ).order_by('-date_debut')
        return self.annotate(
            debut_install=Min('installation__date_debut'),
            fin_install=Max('installation__date_fin_abo'),
            en_cours=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
            modele=Subquery(installs.values('alarme__modele__nom')[:1]),
        ).filter(
            Q(debut_install__lte=date_fin) & (
                Q(en_cours__gt=0) | Q(fin_install__gte=date_deb)
            )
        )

    def en_age_avs(self):
        year_avs_m = date.today().year - 65
        year_avs_f = date.today().year - 64
        return self.filter(
            Q(tarif_avs_acquis=True) |
            (Q(genre='M') & Q(date_naissance__year__lte=year_avs_m)) |
            (Q(genre='F') & Q(date_naissance__year__lte=year_avs_f))
        )

    def hospitalises(self):
        return self.filter(pk__in=AdressePresence.objects.filter(
            Q(adresse__hospitalisation=True) & (
                Q(jusqua__isnull=True) | (Q(jusqua__gt=date.today()) & Q(depuis__lt=date.today()))
            )
        ).values_list('adresse__client__pk'))


class ClientActifManager(models.Manager):
    def get_queryset(self):
        return ClientQuerySet(self.model, using=self._db).filter(archive_le__isnull=True)


def default_client_type():
    return []


class ClientType(models.TextChoices):
    ALARME = 'alarme', "Alarme"
    TRANSPORT = 'transport', "Transport"
    VISITE = 'visite', "Visite"
    SAVD = 'savd', "SAVD"


class Client(GeolocMixin, models.Model):
    BOITIER_CHOICES = (
        ('prive', 'Privé'),
        ('c-r', 'Croix-Rouge'),
    )
    TYPE_LOGEMENT_CHOICES = (
        ('villa', "Villa"),
        ('appartement', "Appartement"),
        ('resid', "Institution/App. avec encadrement"),
    ) + canton_app.EXTRA_LOGEMENT_CHOICES
    DONNEES_MEDIC_MAP = {
        'troubles_eloc': 'Troubles de l’élocution',
        'troubles_aud': 'Troubles auditifs',
        'allergies': 'Allergies',
        'empl_medic': 'Emplacement des principaux médicaments (p. ex. antiallergiques)',
        'diabete': 'Diabète',
        'troubles_cardio': "Troubles cardiovasculaires",
        'epilepsie': "Epilepsie",
        'oxygene': "Oxygène",
        'anticoag': "Anticoagulant",
        'directives': "Directives anticipées, lieu de dépôt",
        'autres': "Autres données importantes",
    }
    SITUATION_VIE_SEUL = "Seul-e"
    SITUATION_VIE_COUPLE = "En couple"

    id_externe = models.BigIntegerField(null=True, blank=True)
    type_client = ChoiceArrayField(
        models.CharField(max_length=10, choices=ClientType.choices),
        default=default_client_type
    )
    nom = models.CharField("Nom", max_length=60)
    prenom = models.CharField("Prénom", max_length=50)
    genre = models.CharField("Genre", max_length=1, choices=Genres.choices, blank=True)
    date_naissance = models.DateField("Date de naissance", blank=True, null=True)
    nom_part = models.CharField("Nom du partenaire", max_length=50, blank=True)
    prenom_part = models.CharField("Prénom du partenaire", max_length=50, blank=True)
    genre_part = models.CharField("Genre du partenaire", max_length=1, choices=Genres.choices, blank=True)
    date_naissance_part = models.DateField("Date de naissance du partenaire", blank=True, null=True)
    tel_part = PhoneNumberField("Tél. du partenaire", blank=True)
    c_o = models.CharField("C/o", max_length=80, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    case_postale = models.CharField("Case postale", max_length=20, blank=True)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    # Not using postgis and PointField, as no geometry calculation is done in the db.
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    tel_1 = PhoneNumberField("Tél. principal", blank=True)
    tel_2 = PhoneNumberField("Tél. secondaire", blank=True)
    courriel = models.EmailField("Courriel", blank=True)
    langues = ChoiceArrayField(
        models.CharField(max_length=2, choices=Languages.choices, blank=True),
        blank=True, null=True
    )
    type_logement = models.CharField("Type de logement", choices=TYPE_LOGEMENT_CHOICES, max_length=12, blank=True)
    type_logement_info = models.CharField("Type de logement", max_length=60, blank=True)
    etage = models.CharField("Étage/niveau", max_length=20, blank=True)
    ascenseur = models.BooleanField("Ascenseur", default=False)
    code_entree = models.CharField("Code entrée immeuble", max_length=20, blank=True)
    nb_pieces = models.CharField("Nbre de pièces", max_length=50, blank=True)
    boitier_cle = models.CharField("Boîtier à clé", max_length=10, choices=BOITIER_CHOICES, blank=True)
    cles = models.CharField("Infos clés", max_length=250, blank=True)
    situation_vie = models.CharField("Situation de vie", max_length=80, blank=True)
    donnees_medic = models.JSONField("Données médicales", blank=True, null=True)
    prest_compl = models.BooleanField("Prestations complémentaires", default=False)
    no_debiteur = models.CharField("N° de débiteur", max_length=30, blank=True)
    no_centrale = models.CharField("N° client de la centrale", max_length=10, blank=True)
    envoi_facture = models.BooleanField("Envoyer une facture", default=True)
    animal_comp = models.CharField("Animal de compagnie", max_length=50, blank=True)
    samaritains = models.PositiveSmallIntegerField("Samaritains comme répondants", blank=True, null=True)
    professionnels = models.ManyToManyField('Professionnel', through='ProfClient', blank=True)
    visiteur = models.ForeignKey(
        'benevole.Benevole', on_delete=models.SET_NULL, null=True, blank=True,
        verbose_name="Bénévole visiteur", related_name="visites"
    )
    spec_alarme = models.TextField("Spécificités du système d'alarme", blank=True)
    remarques_int_alarme = models.TextField("Remarques internes (alarme)", blank=True)
    remarques_int_transport = models.TextField("Remarques internes (transports)", blank=True)
    remarques_int_visite = models.TextField("Remarques internes (visites)", blank=True)
    remarques_ext = models.TextField("Remarques (visibles par bénévoles)", blank=True)
    cree_le = models.DateTimeField("Créé le", auto_now_add=True)
    courrier_envoye = models.DateField(blank=True, null=True)
    archive_le = models.DateField("Archivé le", blank=True, null=True)
    date_deces = models.DateField("Décès le", blank=True, null=True)
    # Pour alarme JU:
    accord_police = models.BooleanField("Accord avant d’envoyer police ou ambulance", default=False)
    # Pour transports:
    handicaps = ChoiceArrayField(
        models.CharField(max_length=10, choices=Handicaps.choices, blank=True),
        blank=True, null=True, verbose_name='Handicaps'
    )
    tarif_avs_acquis = models.BooleanField("Tarif AVS acquis", default=False)
    fonds_transport = models.BooleanField("Fonds Transport handicap", default=False)
    fichiers = GenericRelation(Fichier)

    objects = ClientQuerySet.as_manager()
    actifs = ClientActifManager()

    class Meta:
        db_table = 'alarme_client'
        constraints = [
            models.UniqueConstraint(fields=['id_externe'], name="client_id_externe_unique"),
        ]

    def __str__(self):
        return self.nom_prenom

    def get_absolute_url(self):
        try:
            return (
                reverse('transport-client-detail', args=[self.pk]) if apps.is_installed('transport')
                else reverse('client-edit', args=[self.pk])
            )
        except NoReverseMatch:
            return None

    def can_be_archived(self, user, for_app):
        if self.pk is None or not user.has_perm('client.change_client'):
            return False
        if for_app == 'alarme':
            alarme = self.derniere_alarme()
            if alarme and alarme.retour_mat and alarme.retour_mat < date.today() - timedelta(days=31):
                return True
            elif not alarme and not self.mission_set.filter(effectuee__isnull=True).exists():
                return True
            return False
        elif for_app == 'visite':
            if self.attributions.filter(fin__isnull=True).exists():
                return False
        return True

    def archiver(self, user, for_app, check=True):
        if check and not self.can_be_archived(user, for_app):
            raise PermissionDenied("Il n’est pas possible d’archiver ce client")
        with transaction.atomic():
            if set(self.type_client) in [{for_app}, {for_app, ClientType.SAVD}]:
                self.archive_le = date.today()
                self.save(update_fields=['archive_le'])
                # Pas de update(), sinon le signal de sync referent_saved n'est pas lancé
                for referent in self.referent_set.all():
                    referent.date_archive = date.today()
                    referent.save(update_fields=['date_archive'])
                    referent_saved.send(sender=Referent, instance=referent, created=False)
                journal_msg = "Le client a été archivé"
            else:
                # Le client est aussi client d'une autre app, alors seulement enlever l'app dans type_client.
                self.type_client.remove(for_app)
                self.save(update_fields=['type_client'])
                journal_msg = f"Le client a été archivé pour l’application «{for_app}»"
            Journal.objects.create(
                client=self, description=journal_msg, quand=timezone.now(), qui=user
            )

    def desarchiver(self, user, for_app):
        # Désarchiver
        with transaction.atomic():
            # Désarchiver les référents archivés le même jour que le client
            for referent in self.referent_set.filter(date_archive=self.archive_le):
                referent.date_archive = None
                referent.save(update_fields=['date_archive'])
                referent_saved.send(sender=Referent, instance=referent, created=False)
            self.archive_le = None
            self.type_client = [for_app]
            self.save()
            Journal.objects.create(
                client=self, description="Le client a été réactivé et retiré des archives",
                quand=timezone.now(), qui=user
            )

    @property
    def titre(self):
        return "/".join(
            [genre for genre in [
                {'F': 'Madame', 'M': 'Monsieur'}.get(self.genre, None),
                {'F': 'Madame', 'M': 'Monsieur'}.get(self.genre_part, None)
            ] if genre]
        )

    @property
    def nom_prenom(self):
        if self.nom_part:
            if self.nom == self.nom_part:
                return " ".join([self.nom, self.prenom, 'et', self.prenom_part])
            else:
                return " ".join([self.nom, self.prenom, 'et', self.nom_part, self.prenom_part])
        else:
            return " ".join([self.nom, self.prenom])

    @property
    def noms(self):
        noms = [self.nom]
        if self.nom_part and self.nom_part != self.nom:
            noms.append(self.nom_part)
        return " et ".join(noms)

    @property
    def prenoms(self):
        return " et ".join([p for p in [self.prenom, self.prenom_part] if p])

    @property
    def handicaps_verbose(self):
        return [dict(Handicaps.choices)[v] for v in (self.handicaps or [])]

    def get_handicaps_display(self):
        return ', '.join(self.handicaps_verbose)

    @property
    def langues_verbose(self):
        return [dict(Languages.choices)[v] for v in (self.langues or [])]

    def get_langues_display(self):
        return ', '.join(self.langues_verbose)

    def get_donnees_medic_display(self):
        if not self.donnees_medic:
            return ""

        def serialize(val):
            return {'': "-", False: "Non", True: "Oui", None: '?'}.get(val, val)

        return "\n".join([
            f'{label}: {serialize(self.donnees_medic[key])}'
            for key, label in self.DONNEES_MEDIC_MAP.items() if key in self.donnees_medic
        ])

    def fichiers_app(self, app):
        types = {
            'alarme': [TypeFichier.DOC_GENERAL, TypeFichier.QUEST_ALARME, TypeFichier.CONTRAT_ALARME],
        }.get(app, [TypeFichier.DOC_GENERAL])
        return self.fichiers.filter(typ__in=types)

    def age(self, quand=None):
        if not self.date_naissance:
            return None
        age = ((quand or date.today()) - self.date_naissance).days / 365.25
        return int(age * 10) / 10  # 1 décimale arrondi vers le bas

    def is_avs(self, quand=None):
        """
        Indique si client en âge AVS selon normes OFAS
        (année civile durant laquelle la personne atteint l'âge de la retraite).
        """
        if self.tarif_avs_acquis:
            return True
        age = self.age((quand or date.today()).replace(month=12, day=31))
        return age >= 65 if self.genre == 'M' else age >= 64

    def can_read(self, user):
        return (
            user.benevole.has_intervention_on(self) if user.is_benevole
            else user.has_perm('client.view_client')
        )

    def can_edit(self, user):
        return user.has_perm('client.change_client') or (
            user.is_benevole and user.benevole.has_intervention_on(self, types=['NEW'])
        )

    def autres_adresses(self):
        return self.adresseclient_set.all().prefetch_related('presences') if self.pk else []

    def presence_actuelle(self):
        return next((adr.actuelle for adr in self.autres_adresses() if adr.actuelle), None)

    def alarme_actuelle(self):
        return self.installation_set.filter(
            Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
        ).select_related('alarme').first()

    def derniere_alarme(self):
        if 'installation_set' in getattr(self, '_prefetched_objects_cache', {}):
            # Avoid access to DB
            return next(iter(sorted(
                self._prefetched_objects_cache['installation_set'],
                key=attrgetter('date_debut'), reverse=True
            )), None)
        try:
            return self.installation_set.select_related(
                'alarme', 'alarme__modele', 'abonnement'
            ).latest('date_debut')
        except ObjectDoesNotExist:
            return None

    def date_resiliation(self):
        alarme = self.derniere_alarme()
        return alarme.date_fin_abo if alarme else None

    def materiel_actuel(self):
        return self.materielclient_set.filter(
            Q(date_fin_abo__isnull=True) | Q(date_fin_abo__gt=date.today())
        )

    def has_facture_for_month(self, date_, article):
        # Using factures.all() to take advantage of Prefetch.
        return any([
            same_month(fact.mois_facture, date_) and article == fact.article
            for fact in self.factures.all()
        ])

    @property
    def referents(self):
        return self.referent_set.filter(
            Q(date_archive=None) | Q(date_archive__gt=date.today())
        ).prefetch_related(
            Prefetch('referenttel_set', queryset=ReferentTel.objects.order_by('priorite'))
        )

    def contractant(self):
        """Return self or any referent with contractant boolean set."""
        return next(iter([ref for ref in self.referents if ref.contractant]), self)

    def adresse_facturation(self, facture):
        potentials = sorted(
            [tpl for tpl in [(facture.match(ref), ref) for ref in self.referents] if tpl[0] > 0],
            reverse=True
        )
        return potentials[0][1] if potentials else self

    def repondants(self):
        repondants = [ref for ref in self.referents if ref.repondant]
        if self.samaritains:
            repondants.append(RepondantSama(self.samaritains))
        return sorted(repondants, key=attrgetter('repondant'))

    def contacts_as_context(self):
        return {
            'repondants': self.repondants(),
            'referents': sorted([ref for ref in self.referents if ref.referent], key=attrgetter('referent')),
            'autres_contacts': [ref for ref in self.referents if not any([ref.repondant, ref.referent])],
        }


class AdresseClient(GeolocMixin, models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    hospitalisation = models.BooleanField(default=False)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    remarque = models.TextField(blank=True)

    class Meta:
        db_table = 'alarme_adresseclient'

    def __str__(self):
        return " ".join([item for item in [
            "Hospitalisation" if self.hospitalisation else None, self.rue, self.npa, self.localite
        ] if item])

    def adresse(self):
        if self.rue:
            return f"{self.rue}, {self.npa} {self.localite}"
        elif self.localite:
            return f"{self.npa} {self.localite}"
        return ""

    @property
    def actuelle(self):
        """Renvoie l’objet AdressePresence actuel"""
        # Leverage prefetch_related
        acts = [
            pres for pres in self.presences.all()
            if pres.jusqua is None or (pres.jusqua > date.today() and pres.depuis < date.today())
        ]
        return acts[0] if acts else None


class AdressePresence(models.Model):
    adresse = models.ForeignKey(AdresseClient, on_delete=models.CASCADE, related_name='presences')
    depuis = models.DateField("Dès le")
    jusqua = models.DateField("Jusqu’au", null=True, blank=True)
    dernier_contact = models.DateField("Dernier contact", null=True, blank=True)
    remarque = models.TextField(blank=True)

    class Meta:
        db_table = 'alarme_adressepresence'

    def __str__(self):
        return f"Présence à «{self.adresse}» du {self.depuis} au {self.jusqua}"


class Referent(models.Model):
    SALUTATION_CHOICES = (
        ('M', 'Monsieur'),
        ('F', 'Madame'),
        ('P', 'Madame et Monsieur'),
        ('E', 'Entreprise'),
    )
    PAYS_CHOICES = (
        ('CH', 'Suisse'),
        ('DE', 'Allemagne'),
        ('FR', 'France'),
        ('IT', 'Italie'),
        ('AT', 'Autriche'),
        ('GB', 'Grande-Bretagne'),
        ('US', 'États-Unis'),
        ('FI', 'Finlande'),
        ('IL', 'Israël'),
    )
    FACTURATION_CHOICES = (
        ('al-abo', 'Alarme (install + abo)'),
        ('al-tout', 'Alarme (tout)'),
        ('transp', 'Transports'),
    )
    sama = False
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    id_externe = models.BigIntegerField(null=True, blank=True)
    cree_le = models.DateTimeField("Créé le", auto_now_add=True)
    no_centrale = models.PositiveIntegerField("N° personne à la centrale", null=True, blank=True)
    nom = models.CharField("Nom", max_length=50)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    salutation = models.CharField("Salutation", max_length=1, choices=SALUTATION_CHOICES, blank=True)
    complement = models.CharField("Nom (ligne 2)", max_length=80, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    pays = models.CharField("Pays", max_length=2, choices=PAYS_CHOICES, blank=True)
    courriel = models.EmailField("Courriel", blank=True)
    relation = models.CharField("Relation/lien de parenté", max_length=100, blank=True)
    # Role(s) of Referent
    repondant = models.PositiveSmallIntegerField("Priorité répondant", blank=True, null=True)
    referent = models.PositiveSmallIntegerField("Priorité référent", blank=True, null=True)
    admin = models.BooleanField("Contact administratif et technique", default=False)
    facturation_pour = ChoiceArrayField(
        models.CharField(max_length=10, choices=FACTURATION_CHOICES, blank=True),
        blank=True, default=list, verbose_name="Adresse de facturation pour"
    )
    no_debiteur = models.CharField("N° de débiteur", max_length=30, blank=True)
    contractant = models.BooleanField("Personne contractante", default=False)
    contact_visites = models.BooleanField("Contact pour les visites", default=False)
    remarque = models.TextField(blank=True)
    courrier_envoye = models.DateField(blank=True, null=True)
    date_archive = models.DateField("Archivé le", null=True, blank=True)

    class Meta:
        db_table = 'alarme_referent'

    def __str__(self):
        return f"{self.get_full_name()}, {self.roles()} pour {self.client}"

    @property
    def nom_prenom(self):
        return f"{self.nom} {self.prenom}".strip()

    def get_full_name(self):
        return f"{self.prenom} {self.nom}".strip()

    def rue_localite(self):
        pays = f'{self.pays}-' if (self.pays and self.pays != 'CH') else ''
        return ", ".join(item for item in [
            self.rue, f'{pays}{self.npa} {self.localite}'
        ] if item)

    def roles(self):
        role_list = [
            f'référent ({self.referent})' if self.referent else None,
            f'répondant ({self.repondant})' if self.repondant else None,
            'contact admin./techn.' if self.admin else None,
            'adresse de fact.' if self.facturation_pour else None,
            'contractant' if self.contractant else None,
        ]
        return ", ".join(role for role in role_list if role)

    def telephones(self):
        return self.referenttel_set.order_by('priorite')

    def autres_fonctions(self):
        """Nom des fonctions autres que repondant et referent."""
        fonctions = []
        if self.admin:
            fonctions.append("Contact administratif et technique")
        if self.facturation_pour:
            fonctions.append("Adresse de facturation (%s)" % ", ".join([
                dict(Referent.FACTURATION_CHOICES)[v] for v in self.facturation_pour
            ]))
        if self.contractant:
            fonctions.append("Personne contractante")
        return ", ".join(fonctions)

    def recoit_courrier(self):
        roles = self.roles()
        return True if ('répondant' in roles or 'contact admin' in roles) else False


class RepondantSama:
    """Mock class to simulate a Referent of type repondant."""
    sama = True

    def __init__(self, priorite):
        self.repondant = priorite


class ReferentTel(models.Model):
    referent = models.ForeignKey(Referent, on_delete=models.CASCADE)
    tel = PhoneNumberField("Numéro")
    priorite = models.PositiveSmallIntegerField("Priorité")
    remarque = models.CharField("Remarque", max_length=80, blank=True)

    class Meta:
        db_table = 'alarme_referenttel'
        verbose_name = "Téléphone de référent"
        verbose_name_plural = "Téléphones de référent"

    def __str__(self):
        return f'No {self.tel} pour {self.referent}'


class Professionnel(GeolocMixin, models.Model):
    PROF_CHOICES = (
        ('Médecin', 'Médecin'),
        ('Infirmière', 'Infirmières indépendantes'),
        ('SAD', 'Soins à domicile'),
        ('Sama', 'Samaritain'),
    )
    no_centrale = models.PositiveIntegerField("N° à la centrale", null=True, blank=True)
    type_pro = models.CharField("Type de professionnel", max_length=20, choices=PROF_CHOICES, blank=True)
    nom = models.CharField("Nom", max_length=100)
    prenom = models.CharField("Prénom", max_length=50, blank=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5, blank=True)
    localite = models.CharField("Localité", max_length=30, blank=True)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    courriel = models.EmailField("Courriel", blank=True)
    tel_1 = PhoneNumberField("Téléphone (priorité 1)")
    tel_2 = PhoneNumberField("Téléphone (priorité 2)", blank=True)
    remarque = models.TextField(blank=True)
    inactif = models.BooleanField("Temporairement inactif", default=False)
    date_archive = models.DateField("Archivé le", null=True, blank=True)

    class Meta:
        db_table = 'alarme_professionnel'

    def __str__(self):
        details = ", ".join([txt for txt in [self.localite, self.get_type_pro_display()] if txt])
        return f'{self.nom_prenom} ({details})'

    @property
    def nom_prenom(self):
        return " ".join(n for n in [self.nom, self.prenom] if n)


class ProfClient(models.Model):
    professionnel = models.ForeignKey(Professionnel, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    priorite = models.PositiveSmallIntegerField("Priorité", blank=True, null=True)
    remarque = models.TextField(blank=True)

    class Meta:
        db_table = 'alarme_profclient'

    def __str__(self):
        return f'Lien entre {self.professionnel} et {self.client}'


class Journal(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='journaux')
    description = models.TextField()
    details = models.TextField(blank=True)
    quand = models.DateTimeField()
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)
    auto = models.BooleanField(default=True)
    fichier = models.FileField(upload_to='journal', blank=True)

    class Meta:
        db_table = 'alarme_journal'
        get_latest_by = "quand"

    def __str__(self):
        return f"{self.quand}: {self.description}"

    def can_edit(self, user):
        return self.auto is False and self.qui == user


class Alerte(models.Model):
    """Messages d’alerte pour divers événements à quittancer (centrale d'alarme, etc.)."""
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='alertes')
    fichier = models.FileField("Fichier", upload_to='alertes', blank=True)
    alerte = models.TextField(blank=True)
    cible = models.CharField(max_length=10, choices=ClientType.choices)
    recu_le = models.DateTimeField("Reçu le")
    traite_le = models.DateTimeField("Traité le", null=True, blank=True)
    par = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True)
    remarque = models.TextField(blank=True)
    rel_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    rel_object_id = models.PositiveIntegerField(blank=True, null=True)
    modele_lie = GenericForeignKey("rel_content_type", "rel_object_id")

    class Meta:
        db_table = 'alarme_alerte'
        get_latest_by = ['recu_le']
        indexes = [
            models.Index(fields=['cible'], name='index_cible'),
        ]

    def __str__(self):
        return f"Alerte reçue le {self.recu_le} pour {self.client}"
