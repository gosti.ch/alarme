from django.db import migrations


def migrate_mission_to_modele(apps, schema_editor):
    Alerte = apps.get_model('alarme', 'Alerte')
    ContentType = apps.get_model('contenttypes', 'ContentType')
    try:
        mission_ct = ContentType.objects.get(app_label='alarme', model='mission')
    except ContentType.DoesNotExist:
        return
    for alerte in Alerte.objects.filter(mission__isnull=False):
        alerte.rel_content_type = mission_ct
        alerte.rel_object_id = alerte.mission_id
        alerte.save(update_fields=['rel_content_type', 'rel_object_id'])


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0101_alerte_cible_modele_lie'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [migrations.RunPython(migrate_mission_to_modele)]
