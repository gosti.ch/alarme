from datetime import date

from django.db import migrations


def migrate_activites(apps, schema_editor):
    Benevole = apps.get_model('benevole', 'Benevole')
    Activite = apps.get_model('benevole', 'Activite')
    for benev in Benevole.objects.all():
        for act in benev.activites:
            Activite.objects.create(
                benevole=benev, activite=act,
                debut=(benev.transport_depuis or date(2020, 1, 1)) if act == 'transport' else date(2020, 1, 1),
                fin=benev.archive_le,
                inactif=benev.inactif
            )


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0009_activite'),
    ]

    operations = [
        migrations.RunPython(migrate_activites)
    ]
