from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("client", "__first__"),
        ("benevole", "__first__"),
    ]

    operations = [
        migrations.CreateModel(
            name="Attribution",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("debut", models.DateField(verbose_name="Date début des visites")),
                ("fin", models.DateField(blank=True, null=True)),
                (
                    "en_pause",
                    models.BooleanField(default=False, verbose_name="En pause"),
                ),
                (
                    "motif_fin",
                    models.TextField(blank=True, verbose_name="Motif d’arrêt"),
                ),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, related_name='attributions', to="client.client"
                    ),
                ),
                (
                    "visiteur",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="benevole.benevole",
                    ),
                ),
            ],
        ),
    ]
