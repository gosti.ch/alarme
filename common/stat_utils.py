import calendar
from dataclasses import dataclass
from datetime import date, timedelta

from django import forms
from django.utils.dates import MONTHS

from .export import OpenXMLExport


class DateLimitForm(forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2022, date.today().year + (1 if date.today().month < 12 else 2))
    )
    start_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    start_year = forms.ChoiceField(choices=YEAR_CHOICES)
    end_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    end_year = forms.ChoiceField(choices=YEAR_CHOICES)

    def __init__(self, data, **kwargs):
        if not data:
            today = date.today()
            two_months_ago = today - timedelta(days=60)
            data = {
                'start_year': two_months_ago.year, 'start_month': two_months_ago.month,
                'end_year': today.year,
                'end_month': today.month,
            }
        super().__init__(data, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors and self.start > self.end:
            raise forms.ValidationError("Les dates ne sont pas dans l’ordre.")
        return cleaned_data

    @property
    def start(self):
        return date(int(self.cleaned_data['start_year']), int(self.cleaned_data['start_month']), 1)

    @property
    def end(self):
        return date(
            int(self.cleaned_data['end_year']),
            int(self.cleaned_data['end_month']),
            calendar.monthrange(int(self.cleaned_data['end_year']), int(self.cleaned_data['end_month']))[1]
        )


@dataclass
class Month:
    year: int
    month: int

    def __str__(self):
        return f'{self.month:0>2}.{self.year}'

    def __lt__(self, other):
        return (self.year, self.month) < (other.year, other.month)

    def __hash__(self):
        return hash((self.year, self.month))

    def next(self):
        if self.month == 12:
            return Month(self.year + 1, 1)
        else:
            return Month(self.year, self.month + 1)

    @classmethod
    def from_date(cls, dt):
        return Month(dt.year, dt.month)

    def is_future(self):
        return date(self.year, self.month, 1) > date.today()


class StatsMixin:
    permission_required = 'croixrouge.export_stats'

    def get_months(self):
        """Return a list of tuples [(year, month), ...] from date_start to date_end."""
        months = [Month(self.date_start.year, self.date_start.month)]
        while True:
            next_m = months[-1].next()
            if next_m > Month(self.date_end.year, self.date_end.month):
                break
            months.append(next_m)
        return months

    def month_limits(self, month):
        """From (2020, 4), return (date(2020, 4, 1), date(2020, 5, 1))."""
        next_m = month.next()
        return (
            date(month.year, month.month, 1),
            date(next_m.year, next_m.month, 1)
        )

    @staticmethod
    def init_counters(names, months, total=0):
        """
        Create stat counters:
            {<counter_name>: {(2020, 3): 0, (2020, 4): 0, …, 'total': 0}, …}
        """
        counters = {}
        for count_name in names:
            counters[count_name] = {}
            init_val = timedelta() if 'duree' in count_name else total
            for month in months:
                counters[count_name][month] = '-' if month.is_future() else ([] if count_name == 'age' else init_val)
            counters[count_name]['total'] = init_val
        return counters

    def get_stats(self, months):
        # Here subclasses produce stats to be merged in view context
        return {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_params = self.request.GET.copy()
        self.export_flag = get_params.pop('export', None)
        date_form = DateLimitForm(get_params)
        context['date_form'] = date_form
        if not date_form.is_valid():
            return context
        self.date_start = date_form.start
        self.date_end = date_form.end
        months = self.get_months()
        context.update({
            'date_form': date_form,
            'months': months,
        })
        context.update(self.get_stats(months))
        return context

    def export_data(self, export, context):
        export.fill_data(self.export_lines(context))

    def export_lines(self, context):
        raise NotImplementedError

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = OpenXMLExport(sheet_title="Stats", col_widths=[50])
            self.export_data(export, context)
            return export.get_http_response(self.__class__.__name__.replace('View', '') + '.xlsx')
        else:
            return super().render_to_response(context, **response_kwargs)
