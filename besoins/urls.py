from django.urls import include, path

from besoins import views

handler500 = 'common.views.error_view'

urlpatterns = [
    path('', include('common.urls')),
    path('', include('client.urls')),

    path('', views.HomeView.as_view(), name='home'),
    path('', include('besoins.urls_app')),
]
