from datetime import date

from django.contrib.staticfiles.finders import find

from reportlab.lib.colors import black
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    BaseDocTemplate, Frame, HRFlowable, Image, PageBreak, PageTemplate, Paragraph,
    Spacer, Table, TableStyle
)

from alarme.pdf import BaseCroixrougePDF, PageNumCanvas


class PDFHeaderFooter:
    LOGO = find('img/logo-cr-ne.png')
    EDUQUA = find('img/eduqua.png')
    DON = find('img/logo-zewo.png')

    def draw_header(self, canvas, doc):
        canvas.saveState()
        assert self.LOGO is not None
        canvas.drawImage(
            self.LOGO, 350, A4[1] - 2.6 * cm, 7 * cm, 1.6 * cm, preserveAspectRatio=True, mask='auto'
        )
        canvas.restoreState()

    def draw_footer(self, canvas, doc):
        canvas.saveState()
        page_height = A4[1]
        leftMargin = 1.4 * cm
        canvas.drawImage(
            self.EDUQUA, leftMargin, page_height - 810, 1.8 * cm, 0.8 * cm, preserveAspectRatio=True
        )
        canvas.drawImage(
            self.DON, leftMargin + 60, page_height - 810, 2.5 * cm, 0.8 * cm, preserveAspectRatio=True, mask='auto'
        )
        tab = [220, 365, 512]
        line = [798, 807, 816, 825]

        canvas.setFont("Helvetica", 8)
        canvas.drawRightString(leftMargin + tab[0], page_height - line[2], "CCP 20-1504-8")
        canvas.drawRightString(leftMargin + tab[0], page_height - line[3], "IBAN CH90 0900 0000 2000 1504 8")
        canvas.setLineWidth(0.5)
        canvas.line(leftMargin + 230, 1.7 * cm, leftMargin + 230, 0.5 * cm)

        canvas.drawRightString(leftMargin + tab[1], page_height - line[0], "Rue de la Paix 71")
        canvas.setFont("Helvetica-Bold", 8)
        canvas.drawRightString(leftMargin + tab[1], page_height - line[1], "2300 La Chaux-de-Fonds")
        canvas.setFont("Helvetica", 8)
        canvas.drawRightString(leftMargin + tab[1], page_height - line[2], "Avenue du Premier-Mars 2a")
        canvas.setFont("Helvetica-Bold", 8)
        canvas.drawRightString(leftMargin + tab[1], page_height - line[3], "2000 Neuchâtel")
        canvas.setFont("Helvetica", 8)
        canvas.line(leftMargin + 375, 1.7 * cm, leftMargin + 375, 0.5 * cm)

        canvas.drawRightString(leftMargin + tab[2], page_height - line[0], "+41 32 886 88 60")
        canvas.drawRightString(leftMargin + tab[2], page_height - line[1], "contact@croix-rouge-ne.ch")
        canvas.drawRightString(leftMargin + tab[2], page_height - line[2], "www.croix-rouge-ne.ch")
        canvas.restoreState()


class ContratPDF(BaseCroixrougePDF):
    title = 'Contrat alarme Croix-Rouge'
    left_margin = 3 * cm
    FONTSIZE = 10

    def init_doc(self, tampon):
        width = self.page_size[0] - self.left_margin - self.right_margin
        height = self.page_size[1] - self.top_margin - self.bottom_margin
        frameT = Frame(self.left_margin, self.bottom_margin, width, height - 1.5 * cm, id='normal')
        return BaseDocTemplate(
            tampon,
            pageTemplates=[
                PageTemplate(id='Main', frames=frameT, onPage=self.draw_header_footer, pagesize=self.page_size),
            ],
            title=self.title,
            leftMargin=self.left_margin, rightMargin=self.right_margin,
            topMargin=self.top_margin, bottomMargin=self.bottom_margin
        )

    def define_styles(self):
        super().define_styles()
        self.bold14 = ParagraphStyle(
            name='Bold 14', fontName='Helvetica-Bold', fontSize=14, leading=16
        )
        self.para_style = ParagraphStyle(
            name='Para', fontName='Helvetica', fontSize=self.FONTSIZE,
            alignment=TA_JUSTIFY, spaceAfter=0.2 * cm
        )
        self.title_style = ParagraphStyle(
            name='Bold 11', fontName='Helvetica-Bold', fontSize=11, leading=12,
            spaceBefore=0.9 * cm, spaceAfter=0.2 * cm
        )
        self.style_small = ParagraphStyle(
            name='small', fontName='Helvetica', fontSize=self.FONTSIZE - 1, leading=self.FONTSIZE,
            alignment=TA_LEFT, spaceAfter=0
        )
        # Pour la case à cocher
        registerFont(TTFont('DejaVuSans', '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'))

    def get_filename(self, client):
        return f'contrat_alarme_{client.nom}.pdf'

    def draw_header_footer(self, canvas, doc):
        super().draw_header_footer(canvas, doc)
        canvas.saveState()
        canvas.setFont("Helvetica-Bold", 12)
        left = doc.leftMargin - 1 * cm
        canvas.drawString(left, doc.height + 2 * cm, "Contrat client")
        canvas.drawString(left, doc.height + 1.5 * cm, "Alarme Croix-Rouge")
        canvas.setFont("Helvetica", 9)
        canvas.drawString(left, 2.8 * cm, "Contrat client v2.4")
        canvas.restoreState()

    def produce(
        self, client, appareil=None, abo=None, pour_signature=False, signature_client=None,
        signature_cr=None, signature_cr_nom=None, signature_lieu=None
    ):
        no_centrale = '058 105 04 18'
        contractant = client.contractant()
        if client is contractant:
            adresse = (
                f'{client.titre}<br/>{contractant.nom_prenom}<br/>{contractant.rue}<br/>'
                f'{contractant.npa} {contractant.localite}'
            )
        else:
            adresse = (
                f'Madame/Monsieur<br/>{contractant.prenom} {contractant.nom}<br/>{contractant.rue}<br/>'
                f'{contractant.npa} {contractant.localite}'
            )
        # Page 1
        self.story.append(Spacer(1, 1.5 * cm))
        table_data = [
            ['', Paragraph('Contrat Location<br/>Alarme Croix-Rouge', self.bold14)],
            [Spacer(1, 1 * cm), ''],
            [
                'entre la',
                Paragraph(
                    '<b>Croix-Rouge neuchâteloise<br/><br/>032 886 88 60</b>',
                    self.style_normal
                )
            ],
            ['désignée ci-après «CRNE»', ''],
            [Spacer(1, 1 * cm), ''],
            ['et', Paragraph(f'<b>{adresse}</b>', self.style_normal)],
            ['désigné ci-après «le client»', ''],
        ]
        self.story.append(Table(
            data=table_data, colWidths=[7.5 * cm, 9 * cm],
            style=TableStyle([('VALIGN', (0, 0), (-1, -1), "TOP")]),
            hAlign=TA_LEFT, spaceBefore=2 * cm,
        ))
        self.story.append(Spacer(1, 8.5 * cm))
        self.story.append(Paragraph(
            "NB : Pour faciliter la lecture du contrat, le masculin générique est utilisé "
            "pour désigner les deux sexes.",
            self.style_small
        ))
        self.story.append(PageBreak())
        # Page 2
        self.story.append(Paragraph("1. Objet du contrat", self.title_style))
        paras = [
            "La CRNE fournit la prestation Alarme Croix-Rouge aux clients du canton de Neuchâtel. "
            "La CRNE est habilitée à mandater des tiers pour effectuer tout ou une partie des prestations "
            "listées dans le présent contrat.",
            "Les prestations offertes par la CRS sont disponibles exclusivement sur le territoire suisse.",
            "Le client loue un dispositif à la CRNE. Grâce à cet appareil, il peut établir en tout temps "
            "une liaison téléphonique avec la centrale d’alarme Croix-Rouge ou donner une alerte. Il peut "
            "ainsi transmettre des informations détaillées à la centrale, qui organise les secours adéquats "
            "selon ses indications et ses souhaits.",
            "La centrale d’alarme Croix-Rouge est opérationnelle 7/7 et 24h/24. Elle dispose des "
            "installations techniques nécessaires pour répondre aux appels d’urgence et aux autres appels "
            "des clients. Les appels sont traités par du personnel formé à cet effet et capable "
            "d’organiser les secours adéquats. Toutes les conversations avec la centrale d’alarme "
            "Croix-Rouge sont enregistrées.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(Paragraph("2. Contenu du contrat", self.title_style))
        self.story.append(Paragraph(
            "Le questionnaire et les descriptions des prestations choisies par le client "
            "(listées ci-après) font partie intégrante du présent contrat:", self.para_style)
        )
        self.story.append(Paragraph("2.1 Alarme Croix-Rouge Casa : prestation choisie", self.title_style))
        if abo:
            abo_str = f"{self.checked_box} Alarme Croix-Rouge <i>{abo.nom.replace('Abo', '')}</i>"
        else:
            abo_str = "<i>Aucun abonnement choisi pour le moment</i>"
        self.story.append(Paragraph(abo_str, self.para_style))
        materiels = client.materiel_actuel()
        if materiels:
            self.story.append(Paragraph("2.2 Accessoires en lien avec Alarme Croix-Rouge Casa", self.title_style))
            for mat in materiels:
                nom_mat = mat.type_mat.nom if mat.type_mat else mat.materiel.type_mat.nom
                if nom_mat == "Émetteur":
                    nom_mat = "Bracelet émetteur supplémentaire"
                self.story.append(Paragraph(f"{self.checked_box} {nom_mat}", self.para_style))
        if client.samaritains:
            self.story.append(Paragraph(f"2.{'3' if materiels else '2'} Options", self.title_style))
            self.story.append(Paragraph(f"{self.checked_box} Répondants mandatés par la CRNE", self.para_style))
            if client.nom_part:
                self.story.append(
                    Paragraph(f"{self.checked_box} Répondants mandatés par la CRNE (pour 2e client)", self.para_style)
                )

        self.story.append(Paragraph("3. Nature des prestations d’aide", self.title_style))
        paras = [
            "Les prestations d’aide sont organisées en priorité selon les indications figurant sur "
            "le questionnaire. Toutes les mesures d’intervention prises après clarification "
            "de la situation par le personnel de la centrale d’alarme Croix-Rouge et considérées "
            "comme nécessaires sont mises en œuvre pour le compte du client et en son nom.",
            "Les frais découlant de prestations assurées par des tiers (par ex. médecin, "
            "organisations sanitaires, etc.) organisées par la centrale d’alarme Croix-Rouge "
            "sont à la charge du client. "
            "Ils lui sont facturés directement par le mandataire.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))
        self.story.append(PageBreak())

        # Page 3
        self.story.append(Paragraph("4. Coûts", self.title_style))
        self.story.append(Paragraph(
            "Le prix des prestations fournies dans le cadre du présent contrat est fixé "
            "conformément à la liste des prix disponible sous https://croix-rouge-ne.ch.",
            self.para_style)
        )
        self.story.append(Paragraph(
            "Une facture est établie pour ces prestations. Elle doit être réglée dans les "
            "30 jours. Les mois entamés sont facturés comme des mois pleins.",
            self.para_style)
        )
        self.story.append(Paragraph("5. Participation du client", self.title_style))

        paras = [
            "Le client garantit que toutes les personnes répondantes mentionnées dans le "
            "questionnaire ont consenti à intervenir en cas d’incident et possèdent "
            "toutes une clé de son domicile.",
            "Toute modification relative au questionnaire doit être immédiatement communiquée à la CRNE.",
            f"Le client prévient la <b>centrale d’alarme Croix-Rouge au numéro {no_centrale}</b> (ou en "
            "pressant sur le bouton du bracelet émetteur) lorsqu’il s’absente un minimum de 3 jours "
            "et laisse le bracelet émetteur à domicile.",
            "Le client manipule les appareils mis à sa disposition avec tout le soin nécessaire "
            "et signale immédiatement les éventuels dysfonctionnements.",
            "Le client est responsable des clés confiées à ses répondants.<br/>"
            "Lors de la résiliation, le client est donc en charge de la récupération de ses clés.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))

        self.story.append(Paragraph("6. Responsabilité", self.title_style))
        paras = [
            "La CRNE veille au respect rigoureux des obligations stipulées dans le présent "
            "contrat et au bon fonctionnement des appareils loués. En revanche, elle décline "
            "expressément toute responsabilité, dans les limites autorisées par la loi, pour "
            "tout dommage consécutif ou indirect, ainsi que pour les personnes chargées des "
            "secours. Cela vaut également pour la responsabilité extracontractuelle.",

            "La CRNE n’endosse notamment aucune responsabilité concernant le fonctionnement "
            "des moyens de communication mis en œuvre pour transmettre l’alarme (p. ex. ligne "
            "téléphonique, réception du téléphone mobile (GSM), réception des sms, transmission "
            "radio, etc.).",

            "Le client est responsable des dommages découlant du non-respect de ses obligations "
            "contractuelles, tel qu’un maniement inadéquat de l’appareil ou l’indication de "
            "données inexactes ou non actuelles dans le questionnaire. Un forfait "
            "pour le déplacement d’un collaborateur de la CRNE sera alors facturé à "
            "CHF 50.- (+ TVA)*.",

            "En cas de perte ou dommage des accessoires ou des appareils loués, le client "
            "reconnaît devoir un montant forfaitaire de :<br/>"
            "   -  Pour un bracelet émetteur : CHF 60.- (+ TVA)*<br/>"
            "   -  Pour un câble : CHF 60.- (+ TVA)*<br/>"
            "   -  Pour un appareil Casa : CHF 500.- (+ TVA)*<br/>"
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))

        self.story.append(PageBreak())

        # Page 4
        self.story.append(Paragraph("7. Durée du contrat et résiliation", self.title_style))
        paras = [
            "Ce contrat commence à la date de mise en service de l’alarme Croix-Rouge et est "
            "conclu pour une durée indéterminée. Il peut être résilié en tout temps par l’une "
            "des parties pour la fin d’un mois, moyennant un préavis de 20 jours.",

            "La résiliation sans délai reste réservée, notamment en cas de violation grave "
            "du contrat ou au non-paiement des honoraires dans les délais. L’utilisation "
            "abusive de l’alarme Croix-Rouge est un motif de dissolution de contrat sans "
            "délai de résiliation.",

            "La durée minimale du contrat est de 3 mois.",

            "À l’échéance du contrat, tous les appareils loués sont restitués en parfait "
            "état de fonctionnement dans les 5 jours par le client lui-même ou par une "
            "personne de contact. En cas de déplacement d’un collaborateur de la CRNE "
            "pour rechercher l’appareil loué, un forfait de CHF 50.- (+ TVA)* sera facturé.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))

        self.story.append(Paragraph("8. Protection des données", self.title_style))
        self.story.append(Paragraph(
            "Le traitement des données par la CRNE se fait conformément à la législation en "
            "vigueur. Seules sont collectées, enregistrées et traitées les données nécessaires "
            "à la fourniture des prestations, à l’administration et à la gestion de la relation "
            "client ainsi qu’à la facturation. Le client donne son accord explicite à cet égard.",
            self.para_style
        ))

        self.story.append(Paragraph("9. Dispositions finales", self.title_style))
        paras = [
            "Les accords verbaux relatifs à ce contrat ne sont pas contraignants. Toute "
            "modification et tout complément requièrent la forme écrite.",
            "Le renoncement aux droits contractuels est exclu.",
            "Les parties s’engagent à régler les éventuels différends ou divergences "
            "d’opinions relatifs à ce contrat par la négociation avant de porter "
            "l’affaire devant un tribunal. Le cas échéant, si les dispositions "
            "contractuelles ne conviennent plus, elles doivent être remplacées par de "
            "nouvelles dispositions reflétant le plus fidèlement possible la volonté "
            "originelle des parties.",
            "Si certaines dispositions de ce contrat devaient s’avérer nulles ou caduques, "
            "cela n’affecte en rien la légitimité des autres dispositions. Les parties "
            "s’accordent pour faire en sorte qu’une disposition contractuelle qui ne "
            "convient plus soit remplacée par une nouvelle disposition reflétant le plus "
            "fidèlement possible la volonté originelle des parties.",
            "En cas de litige résultant du présent contrat, le for juridique compétent "
            "est exclusivement Neuchâtel.",
        ]
        for para in paras:
            self.story.append(Paragraph(para, self.para_style))

        self.story.append(Spacer(1, 3 * cm))
        # Might be a real footnote in the future
        self.story.append(Paragraph("<i>*Prix sous réserve de modification</i>", self.style_small))

        if not pour_signature:
            self.story.append(PageBreak())
            # Page 5
            hr = HRFlowable(width="100%", thickness=0.5, color=black)
            self.story.append(Paragraph("Signature du client", self.title_style))
            if signature_lieu:
                self.story.append(Spacer(1, 0.5 * cm))
                self.story.append(
                    Paragraph(f"{signature_lieu}, le {date.today().strftime('%d.%m.%Y')}", self.para_style)
                )
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Lieu, date", self.para_style))
            if signature_client:
                im = Image(signature_client, 2 * cm, 2 * cm)
                self.story.append(im)
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Signature du client", self.para_style))

            self.story.append(Spacer(1, 2 * cm))
            self.story.append(Paragraph("Signature de la Croix-Rouge neuchâteloise", self.title_style))
            if signature_lieu:
                self.story.append(Spacer(1, 0.5 * cm))
                self.story.append(
                    Paragraph(f"{signature_lieu}, le {date.today().strftime('%d.%m.%Y')}", self.para_style)
                )
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Lieu, date", self.para_style))
            if signature_cr_nom:
                self.story.append(Spacer(1, 0.5 * cm))
                self.story.append(Paragraph(signature_cr_nom, self.para_style)                    )
            else:
                self.story.append(Spacer(1, 1.2 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Nom et prénom du collaborateur Croix-Rouge", self.para_style))
            if signature_cr:
                im = Image(signature_cr, 2 * cm, 2 * cm)
                self.story.append(im)
            else:
                self.story.append(Spacer(1, 0.8 * cm))
            self.story.append(hr)
            self.story.append(Paragraph("Signature du collaborateur Croix-Rouge", self.para_style))

        self.doc.build(self.story, canvasmaker=PageNumCanvas)
