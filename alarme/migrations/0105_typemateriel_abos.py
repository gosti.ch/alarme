from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0104_delete_alerte'),
    ]

    operations = [
        migrations.AddField(
            model_name='typemateriel',
            name='abos',
            field=models.ManyToManyField(
                blank=True, to='alarme.typeabo',
                verbose_name="Types d’abonnements possibles pour ce type de matériel"
            ),
        ),
    ]
