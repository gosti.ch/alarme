import common.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="adresse",
            name="tel",
            field=common.fields.PhoneNumberField(
                blank=True, max_length=18, verbose_name="Téléphone"
            ),
        ),
    ]
