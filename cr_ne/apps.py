from importlib import import_module

from django.apps import AppConfig, apps


class CRNEAppConfig(AppConfig):
    name = 'cr_ne'
    verbose_name = "Croix-Rouge Neuchâtel"
    abrev = "NE"
    EXTRA_LOGEMENT_CHOICES = (('ape', "ApE contrat CRN"),)

    def ready(self):
        if apps.is_installed('alarme'):
            from alarme.models import InstallBase
            from . import policies

            InstallBase.fact_policy = policies.FacturationPolicy()

    def pdf_headfoot_class(self):
        try:
            return self.module.pdf.PDFHeaderFooter
        except AttributeError:
            import_module(f'{self.name}.pdf')
            return self.module.pdf.PDFHeaderFooter
