from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0015_refus"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="remarques_chauff",
            field=models.TextField(blank=True, verbose_name="Remarques pour chauffeur"),
        ),
        migrations.AlterField(
            model_name="transport",
            name="remarques",
            field=models.TextField(blank=True, verbose_name="Remarques internes"),
        ),
    ]
