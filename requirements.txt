django~=4.2
psycopg
phonenumberslite  # required by django-two-factor-auth
django-two-factor-auth==1.14.0
python-dateutil==2.8.2
openpyxl==3.1.2
reportlab==4.0.4
workalendar==16.4.0
httpx
openrouteservice==2.3.3
django-leaflet
# Incoming mail read:
imapclient
pypdf==3.8.1
beautifulsoup4
# transports-only
feedparser
svglib
qrbill
