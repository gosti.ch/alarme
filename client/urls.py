"""
URLs liés aux clients, communs à 'alarme' et 'transport'
"""
from django.urls import path

from client import views

urlpatterns = [
    path('clients/<str:types>/autocomplete/', views.ClientAutocompleteView.as_view(),
        name='client-autocomplete'),
    path('clients/alertes/', views.ClientAlertesView.as_view(), name='clients-alertes'),

    path('clients/<int:pk>/journal/', views.ClientJournalView.as_view(), name='client-journal'),
    path('clients/<int:pk>/journal/nouveau/', views.ClientJournalEditView.as_view(is_create=True),
        name='client-journal-add'),
    path('clients/<int:pk>/journal/<int:pk_jr>/edition/', views.ClientJournalEditView.as_view(is_create=False),
        name='client-journal-edit'),
    path('clients/<int:pk>/journal/<int:pk_jr>/details/', views.ClientJournalDetailsView.as_view(),
        name='client-journal-details'),
    path('clients/<int:pk>/archive/', views.ClientArchiveView.as_view(), name='client-archive'),

    path('clients/<int:pk>/fichier/nouveau/', views.ClientFichierAddView.as_view(),
        name='client-fichier-add'),
    path('clients/<int:pk>/fichier/liste/', views.ClientFichierListeView.as_view(),
        name='client-fichier-list'),
    path('fichier/<int:pk>/supprimer/', views.ClientFichierDeleteView.as_view(),
        name='client-fichier-delete'),
    # Adresses
    path('clients/<int:pk>/adresse/nouvelle/', views.ClientAdresseEditView.as_view(is_create=True),
        name='client-adresse-add'),
    path('clients/<int:pk>/adresse/<int:pk_adr>/edition/', views.ClientAdresseEditView.as_view(is_create=False),
        name='client-adresse-edit'),
    path('clients/<int:pk>/adresse/<int:pk_adr>/supprimer/', views.ClientAdresseDeleteView.as_view(),
        name='client-adresse-delete'),
    path('clients/<int:pk>/adresse/liste/', views.ClientAdresseListeView.as_view(), name='client-adresse-list'),
    path('clients/<int:pk>/absence/<int:pk_abs>/edition/', views.ClientAbsenceSuiviView.as_view(),
        name='client-absence-edit'),
    # Référents
    path('clients/<int:pk>/referent/nouveau/', views.ReferentEditView.as_view(is_create=True),
        name='client-referent-add'),
    path('clients/<int:pk>/referent/<int:pk_ref>/edition/', views.ReferentEditView.as_view(is_create=False),
        name='client-referent-edit'),
    path('clients/<int:pk>/referent/<int:pk_ref>/supprimer/', views.ReferentDeleteView.as_view(),
        name='client-referent-delete'),
    path('clients/<int:pk>/referent/liste/', views.ClientReferentListeView.as_view(), name='client-referent-list'),
    path('clients/<int:pk>/referent/reorder/', views.ClientReferentReorderView.as_view(typ='référent'),
        name='client-referent-reorder'),
    path('clients/<int:pk>/repondant/reorder/', views.ClientReferentReorderView.as_view(typ='répondant'),
        name='client-repondant-reorder'),
    path('clients/<int:pk>/professionnel/nouveau/', views.ClientProfessionnelAddView.as_view(),
        name='client-professionnel-add'),
    path('clients/<int:pk>/professionnel/<int:pk_ref>/edition/', views.ClientProfessionnelEditView.as_view(),
        name='client-professionnel-edit'),
    path('clients/<int:pk>/professionnel/<int:pk_ref>/supprimer/', views.ClientProfessionnelDeleteView.as_view(),
        name='client-professionnel-delete'),
    path('alerte/<int:pk>/edition/', views.ClientAlerteEditView.as_view(), name='client-alerte-edit'),
    # Samaritains
    path('clients/<int:pk>/set_samas/', views.ClientSetSamaritainsView.as_view(),
        name='client-setsamas'),
    path('clients/<int:pk>/samas/', views.ClientSamaritainsView.as_view(), name='client-samaritains'),
    path('clients/<int:pk>/add_sama/<int:sama_pk>/', views.ClientSamaritainAddView.as_view(), name='client-samas-add'),
    path('clients/<int:pk>/remove_sama/<int:sama_pk>/', views.ClientSamaritainRemoveView.as_view(), name='client-samas-remove'),
    path('clients/<int:pk>/samas_reorder/', views.ClientSamaritainReorderView.as_view(), name='client-samas-reorder'),
    path('clients/<int:pk>/sama/<int:sama_pk>/distance/', views.client_sama_distance, name='client-sama-distance'),
    # Professionnels
    path('professionnels/', views.ProfessionnelListView.as_view(), name='professionnels'),
    path('professionnels/search/', views.ProfessionnelSearchView.as_view(), name='professionel-search'),
    path('professionnels/nouveau/', views.ProfessionnelEditView.as_view(is_create=True), name='professionnel-new'),
    path('professionnels/<int:pk>/edition/', views.ProfessionnelEditView.as_view(is_create=False), name='professionnel-edit'),
    path('professionnels/<int:pk>/archive/', views.ProfessionnelArchiveView.as_view(), name='professionnel-archive'),
]
