from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("transport", "0037_is_ofas_function"),
    ]

    operations = [
        migrations.RunSQL("""
create or replace function is_ofas(
    transport_pk int, transport_date date, tarif_avs_acquis boolean, date_naissance date, genre varchar(1)
)
returns boolean
language plpgsql
as
$$
declare
   is_ofas boolean;
   num_years integer := EXTRACT(YEAR FROM transport_date) - EXTRACT(YEAR FROM date_naissance);
begin
   select ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND (
        tarif_avs_acquis OR num_years >= 65 OR (genre = 'F' AND num_years >= 64)
   )
   into is_ofas
   from transport_trajet
   where transport_id = transport_pk;
   
   return is_ofas;
end;
$$;""")
    ]
