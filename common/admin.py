from datetime import datetime

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models import Model
from django.utils.timezone import localtime

from .export import OpenXMLExport
from .models import DistanceFigee, Fichier, Utilisateur


class ExportAction:
    short_description = "Exporter la sélection dans une liste"
    __name__ = "export"

    def __init__(self, title):
        self.title = title

    def __call__(self, modeladmin, request, queryset):
        export = OpenXMLExport(self.title)
        fields = modeladmin.model._meta.local_fields
        headers = [f.verbose_name for f in fields]
        export.write_line(headers, bold=True)
        field_names = [f.name for f in fields]
        for obj in queryset:
            export.write_line([self.serialize(obj, field) for field in field_names])
        return export.get_http_response(f"{self.title.lower().replace(' ', '_')}.xlsx")

    def serialize(self, obj, field_name):
        if hasattr(obj, f'get_{field_name}_display'):
            value = getattr(obj, f'get_{field_name}_display')()
        else:
            value = getattr(obj, field_name)
        if isinstance(value, Model):
            value = str(value)
        elif isinstance(value, list):
            value = ", ".join(str(v) for v in value)
        elif isinstance(value, datetime):
            value = localtime(value).replace(tzinfo=None)
        return value


class DateAsBooleanListFilter(admin.FieldListFilter):
    """Filtre de liste comme un booléen valant True si un champ date n'est pas null."""
    def __init__(self, field, request, params, model, model_admin, field_path):
        self.lookup_kwarg = f"{field_path}__isnull"
        self.lookup_val = params.get(self.lookup_kwarg)
        super().__init__(field, request, params, model, model_admin, field_path)

    def choices(self, changelist):
        for lookup, title in (
            (None, "Tous"),
            ('False', "Oui"),
            ('True', "Non"),
        ):
            yield {
                "selected": self.lookup_val == lookup,
                "query_string": changelist.get_query_string(
                    {self.lookup_kwarg: lookup}, []
                ),
                "display": title,
            }

    def expected_parameters(self):
        return [self.lookup_kwarg]


class UtilisateurCreationForm(UserCreationForm):
    class Meta:
        model = Utilisateur
        fields = ("email",)


@admin.register(Utilisateur)
class UtilisateurAdmin(UserAdmin):
    model = Utilisateur
    add_form = UtilisateurCreationForm
    list_display = ['email', 'is_staff', 'is_active', 'last_login']
    ordering = ('email',)
    search_fields = ('first_name', 'last_name', 'email')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        if 'username' in fieldsets[0][1]['fields']:
            # Remove username and add fonction
            fieldsets[0][1]['fields'] = [
                field for field in fieldsets[0][1]['fields'] if field != 'username'
            ]
            fieldsets[1][1]['fields'] += ('fonction',)
        return fieldsets


@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    list_display = ['app_label', 'model']


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ['name', 'content_type', 'codename']
    search_fields = ['codename']


@admin.register(DistanceFigee)
class DistanceFigeeAdmin(admin.ModelAdmin):
    list_display = ['created', 'empl_geo_dep', 'empl_geo_arr', 'distance']


@admin.register(Fichier)
class FichierAdmin(admin.ModelAdmin):
    list_display = ['content_object', 'titre', 'typ', 'qui', 'quand']
    search_fields = ['titre']
