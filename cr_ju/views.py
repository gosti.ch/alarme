import csv

from django.http import HttpResponse
from django.utils.dateformat import format as django_format
from django.utils.timezone import now
from django.views.generic import View

from alarme.models import Facture

CSV_KWARGS = {'delimiter': ';', 'quotechar': '"', 'quoting': csv.QUOTE_MINIMAL}


class FacturesExportView(View):
    headers = [
        'ID_FACTURE',  # chiffre unique de notre côté.
        'DATE_FACTURE',
        'ID_CLIENT',  # récupérer ID AZ
        'FACTURATION_ID',  # laisser vide
        'CODE_ARTICLE',  # attente envoi AZ
        'DEBITEURS',  # "3410/10"
        'TEXTE_1',  # "Date: <date>
        'TEXTE_2',  # libellé article (ligne 1)
        'TEXTE_3',  # libellé article (ligne 2)
        'QUANTITE',  # 1
        'TYPE_TAUX',  # "F"
        'TAUX_HORAIRE',  # Prix unitaire
        'MONTANT',  # Total
        'CODE_TVA',  # "5"
        'VALEUR_TVA',  # "0"
        'SECTEUR',  # 'ALAR'
    ]
    ID_FACTURE_START = 90000
    # Valeurs constantes
    QUANTITE = '1'
    TYPE_TAUX = 'F'
    CODE_TVA = '5'
    VALEUR_TVA = '0'
    SECTEUR = 'ALAR'

    def post(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv', charset='cp1252')
        response['Content-Disposition'] = 'attachment; filename="factures_%s.csv"' % now().strftime('%Y%m%d_%H%M')
        query_factures = Facture.objects.exclude(client__no_debiteur='').filter(exporte__isnull=True
            ).select_related('client', 'article').order_by('client', 'mois_facture')
        self.exporter_factures(response, query_factures)
        return response

    def exporter_factures(self, out, factures):
        writer = csv.writer(out, **CSV_KWARGS)
        writer.writerow(self.headers)
        for fact in factures:
            date_fact = fact.date_facture.strftime('%d.%m.%Y')
            no_fact = self.ID_FACTURE_START + fact.pk
            no_compta = fact.article.compte
            libelle = ''
            if fact.mois_facture and 'service' in fact.article.designation.lower():
                libelle = django_format(fact.mois_facture, "F Y")
            values = [
                no_fact, date_fact, fact.client.no_debiteur, '',
                fact.article.code, no_compta, libelle, '', '', self.QUANTITE,
                self.TYPE_TAUX, fact.montant, fact.montant, self.CODE_TVA, self.VALEUR_TVA, self.SECTEUR,
            ]
            writer.writerow(values)
            if fact.exporte is None:
                fact.exporte = now()
                fact.save()
