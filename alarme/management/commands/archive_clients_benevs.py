from datetime import date, timedelta

from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Count, Max, Q
from django.utils.timezone import now

from benevole.models import Benevole, Journal as JournalBenevole
from client.models import Client, Journal


class Command(BaseCommand):
    """
    Archivage automatique des clients alarme après un certain temps sans appareil actif.
    Généralement exécuté par un job cron toutes les semaines.
    """
    def handle(self, **options):
        self.archiver_clients(options['verbosity'])
        self.archiver_benevoles(options['verbosity'])

    def archiver_clients(self, verbosity):
        to_archive = Client.objects.annotate(
            num_inst=Count('installation'),
            num_active=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
            num_waiting=Count('installation', filter=Q(installation__retour_mat__isnull=True)),
            last_activity=Max('installation__retour_mat'),
        ).filter(
            type_client__contains=['alarme'],
            archive_le__isnull=True,
            num_inst__gt=0,  # Avoid deleting new clients
            num_active=0,
            num_waiting=0,
            last_activity__lt=date.today() - timedelta(days=31)
        )
        for client in to_archive:
            client.archiver(None, 'alarme', check=False)
        if verbosity > 1:
            self.stdout.write(
                self.style.SUCCESS(f"{len(to_archive)} client(s) archivé(s)")
            )

    def archiver_benevoles(self, verbosity):
        to_archive = Benevole.objects.annotate(
            act_actives=Count('activite', filter=Q(activite__fin__isnull=True) | Q(activite__fin__gt=date.today()))
        ).filter(archive_le__isnull=True, act_actives=0)
        for benev in to_archive:
            with transaction.atomic():
                benev.archiver()
                JournalBenevole.objects.create(
                    benevole=benev, description="Plus d’activité en cours, archivage du bénévole.",
                    quand=now(), qui=None
                )
        if verbosity > 1 and len(to_archive) > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{len(to_archive)} bénévole(s) archivé(s)")
            )
