from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("client", "0002_professionnel_inactif"),
    ]

    operations = [
        migrations.AddField(
            model_name="client",
            name="courrier_envoye",
            field=models.DateField(blank=True, null=True),
        ),
    ]
