from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('benevole', '0010_migration_activites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='benevole',
            name='activites',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='inactif',
        ),
        migrations.RemoveField(
            model_name='benevole',
            name='transport_depuis',
        ),
    ]
