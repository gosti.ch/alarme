import client.models
import common.fields
import common.models
from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("benevole", "__first__"),
    ]

    operations = [
        migrations.CreateModel(
            name="AdresseClient",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("hospitalisation", models.BooleanField(default=False)),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                ("npa", models.CharField(max_length=5, verbose_name="NPA")),
                ("localite", models.CharField(max_length=30, verbose_name="Localité")),
                (
                    "empl_geo",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.FloatField(), blank=True, null=True, size=2
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
            ],
            options={
                "db_table": "alarme_adresseclient",
            },
            bases=(common.models.GeolocMixin, models.Model),
        ),
        migrations.CreateModel(
            name="Client",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("id_externe", models.BigIntegerField(blank=True, null=True)),
                (
                    "type_client",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            choices=[("alarme", "Alarme"), ("transport", "Transport"), ("visite", "Visite"), ('savd', 'SAVD')],
                            max_length=10,
                        ),
                        default=client.models.default_client_type,
                        size=None,
                    ),
                ),
                ("nom", models.CharField(max_length=60, verbose_name="Nom")),
                ("prenom", models.CharField(max_length=50, verbose_name="Prénom")),
                (
                    "genre",
                    models.CharField(
                        blank=True,
                        choices=[("F", "Femme"), ("M", "Homme")],
                        max_length=1,
                        verbose_name="Genre",
                    ),
                ),
                (
                    "date_naissance",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date de naissance"
                    ),
                ),
                (
                    "nom_part",
                    models.CharField(
                        blank=True, max_length=50, verbose_name="Nom du partenaire"
                    ),
                ),
                (
                    "prenom_part",
                    models.CharField(
                        blank=True, max_length=50, verbose_name="Prénom du partenaire"
                    ),
                ),
                (
                    "genre_part",
                    models.CharField(
                        blank=True,
                        choices=[("F", "Femme"), ("M", "Homme")],
                        max_length=1,
                        verbose_name="Genre du partenaire",
                    ),
                ),
                (
                    "date_naissance_part",
                    models.DateField(
                        blank=True,
                        null=True,
                        verbose_name="Date de naissance du partenaire",
                    ),
                ),
                (
                    "tel_part",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. du partenaire"
                    ),
                ),
                (
                    "c_o",
                    models.CharField(blank=True, max_length=80, verbose_name="C/o"),
                ),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                (
                    "case_postale",
                    models.CharField(
                        blank=True, max_length=20, verbose_name="Case postale"
                    ),
                ),
                ("npa", models.CharField(max_length=5, verbose_name="NPA")),
                ("localite", models.CharField(max_length=30, verbose_name="Localité")),
                (
                    "empl_geo",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.FloatField(), blank=True, null=True, size=2
                    ),
                ),
                (
                    "tel_1",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. principal"
                    ),
                ),
                (
                    "tel_2",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Tél. secondaire"
                    ),
                ),
                (
                    "courriel",
                    models.EmailField(
                        blank=True, max_length=254, verbose_name="Courriel"
                    ),
                ),
                (
                    "langues",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            blank=True,
                            choices=[
                                ("fr", "français"),
                                ("de", "allemand"),
                                ("it", "italien"),
                                ("en", "anglais"),
                                ("es", "espagnol"),
                                ("pt", "portugais"),
                                ("tr", "turc"),
                                ("cz", "tchèque"),
                            ],
                            max_length=2,
                        ),
                        blank=True,
                        null=True,
                        size=None,
                    ),
                ),
                (
                    "type_logement",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("villa", "Villa"),
                            ("appartement", "Appartement"),
                            ("resid", "Institution/App. avec encadrement"),
                            ("ape", "ApE contrat CRN"),
                        ],
                        max_length=12,
                        verbose_name="Type de logement",
                    ),
                ),
                (
                    "type_logement_info",
                    models.CharField(
                        blank=True, max_length=60, verbose_name="Type de logement"
                    ),
                ),
                (
                    "etage",
                    models.CharField(
                        blank=True, max_length=20, verbose_name="Étage/niveau"
                    ),
                ),
                (
                    "ascenseur",
                    models.BooleanField(default=False, verbose_name="Ascenseur"),
                ),
                (
                    "code_entree",
                    models.CharField(
                        blank=True, max_length=20, verbose_name="Code entrée immeuble"
                    ),
                ),
                (
                    "nb_pieces",
                    models.CharField(
                        blank=True, max_length=50, verbose_name="Nbre de pièces"
                    ),
                ),
                (
                    "boitier_cle",
                    models.CharField(
                        blank=True,
                        choices=[("prive", "Privé"), ("c-r", "Croix-Rouge")],
                        max_length=10,
                        verbose_name="Boîtier à clé",
                    ),
                ),
                (
                    "cles",
                    models.CharField(
                        blank=True, max_length=250, verbose_name="Infos clés"
                    ),
                ),
                (
                    "situation_vie",
                    models.CharField(
                        blank=True, max_length=80, verbose_name="Situation de vie"
                    ),
                ),
                (
                    "donnees_medic",
                    models.JSONField(
                        blank=True, null=True, verbose_name="Données médicales"
                    ),
                ),
                (
                    "prest_compl",
                    models.BooleanField(
                        default=False, verbose_name="Prestations complémentaires"
                    ),
                ),
                (
                    "no_debiteur",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° de débiteur"
                    ),
                ),
                (
                    "no_centrale",
                    models.CharField(
                        blank=True,
                        max_length=10,
                        verbose_name="N° client de la centrale",
                    ),
                ),
                (
                    "envoi_facture",
                    models.BooleanField(
                        default=True, verbose_name="Envoyer une facture"
                    ),
                ),
                (
                    "animal_comp",
                    models.CharField(
                        blank=True, max_length=50, verbose_name="Animal de compagnie"
                    ),
                ),
                (
                    "samaritains",
                    models.PositiveSmallIntegerField(
                        blank=True,
                        null=True,
                        verbose_name="Samaritains comme répondants",
                    ),
                ),
                (
                    "spec_alarme",
                    models.TextField(
                        blank=True, verbose_name="Spécificités du système d'alarme"
                    ),
                ),
                (
                    "remarques_int",
                    models.TextField(blank=True, verbose_name="Remarques internes"),
                ),
                (
                    "remarques_ext",
                    models.TextField(blank=True, verbose_name="Remarques externes"),
                ),
                (
                    "cree_le",
                    models.DateTimeField(auto_now_add=True, verbose_name="Créé le"),
                ),
                (
                    "archive_le",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
                (
                    "date_deces",
                    models.DateField(blank=True, null=True, verbose_name="Décès le"),
                ),
                (
                    "accord_police",
                    models.BooleanField(
                        default=False,
                        verbose_name="Accord avant d’envoyer police ou ambulance",
                    ),
                ),
                (
                    "handicaps",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            blank=True,
                            choices=[
                                ("cannes", "Cannes"),
                                ("deamb", "Déambulateur"),
                                ("vision", "Handicap visuel"),
                                ("audition", "Handicap auditif"),
                                ("poids", "Surpoids"),
                                ("cognitif", "Troubles cognitifs"),
                            ],
                            max_length=10,
                        ),
                        blank=True,
                        null=True,
                        size=None,
                        verbose_name="Handicaps",
                    ),
                ),
                (
                    "tarif_avs_acquis",
                    models.BooleanField(default=False, verbose_name="Tarif AVS acquis"),
                ),
            ],
            options={
                "db_table": "alarme_client",
            },
            bases=(common.models.GeolocMixin, models.Model),
        ),
        migrations.CreateModel(
            name="Professionnel",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "no_centrale",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="N° à la centrale"
                    ),
                ),
                (
                    "type_pro",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("Médecin", "Médecin"),
                            ("Infirmière", "Infirmières indépendantes"),
                            ("SAD", "Soins à domicile"),
                            ("Sama", "Samaritain"),
                        ],
                        max_length=20,
                        verbose_name="Type de professionnel",
                    ),
                ),
                ("nom", models.CharField(max_length=100, verbose_name="Nom")),
                (
                    "prenom",
                    models.CharField(blank=True, max_length=50, verbose_name="Prénom"),
                ),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                ("npa", models.CharField(blank=True, max_length=5, verbose_name="NPA")),
                (
                    "localite",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="Localité"
                    ),
                ),
                (
                    "empl_geo",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.FloatField(), blank=True, null=True, size=2
                    ),
                ),
                (
                    "courriel",
                    models.EmailField(
                        blank=True, max_length=254, verbose_name="Courriel"
                    ),
                ),
                (
                    "tel_1",
                    common.fields.PhoneNumberField(
                        max_length=18, verbose_name="Téléphone (priorité 1)"
                    ),
                ),
                (
                    "tel_2",
                    common.fields.PhoneNumberField(
                        blank=True, max_length=18, verbose_name="Téléphone (priorité 2)"
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
                (
                    "date_archive",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
            ],
            options={
                "db_table": "alarme_professionnel",
            },
            bases=(common.models.GeolocMixin, models.Model),
        ),
        migrations.CreateModel(
            name="Referent",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "no_centrale",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="N° personne à la centrale"
                    ),
                ),
                ("nom", models.CharField(max_length=50, verbose_name="Nom")),
                (
                    "prenom",
                    models.CharField(blank=True, max_length=50, verbose_name="Prénom"),
                ),
                (
                    "salutation",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("M", "Monsieur"),
                            ("F", "Madame"),
                            ("P", "Madame et Monsieur"),
                            ("E", "Entreprise"),
                        ],
                        max_length=1,
                        verbose_name="Salutation",
                    ),
                ),
                (
                    "complement",
                    models.CharField(
                        blank=True, max_length=80, verbose_name="Nom (ligne 2)"
                    ),
                ),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                ("npa", models.CharField(blank=True, max_length=5, verbose_name="NPA")),
                (
                    "localite",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="Localité"
                    ),
                ),
                (
                    "pays",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("CH", "Suisse"),
                            ("DE", "Allemagne"),
                            ("FR", "France"),
                            ("IT", "Italie"),
                            ("AT", "Autriche"),
                            ("GB", "Grande-Bretagne"),
                            ("US", "États-Unis"),
                            ("FI", "Finlande"),
                            ("IL", "Israël"),
                        ],
                        max_length=2,
                        verbose_name="Pays",
                    ),
                ),
                (
                    "courriel",
                    models.EmailField(
                        blank=True, max_length=254, verbose_name="Courriel"
                    ),
                ),
                (
                    "relation",
                    models.CharField(
                        blank=True,
                        max_length=100,
                        verbose_name="Relation/lien de parenté",
                    ),
                ),
                (
                    "repondant",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Priorité répondant"
                    ),
                ),
                (
                    "referent",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Priorité référent"
                    ),
                ),
                (
                    "admin",
                    models.BooleanField(
                        default=False, verbose_name="Contact administratif et technique"
                    ),
                ),
                (
                    "facturation_pour",
                    common.fields.ChoiceArrayField(
                        base_field=models.CharField(
                            blank=True,
                            choices=[
                                ("al-abo", "Alarme (install + abo)"),
                                ("al-tout", "Alarme (tout)"),
                                ("transp", "Transports"),
                            ],
                            max_length=10,
                        ),
                        blank=True,
                        null=True,
                        size=None,
                        verbose_name="Adresse de facturation pour",
                    ),
                ),
                (
                    "no_debiteur",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° de débiteur"
                    ),
                ),
                (
                    "contractant",
                    models.BooleanField(
                        default=False, verbose_name="Personne contractante"
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
                ("courrier_envoye", models.DateField(blank=True, null=True)),
                (
                    "date_archive",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client"
                    ),
                ),
            ],
            options={
                "db_table": "alarme_referent",
            },
        ),
        migrations.CreateModel(
            name="ReferentTel",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "tel",
                    common.fields.PhoneNumberField(
                        max_length=18, verbose_name="Numéro"
                    ),
                ),
                ("priorite", models.PositiveSmallIntegerField(verbose_name="Priorité")),
                (
                    "remarque",
                    models.CharField(
                        blank=True, max_length=80, verbose_name="Remarque"
                    ),
                ),
                (
                    "referent",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="client.referent",
                    ),
                ),
            ],
            options={
                "verbose_name": "Téléphone de référent",
                "verbose_name_plural": "Téléphones de référent",
                "db_table": "alarme_referenttel",
            },
        ),
        migrations.CreateModel(
            name="ProfClient",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "priorite",
                    models.PositiveSmallIntegerField(
                        blank=True, null=True, verbose_name="Priorité"
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client"
                    ),
                ),
                (
                    "professionnel",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="client.professionnel",
                    ),
                ),
            ],
            options={
                "db_table": "alarme_profclient",
            },
        ),
        migrations.CreateModel(
            name="Journal",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
                ("details", models.TextField(blank=True)),
                ("quand", models.DateTimeField()),
                ("auto", models.BooleanField(default=True)),
                ("fichier", models.FileField(blank=True, upload_to="journal")),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="journaux",
                        to="client.client",
                    ),
                ),
                (
                    "qui",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "db_table": "alarme_journal",
                "get_latest_by": "quand",
            },
        ),
        migrations.CreateModel(
            name="Fichier",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("titre", models.CharField(max_length=150, verbose_name="Titre")),
                (
                    "fichier",
                    models.FileField(upload_to="clients", verbose_name="Fichier"),
                ),
                (
                    "typ",
                    models.CharField(
                        choices=[
                            ("doc", "Document général"),
                            ("quest", "Questionnaire signé"),
                            ("contrat", "Contrat signé"),
                        ],
                        default="doc",
                        max_length=10,
                        verbose_name="Type de document",
                    ),
                ),
                ("quand", models.DateTimeField()),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="fichiers",
                        to="client.client",
                    ),
                ),
                (
                    "qui",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "db_table": "alarme_fichier",
            },
        ),
        migrations.CreateModel(
            name="AdressePresence",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("depuis", models.DateField(verbose_name="Dès le")),
                (
                    "jusqua",
                    models.DateField(blank=True, null=True, verbose_name="Jusqu’au"),
                ),
                (
                    "dernier_contact",
                    models.DateField(
                        blank=True, null=True, verbose_name="Dernier contact"
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
                (
                    "adresse",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="presences",
                        to="client.adresseclient",
                    ),
                ),
            ],
            options={
                "db_table": "alarme_adressepresence",
            },
        ),
        migrations.AddField(
            model_name="client",
            name="professionnels",
            field=models.ManyToManyField(
                blank=True, through="client.ProfClient", to="client.professionnel"
            ),
        ),
        migrations.AddField(
            model_name="client",
            name="visiteur",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="visites",
                to="benevole.benevole",
                verbose_name="Bénévole visiteur",
            ),
        ),
        migrations.AddField(
            model_name="adresseclient",
            name="client",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="client.client"
            ),
        ),
        migrations.AddConstraint(
            model_name="client",
            constraint=models.UniqueConstraint(
                fields=("id_externe",), name="client_id_externe_unique"
            ),
        ),
    ]
