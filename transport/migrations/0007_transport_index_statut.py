from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0006_nullable_cancel_fields"),
    ]

    operations = [
        migrations.AddIndex(
            model_name="transport",
            index=models.Index(fields=["statut"], name="index_statut"),
        ),
    ]
