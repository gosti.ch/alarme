from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0038_is_ofas_modified'),
    ]

    operations = [
        migrations.AddField(
            model_name='dispo',
            name='categorie',
            field=models.CharField(
                choices=[('dispo', 'Disponibilité'), ('absence', 'Absence')], default='dispo',
                max_length=10, verbose_name='Catégorie'
            ),
        ),
    ]
