from django.db import migrations


def migrate_debiteur_facture(apps, schema_editor):
    Facture = apps.get_model('transport', 'Facture')
    Referent = apps.get_model('client', 'Referent')

    for fact in Facture.objects.exclude(no_debiteur=''):
        if fact.no_debiteur == fact.client.no_debiteur:
            continue
        ref = Referent.objects.get(client=fact.client, no_debiteur=fact.no_debiteur)
        fact.autre_debiteur = ref
        fact.save(update_fields=['autre_debiteur'])


class Migration(migrations.Migration):

    dependencies = [
        ("client", "__first__"),
        ("transport", "0034_facture_autre_debiteur"),
    ]

    operations = [
        migrations.RunPython(migrate_debiteur_facture),
    ]
