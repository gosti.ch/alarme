from datetime import date
from decimal import Decimal

from django.apps import apps
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Count, F, FilteredRelation, Max, Q
from django.db.models.functions import ExtractDay
from django.urls import reverse

from common.choices import Genres, Handicaps, Languages
from common.fields import ChoiceArrayField, PhoneNumberField
from common.models import GeolocMixin, Utilisateur
from common.utils import arrondi_5, current_app


class Activites(models.TextChoices):
    INSTALLATION = 'installation', 'Installation'
    VISITE = 'visite', 'Visite alarme'
    FORMATION = 'formation', 'Formation'
    TRANSPORT = 'transport', 'Transport'
    VISITES = 'visites', 'Visites'

    @classmethod
    def par_domaine(cls, domaine):
        return {
            'alarme': [cls.INSTALLATION, cls.VISITE, cls.FORMATION],
            'transport': [cls.TRANSPORT],
            'visite': [cls.VISITES],
        }.get(domaine, [])


class BenevoleQuerySet(models.QuerySet):
    def create(self, **kwargs):
        activites = kwargs.pop('activites', [])
        instance = super().create(**kwargs)
        for act in activites:
            Activite.objects.create(
                benevole=instance, activite=act, debut=date.today(), fin=kwargs.get('archive_le'),
            )
        return instance

    def par_activite(self, activite, include=None):
        filtre = Q(activite__activite=activite) & (
            Q(activite__fin__isnull=True) | Q(activite__fin__gt=date.today())
        )
        if include:
            filtre |= Q(pk=include.pk)
        return self.filter(filtre).distinct().order_by('nom', 'prenom')

    def par_domaine(self, domaine, actifs=True, include=None):
        if actifs:
            query = self.annotate(
                activites_actives=FilteredRelation(
                    "activite", condition=Q(activite__inactif=False) & Q(activite__fin__isnull=True)
                )
            )
            filtre = Q(activites_actives__activite__in=Activites.par_domaine(domaine))
        else:
            query = self
            filtre = Q(activite__activite__in=Activites.par_domaine(domaine))
        if include:
            filtre |= Q(pk=include.pk)
        return query.filter(filtre).distinct().order_by('nom', 'prenom')


class Benevole(GeolocMixin, models.Model):
    utilisateur = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    id_externe = models.BigIntegerField(null=True, blank=True)
    nom = models.CharField("Nom", max_length=50)
    prenom = models.CharField("Prénom", max_length=50)
    genre = models.CharField("Genre", max_length=1, choices=Genres.choices)
    date_naissance = models.DateField("Date de naissance", blank=True, null=True)
    rue = models.CharField("Rue", max_length=120, blank=True)
    npa = models.CharField("NPA", max_length=5)
    localite = models.CharField("Localité", max_length=30)
    empl_geo = ArrayField(models.FloatField(), size=2, blank=True, null=True)
    tel_prive = PhoneNumberField("Tél. privé", blank=True)
    tel_mobile = PhoneNumberField("Tél. mobile", blank=True)
    tel_prof = PhoneNumberField("Tél. prof.", blank=True)
    courriel = models.EmailField("Courriel", blank=True)
    no_debiteur = models.CharField("N° de débiteur", max_length=30, blank=True)
    formation = models.TextField("Formation", blank=True)
    langues = ChoiceArrayField(
        models.CharField(max_length=2, choices=Languages.choices, blank=True),
        blank=True, null=True
    )
    absences = models.TextField("Absences", blank=True)
    remarques = models.TextField("Remarques", blank=True)
    archive_le = models.DateField("Archivé le", blank=True, null=True)
    # Spécifique aux transports
    no_plaques = models.CharField("Numéro(s) de plaque", max_length=30, blank=True)
    macaron_depuis = models.DateField("Date d’obtention du macaron", blank=True, null=True)
    incompats = ChoiceArrayField(
        models.CharField(max_length=10, choices=Handicaps.choices, blank=True),
        blank=True, null=True, verbose_name='Incompatibilités'
    )

    objects = BenevoleQuerySet.as_manager()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_externe'], name="benevole_id_externe_unique"),
        ]

    def __str__(self):
        return self.nom_prenom

    @property
    def nom_prenom(self):
        return " ".join([self.nom, self.prenom])

    @property
    def incompats_verbose(self):
        return [dict(Handicaps.choices)[v] for v in (self.incompats or [])]

    def save(self, **kwargs):
        if self.utilisateur_id:
            # Keep Benevole.nom/prenom/courriel in sync with Utilisateur fields
            if self.nom != self.utilisateur.last_name:
                self.utilisateur.last_name = self.nom
                self.utilisateur.save()
            if self.prenom != self.utilisateur.first_name:
                self.utilisateur.first_name = self.prenom
                self.utilisateur.save()
            if self.courriel and self.courriel != self.utilisateur.email:
                self.utilisateur.email = self.courriel
                self.utilisateur.save()
        return super().save(**kwargs)

    def archiver(self):
        msg = ''
        visites = self.visites.filter(archive_le__isnull=True)
        num_visites = visites.count()
        if num_visites:
            visites.update(visiteur=None)
            msg = (
                f"Pour {num_visites} client(s) dont le bénévole était visiteur, "
                "le champ visiteur a été vidé."
            )
        self.archive_le = date.today()
        self.save(update_fields=['archive_le'])
        return msg

    def get_full_name(self):
        return f"{self.prenom} {self.nom}"

    def get_absolute_url(self):
        # Lien vers premier onglet de bénévole
        if apps.is_installed('transport'):
            return reverse('benevole-transports', args=[self.pk])
        return reverse('benevole-edit', args=[current_app(), self.pk])

    def clients_a_visiter(self):
        return self.visites.filter(archive_le__isnull=True, date_deces__isnull=True).annotate(
            derniere=Max('mission__effectuee'),
            num_inst=Count('installation', filter=(
                Q(installation__date_fin_abo__isnull=True) |
                Q(installation__date_fin_abo__gt=date.today())
            )),
        ).exclude(num_inst=0)

    def inactif(self, domaine=None):
        """
        Indique si le bénévole est inactif pour le domaine `domaine`,
        c'est-à-dire que toutes les activités du domaine sont marquées comme 'inactif'
        """
        if domaine is None:
            domaine = current_app()
        return all(
            act.inactif for act in self.activite_set.all()
            if act.activite in Activites.par_domaine(domaine)
        )

    def has_intervention_on(self, client, types=None):
        """
        Indique si ce bénévole a une intervention à faire pour le client, ou
        s'il en a fait une les dix derniers jours.
        """
        missions = self.mission_set.annotate(
            diff_days=ExtractDay(date.today() - F('effectuee'))
        ).filter(Q(effectuee=None) | Q(diff_days__lt=10))
        if types is not None:
            missions = missions.filter(type_mission__code__in=types)
        return missions.exists()

    def get_activites_display(self):
        return ', '.join([
            dict(Activites.choices)[act.activite] for act in self.activite_set.filter(
                Q(fin__isnull=True) | Q(fin__gt=date.today())
            )
        ])


class Activite(models.Model):
    benevole = models.ForeignKey(Benevole, on_delete=models.CASCADE)
    activite = models.CharField("Activité", max_length=15, choices=Activites.choices)
    debut = models.DateField("Depuis le")
    fin = models.DateField("Jusqu’au", null=True, blank=True)
    inactif = models.BooleanField("Temporairement inactif", default=False)
    note = models.TextField("Notes", blank=True)

    def __str__(self):
        return f"Activité «{self.get_activite_display()}» pour {self.benevole}"

    class Meta:
        verbose_name = "Activité"


class NoteFrais(models.Model):
    SERVICE_CHOICES = (
        ('alarme', 'Alarme'),
        ('transport', 'Transport'),
    )
    benevole = models.ForeignKey(Benevole, on_delete=models.CASCADE)
    mois = models.DateField()
    service = models.CharField(max_length=15, choices=SERVICE_CHOICES)
    kms = models.DecimalField(max_digits=5, decimal_places=1)
    date_export = models.DateTimeField(blank=True, null=True)
    id_externe = models.BigIntegerField(null=True, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='mois_service_unique', fields=['benevole', 'mois', 'service'])
        ]

    def __str__(self):
        return f"Note de frais «{self.service}» pour {self.benevole}, {self.mois.month}.{self.mois.year}"

    def somme_pour(self, code):
        return sum([lig.montant for lig in self.lignes.all() if lig.libelle.no == code] + [Decimal(0)])

    def quantite_pour(self, code):
        return sum([lig.quantite for lig in self.lignes.all() if lig.libelle.no == code] + [Decimal(0)])


class TypeFrais(models.Model):
    no = models.CharField("No article", max_length=10)
    libelle = models.CharField("Libellé", max_length=100)
    services = ChoiceArrayField(
        models.CharField(max_length=10, choices=NoteFrais.SERVICE_CHOICES),
    )

    def __str__(self):
        return f"Type de frais «{self.libelle}» ({self.no})"


class LigneFrais(models.Model):
    note = models.ForeignKey(NoteFrais, on_delete=models.CASCADE, related_name="lignes")
    libelle = models.ForeignKey(TypeFrais, on_delete=models.PROTECT)
    quantite = models.DecimalField(max_digits=7, decimal_places=2)
    montant_unit = models.DecimalField(max_digits=7, decimal_places=2)

    def __str__(self):
        return f"Ligne de frais ({self.libelle.libelle}, {self.montant}) pour «{self.note}»"

    @property
    def montant(self):
        return arrondi_5(self.quantite * self.montant_unit)


class Journal(models.Model):
    benevole = models.ForeignKey(Benevole, on_delete=models.CASCADE, related_name='journaux')
    description = models.TextField()
    quand = models.DateTimeField()
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    auto = models.BooleanField(default=True)

    class Meta:
        get_latest_by = "quand"

    def __str__(self):
        return f"{self.quand}: {self.description}"

    def can_edit(self, user):
        return self.auto is False and self.qui == user
