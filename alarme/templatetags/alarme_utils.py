from datetime import date, timedelta

from django.conf import settings
from django.template import Library
from django.utils.html import format_html

register = Library()


@register.filter
def date_delai(dt):
    if dt and dt <= date.today():
        return format_html('<span class="delai-passe">{}</span>', dt.strftime('%d.%m.%Y'))
    return dt.strftime('%d.%m.%Y') if dt else ''


@register.filter
def ligne_adresse(obj):
    return ((f"{obj.rue}, " if obj.rue else '') + f"{obj.npa} {obj.localite}").strip()


@register.filter
def visite_a_faire(client):
    if (
        not client.derniere or
        (date.today() - client.derniere) > timedelta(days=settings.DELAI_ENTRE_VISITES)
    ):
        return True
    return False


@register.filter
def m_to_km(value):
    return round(value / 100) / 10


@register.filter
def sec_to_min(value):
    return round(value / 60)


@register.filter
def label_with_class(bfield, css_class):
    return bfield.label_tag(attrs={'class': css_class})
