from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("client", "__first__"),
        ("transport", "0010_frais_typ"),
    ]

    operations = [
        migrations.CreateModel(
            name="Preference",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("typ", models.CharField(
                    choices=[("pref", "😀 Préféré"), ("incomp", "😡 Incompatible")],
                    max_length=10,
                    verbose_name="Type de lien",
                )),
                ("remarque", models.TextField(blank=True)),
                (
                    "chauffeur",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="benevole.benevole", related_name="preferences",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client",
                        related_name="preferences",
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name="preference",
            constraint=models.UniqueConstraint(
                fields=("client", "chauffeur"), name="client_chauffeur_unique"
            ),
        ),
    ]
