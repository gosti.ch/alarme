from datetime import date, datetime, time, timedelta
from decimal import Decimal

from freezegun import freeze_time

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.timezone import get_current_timezone, localtime, now
from django.utils.dateformat import format as django_format

from common.models import Utilisateur
from benevole.models import Activites, Benevole
from transport.models import Dispo, Frais, Regle, Transport
from .tests import DataMixin


@override_settings(EXEMPT_2FA_NETWORKS=[])
class PortailTests(DataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        user_benev = Utilisateur.objects.create_user(
            'benev@example.org', 'mepassword', first_name='Jeanne', last_name='d’Arc',
        )
        user_benev.user_permissions.add(Permission.objects.get(codename='view_transport'))
        cls.benev = Benevole.objects.create(
            nom="Duplain", prenom="Irma", utilisateur=user_benev,
            activites=[Activites.TRANSPORT],
        )

    def test_acces_benevole(self):
        demain = date.today() + timedelta(days=1)
        self.trclient.code_entree = '1234'
        self.trclient.save()
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(demain, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.ATTRIBUE,
        )
        self.assertFalse(transport.chauffeur_vu)
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home'), follow=True)
        self.assertTemplateUsed(response, 'transport/app/home_benevole.html')
        # Le transport n'est pas encore confirmé
        self.assertContains(response, "Aucun transport planifié pour le moment.")

        transport.statut=Transport.StatutChoices.CONFIRME
        transport.save()
        response = self.client.get(reverse('home'), follow=True)
        self.assertContains(response, "- <i>code entrée: 1234</i>")
        transport.refresh_from_db()
        self.assertTrue(transport.chauffeur_vu)

    def test_login_already_logged_in(self):
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('two_factor:login'), follow=True)
        self.assertRedirects(response, reverse('home-app'))

    def test_liste_jours_dispos(self):
        today = date.today()
        dispo = Dispo.objects.create(
            chauffeur=self.benev,
            debut=now().replace(hour=14, minute=0, microsecond=0),
            fin=now().replace(hour=17, minute=30, microsecond=0),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        # L’occurrence dans une semaine est annulée.
        dispo.cancel_for((dispo.debut + timedelta(days=7)).date())
        self.create_transport(
            client=self.trclient,
            heure_rdv=dispo.debut, duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne,
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.CONFIRME,
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-self-dispos'))
        self.assertContains(response, today.strftime('%d.%m'))
        self.assertContains(response, (today + timedelta(days=15)).strftime('%d.%m'))
        self.assertContains(response, "[1]")  # Le transport situé dans la dispo
        # L'occurrence annulée ne figure pas dans les dispos ici
        self.assertEqual(
            [day for day, occs in response.context['days'].items() if occs],
            [today, today + timedelta(days=14)]
        )
        # Liste pour des jours suivants (bouton + dans l'interface)
        response = self.client.get(
            reverse('benevole-self-dispos') + '?start=' + (today + timedelta(days=50)).strftime('%Y-%m-%d')
        )
        self.assertContains(response, (today + timedelta(days=50)).strftime('%d.%m'))

    def test_jour_dispos(self):
        dispo = Dispo.objects.create(
            chauffeur=self.benev,
            debut=localtime(now()).replace(hour=14, minute=0, second=0, microsecond=0),
            fin=localtime(now()).replace(hour=17, minute=30, second=0, microsecond=0),
            regle=Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        )
        dispo.refresh_from_db()
        # Seul le transport ATTRIBUE doit apparaître
        confirme = self.create_transport(
            client=self.trclient, destination=self.hne_ne,
            heure_rdv=dispo.debut, duree_rdv=timedelta(minutes=70),
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.CONFIRME,
        )
        self.create_transport(
            client=self.trclient, destination=self.hne_ne,
            heure_rdv=dispo.debut, duree_rdv=timedelta(minutes=70),
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.ANNULE,
        )
        self.create_transport(
            client=self.trclient, destination=self.hne_ne,
            heure_rdv=dispo.debut, duree_rdv=timedelta(minutes=70),
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.ATTRIBUE,
        )
        self.client.force_login(self.benev.utilisateur)
        today = date.today()
        response = self.client.get(reverse('benevole-agenda-day', args=[today.year, today.month, today.day]))
        response.context['agendas']
        self.assertEqual(
            response.context['agendas'][0]['week_events'][date.today()]['events'][time(13, 30)],
            [confirme]
        )
        self.assertNotContains(response, "Édition")

    def test_archives_mois(self):
        time_in_last_month = now().replace(day=1, hour=10, minute=0) - timedelta(days=15)

        def create_transp(
            heure_rdv=time_in_last_month, chauffeur=self.benev, statut=Transport.StatutChoices.CONTROLE,
            **kwargs
        ):
            return self.create_transport(
                client=self.trclient, heure_rdv=heure_rdv, retour=False,
                destination=self.hne_ne, chauffeur=chauffeur, statut=statut,
                km=12, **kwargs
            )

        # Ne doivent pas apparaître:
        create_transp(heure_rdv=time_in_last_month - timedelta(days=20))  # trop tôt
        create_transp(heure_rdv=now())  # trop tard
        create_transp(statut=Transport.StatutChoices.ATTRIBUE)  # mauvais statut
        create_transp(statut=Transport.StatutChoices.ANNULE, defrayer_chauffeur=False)  # annulé sans défr.
        # Doivent apparaître
        t1 = create_transp(duree_eff=timedelta(hours=2))
        t2 = create_transp(statut=Transport.StatutChoices.ANNULE, defrayer_chauffeur=None)
        t3 = create_transp(statut=Transport.StatutChoices.ANNULE, defrayer_chauffeur=True)
        Frais.objects.create(transport=t1, typ=Frais.TypeFrais.PARKING, cout='4.5', descriptif='Parking')

        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(
            reverse('benevole-archives-month', args=[time_in_last_month.year, time_in_last_month.month])
        )
        self.assertEqual(response.context['days'], {time_in_last_month.date(): [t1, t2, t3]})
        self.assertContains(response, "CE TRANSPORT A ÉTÉ ANNULÉ")
        self.assertEqual(response.context['sums'], {
            'total_km': Decimal('36.0'), 'total_frais': Decimal('4.50'),
            'total_duree': timedelta(hours=2), 'total_attente': None,
        })
        self.assertNotContains(response, 'None')

    def test_rapport_transport(self):
        hier = date.today() - timedelta(days=1)
        transport = self.create_transport(
            client=self.trclient,
            heure_rdv=datetime.combine(hier, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70), destination=self.hne_ne,
            retour=True, chauffeur=self.benev, statut=Transport.StatutChoices.EFFECTUE,
        )
        # transport seulement confirmé, mais heure_depart est déjà passée.
        self.create_transport(
            client=self.trclient,
            heure_depart=now() - timedelta(seconds=10),
            heure_rdv=now() - timedelta(hours=1),
            duree_rdv=timedelta(minutes=10), destination=self.hne_ne,
            retour=False, chauffeur=self.benev, statut=Transport.StatutChoices.CONFIRME,
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('home'), follow=True)
        self.assertContains(
            response,
            'Rapports <span class="badge text-bg-danger">2</span>'
        )
        response = self.client.get(reverse('transport-rapport-list'))
        rapport_url = reverse("benevole-rapport", args=[transport.pk])
        self.assertContains(
            response,
            f'<a href="{rapport_url}" class="list-group-item list-group-item-action">'
            f'Transport pour Donzé Léa le {django_format(transport.heure_depart, "l d.m.Y - H:i")}</a>',
            html=True
        )
        response = self.client.get(rapport_url)
        self.assertContains(response, 'label for="id_duree_eff">Durée effective :</label>')
        self.assertNotContains(response, 'Adresses')

    def test_can_edit_rapport_par_benevole(self):
        transport = Transport(date=date.today(), chauffeur=None)
        self.assertFalse(transport.can_edit_rapport(self.benev))
        transport.chauffeur = self.benev
        self.assertTrue(transport.can_edit_rapport(self.benev))
        with freeze_time("2023-03-01"):
            transport.date = date(2023, 3, 1)
            self.assertTrue(transport.can_edit_rapport(self.benev))
            transport.date = date(2023, 2, 20)
            self.assertTrue(transport.can_edit_rapport(self.benev))
            transport.date = date(2023, 1, 31)
            self.assertFalse(transport.can_edit_rapport(self.benev))
        with freeze_time("2023-03-02"):
            transport.date = date(2023, 3, 1)
            self.assertTrue(transport.can_edit_rapport(self.benev))
            transport.date = date(2023, 2, 28)
            self.assertFalse(transport.can_edit_rapport(self.benev))

    def test_edition_rapport_par_benevole(self):
        hier = date.today() - timedelta(days=1)
        transport = self.create_transport(
            client=self.trclient, destination=self.hne_ne,
            heure_rdv=datetime.combine(hier, time(11, 30), tzinfo=get_current_timezone()),
            duree_rdv=timedelta(minutes=70),
            retour=False, chauffeur=self.benev, statut=Transport.StatutChoices.CONTROLE,
            km=15, duree_eff=timedelta(hours=2),
        )
        self.client.force_login(self.benev.utilisateur)
        response = self.client.get(reverse('benevole-rapport', args=[transport.pk]))
        self.assertNotIn('temps_attente', response.context['form'].fields)
        response = self.client.post(reverse('benevole-rapport', args=[transport.pk]), data={
            'km': '25',
            'duree_eff': "01:30",
            'rapport_chauffeur': "J'ai corrigé les infos.",
            'frais-INITIAL_FORMS': '0',
            'frais-TOTAL_FORMS': '0',
        })
        self.assertRedirects(response, reverse('home-app'))
        transport.refresh_from_db()
        self.assertEqual(transport.statut, Transport.StatutChoices.RAPPORTE)
