from datetime import date
from decimal import Decimal

from django.conf import settings
from django.contrib import messages
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic import View

from benevole.models import LigneFrais, NoteFrais, TypeFrais
from common.utils import arrondi_5


def get_api():
    if settings.CID_API_URL and settings.ERP_API_TARGET == 'CID':
        from api.core import CIDApi
        return CIDApi()
    else:
        from .nav_api import NavisionAPI
        return NavisionAPI(settings.NAVISION_API_TARGET)


class FacturesTransmissionView(View):
    """Envoi des factures à l'ERP. Il peut s'agir:
        - d'une facture individuelle si `fact_id` est présent.
        - de toutes les factures d'un client si `client` est présent.
        - sinon, envoi de toutes les factures pas encore transmises.
    """
    def post(self, request, *args, **kwargs):
        if kwargs['app'] == 'alarme':
            from alarme.models import Facture
        elif kwargs['app'] == 'transport':
            from transport.models import Facture

        fact_pk = request.POST.get('fact_id')
        client = request.POST.get('client')
        factures = []
        if fact_pk:
            factures.append([get_object_or_404(Facture, pk=fact_pk)])
        else:
            if client:
                query = Facture.non_transmises().filter(client__pk=client).order_by('mois_facture')
            else:
                query = Facture.non_transmises().order_by(
                    'client__nom', 'client__prenom', 'client', 'mois_facture'
                )
            # Group Factures with similar client/date_facture/debiteur
            for fact in query:
                fact.adresse_facturation = fact.client.adresse_facturation(fact)
                if (
                    factures and factures[-1][0].client == fact.client and groupable(fact, factures[-1][0])
                ):
                    factures[-1].append(fact)
                else:
                    factures.append([fact])
        api = get_api()
        errors = []
        success = 0
        for facture in factures:
            try:
                result = api.create_invoice(kwargs['app'], facture)
            except Exception as exc:
                #traceback.print_exc()
                facture[0].export_err = str(exc)
                facture[0].save(update_fields=['export_err'])
                errors.append(exc)
                continue
            if 'error' in result:
                facture[0].export_err = f"Erreur API CID {result['error']['code']}: {result['error']['message']}"
                facture[0].save(update_fields=['export_err'])
                errors.append(facture[0].export_err)
            else:
                ext_id = result.get('id')
                montant_total = result.get('totalamount')
                for fact in facture:
                    fact.exporte = timezone.now()
                    if fact.export_err:
                        fact.export_err = ''
                    if ext_id:
                        fact.id_externe = ext_id
                    if len(facture) == 1 and hasattr(fact, 'montant_total') and montant_total:
                        fact.montant_total = montant_total
                    fact.save()
                    success += 1

        result = {
            'result': 'error' if errors else 'OK',
            'success': success,
            'errors': [str(err) for err in errors],
        }
        if request.accepts('text/html'):
            if fact_pk and len(errors) == 1:
                messages.error(request, f"Erreur d’envoi de facture : {errors[0]}")
            elif errors:
                messages.error(request, "%d facture(s) ont produit des erreurs" % len(errors))
            if success > 0:
                if success == 1:
                    msg = "1 facture a été transmise avec succès"
                else:
                    msg = "%d factures ont été transmises avec succès" % success
                messages.success(request, msg)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return JsonResponse(result)


def groupable(fact1, fact2):
    fact1_env = "" if (fact1.client.envoi_facture or not fact1.mois_facture) else "DA"
    fact2_env = "" if (fact2.client.envoi_facture or not fact2.mois_facture) else "DA"
    return (
        fact1_env == fact2_env and fact1.date_facture == fact2.date_facture and
        fact1.adresse_facturation == fact2.adresse_facturation
    )


class NoteFraisTransmissionView(View):
    def post(self, request, *args, **kwargs):
        self.api = get_api()
        self.errors = []
        self.success = 0
        if request.POST.get('note_id'):
            # Envoi note individuelle
            note = get_object_or_404(NoteFrais, pk=request.POST['note_id'])
            self._envoi_note(note)
        else:
            # Envoi notes du mois
            un_du_mois = date(kwargs['year'], kwargs['month'], 1)
            if request.POST.get('service') == 'alarme':
                self.envoi_frais_alarme(un_du_mois)
            elif request.POST.get('service') == 'transport':
                self.envoi_frais_transport(un_du_mois)
            else:
                raise Http404("No 'service' data or unknown service")
        result = {
            'result': 'error' if self.errors else 'OK',
            'success': self.success,
            'errors': [str(err) for err in self.errors],
        }
        if request.accepts('text/html'):
            for err in result['errors']:
                messages.error(request, f"Erreur: {err}")
            if self.success > 0:
                if self.success > 1:
                    msg = f"{success} notes de frais ont été transmises avec succès"
                else:
                    msg = "Une note de frais a été transmise avec succès"
                messages.success(request, msg)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        else:
            return JsonResponse(result)

    def _envoi_note(self, note):
        try:
            result = self.api.create_expense(note)
        except Exception as exc:
            self.errors.append(exc)
            return
        if 'error' in result:
            msg = f"Erreur API CID {result['error']['code']}: {result['error']['message']}"
            self.errors.append(msg)
            return

        # WARNING: once a note is sent, an exception should NOT revert the current transaction
        # Envoi note
        note.date_export = timezone.now()
        note.id_externe = result['id']
        note.save(update_fields=['date_export', 'id_externe'])
        self.success += 1

    def envoi_frais_alarme(self, mois):
        from alarme.models import Frais, Mission

        liste_notes = Mission.frais_mensuels(mois)
        for line in liste_notes:
            try:
                existing = NoteFrais.objects.get(
                    benevole_id=line['benevole__id'], service='alarme', mois=mois,
                )
            except NoteFrais.DoesNotExist:
                existing = None
            else:
                if existing and existing.date_export:
                    continue
            if existing:
                note = existing
            else:
                note = NoteFrais.objects.create(
                    benevole_id=line['benevole__id'],
                    service='alarme',
                    mois=mois,
                    kms=line['kms'],
                )
                # Participation frais tél.
                LigneFrais.objects.create(
                    note=note, libelle=TypeFrais.objects.get(no='NF-B51'),
                    quantite=Decimal('1'), montant_unit=Frais.PART_FRAIS_TEL
                )
                # Frais repas et divers
                if line['frais_repas'] > 0:
                    LigneFrais.objects.create(
                        note=note, libelle=TypeFrais.objects.get(no='NF-B07'),
                        quantite=Decimal('1'), montant_unit=line['frais_repas']
                    )
                if line['frais_autre'] > 0:
                    LigneFrais.objects.create(
                        note=note, libelle=TypeFrais.objects.get(no='NF-B06'),
                        quantite=Decimal('1'), montant_unit=line['frais_autre']
                    )
                # Indemnités km
                if line['kms'] > 0:
                    LigneFrais.objects.create(
                        note=note, libelle=TypeFrais.objects.get(no='NF-B01'),
                        quantite=line['kms'], montant_unit=settings.TARIF_CHAUFFEUR[0],
                    )
            self._envoi_note(note)

    def envoi_frais_transport(self, mois):
        from transport.utils import generer_notes_frais

        if not NoteFrais.objects.filter(service='transport', mois=mois).exists():
            generer_notes_frais(mois)

        liste_notes = NoteFrais.objects.filter(
            service='transport', mois=mois, date_export=None
        ).prefetch_related('lignes')
        for note in liste_notes:
            self._envoi_note(note)
