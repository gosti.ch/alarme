from django.db import migrations


def offset_statutchoices(apps, schema_editor):
    Transport = apps.get_model('transport', 'Transport')
    Transport.objects.filter(statut=7).update(statut=10)
    Transport.objects.filter(statut=6).update(statut=7)


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0004_transport_rapport_chauffeur"),
    ]

    operations = [
        migrations.RunPython(offset_statutchoices),
    ]
