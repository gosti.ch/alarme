import math
from datetime import time
from django import template
from django.template.defaultfilters import linebreaksbr
from django.urls import reverse
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime

register = template.Library()


@register.simple_tag
def mult(*args):
    return math.prod(args)


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.filter
def dispo_edit_url(dispo, user):
    if not dispo.can_edit(user):
        return ''
    if user.is_benevole:
        return reverse('benevole-self-dispo-edit', args=[dispo.pk])
    return reverse('benevole-dispo-edit', args=[dispo.pk])


@register.filter
def get_events(events, day_div):
    return events['events'].get(day_div)


@register.filter
def is_hour(time_):
    return time_.minute == 0


@register.filter
def is_meal(time_):
    return (time(12, 0) <= time_ < time(13, 30)) or (time(18, 30) <= time_ < time(19, 30))


@register.filter
def as_hour_min(dtime):
    return str(localtime(dtime).time())[:5]


@register.filter
def before_icons(event):
    icons = []
    # aller-retour icon?
    return mark_safe(' '.join(icons))


@register.filter
def event_details(event):
    descr = linebreaksbr(escape(event.description))
    return descr
