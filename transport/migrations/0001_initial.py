from django.conf import settings
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("client", "__first__"),
    ]

    operations = [
        migrations.CreateModel(
            name="Adresse",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=120, verbose_name="Nom")),
                (
                    "rue",
                    models.CharField(blank=True, max_length=120, verbose_name="Rue"),
                ),
                ("npa", models.CharField(max_length=5, verbose_name="NPA")),
                ("localite", models.CharField(max_length=30, verbose_name="Localité")),
                (
                    "empl_geo",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.FloatField(), blank=True, null=True, size=2
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Dispo",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("debut", models.DateTimeField(db_index=True, verbose_name="Début")),
                (
                    "fin",
                    models.DateTimeField(
                        blank=True, db_index=True, null=True, verbose_name="Fin"
                    ),
                ),
                (
                    "description",
                    models.TextField(blank=True, verbose_name="Description"),
                ),
                (
                    "fin_recurrence",
                    models.DateTimeField(
                        blank=True,
                        db_index=True,
                        null=True,
                        verbose_name="Dernière occurrence",
                    ),
                ),
                (
                    "chauffeur",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="dispos",
                        to="benevole.benevole",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Regle",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=40, verbose_name="Nom")),
                ("description", models.TextField(verbose_name="Description")),
                (
                    "frequence",
                    models.CharField(
                        choices=[
                            ("YEARLY", "Chaque année"),
                            ("MONTHLY", "Chaque mois"),
                            ("WEEKLY", "Chaque semaine"),
                            ("DAILY", "Chaque jour"),
                            ("HOURLY", "Chaque heure"),
                            ("MINUTELY", "Chaque minute"),
                            ("SECONDLY", "Chaque seconde"),
                        ],
                        max_length=10,
                        verbose_name="Fréquence",
                    ),
                ),
                ("params", models.TextField(blank=True, verbose_name="Params")),
            ],
            options={
                "verbose_name": "Règle",
                "verbose_name_plural": "Règles",
            },
        ),
        migrations.CreateModel(
            name="Transport",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "heure_rdv",
                    models.DateTimeField(verbose_name="Heure de rendez-vous"),
                ),
                (
                    "duree_rdv",
                    models.DurationField(verbose_name="Durée du rendez-vous"),
                ),
                ("retour", models.BooleanField(default=True)),
                (
                    "duree_eff",
                    models.DurationField(
                        blank=True, null=True, verbose_name="Durée effective du trajet"
                    ),
                ),
                (
                    "km",
                    models.DecimalField(
                        blank=True, decimal_places=1, max_digits=5, null=True
                    ),
                ),
                (
                    "statut",
                    models.PositiveSmallIntegerField(
                        choices=[
                            (1, "Saisi"),
                            (2, "Attribué"),
                            (3, "Confirmé"),
                            (4, "Effectué"),
                            (5, "Rapporté"),
                            (6, "Facturé"),
                            (7, "Annulé"),
                        ],
                        default=1,
                    ),
                ),
                (
                    "temps_attente",
                    models.DurationField(
                        blank=True, null=True, verbose_name="Temps d’attente"
                    ),
                ),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "date_annulation",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="Date d’annulation"
                    ),
                ),
                (
                    "raison_annulation",
                    models.TextField(blank=True, verbose_name="Raison de l’annulation"),
                ),
                (
                    "facturer_frais_annulation",
                    models.BooleanField(
                        default=False, verbose_name="Facturer frais d’annulation"
                    ),
                ),
                (
                    "defrayer_chauffeur",
                    models.BooleanField(
                        default=False, verbose_name="Défrayer les km du chauffeur"
                    ),
                ),
                (
                    "chauffeur",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="transports",
                        to="benevole.benevole",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="transports",
                        to="client.client",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Trajet",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "typ",
                    models.CharField(
                        choices=[
                            ("medic", "Médico-thérapeutique"),
                            ("particip", "Participatif-intégratif"),
                            ("emplettes", "Emplettes à deux"),
                            ("enfants", "Enfants"),
                        ],
                        max_length=10,
                        verbose_name="Type de déplacement",
                    ),
                ),
                ("heure_depart", models.DateTimeField(verbose_name="Heure de départ")),
                ("origine_domicile", models.BooleanField(default=True)),
                ("destination_domicile", models.BooleanField(default=False)),
                (
                    "duree_calc",
                    models.DurationField(
                        blank=True, null=True, verbose_name="Durée estimée du trajet"
                    ),
                ),
                (
                    "dist_calc",
                    models.IntegerField(
                        blank=True,
                        null=True,
                        verbose_name="Distance estimée du trajet [m]",
                    ),
                ),
                (
                    "destination_adr",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="+",
                        to="transport.adresse",
                    ),
                ),
                (
                    "origine_adr",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="+",
                        to="transport.adresse",
                    ),
                ),
                (
                    "transport",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="trajets",
                        to="transport.transport",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Occurrence",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("orig_start", models.DateTimeField(verbose_name="Début d’origine")),
                ("annule", models.BooleanField(default=False)),
                (
                    "dispo",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="exceptions",
                        to="transport.dispo",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="JournalTransport",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
                ("quand", models.DateTimeField()),
                (
                    "qui",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "transport",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="journaux",
                        to="transport.transport",
                        verbose_name="Transport",
                    ),
                ),
            ],
            options={
                "get_latest_by": "quand",
            },
        ),
        migrations.CreateModel(
            name="Frais",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("descriptif", models.TextField(verbose_name="Descriptif")),
                (
                    "cout",
                    models.DecimalField(
                        decimal_places=2, max_digits=5, verbose_name="Coût"
                    ),
                ),
                (
                    "justif",
                    models.FileField(
                        blank=True,
                        upload_to="justificatifs",
                        verbose_name="Justificatif",
                    ),
                ),
                (
                    "transport",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="frais",
                        to="transport.transport",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="dispo",
            name="regle",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="transport.regle",
                verbose_name="Règle",
            ),
        ),
        migrations.CreateModel(
            name="MemOccurrence",
            fields=[],
            options={
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("transport.dispo",),
        ),
    ]
