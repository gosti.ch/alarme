from datetime import date, timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now

from client.models import Alerte, Client
from common.models import Utilisateur
from common.utils import current_app
from .models import Questionnaire


class BesoinsTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(Permission.objects.get(codename='view_questionnaire'))
        cls.user.user_permissions.add(Permission.objects.get(codename='change_questionnaire'))

    def test_flux_lineaire(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', type_client=['alarme']
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:q-intro', args=[client.pk]))
        self.assertContains(response, "Contacts/répondants")
        response = self.client.post(reverse('besoins:q-intro', args=[client.pk]), data={'aide_entretien': ''}, follow=True)
        quest = client.questionnaire_set.first()
        self.assertEqual(response.context['view'].section_key, 'environnement')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'environnement']), data={
            'solitude': '2', 'solitude_details': '',
            'besoin_contacts': '1', 'besoin_contacts_details': 'Souhaite plus de contacts',
            'proche_aidant': '2', 'proche_aidant_details': '',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'logement')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'logement']), data={
            'aide_courses': '1', 'aide_courses_details': 'Peine à trouver certaines choses',
            'repas_auto': '1', 'repas_auto_details': '',
            'entretien_logement': '1', 'entretien_logement_details': 'OK',
            'moyens_aux': '2', 'moyens_aux_details': '',
            'chutes': '2', 'chutes_details': '',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'sante')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'sante']), data={
            'aide_soins': '2', 'aide_soins_details': 'Pas de besoin',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'socioeco')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'socioeco']), data={
            'besoin_admin': '1', 'besoin_admin_details': 'Volontiers pour impôts.',
        }, follow=True)
        self.assertEqual(response.context['view'].section_key, 'fin')
        response = self.client.post(reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'fin']) + '?termine=1', data={
            'remarques': 'Proposé quelques services.'
        })
        self.assertRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        quest.refresh_from_db()
        self.assertEqual(quest.termine_le.date(), date.today())

    def test_envoi_fetch_depuis_fin(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', type_client=['alarme']
        )
        quest = Questionnaire.objects.create(client=client, collab=self.user)
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:questionnaire', args=[client.pk, quest.pk, 'fin']),
            data={
                'message_services': '', 'remarques': 'Blah',
            },
            HTTP_X_REQUESTED_WITH='fetch',
        )
        self.assertEqual(response.json(), {'result': 'OK'})
        quest.refresh_from_db()
        self.assertEqual(quest.remarques, "Blah")

    def test_interet_alarme(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['transport']
        )
        quest = Questionnaire.objects.create(
            client=jill, collab=self.user, interet_alarme=True,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:questionnaire', args=[jill.pk, quest.pk, 'fin']) + '?termine=1',
            data={'remarques': 'Proposé service alarme.'}
        )
        self.assertRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        jill.refresh_from_db()
        self.assertEqual(jill.type_client, ['transport', 'alarme'])

    def test_message_services(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['transport', 'alarme']
        )
        quest = Questionnaire.objects.create(
            client=jill, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:questionnaire', args=[jill.pk, quest.pk, 'fin']) + '?termine=1',
            data={'remarques': '', 'message_services': "Nouvelle adresse: rue des Fleurs 5"}
        )
        self.assertRedirects(response, reverse('besoins:home'), fetch_redirect_response=False)
        num_alertes = 1 if current_app() in ['transport', 'alarme'] else 2
        self.assertEqual(Alerte.objects.filter(client=jill).count(), num_alertes)
        self.assertEqual(
            Alerte.objects.filter(client=jill).first().alerte,
            "Info suite au questionnaire des besoins: «Nouvelle adresse: rue des Fleurs 5»"
        )

    def test_non_reponse(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', type_client=['alarme']
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('besoins:q-intro', args=[client.pk]), data={
            'aide_entretien': '',
            'non_repondu': 'on',
            'remarques': "Pas le temps.",
        }, follow=True)
        quest = client.questionnaire_set.first()
        self.assertEqual(quest.termine_le.date(), date.today())
        self.assertIs(quest.non_repondu, True)
        self.assertEqual(quest.remarques, "Pas le temps.")
        response = self.client.get(reverse('besoins:home'))
        self.assertQuerySetEqual(response.context['object_list'], [])

    def test_quest_commence_puis_non_reponse(self):
        client = Client.objects.create(
            nom='Doe', prenom='Jill', date_naissance='1950-05-03', type_client=['alarme']
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('besoins:q-intro', args=[client.pk]), data={'aide_entretien': ''},
        )
        response = self.client.post(reverse('besoins:q-intro', args=[client.pk]), data={
            'aide_entretien': '',
            'non_repondu': 'on',
            'remarques': "Pas le temps.",
        })
        self.assertEqual(client.questionnaire_set.count(), 1)

    def test_home(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['alarme']
        )
        # Client non AVS
        Client.objects.create(
            nom='Doe', prenom='John', genre='M',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['alarme']
        )
        # Client Fonds transport handicap
        Client.objects.create(
            nom='Doe', prenom='John', genre='M',
            date_naissance=date.today() - timedelta(days=365 * 70), type_client=['alarme'],
            fonds_transport=True,
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:home'))
        self.assertQuerySetEqual(response.context['object_list'], [jill])

    def test_client_list(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['alarme']
        )
        # Client non AVS
        Client.objects.create(
            nom='Doe', prenom='John', genre='M',
            date_naissance=date.today() - timedelta(days=365 * 64), type_client=['alarme']
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:clients') + '?nom_prenom=doe')
        self.assertQuerySetEqual(response.context['object_list'], [jill])

    def test_statistiques(self):
        jill = Client.objects.create(
            nom='Doe', prenom='Jill', genre='F',
            date_naissance=date.today() - timedelta(days=365 * 70), type_client=['transport']
        )
        quest = Questionnaire.objects.create(
            client=jill, collab=self.user,
            **{field.name: 1 for field in Questionnaire._meta.get_fields()
               if field.get_internal_type() == 'SmallIntegerField'
            }, termine_le=now(),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:stats-besoins'))
        # mois transport, total transport, mois total, total total = 4
        self.assertContains(response, '<td class="num">1</td>', count=4)

    def test_liste_quest_non_repondus(self):
        q_non_repondu = Questionnaire.objects.create(
            client=Client.objects.create(nom='A'), collab=self.user,
            non_repondu=True, termine_le=now(), remarques="Bla bla"
        )
        Questionnaire.objects.create(
            client=Client.objects.create(nom='B'), collab=self.user,
            non_repondu=False, termine_le=now(), remarques="Bla bla"
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('besoins:liste-non-repondus'))
        self.assertQuerySetEqual(response.context['object_list'], [q_non_repondu])
