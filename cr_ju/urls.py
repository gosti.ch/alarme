from django.urls import path

from . import views

urlpatterns = [
    path('factures/export/', views.FacturesExportView.as_view(), name='factures-export'),
]
