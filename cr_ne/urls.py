from django.urls import path

from . import views

urlpatterns = [
    path('factures/<str:app>/transmettre/', views.FacturesTransmissionView.as_view(),
        name='factures-transmission'),
    path('notesfrais/transmettre/<int:year>/<int:month>/', views.NoteFraisTransmissionView.as_view(),
        name='notesfrais-transmission'),
]
