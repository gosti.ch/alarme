from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0026_transport_date_facture"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="statut_fact",
            field=models.PositiveSmallIntegerField(
                choices=[
                    (1, "Facturer le transport"),
                    (2, "Facturer frais d’annulation"),
                    (3, "Facturer frais d’annulation et km chauffeur"),
                    (99, "Ne rien facturer"),
                ],
                default=1,
                verbose_name="Statut de facturation",
            ),
        ),
    ]
