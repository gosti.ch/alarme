from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("transport", "0036_remove_facture_no_debiteur"),
    ]

    operations = [
        migrations.RunSQL("""
create or replace function is_ofas(
    transport_pk int, transport_date date, tarif_avs_acquis boolean, date_naissance date, genre varchar(1)
)
returns boolean
language plpgsql
as
$$
declare
   is_ofas boolean;
begin
   select ARRAY_AGG(typ) && (ARRAY['medic', 'particip'])::varchar(10)[] AND (
        tarif_avs_acquis OR
        FLOOR((EXTRACT(DAY FROM (interval '1 day' * (transport_date - date_naissance))) / 365.25)) >= 65.0 OR
        (genre = 'F' AND
         FLOOR((EXTRACT(DAY FROM (interval '1 day' * (transport_date - date_naissance))) / 365.25)) >= 64.0)
   )
   into is_ofas
   from transport_trajet
   where transport_id = transport_pk;
   
   return is_ofas;
end;
$$;""",
            "DROP function is_ofas(transport_pk int, transport_date date, tarif_avs_acquis boolean," "date_naissance date, genre varchar(1))"
        )
    ]
