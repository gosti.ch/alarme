from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("client", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Questionnaire",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                (
                    "termine_le",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="Questionnaire terminé le"
                    ),
                ),
                ("aide_entretien", models.TextField(blank=True)),
                (
                    "remarques",
                    models.TextField(
                        blank=True,
                        verbose_name="Autres remarques générales sur l’entretien",
                    ),
                ),
                ("solitude", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("solitude_details", models.TextField(blank=True)),
                ("besoin_contacts", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("besoin_contacts_details", models.TextField(blank=True)),
                ("proche_aidant", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("proche_aidant_details", models.TextField(blank=True)),
                ("aide_courses", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("aide_courses_details", models.TextField(blank=True)),
                ("repas_auto", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("repas_auto_details", models.TextField(blank=True)),
                ("entretien_logement", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("entretien_logement_details", models.TextField(blank=True)),
                ("moyens_aux", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("moyens_aux_details", models.TextField(blank=True)),
                ("chutes", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("chutes_details", models.TextField(blank=True)),
                ("aide_soins", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("aide_soins_details", models.TextField(blank=True)),
                ("besoin_admin", models.SmallIntegerField(
                    choices=[(-1, "Pas de réponse"), (1, "Oui"), (2, "Non")], blank=True, null=True
                )),
                ("besoin_admin_details", models.TextField(blank=True)),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client"
                    ),
                ),
                (
                    "collab",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
