from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0008_transport_modele"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transport",
            name="duree_rdv",
            field=models.DurationField(
                blank=True, null=True, verbose_name="Durée du rendez-vous"
            ),
        ),
    ]
