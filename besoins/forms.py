from django import forms

from client.models import ClientType
from common.utils import RegionFinder
from .models import Questionnaire


class FilterClientForm(forms.Form):
    region = forms.ChoiceField(
        choices=[('', 'Toutes')] + [(k, k) for k in RegionFinder.regions.keys()] + [('Autre', 'Autre'),],
        required=False
    )
    service = forms.ChoiceField(choices=[('', 'Tous')] + ClientType.choices, required=False)

    def filter(self, clients):
        if self.cleaned_data['region']:
            clients = clients.filter(RegionFinder.get_region_filter('npa', self.cleaned_data['region']))
        if self.cleaned_data['service']:
            clients = clients.filter(type_client__contains=[self.cleaned_data['service']])
        return clients


class IntroForm(forms.ModelForm):
    non_repondu = forms.BooleanField(
        label="La personne ne souhaite pas répondre à ce questionnaire pour cette année",
        required=False
    )

    class Meta:
        model = Questionnaire
        fields = ['aide_entretien', 'non_repondu', 'remarques']


class EnvironnementForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = [
            'solitude', 'solitude_details', 'besoin_contacts', 'besoin_contacts_details',
            'proche_aidant', 'proche_aidant_details'
        ]
        widgets = {
            'solitude': forms.RadioSelect,
            'besoin_contacts': forms.RadioSelect,
            'proche_aidant': forms.RadioSelect,
        }


class LogementForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = [
            'aide_courses', 'aide_courses_details', 'repas_auto', 'repas_auto_details',
            'entretien_logement', 'entretien_logement_details',
            'moyens_aux', 'moyens_aux_details', 'chutes', 'chutes_details', 'interet_alarme'
        ]
        widgets = {
            'aide_courses': forms.RadioSelect,
            'repas_auto': forms.RadioSelect,
            'entretien_logement': forms.RadioSelect,
            'moyens_aux': forms.RadioSelect,
            'chutes': forms.RadioSelect,
        }

    def __init__(self, instance=None, **kwargs):
        super().__init__(instance=instance, **kwargs)
        if 'alarme' in instance.client.type_client:
            del self.fields['interet_alarme']


class SanteForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = ['aide_soins', 'aide_soins_details']
        widgets = {
            'aide_soins': forms.RadioSelect,
        }


class SocioEcoForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = ['besoin_admin', 'besoin_admin_details']
        widgets = {
            'besoin_admin': forms.RadioSelect,
        }


class FinForm(forms.ModelForm):
    class Meta:
        model = Questionnaire
        fields = ['message_services', 'remarques']
