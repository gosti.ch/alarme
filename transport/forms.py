import copy
import unicodedata
from datetime import date, datetime, timedelta

from django import forms
from django.db import transaction
from django.db.models import Max, Q, Value
from django.db.models.functions import Replace
from django.urls import reverse_lazy
from django.utils.timezone import get_current_timezone, localtime, make_aware

from benevole.forms import BenevoleFilterForm
from client.forms import ClientEditFormBase, ClientFilterFormBase
from client.models import Client
from common.templatetags.common_utils import format_duree
from common.forms import (
    BootstrapMixin, BSCheckboxSelectMultiple, BSRadioSelect, DateInput,
    HiddenDeleteInlineFormSet, HMDurationField, ModelForm, NPALocaliteMixin,
    TimeInput
)
from common.views import GeoAddressFormMixin
from .models import (
    Adresse, Benevole, Dispo, Frais, Preference, Regle, Trajet, Transport,
    TransportModel
)

REPET_CHOICES = (
    ('', 'Aucune'),
    ('WEEKLY', 'Chaque semaine'),
    ('BI-WEEKLY', 'Une semaine sur deux'),
    ('DAILY', 'Chaque jour'),
    ('DAILY-open', 'Chaque jour, du lundi au vendredi'),
)

DOMICILE_VALUE = Adresse(nom="Domicile")


class AllerRetourWidget(BSRadioSelect):
    template_name = 'transport/widgets/allerretour.html'


class RepetitionMixin:
    def set_repetition(self, modele):
        if 'repetition' in self.changed_data:
            repet = self.cleaned_data.get('repetition')
            if repet == 'WEEKLY':
                modele.regle, _ = Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')
            elif repet == 'BI-WEEKLY':
                modele.regle, _ = Regle.objects.get_or_create(
                    nom='Une semaine sur deux', frequence='WEEKLY', params='interval:2'
                )
            elif repet == 'DAILY':
                modele.regle, _ = Regle.objects.get_or_create(nom='Chaque jour', frequence='DAILY')
            elif repet == 'DAILY-open':
                modele.regle, _ = Regle.objects.get_or_create(
                    nom='Chaque jour, du lundi au vendredi', frequence='DAILY',
                    params='byweekday:MO,TU,WE,TH,FR'
                )
        if 'repetition_fin' in self.changed_data:
            repet_fin = self.cleaned_data.get('repetition_fin')
            if repet_fin:
                modele.fin_recurrence = make_aware(
                    datetime(repet_fin.year, repet_fin.month, repet_fin.day, 23, 59)
                )


class DestinationField(forms.ModelChoiceField):
    def clean(self, value):
        if value == "-1":
            return DOMICILE_VALUE
        return super().clean(value)


class TrajetForm(BootstrapMixin, ModelForm):
    destination_adr = DestinationField(queryset=Adresse.objects.all(), widget=forms.HiddenInput, required=False)
    destination_adr_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Destination client…',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('adresse-autocomplete'),
        'data-pkfield': 'destination_adr',
    }), required=False)

    class Meta:
        model = Trajet
        fields = ['destination_adr', 'destination_princ']
        widgets = {'destination_princ': forms.HiddenInput}

    def __init__(self, **kwargs):
        if 'instance' in kwargs and kwargs['instance'].destination_domicile:
            kwargs['initial'] = {'destination_adr': -1}
        super().__init__(**kwargs)
        self.fields['destination_adr_select'].widget.attrs['data-pkfield'] = f'{self.prefix}-destination_adr'


class BaseTrajetFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput
    ordering_widget = forms.HiddenInput


class HeureField(forms.TimeField):
    def has_changed(self, initial, data):
        if isinstance(initial, str) and isinstance(data, str) and initial == data:
            return False
        try:
            return not datetime.strptime(data, '%H:%M').time() == localtime(initial).time()
        except Exception:
            pass
        return super().has_changed(initial, data)


class SaisieTransportForm(BootstrapMixin, RepetitionMixin, ModelForm):
    client = forms.ModelChoiceField(queryset=Client.objects.all(), widget=forms.HiddenInput)
    client_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Client…',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('client-autocomplete', args=['tous']),
        'data-clienturl': reverse_lazy('client-address'),
        'data-pkfield': 'client',
    }))
    origine_adr = forms.ModelChoiceField(queryset=Adresse.objects.all(), widget=forms.HiddenInput, required=False)
    origine_adr_select = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'placeholder': 'Départ du transport… (domicile par défaut)',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('adresse-autocomplete'),
        'data-pkfield': 'origine_adr',
    }), required=False)
    heure_depart = HeureField(label="Heure de départ", widget=TimeInput, required=True)
    heure_rdv = HeureField(label="Heure du rendez-vous", widget=TimeInput(), required=True)
    duree_rdv = HMDurationField(required=False)
    typ = forms.ChoiceField(label="Type de déplacement", choices=Trajet.Types.choices, required=True)
    retour = forms.BooleanField(
        widget=AllerRetourWidget(choices=((True, 'Aller-retour'), (False, 'Aller simple'))),
        initial=True, required=False,
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = Transport
        exclude = ['chauffeur']
        fields = [
            'client', 'date', 'heure_rdv', 'duree_rdv', 'retour', 'remarques',
        ]

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        TrajetFormSet = forms.inlineformset_factory(
            Transport, Trajet, form=TrajetForm, formset=BaseTrajetFormSet,
            extra=0 if (self.instance.has_trajets and self.instance.retour) else 1,
            min_num=1, can_delete=True, can_order=True
        )
        self.formset = TrajetFormSet(
            data=data, instance=kwargs['instance'],
        )
        if kwargs['instance']:
            # Surtout important pour les instances de transport en mémoire provenant des modèles.
            self.formset._queryset = copy.deepcopy(kwargs['instance'].trajets_tries)
        self.formset.forms[0].fields['destination_adr'].required = True
        if self.instance.pk is None:
            self.formset.forms[0].fields['destination_princ'].initial = True
        else:
            self.fields['heure_depart'].initial = self.instance.heure_depart.strftime('%H:%M')
            if self.instance.retour:
                self.fields['heure_depart'].widget.attrs['data-dureetrajet'] = format_duree(self.instance.duree_retour)
        if self.instance.pk or self.instance.modele_id:
            del self.fields['client']
            del self.fields['client_select']
            del self.fields['repetition']
            del self.fields['repetition_fin']

    def is_valid(self):
        return all([self.formset.is_valid(), super().is_valid()])

    def clean_client(self):
        client = self.cleaned_data.get('client')
        if client and not client.date_naissance:
            raise forms.ValidationError("Impossible de créer un transport pour un client sans date de naissance")
        if client and not client.check_geolocalized():
            raise forms.ValidationError("L’adresse de cette personne ne peut pas être géolocalisée.")
        return client

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('heure_rdv'):
            cleaned_data['heure_rdv'] = datetime.combine(
                cleaned_data['date'], cleaned_data['heure_rdv'], tzinfo=get_current_timezone()
            )
        if cleaned_data.get('retour') and not cleaned_data.get('duree_rdv'):
            self.add_error('duree_rdv', "La durée de rendez-vous est obligatoire.")
        if self.errors or not len(self.formset.forms) or self.formset.total_error_count() > 0:
            return cleaned_data

        def check_orig_dest(traj):
            if traj.origine_adr and traj.destination_adr and traj.origine_adr == traj.destination_adr:
                self.add_error(None, "L’origine et la destination d’un trajet doivent être différents")
            elif traj.origine_domicile and traj.destination_domicile:
                # Not using add_error, because later calling trajet.destination will crash
                raise forms.ValidationError("L’origine et la destination ne peuvent pas être les deux le domicile")

        def set_destination(trajet, dest):
            if dest is DOMICILE_VALUE or dest is None:
                trajet.destination_domicile = True
                trajet.destination_adr = None
            else:
                trajet.destination_domicile = False
                trajet.destination_adr = dest
            check_orig_dest(trajet)

        self.trajets = []
        heure_premier_depart = datetime.combine(
            cleaned_data['date'], cleaned_data['heure_depart'],
            tzinfo=get_current_timezone()
        )
        trajet_initial = True
        ordered_forms = self.formset.ordered_forms
        for idx, subform in enumerate(ordered_forms):
            if subform.cleaned_data.get('destination_adr') is None:
                continue
            if trajet_initial:
                self.trajets.append(
                    Trajet(
                        transport=None, typ=self.cleaned_data.get('typ'),
                        heure_depart=heure_premier_depart,
                        origine_domicile=cleaned_data['origine_adr'] is None,
                        origine_adr=cleaned_data['origine_adr'],
                        destination_princ=subform.cleaned_data['destination_princ'],
                    )
                )
                set_destination(self.trajets[-1], subform.cleaned_data['destination_adr'])
                trajet_initial = False
                continue
            if self.trajets[-1].destination_princ and not cleaned_data['retour']:
                break
            is_last_subform = idx >= len(ordered_forms) - 1
            if not is_last_subform or not cleaned_data['retour']:
                self.trajets.append(
                    Trajet(
                        transport=None, typ=cleaned_data.get('typ'),
                        origine_domicile=False,
                        origine_adr=self.trajets[-1].destination,
                        destination_princ=subform.cleaned_data['destination_princ'],
                    )
                )
                set_destination(self.trajets[-1], subform.cleaned_data['destination_adr'])

        if cleaned_data['retour']:
            # If last subform.destination_adr is set, use that here
            # Extra form in initial creation is not in ordered_form (extra unchanged form)
            last_form = ordered_forms[-1] if len(ordered_forms) > 1 else self.formset.forms[-1]
            last_destination = last_form.cleaned_data.get('destination_adr')
            if last_destination and last_destination is not DOMICILE_VALUE:
                dest_adr = self.formset.forms[-1].cleaned_data.get('destination_adr')
                dest_dom = False
            else:
                dest_adr = None
                dest_dom = True
            self.trajets.append(
                Trajet(
                    transport=None, typ=cleaned_data.get('typ'),
                    origine_domicile=self.trajets[-1].destination_domicile,
                    origine_adr=self.trajets[-1].destination if not self.trajets[-1].destination_domicile else None,
                    destination_princ=False,
                )
            )
            set_destination(self.trajets[-1], last_destination)
        return cleaned_data

    def save(self, **kwargs):
        should_save_trajets = (
            self.instance.pk is None or
            self.formset.has_changed() or
            len(set(self.changed_data) - set([
                # Champs qui ne nécessitent pas un recalcul des trajets (pour typ, voir plus bas)
                'typ', 'remarques',
            ]))
        )
        with transaction.atomic():
            if not self.instance.pk and self.instance.modele:
                self.instance.save()
            transp = super().save(**kwargs)
            if self.cleaned_data.get('repetition'):
                modele = TransportModel.objects.create(
                    client=transp.client,
                    heure_depart=self.cleaned_data['heure_depart'],
                    heure_rdv=transp.heure_rdv,
                    duree_rdv=transp.duree_rdv,
                    retour=transp.retour,
                    remarques=transp.remarques,
                )
                self.set_repetition(modele)
                transp.modele = modele
                transp.save()
                modele.save()

            if should_save_trajets:
                transp.trajets.all().delete()
                trajet_prec = None
                for trajet in self.trajets:
                    trajet.transport = transp
                    if not trajet.heure_depart and trajet_prec:
                        if trajet_prec.destination_princ:
                            trajet.heure_depart = transp.heure_rdv + transp.duree_rdv
                        else:
                            # En attendant que trajet possède une durée de rdv
                            trajet.heure_depart = trajet_prec.heure_arrivee
                    trajet.save()
                    trajet.calc_trajet()
                    trajet_prec = trajet
                if transp.chauffeur:
                    transp.calc_chauffeur_dists(save=True)

            elif 'typ' in self.changed_data:
                for trajet in transp.trajets.all():
                    trajet.typ = self.cleaned_data['typ']
                    trajet.save(update_fields=['typ'])
        return transp


class TrajetModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, traj):
        return (
            f"Trajet pour {traj.transport.client} de «{traj.origine}» jusqu’à «{traj.destination}», "
            f"à {localtime(traj.heure_depart).strftime('%H:%M')}"
        )


class TrajetAPlusieursForm(BootstrapMixin, forms.Form):
    trajets = TrajetModelMultipleChoiceField(queryset=None, widget=BSCheckboxSelectMultiple, required=False)

    def __init__(self, trajet=None, **kwargs):
        self.trajet = trajet
        if trajet.commun_id:
            kwargs['initial']['trajets'] = trajet.commun.trajet_set.all()
        super().__init__(**kwargs)
        # Définir les trajets communs possibles
        self.fields['trajets'].queryset = Trajet.objects.exclude(transport__pk=self.trajet.transport_id).filter(
            transport__chauffeur=self.trajet.transport.chauffeur,
            heure_depart__date=self.trajet.heure_depart.date()
        ).select_related('transport')


class TransportModelForm(BootstrapMixin, ModelForm):
    heure_rdv = forms.TimeField(label="Heure du rendez-vous", widget=TimeInput, required=True)
    fin_recurrence = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = TransportModel
        fields = [
            'heure_depart', 'heure_rdv', 'duree_rdv', 'remarques', 'suspension_depuis', 'fin_recurrence',
        ]
        field_classes = {
            'duree_rdv': HMDurationField,
        }
        widgets = {
            'heure_depart': TimeInput,
        }

    def clean(self):
        cleaned_data = super().clean()
        cleaned_data['heure_rdv'] = datetime.combine(
            self.instance.heure_rdv.date(), cleaned_data['heure_rdv'], tzinfo=localtime(self.instance.heure_rdv).tzinfo
        )
        return cleaned_data


class TransportFilterForm(BootstrapMixin, forms.Form):
    # immediate_submit needed on individual widgets, as the widgets may be outside of the form DOM node
    date = forms.DateField(widget=DateInput(attrs={'class': 'immediate_submit'}), required=False)
    client = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom client…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )
    destination = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Destination…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )
    chauffeur = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom chauffeur…', 'autocomplete': 'off', 'class': 'immediate_submit'}
        ),
        required=False
    )

    def __init__(self, statut=None, **kwargs):
        self.name = f"ffstatut{statut}"
        super().__init__(**kwargs)
        if statut == Transport.StatutChoices.SAISI:
            del self.fields['chauffeur']
        for fname in self.fields:
            self.fields[fname].widget.attrs['form'] = self.name

    def filter(self, transports):
        if self.cleaned_data['date']:
            transports = transports.filter(date=self.cleaned_data['date'])
        if self.cleaned_data['client']:
            transports = transports.filter(
                Q(client__nom__unaccent__icontains=self.cleaned_data['client']) |
                Q(client__prenom__unaccent__icontains=self.cleaned_data['client'])
            )
        if self.cleaned_data['destination']:
            transports = transports.filter(
                Q(trajets__destination_adr__nom__unaccent__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__rue__unaccent__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__npa__icontains=self.cleaned_data['destination']) |
                Q(trajets__destination_adr__localite__unaccent__icontains=self.cleaned_data['destination'])
            )
        if self.cleaned_data.get('chauffeur'):
            transports = transports.filter(
                Q(chauffeur__nom__unaccent__icontains=self.cleaned_data['chauffeur'])
            )
        return transports

    def filter_occurrences(self, occurrences):
        def normalize(txt):
            return unicodedata.normalize('NFKD', txt.lower()).encode('ASCII', 'ignore').decode()

        if self.cleaned_data['date'] and occurrences:
            occurrences = [occ for occ in occurrences if occ.date == self.cleaned_data['date']]
        if self.cleaned_data['client'] and occurrences:
            client = occurrences[0].client
            term = normalize(self.cleaned_data['client'])
            if term in normalize(client.nom) or term in normalize(client.prenom):
                pass
            else:
                occurrences = []
        if self.cleaned_data['destination'] and occurrences:
            dest = occurrences[0].destination
            term = normalize(self.cleaned_data['destination'])
            if (
                term in normalize(getattr(dest, 'nom', '')) or term in normalize(dest.rue) or
                term in normalize(dest.npa) or term in normalize(dest.localite)
            ):
                pass
            else:
                occurrences = []
        # Pas de chauffeur défini pour les occurrences en mémoire
        return occurrences


class TransportArchivesFilterForm(TransportFilterForm):
    statut = forms.ChoiceField(
        choices=[('', '-------')] + [
            c for c in Transport.StatutChoices.choices if c[0] >= Transport.StatutChoices.EFFECTUE
        ],
        required=False
    )

    def filter(self, transports):
        transports = super().filter(transports)
        if self.cleaned_data['statut']:
            transports = transports.filter(statut=self.cleaned_data['statut'])
        return transports


class ClientEditForm(NPALocaliteMixin, ClientEditFormBase):
    class Meta(ClientEditFormBase.Meta):
        fields = [
            'nom', 'prenom', 'genre', 'date_naissance', 'c_o', 'rue', 'case_postale',
            'npa', 'localite', 'tel_1', 'tel_2', 'courriel', 'langues', 'no_debiteur',
            'type_logement', 'type_logement_info', 'etage', 'nb_pieces', 'ascenseur',
            'code_entree', 'tarif_avs_acquis', 'fonds_transport', 'handicaps', 'date_deces',
            'remarques_int_transport', 'remarques_ext',
        ]

    benev_hidden_fields = {'code_entree', 'tarif_avs_acquis', 'fonds_transport', 'remarques_int_transport'}
    field_order = [
        'nom', 'prenom', 'genre', 'date_naissance', 'rue', 'npalocalite',
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance and (self.instance.age() or 60) >= 65:
            # Afficher tarif_avs_acquis seulement pour clients de moins de 65 ans.
            self.fields.pop('tarif_avs_acquis', None)


class PreferenceEditForm(BootstrapMixin, ModelForm):
    typ = forms.ChoiceField(label='', choices=Preference.PREF_TYPE, widget=BSRadioSelect)

    class Meta:
        model = Preference
        fields = ['chauffeur', 'typ', 'remarque']

    def __init__(self, client=None, **kwargs):
        self.client = client
        super().__init__(**kwargs)
        if kwargs['instance'] and kwargs['instance'].pk:
            del self.fields['chauffeur']
            del self.fields['typ']
        else:
            self.fields['chauffeur'].queryset = Benevole.objects.par_domaine(
                'transport', actifs=True
            )

    def clean(self):
        cleaned_data = super().clean()
        # Uniqueness not auto checked because the client field is not in the form.
        if not self.instance.pk and Preference.objects.filter(
            client=self.client, chauffeur=cleaned_data.get('chauffeur')
        ).exists():
            self.add_error(None, "Il existe déjà une préférence ou incompatibilité entre ce client et ce chauffeur")
        return cleaned_data


class ClientFilterForm(ClientFilterFormBase):
    LIST_CHOICES = (
        ('', '-------'),
        ('no_debit', "Clients sans n° débiteur"),
        ('autres_debit', "Clients avec autre débiteur"),
    )
    recherche_part = False
    recherche_deb = True
    fact_items = ['transp']

    def filter(self, clients):
        clients = super().filter(clients)
        if self.cleaned_data['listes'] == 'no_debit':
            clients = clients.filter(type_client__contains=['transport']).annotate(
                referent_no_debiteur=Max(
                    'referent__no_debiteur', filter=Q(
                        referent__facturation_pour__contains=['transp'], referent__date_archive=None
                    )
                )
            ).filter(
                Q(referent_no_debiteur='') |
                (Q(referent_no_debiteur=None) & Q(no_debiteur=''))
            )
            clients._hints['_has_referent__no_debiteur'] = True
        return clients


class ChauffeurFilterForm(BenevoleFilterForm):
    macaron_echu = forms.BooleanField(label="Échéance macaron proche", required=False)

    def filter(self, benevs):
        benevs = super().filter(benevs)
        if self.cleaned_data.get('macaron_echu'):
            benevs = benevs.filter(macaron_depuis__lt=date.today() - timedelta(days=335))
        return benevs


class AdresseFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off'}), required=False)
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off'}), required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )

    def filter(self, adresses):
        if self.cleaned_data['nom']:
            adresses = adresses.filter(nom__unaccent__icontains=self.cleaned_data['nom'])
        if self.cleaned_data['npa_localite']:
            adresses = adresses.filter(
                Q(npa__icontains=self.cleaned_data['npa_localite']) |
                Q(localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            adresses = adresses.annotate(
                telnospace=Replace('tel', Value(' '), Value('')),
            ).filter(telnospace__icontains=self.cleaned_data['tel'].replace(' ', ''))
        return adresses


class AdresseEditForm(BootstrapMixin, NPALocaliteMixin, GeoAddressFormMixin, ModelForm):
    target = forms.CharField(required=False, widget=forms.HiddenInput)
    search = forms.CharField(
        label="Recherche directories",
        widget=forms.TextInput(attrs={'placeholder': 'Nom, rue, code postal, n° tél'}),
        required=False
    )
    rue = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nom de rue'}), required=True)

    class Meta:
        model = Adresse
        fields = ['nom', 'rue', 'npa', 'localite', 'tel']

    field_order = ['search', 'nom', 'npalocalite', 'rue']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if kwargs['instance']:
            del self.fields['search']

    def clean_nom(self):
        nom = self.cleaned_data['nom']
        if self.instance.pk and Adresse.objects.exclude(pk=self.instance.pk).filter(
            nom=nom, date_archive__isnull=True
        ).exists():
            raise forms.ValidationError("Il existe déjà une adresse avec ce nom")
        return nom

    def clean(self):
        data = super().clean()
        if not self.errors and self.instance.pk is None:
            if Adresse.objects.filter(date_archive__isnull=True, nom=data['nom']).exists():
                self.add_error("nom", "Il existe déjà une adresse avec ce nom")
        return data


class CancelForm(BootstrapMixin, ModelForm):
    annule = forms.BooleanField(label="Le transport a été annulé", required=False, initial=True)

    class Meta:
        model = Transport
        fields = ['raison_annulation', 'statut_fact', 'defrayer_chauffeur', 'km']

    def __init__(self, data=None, **kwargs):
        super().__init__(data=data, **kwargs)
        if self.instance.statut != Transport.StatutChoices.ANNULE:
            del self.fields['annule']
        # Enlever choix "Facturer le transport"
        self.fields['statut_fact'].choices = self.fields['statut_fact'].choices[1:]
        for fname in self._meta.fields:
            if fname != 'km':
                self.fields[fname].required = True

    def clean(self):
        data = super().clean()
        if data['defrayer_chauffeur'] and not data.get('km'):
            self.add_error('km', "Vous devez indiquer les kilomètres pour défrayer le chauffeur.")
        if data['statut_fact'] == Transport.FacturationChoices.FACTURER_ANNUL_KM and not data.get('km'):
            self.add_error('km', "Vous devez indiquer les kilomètres pour facturer les km du chauffeur.")
        return data

    def save(self, **kwargs):
        if not self.instance.pk:
            self.instance.concretiser()
        return super().save(**kwargs)


class AttributionForm(BootstrapMixin, ModelForm):
    page_origine = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Transport
        fields = ['chauffeur']
        widgets = {
            'chauffeur': BSRadioSelect,
        }

    def save(self, **kwargs):
        if not self.instance.pk:
            # Occurrence de modèle en mémoire.
            self.instance.concretiser()
            transp = self.instance
        else:
            transp = super().save(**kwargs)
        if transp.chauffeur and transp.statut < Transport.StatutChoices.ATTRIBUE:
            transp.statut = Transport.StatutChoices.ATTRIBUE
            transp.calc_chauffeur_dists()
            transp.save()
        return transp


class ConfirmationForm(forms.Form):
    selection = forms.ModelMultipleChoiceField(queryset=Transport.objects.all())


class FraisTransportForm(BootstrapMixin, ModelForm):
    MAX_FRAIS_REPAS = 20

    class Meta:
        model = Frais
        fields = ['cout', 'justif', 'typ']
        widgets = {
            'typ': forms.HiddenInput,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.instance.typ:
             self.instance.typ = kwargs.get('initial', {}).get('typ', Frais.TypeFrais.PARKING)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('cout') == 0:
            self.add_error(None, "Si vous n’avez pas de frais, laissez plutôt ces champs vides.")
        elif (
            not self.errors and cleaned_data.get('typ') == Frais.TypeFrais.REPAS and
            cleaned_data.get('cout') > self.MAX_FRAIS_REPAS
        ):
            self.add_error('cout', f"Les frais de repas ne sont remboursés que jusqu’à {self.MAX_FRAIS_REPAS} CHF.")
        return cleaned_data


class RapportForm(BootstrapMixin, ModelForm):
    annule = forms.BooleanField(label="Le transport n’a pas eu lieu", required=False)

    class Meta:
        model = Transport
        fields = ['km', 'duree_eff', 'temps_attente', 'rapport_chauffeur', 'statut_fact']
        labels = {
            'duree_eff': "Durée effective",
        }
        field_classes = {
            'duree_eff': HMDurationField,
            'temps_attente': HMDurationField,
        }

    def __init__(self, data=None, files=None, user=None, **kwargs):
        super().__init__(data=data, files=files, **kwargs)
        if not self.instance.retour and not user.has_perm('transport.change_transport'):
            del self.fields['temps_attente']
        if not user.has_perm('transport.change_transport'):
            del self.fields['statut_fact']

        if self.is_bound and int(data.get('frais-INITIAL_FORMS')) == 0 and self.instance.frais.exists():
            # Détection d'une double soumission de formulaire, suppression frais existants
            self.instance.frais.all().delete()

        extra = 2 - self.instance.frais.all().count()
        initial = [{'typ': Frais.TypeFrais.REPAS}, {'typ': Frais.TypeFrais.PARKING}]
        if not self.instance.retour and extra == 2:
            extra = 1
            initial = initial[1:]
        FraisFormSet = forms.inlineformset_factory(
            Transport, Frais, form=FraisTransportForm, formset=HiddenDeleteInlineFormSet,
            extra=extra,
        )
        self.formset = FraisFormSet(
            instance=self.instance, data=data, files=files, initial=initial[:extra],
        )

    def clean(self):
        data = super().clean()
        if data['annule']:
            if not data.get('rapport_chauffeur'):
                self.add_error('rapport_chauffeur', "Vous devez expliquer les circonstances de l’annulation.")
        else:
            if not self.errors and not data.get('duree_eff'):
                self.add_error('duree_eff', "Vous devez saisir la durée effective.")
            if data.get('km') == '':
                self.add_error('km', "Vous devez saisir le nombre de kilomètres.")
        temps_attente = data.get('temps_attente') or timedelta()
        if temps_attente > timedelta(hours=3) and not data['rapport_chauffeur']:
            self.add_error(
                'rapport_chauffeur',
                "Pour un temps d’attente de plus de 3 heures, merci d'indiquer un commentaire."
            )
        if temps_attente and temps_attente >= data.get('duree_eff', timedelta()):
            self.add_error(
                'duree_eff',
                "La durée effective doit être plus élevée que le temps d’attente."
            )
        return data

    def clean_km(self):
        km = self.cleaned_data.get('km')
        if km and km < 0:
            raise forms.ValidationError("La saisie de km négatifs n’est pas autorisée.")
        if km and km > 500:
            raise forms.ValidationError("La saisie de plus de 500km n’est pas autorisée.")
        return km

    def clean_duree_eff(self):
        duree = self.cleaned_data.get('duree_eff')
        if duree and duree > timedelta(hours=20):
            raise forms.ValidationError("La durée effective ne peut pas dépasser 20 heures.")
        return duree

    def clean_temps_attente(self):
        duree = self.cleaned_data.get('temps_attente')
        if duree and duree > timedelta(hours=15):
            raise forms.ValidationError("Le temps d'attente ne peut pas dépasser 15 heures.")
        return duree

    def is_valid(self):
        if self.formset is not None:
            return all([super().is_valid(), self.formset.is_valid()])
        return super().is_valid()

    def save(self, **kwargs):
        if self.cleaned_data['annule'] is True:
            self.instance.statut = Transport.StatutChoices.ANNULE
        else:
            if self.instance.rapport_complet:
                self.instance.statut = Transport.StatutChoices.RAPPORTE
                if self.instance.rapport_conforme:
                    self.instance.statut = Transport.StatutChoices.CONTROLE
        transport = super().save(**kwargs)
        if self.cleaned_data['annule'] is False and self.formset is not None:
            if not self.formset.instance.pk:
                self.formset.instance = transport
            self.formset.save(**kwargs)
        return transport


class FacturerForm(BootstrapMixin, forms.Form):
    date_facture = forms.DateField(label="Date de la facturation", widget=DateInput, required=True)
    choix_refact = forms.ChoiceField(label="Type de refacturation", choices=(
        ('remplacer', "Remplacer la facture existante"),
        ('annuler', "Refaire la facture, mais conserver l’ancienne facture")
    ), required=True)

    def __init__(self, refacturer=False, **kwargs):
        super().__init__(**kwargs)
        if not refacturer:
            del self.fields['choix_refact']


class PermissiveTimeField(forms.TimeField):
    input_formats = ['%H:%M', '%H', '%H.%M']

    def to_python(self, value):
        value = value.strip()
        if len(value) == 4 and value.isdigit():
            value = f"{value[:2]}:{value[2:]}"
        return super().to_python(value)


class DispoEditForm(BootstrapMixin, RepetitionMixin, ModelForm):
    date = forms.DateField(label="Date", widget=DateInput)
    # person = forms.ModelChoiceField(label="Infirmière", queryset=Utilisateur.objects.none(), required=True)
    heure_de = PermissiveTimeField(
        label="De",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    heure_a = PermissiveTimeField(
        label="À",
        widget=forms.TimeInput(attrs={'class': 'TimeField', 'placeholder': '00:00'}, format='%H:%M')
    )
    repetition = forms.ChoiceField(label='Répétition', choices=REPET_CHOICES, required=False)
    repetition_fin = forms.DateField(
        label='Jusqu’au (y compris)', widget=DateInput, required=False
    )
    description = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Indication facultative sur l’absence'}),
        required=False
    )

    class Meta:
        model = Dispo
        fields = [
            'date', 'heure_de', 'heure_a', 'repetition', 'repetition_fin', 'description',
        ]

    def __init__(self, chauffeur=None, categ=None, **kwargs):
        self.chauffeur = chauffeur
        self.categ = categ
        super().__init__(**kwargs)
        if kwargs['instance']:
            self.fields['date'].widget = forms.HiddenInput()
        if categ != 'absence':
            del self.fields['description']

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors:
            start_date = cleaned_data.get('date')
            # Check overlap (only in the future)
            if self.instance.pk:
                next_occ = self.instance.get_next_occurrence()
                start_date = localtime(next_occ.debut).date() if next_occ else None
            if start_date:
                start = make_datetime(start_date, cleaned_data.get('heure_de'))
                end = make_datetime(start_date, cleaned_data.get('heure_a'))
                overlap = Dispo.get_for_person(
                    self.chauffeur, start + timedelta(seconds=1), end,
                    exclude=[self.instance] if self.instance.pk else [],
                    categ=self.categ,
                )
                if any(occ.cancelled is False for occ in overlap):
                    self.add_error(None, forms.ValidationError(
                        "Cette disponibilité se chevauche avec une autre déjà existante."
                    ))
        return cleaned_data

    def save(self, **kwargs):
        if not self.instance.chauffeur_id:
            self.instance.chauffeur = self.chauffeur
        if set(self.changed_data).intersection({'date', 'heure_de', 'heure_a'}):
            self.instance.debut = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_de'])
            self.instance.fin = make_datetime(self.cleaned_data['date'], self.cleaned_data['heure_a'])
            if self.instance.pk:
                for occ in self.instance.exceptions.all():
                    occ.orig_start = make_datetime(occ.orig_start, self.cleaned_data['heure_de'])
                    occ.save()
        self.set_repetition(self.instance)
        return super().save(**kwargs)


class DateRangeSelectionForm(forms.Form):
    date_de = forms.DateField(label="Du", widget=DateInput, required=True)
    date_a = forms.DateField(label="Au", widget=DateInput, required=True)


def make_datetime(dt, hour, timezone=None):
    return make_aware(datetime(dt.year, dt.month, dt.day, hour.hour, hour.minute), timezone=timezone)
