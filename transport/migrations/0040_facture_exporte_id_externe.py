from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0039_dispo_categorie'),
    ]

    operations = [
        migrations.AddField(
            model_name='facture',
            name='exporte',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Exporté vers compta'),
        ),
        migrations.AddField(
            model_name='facture',
            name='id_externe',
            field=models.BigIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='facture',
            name='export_err',
            field=models.TextField(blank=True, verbose_name='Erreur d’exportation'),
        ),
    ]
