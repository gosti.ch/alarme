from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0004_initial_courrier_envoye'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='remarques_int_alarme',
            field=models.TextField(blank=True, verbose_name='Remarques internes (alarme)'),
        ),
        migrations.AddField(
            model_name='client',
            name='remarques_int_transport',
            field=models.TextField(blank=True, verbose_name='Remarques internes (transports)'),
        ),
        migrations.AddField(
            model_name='client',
            name='remarques_int_visite',
            field=models.TextField(blank=True, verbose_name='Remarques internes (visites)'),
        ),
        migrations.AlterField(
            model_name='client',
            name='remarques_ext',
            field=models.TextField(blank=True, verbose_name='Remarques (visibles par bénévoles)'),
        ),
    ]
