from django.contrib import admin

from .models import Questionnaire


@admin.register(Questionnaire)
class QuestionnaireAdmin(admin.ModelAdmin):
    list_display = ['client', 'created', 'termine_le', 'collab']
    search_fields = ['client__nom']
