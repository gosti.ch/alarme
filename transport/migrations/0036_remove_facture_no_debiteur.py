from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0035_migrate_debiteur"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="facture",
            name="no_debiteur",
        ),
    ]
