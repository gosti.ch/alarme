from os.path import dirname


def load_tests(loader, tests, pattern):
    from django.apps import apps

    if not apps.is_installed("cr_ne"):
        return None
    if apps.is_installed("alarme"):
        return loader.discover(start_dir=dirname(__file__), pattern='test_alarme*.py')
    elif apps.is_installed("transport"):
        return loader.discover(start_dir=dirname(__file__), pattern='test_transport*.py')

