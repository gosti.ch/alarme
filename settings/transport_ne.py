from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from .base import *

WSGI_APPLICATION = 'common.wsgi_transport.application'

DATABASES['default']['NAME'] = 'alarmes_ne'
DATABASES['default']['TEST'] = {'NAME': 'test_transport_ne'}

ROOT_URLCONF = 'transport.urls'

STATIC_ROOT = BASE_DIR / 'static' / 'transport'

CANTON_APP = 'cr_ne'
INSTALLED_APPS.extend([
    'transport',
    CANTON_APP,
    'api',
    'besoins',
])
DEFAULT_FROM_EMAIL = 'transports@croix-rouge-ne.ch'

CID_SYNC_ENABLE = []
CID_API_URL = ""
CID_API_TOKEN = ""

FACTURES_TRANSMISSION_URL = reverse_lazy('factures-transmission', args=['transport'])

CODES_ARTICLE_INSTALLATION = ['40.110']
MAIN_LOGO = 'img/logo-cr-ne.svg'
APP_LOGO = 'logos/transport.svg'

TARIF_AVS = Decimal('0.90')
TARIF_NON_AVS = Decimal('1.10')
TAUX_TVA = Decimal('0.077')
FACTURE_IBAN = "CH41 3000 0001 2000 1504 8"
FORFAIT_ALLER = Decimal('4.50')
FORFAIT_ALLER_RETOUR = Decimal('9.00')
FORFAIT_ANNULATION = {
    date(2000, 1, 1): Decimal('10.00'),
    date(2024, 1, 1): Decimal('25.00'),
}
SEARCH_CH_API_KEY = ''
