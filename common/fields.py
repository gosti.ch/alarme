from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.db import models
from django.forms import CharField, TypedMultipleChoiceField
from django.utils.deconstruct import deconstructible

import phonenumbers


class PhoneCharField(CharField):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.validators.append(PhoneNumberValidator())

    def clean(self, value):
        """Format number as 032 xxx xx xx (for CH), or +33 xx xx... (for International)."""
        value = super().clean(value)
        if value:
            value = phonenumbers.parse(value, "CH")
            fmt = (
                phonenumbers.PhoneNumberFormat.NATIONAL if value.country_code == 41
                else phonenumbers.PhoneNumberFormat.INTERNATIONAL
            )
            value = phonenumbers.format_number(value, fmt)
        return value


class ChoiceArrayField(ArrayField):
    """
    From https://blogs.gnome.org/danni/2016/03/08/multiple-choice-using-djangos-postgres-arrayfield/
    A field that allows us to store an array of choices.

    Uses Django's postgres ArrayField and a MultipleChoiceField for its formfield.
    See also https://code.djangoproject.com/ticket/27704
    """
    @property
    def widget(self):
        from common.forms import BSCheckboxSelectMultiple
        return BSCheckboxSelectMultiple

    def formfield(self, **kwargs):
        defaults = {
            'form_class': TypedMultipleChoiceField,
            'coerce': self.base_field.to_python,
            'choices': self.base_field.choices,
            'widget': self.widget,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't
        # care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)


@deconstructible
class PhoneNumberValidator:
    def __call__(self, value):
        try:
            numb = phonenumbers.parse(value, "CH")
        except phonenumbers.phonenumberutil.NumberParseException as exc:
            raise ValidationError(str(exc))
        if not phonenumbers.is_valid_number(numb):
            raise ValidationError("Le numéro de téléphone n’est pas valide")


class PhoneNumberField(models.CharField):
    description = "Numéro de téléphone"
    default_validators = [PhoneNumberValidator()]

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 18)
        super().__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        return super().formfield(**{
            "form_class": PhoneCharField,
            **kwargs,
        })
