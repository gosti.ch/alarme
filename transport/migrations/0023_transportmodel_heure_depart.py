from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0022_transport_chauff_aller_retour_km"),
    ]

    operations = [
        migrations.AddField(
            model_name="transportmodel",
            name="heure_depart",
            field=models.TimeField(null=True, verbose_name="Heure de départ"),
        ),
    ]
