import json
import logging

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import mail_admins
from django.http import Http404, JsonResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from client.models import Client, Referent
from client.views import ClientJournalMixin
from benevole.models import Benevole, Journal
from .forms import BenevoleForm, ClientForm, ReferentForm
from .signals import disconnect_sync_signals

logger = logging.getLogger('api')


class APIAccessCheckMixin:
    """Mixin checking the origin IP and the Bearer auth token to access the API."""
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        remote_ip = request.META['REMOTE_ADDR']
        auth_header = request.headers.get('Authorization')
        token = auth_header.split()[-1] if auth_header else None
        if remote_ip not in settings.API_ALLOWED_IPS:
            logger.error(f"IP {remote_ip} not in allowed IPs")
            raise PermissionDenied("Invalid request origin or token")
        if token not in settings.API_ALLOWED_TOKENS:
            logger.error(f"Token {token} not in allowed tokens")
            raise PermissionDenied("Invalid request origin or token")
        return super().dispatch(request, *args, **kwargs)


class ClientUpdateView(APIAccessCheckMixin, ClientJournalMixin, View):
    def map_client_fields(self, data):
        tels = [tel for tel in [data["phone"], data["mobile"]] if tel]
        return {
            'nom': data["name"],
            'prenom': data["firstname"],
            'genre': 'M' if data["title"] == "Sir" else 'F',
            'rue': data["street"],
            'npa': data["zip"],
            'localite': data["city"],
            'tel_1': tels[0] if tels else '',
            'tel_2': tels[1] if len(tels) > 1 else '',
            'courriel': data["email"],
            'date_naissance': None if data["birthdate"] is False else data["birthdate"],
            'date_deces': None if data["deathdate"] is False else data["deathdate"]
        }

    def map_referent_fields(self, data):
        return {
            'nom': data["name"],
            'prenom': data["firstname"],
            'rue': data["street"],
            'npa': data["zip"],
            'localite': data["city"],
            'courriel': data["email"],
        }

    def get_client(self):
        return self.client

    def post(self, request, *args, **kwargs):
        # Création client
        self.is_create = True
        data = json.loads(request.body)
        # TODO: attribution du client au bon secteur.
        try:
            form = ClientForm(instance=None, data=self.map_client_fields(data))
            is_valid = form.is_valid()
        except Exception as err:
            err_msg = f"Unkwown error {err}. Send data: {data}"
            mail_admins(f"CID Error on {request.path}", err_msg)
            return JsonResponse({'error': {'code': 400, 'message': err_msg}})
        if is_valid:
            form.instance.id_externe = data["id"]
            with disconnect_sync_signals():
                self.client = form.save()
            self.journalize(form, add_message="Création du client par CID")
        elif form.doublons:
            return JsonResponse({'error': {
                'code': 409, 'message': "Ce client semble être un doublon.", 'doublon_id': form.doublons[0].pk,
            }})
        else:
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})
        return JsonResponse({'id': self.client.pk})

    def put(self, request, *args, **kwargs):
        """Mise à jour d'un client ou référent depuis CID."""
        self.is_create = False
        data = json.loads(request.body)
        referents = []
        try:
            self.client = Client.objects.get(id_externe=data["id"])
        except Client.DoesNotExist:
            self.client = None
            referents = Referent.objects.filter(no_debiteur=data["id"])
        if not self.client and not referents:
            raise Http404("Aucun partenaire avec cet identifiant")
        with disconnect_sync_signals():
            if self.client:
                return self.update_client(data)
            else:
                for referent in referents:
                    self.client = referent.client  # Used in journalization
                    errors = self.update_referent(referent, data)
                    if errors:
                        return JsonResponse({'error': {
                            'code': 400, 'message': "Sorry, the data are not valid.",
                            'errors': errors.get_json_data(),
                        }})
                return JsonResponse({'id': referents[0].pk})

    def update_client(self, data):
        self.journal_edit_message = "Modification du client par CID: {fields}"
        form = ClientForm(instance=self.client, data=self.map_client_fields(data))
        if form.is_valid():
            form.save()
            self.journalize(form)
            form.notify_other_teams()
            return JsonResponse({'id': self.client.pk})
        else:
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})

    def update_referent(self, referent, data):
        self.journal_edit_message = "Modification du contact «{obj}» par CID: {fields}"
        form = ReferentForm(instance=referent, data=self.map_referent_fields(data))
        # On renonce à synchroniser en entrée les no de tél. des référents, car
        # ils sont plus précis côté alarme.
        if form.is_valid():
            form.save()
            self.journalize(form)
        else:
            return form.errors


class BenevoleUpdateView(APIAccessCheckMixin, View):
    def map_benev_fields(self, data):
        return {
            'nom': data["name"],
            'prenom': data["firstname"],
            'rue': data["street"],
            'npa': data["zip"],
            'localite': data["city"],
            'tel_mobile': data.get("mobile", ""),
            'tel_prive': data.get("phone", ""),
            'courriel': data.get("email", ""),
            'date_naissance': data.get("birthdate"),
            # tag?
        }

    def journalize(self, form, msg=None):
        if not msg:
            changes = form.get_changed_string()
            if changes:
                msg = f"Modification du bénévole depuis CID: {changes}"
        if msg:
            Journal.objects.create(
                benevole=form.instance,
                description=msg, quand=timezone.now(),
                qui=self.request.user if not self.request.user.is_anonymous else None
            )

    def post(self, request, *args, **kwargs):
        # Création bénévole
        self.is_create = True
        data = json.loads(request.body)
        form = BenevoleForm(instance=None, data=self.map_benev_fields(data))
        if form.is_valid():
            form.instance.activites = []
            form.instance.id_externe = data["id"]
            with disconnect_sync_signals():
                self.benevole = form.save()
            self.journalize(form, msg="Création du client par CID")
        elif form.doublons:
            logger.error(f"Benevole looks like duplicate of id {form.doublons[0].pk}")
            return JsonResponse({'error': {
                'code': 409, 'message': "Ce bénévole semble être un doublon.", 'doublon_id': form.doublons[0].pk,
            }})
        else:
            logger.error(f"Benevole form errors: {form.errors.as_text()}")
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})
        return JsonResponse({'id': self.benevole.pk})

    def put(self, request, *args, **kwargs):
        # Mise à jour bénévole
        data = json.loads(request.body)
        try:
            self.benev = Benevole.objects.get(id_externe=data["id"])
        except Benevole.DoesNotExist:
            # Création du bénévole si non existant dans la base
            logger.warning(f"No benevole with external id {data['id']}, creating it")
            return self.post(request, *args, **kwargs)
        self.is_create = False
        form = BenevoleForm(instance=self.benev, data=self.map_benev_fields(data))
        if form.is_valid():
            with disconnect_sync_signals():
                form.save()
            self.journalize(form)
            return JsonResponse({'id': self.benev.pk})
        else:
            logger.error(f"Benevole form errors: {form.errors.as_text()}")
            return JsonResponse({'error': {
                'code': 400, 'message': "Sorry, the data are not valid.", 'errors': form.errors.get_json_data(),
            }})
