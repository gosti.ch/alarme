from datetime import timedelta

from django.utils.dateformat import format as django_format

from alarme.models import Facture, FacturationPolicyBase
from common.utils import same_month


class FacturationPolicy(FacturationPolicyBase):
    def creer_facture_mensuelle(self, mois, installs, date_factures):
        factures = super().creer_facture_mensuelle(mois, installs, date_factures)
        # Facturation minimale de 3 mois (#401)
        install1 = installs[0]
        if (
            len(installs) == 1 and install1.date_fin_abo and
            same_month(install1.date_fin_abo, mois)
        ):
            num_factures = install1.client.factures.filter(article=install1.abonnement.article).count()
            if num_factures < 3:
                for i in range(3 - num_factures):
                    mois_futur = (mois.replace(day=1) + timedelta(days=31 * (i + 1))).replace(day=1)
                    nom_mois = django_format(mois_futur, "F Y")
                    factures.append(
                        Facture.objects.create(
                            client=install1.client,
                            mois_facture=mois_futur,
                            date_facture=date_factures,
                            article=install1.abonnement.article,
                            install=install1,
                            materiel=None,
                            libelle=f'Service d’alarme à domicile - {nom_mois}',
                            montant=install1.abonnement.article.prix,
                        )
                    )
        return factures
