from django.db import migrations, models


def update_created_date(apps, schema_editor):
    Referent = apps.get_model('client', 'Referent')
    for ref in Referent.objects.all():
        ref.cree_le = ref.client.cree_le
        ref.save(update_fields=['cree_le'])


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0007_remove_client_remarques_int'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='cree_le',
            field=models.DateTimeField(auto_now_add=True, default='2020-1-1', verbose_name='Créé le'),
            preserve_default=False,
        ),
        migrations.RunPython(update_created_date),
    ]
