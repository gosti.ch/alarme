from os.path import dirname


def load_tests(loader, tests, pattern):
    from django.apps import apps

    if apps.is_installed("alarme") and not apps.is_installed("transport"):
        return loader.discover(start_dir=dirname(__file__), pattern=pattern)

