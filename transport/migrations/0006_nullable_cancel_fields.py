from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0005_new_statutchoice"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transport",
            name="defrayer_chauffeur",
            field=models.BooleanField(
                null=True, verbose_name="Défrayer les km du chauffeur"
            ),
        ),
        migrations.AlterField(
            model_name="transport",
            name="facturer_frais_annulation",
            field=models.BooleanField(
                null=True, verbose_name="Facturer frais d’annulation"
            ),
        ),
        migrations.AlterField(
            model_name="transport",
            name="statut",
            field=models.PositiveSmallIntegerField(
                choices=[
                    (1, "Saisi"),
                    (2, "Attribué"),
                    (3, "Confirmé"),
                    (4, "Effectué"),
                    (5, "Rapporté"),
                    (6, "Contrôlé"),
                    (10, "Annulé"),
                ],
                default=1,
            ),
        ),
    ]
