import argparse
from datetime import date

from openpyxl import load_workbook

from django.core.management.base import BaseCommand
from django.db import transaction

from client.models import Client


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('fichier', type=argparse.FileType('rb'))

    def handle(self, **options):
        wb = load_workbook(options['fichier'])
        ws = wb.active
        with transaction.atomic():
            for idx, row in enumerate(ws, start=1):
                if idx == 1:
                    headers = [cell.value for cell in row]
                    continue
                values = dict(zip(headers, [cell.value for cell in row]))
                self.import_clients(values)
            #raise Exception("Importation en phase de test")

    def import_clients(self, values):
        try:
            client = Client.objects.get(nom=values['Nom'], prenom=values['Prénom'], npa=values['Code Postal'])
        except Client.DoesNotExist:
            pass
        else:
            self.stdout.write(f"Le client {client} existe déjà")
            data_local = f'{client.rue}/{client.npa}/{client.localite}'
            data_import = f"{values['Rue']} {values['Numéro']}/{values['Code Postal']}/{values['Lieu']}"
            if data_local != data_import:
                self.stdout.write(f"Différence entre «{data_local}» et «{data_import}»")
            # Compléter avec nouvelles valeurs
            if 'transport' not in client.type_client:
                client.type_client.append('transport')
            client.no_debiteur = clean(values['Navision ID'])
            client.save()
            return

        if not values['Code Postal'] or not values['Lieu']:
            self.stdout.write(
                f"Code postal et localité sont obligatoires, importation de {values['Nom']} {values['Prénom']} annulée"
            )
            return

        client = Client(
            nom=values['Nom'], prenom=values['Prénom'], langues=['fr'], type_client=['transport'],
            genre=values['Sexe'], date_naissance=read_date(values['Date de naissance']),
            rue=" ".join([val for val in [values['Rue'], str(values['Numéro'])] if val]),
            npa=values['Code Postal'], localite=values['Lieu'],
            tel_1=clean(values['privé Téléphone']), courriel=clean(values['privé Courriel']),
            no_debiteur=clean(values['Navision ID']),
        )
        client.full_clean()
        client.save()


def clean(val):
    if val is None:
        return ''
    return val.strip() if isinstance(val, str) else val


def read_date(dt):
    if not dt or dt in {'00.00.0000', '- - -'}:
        return None
    return date(*reversed([int(part) for part in dt.split('.')]))
