from django.contrib import admin

from .models import Activite, Benevole, Journal, LigneFrais, NoteFrais, TypeFrais


class ActiviteInline(admin.TabularInline):
    model = Activite
    extra = 0


@admin.register(Benevole)
class BenevoleAdmin(admin.ModelAdmin):
    ordering = ['nom']
    list_display = ['nom_prenom', 'rue', 'npa', 'localite', 'archive_le']
    list_filter = ['archive_le']
    search_fields = ['nom']
    inlines = [ActiviteInline]


@admin.register(Journal)
class JournalAdmin(admin.ModelAdmin):
    list_display = ['quand', 'benevole', 'description']
    list_select_related = ['benevole']
    date_hierarchy = 'quand'
    list_filter = ['qui']
    search_fields = ['benevole__nom', 'description']


class LigneFraisInline(admin.TabularInline):
    model = LigneFrais
    extra = 0


@admin.register(NoteFrais)
class NoteFraisAdmin(admin.ModelAdmin):
    list_display = ['benevole', 'service', 'mois', 'kms', 'date_export']
    date_hierarchy = 'mois'
    search_fields = ['benevole__nom']
    inlines = [LigneFraisInline]


@admin.register(TypeFrais)
class TypeFraisAdmin(admin.ModelAdmin):
    list_display = ['no', 'libelle', 'services']
    ordering = ['no']


@admin.register(LigneFrais)
class LigneFraisAdmin(admin.ModelAdmin):
    list_display = ['note', 'libelle', 'montant']
