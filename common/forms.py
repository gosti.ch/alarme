from datetime import date, datetime, timedelta

from django import forms
from django.contrib.auth.models import Group
from django.core.exceptions import FieldDoesNotExist
from django.db.models import Q
from django.forms.utils import ErrorList as DjangoErrorList
from django.urls import reverse_lazy
from django.utils.dates import MONTHS
from django.utils.functional import cached_property
from django.utils.timezone import localtime

from common.models import Fichier, Utilisateur
from common.utils import format_HM
from common.templatetags.common_utils import format_duree


class DateInput(forms.DateInput):
    input_type = 'date'

    def format_value(self, value):
        return str(value) if value is not None else None


class TimeInput(forms.TimeInput):
    input_type = 'time'

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('format', '%H:%M')
        super().__init__(*args, **kwargs)

    def format_value(self, value):
        if isinstance(value, datetime):
            value = localtime(value)
        return super().format_value(value)


class SplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, attrs=None):
        widgets = [DateInput(attrs={'class': 'inline'}), TimeInput(attrs={'class': 'inline'})]
        forms.MultiWidget.__init__(self, widgets, attrs)


class DurationInput(TimeInput):
    input_type = 'text'

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('attrs', {'placeholder': 'HH:MM'})
        super().__init__(*args, **kwargs)


class PriceInput(forms.TextInput):
    def __init__(self, attrs=None):
        super().__init__(attrs=attrs)
        self.attrs['pattern'] = r"[0-9]+[\.,]?([0-9][0,5])?"


class HMDurationField(forms.DurationField):
    """A duration field taking HH:MM as input."""
    widget = DurationInput

    def to_python(self, value):
        if value in self.empty_values or isinstance(value, timedelta):
            return super().to_python(value)
        elif len(value) == 4 and value.isdigit():
            value = f'{value[:2]}:{value[2:4]}'
        elif 'h' in value:
            value = value.replace('h', ':')
        elif '.' in value:
            value = value.replace('.', ':')
        if ':' not in value:
            raise forms.ValidationError("Indiquez une durée de type hh:mm")
        if value.endswith(':'):
            value += '0'
        value += ':00'  # Simulate seconds
        return super().to_python(value)

    def prepare_value(self, value):
        if isinstance(value, timedelta):
            value = format_HM(value)
        return value


class NPALocaliteField(forms.CharField):
    widget = forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'placeholder': 'N° postal ou nom de localité',
        'data-searchurl': reverse_lazy('npalocalite-search'),
    })

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if not self.label:
            self.label = "NPA Localité"

    def clean(self, value):
        return str(value)


class NPALocaliteMixin(forms.Form):
    """
    Applied to forms having a NPALocaliteField, to split npa/localite in
    two.
    """
    npalocalite = NPALocaliteField(required=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if 'npa' in self.fields:
            self.fields['npa'].widget = forms.HiddenInput()
            self.fields['npa'].required = False
        if 'localite' in self.fields:
            self.fields['localite'].widget = forms.HiddenInput()
            self.fields['localite'].required = False

    def get_initial_for_field(self, field, field_name):
        if field_name == 'npalocalite':
            value = ' '.join([v for v in [self.initial.get('npa', ''), self.initial.get('localite', '')] if v])
            field.choices = ((value, value),)
            return value
        return super().get_initial_for_field(field, field_name)

    @cached_property
    def changed_data(self):
        changed = [name for name, bf in self._bound_items() if bf._has_changed()]
        if 'npalocalite' in changed:
            changed.remove('npalocalite')
            if self.initial.get('npa', '') != self.cleaned_data['npa']:
                changed.append('npa')
            if self.initial.get('localite', '') != self.cleaned_data['localite']:
                changed.append('localite')
        return changed

    def _clean_form(self):
        if self.cleaned_data['npalocalite']:
            if self.cleaned_data.get('pays') == 'GB':
                self.cleaned_data['npa'] = ''
                self.cleaned_data['localite'] = self.cleaned_data['npalocalite']
            else:
                try:
                    npa, loc = self.cleaned_data['npalocalite'].split(' ', maxsplit=1)
                except ValueError:
                    self.add_error('npalocalite', "Vous devez saisir numéro postal et localité")
                else:
                    if not npa.isdigit():
                        self.add_error('npalocalite', "Le numéro postal doit être uniquement composé de chiffres.")
                    self.cleaned_data['npa'] = npa
                    self.cleaned_data['localite'] = loc
        super()._clean_form()


class ErrorList(DjangoErrorList):
    def __init__(self, *args, error_class=None, **kwargs):
        super().__init__(*args, error_class="formfield-error", **kwargs)


def form_field(field, **kwargs):
    if field.__class__.__name__ == 'DateField' and 'widget' not in kwargs:
        kwargs['widget'] = DateInput
    return field.formfield(**kwargs)


class HiddenDeleteInlineFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput


class FormsetMixin:
    def is_valid(self):
        if self.formset is not None:
            return all([super().is_valid(), self.formset.is_valid()])
        return super().is_valid()

    def save(self, **kwargs):
        parent = super().save(**kwargs)
        if self.formset is not None:
            if not self.formset.instance.pk:
                self.formset.instance = parent
            self.formset.save(**kwargs)
        return parent


class ReadOnlyableMixin:
    def __init__(self, *args, readonly=False, **kwargs):
        self.readonly = readonly
        super().__init__(*args, **kwargs)
        if self.readonly:
            for field in self.fields.values():
                field.disabled = True


class BootstrapMixin:
    widget_classes = {
        'checkbox': 'form-check-input',
        'select': 'form-select',
    }
    required_css_class = 'required'

    class Meta:
        formfield_callback = form_field

    def __init__(self, *args, **kwargs):
        kwargs.update({'error_class': ErrorList})
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if getattr(field.widget, '_bs_enabled', False):
                continue
            widgets = getattr(field.widget, 'widgets', [field.widget])
            for widget in widgets:
                input_type = getattr(widget, 'input_type', '')
                class_name = self.widget_classes.get(input_type, 'form-control')
                if 'class' in widget.attrs:
                    widget.attrs['class'] += ' ' + class_name
                else:
                    widget.attrs.update({'class': class_name})


class BSChoicesMixin:
    """
    Custom widget to set 'form-check' on container and 'form-check-input' on sub-options.
    """
    _bs_enabled = True

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        context['widget']['attrs']['class'] = 'form-check'
        return context

    def create_option(self, *args, attrs=None, **kwargs):
        attrs = attrs.copy() if attrs else {}
        if 'class' in attrs:
            attrs['class'] += ' form-check-input'
        else:
            attrs.update({'class': 'form-check-input'})
        return super().create_option(*args, attrs=attrs, **kwargs)


class BSCheckboxSelectMultiple(BSChoicesMixin, forms.CheckboxSelectMultiple):
    pass


class BSRadioSelect(BSChoicesMixin, forms.RadioSelect):
    pass


class AnneeForm(BootstrapMixin, forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2022, date.today().year + (1 if date.today().month < 12 else 2))
    )
    year = forms.ChoiceField(choices=YEAR_CHOICES)


class MoisForm(BootstrapMixin, forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2022, date.today().year + (1 if date.today().month < 12 else 2))
    )
    year = forms.ChoiceField(choices=YEAR_CHOICES)
    month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])


class ModelForm(forms.ModelForm):
    """
    Extension de forms.ModelForm avec la capacité de produire un résumé des
    valeurs modifiées lors de l'enregistrement du formulaire.
    """
    @staticmethod
    def to_readable(val, choices=None):
        if val is True:
            return 'vrai'
        elif val is False:
            return 'faux'
        elif val is None:
            return ''
        elif isinstance(val, timedelta):
            return format_duree(val)
        elif choices and val:
            return ", ".join([choices[ch] for ch in val])
        return val

    def _changed_value(self, fname, detail=False):
        if not self.fields[fname].label:
            return None
        if detail:
            try:
                model_field = self.instance._meta.get_field(fname)
            except FieldDoesNotExist:
                model_field = None
            try:
                choices = dict(model_field.base_field.choices)
            except (AttributeError, KeyError):
                choices = None
            initial_value = self.initial.get(fname, '')
            if (
                model_field and model_field.get_internal_type() == 'ForeignKey' and
                isinstance(initial_value, int)
            ):
                prev_value = str(model_field.related_model.objects.get(pk=initial_value))
            else:
                prev_value = self.to_readable(initial_value, choices)
            new_value = self.to_readable(self.cleaned_data[fname], choices)
            if new_value == prev_value:
                return None
            if prev_value:
                return "{} (de «{}» à «{}»)".format(
                    self.fields[fname].label.lower(),
                    prev_value, new_value
                )
            else:
                return "ajout de {} («{}»)".format(
                    self.fields[fname].label.lower(), new_value
                )
        else:
            return self.fields[fname].label.lower()

    def _get_changes(self):
        return [val for val in [
            self._changed_value(fname, detail=True) for fname in self.changed_data
        ] if val is not None]

    def _change_prefix(self):
        """Dans un contexte Formset, préfixe identifiant l'instance liée."""
        return self._meta.model._meta.verbose_name.lower()

    def get_changed_string(self, changed_values=True):
        has_changed_data = self.changed_data or (
            getattr(self, 'formset', None) and any(f.changed_data for f in self.formset)
        )
        if not has_changed_data:
            return ''
        if changed_values:
            # Détail des valeurs modifiées (ancienne et nouvelle valeur)
            changes = self._get_changes()
            if getattr(self, 'formset', None):
                for subform in self.formset.forms:
                    if subform in self.formset.deleted_forms:
                        changes.append(f'suppression de {subform._change_prefix()}')
                    elif subform.has_changed():
                        if not subform.instance.pk:
                            changes.append(f'ajout de {subform._change_prefix()}')
                        else:
                            changes.append(f'{subform._change_prefix()}: {", ".join(subform._get_changes())}')
        else:
            # Uniquement nom des champs modifiés
            changes = [self._changed_value(fname) for fname in self.changed_data]
            if getattr(self, 'formset', None):
                for subform in self.formset.forms:
                    if subform in self.formset.deleted_forms:
                        changes.append(f'suppression de {subform._change_prefix()}')
                    elif subform.has_changed():
                        if not subform.instance.pk:
                            changes.append(f'ajout de {subform._change_prefix()}')
                        else:
                            changes.append([
                                subform._changed_value(fname) for fname in subform.changed_data
                            ])
        return ", ".join(c for c in changes if c is not None)


class UtilisateurFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}),
        required=False
    )
    groupe = forms.ModelChoiceField(queryset=Group.objects.all().order_by('name'), required=False)

    def filter(self, utils):
        if self.cleaned_data['nom']:
            term = self.cleaned_data['nom'].split()[0]
            utils = utils.filter(
                Q(last_name__unaccent__icontains=term) | Q(first_name__unaccent__icontains=term)
            )
        if self.cleaned_data['groupe']:
            utils = utils.filter(groups=self.cleaned_data['groupe'])
        return utils


class UtilisateurForm(BootstrapMixin, forms.ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Utilisateur
        fields = ['last_name', 'first_name', 'email', 'fonction', 'groups', 'is_active']
        widgets = {
            'groups': BSCheckboxSelectMultiple,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].queryset = self.fields['groups'].queryset.order_by('name')


class FichierForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Fichier
        fields = ['titre', 'typ', 'fichier']

    def __init__(self, app=None, **kwargs):
        super().__init__(**kwargs)
        # Seule l'alarme a tous les choix de type de doc.
        if app != 'alarme':
            self.fields['typ'].choices = self.fields['typ'].choices[:1]
