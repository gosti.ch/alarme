"""Vues pour application bénévoles sous /app"""

from datetime import date, datetime, timedelta
from itertools import groupby
from operator import attrgetter

from django.core.exceptions import PermissionDenied
from django.db.models import Min, Q, Sum
from django.db.models.functions import TruncMonth
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import ListView, TemplateView

from common.views import BenevoleMixin
from transport.models import Dispo, JournalTransport, Transport
from .views import ChauffeurAgendaView, ChauffeurDispoEditView, TransportRapportView


def a_rapporter(chauffeur):
    return chauffeur.transports.annotate(depart=Min('trajets__heure_depart')).filter(
        Q(statut=Transport.StatutChoices.EFFECTUE) |
        (Q(statut=Transport.StatutChoices.CONFIRME) & Q(depart__lte=timezone.now()))
    )


class HomeBenevoleView(BenevoleMixin, TemplateView):
    template_name = 'transport/app/home_benevole.html'
    tab = 'transports'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        Transport.check_effectues(self.benev)
        futurs = self.benev.transports.filter(
            statut=Transport.StatutChoices.CONFIRME, date__gte=date.today()
        ).order_by('date', 'heure_rdv')

        # Marquers tous les futurs transports comme vus
        for non_vu in futurs.filter(chauffeur_vu=False):
            non_vu.chauffeur_vu = True
            non_vu.save(update_fields=['chauffeur_vu'])
            JournalTransport.objects.create(
                transport=non_vu, description=f"Vu par le chauffeur ({non_vu.chauffeur})",
                quand=timezone.now(), qui=self.request.user
            )

        context.update({
            'actif': self.tab,
            'futurs': futurs,
            'arapporter': a_rapporter(self.benev).order_by('date'),
        })
        return context


class TransportRapportListView(BenevoleMixin, ListView):
    """Liste des transports à rapporter par l'utilisateur connecté (bénévole)"""
    template_name = 'transport/app/transports_a_rapporter.html'

    def get_queryset(self):
        return a_rapporter(self.benev).order_by('date')


class BenevoleRapportView(BenevoleMixin, TransportRapportView):
    next_page = reverse_lazy('home-app')
    base_template = 'transport/app/base.html'
    par_chauffeur = True

    def get_object(self):
        transport = super().get_object()
        if transport.chauffeur != self.benev:
            raise PermissionDenied("Vous n'avez pas la permission de modifier ce rapport.")
        return transport


class BenevoleDisposView(BenevoleMixin, TemplateView):
    """Liste des jours avec disponibilités pour app bénévole."""
    template_name = 'transport/app/chauffeur-dispo-jours.html'
    nb_jours = 15

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start = self.request.GET.get('start')
        if start:
            start = datetime.strptime(start, '%Y-%m-%d').replace(tzinfo=timezone.get_current_timezone())
        else:
            start = timezone.now().replace(hour=0, minute=0)
        end = (start + timedelta(days=self.nb_jours)).replace(hour=23, minute=59)
        dispos = Dispo.get_for_person(self.benev, start, end)
        transports = list(
            Transport.get_for_person(self.benev, start, end).filter(statut__in=[
                Transport.StatutChoices.CONFIRME, Transport.StatutChoices.EFFECTUE
            ])
        )
        context['days'] = {start.date() + timedelta(days=d): [] for d in range(0, self.nb_jours + 1)}
        for dispo in dispos:
            dispo_jour = timezone.localtime(dispo.debut).date()
            if dispo_jour not in context['days']:
                # It may append at the limit of timezones
                continue
            if dispo.cancelled:
                dispo.nb_transports = 0
                continue
            dispo.nb_transports = len([tr.pk for tr in transports if (dispo.fin >= tr.heure_rdv >= dispo.debut)])
            context['days'][dispo_jour].append(dispo)
        context.update({
            'benev': self.benev,
            'next_day': start.date() + timedelta(days=self.nb_jours + 1),
        })
        return context


class ChauffeurAgendaDayView(BenevoleMixin, ChauffeurAgendaView):
    """Affichage agenda chauffeur pour un seul jour (app bénévole)."""
    NUM_DAYS = 1

    def get_chauffeur(self):
        return self.benev

    def get_start_end_dates(self):
        self.day = timezone.make_aware(
            datetime(self.kwargs['year'], self.kwargs['month'], self.kwargs['day'], 0, 0)
        )
        return self.day, self.day.replace(hour=23, minute=59)

    def prev_next_urls(self, benev, start, end):
        hier = start - timedelta(days=1)
        demain = start + timedelta(days=1)
        return (
            reverse('benevole-agenda-day', args=[hier.year, hier.month, hier.day]),
            reverse('benevole-agenda-day', args=[demain.year, demain.month, demain.day]),
        )

    def filter_transports(self, queryset):
        return queryset.filter(statut__in=[
            Transport.StatutChoices.CONFIRME, Transport.StatutChoices.EFFECTUE
        ])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agendas'][0].update({
            'title': '',
            'add_dispo_url': (
                reverse('benevole-self-dispo-add') + f"?date={self.day.strftime('%Y-%m-%d')}"
            ),
            'add_absence_url': (
                reverse('benevole-self-absence-add') + f"?date={self.day.strftime('%Y-%m-%d')}"
            ),
        })
        context['base'] = 'transport/app/base.html'
        context['show_tabs'] = False
        return context


class DispoEditView(BenevoleMixin, ChauffeurDispoEditView):
    def get_chauffeur(self):
        return self.benev


class BenevoleArchivesView(BenevoleMixin, TemplateView):
    """Liste des mois d'archives"""
    template_name = 'benevoles/archives-mois.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs, base_template='transport/app/base.html')
        context['months'] = self.benev.transports.annotate(
            month=TruncMonth('date')
        ).values_list('month', flat=True).distinct().order_by('-month')
        return context


class BenevoleArchivesMonthView(BenevoleMixin, TemplateView):
    template_name = 'transport/app/archives-mois-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            ANNULE=Transport.StatutChoices.ANNULE, benev=self.benev, **kwargs
        )
        start = date(self.kwargs['year'], self.kwargs['month'], 1)
        end = (start + timedelta(days=32)).replace(day=1) - timedelta(days=1)
        transports = self.benev.transports.filter(
            Q(date__range=[start, end]) & (
                Q(statut__in=[
                    Transport.StatutChoices.EFFECTUE, Transport.StatutChoices.RAPPORTE,
                    Transport.StatutChoices.CONTROLE
                ]) | (
                    Q(statut=Transport.StatutChoices.ANNULE) &
                    ~Q(defrayer_chauffeur=False)
                )
            )
        ).prefetch_related('frais').order_by('-heure_rdv')
        context['days'] = {
            dt: list(transps) for dt, transps in groupby(transports, key=attrgetter('date'))
        }
        context['sums'] = transports.aggregate(
            total_km=Sum('km'), total_frais=Sum('frais__cout'),
            total_duree=Sum('duree_eff'), total_attente=Sum('temps_attente'),
        )
        return context
