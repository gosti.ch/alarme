from django.apps import apps
from django.template import Library
from django.templatetags.static import static
from django.utils.html import format_html
from django.utils.safestring import SafeString

from ..models import ClientType

register = Library()


@register.simple_tag
def bandeau_absence(presence):
    """presence is AdressePresence instance."""
    if not presence:
        return ''
    adr = presence.adresse
    if adr.hospitalisation:
        msg = "Actuellement à l’hôpital"
        if adr.localite:
            msg += f" ({adr})"
    else:
        msg = f"Actuellement à {adr}"
    if presence.jusqua:
        msg += f", jusqu’au {presence.jusqua.strftime('%d.%m.%Y')}"
    if adr.remarque:
        msg += f" ({adr.remarque})"
    return format_html('<div class="alert alert-danger">{}</div>', msg)


@register.simple_tag
def services_as_icons(client, exclude='', classes=''):
    icons = ""
    if 'alarme' in client.type_client and exclude != 'alarme':
        icons += (
            f'<img class="icon ms-2 {classes}" src="{static("logos/logo-alarme.svg")}" '
            'title="Cette personne est cliente de l’Alarme" data-bs-toggle="tooltip">'
        )
    if 'transport' in client.type_client and exclude != 'transport':
        icons += (
            f'<img class="icon ms-2 {classes}" src="{static("logos/transport.svg")}" '
            'title="Cette personne est cliente des Transports" data-bs-toggle="tooltip">'
        )
    if 'visite' in client.type_client and exclude != 'visite':
        icons += (
            f'<img class="icon ms-2 {classes}" src="{static("logos/visite.svg")}" '
            'title="Cette personne est cliente des Visites" data-bs-toggle="tooltip">'
        )
    if 'savd' in client.type_client and exclude != 'savd':
        icons += (
            f'<img class="icon ms-2 {classes}" src="{static("logos/savd.svg")}" '
            'title="Cette personne est cliente SAVD" data-bs-toggle="tooltip">'
        )
    return SafeString(icons)


@register.simple_tag
def client_details_url(client):
    """
    Selon utilisateur connecté et type de client, renvoie l'URL pour accéder
    au dossier client.
    """
    if (
        (apps.is_installed('alarme') and ClientType.ALARME in client.type_client) or
        (apps.is_installed('transport') and ClientType.TRANSPORT in client.type_client) or
        (apps.is_installed('visite') and ClientType.VISITE in client.type_client)
    ):
        return client.get_absolute_url()
    return ""
