from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alarme', '0103_remove_alerte_mission'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            state_operations=[
                migrations.DeleteModel(
                    name='Alerte',
                ),
            ],
            database_operations=[],
        )
    ]
