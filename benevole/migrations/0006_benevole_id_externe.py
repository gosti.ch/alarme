from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0005_benevole_inactif"),
    ]

    operations = [
        migrations.AddField(
            model_name="benevole",
            name="id_externe",
            field=models.BigIntegerField(blank=True, null=True),
        ),
        migrations.AddConstraint(
            model_name="benevole",
            constraint=models.UniqueConstraint(
                fields=("id_externe",), name="benevole_id_externe_unique"
            ),
        ),
    ]
