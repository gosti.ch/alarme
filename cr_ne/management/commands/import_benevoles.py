from pathlib import Path

from openpyxl import load_workbook

from django.core.management.base import BaseCommand, CommandError

from alarme.models import Benevole, LANGUAGE_CHOICES


class Command(BaseCommand):
    """A command to read xlsx list of benevoles and import to this DB."""
    def add_arguments(self, parser):
        parser.add_argument('path')

    def handle(self, **options):
        file_path = Path(options['path'])
        if not file_path.exists():
            raise CommandError(f"File {file_path} does not exist.")
        wb = load_workbook(file_path)
        ws = wb.active
        headers = []
        languages = {v: k for k, v in dict(LANGUAGE_CHOICES).items()}
        for idx, row in enumerate(ws, start=1):
            if idx < 3:
                continue
            elif idx == 3:
                headers = [cell.value for cell in row]
                continue
            values = dict(zip(headers, [cell.value for cell in row]))
            if not values['Adresse']:
                continue

            act_value = (values['Activités'] or '').lower()
            activites = []
            if 'visite' in act_value:
                activites.append('visite')
            if 'install' in act_value:
                activites.append('installation')
            if 'formation' in act_value:
                activites.append('formation')
            tels = (values['Téléphone'] or '').split('\n')
            tels_prives = [tel for tel in tels if tel.startswith(('03', '02'))]
            tels_mobiles = [tel for tel in tels if tel.startswith('07')]
            langues = ['fr']
            if values['Langues étrangères']:
                langues.extend([
                    languages[v.replace(',', '').replace('(', '').replace(')' ,'')]
                    for v in values['Langues étrangères'].split()
                ])

            Benevole.objects.create(
                nom=values['Nom Prénom'].split()[0],
                prenom=values['Nom Prénom'].split()[1],
                rue=values['Adresse'], npa=values['NPA'], localite=values['Localité'],
                tel_prive=tels_prives[0] if tels_prives else '',
                tel_mobile=tels_mobiles[0] if tels_mobiles else '',
                courriel=values['E-mail'],
                langues=langues,
                remarques=values['Remarques'] or '',
                formation=values['Appareil'] or '',
                activites=activites,
            )
