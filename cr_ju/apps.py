from importlib import import_module

from django.apps import AppConfig
from django.db.models.signals import post_save


class CRJUAppConfig(AppConfig):
    name = 'cr_ju'
    verbose_name = "Croix-Rouge Jura"
    abrev = "JU"
    EXTRA_LOGEMENT_CHOICES = ()

    def ready(self):
        from alarme.models import InstallBase, Installation
        from . import signals
        from . import policies

        InstallBase.fact_policy = policies.FacturationPolicy()
        post_save.connect(signals.install_saved, sender=Installation)

    def pdf_headfoot_class(self):
        try:
            return self.module.pdf.PDFHeaderFooter
        except AttributeError:
            import_module(f'{self.name}.pdf')
            return self.module.pdf.PDFHeaderFooter
