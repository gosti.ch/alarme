from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save, pre_save


class ApiConfig(AppConfig):
    name = "api"

    def ready(self):
        if settings.CID_SYNC_ENABLE:
            from client.models import Client, referent_saved, Referent
            from benevole.models import Benevole
            from . import signals

            if 'clients' in settings.CID_SYNC_ENABLE:
                pre_save.connect(signals.cache_previous_instance, sender=Client)
                post_save.connect(signals.client_sync_receiver, sender=Client)
            if 'referents' in settings.CID_SYNC_ENABLE:
                pre_save.connect(signals.cache_previous_instance, sender=Referent)
                referent_saved.connect(signals.referent_sync_receiver, sender=Referent)
            if 'benevoles' in settings.CID_SYNC_ENABLE:
                pre_save.connect(signals.cache_previous_instance, sender=Benevole)
                post_save.connect(signals.benevole_sync_receiver, sender=Benevole)
