from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0025_transport_chauffeur_vu"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="date_facture",
            field=models.DateField(
                blank=True, null=True, verbose_name="Date de facture"
            ),
        ),
    ]
