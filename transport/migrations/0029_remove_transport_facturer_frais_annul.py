from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0028_migrate_statut_fact"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="transport",
            name="facturer",
        ),
        migrations.RemoveField(
            model_name="transport",
            name="facturer_frais_annulation",
        ),
    ]
