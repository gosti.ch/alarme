from django.core.exceptions import FieldDoesNotExist
from django.db import models
from django.utils.html import format_html

from client.models import Client
from common.models import Utilisateur


class Question:
    def __init__(self, section, question, field, compl='', aide=''):
        self.section = section
        self.question = question
        self.field = field
        self.aide = aide
        self.complement = compl

    @property
    def field_detail(self):
        try:
            Questionnaire._meta.get_field(f'{self.field}_details')
            return f'{self.field}_details'
        except FieldDoesNotExist:
            return ''


class Section:
    def __init__(self, key, theme='', buts='', champs=()):
        self.key = key
        self.theme = theme
        self.buts = buts
        self.champs = [Question(self, **champ) for champ in champs]


class Questionnaire(models.Model):
    class Reponse(models.IntegerChoices):
        PAS_DE_REPONSE = -1, "Pas de réponse"
        OUI = 1, "Oui"
        NON = 2, "Non"

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    collab = models.ForeignKey(Utilisateur, on_delete=models.PROTECT)
    #statut = models.CharField(max_length=10, choices=...)
    non_repondu = models.BooleanField(default=False)
    termine_le = models.DateTimeField("Questionnaire terminé le", blank=True, null=True)
    aide_entretien = models.TextField(blank=True)
    interet_alarme = models.BooleanField(default=False)
    message_services = models.TextField("Information pour les services", blank=True)
    remarques = models.TextField("Autres remarques générales sur l’entretien", blank=True)

    # Champs du questionnaire
    # Un champ integer pour une réponse oui/non/pas de rép.,
    # un champ texte pour des détails (pour le oui ou le non).
    solitude = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    solitude_details = models.TextField(blank=True)
    besoin_contacts = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    besoin_contacts_details = models.TextField(blank=True)
    proche_aidant = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    proche_aidant_details = models.TextField(blank=True)
    aide_courses = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    aide_courses_details = models.TextField(blank=True)
    repas_auto = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    repas_auto_details = models.TextField(blank=True)
    entretien_logement = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    entretien_logement_details = models.TextField(blank=True)
    moyens_aux = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    moyens_aux_details = models.TextField(blank=True)
    chutes = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    chutes_details = models.TextField(blank=True)
    aide_soins = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    aide_soins_details = models.TextField(blank=True)
    besoin_admin = models.SmallIntegerField(blank=True, null=True, choices=Reponse.choices)
    besoin_admin_details = models.TextField(blank=True)

    structure = {
        'intro': Section('intro', theme="Introduction"),
        'environnement': Section(
            'environnement',
            theme="Environnement familial et social",
            buts="détecter des souffrances en lien avec la solitude et proposer "
                 "prestation , détection, prévention épuisement proche aidant",
            champs=[
                {"question": "Ressentez-vous le besoin de contacts extérieurs ?", "field": "besoin_contacts"},
                {"question": "Souffrez-vous de solitude ?", "field": "solitude"},
                {"question": "Avez-vous de l’aide de la part d’un proche ?", "field": "proche_aidant",
                 "compl": "Si oui, un ou plusieurs et quelle fréquence ?"},
            ]
        ),
        'logement': Section(
            'logement',
            theme="Conditions de logement",
            buts="Identifier les besoins avec la vie quotidienne, identifier les besoins d’adaptation du logement",
            champs=[
                {"question": "Faites-vous vos courses vous-même ?", "field": "aide_courses", "compl": "Si non, qui vous aide ?"},
                {"question": "Est-ce que vous arrivez à préparer vos repas vous-même ?", "field": "repas_auto",
                 "compl": "Si non, auriez-vous besoin d’aide ou en recevez-vous déjà ?"},
                {"question": "Entretenez-vous seul-e votre logement ?", "field": "entretien_logement",
                 "compl": "Si non, auriez-vous besoin d’aide ou en recevez-vous déjà ?"},
                {"question": "Avez-vous des moyens auxiliaires ?", "field": "moyens_aux",
                 "compl": "Si oui lesquels : intérieur/extérieur (orientation ergo/médecin)"},
                {"question": "Avez-vous déjà chuté ? Est-ce que cela génère une inquiétude ?", "field": "chutes"},
            ],
        ),
        'sante': Section(
            'sante',
            theme="État de santé",
            buts="Proposer des solutions pour soulager des douleurs, proposer tous types de soins",
            champs=[
                {"question": "Avez-vous besoin d’aide pour votre toilette ou d’autres soins ? "
                             "(soins de base, aide à l’habillage, bas de contention…)",
                 "field": "aide_soins"},
            ],
        ),
        'socioeco': Section(
            'socioeco',
            theme="Conditions socio-économiques",
            buts="Détecter les besoins en soutien administratif, détecter la précarité financière",
            champs=[
                {"question": "Avez-vous besoin d’aide pour vos besoins administratifs (paiements, impôts, etc.) ?",
                 "field": "besoin_admin"},
            ],
        ),
        'fin': Section('fin', theme="Résumé/fin de l’entretien", champs=[
            {"question": "Informations pour les autres services", "field": "message_services",
             "aide": (
                "Ce contenu sera transmis aux services concernés par ce client sous "
                "forme d’alertes. Typiquement utilisé pour communiquer une modification "
                "de la situation d’un client."
             )
            },
            {"question": "Autres remarques générales sur l’entretien", "field": "remarques"},
        ]),
    }

    def __str__(self):
        qdate = f"du {self.termine_le.strftime('%d.%m.%Y')}" if self.termine_le else "en cours"
        return f"Questionnaire {qdate} pour {self.client}"

    def theme_ok(self, key):
        """Renvoie True si le thème donné est complètement termine, sinon False."""
        fields = [quest.field for quest in self.structure[key].champs]
        return all(getattr(self, fname) is not None for fname in fields)

    def quest_ok(self):
        """Renvoie True si toutes les questions du questionnaire ont été abordées."""
        return all(self.theme_ok(key) for key in list(self.structure.keys())[:-1])

    def get_reponse(self, field_name):
        try:
            return getattr(self, f'get_{field_name}_display')()
        except AttributeError:
            return getattr(self, field_name)

    def resume(self):
        for section in self.structure.values():
            for question in section.champs:
                details = getattr(self, question.field_detail, '')
                if question.field == "chutes" and self.interet_alarme:
                    details = format_html(
                        "{}<br><i>La personne a donné son accord pour signaler son intérêt "
                        "à une installation d’alarme</i>",
                        details
                    )
                yield {
                    'question': question,
                    'reponse': self.get_reponse(question.field),
                    'reponse_details': details,
                }

"""
class Report(models.Model):
    jusqua = models.DateField()
    details = models.TextField()
    termine = models.BooleanField(default=False)
"""
