from django.test import TestCase

from client.models import Client
from .utils import RegionFinder


class CommonTests(TestCase):
    def test_region_finder_get_region(self):
        self.assertEqual(RegionFinder.get_region('1800'), 'Autre')
        self.assertEqual(RegionFinder.get_region('2000'), 'Neuchâtel')
        self.assertEqual(RegionFinder.get_region('2523'), 'Littoral Est')
        self.assertEqual(RegionFinder.get_region('2333'), 'Autre')

    def test_region_finder_get_filter(self):
        cl_neuch = Client.objects.create(nom='Doe', npa='2000')
        Client.objects.create(nom='Smith', npa='2300')
        cl_vevey = Client.objects.create(nom='Smith', npa='1800')
        self.assertQuerySetEqual(
            Client.objects.filter(RegionFinder.get_region_filter('npa', 'Neuchâtel')),
            [cl_neuch]
        )
        self.assertQuerySetEqual(
            Client.objects.filter(RegionFinder.get_region_filter('npa', 'Autre')),
            [cl_vevey]
        )
