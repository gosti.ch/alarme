from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.db import models

from .distance import get_point_from_address, wgs84_to_mn95
from .utils import icon_url

GEOADMIN_MAP_URL_TEMPLATE = (
    "https://map.geo.admin.ch/?lang=fr&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe"
    "&E={long}&N={lat}&zoom=8&crosshair=marker"
)
GOOGLE_MAP_URL_TEMPLATE = "https://www.google.com/maps/search/?api=1&query={lat}%2C{long}"


class GeolocMixin:
    def check_geolocalized(self):
        if self.empl_geo:
            return True
        return self.geolocalize()

    def geolocalize(self, save=True):
        lon_lat = get_point_from_address(self.rue, f"{self.npa} {self.localite}")
        if lon_lat:
            self.empl_geo = list(lon_lat)
            if save:
                self.save()
            return True
        elif self.empl_geo:
            self.empl_geo = None
            if save:
                self.save()
        return False

    @property
    def map_link(self):
        if not self.empl_geo:
            return None
        pt = wgs84_to_mn95(*self.empl_geo)
        return GEOADMIN_MAP_URL_TEMPLATE.format(long=pt.x, lat=pt.y)

    @property
    def gmap_link(self):
        if not self.empl_geo:
            return None
        wgs84_to_mn95(*self.empl_geo)
        return GOOGLE_MAP_URL_TEMPLATE.format(long=self.empl_geo[0], lat=self.empl_geo[1])


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Le courriel est obligatoire')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self.create_user(email, password, **extra_fields)


class Utilisateur(AbstractUser):
    """Main user model."""
    email = models.EmailField('Courriel', unique=True)
    fonction = models.CharField('Fonction', max_length=100, blank=True)

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
        return ' '.join([self.last_name, self.first_name]) if self.last_name else self.email

    @property
    def is_benevole(self):
        return hasattr(self, 'benevole') and not self.has_perm('client.change_client')


class DistanceFigee(models.Model):
    """
    Distance figée de manière statique entre deux points, pour éviter d'éventuelles
    variation du service d'interrogation des distances (par ex. pour une facturation
    stable des km d'un transport régulier).
    """
    created = models.DateTimeField(auto_now_add=True)
    empl_geo_dep = ArrayField(models.FloatField(), size=2, verbose_name="Empl. géo de départ")
    empl_geo_arr = ArrayField(models.FloatField(), size=2, verbose_name="Empl. géo d’arrivée")
    distance = models.DecimalField("Distance [km]", max_digits=5, decimal_places=1)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['empl_geo_dep', 'empl_geo_arr'], name='trajet_unique'
            ),
        ]

    def __str__(self):
        return f"Distance figée de {self.distance} entre {self.empl_geo_dep} et {self.empl_geo_arr}"


class TypeFichier(models.TextChoices):
    DOC_GENERAL = 'doc', "Document général"
    QUEST_ALARME = 'quest', "Questionnaire signé"
    CONTRAT_ALARME = 'contrat', "Contrat signé"


def content_type_path(instance, filename):
     return "fichiers/{0}/{1}".format(instance.content_type.model, filename)


class Fichier(models.Model):
    """
    Modèle générique de stockage de fichier pour différents autres modèles (clients, bénévoles, etc.).
    """
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")
    titre = models.CharField("Titre", max_length=150)
    fichier = models.FileField("Fichier", upload_to=content_type_path)
    typ = models.CharField("Type de document", max_length=10, choices=TypeFichier.choices, default='doc')
    qui = models.ForeignKey(Utilisateur, on_delete=models.SET_NULL, blank=True, null=True, related_name='+')
    quand = models.DateTimeField()

    def __str__(self):
        return f'Fichier «{self.titre}» pour {self.content_object}'

    def delete(self, **kwargs):
        self.fichier.storage.delete(self.fichier.name)
        super().delete(**kwargs)

    @property
    def icon(self):
        return icon_url(self.fichier.name)
