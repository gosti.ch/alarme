from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0023_transportmodel_heure_depart"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="facturer",
            field=models.BooleanField(
                default=True, verbose_name="Facturer le transport"
            ),
        ),
    ]
