Client Alarme Croix-Rouge {{ canton }}
====
Nom: {{ client.nom }}
Prénom: {{ client.prenom }}
Rue: {{ client.rue }}
NPA, localité: {{ client.npa }} {{ client.localite }}
Tél. 1: {{ client.tel_1 }}
Tél. 2: {{ client.tel_2 }}
Courriel: {{ client.courriel }}
Date de naissance: {{ client.date_naissance }}
Langue(s): {{ client.get_langues_display }}
{% if client.nom_part %}
Nom Prénom client-e 2: {{ client.nom_part }} {{ client.prenom_part }}
Date de naissance client-e 2: {{ client.date_naissance_part }}
{% endif %}
Appareil: {% if alarme %}{{ alarme.modele }}, SIM {{ alarme.carte_sim }}, SN {{ alarme.no_serie }}{% else %}pas encore défini{% endif %}{% if mission and mission.planifiee %}
Type d’intervention: {{ mission.type_mission.nom }}
Intervention planifiée le: {{ mission.planifiee|date:'d.m.Y H:i'}}{% endif %}

{% if client.situation_vie %}Situation de vie: {{ client.situation_vie }}{% endif %}{% if client.animal_comp %}
Animal de compagnie: {{ client.animal_comp }}{% endif %}{% if client.type_logement %}
Type de logement: {{ client.get_type_logement_display }}{% if client.type_logement_info %}, {{ client.type_logement_info }}{% endif %}{% endif %}{% if client.nb_pieces %}
Nb de pièces: {{ client.nb_pieces }}{% endif %}{% if client.etage %}
Étage: {{ client.etage }}{% if client.ascenseur %}, avec ascenseur{% else %}, sans ascenseur{% endif %}{% endif %}{% if client.code_entree %}
Code d’entrée: {{ client.code_entree }}{% endif %}{% if client.cles %}
Infos clés: {{ client.cles }}{% endif %}

{% if client.donnees_medic %}Données médicales
========
{{ client.get_donnees_medic_display }}{% endif %}{% for medecin in medecins %}
{% if forloop.first %}
Médecins/soignant-e-s:
{% endif %} - {{ medecin }}{% endfor %}

Répondants
========
{% for pers in contacts.repondants %}{{ forloop.counter }}. {% if pers.sama %}Samaritains
{% for sama in liste_samas %}    {{ forloop.counter }}. {{ sama.professionnel.nom }} {{ sama.professionnel.prenom }}{% if sama.professionnel.inactif %} (temporairement en inactivité){% endif %}
{% endfor %}{% else %}{{ pers.nom }} {{ pers.prenom }}, {{ pers.rue }}, {{ pers.npa }} {{ pers.localite }}
{% for tel in pers.telephones %}Tél {{ forloop.counter }}: {{ tel.tel }}{% if tel.remarque %} ({{ tel.remarque }}){% endif %}
{% endfor %}{% if pers.courriel %}Courriel: {{ pers.courriel }}
{% endif %}Relation: {{ pers.relation }}{% endif %}
{% endfor %}
Personnes à prévenir en cas d’urgence
========
{% for pers in contacts.referents %}{{ forloop.counter }}. {{ pers.nom }} {{ pers.prenom }}, {{ pers.rue }}, {{ pers.npa }} {{ pers.localite }}
{% for tel in pers.telephones %}Tél {{ forloop.counter }}: {{ tel.tel }}{% if tel.remarque %} ({{ tel.remarque }}){% endif %}
{% endfor %}{% if pers.courriel %}Courriel: {{ pers.courriel }}
{% endif %}Relation: {{ pers.relation }}
{% endfor %}
Historique récent (30j.)
========
{% for hist in historique %}{{ hist.quand.date }}: {{ hist.description }}
{% endfor %}
