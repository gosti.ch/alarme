from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0002_adresse_tel"),
    ]

    operations = [
        migrations.AddIndex(
            model_name="transport",
            index=models.Index(
                fields=["heure_rdv"], name="index_heure_rdv"
            ),
        ),
    ]
