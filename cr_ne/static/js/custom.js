
class NavSender {
    constructor(form) {
        this.clientWithErrors = [];
        this.form = form;
    }

    send() {
        // Take first client in list
        let trs = document.querySelectorAll("table.facture-list tbody tr");
        let clientTr = Array.from(trs).find(tr => (tr.dataset.clientid && !this.clientWithErrors.includes(tr.dataset.clientid)));
        if (typeof clientTr === 'undefined') return;
        this.sendNextToNavision(clientTr.dataset.clientid).then(result => {
            if (result == 'error') this.clientWithErrors.push(clientTr.dataset.clientid);
            this.send();
        })
    }

    sendNextToNavision(clientId) {
        let formData = new FormData();
        formData.append('csrfmiddlewaretoken', document.querySelector('[name=csrfmiddlewaretoken]').value);
        formData.append('client', clientId);
        let url = this.form.action;
        console.log(`POSTing url ${url}`);
        return fetch(url, {
            method: 'POST', headers: {'Accept': 'application/json'}, body: formData
        }).then(
            resp => resp.json()
        ).then((data) => {
            console.log(data.result);
            if (data.result == 'OK') {
                // Ajax-refresh list of factures
                fetch(location.href).then(resp => resp.text()).then(html => {
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(html, "text/html");
                    document.querySelector('.facture-list tbody').replaceWith(doc.querySelector('.facture-list tbody'));
                    document.querySelector('.list-count').replaceWith(doc.querySelector('.list-count'));
                    document.querySelector('#pagination').replaceWith(doc.querySelector('#pagination'));
                });
            }
            return data.result
        });
    }
}


window.addEventListener('DOMContentLoaded', () => {
    const envoiBtn = document.querySelector('#envoi-factures');
    if (envoiBtn) envoiBtn.addEventListener('submit', ev => {
        ev.preventDefault();
        sender = new NavSender(ev.target);
        sender.send();
    });
});
