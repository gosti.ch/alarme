from datetime import timedelta

from .base import *

from django.urls import reverse_lazy

WSGI_APPLICATION = 'common.wsgi_alarme.application'
ROOT_URLCONF = 'alarme.urls'

CANTON_APP = 'cr_ju'
INSTALLED_APPS.extend([
    CANTON_APP,
    'alarme',
])
DATABASES['default']['NAME'] = 'alarmes_ju'

MAIN_LOGO = 'img/logo-cr-ju.svg'
APP_LOGO = 'logos/logo-alarme.svg'

SERVER_EMAIL = 'technique@croix-rouge-jura.ch'
DEFAULT_FROM_EMAIL = 'technique@croix-rouge-jura.ch'

EMAIL_HOST = 'mails.croix-rouge-jura.ch'
ALARMPOST_EMAIL = 'technique@croix-rouge-jura.ch'
ALARMPOST_EMAIL_HOST = EMAIL_HOST
ALARMPOST_EMAIL_PASSWORD = ''

APPAREIL_REVISION_ACTIF = False
FACTURES_EXPORT_URL = reverse_lazy('factures-export')
FACTURES_TRIMESTRIELLES = True
CODES_ARTICLE_INSTALLATION = ['TAXE', 'TAXE2']
COMPTA_EMAIL = None
DUREE_SANS_CLIENT_NEW_DEFAULT = timedelta(seconds=30 * 60)
