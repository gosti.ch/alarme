from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0016_transport_remarques_chauff"),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transport',
            name='remarques',
        ),
        migrations.RenameField(
            model_name='transport',
            old_name='remarques_chauff',
            new_name='remarques',
        ),
        migrations.AlterField(
            model_name="transport",
            name="remarques",
            field=models.TextField(blank=True, verbose_name="Remarques"),
        ),
    ]
