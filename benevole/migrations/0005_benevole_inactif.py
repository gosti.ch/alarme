from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("benevole", "0004_benevole_incompats"),
    ]

    operations = [
        migrations.AddField(
            model_name="benevole",
            name="inactif",
            field=models.BooleanField(
                default=False, verbose_name="Temporairement inactif"
            ),
        ),
    ]
