from django import forms
from django.db.models import Q, Value
from django.db.models.functions import Replace

from common.forms import (
    BootstrapMixin, FormsetMixin, ModelForm, NPALocaliteMixin
)
from common.models import Utilisateur

from .models import Activite, Benevole


class BenevActiviteForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Activite
        fields = ['activite', 'debut', 'fin', 'inactif', 'note']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.instance.pk is not None:
            self.fields['activite'].disabled = True

    def _change_prefix(self):
        return f"activité {self.instance.get_activite_display()}"


class BenevoleForm(BootstrapMixin, NPALocaliteMixin, FormsetMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Benevole
        exclude = ['utilisateur']
        fields = [
            'nom', 'prenom', 'genre', 'date_naissance', 'rue', 'npa', 'localite',
            'tel_prive', 'tel_mobile', 'tel_prof', 'courriel', 'no_debiteur',
            'formation', 'langues', 'absences', 'remarques',
        ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        ActivitesFormSet = forms.inlineformset_factory(
            Benevole, Activite, form=BenevActiviteForm, extra=1, min_num=1, can_delete=False,
        )
        self.formset = ActivitesFormSet(instance=kwargs['instance'], data=kwargs.get('data'))

    def clean_courriel(self):
        courriel = self.cleaned_data.get('courriel')
        if courriel and 'courriel' in self.changed_data and Utilisateur.objects.filter(email=courriel).exists():
            raise forms.ValidationError("Il existe déjà un utilisateur de cette application avec ce courriel.")
        return courriel


class BenevoleTransportForm(BenevoleForm):
    class Meta(BenevoleForm.Meta):
        fields = BenevoleForm.Meta.fields + ['no_plaques', 'incompats', 'macaron_depuis']


class BenevoleFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'autofocus': True, 'size': '8'}),
        required=False
    )
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Tél', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    toutes_activites = forms.BooleanField(label="Toutes activités", required=False)

    def filter(self, benevs):
        if self.cleaned_data['nom']:
            term = self.cleaned_data['nom'].split()[0]
            benevs = benevs.filter(
                Q(nom__unaccent__icontains=term) | Q(prenom__unaccent__icontains=term)
            )
        if self.cleaned_data['npa_localite']:
            benevs = benevs.filter(
                Q(npa__icontains=self.cleaned_data['npa_localite']) |
                Q(localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            benevs = benevs.annotate(
                tel1=Replace('tel_mobile', Value(' '), Value('')),
                tel2=Replace('tel_prive', Value(' '), Value('')),
                tel3=Replace('tel_prof', Value(' '), Value('')),
            ).filter(
                Q(tel1__icontains=self.cleaned_data['tel'].replace(' ', '')) |
                Q(tel2__icontains=self.cleaned_data['tel'].replace(' ', '')) |
                Q(tel3__icontains=self.cleaned_data['tel'].replace(' ', ''))
            ).distinct()

        return benevs
