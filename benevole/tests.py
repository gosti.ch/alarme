from datetime import date, datetime, timedelta

from django.apps import apps
from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from client.models import Client
from common.export import openxml_contenttype
from common.models import Utilisateur
from .models import Activite, Activites, Benevole


class BenevoleTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            Permission.objects.get(codename='view_benevole'),
            Permission.objects.get(codename='change_benevole')
        )

    def test_benevole_new(self):
        current_app = 'transport' if apps.is_installed('transport') else 'alarme'
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-new', args=[current_app]))
        self.assertContains(
            response,
            '<input type="text" name="nom" maxlength="50" class="form-control" required="" id="id_nom">',
            html=True
        )
        form_data = {
            'nom': 'Dupond',
            'prenom': 'Ladislas',
            'genre': 'M',
            'npalocalite': '2345 Petaouchnok',
            'langues': ['fr', 'it'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '0',
        }
        response = self.client.post(reverse('benevole-new', args=[current_app]), data=form_data)
        self.assertEqual(
            response.context['form'].formset.errors,
            [{'activite': ['Ce champ est obligatoire.'], 'debut': ['Ce champ est obligatoire.']}]
        )
        form_data.update({
            'activite_set-0-activite': Activites.INSTALLATION,
            'activite_set-0-debut': date.today().strftime('%d.%m.%Y'),
        })
        response = self.client.post(reverse('benevole-new', args=[current_app]), data=form_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors or response.context['form'].formset.errors)
        self.assertRedirects(response, reverse('benevoles', args=[current_app]))
        benevole = Benevole.objects.get(nom='Dupond')
        self.assertEqual(benevole.langues, ['fr', 'it'])
        self.assertEqual(benevole.journaux.latest().description, "Création du bénévole")
        # Now edit
        form_data['langues'].append('en')
        form_data.update({
            'activite_set-TOTAL_FORMS': '2',
            'activite_set-INITIAL_FORMS': '1',
            'activite_set-0-id': benevole.activite_set.first().pk,
            'activite_set-1-id': '',
            'activite_set-1-activite': '',
            'activite_set-1-debut': '',
        })
        response = self.client.get(reverse('benevole-edit', args=[current_app, benevole.pk]))
        self.assertContains(
            response,
            '<input type="checkbox" name="langues" value="it" id="id_langues_2" class="form-check-input" checked>',
            html=True
        )
        response = self.client.post(reverse('benevole-edit', args=[current_app, benevole.pk]), data=form_data)
        self.assertRedirects(response, reverse('benevoles', args=[current_app]))
        benevole.refresh_from_db()
        self.assertEqual(benevole.activite_set.count(), 1)
        self.assertEqual(benevole.activite_set.first().activite, 'installation')
        self.assertEqual(benevole.langues, ['fr', 'it', 'en'])
        self.assertEqual(
            benevole.journaux.latest().description,
            "Modification du bénévole: langues (de «français, italien» à «français, italien, anglais»)"
        )
        response = self.client.get(reverse('benevole-journal', args=[current_app, benevole.pk]))
        self.assertQuerySetEqual(response.context['object_list'], benevole.journaux.order_by('-quand'))

    def test_benevole_list(self):
        """Test des listes de bénévoles alarme et transport, actifs et archivés."""
        il_y_a_2_mois = (datetime.now() - timedelta(days=60)).date()
        benevs = [
            Benevole.objects.create(
                nom='Müller', prenom='Irma', genre='F', langues=['fr'],
                activites=[Activites.INSTALLATION]
            ),
            Benevole.objects.create(
                nom='Vieux', prenom='Alarme', genre='F',
                activites=[Activites.INSTALLATION],
                archive_le=il_y_a_2_mois, langues=['fr'],
            ),
            Benevole.objects.create(
                nom='Schmid', prenom='Marco', genre='M', langues=['fr'],
                activites=[Activites.TRANSPORT]
            ),
            Benevole.objects.create(
                nom='Vieux', prenom='Transp', genre='M',
                activites=[Activites.TRANSPORT],
                archive_le=il_y_a_2_mois, langues=['fr'],
            ),
            Benevole.objects.create(
                nom='SansActivité', prenom='Nina', genre='F', langues=['fr'],
                activites=[]
            ),
        ]
        # Fin d'activité dans le futur ne doit pas affecter les listes
        Activite.objects.filter(benevole__nom='Müller').update(fin=date.today() + timedelta(days=30))

        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['alarme']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles-archives', args=['alarme']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[1]])
        response = self.client.get(reverse('benevoles', args=['transport']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[2]])
        response = self.client.get(reverse('benevoles-archives', args=['transport']))
        self.assertQuerySetEqual(response.context['object_list'], [benevs[3]])
        # Filter
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?nom=mull')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0]])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?nom=xyz')
        self.assertQuerySetEqual(response.context['object_list'], [])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?toutes_activites=on')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[0], benevs[4], benevs[2]])
        response = self.client.get(reverse('benevoles', args=['transport']) + '?toutes_activites=on&nom=sans')
        self.assertQuerySetEqual(response.context['object_list'], [benevs[4]])
        # Export
        response = self.client.get(reverse('benevoles', args=['alarme']) + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_get_activites_display(self):
        b1 = Benevole.objects.create(
            nom='Alarme', prenom='Marco', genre='M', langues=['fr'],
            activites=[Activites.INSTALLATION, Activites.VISITE],
        )
        self.assertEqual(b1.get_activites_display(), "Installation, Visite alarme")
        act_vis = b1.activite_set.get(activite=Activites.VISITE)
        act_vis.fin = date.today() + timedelta(days=3)
        act_vis.save()
        self.assertEqual(b1.get_activites_display(), "Installation, Visite alarme")
        act_vis.fin = date.today() - timedelta(days=3)
        act_vis.save()
        self.assertEqual(b1.get_activites_display(), "Installation")

    def test_benevole_par_activite(self):
        b1 = Benevole.objects.create(
            nom='Alarme', prenom='Marco', genre='M', langues=['fr'],
            activites=[Activites.INSTALLATION, Activites.VISITE],
        )
        # Pas OK: archivé
        Benevole.objects.create(
            nom='Archive', prenom='Marco', genre='M', langues=['fr'], activites=[Activites.VISITE],
            archive_le=date.today(),
        )
        # Pas OK: autre activite
        Benevole.objects.create(
            nom='Transport', prenom='Marco', genre='M', langues=['fr'], activites=[Activites.TRANSPORT]
        )
        # Pas OK: activite concernée est terminée
        b4 = Benevole.objects.create(
            nom='Transport-Alarme', prenom='Marco', genre='M', langues=['fr'],
            activites=[Activites.TRANSPORT, Activites.VISITE],
        )
        b4.activite_set.filter(activite=Activites.VISITE).update(fin=date.today() - timedelta(days=1))
        # OK: activité concernée jusqu'à demain
        b5 = Benevole.objects.create(
            nom='Transport-Alarme2', prenom='Marco', genre='M', langues=['fr'],
            activites=[Activites.TRANSPORT, Activites.VISITE],
        )
        b5.activite_set.filter(activite=Activites.VISITE).update(fin=date.today() + timedelta(days=1))

        self.assertQuerySetEqual(Benevole.objects.par_activite(Activites.VISITE), [b1, b5])
        self.assertQuerySetEqual(Benevole.objects.par_activite(Activites.VISITE, include=b4), [b1, b4, b5])

    def test_benevole_par_domaine(self):
        b1 = Benevole.objects.create(
            nom='Transport', prenom='Marco', genre='M', langues=['fr'], activites=[Activites.TRANSPORT]
        )
        b2 = Benevole.objects.create(
            nom='Archive', prenom='Marco', genre='M', langues=['fr'], activites=[Activites.TRANSPORT],
            archive_le=date.today(),
        )
        b3 = Benevole.objects.create(
            nom='Alarme', prenom='Marco', genre='M', langues=['fr'],
            activites=[Activites.INSTALLATION, Activites.VISITE],
        )
        b4 = Benevole.objects.create(
            nom='Transport-Alarme', prenom='Morco', genre='M', langues=['fr'],
            activites=[Activites.TRANSPORT, Activites.INSTALLATION],
        )
        b4.activite_set.filter(activite=Activites.INSTALLATION).update(inactif=True)
        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport'), [b1, b4])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport', actifs=None), [b2, b1, b4])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('transport', include=b3), [b3, b1, b4])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('alarme'), [b3])
        self.assertQuerySetEqual(Benevole.objects.par_domaine('alarme', actifs=None), [b3, b4])

    def test_benevole_autocomplete(self):
        benevole = Benevole.objects.create(
            nom='Müller', prenom='Irma', rue="Av du Stand 12", npa="2300", localite="La Chaux-de-Fonds",
            langues=['fr'], activites=[Activites.INSTALLATION, Activites.VISITE]
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-autocomplete', args=['tous']) + '?q=mul')
        self.assertEqual(response.json(), [{
            'label': 'Müller Irma', 'value': benevole.pk,
            'adresse': 'Av du Stand 12, 2300 La Chaux-de-Fonds',
            'types': ['installation', 'visite'],
            'url': benevole.get_absolute_url(),
        }])

    def test_benevole_archiver_par_fin_activite(self):
        """Plus d'activité en cours, auto-archivage."""
        benevole = Benevole.objects.create(
            nom='Müller', prenom='Irma', genre='F', langues=['fr'], activites=[Activites.INSTALLATION]
        )
        dans_3_jours = date.today() + timedelta(days=3)
        form_data = {
            'nom': 'Müller',
            'prenom': 'Irma',
            'genre': 'F',
            'npalocalite': '',
            'langues': ['fr'],
            'activite_set-TOTAL_FORMS': '1',
            'activite_set-INITIAL_FORMS': '1',
            'activite_set-0-id': benevole.activite_set.first().pk,
            'activite_set-0-activite': Activites.INSTALLATION,
            'activite_set-0-debut': date.today().strftime('%d.%m.%Y'),
            'activite_set-0-fin': dans_3_jours.strftime('%d.%m.%Y'),
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-edit', args=['alarme', benevole.pk]), data=form_data)
        self.assertRedirects(response, reverse('benevoles', args=['alarme']))
        benevole.refresh_from_db()
        # Pas encore archivé si la date de fin d'activité est dans le futur
        self.assertIsNone(benevole.archive_le)
        self.assertEqual(
            benevole.journaux.latest().description,
            "Modification du bénévole: activité Installation: ajout de "
            f"jusqu’au («{dans_3_jours.strftime('%Y-%m-%d')}»)"
        )

        form_data['activite_set-0-fin'] = date.today().strftime('%d.%m.%Y')
        response = self.client.post(reverse('benevole-edit', args=['alarme', benevole.pk]), data=form_data)
        self.assertRedirects(response, reverse('benevoles', args=['alarme']))
        benevole.refresh_from_db()
        self.assertEqual(benevole.archive_le, date.today())
        dernier, avantdernier = benevole.journaux.order_by('-quand')[:2]
        self.assertEqual(
            avantdernier.description,
            "Modification du bénévole: activité Installation: jusqu’au "
            f"(de «{dans_3_jours.strftime('%Y-%m-%d')}» à «{date.today().strftime('%Y-%m-%d')}»)"
        )
        self.assertEqual(
            dernier.description,
            "Plus d’activité en cours, archivage du bénévole."
        )

    def test_benevole_archiver(self):
        benevole = Benevole.objects.create(
            nom='Müller', prenom='Irma', langues=['fr'], activites=[Activites.INSTALLATION, Activites.VISITE]
        )
        Client.objects.create(
            nom="Dupond", prenom="Ladislas", npa='2345',
            type_client=['alarme'], visiteur=benevole,
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('benevole-archive', args=['alarme', benevole.pk]), {}, follow=True)
        self.assertEqual(
            str(list(response.context['messages'])[0]),
            'Müller Irma a bien été archivé. Pour 1 client(s) '
            'dont le bénévole était visiteur, le champ visiteur a été vidé.'
        )

    def test_benevole_archived_edit(self):
        benevole = Benevole.objects.create(
            nom='Müller', prenom='Irma', langues=['fr'], activites=[Activites.INSTALLATION, Activites.VISITE],
            archive_le=date.today(),
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-edit', args=['alarme', benevole.pk]))
        self.assertContains(response, "Müller")
