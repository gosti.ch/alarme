from datetime import date

from django import forms
from django.db.models import Count, Max, Prefetch, Q, Value
from django.db.models.functions import Replace
from django.forms.models import inlineformset_factory
from django.urls import reverse_lazy
from django.utils.timezone import now

from common.forms import (
    BootstrapMixin, BSRadioSelect, DateInput, ModelForm, NPALocaliteMixin,
    ReadOnlyableMixin,
)
from common.utils import current_app
from common.views import GeoAddressFormMixin
from .models import (
    AdresseClient, AdressePresence, Alerte, Client, Journal, Professionnel,
    ProfClient, Referent, ReferentTel, referent_saved,
)


class DateArchiveForm(forms.Form):
    date_archive = forms.DateField(label="Date de l’archivage", widget=DateInput, required=True)


class ClientFilterFormBase(BootstrapMixin, forms.Form):
    nom_prenom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom ou prénom', 'autocomplete': 'off', 'autofocus': True}
        ),
        required=False
    )
    rue = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )
    npa_localite = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Code post. ou localité', 'autocomplete': 'off'}), required=False
    )
    tel = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off'}), required=False
    )
    listes = forms.ChoiceField(
        choices=(),
        widget=forms.Select(attrs={'class': 'w-auto ms-auto'}), required=False
    )
    recherche_part = False
    recherche_deb = False
    fact_items = []
    LIST_CHOICES = ()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.recherche_deb:
            self.fields['nom_prenom'].widget.attrs['placeholder'] = "Nom ou prénom (ou nom déb.)"
        if self.LIST_CHOICES:
            self.fields['listes'].choices = self.LIST_CHOICES
        else:
            del self.fields['listes']

    def filter(self, clients):
        if self.cleaned_data['nom_prenom']:
            nom_filters = (
                Q(nom__unaccent__icontains=self.cleaned_data['nom_prenom']) |
                Q(prenom__unaccent__icontains=self.cleaned_data['nom_prenom'])
            )
            if self.recherche_part:
                nom_filters |= Q(nom_part__unaccent__icontains=self.cleaned_data['nom_prenom'])
                nom_filters |= Q(prenom_part__unaccent__icontains=self.cleaned_data['nom_prenom'])
            if self.recherche_deb:
                nom_filters |= (
                    Q(referent__nom__unaccent__icontains=self.cleaned_data['nom_prenom']) &
                    ~Q(referent__facturation_pour=[])
                )
            clients = clients.filter(nom_filters).distinct()
        if self.cleaned_data['rue']:
            clients = clients.filter(
                rue__unaccent__icontains=self.cleaned_data['rue']
            )
        if self.cleaned_data['npa_localite']:
            clients = clients.filter(
                Q(npa__icontains=self.cleaned_data['npa_localite']) |
                Q(localite__unaccent__icontains=self.cleaned_data['npa_localite'])
            )
        if self.cleaned_data['tel']:
            clients = clients.annotate(
                tel1=Replace('tel_1', Value(' '), Value('')),
                tel2=Replace('tel_2', Value(' '), Value(''))
            ).filter(
                Q(tel1__icontains=self.cleaned_data['tel'].replace(' ', '')) |
                Q(tel2__icontains=self.cleaned_data['tel'].replace(' ', ''))
            ).distinct()
        if self.cleaned_data.get('listes') == 'autres_debit':
            clients = clients.annotate(
                num_fact_autre=Count('referent', filter=Q(
                    referent__date_archive=None, referent__facturation_pour__overlap=self.fact_items
                ))
            ).filter(num_fact_autre__gt=0).prefetch_related(
                Prefetch('referent_set', to_attr="debiteurs", queryset=Referent.objects.filter(
                    date_archive=None, facturation_pour__overlap=self.fact_items
                ))
            )
            clients._hints['_export_autres_debiteurs'] = True
        return clients


class ClientEditFormBase(BootstrapMixin, ReadOnlyableMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Client
        fields = []  # overriden by child classes
        widgets = {
            'genre': BSRadioSelect,
            'type_logement': BSRadioSelect,
        }
        labels = {
            'type_logement_info': "Autres infos sur le type de logement",
        }
    benev_hidden_fields = set()  # Champs non disponibles pour les bénévoles

    def __init__(self, user_is_benev=False, **kwargs):
        super().__init__(**kwargs)
        if user_is_benev:
            for field_name in self.benev_hidden_fields:
                del self.fields[field_name]

    def notify_other_teams(self, from_app=None):
        other_types = set(self.instance.type_client) - {from_app}
        if 'date_deces' in self.changed_data:
            if from_app:
                msg = f"L’équipe «{from_app}» a marqué cette personne comme décédée"
            else:
                msg = "Cette personne a été marquée comme décédée depuis l’ERP"
            for app in other_types:
                Alerte.objects.create(
                    client=self.instance, cible=app, alerte=msg, recu_le=now(),
                )
        elif other_types:
            changes = self.get_changed_string(changed_values=True)
            if changes:
                if from_app:
                    msg = f"Modifications depuis l’application «{from_app}»: {changes}"
                else:
                    msg = f"Modifications depuis l’ERP: {changes}"
                for app in other_types:
                    Alerte.objects.create(
                        client=self.instance, cible=app, alerte=msg, recu_le=now()
                    )


class AdressePresenceForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = AdressePresence
        fields = '__all__'


class AdressePresenceSuiviForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = AdressePresence
        fields = ['jusqua', 'dernier_contact', 'remarque']


class AdresseForm(BootstrapMixin, NPALocaliteMixin, GeoAddressFormMixin, ModelForm):
    geo_required = False

    class Meta(BootstrapMixin.Meta):
        model = AdresseClient
        fields = ['hospitalisation', 'rue', 'npa', 'localite', 'npalocalite', 'remarque']

    def __init__(self, *args, data=None, **kwargs):
        PresenceFormSet = forms.inlineformset_factory(
            AdresseClient, AdressePresence, form=AdressePresenceForm, extra=1
        )
        self.formset = PresenceFormSet(data=data, instance=kwargs['instance'])
        super().__init__(*args, data=data, **kwargs)

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def clean(self):
        data = super().clean()
        if (
            not data.get('hospitalisation') and not data.get('npa') and
            not data.get('localite') and not data.get('remarque')
        ):
            raise forms.ValidationError(
                "Vous devez indiquer un lieu, une hospitalisation ou une remarque"
            )
        if self.instance.pk is None and data.get('hospitalisation') and not self.formset.has_changed():
            raise forms.ValidationError("Vous devez indiquer un début d’hospitalisation")
        return data

    def save(self, **kwargs):
        adresse = super().save(**kwargs)
        if not self.formset.instance.pk:
            self.formset.instance = adresse
        self.formset.save(**kwargs)
        return adresse


class JournalForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Journal
        fields = ['description']


class HiddenDeleteInlineFormSet(forms.BaseInlineFormSet):
    deletion_widget = forms.HiddenInput
    ordering_widget = forms.HiddenInput


class ReferentTelForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = ReferentTel
        exclude = ['priorite']


def get_referent_formset(extra=0):
    return inlineformset_factory(
        Referent, ReferentTel, form=ReferentTelForm, formset=HiddenDeleteInlineFormSet, extra=extra, can_order=True
    )


class ReferentForm(BootstrapMixin, NPALocaliteMixin, ReadOnlyableMixin, ModelForm):
    contact_existant = forms.CharField(
        label="Copier contact de facturation existant",
        widget=forms.TextInput(attrs={
            'class': 'autocomplete',
            'autocomplete': 'off',
            'placeholder': "Recherche de contact…",
            'data-searchurl': reverse_lazy('referent-fact-search'),
        }),
        required=False
    )
    est_repondant = forms.BooleanField(label='Répondant', required=False)
    est_referent = forms.BooleanField(
        label='Personne de référence à prévenir en cas d’urgence', required=False
    )
    courrier_envoye = forms.BooleanField(label="Le courrier initial a été envoyé", required=False)

    app_specific_fields = {
        'alarme': ['courrier_envoye', 'contractant', 'est_repondant', 'est_referent'],
        'visite': ['contact_visites'],
    }

    class Meta(BootstrapMixin.Meta):
        model = Referent
        fields = [
            'nom', 'prenom', 'salutation', 'complement', 'rue', 'npa', 'localite', 'npalocalite',
            'pays', 'courriel', 'relation', 'repondant', 'referent', 'admin', 'facturation_pour',
            'contractant', 'no_debiteur', 'remarque', 'courrier_envoye', 'contact_visites',
        ]
        labels = {
            'referent': 'Personne de référence à prévenir en cas d’urgence, priorité',
        }
        widgets = {
            'repondant': forms.HiddenInput,
            'referent': forms.HiddenInput,
        }

    def __init__(self, *args, client=None, data=None, user_is_benev=False, initial=None, **kwargs):
        initial['est_repondant'] = bool(kwargs.get('instance') and kwargs['instance'].repondant)
        initial['est_referent'] = bool(kwargs.get('instance') and kwargs['instance'].referent)
        initial['courrier_envoye'] = bool(kwargs.get('instance') and kwargs['instance'].courrier_envoye)
        super().__init__(*args, data=data, initial=initial, **kwargs)
        for app, flist in self.app_specific_fields.items():
            # Selon l'application active, désactiver certains champs (avec valeurs)
            # ou les supprimer du formulaire
            if app != current_app():
                for field_name in flist:
                    if (
                        self.instance.pk is None or
                        not getattr(self.instance, field_name, initial.get(field_name, False))
                    ):
                        del self.fields[field_name]
                    else:
                        self.fields[field_name].disabled = True
        if user_is_benev:
            self.fields.pop('courrier_envoye', None)
            self.fields.pop('no_debiteur', None)
        if self.instance.pk:
            del self.fields['contact_existant']
        TelFormset = get_referent_formset(extra=0 if self.instance.pk else 2)
        self.formset = TelFormset(
            data=data, instance=kwargs.get('instance'),
            queryset=ReferentTel.objects.order_by('priorite'),
            initial=initial.get('formsets', []),
        )
        self.client = client

    def is_valid(self):
        return all([super().is_valid(), self.formset.is_valid()])

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['salutation'] == 'E' and cleaned_data['prenom']:
            self.add_error('prenom', "Les contacts Entreprise ne peuvent pas posséder de prénom")
        if cleaned_data.get('facturation_pour') and not cleaned_data.get('rue'):
            self.add_error('rue', "La rue est obligatoire pour les contacts de facturation")
        if cleaned_data.get('facturation_pour') and not cleaned_data.get('npalocalite'):
            self.add_error('npalocalite', "La localité est obligatoire pour les contacts de facturation")
        if 'est_repondant' in self.changed_data:
            if cleaned_data['est_repondant']:
                cleaned_data['repondant'] = (
                    self.client.referents.aggregate(Max('repondant'))['repondant__max'] or 0
                ) + 1
            else:
                cleaned_data['repondant'] = None
        if 'est_referent' in self.changed_data:
            if cleaned_data['est_referent']:
                cleaned_data['referent'] = (
                    self.client.referents.aggregate(Max('referent'))['referent__max'] or 0
                ) + 1
            else:
                cleaned_data['referent'] = None
        if 'courrier_envoye' in self.changed_data:
            cleaned_data['courrier_envoye'] = date.today() if cleaned_data['courrier_envoye'] else None
        else:
            cleaned_data['courrier_envoye'] = self.instance.courrier_envoye
        return cleaned_data

    def clean_facturation_pour(self):
        fact = self.cleaned_data['facturation_pour']
        if fact:
            for item in fact:
                if self.client.referents.exclude(pk=self.instance.pk).filter(
                    facturation_pour__contains=[item]
                ).exists():
                    raise forms.ValidationError(
                        "Il existe déjà un contact de facturation pour ce client et cet item"
                    )
        return fact

    def clean_contractant(self):
        contr = self.cleaned_data['contractant']
        if contr and self.client.referents.exclude(pk=self.instance.pk).filter(contractant=True).exists():
            raise forms.ValidationError("Il existe déjà un contact contractant pour ce client")
        return contr

    def _changed_value(self, fname, detail=False):
        if fname == 'ORDER' and 'tel' in self.cleaned_data:
            return f"Priorité {self.cleaned_data['ORDER']} pour n° {self.cleaned_data['tel']}"
        return super()._changed_value(fname, detail=detail)

    def save(self, **kwargs):
        is_new = self.instance.pk is None
        self.instance.client = self.client
        referent = super().save(**kwargs)
        if not self.formset.instance.pk:
            self.formset.instance = referent
        for idx, frm in enumerate(self.formset.ordered_forms):
            frm.instance.priorite = idx + 1
        self.formset.save(**kwargs)
        referent_saved.send(sender=Referent, instance=referent, created=is_new)
        return referent


class ProfessionnelSelectForm(BootstrapMixin, forms.Form):
    professionnel_pk = forms.IntegerField(widget=forms.HiddenInput)
    professionnel = forms.CharField(label="Choisir un professionnel de la santé", widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'placeholder': "Nom du professionnel…",
        'data-searchurl': reverse_lazy('professionel-search'),
        'data-pkfield': 'professionnel_pk',
    }))
    remarque_client = forms.CharField(label="Remarque (pour ce client)", widget=forms.Textarea, required=False)


class ProfessionnelForm(BootstrapMixin, NPALocaliteMixin, ModelForm):
    field_order = ['type_pro', 'nom', 'prenom', 'rue', 'npalocalite']

    class Meta(BootstrapMixin.Meta):
        model = Professionnel
        exclude = ['no_centrale', 'empl_geo', 'date_archive']


class ProfessionnelClientForm(ProfessionnelForm):
    remarque_client = forms.CharField(label="Remarque (pour ce client)", widget=forms.Textarea, required=False)


class ProfessionnelClientEditForm(BootstrapMixin, ModelForm):
    class Meta:
        model = ProfClient
        fields = ['remarque']
        labels = {
            'remarque': "Remarques (pour le client)",
        }


class ProfFilterForm(BootstrapMixin, forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    type_pro = forms.ChoiceField(choices=(('', 'Tous'),) + Professionnel.PROF_CHOICES, required=False)

    def filter(self, profs):
        if self.cleaned_data['nom']:
            profs = profs.filter(nom__icontains=self.cleaned_data['nom'])
        if self.cleaned_data['type_pro']:
            profs = profs.filter(type_pro=self.cleaned_data['type_pro'])
        return profs


class AlerteForm(BootstrapMixin, ModelForm):
    traite_le = forms.BooleanField(label="Cette alerte a été traitée", required=False)

    class Meta(BootstrapMixin.Meta):
        model = Alerte
        fields = ['traite_le', 'remarque']

    def _post_clean(self):
        # Replace boolean by current date.
        if self.cleaned_data.get('traite_le') is True:
            self.cleaned_data['traite_le'] = now()
        else:
            self.cleaned_data['traite_le'] = None
        return super()._post_clean()
