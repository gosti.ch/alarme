from reportlab.lib.enums import TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas
from reportlab.platypus import Flowable, SimpleDocTemplate

from common.utils import canton_app


class PageNumCanvas(canvas.Canvas):
    """A special canvas to be able to draw the total page number in the footer."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = []

    def showPage(self):
        self._pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self._pages)
        for page in self._pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)
        super().save()

    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 9)
        self.drawRightString(
            self._pagesize[0] - 1.6 * cm, 2.9 * cm,
            f"Page {self._pageNumber} sur {page_count}"
        )


class VerticalText(Flowable):
    """Rotates a text in a table cell."""
    def __init__(self, text):
        Flowable.__init__(self)
        self.text = text

    def draw(self):
        canvas = self.canv
        canvas.rotate(90)
        fs = canvas._fontsize
        canvas.translate(1, -fs/1.2)  # canvas._leading?
        canvas.drawString(0, 0, self.text)

    def wrap(self, aW, aH):
        canv = self.canv
        fn, fs = canv._fontname, canv._fontsize
        return canv._leading, 1 + canv.stringWidth(self.text, fn, fs)


class BaseCroixrougePDF:
    FONTSIZE = 9
    page_size = A4
    left_margin = right_margin = 1.5 * cm
    top_margin = bottom_margin = 2.5 * cm
    checked_box = '<font name="DejaVuSans" size="14">☒</font>'
    unchecked_box = '<font name="DejaVuSans" size="14">☐</font>'

    def __init__(self, tampon, *args, **kwargs):
        self.head_foot_class = canton_app.pdf_headfoot_class()()
        self.doc = self.init_doc(tampon)
        self.story = []
        super().__init__(*args, **kwargs)
        self.define_styles()

    def init_doc(self, tampon):
        return SimpleDocTemplate(
            tampon, title=self.title, pagesize=A4,
            leftMargin=self.left_margin, rightMargin=self.right_margin,
            topMargin=self.top_margin, bottomMargin=self.bottom_margin
        )

    def define_styles(self):
        self.styles = getSampleStyleSheet()
        self.style_title = ParagraphStyle(
            name='title', fontName='Helvetica-Bold', fontSize=self.FONTSIZE + 4, leading=self.FONTSIZE + 5,
            alignment=TA_CENTER
        )
        self.style_normal = ParagraphStyle(
            name='normal', fontName='Helvetica', fontSize=self.FONTSIZE, leading=self.FONTSIZE + 2,
            alignment=TA_LEFT, spaceAfter=2
        )

    def draw_header_footer(self, canvas, doc):
        self.head_foot_class.draw_header(canvas, doc)
        self.head_foot_class.draw_footer(canvas, doc)
