from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0009_alter_transport_duree_rdv"),
    ]

    operations = [
        migrations.AddField(
            model_name="frais",
            name="typ",
            field=models.CharField(
                choices=[("repas", "Repas"), ("autre", "Autre")],
                default="autre",
                max_length=6,
                verbose_name="Type de frais",
            ),
            preserve_default=False,
        ),
    ]
