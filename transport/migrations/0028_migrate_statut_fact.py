from django.db import migrations


def migrate_statut_fact(apps, schema_editor):
    Transport = apps.get_model('transport', 'Transport')
    Transport.objects.filter(facturer=False).update(statut_fact=99)
    Transport.objects.filter(facturer_frais_annulation=True, defrayer_chauffeur=True).update(statut_fact=2)
    Transport.objects.filter(facturer_frais_annulation=True, defrayer_chauffeur=False).update(statut_fact=3)


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0027_transport_statut_fact"),
    ]

    operations = [
        migrations.RunPython(migrate_statut_fact),
    ]
