from django.db.models import TextChoices


class Languages(TextChoices):
    FR = 'fr', 'français'
    DE = 'de', 'allemand'
    IT = 'it', 'italien'
    EN = 'en', 'anglais'
    ES = 'es', 'espagnol'
    PT = 'pt', 'portugais'
    TR = 'tr', 'turc'
    CZ = 'cz', 'tchèque'


class Genres(TextChoices):
    FEMME = 'F', 'Femme'
    HOMME = 'M', 'Homme'


class Handicaps(TextChoices):
    CANNES = 'cannes', 'Cannes'
    DEAMBULATEUR = 'deamb', 'Déambulateur'
    VISION = 'vision', 'Handicap visuel'
    AUDITION = 'audition', 'Handicap auditif'
    SURPOIDS = 'poids', 'Surpoids'
    COGNITIFS = 'cognitif', 'Troubles cognitifs'
