from datetime import date, timedelta

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction
from django.db.models import Count, F, Max, Q
from django.db.models.functions import TruncMonth
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import TemplateView, UpdateView

from client.models import Alerte, Client, ClientType, Journal
from client.views import ClientAccessCheckMixin, ClientListViewBase
from common.forms import AnneeForm
from common.stat_utils import Month
from common.utils import current_app
from common.views import FilterFormMixin, ListView

from .models import Questionnaire
from . import forms


def client_url_name():
    match current_app():
        case 'alarme':
            return 'client-edit'
        case 'transport':
            return 'transport-client-detail'
        case _:
            return ''


class HomeView(FilterFormMixin, PermissionRequiredMixin, ListView):
    template_name = 'besoins/home.html'
    permission_required = 'besoins.view_questionnaire'
    filter_formclass = forms.FilterClientForm
    paginate_by = 20

    def get_queryset(self, base_qs=None):
        delai_entre_quest = 210 if date.today() < date(2024, 8, 1) else 360  # cf. issue #38
        # TODO: exclude clients seulement alarme et en résiliation (#20)
        # Par API ?
        return super().get_queryset(
            base_qs=Client.actifs.all().en_age_avs().exclude(
                Q(date_deces__isnull=False) | Q(fonds_transport=True)
            ).annotate(
                last_quest=Max('questionnaire__created')
            ).filter(
                Q(last_quest__isnull=True) |
                Q(last_quest__date__lt=date.today() - timedelta(days=delai_entre_quest))
            ).order_by(F('last_quest').asc(nulls_first=True), 'nom')
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cette_annee = date.today().year
        context.update({
            'en_cours': Questionnaire.objects.filter(
                termine_le__isnull=True
            ).select_related('client').order_by('client__nom'),
            'cette_annee': cette_annee,
            'quests_restant': context['paginator'].count,
        })
        return context


class ClientListView(ClientListViewBase):
    template_name = 'besoins/client_list.html'
    return_all_if_unbound = False
    client_types = ['alarme', 'transport']

    def get_queryset(self):
        return super().get_queryset().en_age_avs().annotate(
            last_quest=Max('questionnaire__created')
        ).order_by('nom', 'prenom')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'base_template': "besoins/base.html",
            'home_url': reverse('home'),
            'client_url_name': client_url_name(),
        }


class QuestionnaireView(PermissionRequiredMixin, UpdateView):
    permission_required = 'besoins.change_questionnaire'
    template_name = 'besoins/q-intro.html'
    model = Questionnaire
    pk_url_kwarg = 'qpk'
    section = None
    forms = {
        'intro': forms.IntroForm,
        'environnement': forms.EnvironnementForm,
        'logement': forms.LogementForm,
        'sante': forms.SanteForm,
        'socioeco': forms.SocioEcoForm,
        'fin': forms.FinForm,
    }

    def dispatch(self, *args, **kwargs):
        self.section_key = self.kwargs['section']
        self.section = Questionnaire.structure.get(self.section_key)
        form_keys = list(self.forms.keys())
        if self.section_key == form_keys[0]:
            self.previous_section_key = None
        else:
            self.previous_section_key = form_keys[form_keys.index(self.section_key) - 1]
        if self.section_key == form_keys[-1]:
            self.next_section_key = None
        else:
            self.next_section_key = form_keys[form_keys.index(self.section_key) + 1]
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().dispatch(*args, **kwargs)

    def get_template_names(self):
        return ['besoins/q-intro.html'] if self.section_key == 'intro' else ['besoins/q-generic.html']

    def get_object(self):
        if 'qpk' not in self.kwargs:
            # Si le questionneur revient sur quest/intro avec le Back du navigateur et
            # resoumet, il faut reprendre ici le questionnaire en cours, le cas échéant
            try:
                return self.client.questionnaire_set.filter(termine_le__isnull=True).first()
            except Questionnaire.DoesNotExist:
                return None
        return super().get_object()

    def get_form_class(self):
        return self.forms[self.section_key]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'client': self.client,
            'client_url_name': client_url_name(),
            'previous': (self.previous_section_key, Questionnaire.structure[self.previous_section_key].theme if self.previous_section_key else ''),
            'next': (self.next_section_key, Questionnaire.structure[self.next_section_key].theme if self.next_section_key else ''),
            'themes_status': {
                key: {'label': data.theme, 'done': self.object.theme_ok(key) if self.object else False}
                for key, data in Questionnaire.structure.items()
            },
        })
        try:
            context['prev_quest'] = self.client.questionnaire_set.filter(termine_le__isnull=False).latest('termine_le')
        except Questionnaire.DoesNotExist:
            context['prev_quest'] = None
        context.update(self.client.contacts_as_context())
        return context

    def terminer(self, quest):
        quest.termine_le = timezone.now()
        msg = f"Le questionnaire pour {quest.client} a bien été terminé."
        with transaction.atomic():
            quest.save(update_fields=['termine_le'])
            Journal.objects.create(
                client=quest.client, description="Questionnaire des besoins rempli.",
                quand=timezone.now(), qui=self.request.user,
            )
            if quest.interet_alarme:
                if 'alarme' in quest.client.type_client:
                    raise ValueError("Le client est déjà client de l’alarme")
                quest.client.type_client.append('alarme')
                quest.client.save()
                Journal.objects.create(
                    client=quest.client,
                    description="Intérêt signifié pour le service alarme à la suite du questionnaire des besoins.",
                    quand=timezone.now(), qui=self.request.user,
                )
                msg += " La personne a été signalée à l'équipe alarme."
            if quest.message_services:
                for app in quest.client.type_client:
                    if app == current_app():
                        continue
                    Alerte.objects.create(
                        client=quest.client, cible=app,
                        alerte=f"Info suite au questionnaire des besoins: «{quest.message_services}»",
                        recu_le=timezone.now(), par=self.request.user,
                    )
        messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('besoins:home'))

    def non_repondu(self, quest):
        quest.termine_le = timezone.now()
        msg = f"Le questionnaire pour {quest.client} a été marqué comme «non répondu»."
        with transaction.atomic():
            quest.save(update_fields=['termine_le'])
            Journal.objects.create(
                client=quest.client,
                description="Souhait de ne pas répondre au questionnaire des besoins.",
                quand=timezone.now(), qui=self.request.user,
            )
        messages.success(self.request, msg)
        return HttpResponseRedirect(reverse('besoins:home'))

    def form_valid(self, form):
        if not form.instance.pk:
            form.instance.client = self.client
            form.instance.collab = self.request.user
        quest = form.save()
        if self.section_key == 'fin' and self.request.GET.get('termine') == '1' and quest.quest_ok():
            return self.terminer(quest)
        if self.section_key == 'intro' and quest.non_repondu:
            return self.non_repondu(quest)

        if self.request.headers.get('x-requested-with') == 'fetch':
            return JsonResponse({'result': 'OK'})
        return HttpResponseRedirect(reverse('besoins:questionnaire', args=[
            self.client.pk, form.instance.pk,
            self.previous_section_key if self.request.GET.get('back') == '1' else self.next_section_key
        ]))


class QuestionnaireResumeView(ClientAccessCheckMixin, TemplateView):
    template_name = 'besoins/onglet_client.html'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
            'questionnaires': self.client.questionnaire_set.order_by('-created'),
        }


class QuestNonRepondusView(ListView):
    model = Questionnaire
    template_name = 'besoins/non_repondus.html'

    def get(self, request, *args, **kwargs):
        self.year = self.request.GET.get('year', date.today().year)
        self.month = self.request.GET.get('month', date.today().month)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return Questionnaire.objects.filter(
            termine_le__year=self.year, termine_le__month=self.month, non_repondu=True
        )


class StatistiquesView(TemplateView):
    template_name = 'besoins/stats.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        annee_form = AnneeForm(self.request.GET or {'year': date.today().year})
        context['date_form'] = annee_form
        if not annee_form.is_valid():
            return context
        year = int(annee_form.cleaned_data['year'])
        query = Questionnaire.objects.filter(termine_le__year=year).annotate(
            month=TruncMonth('termine_le')
        ).values('month').annotate(
            num_alarme=Count('id', filter=Q(
                non_repondu=False, client__type_client__contains=[ClientType.ALARME]
            )),
            num_transport=Count('id', filter=Q(
                non_repondu=False, client__type_client__contains=[ClientType.TRANSPORT]
            )),
            num_visite=Count('id', filter=Q(
                non_repondu=False, client__type_client__contains=[ClientType.VISITE]
            )),
            num_savd=Count('id', filter=Q(
                non_repondu=False, client__type_client__contains=[ClientType.SAVD]
            )),
            num_total=Count('id', filter=Q(non_repondu=False)),
            non_repondus = Count('id', filter=Q(non_repondu=True)),
        )
        keys = ['num_alarme', 'num_transport', 'num_visite', 'num_savd', 'non_repondus', 'num_total']
        totaux = {key: 0 for key in keys}
        for line in query:
            for key in keys:
                totaux[key] += line.get(key, 0)
        context.update({
            'months': [Month(year, i + 1) for i in range(12)],
            'results': {Month.from_date(line['month']): line for line in query},
            'totaux': totaux,
        })
        return context
