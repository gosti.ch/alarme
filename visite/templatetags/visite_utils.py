from datetime import timedelta

from django.template import Library

register = Library()


@register.simple_tag(takes_context=True)
def duree_par_mois(context, mois, client, visiteur):
    # total_durees: voir AttributionsView.get_context_data
    return context['total_durees'].get(f"{str(mois)}-{client}-{visiteur}", timedelta())
