from datetime import date, datetime, time, timedelta
from pathlib import Path
from time import sleep
from unittest import mock

from django.conf import settings
from django.urls import reverse
from django.utils.timezone import get_current_timezone, localtime

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from benevole.models import Benevole
from client.models import Client
from common.test_utils import LiveTestBase
from transport.models import Adresse, Transport
from .tests import DataMixin


class file_path_exists:
    def __init__(self, path):
        self.path = path

    def __call__(self, driver):
        if self.path.exists():
            return self.path
        else:
            return False


class LiveTests(DataMixin, LiveTestBase):
    def setUp(self):
        DataMixin.setUpTestData()
        # TODO: try to detect date format from browser
        self.date_fmt = '%d/%m/%Y' if self.driver == 'chromium' else '%d.%m.%Y'

    def _saisie(self, nom_client, date_):
        """Connexion, URL de saisie, saisie client et date."""
        self.login()
        self.goto(reverse('transport-saisie'))
        self.assertFalse(
            self.selenium.find_element(By.ID, value='fonds_transport').is_displayed(),
        )
        self.selenium.find_element(By.ID, value='id_client_select').send_keys(nom_client)
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "dropdown-item"))
        )
        self.selenium.find_element(By.XPATH, value='//button[@class="dropdown-item"]').click()
        self.selenium.find_element(By.ID, value='id_date').send_keys(date_.strftime(self.date_fmt))

    def _select_adr(self, id_, text):
        select = self.selenium.find_element(By.ID, id_)
        select.send_keys(text)
        menu = WebDriverWait(self.selenium, 2).until(EC.visibility_of(
            select.parent.find_element(By.CSS_SELECTOR, '.dropdown-menu.show')
        ))
        menu.find_element(By.CSS_SELECTOR, 'button.dropdown-item').click()

    @mock.patch('transport.views.distance_real')
    def test_saisie_transport(self, mocked):
        mocked.return_value = {'duration': 1550}
        client = Client.objects.create(
            nom='Bronzé', prenom='Léa', no_debiteur=999, type_client=['transport'],
            rue='Rue des Crêtets 92', npa='2300', localite='La Chaux-de-Fonds',
            date_naissance=date(1950, 5, 23),
            empl_geo=[6.82058572769165, 47.093910217285156],
            fonds_transport=True,
            remarques_ext="Aller la chercher au 4e",
        )
        self._saisie('Bronze', date.today())
        self.assertEqual(
            self.selenium.find_element(By.ID, value='adresse_depart').text,
            "Rue des Crêtets 92\n2300 La Chaux-de-Fonds"
        )
        self.assertEqual(
            self.selenium.find_element(By.ID, value='adresse_retour').text,
            "Rue des Crêtets 92\n2300 La Chaux-de-Fonds"
        )
        self.assertEqual(
            self.selenium.find_element(By.ID, value='remarques_client').text,
            "Aller la chercher au 4e"
        )
        self.assertTrue(
            self.selenium.find_element(By.ID, value='fonds_transport').is_displayed(),
        )
        heure_dep = self.selenium.find_element(By.ID, value='id_heure_depart')
        self.assertFalse(heure_dep.is_enabled())
        self.selenium.find_element(By.ID, value='id_heure_rdv').send_keys('13:40')
        self._select_adr('id_trajets-0-destination_adr_select', 'hop')
        # L'heure de départ est calculée:
        WebDriverWait(self.selenium, 2).until(EC.element_to_be_clickable(heure_dep))
        self.assertTrue(heure_dep.is_enabled())
        self.assertEqual(heure_dep.get_attribute('value'), '13:05')
        self.selenium.find_element(By.ID, value='id_heure_rdv').send_keys('13:45')
        sleep(0.4)
        self.assertEqual(heure_dep.get_attribute('value'), '13:10')

        self.selenium.find_element(By.ID, value='id_duree_rdv').send_keys('00:45')
        WebDriverWait(self.selenium, 2).until(EC.text_to_be_present_in_element((By.ID, 'depart_retour'), '14:30'))
        self.assertEqual(self.selenium.find_element(By.ID, value='arrivee_retour').text, '14:55')
        with mock.patch('transport.models.distance_real') as real_mocked:
            real_mocked.return_value = {'distance': 20701.5, 'duration': 1525.1}
            self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
            WebDriverWait(self.selenium, 2).until(EC.url_to_be(
                f"{self.live_server_url}{reverse('transport-client-detail', args=[client.pk])}"
            ))
        transp = client.transports.first()
        self.assertEqual(transp.date, date.today())
        self.assertEqual(transp.trajets.count(), 2)

    @mock.patch('transport.models.distance_real')
    @mock.patch('transport.views.distance_real')
    def test_saisie_retour_domicile(self, mocked1, mocked2):
        mocked1.return_value = {'duration': 1550, 'distance': 20701}
        mocked2.return_value = {'duration': 1550, 'distance': 20701}

        self._saisie(self.trclient.nom, date.today())
        self.selenium.find_element(By.ID, 'edit_depart').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, 'id_origine_adr_select'))
        )
        self._select_adr('id_origine_adr_select', 'Hopi')
        self._select_adr('id_trajets-0-destination_adr_select', 'Domicile')
        self.selenium.find_element(By.ID, value='id_heure_rdv').send_keys('13:40')
        # L'heure de départ est calculée:
        heure_dep = self.selenium.find_element(By.ID, value='id_heure_depart')
        WebDriverWait(self.selenium, 2).until(EC.element_to_be_clickable(heure_dep))
        self.assertTrue(heure_dep.is_enabled())
        self.assertEqual(heure_dep.get_attribute('value'), '13:40')
        self.selenium.find_element(By.XPATH, '//label[@for="id_retour_1"]').click()
        self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('transport-client-detail', args=[self.trclient.pk])}"
        ))
        transp = self.trclient.transports.first()
        self.assertEqual(transp.date, date.today())
        self.assertEqual(transp.trajets.count(), 1)

    @mock.patch('transport.models.distance_real')
    @mock.patch('transport.views.distance_real')
    def test_saisie_transport_intermediaire(self, mocked1, mocked2):
        mocked1.return_value = {'duration': 1550, 'distance': 20701}
        mocked2.return_value = {'duration': 1550, 'distance': 20701}
        Adresse.objects.create(
            nom='Pharmacie', rue='Rue Frédéric Soguel 4', npa='2053', localite='Cernier',
            empl_geo=[6.90133, 47.05917]
        )

        self._saisie(self.trclient.nom, date.today())
        ajout1, ajout2 = self.selenium.find_elements(By.CLASS_NAME, 'ajout_destination')
        ajout1.click()
        self._select_adr('id_trajets-2-destination_adr_select', 'pharma')
        self.selenium.find_element(By.ID, value='id_heure_rdv').send_keys('13:40')
        self.selenium.find_element(By.ID, value='id_duree_rdv').send_keys('00:45')
        self._select_adr('id_trajets-0-destination_adr_select', 'hopi')
        # L'heure de départ est calculée:
        heure_dep = self.selenium.find_element(By.ID, value='id_heure_depart')
        WebDriverWait(self.selenium, 2).until(EC.element_to_be_clickable(heure_dep))
        self.assertTrue(heure_dep.is_enabled())
        self.assertEqual(heure_dep.get_attribute('value'), '12:40')

        ajout2.click()
        self._select_adr('id_trajets-3-destination_adr_select', 'pharma')
        self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('transport-client-detail', args=[self.trclient.pk])}"
        ))

        transp = self.trclient.transports.first()
        self.assertEqual(transp.date, date.today())
        self.assertEqual(transp.trajets.count(), 4)
        self.assertEqual(
            [tra.destination_princ for tra in transp.trajets_tries],
            [False, True, False, False]
        )

    @mock.patch('transport.models.distance_real')
    def test_suppression_transport_intermediaire(self, mocked1):
        mocked1.return_value = {'duration': 1550, 'distance': 20701}
        adr_interm = Adresse.objects.create(
            nom='Pharmacie', rue='Rue Frédéric Soguel 4', npa='2053', localite='Cernier',
            empl_geo=[6.90133, 47.05917]
        )
        transport = self.create_transport(
            client=self.trclient, date=self.demain_1130.date(), heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True,
            adr_interm_retour=adr_interm,
        )
        self.assertEqual(len(transport.trajets_tries), 3)
        self.login()
        self.goto(reverse('transport-edit', args=[transport.pk]))
        self.selenium.find_element(By.CLASS_NAME, 'enlever-ligne').click()
        self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('gestion')}"
        ))
        transport = Transport.objects.get(pk=transport.pk)
        self.assertEqual(len(transport.trajets_tries), 2)

    @mock.patch('transport.models.distance_real')
    @mock.patch('transport.views.distance_real')
    def test_saisie_arrivee_autre(self, mocked1, mocked2):
        mocked1.return_value = {'duration': 1550, 'distance': 20701}
        mocked2.return_value = {'duration': 1550, 'distance': 20701}
        pharma = Adresse.objects.create(
            nom='Pharmacie', rue='Rue Frédéric Soguel 4', npa='2053', localite='Cernier',
            empl_geo=[6.90133, 47.05917]
        )
        self._saisie(self.trclient.nom, date.today())
        self._select_adr('id_trajets-0-destination_adr_select', 'Hopi')
        self.selenium.find_element(By.ID, value='id_heure_rdv').send_keys('13:40')
        self.selenium.find_element(By.ID, value='id_duree_rdv').send_keys('00:45')
        self.selenium.find_element(By.ID, 'edit_arrivee').click()
        WebDriverWait(self.selenium, 2).until(
            EC.visibility_of_element_located((By.ID, 'id_trajets-1-destination_adr_select'))
        )
        self._select_adr('id_trajets-1-destination_adr_select', 'pharma')
        self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('transport-client-detail', args=[self.trclient.pk])}"
        ))
        transp = self.trclient.transports.first()
        self.assertEqual(transp.trajets_tries[1].origine_adr, self.hne_ne)
        self.assertEqual(transp.trajets_tries[1].destination_adr, pharma)

    def test_edit_transport(self):
        transport = self.create_transport(
            client=self.trclient, date=self.demain_1130.date(), heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=True
        )
        self.login()
        self.goto(reverse('transport-edit', args=[transport.pk]))
        self.assertEqual(self.selenium.find_element(By.ID, 'depart_retour').text, '12:40')
        self.assertEqual(self.selenium.find_element(By.ID, 'arrivee_retour').text, '12:56')
        self.selenium.find_element(By.ID, 'id_date').clear()
        self.selenium.find_element(By.ID, 'id_date').send_keys(date.today().strftime(self.date_fmt))
        self.selenium.find_element(By.ID, 'id_duree_rdv').clear()
        self.selenium.find_element(By.ID, 'id_duree_rdv').send_keys("01:00")
        WebDriverWait(self.selenium, 2).until(EC.text_to_be_present_in_element(
            (By.ID, 'arrivee_retour'), '12:46')
        )
        with mock.patch('transport.models.distance_real') as real_mocked:
            real_mocked.return_value = {'distance': 20701.5, 'duration': 1525.1}
            self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('gestion')}"
        ))
        WebDriverWait(self.selenium, 2).until(EC.presence_of_element_located((By.ID, 'statutTabs')))
        transport.refresh_from_db()
        self.assertEqual(transport.date, date.today())
        self.assertTrue(transport.trajets_tries[0].destination_princ)
        self.assertEqual(localtime(transport.trajets_tries[1].heure_depart).time(), time(12, 30))

    def test_edit_transport_aller(self):
        transport = self.create_transport(
            client=self.trclient, date=self.demain_1130.date(), heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=False
        )
        self.login()
        self.goto(reverse('transport-edit', args=[transport.pk]))
        with mock.patch('transport.models.distance_real') as real_mocked:
            real_mocked.return_value = {'distance': 20701.5, 'duration': 1525.1}
            self.selenium.find_element(By.ID, 'id_heure_rdv').send_keys('14:00')
            self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('gestion')}"
        ))
        WebDriverWait(self.selenium, 2).until(EC.presence_of_element_located((By.ID, 'statutTabs')))
        transport.refresh_from_db()
        self.assertEqual(localtime(transport.heure_rdv).time(), time(14, 0))

    def test_edit_transport_aller_en_aller_retour(self):
        transport = self.create_transport(
            client=self.trclient, date=self.demain_1130.date(), heure_rdv=self.demain_1130,
            duree_rdv=timedelta(minutes=70),
            destination=self.hne_ne, retour=False
        )
        self.login()
        self.goto(reverse('transport-edit', args=[transport.pk]))
        with mock.patch('transport.models.distance_real') as real_mocked:
            real_mocked.return_value = {'distance': 20701.5, 'duration': 1525.1}
            self.selenium.find_element(By.XPATH, '//label[@for="id_retour_0"]').click()
            self.selenium.find_element(By.ID, 'id_duree_rdv').send_keys('01:00')
            self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('gestion')}"
        ))
        WebDriverWait(self.selenium, 2).until(EC.presence_of_element_located((By.ID, 'statutTabs')))
        transport.refresh_from_db()
        self.assertTrue(transport.retour)
        self.assertEqual(len(transport.trajets_tries), 2)
        self.assertTrue(transport.trajets_tries[1].destination_domicile)

    def test_edit_transport_retour_domicile(self):
        transport = self.create_transport(
            client=self.trclient, date=self.demain_1130.date(), heure_rdv=self.demain_1130,
            origine=self.hne_ne, retour=False
        )
        self.login()
        self.goto(reverse('transport-edit', args=[transport.pk]))
        with mock.patch('transport.models.distance_real') as real_mocked:
            real_mocked.return_value = {'distance': 20701.5, 'duration': 1525.1}
            self.selenium.find_element(By.ID, 'id_heure_rdv').send_keys('14:30')
            self.find_and_scroll_to(By.XPATH, '//button[@type="submit"]').click()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            f"{self.live_server_url}{reverse('gestion')}"
        ))
        transport.refresh_from_db()
        self.assertEqual(localtime(transport.heure_rdv).time(), time(14, 30))
        self.assertEqual(transport.origine, self.hne_ne)
        self.assertEqual(transport.destination.rue, self.trclient.rue)

    def test_validation_transport_rapporte(self):
        hier1130 = datetime.combine(date.today() + timedelta(days=1), time(11, 30), tzinfo=get_current_timezone())
        chauffeur = Benevole.objects.create(
            nom="Dupond", prenom="Lily3", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur, date=hier1130.date(), heure_rdv=hier1130,
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.RAPPORTE,
            km=12, duree_eff=timedelta(hours=1),
        )
        avhier1130 = hier1130 - timedelta(days=1)
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur, date=avhier1130.date(), heure_rdv=avhier1130,
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.RAPPORTE,
            km=12, duree_eff=timedelta(hours=1),
        )
        self.login()
        self.goto(reverse('gestion-with-tab', args=[Transport.StatutChoices.RAPPORTE]))
        form = WebDriverWait(self.selenium, 2).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'validate_transport'))
        )
        form.find_element(By.CLASS_NAME, 'icon').click()
        # Développer jour suivant et valider
        self.selenium.find_elements(By.CSS_SELECTOR, '.transp_group_details')[-1].click()
        WebDriverWait(self.selenium, 2).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'validate_transport'))
        )
        sleep(0.4)
        self.selenium.find_element(By.CLASS_NAME, 'validate_transport').find_element(By.CLASS_NAME, 'icon').click()
        WebDriverWait(self.selenium, 3).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, 'validate_transport'))
        )
        self.assertIn('<span id="badge5" class="badge bg-secondary">0</span>', self.selenium.page_source)
        self.assertIn('<span id="badge6" class="badge bg-secondary">2</span>', self.selenium.page_source)

    def test_archive_client(self):
        self.login()
        self.goto(reverse('transport-client-edit', args=[self.trclient.pk]))
        self.find_and_scroll_to(By.CLASS_NAME, "btn-danger").click()
        self.selenium.switch_to.alert.accept()
        WebDriverWait(self.selenium, 2).until(EC.url_to_be(
            '%s%s' % (self.live_server_url, reverse('clients'))
        ))
        self.trclient.refresh_from_db()
        self.assertEqual(self.trclient.archive_le, date.today())

    def test_facturer_transports(self):
        moispasse = datetime.combine(
            date.today().replace(day=1) - timedelta(days=5), time(11, 30), tzinfo=get_current_timezone()
        )
        chauffeur = Benevole.objects.create(
            nom="Dupond", prenom="Lily3", activites=['transport'], empl_geo=[6.82, 47.087]  # CdF
        )
        self.create_transport(
            client=self.trclient, chauffeur=chauffeur, date=moispasse.date(), heure_rdv=moispasse,
            origine=self.hne_ne, retour=False, statut=Transport.StatutChoices.CONTROLE,
            km=12, duree_eff=timedelta(hours=1),
        )
        self.login()
        self.goto(reverse('finances'))
        self.selenium.find_element(By.CSS_SELECTOR, 'button[title="Facturer les transports"').click()
        save_btn = WebDriverWait(self.selenium, 2).until(
            EC.presence_of_element_located((By.ID, 'modal-save'))
        )
        sleep(0.2)
        save_btn.click()
        WebDriverWait(self.selenium, 2).until(file_path_exists(Path(
            settings.MEDIA_ROOT, 'transports', 'factures',
            f'fact_{self.trclient.pk}_{moispasse.year}_{moispasse.month}.pdf'
        )))
