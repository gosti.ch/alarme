from django.apps import apps
from django.urls import include, path

from visite import views

handler500 = 'common.views.error_view'

urlpatterns = [
    path('benevoles/visite/', views.BenevoleListView.as_view(), {'app': 'visite'}, name='benevoles'),
    path('benevoles/visite/<int:pk>/edition/', views.BenevoleEditView.as_view(is_create=False), {'app': 'visite'},
        name='benevole-edit'),

    path('', include('common.urls')),
    path('', include('client.urls')),

    path('', views.home, name='home'),
    path('clients/', views.ClientListView.as_view(), name='clients'),
    path('clients/archives/', views.ClientListView.as_view(is_archive=True), name='clients-archives'),
    path('clients/new/', views.ClientEditView.as_view(is_create=True), name='client-new'),
    path('clients/<int:pk>/edition/', views.ClientEditView.as_view(is_create=False), name='client-edit'),
    path('clients/attente/', views.ClientsAttenteView.as_view(), name='clients-attente'),
    path('attributions/nouvelle/', views.AttributionEditView.as_view(is_create=True), name='attribution-new'),
    path('attributions/<int:pk>/edition/', views.AttributionEditView.as_view(is_create=False),
        name='attribution-edit'),
    path('attributions/<str:status>/', views.AttributionsView.as_view(), name='attributions'),
    path('visites/', views.VisitesView.as_view(), name='visites'),
    path('visites/nouvelle/', views.VisiteEditView.as_view(is_create=True), name='visite-new'),
    path('visites/<int:pk>/edition/', views.VisiteEditView.as_view(is_create=False),
        name='visite-edit'),
    path('visites/<int:pk>/supprimer/', views.VisiteDeleteView.as_view(), name='visite-delete'),
]

if apps.is_installed('besoins'):
    urlpatterns.append(
        path('besoins/', include('besoins.urls_app'))
    )
