import csv
from datetime import datetime
from pathlib import Path

from django.core.management.base import BaseCommand, CommandError

from alarme.models import Client


class Command(BaseCommand):
    """A command to read az ERP exports and copy debtor numbers to this DB."""
    def add_arguments(self, parser):
        parser.add_argument('path')

    def handle(self, **options):
        file_path = Path(options['path'])
        if not file_path.exists():
            raise CommandError(f"File {file_path} does not exist.")
        trouves = 0
        with file_path.open(encoding='latin-1') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                # Try to match a Client
                if row['AZCLDNAI']:
                    dt_naiss = row['AZCLDNAI'].replace(' 00:00', '')
                    dt_naiss = datetime.strptime(dt_naiss, '%d.%m.%Y').date()
                    possibles = Client.objects.filter(
                        nom__istartswith=row['AZCLNOMS'].split()[0],
                        date_naissance=dt_naiss
                    )
                else:
                    possibles = Client.objects.filter(
                        nom__istartswith=row['AZCLNOMS'].split()[0],
                        rue=row['AZCLADRE']
                    )
                if len(possibles) == 0:
                    possibles = Client.objects.filter(
                        nom=row['AZCLNOMS'].split()[0],
                        prenom=row['AZCLNOMS'].split()[1],
                    )
                if len(possibles) == 0:
                    self.stderr.write(f"Aucun client trouvé pour {row['AZCLNOMS']}")
                elif len(possibles) > 1:
                    self.stderr.write(f"Plusieurs clients trouvés pour {row['AZCLNOMS']}")
                else:
                    client = possibles[0]
                    client.no_debiteur = row['AZCLCODE'].replace("'", '')
                    client.save()
                    trouves += 1
        print(f'{trouves} clients trouvés dans la base de données')
