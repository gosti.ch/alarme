from datetime import date
from random import randint

import factory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice, FuzzyInteger
from faker import Faker

from common.distance import wgs84_to_mn95
from . import models

faker = Faker(locale='fr_CH')


class PhoneFaker(factory.Faker):
    def evaluate(self, instance, step, extra):
        result = super().evaluate(instance, step, extra)
        if self.provider == 'phone_number' and '+41' in result:
            result = result.replace('+41 (0)', '0').replace('+41', '0')
        return result


class ReferentFactory(DjangoModelFactory):
    class Meta:
        model = models.Referent

    nom = factory.Faker('last_name')
    prenom = factory.Faker('first_name')
    npa = factory.Faker('postcode')
    localite = factory.Faker('city')
    repondant = factory.Iterator([1, 2, 3, 4])


class ClientFactory(DjangoModelFactory):
    class Meta:
        model = models.Client

    nom = factory.Faker('last_name')
    prenom = factory.Faker('first_name')
    type_client = FuzzyChoice(models.ClientType.choices)
    date_naissance = factory.Faker('date_between', start_date=date(1930, 1, 1), end_date=date(1960, 12, 31))
    npa = factory.Faker('postcode')
    localite = factory.Faker('city')
    tel_1 = PhoneFaker('phone_number')
    referents = factory.RelatedFactoryList(
        ReferentFactory, factory_related_name='client', size=lambda: randint(1, 4)
    )


class ClientAlarmeFactory(ClientFactory):
    type_client = ['alarme']


class ClientTransportFactory(ClientFactory):
    type_client = ['transport']
    empl_geo = factory.LazyAttribute(
        # FIXME: looks like local_latlng only iterates between 2 values.
        lambda o: list(wgs84_to_mn95(*reversed(faker.local_latlng('CH', coords_only=True))).coords)
    )
