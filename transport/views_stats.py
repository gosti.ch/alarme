from datetime import timedelta

from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import BooleanField, Count, F, Q, Sum
from django.db.models.expressions import Func
from django.db.models.functions import Trunc
from django.views.generic import TemplateView

from common.stat_utils import Month, StatsMixin

from .models import Facture, Trajet, Transport


class IsOFAS(Func):
    """
    Paramètres: id transport, date_transport, tarif_avs_acquis, date_naissance, genre
    Return True si:
        categ transport est médical ou participatif, client.tarif_avs_acquis ou client en âge AVS
    """
    function = "is_ofas"
    arity = 5
    output_field = BooleanField()


class StatsTransportsView(StatsMixin, TemplateView):
    template_name = 'transport/stats/transports.html'
    active = 'transports'
    stat_items = {
        'clients': {'label': 'Nbre de clients uniques'},
        'transports': {'label': 'Transports effectués'},
        'allers': {'label': 'Allers simples'},
        'allers_retours': {'label': 'Allers-retours'},
        'km_chauffeurs': {'label': 'Km effectués'},
        'frais_chauffeurs': {'label': 'Frais des chauffeurs'},
        'duree_chauffeurs': {'label': 'Durée effective des chauffeurs'},
        'total_factures': {'label': 'Total facturé aux clients (CHF)'},
    }
    categ_keys = [str(tp) for tp in Trajet.Types]

    def get_base_qs(self, start, end):
        return Transport.objects.filter(date__range=(start, end)).annotate(
            month=Trunc('date', 'month'),
        )

    def get_stats(self, months):
        months = [m for m in months if not m.is_future()]
        start, _ = self.month_limits(months[0])
        _, end = self.month_limits(months[-1])
        base_qs = self.get_base_qs(start, end)
        stats = base_qs.values('month').annotate(
            count_tr=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE)),
            count_tr_al=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=False)),
            count_tr_ar=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE) & Q(retour=True)),
            count_cl=Count('client', distinct=True, filter=~Q(statut=Transport.StatutChoices.ANNULE)),
            sum_km=Sum('km', filter=~Q(defrayer_chauffeur=False), default=0),
            sum_duree=Sum('duree_eff', default=timedelta()),
        )
        by_month = {
            Month(line['month'].year, line['month'].month): {
                'clients': line['count_cl'], 'transports': line['count_tr'],
                'allers': line['count_tr_al'], 'allers_retours': line['count_tr_ar'],
                'km_chauffeurs': line['sum_km'], 'duree_chauffeurs': line['sum_duree'],
                'total_factures': 0,
                'categs': {key: 0 for key in self.categ_keys},
            } for line in stats
        }
        # Le calcul avec annotations sur des tables externes doit se faire dans une requête séparée
        # pour éviter de doubler certaines annotations.
        stats_frais = base_qs.values('month').annotate(
            sum_frais=Sum('frais__cout', default=0),
        )
        for line in stats_frais:
            by_month[Month(line['month'].year, line['month'].month)]['frais_chauffeurs'] = line['sum_frais']

        stats_categ = base_qs.annotate(
            type_traj=ArrayAgg('trajets__typ', distinct=True)
        ).values('month', 'trajets__typ').annotate(
            count_tr=Count('pk', filter=~Q(statut=Transport.StatutChoices.ANNULE), distinct=True)
        )
        for line in stats_categ:
            mkey = Month(line['month'].year, line['month'].month)
            by_month[mkey]['categs'][line['trajets__typ']] = line['count_tr']

        stats_factures = Facture.objects.filter(
            mois_facture__range=(start, end), annulee=False
        ).values('mois_facture').annotate(
            sum_facts=Sum('montant_total'),
        )
        for line in stats_factures:
            mkey = Month(line['mois_facture'].year, line['mois_facture'].month)
            by_month[mkey]['total_factures'] = line['sum_facts']

        counters = self.init_counters(list(self.stat_items.keys()) + self.categ_keys, months)
        for month in months:
            if month not in by_month:
                continue
            for key in self.stat_items.keys():
                counters[key][month] += by_month[month][key]
                if key != 'clients':
                    counters[key]['total'] += by_month[month][key]
            for key in self.categ_keys:
                counters[key][month] += by_month[month]['categs'][key]
                counters[key]['total'] += by_month[month]['categs'][key]

        return {
            'labels': list(self.stat_items.values()),
            'stats': {key: counters[key] for key in list(self.stat_items.keys()) + self.categ_keys},
        }

    def export_lines(self, context):
        months = context['months']
        yield ['BOLD', 'Statistique transports'] + [str(month) for month in months] + ['Total']
        for key, data in self.stat_items.items():
            yield (
                [data['label']] +
                [context['stats'][key][month] for month in months] +
                [context['stats'][key]['total']]
            )
            if key == 'allers_retours':
                for label, key in [(tp.label, str(tp)) for tp in Trajet.Types]:
                    yield (
                        [label] + [context['stats'][key][month] for month in months] +
                        [context['stats'][key]['total']]
                    )


class StatsOFASTransportsView(StatsTransportsView):
    active = 'transports-ofas'

    def get_base_qs(self, start, end):
        return Transport.objects.annotate(
            month=Trunc('date', 'month'),
            is_ofas=IsOFAS(
                F('id'), F('date'), F('client__tarif_avs_acquis'), F('client__date_naissance'), F('client__genre')
            ),
        ).filter(is_ofas=True, date__range=(start, end))
