from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("client", "__first__"),
        ("transport", "0030_permission_gestion_finances"),
    ]

    operations = [
        migrations.CreateModel(
            name="Facture",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("mois", models.DateField(verbose_name="Mois facturé")),
                ("no", models.BigIntegerField(verbose_name="N° de facture")),
                ("date_facture", models.DateField(verbose_name="Date de facture")),
                (
                    "montant_total",
                    models.DecimalField(
                        decimal_places=2, max_digits=6, verbose_name="Montant total"
                    ),
                ),
                (
                    "fichier_pdf",
                    models.FileField(
                        upload_to="transports/factures", verbose_name="Facture PDF"
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=models.deletion.CASCADE,
                        related_name="factures_transports",
                        to="client.client",
                    ),
                ),
            ],
        ),
    ]
