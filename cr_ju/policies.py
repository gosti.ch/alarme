from dateutil import relativedelta

from django.utils.dateformat import format as django_format

from alarme.models import (
    ArticleFacture, Facture, FacturationPolicyBase, MaterielClient,
)


class FacturationPolicy(FacturationPolicyBase):

    def creer_facture_installation(self, install, date_factures):
        factures = super().creer_facture_installation(install, date_factures)
        # Si boîtier C-R, changer d'article et de montant
        if install.client.boitier_cle == 'c-r':
            article, _ = ArticleFacture.objects.get_or_create(code='TAXE2', defaults={
                'designation': "Frais d'installation et boîtier à clés", 'prix': 170, 'compte': '3410/00'
            })
            factures[-1].article = article
            factures[-1].libelle = article.designation
            factures[-1].montant = article.prix
            factures[-1].save()
        return factures

    def creer_facture_mensuelle(self, mois, installs, date_factures):
        factures = super().creer_facture_mensuelle(mois, installs, date_factures)
        un_du_mois = mois.replace(day=1)
        install1 = installs[0]
        client = install1.client
        pour_materiel = isinstance(install1, MaterielClient)
        if pour_materiel:
            libelle = f'{install1.abonnement.nom} - %s'
        else:
            libelle = 'Service d’alarme à domicile - %s'
        # Create factures for next months, if needed
        for i in range(4):
            nextmonth = un_du_mois + relativedelta.relativedelta(months=i + 1)
            if (
                client is not None and nextmonth.month in (1, 4, 7, 10) and
                not Facture.objects.filter(mois_facture=nextmonth).exists()
            ):
                # Only bill for next quarter if there are already bills for the month
                break
            if client.has_facture_for_month(nextmonth, install1.abonnement.article):
                continue
            # Check if abo ends
            duree_installations = sum(install.duree_pour_mois(nextmonth) for install in installs)
            if duree_installations < 15:
                break
            montant = install1.abonnement.article.prix
            fact = Facture.objects.create(
                client=client, date_facture=nextmonth, mois_facture=nextmonth,
                install=install1 if not pour_materiel else None,
                article=install1.abonnement.article,
                materiel=install1 if pour_materiel else None,
                libelle=libelle % django_format(nextmonth, "F Y"),
                montant=montant, exporte=None
            )
            factures.append(fact)
        return factures
