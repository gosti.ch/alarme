from operator import attrgetter
from django.db import migrations


def migrate_trajets(apps, schema_editor):
    Transport = apps.get_model('transport', 'Transport')
    for transp in Transport.objects.all():
        trajet = sorted(list(transp.trajets.all()), key=attrgetter('heure_depart'))[0]
        trajet.destination_princ = True
        trajet.save()


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0019_trajet_destination_princ"),
    ]

    operations = [migrations.RunPython(migrate_trajets)]
