from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("client", "__first__"),
        ("transport", "0007_transport_index_statut"),
    ]

    operations = [
        migrations.CreateModel(
            name="TransportModel",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "heure_rdv",
                    models.DateTimeField(verbose_name="Heure de rendez-vous"),
                ),
                (
                    "duree_rdv",
                    models.DurationField(verbose_name="Durée du rendez-vous"),
                ),
                ("retour", models.BooleanField(default=True)),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "suspension_depuis",
                    models.DateField(
                        blank=True, null=True, verbose_name="Suspension dès le"
                    ),
                ),
                (
                    "fin_recurrence",
                    models.DateTimeField(
                        blank=True,
                        db_index=True,
                        null=True,
                        verbose_name="Dernière occurrence",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="models",
                        to="client.client",
                    ),
                ),
                (
                    "regle",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="transport.regle",
                        verbose_name="Règle",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="transport",
            name="modele",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="transports",
                to="transport.transportmodel",
            ),
        ),
    ]
