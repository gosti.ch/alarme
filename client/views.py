from datetime import date

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Count, Max, Q
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.dateformat import format as django_format
from django.views.generic import (
    DeleteView, DetailView, FormView, ListView, TemplateView, UpdateView, View
)

from common.distance import distance_real, distance_vo
from common.forms import FichierForm
from common.models import Fichier, TypeFichier
from common.utils import current_app
from common.views import (
    CreateUpdateView, ExportableMixin, FilterFormMixin, GeoAddressMixin, JournalMixin
)
from .forms import (
    AdresseForm, AdressePresenceSuiviForm, AlerteForm, ClientFilterFormBase, DateArchiveForm,
    JournalForm, ProfessionnelClientForm, ProfessionnelClientEditForm,
    ProfessionnelForm, ProfessionnelSelectForm, ProfFilterForm, ReferentForm
)
from .models import (
    AdresseClient, AdressePresence, Alerte, Client, ClientType, Journal,
    Professionnel, ProfClient, Referent
)


class ClientAccessCheckMixin:
    def dispatch(self, request, *args, **kwargs):
        if 'pk' not in self.kwargs:
            self.read_only = False
        else:
            self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
            if self.client.can_edit(request.user):
                self.read_only = False
            elif self.client.can_read(request.user):
                self.read_only = True
            else:
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour voir ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'readonly': self.read_only}


class ClientJournalMixin(JournalMixin):
    def _create_instance(self, **kwargs):
        client = self.get_client()
        if client is not None:
            Journal.objects.create(
                client=self.get_client(), **kwargs
            )


class ClientListViewBase(ExportableMixin, FilterFormMixin, ListView):
    model = Client
    template_name = 'client/client_list_base.html'
    base_template = None
    is_archive = False
    paginate_by = 25
    title = "Liste des clients"
    filter_formclass = ClientFilterFormBase
    col_widths = [15, 30, 5, 12, 26, 10, 6, 22, 15, 15, 20, 12, 12]

    def get_queryset(self):
        if self.is_archive and self.client_types == ['alarme']:
            # Renvoyer également les clients non alarme qui ont (eu) une install alarme,
            # ne sont pas archivés car encore clients transports.
            return super().get_queryset().annotate(num_install=Count('installation')).filter(
                Q(type_client__overlap=self.client_types, archive_le__isnull=False) |
                Q(~Q(type_client__contains=['alarme']) & Q(num_install__gt=0))
            ).order_by('nom', 'prenom')
        else:
            return super().get_queryset().filter(
                type_client__overlap=self.client_types, archive_le__isnull=not self.is_archive
            ).order_by('nom', 'prenom')

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': self.get_title(),
            'exportable': True,
            'base_template': context.get('base_template', self.base_template),
            'active': 'archives' if self.is_archive else 'actifs',
            'num_alertes': Alerte.objects.filter(
                cible=current_app(), traite_le__isnull=True
            ).count(),
        })
        return context

    def export_lines(self, context):
        queryset = self.get_queryset()
        export_autres_debs = queryset._hints.get('_export_autres_debiteurs', False)
        headers = [
            'BOLD', 'Secteurs', 'Nom, Prénom', 'Genre', 'Date de naissance', 'Rue', 'Case postale',
            'NPA', 'Localité', 'Tél. principal', 'Tél. secondaire', 'Courriel', 'Archivé le', 'Décès le'
        ]
        if export_autres_debs:
            headers.extend(['Débiteur (1)', 'Débiteur(2)'])
        for idx, client in enumerate(queryset):
            if idx == 0:
                avec_ref_deb = hasattr(client, 'referent_no_debiteur')
                if avec_ref_deb:
                    headers.append('Autre débit.')
                avec_date_resiliation = hasattr(client, 'date_resiliation_annot')
                if avec_date_resiliation:
                    headers.append('Date de résiliation')
                avec_modele = hasattr(client, 'modele')
                if avec_modele:
                    headers.append('Modèle appareil')
                yield headers

            line = [
                ", ".join(client.type_client), client.nom_prenom, client.genre,
                client.date_naissance, client.rue, client.case_postale,
                client.npa, client.localite, client.tel_1, client.tel_2, client.courriel,
                client.archive_le, client.date_deces
            ]
            if avec_ref_deb:
                line.append(client.referent_no_debiteur is not None and '*' or '')
            if export_autres_debs:
                def format_deb(deb):
                    if deb is None:
                        return ''
                    return f"{deb.nom_prenom}, {deb.rue_localite()} ({deb.cree_le.strftime('%d.%m.%Y')})"

                if len(client.debiteurs) == 0:
                    line.extend(['', ''])
                elif len(client.debiteurs) == 1:
                    line.extend([format_deb(client.debiteurs[0]), ''])
                else:
                    line.extend([format_deb(client.debiteurs[0]), format_deb(client.debiteurs[1])])
            if avec_date_resiliation:
                line.append(client.date_resiliation_annot)
            if avec_modele:
                line.append(client.modele)
            yield line


class ClientAlertesView(ListView):
    model = Alerte
    template_name = 'client/client_alertes.html'
    paginate_by = 25

    def get_queryset(self):
        return super().get_queryset().filter(
            cible=current_app(), traite_le__isnull=True
        ).order_by('-recu_le')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['num_alertes'] = context['object_list'].count
        return context


class ClientEditViewBase(ClientAccessCheckMixin, GeoAddressMixin, ClientJournalMixin, CreateUpdateView):
    model = Client
    form_class = None
    template_name = 'alarme/client.html'
    base_template = None
    client_type = None  # 'alarme', 'transport', 'visite', ...
    journal_add_message = "Création du client"
    journal_edit_message = "Modification du client: {fields}"

    def get_initial(self):
        if self.is_create:
            return {'langues': ['fr']}
        return super().get_initial()

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'readonly': self.read_only or (self.object and self.object.archive_le is not None),
            'user_is_benev': self.request.user.is_benevole,
        }

    def get_client(self):
        return self.object

    def get_success_url(self):
        return reverse('client-edit', args=[self.object.pk])

    def get_success_message(self, obj):
        if self.is_create:
            return "La création d’un nouveau client a réussi."
        else:
            return f"«{obj.nom} {obj.prenom}» a bien été modifié"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'base_template' not in context:
            context['base_template'] = self.base_template
        if not self.is_create:
            context.update({
                'user_is_benev': self.request.user.is_benevole,
                'adresses': self.object.autres_adresses(),
                'can_be_archived': self.object.can_be_archived(self.request.user, self.client_type),
                'professionnels': self.object.profclient_set.exclude(
                    professionnel__type_pro='Sama').select_related('professionnel'),
                'samas_count': self.object.profclient_set.filter(professionnel__type_pro='Sama').count(),
                'fichiers': self.object.fichiers_app(current_app()).order_by('-quand'),
            })
            context.update(self.object.contacts_as_context())
        return context

    def form_valid(self, form):
        with transaction.atomic():
            if self.client_type not in form.instance.type_client:
                form.instance.type_client.append(self.client_type)
                if form.instance.pk:
                    Journal.objects.create(
                        client=self.get_client(), description=f"Ajout type de client «{self.client_type}»",
                        quand=timezone.now(), qui=self.request.user
                    )
            response = super().form_valid(form)
            form.notify_other_teams(current_app())
        return response

    def journalize(self, form, **kwargs):
        super().journalize(form, **kwargs)
        if not self.is_create and 'rue' in form.changed_data:
            old_version = Client.objects.get(pk=form.instance.pk)
            journal_msg = (
                f'Ancienne adresse: {old_version.rue}, {old_version.npa} {old_version.localite}<br>'
                f'Nouvelle adresse: {form.instance.rue}, {form.instance.npa} {form.instance.localite}'
            )
            Journal.objects.create(
                client=self.get_client(), description=journal_msg,
                quand=timezone.now(), qui=self.request.user
            )


class ClientAutocompleteView(View):
    """Endpoint for autocomplete search for Client."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        if not term:
            raise Http404("Le terme de recherche est obligatoire")
        client_types = ClientType.values if kwargs['types'] == 'tous' else [kwargs['types']]
        results = [{
            'label': str(client), 'value': client.pk,
            'adresse': f"{client.rue}, {client.npa} {client.localite}",
            'types': client.type_client,
            'url': client.get_absolute_url(),
            'archive': client.archive_le is not None,
        } for client in Client.objects.filter(
                type_client__overlap=client_types,
                date_deces__isnull=True,
                nom__unaccent__icontains=term
        )[:20]]
        return JsonResponse(results, safe=False)


class ClientJournalView(ListView):
    model = Journal
    paginate_by = 20
    template_name = 'client/client_journal.html'

    def get(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.client.journaux.order_by('-quand')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'client': self.client,
        }


class ClientJournalEditView(CreateUpdateView):
    model = Journal
    form_class = JournalForm
    pk_url_kwarg = 'pk_jr'
    json_response = True

    def form_valid(self, form):
        client = Client.objects.get(pk=self.kwargs['pk'])
        form.instance.client = client
        form.instance.quand = timezone.now()
        form.instance.qui = self.request.user
        form.instance.auto = False
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientJournalDetailsView(DetailView):
    model = Journal
    pk_url_kwarg = 'pk_jr'
    template_name = 'client/journal_details.html'


class ClientAdresseEditView(ClientJournalMixin, CreateUpdateView):
    model = AdresseClient
    form_class = AdresseForm
    template_name = 'client/adresse_edit.html'
    pk_url_kwarg = 'pk_adr'
    journal_add_message = "Ajout de l’adresse «{obj}»"
    journal_edit_message = "Modification de l’adresse «{obj}»"
    json_response = True

    def get_client(self):
        client = self.object.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def journalize(self, form, add_message=None, **kwargs):
        if self.is_create and form.cleaned_data['hospitalisation']:
            depuis = form.formset.forms[0].cleaned_data.get('depuis')
            add_message = f"Ajout d’une hospitalisation (dès le {depuis.strftime('%d.%m.%Y')})"
        super().journalize(form, add_message=add_message, **kwargs)

    def form_valid(self, form):
        client = self.get_client()
        form.instance.client = client
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': '#client-addresses'})


class ClientAdresseDeleteView(DeleteView):
    model = AdresseClient
    pk_url_kwarg = 'pk_adr'

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.object.delete()
        Journal.objects.create(
            client=self.object.client, description=f"Suppression de l’adresse «{self.object}»",
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': '#client-addresses'})


class ClientAdresseListeView(ClientAccessCheckMixin, TemplateView):
    template_name = 'client/adresse_list.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'client': client,
            'adresses': client.autres_adresses(),
        }


class ReferentEditView(ClientJournalMixin, CreateUpdateView):
    model = Referent
    form_class = ReferentForm
    pk_url_kwarg = 'pk_ref'
    template_name = 'client/client_referent.html'
    journal_add_message = "Ajout du contact «{obj}»"
    journal_edit_message = "Modification du contact «{obj}» ({fields})"
    json_response = True

    def get_client(self):
        client = self.object.client if self.object else get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        client = self.get_client()
        if kwargs['instance'] is None and self.request.GET.get('copy_from'):
            # Données initiales copiées d'un autre contact existant
            copy_from = get_object_or_404(self.model, pk=self.request.GET.get('copy_from'))
            kwargs['initial']['formsets'] = [
                {'tel': tel.tel, 'priorite': tel.priorite, 'remarque': tel.remarque}
                for tel in copy_from.telephones()
            ]
            copy_from.client = None
            copy_from.pk = None
            kwargs['instance'] = copy_from
        return {
            **kwargs,
            'client': client,
            'readonly': not client.can_edit(self.request.user),
            'user_is_benev': self.request.user.is_benevole,
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'user_is_benev': self.request.user.is_benevole,
        }

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': '#client-contacts'})


class ReferentDeleteView(DeleteView):
    model = Referent
    pk_url_kwarg = 'pk_ref'
    form_class = DateArchiveForm

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        with transaction.atomic():
            self.object.date_archive = form.cleaned_data['date_archive']
            self.object.save()
            Journal.objects.create(
                client=self.object.client, description=f"Archivage du contact «{self.object}»",
                quand=timezone.now(), qui=self.request.user
            )
        return HttpResponseRedirect(self.object.client.get_absolute_url())


class ClientReferentListeView(ClientAccessCheckMixin, TemplateView):
    template_name = 'client/contact_list.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        context = {
            **super().get_context_data(**kwargs),
            'client': client,
            'samas_count': client.profclient_set.filter(professionnel__type_pro='Sama').count(),
        }
        context.update(client.contacts_as_context())
        return context


class ClientReferentReorderView(View):
    typ = None

    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        ref_pks = [('samas' if pk == 'samas' else int(pk)) for pk in request.POST.get('pks').split(',')]
        qset = {
            'référent': client.referents.filter(referent__isnull=False).order_by('referent'),
            'répondant': client.referents.filter(repondant__isnull=False).order_by('repondant')
        }[self.typ]
        old_order = [contact.get_full_name() for contact in qset]
        if self.typ == 'répondant' and client.samaritains:
            old_order.insert(client.samaritains - 1, "Samaritains")
        old_order = ', '.join(f"{idx + 1}. {el}" for idx, el in enumerate(old_order))
        for contact in qset:
            setattr(contact, self.typ.replace('é', 'e'), ref_pks.index(contact.pk) + 1)
            contact.save()
        if self.typ == 'répondant' and 'samas' in ref_pks:
            client.samaritains = ref_pks.index('samas') + 1
            client.save()
        new_order = [contact.get_full_name() for contact in qset.all()]
        if self.typ == 'répondant' and client.samaritains:
            new_order.insert(client.samaritains - 1, "Samaritains")
        new_order = ', '.join(f"{idx + 1}. {el}" for idx, el in enumerate(new_order))
        Journal.objects.create(
            client=client,
            description=(
                f"Changement de priorité des {self.typ}s.<br>"
                f"<i>Avant:</i> {old_order}.<br>"
                f"<i>Après:</i> {new_order}"
            ),
            quand=timezone.now(), qui=request.user
        )
        return JsonResponse({'result': 'OK'})


class ClientProfessionnelAddView(FormView):
    template_name = 'client/client_professionnel.html'

    def dispatch(self, request, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not self.client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """One form for Professionnel selection and one for Professionnel creation."""
        # We do not call super as get_form would crash
        return {
            'client': self.client,
            'form_chose': (
                kwargs['form'] if isinstance(kwargs.get('form'), ProfessionnelSelectForm)
                else ProfessionnelSelectForm()
            ),
            'form_create': (
                kwargs['form'] if isinstance(kwargs.get('form'), ProfessionnelClientForm)
                else ProfessionnelClientForm()
            ),
        }

    def post(self, request, *args, **kwargs):
        if 'professionnel' in request.POST:
            form = ProfessionnelSelectForm(data=request.POST)
        else:
            form = ProfessionnelClientForm(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        if isinstance(form, ProfessionnelSelectForm):
            prof = Professionnel.objects.get(pk=form.cleaned_data['professionnel_pk'])
        else:
            prof = form.save()
        with transaction.atomic():
            remarque = form.cleaned_data.get('remarque_client', '')
            ProfClient.objects.create(
                professionnel=prof, client=self.client, remarque=remarque
            )
            Journal.objects.create(
                client=self.client,
                description=f"Ajout du professionnel «{prof}»" + (f" ({remarque})" if remarque else ""),
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientProfessionnelEditView(UpdateView):
    model = ProfClient
    pk_url_kwarg = 'pk_ref'
    form_class = ProfessionnelClientEditForm
    template_name = 'client/client_professionnel_edit.html'

    def form_valid(self, form):
        if not form.instance.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        form.save()
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientProfessionnelDeleteView(DeleteView):
    model = ProfClient
    pk_url_kwarg = 'pk_ref'

    def form_valid(self, form):
        if not self.object.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        self.object.delete()
        Journal.objects.create(
            client=self.object.client, description=f"Suppression du professionnel «{self.object}»",
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ProfessionnelListView(FilterFormMixin, ListView):
    model = Professionnel
    filter_formclass = ProfFilterForm
    paginate_by = 25

    def get_queryset(self):
        return super().get_queryset().filter(date_archive__isnull=True).annotate(
            clients=Count('profclient', filter=Q(profclient__client__archive_le__isnull=True))
        ).order_by('nom', 'prenom')


class ProfessionnelSearchView(View):
    """Endpoint for autocomplete search for ClientProfessionnelAddView."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        results = [
            {'label': str(prof), 'value': prof.pk, 'details': {'remarque': prof.remarque}}
            for prof in Professionnel.objects.filter(
                Q(date_archive__isnull=True) & (
                    Q(nom__unaccent__icontains=term) | Q(remarque__unaccent__icontains=term)
                )
            )[:10]
        ]
        return JsonResponse(results, safe=False)


class ProfessionnelEditView(PermissionRequiredMixin, GeoAddressMixin, CreateUpdateView):
    model = Professionnel
    form_class = ProfessionnelForm
    template_name = 'client/professionnel_edit.html'
    permission_required = 'client.change_professionnel'
    json_response = True

    def geo_enabled(self, instance):
        return super().geo_enabled(instance) and instance.type_pro == 'Sama'

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'clients': self.object.profclient_set.all().order_by('client__nom') if self.object else None,
        }

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ProfessionnelArchiveView(PermissionRequiredMixin, View):
    model = Professionnel
    success_message = "Le professionnel a bien été archivé."
    permission_required = 'client.change_professionnel'

    def post(self, request, *args, **kwargs):
        prof = get_object_or_404(self.model, pk=kwargs['pk'])
        type_pro_str = "samaritain" if prof.type_pro == "Sama" else "professionnel"
        profclients = prof.profclient_set.filter(client__archive_le__isnull=True)
        with transaction.atomic():
            msg = f"Suppression du {type_pro_str} «{prof}» qui a été archivé."
            for lien in profclients:
                Journal.objects.create(
                    client=lien.client, description=msg,
                    quand=timezone.now(), qui=self.request.user
                )
                lien.delete()
            prof.date_archive = timezone.now()
            prof.save()
        messages.success(request, self.success_message)
        return JsonResponse({'result': 'OK', 'reload': 'page'})


class ClientSetSamaritainsView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        if not client.samaritains:
            priorite = client.referents.filter(repondant__isnull=False).aggregate(Max('repondant'))['repondant__max']
            if priorite is None:
                priorite = 1
            else:
                priorite = priorite + 1
            client.samaritains = priorite
            msg = "Ajout des samaritains comme répondants"
        else:
            client.samaritains = None
            client.profclient_set.filter(professionnel__type_pro='Sama').delete()
            msg = "Suppression des samaritains comme répondants"
        client.save()
        Journal.objects.create(
            client=client, description=msg,
            quand=timezone.now(), qui=request.user
        )
        return JsonResponse({'result': 'OK', 'reload': '#client-contacts'})


class ClientSamaritainsView(TemplateView):
    template_name = 'alarme/client_samas.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        current = client.profclient_set.filter(
            professionnel__type_pro='Sama'
        ).select_related('professionnel').order_by('priorite')
        for sama in current:
            sama.dist_vo = distance_vo(sama.professionnel.empl_geo, client.empl_geo)
            sama.dist_real = distance_real(sama.professionnel.empl_geo, client.empl_geo, only_cache=True)
        others = Professionnel.objects.filter(type_pro='Sama').exclude(
            pk__in=current.values_list('professionnel__pk')
        )
        for sama in others:
            sama.dist_vo = distance_vo(sama.empl_geo, client.empl_geo)
            sama.dist_real = distance_real(sama.empl_geo, client.empl_geo, only_cache=True)
        others = sorted(others, key=lambda s: (s.dist_vo is None, s.dist_vo))
        return {
            **super().get_context_data(**kwargs),
            'client': client,
            'current_samas': current,
            'other_samas': others,
        }


class ClientSamaritainAddView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        sama = get_object_or_404(Professionnel, pk=self.kwargs['sama_pk'])
        priorite = (
            client.profclient_set.filter(
                professionnel__type_pro='Sama'
            ).aggregate(Max('priorite'))['priorite__max'] or 0
        ) + 1
        with transaction.atomic():
            ProfClient.objects.create(client=client, professionnel=sama, priorite=priorite)
            Journal.objects.create(
                client=client,
                description=f"Ajout samaritain {sama.nom_prenom} en priorité {priorite}",
                quand=timezone.now(), qui=request.user
            )
        return HttpResponseRedirect(reverse('client-samaritains', args=[client.pk]))


class ClientSamaritainRemoveView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        rel = get_object_or_404(ProfClient, pk=self.kwargs['sama_pk'])
        with transaction.atomic():
            msg = f"Suppression du samaritain {rel.professionnel.nom_prenom}"
            rel.delete()
            Journal.objects.create(
                client=client, description=msg, quand=timezone.now(), qui=request.user
            )
        return HttpResponseRedirect(reverse('client-samaritains', args=[client.pk]))


class ClientSamaritainReorderView(View):
    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        sama_pks = [int(pk) for pk in request.POST.get('pks').split(',')]
        qset = client.profclient_set.filter(
            professionnel__type_pro='Sama'
        ).select_related('professionnel').order_by('priorite')
        old_order = ', '.join(f"{idx + 1}. {pro.professionnel.nom_prenom}" for idx, pro in enumerate(qset))
        with transaction.atomic():
            for pro in qset:
                pro.priorite = sama_pks.index(pro.pk) + 1
                pro.save()
            new_order = ', '.join(f"{idx + 1}. {pro.professionnel.nom_prenom}" for idx, pro in enumerate(qset.all()))
            Journal.objects.create(
                client=client,
                description=(
                    f"Changement de priorité des samaritains.<br>"
                    f"<i>Avant:</i> {old_order}.<br>"
                    f"<i>Après:</i> {new_order}"
                ),
                quand=timezone.now(), qui=request.user
            )
        return JsonResponse({'result': 'OK'})


def client_sama_distance(request, pk, sama_pk):
    client = get_object_or_404(Client, pk=pk)
    sama = get_object_or_404(Professionnel, pk=sama_pk)
    return JsonResponse(distance_real(sama.empl_geo, client.empl_geo))


class ClientAbsenceSuiviView(UpdateView):
    model = AdressePresence
    pk_url_kwarg = 'pk_abs'
    template_name = 'client/absence_edit.html'
    form_class = AdressePresenceSuiviForm

    def form_valid(self, form):
        def format_si_date(val):
            if isinstance(val, date):
                return django_format(val, 'd.m.Y')
            return val

        if not form.instance.adresse.client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")

        with transaction.atomic():
            form.save()
            changes = ", ".join([
                "{label}: {value}".format(
                    label=form.fields[fname].label.lower(),
                    value=format_si_date(form.cleaned_data[fname])
                )
                for fname in form.changed_data if form.fields[fname].label
            ])
            msg = f"Suivi d’absence ({changes})"
            Journal.objects.create(
                client=self.object.adresse.client, description=msg,
                quand=timezone.now(), qui=self.request.user
            )
        return JsonResponse({'result': 'OK', 'reload': 'page#absences'})


class ClientFichierAddView(ClientJournalMixin, CreateUpdateView):
    model = Fichier
    form_class = FichierForm
    template_name = 'fichier_edit.html'
    journal_add_message = "Ajout d’un fichier: {obj.titre}"
    is_create = True
    json_response = True

    def get_client(self):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour modifier ce client.")
        return client

    def get_form_kwargs(self):
        return {
            **super().get_form_kwargs(),
            'app': current_app(),
        }

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'obj': self.get_client(),
        }

    def form_valid(self, form):
        client = self.get_client()
        form.instance.content_object = client
        form.instance.qui = self.request.user
        form.instance.quand = timezone.now()
        from_app = current_app()
        with transaction.atomic():
            super().form_valid(form)
            if form.instance.typ == TypeFichier.DOC_GENERAL:
                other_types = set(client.type_client) - {from_app}
                for app in other_types:
                    Alerte.objects.create(
                        client=client,
                        cible=app,
                        alerte=f"Ajout d'un fichier ({form.instance.titre}) depuis l’application «{from_app}».",
                        recu_le=timezone.now()
                    )
        return JsonResponse({'result': 'OK', 'reload': '#client-fichiers'})


class ClientFichierListeView(ClientAccessCheckMixin, TemplateView):
    """Rechargement dynamique du bloc des fichiers uniquement."""
    template_name = 'client/fichier_list.html'

    def get_context_data(self, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        return {
            **super().get_context_data(**kwargs),
            'client': client,
            'fichiers': client.fichiers.all(),
            'fichiers': client.fichiers_app(current_app()).order_by('-quand'),
        }


class ClientFichierDeleteView(DeleteView):
    model = Fichier

    def form_valid(self, form):
        client = self.object.content_object
        if not client.can_edit(self.request.user):
            raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour supprimer ce fichier.")
        self.object.delete()
        Journal.objects.create(
            client=client, description=f"Suppression du fichier «{self.object.titre}»",
            quand=timezone.now(), qui=self.request.user
        )
        return JsonResponse({'result': 'OK', 'reload': '#client-fichiers'})


class ClientArchiveView(PermissionRequiredMixin, View):
    permission_required = 'client.change_client'

    def post(self, request, *args, **kwargs):
        client = get_object_or_404(Client, pk=self.kwargs['pk'])
        if client.archive_le:
            client.desarchiver(request.user, current_app())
            messages.success(request, f"Le client {client} a bien été réactivé.")
            return HttpResponseRedirect(client.get_absolute_url())

        client.archiver(request.user, current_app())
        messages.success(request, f"Le client {client} a bien été archivé.")
        return HttpResponseRedirect(reverse('clients'))


class ClientAlerteEditView(PermissionRequiredMixin, UpdateView):
    model = Alerte
    form_class = AlerteForm
    template_name = 'alarme/alerte_edit.html'
    permission_required = 'client.view_client'

    def form_valid(self, form):
        if form.cleaned_data['traite_le']:
            form.instance.par = self.request.user
        with transaction.atomic():
            form.save()
            if form.cleaned_data['traite_le'] and form.cleaned_data['remarque']:
                Journal.objects.create(
                    client=self.object.client, description=f"Alerte traitée: {form.cleaned_data['remarque']}",
                    quand=timezone.now(), qui=self.request.user
                )
        return JsonResponse({'result': 'OK', 'reload': 'page'})
