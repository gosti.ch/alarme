import csv
from datetime import date
from pathlib import Path

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from alarme.fields import PhoneNumberValidator
from alarme.models import (
    Alarme, Client, Installation, Materiel, MaterielClient, ModeleAlarme,
    Professionnel, Referent, ReferentTel, TypeMateriel,
)

LANG_MAPPING = {
    'Französisch': 'fr',
    'Deutsch': 'de',
    'Italienisch': 'it',
    'Spanisch': 'es',
    'Englisch': 'en',
}


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('dir_path')

    def handle(self, **options):
        directory = Path(options['dir_path'])
        if not directory.exists():
            raise CommandError(f"Directory {directory} does not exist")
        imp_files = {}
        for _file in directory.glob('*.csv'):
            imp_files[_file.name[3:]] = _file
        assert len(imp_files) == 9, "The directory should contain 9 csv files"
        delete_existing = False
        with transaction.atomic():
            if delete_existing:
                Client.objects.exclude(no_centrale='').delete()
            self.import_clients(imp_files['Teilnehmer.csv'])
            self.import_tel_clients(imp_files['Telefon_Teilnehmer.csv'])
            if delete_existing:
                Alarme.objects.exclude(no_centrale__isnull=True).delete()
                Materiel.objects.exclude(no_centrale__isnull=True).delete()
            self.import_devices(imp_files['Geraete.csv'])
            if delete_existing:
                Referent.objects.exclude(no_centrale__isnull=True).delete()
            self.import_referents(imp_files['Bezugsperson.csv'])
            self.import_referents_tel(imp_files['Telefon_Bezugsperson.csv'])
            if delete_existing:
                Professionnel.objects.exclude(no_centrale__isnull=True).delete()
            self.import_professionnels(
                imp_files['Berufliche_Hilfspersonen.csv'],
                imp_files['Telefon_BeruflicheHilfsperson.csv'],
                imp_files['Beziehung_BerHilfspersonen_Teilnehmer.csv'],
            )
            # Ignored: Reportagegruppen.csv

    # 'Vorlage SRK JURA-JURA'/Demo Gerät/'Vorlage CRS NE'
    ignored_nums = ['51349', '81305', '86450', '104164', '97457', '61138']

    def import_clients(self, path):
        with path.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for idx, line in enumerate(reader):
                # Ignored: 'Erstellungsdatum', 'Kanton', 'Land', 'Anrede' utile?, 'Loc'?
                if line['Nr'] in self.ignored_nums:
                    continue
                if clean(line['Ort']) in ['.', '']:
                    continue
                client = Client(
                    no_centrale=f"{line['Loc']}-{line['Nr']}",
                    nom=clean(line['Nachname']), prenom=clean(line['Vorname']) or '-',
                    rue=clean(line['Strasse']), npa=clean(line['PLZ']), localite=clean(line['Ort']),
                    tel_1=read_tel(line['Telefon']), courriel=clean(line['E-Mail']),
                    date_naissance=read_date(line['Geburtstag']),
                    langues=read_langs(line['Sprache']), genre=read_genre(line['Geschlecht']),
                    type_logement=clean(line['Liegenschaftstyp']), etage=clean(line['Stockwerk']),
                    nb_pieces=clean(line['Grösse Liegenschaft']),
                    cles=" / ".join([
                        val for val in [clean(line['Schlüssel Standort']), clean(line['Beschreibung Schlüssel'])] if val
                    ]),
                    situation_vie=clean(line['Familienstand']),
                )
                try:
                    client.full_clean()
                except ValidationError as err:
                    print(err)
                    print(client.__dict__)
                    import pdb; pdb.set_trace()
                    continue
                client.save()
        self.stdout.write(f"{idx} clients importés")

    def import_tel_clients(self, path):
        with path.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for idx, line in enumerate(reader):
                if line['Teilnehmer-Nr.'] in self.ignored_nums:
                    continue
                try:
                    client = Client.objects.get(no_centrale=f"{line['Teilnehmer LOC-ID']}-{line['Teilnehmer-Nr.']}")
                except Client.DoesNotExist:
                    self.stderr.write(f"Aucun client avec le no '{line['Teilnehmer-Nr.']}'")
                    continue
                no = read_tel(line['Nummer'])
                if no == client.tel_1:
                    continue
                elif not client.tel_1:
                    client.tel_1 = no
                elif line['Reihenfolge'] == '1':
                    client.tel_2 = client.tel_1
                    client.tel_1 = no
                elif not client.tel_2:
                    client.tel_2 = no
                else:
                    raise ValueError(f"Client {client.no_centrale} has already two tel nums")
                client.save()
        self.stdout.write(f"{idx} téléphones importés")

    def import_referents(self, path):
        with path.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for idx, line in enumerate(reader):
                # Nr. Ber. Hilfsperson	Reihenfolge
                no_client = clean(f"{line['Loc. Nr. Teilnehmer']}-{line['Nr. Teilnehmer']}")
                try:
                    client = Client.objects.get(no_centrale=no_client)
                except Client.DoesNotExist:
                    self.stderr.write(f"Aucun client avec le no '{no_client}'")
                    continue
                type_ref = clean(line['Art Hilfsperson'])
                priorite = clean(line['Reihenfolge'])
                courriel = clean(line['E-Mail']).replace('ç', 'c')
                if ' ' in courriel:
                    # Si plusieurs, seul le premier est pris en compte.
                    courriel = courriel.split()[0]
                referent = Referent(
                    no_centrale=line['Nr. Bezugsperson'],
                    client=client, nom=clean(line['Name']) or type_ref, rue=clean(line['Strasse']),
                    npa=clean(line['PLZ']).replace('F', ''), localite=clean(line['Ort']), courriel=courriel,
                    relation=clean(line['Verhältnis']), courrier_envoye=date(2000, 1, 1),
                    remarque=clean(line['Beschreibung']),
                )
                has_function = False
                if 'BP' in type_ref or 'Notfallinfo' in type_ref:
                    referent.referent = priorite
                    has_function = True
                if 'KP' in type_ref:
                    referent.repondant = priorite
                    has_function = True
                if 'Arzt' in type_ref:
                    referent.medecin = True
                    has_function = True
                if type_ref == 'Psychiater':
                    referent.medecin = True
                    referent.remarque += '/Psychiatre'
                    has_function = True
                if not has_function:
                    # Pflegedienst + Beistand + Interventions'-Samariter / SMS + Sanität + Schlüsseldepot
                    referent.relation += '/' + type_ref
                try:
                    referent.full_clean()
                except ValidationError as err:
                    print(err)
                    print(referent.__dict__)
                    continue
                referent.save()
        self.stdout.write(f"{idx} relations importées")

    def import_referents_tel(self, path):
        with path.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            created = 0
            for idx, line in enumerate(reader):
                no_referent = clean(line['Nr. Bezugsperson'])
                try:
                    referent = Referent.objects.get(no_centrale=no_referent)
                except Referent.DoesNotExist:
                    self.stderr.write(f"Aucun référent avec le no '{no_referent}'")
                    continue
                no_tel = ReferentTel(
                    referent=referent,
                    tel=clean(line['Telefon']),
                    priorite=clean(line['Reihenfolge']),
                    remarque=clean(line['Typ']),
                )
                try:
                    no_tel.full_clean()
                except ValidationError as err:
                    print(err)
                    print(no_tel.__dict__)
                    continue
                no_tel.save()
                created += 1
        self.stdout.write(f"{created} no tél de référents importés")

    def import_devices(self, path):
        with path.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for idx, line in enumerate(reader):
                # Ignored: Batteriedatum (semble toujours égale à date achat?)
                #          Gerät LOC?, Gerät-Status, Teilmehmer Loc Nr.?
                no_client = clean(f"{line['Teilmehmer Loc Nr.']}-{line['Teilnehmer Nr.']}")
                type_app = clean(line['Gerät-Typ'])
                if not no_client and not type_app:
                    continue
                if type_app in ["ID (Kein Gerät)", "Notruf Handy Mobile"]:
                    if type_app == "ID (Kein Gerät)":
                        type_mat, _ = TypeMateriel.objects.get_or_create(nom='Émetteur')
                    else:
                        type_mat, _ = TypeMateriel.objects.get_or_create(nom='Portable de secours')
                    materiel = Materiel.objects.create(
                        type_mat=type_mat, no_ref=clean(line['Gerätenummer']),
                        no_centrale=clean(line['Gerät ID']), date_achat=read_date(line['Kaufdatum']),
                    )
                    if no_client:
                        try:
                            client = Client.objects.get(no_centrale=no_client)
                        except Client.DoesNotExist:
                            self.stderr.write(f"Aucun client avec le no '{no_client}'")
                        else:
                            MaterielClient.objects.create(
                                materiel=materiel, client=client,
                                date_debut=read_date(line['Erstellungsdatum']),
                            )
                    continue

                model, _ = ModeleAlarme.objects.get_or_create(nom=type_app)
                alarme = Alarme(
                    modele=model, no_centrale=clean(line['Gerät ID']), centrale='curena',
                    no_appareil=clean(line['Gerätenummer']), no_serie=clean(line['Seriennummer']),
                    carte_sim=clean(line['SIM Code']), date_achat=read_date(line['Kaufdatum']),
                )
                alarme.full_clean()
                alarme.save()
                if no_client:
                    try:
                        client = Client.objects.get(no_centrale=no_client)
                    except Client.DoesNotExist:
                        self.stderr.write(f"Aucun client avec le no '{no_client}'")
                    else:
                        install = Installation(
                            client=client, alarme=alarme, date_debut=read_date(line['Erstellungsdatum'])
                        )
                        try:
                            install.full_clean()
                        except ValidationError:
                            if not client.nom.startswith('Demogerät'):
                                raise
                        else:
                            install.save()
        self.stdout.write(f"{idx} alarmes importées")

    def import_professionnels(self, path_prof, path_prof_tel, path_links):
        # Read prof file and store its content in profs.
        profs = {}
        ignored_profs = [
            # NE: FUS/SIS/CRNE
            536875235, 536875234, 536874410, 536875271, 536874347, 536875735,
            # JU:
            536873252, 536873253, 536873254, 536873256, 536875723, 536876158,
        ]

        with path_prof.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for line in reader:
                profs[line['Nr.']] = line

        with path_prof_tel.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for line in reader:
                if line['Reihenfolge'] == '1':
                    profs[line['Nr. Ber. Hilfsperson']]['tel_1'] = line['Telefon']
                elif line['Reihenfolge'] == '2':
                    profs[line['Nr. Ber. Hilfsperson']]['tel_2'] = line['Telefon']

        with path_links.open('r', encoding='latin1') as fh:
            reader = csv.DictReader(fh, delimiter=';')
            for idx, line in enumerate(reader):
                no_client = clean(f"{line['Loc Nr. Teilnehmer']}-{line[' Nr. Teilnehmer']}")
                no_prof = clean(line['Nr. Ber. Hilfsperson'])
                if int(no_prof) in ignored_profs:
                    continue
                try:
                    client = Client.objects.get(no_centrale=no_client)
                except Client.DoesNotExist:
                    self.stderr.write(f"Aucun client avec le no '{no_client}'")
                    continue
                if no_prof not in profs:
                    self.stderr.write(f"Aucun professionnel avec le no '{no_prof}'")
                    continue
                prof_data = profs[no_prof]
                if 'Arzt' in clean(prof_data['Firmentyp']):
                    prof, _ = Professionnel.objects.get_or_create(
                        no_centrale=no_prof,
                        defaults={
                            'type_pro': 'Médecin',
                            'nom': clean(prof_data['Name']), 'rue': clean(prof_data['Strasse']),
                            'npa': clean(prof_data['PLZ']), 'localite': clean(prof_data['Ort']),
                            'courriel': clean(prof_data['E-Mail']),
                            'tel_1': clean(prof_data['tel_1']), 'tel_2': clean(prof_data.get('tel_2')),
                            'remarque': clean(prof_data['Beschreibung']),
                        }
                    )
                    client.professionnels.add(prof)
                else:
                    self.stderr.write(f"Lien ignoré entre {client} et le professionnel '{no_prof}'")


def clean(val):
    if val is None:
        return ''
    return val.strip()


def read_date(dt):
    if not dt:
        return None
    return date(*reversed([int(part) for part in dt.split('.')]))


def read_tel(tel):
    no = clean(tel.replace('/', ' '))
    if not no:
        return ''
    try:
        PhoneNumberValidator()(no)
    except Exception:
        print(f"Le no de téléphone '{no}' n’est pas valide.")
        return ''
    return no


def read_langs(langs):
    return [code for key, code in LANG_MAPPING.items() if key in clean(langs)]


def read_genre(genre):
    return {'V': 'F', 'M': 'M'}.get(genre, '')
