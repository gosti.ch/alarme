"""
Django settings for alarmes project.
"""

from decimal import Decimal
from ipaddress import IPv4Network
from pathlib import Path
from django.contrib.messages import constants

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-wo_@n9#jam6j)a6pq%+ajt@oxjn60+ovtcp^xscmboiz7yb^i#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',

    'django_otp',
    'django_otp.plugins.otp_totp',
    'django_otp.plugins.otp_static',
    'two_factor',
    'leaflet',

    'common',
    'benevole',
    'client',

    'django.forms',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'common.middleware.LoginRequiredMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'common.context_processors.common',
            ],
            'builtins': [
                'django.templatetags.static',
                'common.templatetags.common_utils',
            ],
        },
    },
]

WSGI_APPLICATION = 'common.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'alarmes',
    }
}


# Password validation
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
AUTH_USER_MODEL = 'common.Utilisateur'

INTERNAL_IPS = ["127.0.0.1"]

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGES = [
    ('fr-ch', 'Français'),
]
LANGUAGE_CODE = 'fr-ch'

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_TZ = True

STATIC_URL = 'static/'
STATIC_ROOT = BASE_DIR / 'static'

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'media'

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

# Default primary key field type
# https://docs.djangoproject.com/en/dev/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{levelname} {asctime} {module} {message}",
            "style": "{",
        },
    },
    "handlers": {
        "apilogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": str(BASE_DIR / "croixrouge_api.log"),
            "maxBytes": 100000,
            "backupCount": 2,
            "formatter": "verbose",
        },
    },
    "loggers": {
        "api": {
            "handlers": ["apilogfile"],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}

MESSAGE_TAGS = {
    constants.DEBUG: 'alert-info',
    constants.INFO: 'alert-info',
    constants.SUCCESS: 'alert-success',
    constants.WARNING: 'alert-warning',
    constants.ERROR: 'alert-danger',
}

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'SAMEORIGIN'

LOGIN_URL = 'two_factor:login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
EXEMPT_2FA_NETWORKS = [IPv4Network('127.0.0.0/30')]
# These users should not have TOTP devices configured.
EXEMPT_2FA_USERS = []

LEAFLET_CONFIG = {
    'SPATIAL_EXTENT': (6.3, 46.9, 7.6, 47.5),
}

QUERY_GEOADMIN_FOR_ADDRESS = False
OPENROUTE_API_KEY = ''

# Adresses IP et tokens acceptés pour accéder à notre API (cf api/views.py).
API_ALLOWED_IPS = []
API_ALLOWED_TOKENS = []

TARIF_CHAUFFEUR = {
    0: Decimal('0.70'),
    6001: Decimal('0.55'),
}
FACTURES_EXPORT_URL = None
FACTURES_TRANSMISSION_URL = None
NOTESFRAIS_TRANSMISSION_URLNAME = None
FACTURES_TRIMESTRIELLES = False
CODES_ARTICLE_INSTALLATION = []
CODE_ARTICLE_INTERV_SAMA = ''
# Indique si le mode révision des appareils (date de révision doit être saisie
# après chaque retour d'appareil pour être réutilisée) est actif ou non.
APPAREIL_REVISION_ACTIF = True
CENTRALE_EMAIL = ''
DELAI_ENTRE_VISITES = 182  # en jours
# Pour nouvelles installs, durée par défaut du champ duree_seul
DUREE_SANS_CLIENT_NEW_DEFAULT = None

TEST_INSTANCE = False
