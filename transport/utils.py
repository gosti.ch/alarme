from decimal import Decimal

from django.conf import settings
from django.db import transaction
from django.db.models import Sum

from benevole.models import LigneFrais, NoteFrais, TypeFrais
from common.utils import arrondi_5
from transport.models import Transport


def generer_notes_frais(mois):
    if NoteFrais.objects.filter(service='transport', mois=mois).exists():
        raise Exception("Les notes de frais pour existent déjà.")

    liste_notes = Transport.frais_mensuels(mois)
    with transaction.atomic():
        for chauffeur, line in liste_notes.items():
            kms_avant_ce_mois = chauffeur.notefrais_set.filter(
                mois__year=mois.year
            ).aggregate(total_kms=Sum('kms'))['total_kms'] or Decimal('0')
            note = NoteFrais.objects.create(
                benevole=chauffeur,
                service='transport',
                mois=mois,
                kms=line['kms'],
            )
            # Frais repas et divers
            if line['frais_repas'] > 0:
                LigneFrais.objects.create(
                    note=note, libelle=TypeFrais.objects.get(no='NF-B07'),
                    quantite=Decimal('1'), montant_unit=line['frais_repas']
                )
            if line['frais_divers'] > 0:
                LigneFrais.objects.create(
                    note=note, libelle=TypeFrais.objects.get(no='NF-B06'),
                    quantite=Decimal('1'), montant_unit=line['frais_divers']
                )
            if line['frais_attente'] > 0:
                LigneFrais.objects.create(
                    note=note, libelle=TypeFrais.objects.get(no='NF-B12'),
                    quantite=line['attente_nb'], montant_unit=Transport.COUT_ATTENTE_UNITE,
                )
            if line['frais_plusieurs'] > 0:
                LigneFrais.objects.create(
                    note=note, libelle=TypeFrais.objects.get(no='NF-B13'),
                    quantite=line['plusieurs_nb'], montant_unit=Transport.FRAIS_CLIENT_SUPPL,
                )
            # Indemnités km
            if line['kms'] > 0:
                kms_over_6001 = kms_avant_ce_mois + line['kms'] - max(6000, kms_avant_ce_mois)
                kms_over_6001 = kms_over_6001 if kms_over_6001 > 0 else 0
                kms_below_6001 = line['kms'] - kms_over_6001
                # Dès total sur année civile > 6000kms, tarif différent (NF-B11)
                if kms_below_6001:
                    LigneFrais.objects.create(
                        note=note, libelle=TypeFrais.objects.get(no='NF-B10'),
                        quantite=kms_below_6001, montant_unit=settings.TARIF_CHAUFFEUR[0],
                    )
                if kms_over_6001:
                    LigneFrais.objects.create(
                        note=note, libelle=TypeFrais.objects.get(no='NF-B11'),
                        quantite=kms_over_6001, montant_unit=settings.TARIF_CHAUFFEUR[6001],
                    )
