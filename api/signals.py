from contextlib import contextmanager

import httpx

from django.core.mail import mail_admins
from django.db.models.signals import post_save, pre_save

from client.models import Client, Referent, referent_saved
from benevole.models import Benevole
from .core import BearerAuth, CIDApi, is_success, report_error


def cache_previous_instance(sender, instance, *args, **kwargs):
    """pre_save signal listener to keep previous instance in cache."""
    original_obj = None
    if instance.id:
        original_obj = instance.__class__.objects.get(pk=instance.id)
        if isinstance(instance, Referent):
            # cache phone/mobile
            tels = [tel.tel for tel in instance.telephones()]
            original_obj.phone = next((tel for tel in tels if tel and not tel.startswith('07')), '')
            original_obj.mobile = next((tel for tel in tels if tel.startswith('07')), '')
    instance._original_obj = original_obj


class CIDSyncHandlerBase:
    """post_save signal listener to sync Client/Benevole to CID."""
    api_url = CIDApi.partner_url
    api_timeout = 15
    model = None
    external_id_field = 'id_externe'
    # Correspondance des champs à synchroniser vers CID
    synced_fields = {}
    required_fields = []

    def __call__(self, sender, instance, created, **kwargs):
        has_missing_values = any([getattr(instance, fname) in (None, '') for fname in self.required_fields])
        if has_missing_values:
            return
        if not getattr(instance, self.external_id_field):
            return self.post_object(instance)
        if created:
            self.post_object(instance)
        else:
            self.put_object(instance)

    @staticmethod
    def serialize(instance, field_name):
        value = getattr(instance, field_name)
        if field_name in ['date_naissance', 'date_deces']:
            value = value.strftime('%Y-%m-%d') if value else False
        elif field_name in ['archive_le', 'date_archive']:
            return not bool(value)
        return value

    def build_json_data(self, instance):
        json_data = {'id': instance.pk}
        for fname, json_name in self.synced_fields.items():
            json_data[json_name] = self.serialize(instance, fname)
        if json_data.get('title'):
            json_data['title'] = {'M': 'Sir', 'F': 'Madam'}.get(json_data['title'])
        if self.model == Client:
            tels = [instance.tel_1, instance.tel_2]
            json_data['phone'] = next((tel for tel in tels if tel and not tel.startswith('07')), '')
            json_data['mobile'] = next((tel for tel in tels if tel.startswith('07')), '')
        if self.model == Referent:
            if hasattr(instance, 'phone'):  # en cache
                json_data['phone'] = instance.phone
                json_data['mobile'] = instance.mobile
            else:
                tels = [tel.tel for tel in instance.telephones()]
                json_data['phone'] = next((tel for tel in tels if tel and not tel.startswith('07')), '')
                json_data['mobile'] = next((tel for tel in tels if tel.startswith('07')), '')
        json_data['country'] = "NE" if ("2000" <= instance.npa <= "2525") else "CH"
        return json_data

    def post_object(self, instance):
        # Push new object to CID
        json_sent = self.build_json_data(instance)
        try:
            response = httpx.post(self.api_url, json=json_sent, auth=BearerAuth(), timeout=self.api_timeout)
        except httpx.TimeoutException:
            # Store on a stack and retry later?
            mail_admins(
                "CID timeout",
                f"Got timeout when trying to push new {instance.__class__.__name__} {instance.pk} to CID server."
            )
        else:
            if is_success(response):
                setattr(instance, self.external_id_field, response.json()['id'])
                instance.save()
            else:
                msg = f"Trying to push new {instance.__class__.__name__} {instance.pk}"
                report_error(
                    response, msg, json_sent=json_sent
                )

    def put_object(self, instance, force=False):
        """Return True if instance was successfully sent to remote server."""
        # Check if any synced fields changed
        new_data = self.build_json_data(instance)
        if force or new_data != self.build_json_data(instance._original_obj):
            # Update object to CID
            try:
                response = httpx.put(self.api_url, json=new_data, auth=BearerAuth(), timeout=self.api_timeout)
            except httpx.TimeoutException:
                # Store on a stack and retry later?
                mail_admins(
                    "CID timeout",
                    f"Got timeout when trying to update (put) {instance.__class__.__name__} {instance.pk} to CID server."
                )
                return False
            if is_success(response):
                return True
            report_error(
                response, f"Trying to update (put) {instance.__class__.__name__} {instance.pk}",
                json_sent=new_data
            )
        return False


class CIDClientSyncHandler(CIDSyncHandlerBase):
    api_url = CIDApi.partner_url
    model = Client
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        'genre': 'title',
        'rue': 'street',
        'npa': 'zip',
        'localite': 'city',
        'courriel': 'email',
        'date_naissance': 'birthdate',
        'date_deces': 'deathdate',
        'archive_le': 'active',
        'no_debiteur': 'olderpid',
        # tel_1 and tel_2 are manually mapped to either 'phone' or 'mobile'.
    }
    required_fields = ['nom', 'prenom', 'genre', 'npa', 'localite']


class CIDReferentSyncHandler(CIDSyncHandlerBase):
    api_url = CIDApi.partner_url
    model = Referent
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        #'genre': 'title',
        'rue': 'street',
        'npa': 'zip',
        'localite': 'city',
        'courriel': 'email',
        'date_archive': 'active',
        'no_debiteur': 'olderpid',
        # tel_1 and tel_2 are manually mapped to ReferentTel instances.
    }

    def __call__(self, sender, instance, created, **kwargs):
        if not instance.facturation_pour:
            # Pas besoin de synchro pour les référents sans lien avec la facturation.
            return
        super().__call__(sender, instance, created, **kwargs)

    def post_object(self, instance):
        autres_refs_meme_deb = Referent.objects.exclude(pk=instance.pk).filter(
            no_debiteur=instance.no_debiteur, id_externe__isnull=False
        )
        if instance.no_debiteur and autres_refs_meme_deb.exists():
            # Si ce référent existe déjà pour un autre client, pas besoin de le créer sur CID.
            if not instance.id_externe:
                instance.id_externe = autres_refs_meme_deb.first().id_externe
                instance.save(update_fields=['id_externe'])
            return
        super().post_object(instance)

    def put_object(self, instance, **kwargs):
        sent = super().put_object(instance, **kwargs)
        # Év. synchroniser autres référents ayant le même no débiteur.
        other_same_refs = Referent.objects.exclude(pk=instance.pk).filter(no_debiteur=instance.no_debiteur)
        if sent and other_same_refs:
            with disconnect_sync_signals():
                for ref in other_same_refs:
                    changed = False
                    for fname in self.synced_fields:
                        if getattr(instance, fname) != getattr(ref, fname):
                            setattr(ref, fname, getattr(instance, fname))
                            changed = True
                    if changed:
                        ref.save()


class CIDBenevoleSyncHandler(CIDSyncHandlerBase):
    api_url = CIDApi.employees_url
    model = Benevole
    synced_fields = {
        'nom': 'name',
        'prenom': 'firstname',
        'genre': 'gender',
        'rue': 'street',
        'npa': 'zip',
        'localite': 'city',
        'tel_prive': 'phone',
        'tel_mobile': 'mobile',
        'courriel': 'email',
        'date_naissance': 'birthdate',
    }
    required_fields = ['nom', 'prenom', 'npa', 'localite', 'date_naissance']


client_sync_receiver = CIDClientSyncHandler()
referent_sync_receiver = CIDReferentSyncHandler()
benevole_sync_receiver = CIDBenevoleSyncHandler()


@contextmanager
def connect_sync_signals(*args, **kwargs):
    """Connexion temporaire des signaux de synchonisation."""
    pre_save.connect(cache_previous_instance, sender=Client)
    post_save.connect(client_sync_receiver, sender=Client)
    pre_save.connect(cache_previous_instance, sender=Referent)
    referent_saved.connect(referent_sync_receiver, sender=Referent)
    pre_save.connect(cache_previous_instance, sender=Benevole)
    post_save.connect(benevole_sync_receiver, sender=Benevole)
    try:
        yield
    finally:
        pre_save.disconnect(cache_previous_instance, sender=Client)
        post_save.disconnect(client_sync_receiver, sender=Client)
        pre_save.disconnect(cache_previous_instance, sender=Referent)
        referent_saved.disconnect(referent_sync_receiver, sender=Referent)
        pre_save.disconnect(cache_previous_instance, sender=Benevole)
        post_save.disconnect(benevole_sync_receiver, sender=Benevole)


@contextmanager
def disconnect_sync_signals(*args, **kwargs):
    """Déconnexion temporaire des signaux de synchonisation."""
    pre_save_dcc = pre_save.disconnect(cache_previous_instance, sender=Client)
    post_save_dcc = post_save.disconnect(client_sync_receiver, sender=Client)
    pre_save_dcr = pre_save.disconnect(cache_previous_instance, sender=Referent)
    referent_saved_dcr = post_save.disconnect(referent_sync_receiver, sender=Referent)
    pre_save_dcb = pre_save.disconnect(cache_previous_instance, sender=Benevole)
    post_save_dcb = post_save.disconnect(benevole_sync_receiver, sender=Benevole)
    try:
        yield
    finally:
        if pre_save_dcc:
            pre_save.connect(cache_previous_instance, sender=Client)
        if post_save_dcc:
            post_save.connect(client_sync_receiver, sender=Client)
        if pre_save_dcr:
            pre_save.connect(cache_previous_instance, sender=Referent)
        if referent_saved_dcr:
            referent_saved.connect(referent_sync_receiver, sender=Referent)
        if pre_save_dcb:
            pre_save.connect(cache_previous_instance, sender=Benevole)
        if post_save_dcb:
            post_save.connect(benevole_sync_receiver, sender=Benevole)
