from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0014_delete_fichier'),
    ]

    operations = [
        migrations.AddField(
            model_name='referent',
            name='id_externe',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
