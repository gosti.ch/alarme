from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('alarme', '0001_squashed_0100_move_client_models_to_client_app'),
    ]

    operations = [
        migrations.AddField(
            model_name='alerte',
            name='cible',
            field=models.CharField(
                choices=[('alarme', 'Alarme'), ('transport', 'Transport'), ('visite', 'Visite')],
                default='alarme', max_length=10
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='alerte',
            name='rel_content_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.CASCADE, to='contenttypes.contenttype'),
        ),
        migrations.AddField(
            model_name='alerte',
            name='rel_object_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
        migrations.AddIndex(
            model_name='alerte',
            index=models.Index(fields=['cible'], name='index_cible'),
        ),
    ]
