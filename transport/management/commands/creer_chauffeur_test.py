from datetime import date, datetime, time, timedelta

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.utils.timezone import get_current_timezone

from client.models import Client
from common.models import Utilisateur
from benevole.models import Benevole
from transport.models import Adresse, Dispo, Regle, Transport
from transport.tests.tests import DataMixin


class Command(BaseCommand):
    def handle(self, **options):
        default_email = "test@example.org"
        default_password = "abcd1234"
        benevole, _ = Benevole.objects.get_or_create(courriel=default_email, defaults={
            'nom': "Test", 'prenom': "Chauffeur",
            'rue': "Rue des Montagnons 48", 'npa': "2300", 'localite': "La Chaux-de-Fonds",
            'empl_geo': [6.8199, 47.0870], 'activites': ['transport'],
        })
        try:
            user = Utilisateur.objects.get(email=benevole.courriel)
        except Utilisateur.DoesNotExist:
            user = Utilisateur.objects.create_user(
                benevole.courriel, default_password,
                first_name=benevole.prenom, last_name=benevole.nom,
            )
        user.groups.add(Group.objects.get(name="Bénévoles"))
        benevole.utilisateur = user
        benevole.save()
        # Créer dispos
        demain_1130 = datetime.combine(date.today() + timedelta(days=1), time(11, 30), tzinfo=get_current_timezone())
        weekly_rule = Regle.objects.get_or_create(nom='Chaque semaine', frequence='WEEKLY')[0]
        Dispo.objects.create(
            chauffeur=benevole,
            debut=demain_1130 - timedelta(hours=1),
            fin=demain_1130 + timedelta(hours=3),
            regle=weekly_rule,
        )
        Dispo.objects.create(
            chauffeur=benevole,
            debut=demain_1130 - timedelta(days=2) - timedelta(hours=3),
            fin=demain_1130 - timedelta(days=2) + timedelta(hours=1),
            regle=weekly_rule,
        )
        # Créer transports passés et transports futurs
        clients = Client.objects.filter(type_client__contains=['transport'], archive_le__isnull=True)[:3]
        adresses = Adresse.objects.filter(nom__icontains='hôpital')[:2]
        DataMixin.create_transport(
            client=clients[0], chauffeur=benevole,
            heure_rdv=demain_1130 - timedelta(days=2), duree_rdv=timedelta(minutes=70),
            statut=Transport.StatutChoices.EFFECTUE,
            destination=adresses[0], retour=True
        )
        DataMixin.create_transport(
            client=clients[1], chauffeur=benevole,
            heure_rdv=demain_1130, duree_rdv=timedelta(minutes=70),
            statut=Transport.StatutChoices.CONFIRME,
            destination=adresses[1], retour=True
        )
        return "Un chauffeur test@example.org a été créé avec le mot de passe abcd1234"

