from datetime import date

from django.conf import settings
from django.core.mail import send_mail
from django.db import transaction



def install_saved(sender, instance, created=False, **kwargs):
    """Post-save signal for Installation sender."""
    if (
        instance._previous_state and instance._previous_state.date_fin_abo is None and
        instance.date_fin_abo is not None and not instance._is_change
    ):
        # Fin d’abonnement, alerter la compta
        transaction.on_commit(lambda: send_resiliation_mail(instance))
        return

    if created and instance.client.installation_set.count() < 2:
        # Envoyer nouveau client à la compta
        transaction.on_commit(lambda: send_new_client_mail(instance))

    if instance.date_debut is not None:
        # Vérifier si les factures adéquates sont créées
        instance.generer_factures(max(instance.date_debut, date.today()), client=instance.client)


def send_resiliation_mail(instance):
    message = (
        f"L’abonnement alarme de {instance.client.nom_prenom} a été résilié pour "
        f"le {instance.date_fin_abo.strftime('%d.%m.%Y')}. Veuillez vérifier si "
        "des factures en cours doivent être annulées.\n"
        "\n--\n"
        "Ceci est un message automatique, ne pas répondre"
    )
    send_mail(
        "[alarme] Abonnement résilié",
        message,
        None,
        [settings.COMPTA_EMAIL]
    )


def send_new_client_mail(instance):
    client = instance.client
    fact_adresse = client.referents.filter(facturation_pour__len__gt=0).first()

    message = (
        f"Un nouvel abonnement alarme a été créé pour {client.nom_prenom}. "
        "Merci de bien vouloir créer un nouveau client dans votre système comptable "
        "et de communiquer le n° de débiteur à la personne responsable des alarmes.\n\n"
        "Client:\n"
        f"{client.nom_prenom}\n"
    )
    if client.c_o:
        message += f"c/o {client.c_o}\n"
    message += f"{client.rue}\n"
    if client.case_postale:
        message += f"{client.case_postale}\n"
    message += f"{client.npa} {client.localite}\n"

    if client.no_debiteur:
        message += f"N° débiteur : {client.no_debiteur}\n"

    if fact_adresse:
        message += (
            "\nAdresse de facturation:\n"
            f"{fact_adresse.nom} {fact_adresse.prenom}\n"
            f"{fact_adresse.rue}\n"
            f"{fact_adresse.npa} {fact_adresse.localite}\n"
        )
    message += (
        "\n--\n"
        "Ceci est un message automatique, ne pas répondre"
    )
    send_mail(
        "[alarme] Nouveau client à créer",
        message,
        None,
        [settings.COMPTA_EMAIL]
    )
