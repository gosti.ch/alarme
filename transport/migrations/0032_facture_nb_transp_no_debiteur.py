from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0031_transport_facture"),
    ]

    operations = [
        migrations.AddField(
            model_name="facture",
            name="nb_transp",
            field=models.PositiveSmallIntegerField(
                default=0, verbose_name="Nb de transports"
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="facture",
            name="no_debiteur",
            field=models.CharField(
                default="", max_length=30, verbose_name="N° de débiteur"
            ),
            preserve_default=False,
        ),
        migrations.AddConstraint(
            model_name="facture",
            constraint=models.UniqueConstraint(
                fields=("no",), name="no_facture_unique"
            ),
        ),
    ]
