import json
from datetime import date
from decimal import Decimal
from unittest import mock

from freezegun import freeze_time

from django.conf import settings
from django.test import TestCase, override_settings, tag
from django.urls import reverse

from alarme.models import (
    ArticleFacture, Facture, Frais, Installation, Materiel, MaterielClient,
    Mission, TypeAbo
)
from alarme.tests.test_general import TestUtils
from benevole.models import Activites, Benevole, TypeFrais
from client.models import Client, Referent
from common.test_utils import mocked_httpx

from ..views import get_api

today = date.today()


class NETests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article = ArticleFacture.objects.create(
            code='40.010', designation="Abonnement mensuel Casa", prix=Decimal('25.5'),
        )

    @tag("factures")
    def test_install_des_20_non_facturee(self):
        """Seuls les frais d'install sont facturés après le 20 du mois."""
        self.create_client_with_installations(1, debut_install=date.today().replace(day=20))[0]
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 1)
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 0)

    @tag("factures")
    @freeze_time(f"{today.year}-{today.month}-04")
    def test_generer_factures_avec_samas(self):
        ArticleFacture.objects.create(
            code='40.220', designation="Répondant samaritain - abonnement mensuel", prix=5
        )
        install1 = self.create_client_with_installations(1)[0]
        install1.client.samaritains = 1
        install1.client.nom_part = "Haddock"
        install1.client.prenom_part = "Georges"
        install1.client.save()
        article = ArticleFacture.objects.create(
            code='EMO1', designation='Émetteur', prix=Decimal('11.30')
        )
        abo_emetteur = TypeAbo.objects.create(nom='Émetteur supp.', article=article)
        MaterielClient.objects.create(
            client=install1.client,
            materiel=Materiel.objects.create(type_mat=self.type_emetteur),
            abonnement=abo_emetteur,
            date_debut=date(2020, 1, 1),
        )
        Facture.objects.all().delete()
        num_factures = Installation.generer_factures(date.today())
        num_factures += MaterielClient.generer_factures(date.today())
        self.assertEqual(num_factures, 4)
        factures = Facture.objects.filter(client=install1.client)
        self.assertEqual(
            sorted(factures.values_list('article__designation', flat=True)),
            ['Abonnement mensuel',
             'Répondant samaritain - abonnement mensuel', 'Répondant samaritain - abonnement mensuel',
             'Émetteur']
        )
        # Idempotence...
        num_factures = Installation.generer_factures(date.today())
        self.assertEqual(num_factures, 0)

    @tag("factures")
    @freeze_time(f"2023-11-04")
    def test_facturation_3mois_minimum(self):
        install1 = self.create_client_with_installations(1)[0]
        install1.date_fin_abo = date(2023, 11, 26)
        install1.save()
        num_factures = Installation.generer_factures(date(2023, 11, 1))
        self.assertEqual(num_factures, 3)
        self.assertEqual(
            list(install1.client.factures.values_list('date_facture', 'libelle')),
            [(date(2023, 11, 4), 'Service d’alarme à domicile - novembre 2023'),
             (date(2023, 11, 4), 'Service d’alarme à domicile - décembre 2023'),
             (date(2023, 11, 4), 'Service d’alarme à domicile - janvier 2024'),
            ]
        )

    @tag("factures")
    @freeze_time(f"2023-11-04")
    def test_facturation_3mois_minimum_apres_20(self):
        """Install et désinstallation après le 20 du mois."""
        install1 = self.create_client_with_installations(1, debut_install=date(2023, 11, 22))[0]
        install1.date_fin_abo = date(2023, 11, 26)
        install1.save()
        num_factures = Installation.generer_factures(date(2023, 11, 1))
        self.assertEqual(num_factures, 4)
        facts_abo = install1.client.factures.exclude(libelle__contains='installation')
        self.assertEqual(
            list(facts_abo.values_list('date_facture', 'libelle')),
            [(date(2023, 11, 4), 'Service d’alarme à domicile - décembre 2023'),
             (date(2023, 11, 4), 'Service d’alarme à domicile - janvier 2024'),
             (date(2023, 11, 4), 'Service d’alarme à domicile - février 2024'),
            ]
        )


@tag("factures")
class EnvoiFacturesTests(TestUtils):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.article = ArticleFacture.objects.create(
            code='40.010', designation="Abonnement mensuel Casa", prix=Decimal('25.5')
        )

    def test_envoi_facture_mensuelle(self):
        install = self.create_client_with_installations(1)[0]
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture", montant=12.5,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_envoi_facture_mensuelle(sent_data, result)

    def test_envoi_facture_manuelle(self):
        install = self.create_client_with_installations(1)[0]
        article = ArticleFacture.objects.create(
            code='40.300', designation="Forfait déplacement", prix=50
        )
        facture = Facture.objects.create(
            client=install.client, article=article,
            date_facture=date(2022, 4, 3), libelle="Forfait déplacement", montant=50,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_envoi_facture_manuelle(sent_data, result)

    def test_envoi_factures_groupees(self):
        install = self.create_client_with_installations(1, debut_install=date(2022, 2, 1))[0]
        num_fact = install.generer_factures(date(2022, 3, 1), date_factures=date(2022, 3, 13))
        self.assertEqual(num_fact, 1)
        Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 3, 13), mois_facture=date(2022, 3, 1), libelle="Facture", montant=12.5,
        )
        art2 = ArticleFacture.objects.create(
            code='40.320', designation="Bracelet émetteur", prix=60
        )
        Facture.objects.create(
            client=install.client, install=None, article=art2,
            date_facture=date(2022, 3, 13), mois_facture=date(2022, 3, 13), libelle="Facture 2", montant=60,
        )
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('factures-transmission', args=['alarme']), data={'client': install.client_id}, follow=True
            )
        self.assert_envoi_factures_groupees(mock_response)

    def test_autre_adresse_facturation(self):
        install = self.create_client_with_installations(1)[0]
        Referent.objects.create(
            salutation='M', nom='Meier', prenom='Aristide', npa='2000', localite='Neuchâtel', rue='Fleurs 5',
            courriel='ad@example.com', facturation_pour=['al-tout'], client=install.client, no_debiteur='4321',
        )
        Referent.objects.create(
            salutation='M', nom='Mayer', prenom='John', npa='2000', localite='Neuchâtel', rue='Fleurs 15',
            courriel='ad2@example.com', facturation_pour=['al-abo'], client=install.client, no_debiteur='1234',
        )
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture", montant=12.5,
        )
        api = get_api()
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            result = api.create_invoice('alarme', [facture])
            sent_data = mock_response.call_args[1]
        self.assert_autre_adresse_facturation(sent_data, result)

    def test_envoi_facture_erreur(self):
        install = self.create_client_with_installations(1)[0]
        install.client.nom = f'Errorum {settings.ERP_API_TARGET}'
        install.client.id_externe = 98765
        install.client.save()
        facture = Facture.objects.create(
            client=install.client, install=install, article=self.article,
            date_facture=date(2022, 4, 3), mois_facture=date(2022, 3, 1), libelle="Facture erreur", montant=12.5,
        )
        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx):
            response = self.client.post(
                reverse('factures-transmission', args=['alarme']),
                data={'fact_id': facture.pk},
                follow=True
            )
        self.assert_envoi_facture_erreur(facture, response)


@override_settings(ERP_API_TARGET='Navision', NAVISION_API_TARGET='test')
class NavisionTests(EnvoiFacturesTests, TestCase):
    def assert_envoi_facture_mensuelle(self, sent_data, result):
        data = sent_data['data'].decode('utf-8')
        self.assertIn('"Léa0"', data)
        self.assertIn('"postingDate": "31032022"', data)
        #self.assertIn('"invoiceDate": "03042022"', data) or today?
        self.assertIn('"invoiceReference": "ALCR, 01.03.22..31.03.22"', data)
        self.assertEqual(result['provInvoiceCreated'], 'some date?')

    def assert_envoi_facture_manuelle(self, sent_data, result):
        data = json.loads(sent_data['data'].decode('utf-8'))
        self.assertEqual(data['address'][0]['firstName'], "Léa0")
        self.assertEqual(data['billingMeta']['postingDate'], date.today().strftime("%d%m%Y"))
        self.assertEqual(data['billingMeta']['invoiceDate'], "03042022")
        self.assertEqual(data['billingMeta']['invoiceReference'], '')
        self.assertEqual(result['provInvoiceCreated'], 'some date?')

    def assert_envoi_factures_groupees(self, mock_response):
        self.assertEqual(len(mock_response.call_args_list), 2)
        posted_data = json.loads(mock_response.call_args_list[1].kwargs['data'])
        self.assertEqual(posted_data['article']['articleCode'], "ABO1,40.010,40.320")
        self.assertEqual(posted_data['article']['articleAmount'], "1,1,1")

    def assert_autre_adresse_facturation(self, sent_data, result):
        data = json.loads(sent_data['data'].decode('utf-8'))
        self.assertEqual(data['address'][1]['salutation'], 1)
        self.assertEqual(data['address'][1]['lastName'], 'Mayer')

    def assert_envoi_facture_erreur(self, facture, response):
        self.assertContains(response, 'Erreur API Navision')
        facture.refresh_from_db()
        self.assertEqual(facture.export_err, 'Erreur API Navision')


@override_settings(ERP_API_TARGET='CID')
class CIDTests(EnvoiFacturesTests, TestCase):
    def create_client_with_installations(self, *args, **kwargs):
        installs = super().create_client_with_installations(*args, **kwargs)
        installs[0].client.id_externe = 7777
        installs[0].client.save(update_fields=['id_externe'])
        return installs

    def assert_envoi_facture_mensuelle(self, sent_data, result):
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-04-03', 'period': '03/2022',
                'journal': 'F520',
                'lines': [{
                    'productref': '40.010', 'description': 'Abonnement mensuel Casa',
                    'quantity': 1.0, 'unitamount': 25.5,
                }],
            }
        )
        self.assertEqual(result, {'id': 1234})

    def assert_envoi_facture_manuelle(self, sent_data, result):
        self.assertEqual(
            sent_data['json'], {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-04-03', 'period': '',
                'journal': 'F520',
                'lines': [{
                    'productref': '40.300', 'description': 'Forfait déplacement',
                    'quantity': 1.0, 'unitamount': 50.0,
                }],
            }
        )
        self.assertEqual(result, {'id': 1234})

    def assert_envoi_factures_groupees(self, mock_response):
        posted_data = mock_response.call_args_list[0].kwargs['json']
        self.maxDiff = None
        self.assertEqual(
            posted_data, {
                'debitorid': 7777, 'beneficiaryid': 7777,
                'invoicedate': '2022-03-13', 'period': '03/2022',
                'journal': 'F520',
                'lines': [{
                    'productref': 'ABO1', 'description': 'Abonnement mensuel',
                    'quantity': 1.0, 'unitamount': 37.45,
                }, {
                    'productref': '40.010', 'description': 'Abonnement mensuel Casa',
                    'quantity': 1.0, 'unitamount': 25.5,
                }, {
                    'productref': '40.320', 'description': 'Bracelet émetteur',
                    'quantity': 1.0, 'unitamount': 60.0,
                }],
            }
        )
        self.assertTrue(Facture.objects.filter(id_externe=1234).exists())

    def assert_autre_adresse_facturation(self, sent_data, result):
        self.assertEqual(sent_data['json']['beneficiaryid'], 7777)
        self.assertEqual(sent_data['json']['debitorid'], 1234)

    def assert_envoi_facture_erreur(self, facture, response):
        self.assertContains(response, 'Erreur API CID 400:')
        facture.refresh_from_db()
        self.assertTrue(facture.export_err.startswith('Erreur API CID 400:'))


class EnvoiNotesFraisTests(TestUtils, TestCase):
    def test_envoi_note_frais(self):
        TypeFrais.objects.bulk_create([
            TypeFrais(no='NF-B01', services=['alarme']),
            TypeFrais(no='NF-B06', services=['alarme']),
            TypeFrais(no='NF-B07', services=['alarme']),
            TypeFrais(no='NF-B51', services=['alarme']),
        ])
        benev = Benevole.objects.create(
            id_externe=22,
            nom="Dupond", prenom="Jean", rue="Rue du Stand 3", npa="2000", localite="Neuchâtel",
            activites=[Activites.INSTALLATION]
        )
        client = Client.objects.create(nom='Donzé', prenom='Léa', type_client=['alarme'])
        m1 = Mission.objects.create(
            type_mission=self.typem, client=client, benevole=benev,
            delai=date.today(), effectuee=date(2023, 5, 12), km=26
        )
        Frais.objects.create(mission=m1, typ='repas', cout='18.35', descriptif='Repas')
        Frais.objects.create(mission=m1, typ='autre', cout='4.5', descriptif='Parking')

        self.client.force_login(self.user)
        with mock.patch('httpx.post', side_effect=mocked_httpx) as mock_response:
            self.client.post(
                reverse('notesfrais-transmission', args=[2023, 5]), data={'service': 'alarme'}
            )
            sent_data = mock_response.call_args[1]
        note = benev.notefrais_set.first()
        self.maxDiff = None
        self.assertEqual(sent_data['json'], {
            'id': note.pk,
            'date': date.today().strftime('%Y-%m-%d'),
            'description': 'Note de frais alarme',
            'employeeid': 22,
            'lines': [
                {'productref': 'NF-B51', 'quantity': 1.0, 'unitamount': 10.0},
                {'productref': 'NF-B07', 'quantity': 1.0, 'unitamount': 18.35},
                {'productref': 'NF-B06', 'quantity': 1.0, 'unitamount': 4.5},
                {'productref': 'NF-B01', 'quantity': 52.0, 'unitamount': 0.7}
            ],
        })
