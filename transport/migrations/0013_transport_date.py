from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0012_benevole_model_moved"),
    ]

    operations = [
        migrations.AddField(
            model_name="transport",
            name="date",
            field=models.DateField(
                default="2020-1-1", verbose_name="Date du transport"
            ),
            preserve_default=False,
        ),
    ]
