from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("client", "__first__"),
        ("transport", "0033_facture_annulee"),
    ]

    operations = [
        migrations.AddField(
            model_name="facture",
            name="autre_debiteur",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="client.referent",
                verbose_name="Autre débiteur",
            ),
        ),
    ]
