import string

import factory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyText

from . import models


class AlarmeFactory(DjangoModelFactory):
    class Meta:
        model = models.Alarme

    modele = factory.Iterator(models.ModeleAlarme.objects.all(), cycle=True)
    no_appareil = FuzzyText(length=12)
    no_serie = FuzzyText(length=8, chars=string.digits)
    date_achat = factory.Faker('date')


class InstallationFactory(DjangoModelFactory):
    class Meta:
        model = models.Installation

    client = factory.Iterator(models.Client.objects.filter(type_client__contains=['alarme']))
    alarme = factory.Iterator(models.Alarme.objects.filter(en_rep_depuis=None))
    abonnement = factory.Iterator(models.TypeAbo.objects.all())
    date_debut = factory.Faker('date')
