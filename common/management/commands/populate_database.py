import factory

from django.core.management.base import BaseCommand

from alarme.factories import AlarmeFactory, InstallationFactory
from alarme.models import ArticleFacture, ModeleAlarme, TypeAbo
from client.factories import ClientAlarmeFactory, ClientTransportFactory
from common.models import Utilisateur


class Command(BaseCommand):
    def handle(self, **options):
        Utilisateur.objects.create_superuser(last_name='Admin', email='admin@example.org', password='admin') 
        ModeleAlarme.objects.bulk_create([
            ModeleAlarme(nom='Domo Night'),
            ModeleAlarme(nom='Domo Start'),
            ModeleAlarme(nom='Neat Novo 4G'),
        ])
        TypeAbo.objects.bulk_create([
            TypeAbo(
                nom='Émetteur supplémentaire',
                article=ArticleFacture.objects.create(
                    code='40.210', designation='Emetteur supplémentaire - location mensuelle',
                    prix='10.80'
                )
            ),
            TypeAbo(
                nom='Abo Casa Night',
                article=ArticleFacture.objects.create(
                    code='40.030', designation='Abonnement mensuel Casa Night',
                    prix='79.00'
                )
            ),
            TypeAbo(
                nom='Abo Casa',
                article=ArticleFacture.objects.create(
                    code='40.010', designation='Abonnement mensuel Casa',
                    prix='59.40'
                )
            ),
        ])
        with factory.Faker.override_default_locale('fr_CH'):
            ClientAlarmeFactory.create_batch(20)
            ClientTransportFactory.create_batch(25)
            AlarmeFactory.create_batch(15)
            InstallationFactory.create_batch(12)
        return "The database was populated, and a superuser admin@example.org (password 'admin') was created"
