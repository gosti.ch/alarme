from datetime import date

from django.apps import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Count, Prefetch, Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.views.generic import ListView, View

from common.views import (
    CreateUpdateView, ExportableMixin, FilterFormMixin, GeoAddressMixin, JournalMixin
)
from .models import Activite, Activites, Benevole, Journal, NoteFrais
from . import forms


class BenevoleListView(ExportableMixin, FilterFormMixin, PermissionRequiredMixin, ListView):
    model = Benevole
    filter_formclass = forms.BenevoleFilterForm
    permission_required = 'benevole.view_benevole'
    template_name = 'benevole/benevole_list.html'
    paginate_by = 25
    is_archive = False
    col_widths = [30, 25, 25, 30, 16, 12, 12, 12, 12, 40]

    def get_queryset(self, base_qs=None):
        base_qs = base_qs or Benevole.objects.all()
        toutes_activites = (
            self.filter_form.is_bound and self.filter_form.is_valid() and
            self.filter_form.cleaned_data['toutes_activites']
        )
        if toutes_activites:
            if self.is_archive:
                self.queryset = base_qs.filter(archive_le__isnull=False)
            else:
                self.queryset = base_qs.filter(archive_le__isnull=True)
        else:
            acts = Activites.par_domaine(self.kwargs['app'])
            self.queryset = base_qs.annotate(
                app_inactive=Count('activite', filter=(
                    Q(activite__activite__in=acts) & Q(activite__fin__isnull=False) & Q(activite__fin__lt=date.today())
                )),
                app_active=Count('activite', filter=(
                    Q(activite__activite__in=acts) & (
                        Q(activite__fin__isnull=True) | Q(activite__fin__gte=date.today())
                    )
                )),
            )
            if self.is_archive:
                self.queryset = self.queryset.filter(app_inactive__gt=0, app_active=0)
            else:
                self.queryset = self.queryset.filter(app_active__gt=0)
        self.queryset = self.queryset.prefetch_related('activite_set').order_by('nom', 'prenom')
        return super().get_queryset()

    def get_context_data(self, **kwargs):
        labels = {
            'alarme': {
                'breadcrumb': 'Bénévoles', 'title': 'Liste des bénévoles alarme', 'new': 'Nouveau bénévole'
            },
            'transport': {
                'breadcrumb': 'Chauffeurs', 'title': 'Liste des chauffeurs bénévoles', 'new': 'Nouveau chauffeur'
            },
            'visite': {
                'breadcrumb': 'Visiteurs', 'title': 'Liste des bénévoles visite', 'new': 'Nouveau bénévole'
            },
        }.get(self.kwargs['app'])
        if self.is_archive:
            labels['title'] += ' archivés'
        return {
            **super().get_context_data(**kwargs),
            'app': self.kwargs['app'],
            'labels': labels,
            'agenda_url_name': 'benevole-agenda' if apps.is_installed('transport') else '',
        }

    def export_lines(self, context):
        yield ['BOLD', 'Nom', 'Rue', 'Localité', 'Courriel', 'Téls'] + Activites.labels + ['Remarques']
        for benev in self.get_queryset():
            yield [
                benev.nom_prenom, benev.rue, f"{benev.npa} {benev.localite}", benev.courriel
            ] + [
                "\n".join([no for no in [benev.tel_prive, benev.tel_mobile, benev.tel_prof] if no])
            ] + [
                ('x' if val in [act.activite for act in benev.activite_set.all()] else '') for val in Activites.values
            ] + [benev.remarques]


class BenevoleEditView(GeoAddressMixin, PermissionRequiredMixin, JournalMixin, CreateUpdateView):
    model = Benevole
    permission_required = 'benevole.change_benevole'
    template_name = 'benevole/benevole.html'
    labels = {
        'alarme': {
            'breadcrumb1': 'Bénévoles', 'breadcrumb2': 'Nouveau bénévole',
            'success': "La création d’un nouveau bénévole a réussi.",
        },
        'transport': {
            'breadcrumb1': 'Chauffeurs', 'breadcrumb2': 'Nouveau chauffeur',
            'success': "La création d’un nouveau chauffeur a réussi.",
        },
        'visite': {
            'breadcrumb1': 'Bénévoles', 'breadcrumb2': 'Nouveau bénévole',
            'success': "La création d’un nouveau bénévole a réussi.",
        },
    }
    journal_add_message = "Création du bénévole"
    journal_edit_message = "Modification du bénévole: {fields}"

    def dispatch(self, *args, **kwargs):
        self.app = self.kwargs['app']
        return super().dispatch(*args, **kwargs)

    def get_initial(self):
        if self.object is None:
            return {'langues': ['fr']}
        else:
            return super().get_initial()

    def get_form_class(self):
        return forms.BenevoleTransportForm if self.app == 'transport' else forms.BenevoleForm

    def get_success_url(self):
        return reverse('benevoles', args=[self.app])

    def get_success_message(self, obj):
        if self.is_create:
            return self.labels[self.app]['success']
        else:
            return f"«{obj.nom} {obj.prenom}» a bien été modifié"

    def form_valid(self, form):
        response = super().form_valid(form)
        if 'courriel' in form.changed_data and self.object.utilisateur_id:
            messages.warning(
                self.request,
                "La modification du courriel implique également que cette "
                "personne se connecte avec cette nouvelle adresse."
            )
        # Auto archivage si plus d'activité active
        has_act_actives = self.object.activite_set.filter(
            Q(fin__isnull=True) | Q(fin__gt=date.today())
        ).exists()
        if not has_act_actives and not self.object.archive_le:
            self.object.archiver()
            self._create_instance(
                description="Plus d’activité en cours, archivage du bénévole.",
                quand=timezone.now(), qui=self.request.user
            )
        elif has_act_actives and self.object.archive_le:
            self.object.archive_le = None
            self.object.save(update_fields=['archive_le'])
            self._create_instance(
                description="Nouvelle activité active, réactivation du bénévole.",
                quand=timezone.now(), qui=self.request.user
            )
        return response

    def _create_instance(self, **kwargs):
        """Surcharge de JournalMixin._create_instance"""
        Journal.objects.create(benevole=self.object, **kwargs)

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'app': self.app,
            'labels': self.labels[self.app],
        }
        if not self.is_create and hasattr(self.object, 'mission_set'):
            context['missions'] = self.object.mission_set.all().order_by('-effectuee', 'delai')
        if hasattr(self.object, 'preferences'):
            context['preferences'] = self.object.preferences.filter(
                client__archive_le__isnull=True
            ).select_related('client').order_by('client__nom')
        return context


class BenevoleCreateAccountView(PermissionRequiredMixin, View):
    permission_required = 'benevole.change_benevole'

    def post(self, request, *args, **kwargs):
        from common.models import Utilisateur

        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        if Utilisateur.objects.filter(email=benevole.courriel).exists():
            messages.error(
                request, f"Un compte existe déjà pour le courriel {benevole.courriel}."
            )
            return HttpResponseRedirect(reverse('benevole-edit', args=[self.kwargs['app'], benevole.pk]))

        pwd = get_random_string(length=10)
        user = Utilisateur.objects.create_user(
            benevole.courriel, pwd,
            first_name=benevole.prenom, last_name=benevole.nom,
        )
        user.groups.add(Group.objects.get(name="Bénévoles"))
        benevole.utilisateur = user
        benevole.save()
        send_mail(
            "[Croix-Rouge NE] Compte pour application",
            "Bonjour\n\nUn nouveau compte a été créé pour que vous ayez accès à l’application Croix-Rouge:\n"
            f"Site: https://{settings.ALLOWED_HOSTS[0]}\n"
            f"Mot de passe: {pwd}\n\n"
            "Cordiales salutations",
            None,
            [benevole.courriel]
        )
        messages.success(
            request,
            f"Un compte a bien été créé pour {benevole} et son mot de passe lui a été envoyé par courriel."
        )
        return HttpResponseRedirect(reverse('benevole-edit', args=[self.kwargs['app'], benevole.pk]))


class BenevoleAutocompleteView(View):
    """Endpoint for autocomplete search for Benevole."""
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q')
        base_qs = Benevole.objects.filter(archive_le__isnull=True).prefetch_related(
            Prefetch('activite_set', queryset=Activite.objects.filter(fin__isnull=True))
        )
        if kwargs['app'] != 'tous':
            base_qs = base_qs.par_domaine(kwargs['app'])
        results = [{
            'label': str(benev), 'value': benev.pk,
            'adresse': f"{benev.rue}, {benev.npa} {benev.localite}",
            'types': [act.activite for act in benev.activite_set.all()],
            'url': benev.get_absolute_url(),
        } for benev in base_qs.filter(nom__unaccent__icontains=term)[:20]]
        return JsonResponse(results, safe=False)


class BenevoleJournalView(PermissionRequiredMixin, ListView):
    model = Journal
    paginate_by = 20
    template_name = 'benevole/benevole_journal.html'
    permission_required = 'benevole.change_benevole'

    def get(self, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.benevole.journaux.order_by('-quand')

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
        }


class BenevoleNotesFraisView(PermissionRequiredMixin, ListView):
    model = NoteFrais
    paginate_by = 20
    template_name = 'benevole/benevole_notesfrais.html'
    permission_required = 'benevole.change_benevole'

    def get(self, *args, **kwargs):
        self.benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        return super().get(*args, **kwargs)

    def get_queryset(self):
        return self.benevole.notefrais_set.prefetch_related('lignes').order_by('-mois')

    def get_context_data(self, **kwargs):
        context = {
            **super().get_context_data(**kwargs),
            'benevole': self.benevole,
        }
        if settings.NOTESFRAIS_TRANSMISSION_URLNAME:
            context['trans_url'] = reverse(settings.NOTESFRAIS_TRANSMISSION_URLNAME, args=[0, 0])
        return context


class BenevoleArchiveView(PermissionRequiredMixin, View):
    """
    Vue actuellement non utilisée, l'archivage se faisant par cessation de toutes les activités.
    À supprimer si la méthode actuelle se confirme.
    """
    permission_required = 'benevole.change_benevole'

    def post(self, request, *args, **kwargs):
        benevole = get_object_or_404(Benevole, pk=self.kwargs['pk'])
        if benevole.archive_le:
            # Désarchiver
            if not request.user.has_perm('benevole.change_benevole'):
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour réactiver cette personne.")
            with transaction.atomic():
                benevole.archive_le = None
                benevole.save()
            messages.success(request, f"{benevole} a bien été réactivé.")
        else:
            # Archiver
            if not request.user.has_perm('benevole.change_benevole'):
                raise PermissionDenied("Vous n’avez pas les permissions nécessaires pour archiver cette personne.")
            with transaction.atomic():
                msg = benevole.archiver()
                success_msg = f"{benevole} a bien été archivé. {msg}"
            messages.success(request, success_msg)
        return HttpResponseRedirect(reverse('benevoles', args=[self.kwargs['app']]))
