from io import BytesIO

import httpx

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied, ValidationError
from django.core.mail import send_mail
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import transaction
from django.db.models import Q
from django.http import (
    FileResponse, HttpResponse, HttpResponseRedirect, HttpResponseServerError,
    JsonResponse
)
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.module_loading import import_string
from django.views.generic import (
    ListView as DjangoListView, TemplateView, UpdateView, View
)
from django.views.static import serve

from two_factor.views import LoginView as DTFALoginView

from common.distance import get_point_from_address
from common.export import OpenXMLExport
from common.models import Utilisateur
from client.models import Referent
from . import forms
from .utils import canton_abrev


def error_view(request):
    template = loader.get_template('500.html')
    return HttpResponseServerError(template.render({'main_logo': settings.MAIN_LOGO}))


class LoginView(DTFALoginView):
    redirect_authenticated_user = True


class NPALocaliteAutocomplete(View):
    # Also read https://swisspost.opendatasoft.com/explore/dataset/plz_verzeichnis_v2/api/
    POST_API = 'https://swisspost.opendatasoft.com/api/records/1.0/search/'

    def get(self, request, *args, **kwargs):
        values = []
        q = request.GET.get('q').strip().lower().replace('saint', 'st')
        if q:
            params = {'dataset': 'plz_verzeichnis_v2', 'rows': 30, 'q': q}
            try:
                response = httpx.get(self.POST_API, params=params, headers={'Accept': "application/json"})
            except httpx.HTTPError:
                return JsonResponse({'result': 'error', 'message': "Échec de connexion à l’API SwissPost"})
            results = response.json()
            # filtrer plz 10 ou 20 (adresses de domiciles)
            values = [
                f"{res['fields']['postleitzahl']} {res['fields']['ortbez27']}"
                for res in results.get('records', [])
                if (
                    res['fields']['plz_typ'] in (10, 20) and
                    (not q.isdigit() or res['fields']['postleitzahl'].startswith(q))
                )
            ]
            values = [
                {'value': val, 'label': val} for val in values
            ]
        return JsonResponse(values, safe=False)


class ReferentFactAutocomplete(View):
    def get(self, request, *args, **kwargs):
        term = request.GET.get('q').strip()
        query = Referent.objects.exclude(
            Q(date_archive__isnull=False) | Q(facturation_pour=[])
        ).filter(
            Q(nom__unaccent__icontains=term)
        )[:15]
        referents = [
            {'label': f"{ref.nom_prenom}, {ref.rue_localite()}", 'value': ref.pk}
            for ref in query
        ]
        results = []
        seen_labels = set()
        for ref in referents:
            if ref['label'] in seen_labels:
                continue
            results.append(ref)
            seen_labels.add(ref['label'])
        return JsonResponse(results, safe=False)


class GeoAddressFormMixin:
    geo_required = True

    def geo_enabled(self, instance):
        return settings.QUERY_GEOADMIN_FOR_ADDRESS

    def clean(self):
        cleaned_data = super().clean()
        if set(self.changed_data).intersection({'npa', 'localite', 'rue'}) and self.geo_enabled(self.instance):
            lon_lat = get_point_from_address(cleaned_data.get('rue'), f"{cleaned_data.get('npa')} {cleaned_data.get('localite')}")
            if lon_lat:
                self.instance.empl_geo = list(lon_lat)
            elif self.geo_required:
                raise ValidationError("Impossible de géolocaliser cette adresse.")
        return cleaned_data


class GeoAddressMixin:
    def geo_enabled(self, instance):
        return settings.QUERY_GEOADMIN_FOR_ADDRESS

    def form_valid(self, form):
        response = super().form_valid(form)
        if set(form.changed_data).intersection({'npa', 'localite', 'rue'}) and self.geo_enabled(form.instance):
            form.instance.geolocalize()
        return response


class ExportableMixin:
    export_class = OpenXMLExport
    col_widths = [50]

    def dispatch(self, request, *args, **kwargs):
        self.export_flag = request.GET.get('export', None) == '1'
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'exportable': True}

    @staticmethod
    def _serialize(value):
        if value is None:
            return ''
        return str(value)

    def export_lines(self, context):
        raise NotImplementedError("Subclasses of ExportableMixin must implement export_lines()")

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            export = self.export_class(col_widths=self.col_widths)
            export.fill_data(self.export_lines(context))
            return export.get_http_response(self.__class__.__name__.replace('View', '') + '.xlsx')
        else:
            return super().render_to_response(context, **response_kwargs)


class JournalMixin:
    def _create_instance(self, **kwargs):
        raise NotImplementedError

    def journalize(self, form, changed_values=True, add_message=None):
        if self.is_create:
            if not add_message and not hasattr(self, 'journal_add_message'):
                return
            journal_msg = add_message or self.journal_add_message.format(obj=form.instance)
        else:
            changes = form.get_changed_string(changed_values=changed_values)
            if changes:
                journal_msg = self.journal_edit_message.format(obj=form.instance, fields=changes)
            else:
                return
        self._create_instance(
            description=journal_msg, quand=timezone.now(),
            qui=self.request.user if not self.request.user.is_anonymous else None
        )


class BenevoleMixin:
    """Accès limité à un compte de bénévole, définition de self.benev"""
    def dispatch(self, request, *args, **kwargs):
        try:
            self.benev = request.user.benevole
        except ObjectDoesNotExist:
            raise PermissionDenied("Aucun bénévole n’est lié à votre compte utilisateur.")
        return super().dispatch(request, *args, **kwargs)


class CreateUpdateView(UpdateView):
    """Mix generic Create and Update views."""
    template_name = 'alarme/general_edit.html'
    is_create = False
    json_response = False

    def get_object(self):
        return None if self.is_create else super().get_object()

    def get_client(self):
        return None

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'client': self.get_client()}

    def form_valid(self, form):
        with transaction.atomic():
            if self.is_create:
                self.object = form.save()
                if hasattr(self, 'journalize'):
                    self.journalize(form)
            else:
                # Journalize before saving form so it's possible to get previous
                # version of objects during journalization.
                if hasattr(self, 'journalize'):
                    self.journalize(form, changed_values=True)
                self.object = form.save()
        if not self.json_response:
            success_message = self.get_success_message(self.object)
            if success_message:
                messages.success(self.request, success_message)
            return HttpResponseRedirect(self.get_success_url())


class FilterFormMixin:
    filter_formclass = None
    return_all_if_unbound = True

    def get(self, request, *args, **kwargs):
        if self.filter_formclass:
            self.filter_form = self.filter_formclass(data=request.GET or None)
        else:
            self.filter_form = None
        return super().get(request, *args, **kwargs)

    def get_queryset(self, base_qs=None):
        items = super().get_queryset() if base_qs is None else base_qs
        if self.filter_form.is_bound and self.filter_form.is_valid() and (
             self.return_all_if_unbound or any(self.filter_form.cleaned_data.values())
        ):
            items = self.filter_form.filter(items)
        elif not self.return_all_if_unbound:
            return self.model.objects.none()
        return items

    def get_context_data(self, *args, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            'form': self.filter_form,
        }


class BasePDFView(View):
    obj_class = None
    pdf_class = None
    produce_kwargs = {}

    def get_object(self):
        return get_object_or_404(self.obj_class, pk=self.kwargs[getattr(self, 'pk_url_kwarg', 'pk')])

    def get(self, request, *args, as_attachment=True, **kwargs):
        instance = self.get_object()
        temp = BytesIO()
        if isinstance(self.pdf_class, str):
            PDFClass = import_string(self.pdf_class % {'CANTON_APP': settings.CANTON_APP})
        else:
            PDFClass = self.pdf_class
        pdf = PDFClass(temp)
        pdf.produce(instance, **self.produce_kwargs)
        filename = pdf.get_filename(instance)
        temp.seek(0)
        return FileResponse(temp, as_attachment=as_attachment, filename=filename)


class MediaServeView(View):
    def get(self, request, *args, **kwargs):
        if self.request.get_host() == 'localhost:8000':
            return serve(request, kwargs['path'], document_root=settings.MEDIA_ROOT)
        filepath = settings.MEDIA_ROOT / kwargs['path']
        response = HttpResponse()
        response['Content-Type'] = 'application/force-download'
        response['Content-Disposition'] = 'attachment; filename="%s"' % filepath.name
        response['X-Sendfile'] = str(filepath).encode('utf-8')
        return response


class ManifestView(TemplateView):
    content_type = "application/json"
    extra_context = {'canton': canton_abrev().lower()}


class GracefulPaginator(Paginator):
    def page(self, number):
        # We do this as unfortunately Django ListView calls .page instead if .get_page
        try:
            number = self.validate_number(number)
        except PageNotAnInteger:
            number = 1
        except EmptyPage:
            number = self.num_pages
        return super().page(number)


class ListView(DjangoListView):
    paginator_class = GracefulPaginator

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if context['paginator'] is not None:
            context['elided_page_range'] = context['paginator'].get_elided_page_range(context['page_obj'].number)
        return context


class UtilisateurListView(PermissionRequiredMixin, FilterFormMixin, ListView):
    template_name = 'utilisateurs.html'
    model = Utilisateur
    permission_required = 'common.change_utilisateur'
    filter_formclass = forms.UtilisateurFilterForm
    is_active = True
    paginate_by = 50

    def get_queryset(self):
        return super().get_queryset(
            base_qs=Utilisateur.objects.filter(
                is_active=self.is_active
            ).prefetch_related('groups').order_by('last_name')
        )


class UtilisateurUpdateView(PermissionRequiredMixin, CreateUpdateView):
    template_name = 'utilisateur_edit.html'
    permission_required = 'common.change_utilisateur'
    model = Utilisateur
    form_class = forms.UtilisateurForm
    success_url = reverse_lazy('utilisateur-list')

    def get_success_message(self, obj):
        if self.is_create:
            return f"Vous avez bien créé {obj}."
        else:
            return f"{obj} a bien été modifié"

    def form_valid(self, form):
        if self.is_create:
            pwd = get_random_string(length=10)
            form.instance.set_password(pwd)
            send_mail(
                "Nouveau compte pour application Croix-Rouge",
                "Bonjour\n\nUn nouveau compte a été créé pour vous:\n"
                f"Site: https://{settings.ALLOWED_HOSTS[0]}\n"
                f"Mot de passe: {pwd}\n\n"
                "Cordiales salutations",
                None,
                [form.instance.email]
            )
        return super().form_valid(form)


class UtilisateurOtpReinitView(PermissionRequiredMixin, View):
    permission_required = 'common.change_utilisateur'

    def post(self, request, *args, **kwargs):
        utilisateur = get_object_or_404(Utilisateur, pk=self.kwargs['pk'])
        utilisateur.totpdevice_set.all().delete()
        messages.success(
            request,
            f'Le 2ème facteur d’authentification de «{utilisateur}» a été réinitialisé. '
            'Lors de sa prochaine connexion, cette personne devra reconfigurer son accès '
            'avec un nouveau code QR.'
        )
        return HttpResponseRedirect(reverse('utilisateur-edit', kwargs=self.kwargs))
