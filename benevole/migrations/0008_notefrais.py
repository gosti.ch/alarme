from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("benevole", "0007_benevole_genre_non_blank"),
    ]

    operations = [
        migrations.CreateModel(
            name="NoteFrais",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("mois", models.DateField()),
                ("montant", models.DecimalField(decimal_places=2, max_digits=7)),
                (
                    "service",
                    models.CharField(
                        choices=[
                            ("alarme", "Alarme"),
                            ("transport", "Transport"),
                        ],
                        max_length=15,
                    ),
                ),
                ("kms", models.DecimalField(decimal_places=1, max_digits=5)),
                ("date_export", models.DateTimeField(blank=True, null=True)),
                ("id_externe", models.BigIntegerField(blank=True, null=True)),
                (
                    "benevole",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="benevole.benevole",
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name="notefrais",
            constraint=models.UniqueConstraint(
                fields=("benevole", "mois", "service"), name="mois_service_unique"
            ),
        ),
    ]
