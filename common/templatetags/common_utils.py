from datetime import date

from django.template import Library
from django.templatetags.static import static
from django.utils.dates import MONTHS
from django.utils.html import format_html
from django.utils.safestring import SafeString

from common.utils import RegionFinder
register = Library()


@register.filter
def get_item(obj, key):
    try:
        return obj.get(key) if obj is not None else obj
    except Exception:
        raise TypeError(f"Unable to get key '{key}' from obj '{obj}'")


@register.filter
def get_field(form, key):
    """Obtenir un champ de formulaire d'après son nom."""
    return form[key]


@register.filter
def get_attribute(instance, field_name):
    """Obtenir un champ de formulaire d'après son nom."""
    return getattr(instance, field_name, None)


@register.filter
def to_int(value):
    return int(value) if value else value


@register.filter
def can_edit(obj, user):
    return obj.can_edit(user)


@register.filter
def as_tel(no_tel):
    if no_tel:
        return format_html('<a class="tel" href="tel:{}">{}</a>', no_tel, no_tel)
    return ''


@register.filter
def as_tel_icon(no_tel):
    if no_tel:
        return format_html(
            '<a class="tel" href="tel:{}" title="{}" data-bs-toggle="tooltip"><img class="icon" src="{}"></a>',
            no_tel, f'Appeler {no_tel}',
            static('img/mobile.svg') if no_tel.startswith('07') else static("img/phone.svg"),
        )
    return ''


@register.filter
def as_email(email):
    if email:
        return format_html('<a href="mailto:{}">{}</a>', email, email)
    return ''


@register.filter
def as_email_icon(email):
    if email:
        return format_html(
            '<a href="mailto:{}" title="{}" data-bs-toggle="tooltip" class="opacity-75 ms-2"><img class="icon" src="{}"></a>',
            email, f'Envoyer courriel à {email}', static('img/email.svg'),
        )
    return ''


@register.filter
def boolean_icon(field_val):
    icon_url = static(
        "admin/img/icon-%s.svg" % {True: "yes", False: "no", None: "unknown"}[field_val]
    )
    return format_html('<img src="{}" alt="{}">', icon_url, field_val)


@register.filter(is_safe=True)
def strip_colon(txt):
    return txt.replace(' :', '')


@register.filter
def region(npa):
    return RegionFinder.get_region(npa)


@register.filter
def format_duree(duree, default=''):
    if not duree:
        return default
    elif not hasattr(duree, 'total_seconds'):
        return duree
    secondes = duree.total_seconds()
    heures = secondes // 3600
    minutes = (secondes % 3600) // 60
    return '{:02}:{:02}'.format(int(heures), int(minutes))


@register.filter
def strip_seconds(duree):
    if duree is not None and str(duree).count(':') > 1:
        return ':'.join(format_duree(duree).split(':')[:2])
    return duree


@register.filter
def date_passed(dt):
    return dt > date.today()


@register.filter
def mois_fr(dt):
    return MONTHS[dt.month]


@register.filter
def monetize(dec):
    return f'{dec:.2f}' if dec else 0


@register.simple_tag(takes_context=True)
def param_replace(context, **kwargs):
    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        d[k] = v
    for k in [k for k, v in d.items() if not v]:
        del d[k]
    return SafeString(d.urlencode())
