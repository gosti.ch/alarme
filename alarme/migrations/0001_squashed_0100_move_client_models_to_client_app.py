from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("benevole", "__first__"),
        ("client", "__first__"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Alarme",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "no_centrale",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="N° appareil à la centrale"
                    ),
                ),
                (
                    "no_appareil",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° d’appareil (BU, …)"
                    ),
                ),
                (
                    "no_serie",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° de série (SN)"
                    ),
                ),
                (
                    "carte_sim",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="Carte SIM"
                    ),
                ),
                (
                    "centrale",
                    models.CharField(
                        blank=True,
                        choices=[("curena", "Curena"), ("medicall", "Medicall")],
                        max_length=10,
                        verbose_name="Centrale d’alarme",
                    ),
                ),
                (
                    "date_achat",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date d’achat"
                    ),
                ),
                (
                    "prix_achat",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=7,
                        null=True,
                        verbose_name="Prix d’achat",
                    ),
                ),
                (
                    "fournisseur",
                    models.CharField(
                        blank=True, max_length=80, verbose_name="Fournisseur"
                    ),
                ),
                (
                    "en_rep_depuis",
                    models.DateField(
                        blank=True, null=True, verbose_name="Envoyé en réparation le"
                    ),
                ),
                (
                    "date_revision",
                    models.DateField(blank=True, null=True, verbose_name="Révision le"),
                ),
                (
                    "date_archive",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
                ("remarques", models.TextField(blank=True)),
                (
                    "chez_benevole",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="benevole.benevole",
                        verbose_name="Chez le bénévole",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ArticleFacture",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("code", models.CharField(max_length=25, unique=True)),
                (
                    "designation",
                    models.CharField(max_length=100, verbose_name="Désignation"),
                ),
                (
                    "prix",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=7,
                        null=True,
                        verbose_name="Montant",
                    ),
                ),
                (
                    "compte",
                    models.CharField(
                        blank=True, max_length=25, verbose_name="Compte de débit"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Materiel",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "no_ref",
                    models.CharField(
                        blank=True, max_length=30, verbose_name="N° de référence"
                    ),
                ),
                (
                    "no_centrale",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="N° matériel à la centrale"
                    ),
                ),
                (
                    "date_achat",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date d’achat"
                    ),
                ),
                (
                    "prix_achat",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=7,
                        null=True,
                        verbose_name="Prix d’achat",
                    ),
                ),
                (
                    "fournisseur",
                    models.CharField(
                        blank=True, max_length=100, verbose_name="Fournisseur"
                    ),
                ),
                (
                    "date_archive",
                    models.DateField(blank=True, null=True, verbose_name="Archivé le"),
                ),
                (
                    "chez_benevole",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="benevole.benevole",
                        verbose_name="Chez le bénévole",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="TypeMission",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=50)),
                (
                    "code",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("NEW", "Nouvelle installation"),
                            ("CHANGE", "Changement d’installation"),
                            ("UNINSTALL", "Suppression d’installation"),
                            ("VISITE", "Visite chez un client"),
                        ],
                        max_length=10,
                    ),
                ),
                (
                    "avec_client",
                    models.BooleanField(
                        default=True, verbose_name="Toujours liée avec un client"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="TypeMateriel",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=120)),
                (
                    "article_achat",
                    models.ForeignKey(
                        blank=True,
                        help_text="Si un article d’achat est défini, les appareils de ce modèle sont automatiquement facturés au client",
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.articlefacture",
                        verbose_name="Article pour achat",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="TypeAbo",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=100, unique=True)),
                (
                    "article",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.articlefacture",
                        verbose_name="Article",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ModeleAlarme",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("nom", models.CharField(max_length=100, unique=True)),
                (
                    "prix_location",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=7,
                        null=True,
                        verbose_name="Location mensuelle",
                    ),
                ),
                ("abos", models.ManyToManyField(blank=True, to="alarme.typeabo")),
                (
                    "article_achat",
                    models.ForeignKey(
                        blank=True,
                        help_text="Si un article d’achat est défini, les appareils de ce modèle sont automatiquement facturés au client",
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.articlefacture",
                        verbose_name="Article pour achat",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Mission",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField(blank=True)),
                ("delai", models.DateField(null=True, verbose_name="Délai")),
                (
                    "planifiee",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="Planifiée le"
                    ),
                ),
                (
                    "effectuee",
                    models.DateField(
                        blank=True, null=True, verbose_name="Effectuée le"
                    ),
                ),
                ("rapport", models.TextField(blank=True)),
                (
                    "duree_client",
                    models.DurationField(
                        blank=True, null=True, verbose_name="Durée (avec client)"
                    ),
                ),
                (
                    "duree_seul",
                    models.DurationField(
                        blank=True, null=True, verbose_name="Durée (sans client)"
                    ),
                ),
                (
                    "km",
                    models.DecimalField(
                        blank=True, decimal_places=1, max_digits=5, null=True
                    ),
                ),
                (
                    "abonnement",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="alarme.typeabo",
                    ),
                ),
                (
                    "alarme",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="alarme.alarme",
                    ),
                ),
                (
                    "benevole",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="benevole.benevole",
                        verbose_name="Bénévole",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="client.client",
                    ),
                ),
                (
                    "type_mission",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.typemission",
                        verbose_name="Type d’intervention",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="MaterielClient",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date_debut", models.DateField(verbose_name="Date d’installation")),
                (
                    "date_fin_abo",
                    models.DateField(
                        blank=True, null=True, verbose_name="Fin d’abonnement le"
                    ),
                ),
                (
                    "motif_fin",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("décès", "Décès"),
                            ("ems", "Départ en EMS"),
                            ("plus_utile", "Plus d’utilité"),
                            ("autre", "Autre motif (voir remarques)"),
                        ],
                        max_length=20,
                        verbose_name="Motif de résiliation",
                    ),
                ),
                (
                    "retour_mat",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date de retour du matériel"
                    ),
                ),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "abonnement",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.typeabo",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client"
                    ),
                ),
                (
                    "materiel",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.materiel",
                        verbose_name="Matériel",
                    ),
                ),
                (
                    "type_mat",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.typemateriel",
                        verbose_name="Type de matériel",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="materiel",
            name="type_mat",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                to="alarme.typemateriel",
                verbose_name="Type de matériel",
            ),
        ),
        migrations.CreateModel(
            name="JournalAlarme",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("description", models.TextField()),
                ("quand", models.DateTimeField()),
                (
                    "alarme",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="journaux",
                        to="alarme.alarme",
                        verbose_name="Alarme",
                    ),
                ),
                (
                    "qui",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "get_latest_by": "quand",
            },
        ),
        migrations.CreateModel(
            name="Installation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date_debut", models.DateField(verbose_name="Date d’installation")),
                (
                    "date_fin_abo",
                    models.DateField(
                        blank=True, null=True, verbose_name="Fin d’abonnement le"
                    ),
                ),
                (
                    "motif_fin",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("décès", "Décès"),
                            ("ems", "Départ en EMS"),
                            ("plus_utile", "Plus d’utilité"),
                            ("autre", "Autre motif (voir remarques)"),
                        ],
                        max_length=20,
                        verbose_name="Motif de résiliation",
                    ),
                ),
                (
                    "retour_mat",
                    models.DateField(
                        blank=True, null=True, verbose_name="Date de retour du matériel"
                    ),
                ),
                ("remarques", models.TextField(blank=True, verbose_name="Remarques")),
                (
                    "abonnement",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.typeabo",
                    ),
                ),
                (
                    "alarme",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT, to="alarme.alarme"
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="client.client"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Frais",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("descriptif", models.TextField(verbose_name="Descriptif")),
                (
                    "cout",
                    models.DecimalField(
                        decimal_places=2, max_digits=5, verbose_name="Coût"
                    ),
                ),
                (
                    "typ",
                    models.CharField(
                        choices=[("repas", "Repas"), ("autre", "Autre")],
                        max_length=6,
                        verbose_name="Type de frais",
                    ),
                ),
                (
                    "justif",
                    models.FileField(
                        blank=True,
                        upload_to="justificatifs",
                        verbose_name="Justificatif",
                    ),
                ),
                (
                    "mission",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="frais",
                        to="alarme.mission",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Facture",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "mois_facture",
                    models.DateField(
                        blank=True, null=True, verbose_name="Mois facturé"
                    ),
                ),
                ("date_facture", models.DateField(verbose_name="Date de facturation")),
                ("libelle", models.CharField(max_length=200, verbose_name="Libellé")),
                (
                    "montant",
                    models.DecimalField(
                        decimal_places=2, max_digits=7, verbose_name="Montant"
                    ),
                ),
                (
                    "exporte",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="Exporté vers compta"
                    ),
                ),
                (
                    "export_err",
                    models.TextField(blank=True, verbose_name="Erreur d’exportation"),
                ),
                ("id_externe", models.BigIntegerField(blank=True, null=True)),
                (
                    "article",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="alarme.articlefacture",
                        verbose_name="Article",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="factures",
                        to="client.client",
                    ),
                ),
                (
                    "install",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="factures",
                        to="alarme.installation",
                    ),
                ),
                (
                    "materiel",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="factures",
                        to="alarme.materielclient",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Alerte",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "fichier",
                    models.FileField(upload_to="alertes", verbose_name="Fichier"),
                ),
                ("alerte", models.TextField(blank=True)),
                ("recu_le", models.DateTimeField(verbose_name="Reçu le")),
                (
                    "traite_le",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="Traité le"
                    ),
                ),
                ("remarque", models.TextField(blank=True)),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="alertes",
                        to="client.client",
                    ),
                ),
                (
                    "mission",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="alarme.mission",
                    ),
                ),
                (
                    "par",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="alarme",
            name="modele",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                to="alarme.modelealarme",
                verbose_name="Modèle",
            ),
        ),
        migrations.AddConstraint(
            model_name="installation",
            constraint=models.UniqueConstraint(
                condition=models.Q(("date_fin_abo", None)),
                fields=("client",),
                name="client_current_unique",
            ),
        ),
    ]
