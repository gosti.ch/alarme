from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("transport", "0029_remove_transport_facturer_frais_annul"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="transport",
            options={
                "permissions": [
                    ("gestion_finances", "Autorisé à gérer finances et facturation")
                ]
            },
        ),
    ]
