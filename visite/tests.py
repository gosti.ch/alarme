from datetime import date, datetime, timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import make_aware

from benevole.models import Activites, Benevole
from client.models import Client
from common.export import openxml_contenttype
from common.models import Utilisateur
from .models import Attribution, Visite


class VisiteTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            Permission.objects.get(content_type__model='client', codename='change_client'),
            Permission.objects.get(content_type__model='benevole', codename='view_benevole'),
            Permission.objects.get(content_type__model='benevole', codename='change_benevole'),
            Permission.objects.get(content_type__model='visite', codename='view_visite'),
            Permission.objects.get(content_type__model='visite', codename='add_visite'),
            Permission.objects.get(content_type__model='attribution', codename='view_attribution'),
        )
        cls.visiteur1 = Benevole.objects.create(
            nom='Valjean', prenom='Jean', npa='2300', activites=[Activites.VISITES]
        )
        cls.visiteur2 = Benevole.objects.create(
            nom='Doe', prenom='Kyle', npa='2000', activites=[Activites.VISITES]
        )

    def test_create_client(self):
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-new'), data={
            'nom': 'Trucmuche',
            'prenom': 'Oscar',
            'genre': 'M',
            'rue': 'Grand-Rue 17',
            'npalocalite': '2000 Neuchâtel',
            'remarques_int_visite': "Pour beurre",
        })
        self.assertEqual(response.status_code, 302)
        client = Client.objects.get(nom='Trucmuche')
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        self.assertEqual(client.type_client, ['visite'])

    def test_convert_client_to_visite(self):
        self.client.force_login(self.user)
        client = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["alarme"],
        )
        response = self.client.post(reverse('client-edit', args=[client.pk]), data={
            'nom': 'Trucmuche',
            'prenom': 'Oscar',
            'npalocalite': '2000 Neuchâtel',
        })
        self.assertRedirects(response, reverse('client-edit', args=[client.pk]))
        client.refresh_from_db()
        self.assertEqual(client.type_client, ["alarme", "visite"])
        journal = client.journaux.latest()
        self.assertEqual(journal.description, "Ajout type de client «visite»")

    def test_client_list(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["visite"],
        )
        Client.objects.create(
            nom="Doe", prenom="John", npa="2000", localite="Neuchâtel", type_client=["alarme"],
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('clients'))
        self.assertQuerySetEqual(response.context['object_list'], [cl_visite])
        response = self.client.get(reverse('clients') + '?nom=Doe')
        self.assertQuerySetEqual(response.context['object_list'], [cl_visite])
        response = self.client.get(reverse('clients') + '?export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_benevole_list(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["visite"],
        )
        attr = Attribution.objects.create(
            client=cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )

        self.client.force_login(self.user)
        response = self.client.get(reverse('benevoles', args=['visite']))
        self.assertEqual(response.status_code, 200)
        self.assertQuerySetEqual(response.context['object_list'], [self.visiteur2, self.visiteur1])
        response = self.client.get(reverse('benevoles', args=['visite']) + '?sans_visite=on')
        self.assertQuerySetEqual(response.context['object_list'], [self.visiteur2])

    def test_edit_benevole(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('benevole-edit', args=['visite', self.visiteur1.pk]))
        self.assertContains(response, self.visiteur1.nom)

    def test_ajout_visite(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["visite"],
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('visite-new'), data={
            'client': str(cl_visite.pk),
            'visiteur': str(self.visiteur1.pk),
            'debut_0': '5.07.2023',
            'debut_1': '14:00',
            'duree': '2:00',
            'compte_rendu': '',
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': 'page'})

    def test_visite_list(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["visite"],
        )
        v1 = Visite.objects.create(
            client=cl_visite, visiteur=self.visiteur1,
            debut=make_aware(datetime(2023, 6, 25, 12, 0)), duree=timedelta(hours=2)
        )
        v2 = Visite.objects.create(
            client=cl_visite, visiteur=self.visiteur2,
            debut=make_aware(datetime(2023, 7, 8, 14, 30)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('visites'))
        self.assertQuerySetEqual(response.context['object_list'], [v2, v1])
        response = self.client.get(reverse('visites') + '?nom_client=Truc')
        self.assertQuerySetEqual(response.context['object_list'], [v2, v1])
        response = self.client.get(reverse('visites') + f'?visiteur={self.visiteur1.pk}')
        self.assertQuerySetEqual(response.context['object_list'], [v1])
        response = self.client.get(reverse('visites') + '?mois=7.2023')
        self.assertQuerySetEqual(response.context['object_list'], [v2])

    def test_attribution_list(self):
        cl_visite = Client.objects.create(
            nom="Trucmuche", prenom="Oscar", npa="2000", localite="Neuchâtel", type_client=["visite"],
        )
        attr = Attribution.objects.create(
            client=cl_visite, visiteur=self.visiteur1, debut=date.today() - timedelta(days=60)
        )
        last_m = date.today().replace(day=25) - timedelta(days=30)
        Visite.objects.create(
            client=cl_visite, visiteur=self.visiteur1,
            debut=make_aware(datetime(last_m.year, last_m.month, 25, 12, 0)), duree=timedelta(hours=2)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('attributions', args=['encours']))
        self.assertQuerySetEqual(response.context['object_list'], [attr])
        self.assertContains(response, "02:00")
        response = self.client.get(reverse('attributions', args=['terminees']))
        self.assertQuerySetEqual(response.context['object_list'], [])
