from django.urls import path

from besoins import views

app_name = "besoins"

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('clients/', views.ClientListView.as_view(), name='clients'),
    path('client/<int:pk>/quest/intro/', views.QuestionnaireView.as_view(), {'section': 'intro'}, name='q-intro'),
    path('client/<int:pk>/quest/<int:qpk>/<str:section>/', views.QuestionnaireView.as_view(), name='questionnaire'),
    path('client/<int:pk>/quest/resume/', views.QuestionnaireResumeView.as_view(), name='resume'),
    path('stats/', views.StatistiquesView.as_view(), name='stats-besoins'),
    path('non_repondus/', views.QuestNonRepondusView.as_view(), name='liste-non-repondus'),
]
