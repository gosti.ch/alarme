from django.contrib import admin

from .models import Attribution, Visite


@admin.register(Attribution)
class AttributionAdmin(admin.ModelAdmin):
    list_display = ['client', 'visiteur', 'debut', 'remarques']
    search_fields = ['client__nom', 'visiteur__nom']


@admin.register(Visite)
class VisiteAdmin(admin.ModelAdmin):
    list_display = ['client', 'visiteur', 'debut', 'duree']
    search_fields = ['client__nom', 'visiteur__nom']
