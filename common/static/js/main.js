'use strict';

function hide(selector_or_el) {
    if (typeof selector_or_el === 'string' || selector_or_el instanceof String) {
        document.querySelector(selector_or_el).setAttribute('hidden', true);
    } else {
        selector_or_el.setAttribute('hidden', true);
    }
}
function show(selector_or_el) {
    if (typeof selector_or_el === 'string' || selector_or_el instanceof String) {
        document.querySelector(selector_or_el).removeAttribute('hidden');
    } else {
        selector_or_el.removeAttribute('hidden');
    }
}

function openModal(ev) {
    const modalDiv = document.getElementById('siteModal');
    modalDiv.querySelector('.modal-dialog').style = '';
    if (modalDiv.dataset.oldhtmlcontent) {
        // Restore any content that was temporarily overwritten
        modalDiv.querySelector('.modal-content').innerHTML = modalDiv.dataset.oldhtmlcontent
        delete modalDiv.dataset.oldhtmlcontent;
    }
    const modal = new bootstrap.Modal(modalDiv);

    ev.preventDefault();
    const element = ev.currentTarget;
    if (!element.classList.contains('openmodal')) return false;  // because of ev. delegation
    ev.stopPropagation();
    const modalBody = modalDiv.querySelector('.modal-body'),
          modalWait = modalDiv.querySelector('.modal-wait');
    modalBody.innerHTML = '';
    hide(modalBody);
    let tout = setTimeout(() => show(modalWait), 400);
    modal.show();
    fetch(element.href || element.dataset.modalurl).then(resp => resp.text()).then(html => {
        clearTimeout(tout);
        modalBody.innerHTML = html;
        if (modalWait) hide(modalWait);
        show(modalBody);
        modalDiv.querySelector('.modal-title').textContent = (
            element.title || element.dataset.bsTitle || element.getAttribute("aria-label") || "Édition"
        );
        modalDiv.querySelector('#modal-save').textContent = element.dataset.savelabel || "Enregistrer";
        const frm = modalBody.querySelector('form');
        if (frm && frm.classList.contains('readonly')) {
            hide(modalDiv.querySelector('.modal-footer.edition'));
            show(modalDiv.querySelector('.modal-footer.readonly'));
        } else if (frm && !frm.classList.contains('no-button-bar')) {
            hide(modalDiv.querySelector('.modal-footer.readonly'));
            show(modalDiv.querySelector('.modal-footer.edition'));
            // Show/hide Supprimer button depending on data-deleteurl presence.
            if (element.dataset.deleteurl) {
                const modalDelete = modalDiv.querySelector('#modal-delete');
                modalDelete.textContent = element.dataset.deletelabel || "Supprimer";
                show(modalDelete);
                if (element.dataset.deletemethod) modalDelete.form.method = element.dataset.deletemethod;
                modalDelete.formaction = element.dataset.deleteurl;
                if (element.dataset.deleteconfirm) modalDelete.dataset.confirm = element.dataset.deleteconfirm;
            } else hide('#modal-delete');
        } else {
            hide(modalDiv.querySelector('.modal-footer.edition'));
        }
        attachModalHandlers(modalBody);
        modalDiv.dispatchEvent(new Event('containerfilled'));
    });
}

function syncTypeMissionAndAbo() {
    const tmField = document.getElementById('id_type_mission');
    const aboField = document.getElementById('id_abonnement');
    if (aboField && ['NEW', 'CHANGE'].includes(tmField.selectedOptions[0].dataset.code)) {
        show(aboField.closest('div'));
    } else if (aboField) {
        hide(aboField.closest('div'));
    }
}

function attachModalHandlers(modalBody) {
    attachHandlers(modalBody);
    // Hook form submit with modalSubmit
    modalBody.querySelectorAll('form').forEach(form => {
        form.addEventListener('submit', modalSubmit);
        form.addEventListener('change', monitorFormChange);
    });
    // Ensure save btn is enabled (after being disabled by avoidDoubleSubmission)
    const saveBtn = modalBody.parentNode.querySelector('#modal-save');
    if (saveBtn) saveBtn.removeAttribute('disabled');
    // Hide/show abo field in Mission form
    if (document.getElementById('id_type_mission')) {
        syncTypeMissionAndAbo();
        document.getElementById('id_type_mission').addEventListener('change', syncTypeMissionAndAbo);
    }
}

function setupAutocomplete(section) {
    section.querySelectorAll('.autocomplete').forEach(inp => {
        new Autocomplete(inp, {
            maximumItems: 10,
            onInput: async (ac, value) => {
                if (value) {
                    const resp = await fetch(`${inp.dataset.searchurl}?q=${value}`);
                    const data = await resp.json();
                    if (data.result && data.result == 'error') {
                        ac.setData([{label: `<i>Erreur: ${data.message}</i>`, value: ''}])
                    } else {
                        // data expected as list of {'label'/'value'} objects.
                        ac.setData(data);
                    }
                } else ac.setData({});
            },
            onSelectItem: (data) => {
                if (inp.dataset.pkfield) {
                    const pkInput = inp.form.querySelector(`input[name=${inp.dataset.pkfield}]`);
                    pkInput.value = data.value;
                    pkInput.dispatchEvent(new Event('change'));
                }
                inp.dispatchEvent(new CustomEvent('itemselected', {detail: data}));
            }
        });
    });
}

/* Submit a form from a modal
 * If the form has the class 'normal_submit', a normal form submit happens.
 * Else the form is submitted by AJAX, the modal is closed on success or
 * re-displayed if the form contains errors.
 */
function modalSubmit(ev) {
    ev.preventDefault();
    const form = ev.target.form || ev.currentTarget;
    // get it soon as it can change in the flow of the function
    const activeElement = document.activeElement;
    const modal = document.getElementById('siteModal');
    if (form.method == 'post') avoidDoubleSubmit(form);
    if (form.classList.contains('normal_submit')) {
        form.submit();
        bootstrap.Modal.getInstance(modal).hide();
        return;
    }
    const formUrl = activeElement.formaction || activeElement.getAttribute("formaction") || form.action;
    const valid = form.reportValidity();
    if (!valid) return;
    form.dataset.changed = "false";
    if (ev.target.tagName == 'BUTTON' && ev.target.dataset.confirm) {
        var resp = confirm(ev.target.dataset.confirm);
        if (!resp) return;
    }
    if (form.method == 'get') {
        fetch(formUrl).then(resp => resp.text()).then(html => {
            const modalContent = modal.querySelector('.modal-content');
            modal.dataset.oldhtmlcontent = modalContent.innerHTML;
            modalContent.innerHTML = html;
            attachModalHandlers(modal.querySelector('.modal-body'));
        });
        return;
    }

    const data = new FormData(form);
    addCSRFToken(data);
    fetch(formUrl, {
        method: 'POST',
        body: data
    }).then(resp => {
        const contentType = resp.headers.get("content-type");
        if (!contentType || !contentType.includes("application/json")) {
            return resp.text().then(html => {
                // redisplay form (with probable errors)
                const modalBody = modal.querySelector('.modal-body')
                modalBody.innerHTML = html;
                attachModalHandlers(modalBody);
                const submitBtn = modalBody.parentNode.querySelector('#modal-save');
                if (submitBtn) submitBtn.removeAttribute("disabled");
                throw new Error("The form did not validate.");
            });
        } else return resp.json();
    }).then(data => {
        // Post is success, hide modal and reload appropriate section
        bootstrap.Modal.getInstance(modal).hide();
        if (data.reload) reload(data.reload, data);
    }).catch(err => console.log(err.message));
}

function reload(selector, data) {
    if (selector == 'page') {
        window.location.reload();
    } else if (selector.startsWith('/')) {
        window.location.replace(selector);
    } else if (selector.startsWith('page#')) {
        window.location.replace(window.location.pathname + '#' + selector.split('#')[1]);
        window.location.reload();
    } else if (selector == 'populateAC') {
        // The value returned by the popup should be used to populate an autocomplete widget combi.
        document.querySelector(`#${data.target}`).value = data.object.value;
        document.querySelector(`#${data.target}_select`).value = data.object.label;
    } else {
        const reloadElement = document.querySelector(selector);
        reloadElement.classList.add('reloading');
        fetch(reloadElement.dataset.url, {method: 'GET'}).then(resp => resp.text()).then(html => {
            reloadElement.innerHTML = html;
            attachHandlers(reloadElement);
            reloadElement.classList.remove('reloading');
        });
    }
}

function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function submitAfterDelay(ev) {
    const code = (ev.keyCode || ev.which);
    // do nothing if it's an arrow key
    if (code == 37 || code == 38 || code == 39 || code == 40) {
        return;
    }
    submitForm(ev.target.form);
}

function addCSRFToken(fdata) {
    if (!fdata.has('csrfmiddlewaretoken')) {
        fdata.append('csrfmiddlewaretoken', document.querySelector('input[name=csrfmiddlewaretoken]').value);
    }
}

function submitFilterForm(form) {
    const params = new URLSearchParams(new FormData(form));
    // Set or remove bound_filter form class depending on form values
    const bound = Array.from(params.values()).some(val => val != '');
    bound ? form.classList.add('bound_filter') : form.classList.remove('bound_filter');
    const sep = form.action.includes('?') ? '&' : '?';
    fetch(`${form.action}${sep}${params}`, {
        headers: {'X-Requested-With': 'fetch'}
    }).then(resp => resp.text()).then(html => {
        var parser = new DOMParser();
        var doc = parser.parseFromString(html, "text/html");
        const fltTarget = form.dataset.target || '#filter_container';
        const oldBody = document.querySelector(fltTarget);
        const newBody = doc.querySelector(fltTarget);
        if (newBody) oldBody.replaceWith(newBody);
        else oldBody.innerHTML = '';
        const pagin = document.querySelector('#pagination');
        if (pagin) pagin.replaceWith(doc.querySelector('#pagination'));
        const listCount = document.querySelector('.list-count');
        if (listCount) listCount.replaceWith(doc.querySelector('.list-count'));
        if (newBody) attachHandlers(newBody);
        form.dispatchEvent(new Event('filterapplied'));
    });
}

function submitForm(form) {
    if (form.classList.contains('table-filter')) {
        submitFilterForm(form);
    } else if (form.classList.contains('ajax')) {
        const formData = new FormData(form);
        addCSRFToken(formData);
        return fetch(
            form.action,
            {method: 'POST', body: formData, headers: {'X-Requested-With': 'fetch'}}
        ).then(resp => resp.json()).then(data => {
            if (data.reload) reload(data.reload);
        });
    } else {
        form.submit();
    }
}

function deleteFile(ev) {
    const fileDiv = ev.currentTarget.parentNode;
    const rep = confirm("Voulez-vous réellement supprimer ce fichier ?");
    if (!rep) return;
    const formData = new FormData();
    addCSRFToken(formData);
    fetch(
        fileDiv.dataset.deleteurl,
        {method: 'POST', body: formData}
    ).then(resp => resp.json()).then(data => {
        if (data.reload) reload(data.reload);
    });
}

function markFormsetForDeletion(ev) {
    const fsetLine = ev.currentTarget.closest('.fset-line');
    const rep = confirm("Voulez-vous réellement effacer cette ligne ?");
    if (!rep) return;
    fsetLine.classList.add('formset-deleted');
    fsetLine.querySelector('input[id$="-DELETE"]').value = 'on';
}

function incrementTotalForms(form, numb) {
    const formTotal = form.querySelector('[id$="-TOTAL_FORMS"]');
    const totalValue = parseInt(formTotal.value);
    formTotal.value = totalValue + numb;
    return totalValue + numb;
}

/* Dynamically add new empty form in a formset. */
function cloneMore(ev) {
    ev.preventDefault();
    const form = ev.target.closest('form');
    const emptyForm = form.querySelector('.empty_form');
    let newElement = emptyForm.cloneNode(true);
    newElement.classList.remove('empty_form');
    const newTotal = incrementTotalForms(form, 1);
    newElement.querySelectorAll('input').forEach(inp => {
        for (const attr of inp.attributes) {
            if (attr.value.includes('__prefix__')) {
               inp.setAttribute(attr.name, attr.value.replace('__prefix__', newTotal - 1));
            }
        }
    });
    const plusBtn = newElement.querySelector('button.openmodal');
    if (plusBtn) plusBtn.dataset.modalurl = plusBtn.dataset.modalurl.replace('__prefix__', newTotal - 1);
    show(newElement);
    emptyForm.parentNode.insertBefore(newElement, emptyForm);
    return newElement;
}

const beforeUnloadListener = (event) => {
  event.preventDefault();
  return (event.returnValue = "Le formulaire a été modifié. Souhaitez-vous vraiment fermer cette fenêtre ?");
};

function monitorFormChange(ev) {
    ev.currentTarget.dataset.changed = "true";
}

function checkFormChanged(ev, parent) {
    const form = parent.querySelector('form:not(.unmonitor)');
    if (form && form.dataset.changed == "true") {
        const rep = confirm("Le formulaire a été modifié. Souhaitez-vous vraiment fermer cette fenêtre ?");
        if (!rep) {
            ev.preventDefault();
            return false;
        } else form.dataset.changed = "false";
    }
    return true;
}

function findInstanceDups(ev) {
    console.log(ev.target.value);
    const doublDiv = document.querySelector('#doublons');
    const searchTerm = ev.target.value;
    if (searchTerm.length < 2) {
        hide(doublDiv);
        return;
    }
    const params = new URLSearchParams(`q=${searchTerm}`);
    fetch(`${doublDiv.dataset.url}?${params}`).then(resp => resp.json()).then(data => {
        if (data.length) {
            const content = data.map(line => {
                const arch = line.archive ? " <i>(archivé-e)</i>" : "";
                return `<li><a href="${line.url}">${line.label}</a>, ${line.adresse}${arch}</li>`;
            }).join("\n");
            doublDiv.querySelector('#doublons_content').innerHTML = content;
            show(doublDiv);
        } else hide(doublDiv);
    });
}

function alarmSelected(ev) {
    const alarmeInp = document.getElementById('id_alarme');
    fetch(alarmeInp.dataset.jsonurl + '?pk=' + ev.detail.value).then(resp => resp.json()).then(json => {
        if (document.querySelector('#id_simspan')) document.querySelector('#id_simspan').remove();
        const SIMspan = ce(`<div id="id_simspan"><b>Carte SIM:</b> ${json[0].fields.carte_sim}</div>`);
        ev.target.parentNode.appendChild(SIMspan);
    }).catch((err) => {
       console.error(`Unable to get alarme details: ${err}`);
    });
}

function professionnelSelected(ev) {
    const profInp = document.getElementById('id_professionnel');
    const remDiv = document.getElementById('remarque_professionnel');
    if (remDiv) {
        remDiv.textContent = JSON.parse(ev.detail.details).remarque;
    }
}

function contactExistantSelected(ev) {
    // Recharger le formulaire avec données initiales = contact
    const modal = bootstrap.Modal.getInstance(document.querySelector('#siteModal'));
    document.querySelector('.ReferentForm').dataset.changed = "false";
    modal.hide()
    const btn = document.querySelector('#new_contact_button');
    const oldurl = btn.dataset.modalurl;
    btn.dataset.modalurl += `?copy_from=${ev.detail.value}`;
    btn.click();
    btn.dataset.modalurl = oldurl;
}

function calculerAge(ev) {
    let ageInput;
    if (ev === undefined) {
        ageInput = document.querySelector('#id_date_naissance');
        if (!ageInput) return;
    } else {
        ageInput = ev.target;
    }
    if (!ageInput.value) return;
    const diff = new Date().getTime() - new Date(ageInput.value).getTime();
    const age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
    if (age > 150) return;
    let span = ageInput.parentNode.querySelector('span.age');
    if (!span) {
        span = document.createElement("span");
        span.classList.add('age');
        ageInput.parentNode.append(span);
    }
    span.textContent = `${age} ans`
}

function avoidDoubleSubmit(form) {
    // Avoid double form submission by disabling the submit button during 5 secs.
    const submitBtn = Array.from(form.elements).filter(
        el => el.nodeName === "BUTTON" && el.getAttribute("type") == 'submit'
    ).shift();
    if (submitBtn) {
        submitBtn.setAttribute('disabled', true);
        setTimeout(() => submitBtn.removeAttribute('disabled'), 5000);
    }
}

function feedbackLongOperation(btn) {
    // Add waiting logo inside the button
    const svgEl = document.querySelector('#loader-1');
    const svgNew = svgEl.cloneNode(true);
    btn.disabled = true;
    btn.appendChild(document.createElement('br'));
    btn.appendChild(svgNew);
    return true;
}

/* Ask server for *real* distance/duration through an API */
async function calculateDistance(ev) {
    const response = await fetch(ev.target.dataset.url);
    const data = await response.json();
    const dist = Math.round(data.distance / 100) / 10;
    const time = Math.round(data.duration / 60);
    ev.target.replaceWith(` / ${dist} km (${time} min.)`);
}

/*
 * Avoid a hidden pk input keeping a selected value when the user types anything
 * else in the field after the selection.
 */
function clearPKfield(ev) {
    const pkInput = document.querySelector(`input[name=${ev.target.dataset.pkfield}]`);
    pkInput.value = '';
}

function attachHandlerSelector(section, selector, event, func) {
    section.querySelectorAll(selector).forEach(item => {
        item.addEventListener(event, func);
    });
}

function attachHandlers(section) {
    attachHandlerSelector(section, 'button[data-confirm], input[data-confirm]', 'click', (ev) => {
        let resp = confirm(ev.currentTarget.dataset.confirm);
        if (!resp) { ev.preventDefault(); }
    });
    attachHandlerSelector(section, 'form.debounce', 'submit', (ev) => avoidDoubleSubmit(ev.target));
    attachHandlerSelector(section, '.openmodal', 'click', openModal);
    attachHandlerSelector(
        section,
        'form.immediate_submit select, input:not([type=text]).immediate_submit, form.immediate_submit input[type=date], form.immediate_submit input[type=checkbox]',
        'change', (ev) => {
            submitForm(ev.target.form);
        }
    );
    attachHandlerSelector(
        section,
        'form.immediate_submit input[type=text], input[type=text].immediate_submit',
        'keyup',
        delay(submitAfterDelay, 500)
    );
    attachHandlerSelector(section, 'input[type=checkbox][data-visibilitytarget]', 'change', (ev) => {
        // For checkboxes able to show/hide some target section of a page
        const target = document.getElementById(ev.target.dataset.visibilitytarget);
        if (ev.target.checked) show(target);
        else hide(target);
    });
    attachHandlerSelector(section, 'input[data-pkfield]', 'input', clearPKfield);
    attachHandlerSelector(section, 'form.ajax', 'submit', (ev) => {
        ev.preventDefault();
        avoidDoubleSubmit(ev.target);
        submitForm(ev.target);
    });
    attachHandlerSelector(section, 'form.noenter', 'keydown', (ev) => {
        // Avoid Enter submitting the form
        if (ev.keyCode==13 && document.activeElement.tagName != "TEXTAREA") {ev.preventDefault(); return false;}
    });
    if (section.querySelector('#doublons')) {
        attachHandlerSelector(section, '#id_nom', 'keydown', delay(findInstanceDups, 700));
    }
    attachHandlerSelector(section, 'span.delete-link', 'click', deleteFile);
    attachHandlerSelector(section, '.delete-formset-link', 'click', markFormsetForDeletion);
    attachHandlerSelector(section, '.add_more', 'click', cloneMore);
    attachHandlerSelector(section, '.calc-dist-icon', 'click', calculateDistance);
    attachHandlerSelector(section, '#id_alarme_select', 'itemselected', alarmSelected);
    attachHandlerSelector(section, '#id_professionnel', 'itemselected', professionnelSelected);
    attachHandlerSelector(section, '#id_contact_existant', 'itemselected', contactExistantSelected);
    attachHandlerSelector(section, '#id_date_naissance', 'change', calculerAge);
    calculerAge();
    // Enable drag and drop for repondant/referent contact lists.
    section.querySelectorAll('.contact-list-orderable').forEach(list => attachDragDropHandlers(list, 'list-group-item'));
    section.querySelectorAll('.div-sortable').forEach(body => attachDragDropHandlers(body, 'row'));

    setupAutocomplete(section);
    // Initialize tooltips
    const tooltipTriggerList = [].slice.call(section.querySelectorAll('[data-bs-toggle="tooltip"]'))
    tooltipTriggerList.map((tooltipEl) => { return new bootstrap.Tooltip(tooltipEl) })
}

function loadURLInTab(tab) {
    // Dynamically load tab content from its href attribute.
    return fetch(tab.href, {headers: {'X-Requested-With': 'fetch'}})
    .then(resp => resp.text()).then(html => {
        const tabTarget = document.querySelector(tab.dataset.bsTarget);
        tabTarget.innerHTML = html;
        attachHandlers(tabTarget);
        return tab;
    });
}

function loadURLInSection(section) {
    // Dynamically load collapsed section content from its href attribute.
    return fetch(section.dataset.url, {headers: {'X-Requested-With': 'fetch'}})
    .then(resp => resp.text()).then(html => {
        section.innerHTML = html;
        attachHandlers(section);
        return section;
    });
}

var dragSrcEl = null;
function handleDragStart(ev) {
    dragSrcEl = this;
    ev.dataTransfer.effectAllowed = 'move';
    ev.dataTransfer.setData('text/html', this.outerHTML);
}
function handleDragOver(ev) {
    if (this.parentNode == dragSrcEl.parentNode) ev.preventDefault();
}
function handleDragEnter(ev) {
    ev.preventDefault();
}
function handleDrop(ev, childrenClass) {
    /* Optionnally, a data-confirm attribute on the parent node contains a confirm message.
     * The data-reorder attribute on parent node contains a URL to send the new pk list to.
     * A .counter node in each sortable children is used to number the elements.
     * Children elements are supposed to have the `childrenClass` class.
     */
    ev.stopPropagation();
    ev.preventDefault();
    const dropEl = ev.currentTarget;
    if (dragSrcEl === dropEl) return;

    const parent = dragSrcEl.parentNode;
    const content = ev.dataTransfer.getData('text/html');
    if (parent.dataset.confirm) {
        let resp = confirm(parent.dataset.confirm);
        if (!resp) return;
    }
    // Exchange positions by pushing lower lines
    const children = Array.from(parent.children).filter(el => el.classList.contains(childrenClass));
    let currentpos = 0, droppedpos = 0;
    for (let it=0; it<children.length; it++) {
      if (dragSrcEl == children[it]) { currentpos = it; }
      if (dropEl == children[it]) { droppedpos = it; }
    }
    if (currentpos < droppedpos) {
      parent.insertBefore(dragSrcEl, dropEl.nextSibling);
    } else {
      parent.insertBefore(dragSrcEl, dropEl);
    }

    function renumber() {
        // Update displayed numbering, or ORDER field numbering
        Array.from(parent.children).filter(
            el => el.classList.contains(childrenClass)
        ).forEach((el, idx) => {
            const counter = el.querySelector('.counter');
            if (counter) counter.textContent = idx + 1;
            const order = el.querySelector('[name$=ORDER]');
            if (order) order.value = idx + 1;
        });
    }

    const orderURL = parent.dataset.reorder;
    if (orderURL) {
        let formData = new FormData();
        formData.append('pks', Array.from(parent.querySelectorAll(`.${childrenClass}`)).map(child => child.dataset.contact));
        addCSRFToken(formData);
        fetch(orderURL, {method: 'POST', body: formData}).then(
            resp => resp.text()
        ).then(html => renumber());
    } else {
        renumber();
    }
}
function attachDragDropHandlers(list, childrenClass) {
    list.querySelectorAll(`.${childrenClass}`).forEach(item => {
        item.addEventListener('dragstart', handleDragStart);
        item.addEventListener('dragenter', handleDragEnter);
        item.addEventListener('dragover', handleDragOver);
        item.addEventListener('drop', (ev) => handleDrop(ev, childrenClass));
    });
}

/* event handler for when a drag started and mouse moves. */
var selected = null;
function mouseMoved (ev) {
    const xOffset = selected.mouseX - ev.pageX;
    const yOffset = selected.mouseY - ev.pageY;
    if (selected !== null) {
        selected.elem.style.left = (selected.x - xOffset) + 'px';
        selected.elem.style.top = (selected.y - yOffset) + 'px';
    }
}

window.addEventListener('DOMContentLoaded', () => {
    // Attach events for main modal.
    document.getElementById('modal-delete').addEventListener('click', modalSubmit);
    attachHandlers(document);
    // Client list search input
    const searchInput = document.querySelector('.selection_form #id_client');
    if (searchInput) {
        searchInput.addEventListener('keyup', delay(submitAfterDelay, 500));
        const len = searchInput.value.length;
        searchInput.setSelectionRange(len, len);
    }
    attachHandlerSelector(document, 'button.long-op', 'click', (ev) => {
        ev.preventDefault();
        const btn = ev.target;
        feedbackLongOperation(btn);
        btn.form.submit();
    });

    // Make boostrap modals draggable
    document.querySelector('.modal-header').addEventListener('mousedown', (ev) => {
        selected = {
            elem: ev.target.closest('.modal-dialog'),
            mouseX: ev.pageX,
            mouseY: ev.pageY
        };
        selected.x = selected.elem.style.left.replace('px', '') || 0;
        selected.y = selected.elem.style.top.replace('px', '') || 0;
        document.addEventListener('mouseup', (ev) => {
            selected = null;
            document.removeEventListener('mousemove', mouseMoved);
        }, {once: true});
        document.addEventListener('mousemove', mouseMoved);
        return false;
    });
    // Monitor forms for unsaved data
    if (!window.location.toString().includes('/login/')) {
        document.querySelectorAll('form:not(.unmonitor)').forEach(form => {
            form.addEventListener('change', (ev) => addEventListener('beforeunload', beforeUnloadListener));
            form.addEventListener('submit', (ev) => removeEventListener('beforeunload', beforeUnloadListener));
        });
    }
    // Avoid closing modal with changed form without notice.
    document.querySelector('#siteModal').addEventListener('hide.bs.modal', (ev) => checkFormChanged(ev, ev.currentTarget));

    // Show specific tab on home page if a location hash is present
    if (["/", "/app/"].includes(window.location.pathname) && window.location.hash) {
        const triggerEl = document.querySelector(`.nav-tabs .nav-link[data-bs-target="${window.location.hash}"]`);
        if (triggerEl) {
            bootstrap.Tab.getOrCreateInstance(triggerEl).show();
        }
    }
    attachHandlerSelector(document, '#appTabs .nav-link', 'shown.bs.tab', ev => {
        const target = ev.target.dataset.bsTarget.replace('#', '');
        history.pushState({tab: target}, "", `#${target}`);
    });

    attachHandlerSelector(document, '.dynamic-content', 'shown.bs.collapse', (ev) => {
        if (ev.target == ev.currentTarget) loadURLInSection(ev.target);
    });

    if (typeof autosize === "function") autosize(document.querySelectorAll('textarea'));
    // Alarme-specific
    attachHandlerSelector(document, '#home-tabs button[data-bs-toggle="tab"]', 'shown.bs.tab', ev => {
        const target = ev.target.dataset.bsTarget.replace('#', '');
        history.pushState({tab: target}, "", `/tab-${target}`);
    });
    attachHandlerSelector(document, '#home-tabs a[data-bs-toggle="tab"]', 'shown.bs.tab', ev => {
        loadURLInTab(ev.target);
    });
    const signCanvas = document.querySelectorAll('.signature_canvas');
    if (signCanvas.length) {
        const pads = [];
        signCanvas.forEach((canv) => {
            let pad = new SignaturePad(canv);
            pads.push(pad);
            canv.parentNode.querySelector('.clear').addEventListener('click', () => {
                pad.clear();
            });
        });
        document.querySelector('button#save').addEventListener('click', (ev) => {
            ev.preventDefault();
            let dataFilled = true;
            pads.forEach((pad) => {
                if (pad.isEmpty()) {
                    dataFilled = false;
                    return alert("Une signature est manquante.");
                }
                document.getElementById(pad.canvas.dataset.input).value = pad.toDataURL('image/png');
            });
            if (dataFilled) ev.target.form.submit();
        });
    }
});
