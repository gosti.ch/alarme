from datetime import date, timedelta
from pathlib import Path

from django.contrib.auth.models import Permission
from django.core.files import File
from django.test import TestCase
from django.urls import reverse

from common.models import Utilisateur
from common.test_utils import TempMediaRootMixin
from common.utils import current_app
from ..forms import ReferentForm
from ..models import AdresseClient, Client, Professionnel, ProfClient, Referent

NOT_SET = object()


class DataMixin:
    @classmethod
    def setUpTestData(cls):
        cls.user = Utilisateur.objects.create_user(
            'me@example.org', 'mepassword', first_name='Jean', last_name='Valjean',
        )
        cls.user.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'view_client', 'change_client', 'change_professionnel'
            ]))
        )

    def assertJournalMessage(self, obj, message, user=NOT_SET):
        journal = obj.journaux.latest()
        self.assertEqual(journal.description, message)
        self.assertEqual(journal.qui, self.user if user is NOT_SET else user)


class ClientTests(TempMediaRootMixin, DataMixin, TestCase):

    def test_titre(self):
        client = Client(
            nom='Dupond', prenom='Jules', genre='M',
        )
        self.assertEqual(client.titre, "Monsieur")
        client = Client(
            nom='Dupond', prenom='Julie', genre='F',
        )
        self.assertEqual(client.titre, "Madame")
        client = Client(
            nom='Dupond', prenom='Jules', genre='M',
            nom_part='Dupond', prenom_part='Julie', genre_part='F'
        )
        self.assertEqual(client.titre, "Monsieur/Madame")

    def test_noms_prenoms(self):
        client = Client(
            nom='Dupond', prenom='Jules', genre='M',
            nom_part='Dupond', prenom_part='Julie', genre_part='F'
        )
        self.assertEqual(client.noms, 'Dupond')
        self.assertEqual(client.prenoms, 'Jules et Julie')
        client = Client(
            nom='Dupond', prenom='Jules', genre='M',
            nom_part='Meier', prenom_part='Julie', genre_part='F'
        )
        self.assertEqual(client.noms, 'Dupond et Meier')
        self.assertEqual(client.prenoms, 'Jules et Julie')

    def test_langues(self):
        client = Client(
            nom='Dupond', prenom='Jules', genre='M', langues=None, type_client=['alarme']
        )
        self.assertEqual(client.get_langues_display(), "")
        client.langues = []
        self.assertEqual(client.get_langues_display(), "")
        client.langues = ['fr']
        self.assertEqual(client.get_langues_display(), "français")
        client.langues = ['fr', 'de', 'es']
        self.assertEqual(client.get_langues_display(), "français, allemand, espagnol")

    def test_client_add_fichier(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme', 'transport'])
        self.client.force_login(self.user)
        url = reverse('client-fichier-add', args=[client.pk])
        response = self.client.get(url)
        self.assertEqual(
            [ch[0] for ch in response.context['form'].fields['typ'].choices],
            ['doc', 'quest', 'contrat'] if current_app() == 'alarme' else ['doc']
        )
        with Path(__file__).open(mode='rb') as fh:
            response = self.client.post(url, data={
                'titre': 'Titre',
                'typ': 'doc',
                'fichier': File(fh),
            })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-fichiers'})
        self.assertEqual(client.fichiers.count(), 1)
        self.assertJournalMessage(client, "Ajout d’un fichier: Titre")
        # Une alerte est créée pour les autres apps
        self.assertEqual(client.alertes.count(), len(set(client.type_client) - {current_app()}))

    def test_client_autocomplete(self):
        Client.objects.create(nom="Smitter", prenom="Ladislas", type_client=['alarme'])
        Client.objects.create(nom="Smith", prenom="John", type_client=['transport'])
        Client.objects.create(nom="Rosmid", prenom="Esther", type_client=['transport', 'visite'])
        Client.objects.create(
            nom="Smitter", prenom="Lisy", type_client=['transport'], date_deces=date(2021, 10, 4)
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-autocomplete', args=['tous']))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(reverse('client-autocomplete', args=['tous']) + '?q=smi')
        self.assertEqual(len(response.json()), 3)
        response = self.client.get(reverse('client-autocomplete', args=['transport']) + '?q=smi')
        self.assertEqual(len(response.json()), 2)

    def test_adresse_new(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme'])
        AdresseClient.objects.create(client=client, npa='1234', localite='Quelquepart')
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'remarque': 'Résidence de vacances en été',
            'presences-TOTAL_FORMS': '0',
            'presences-INITIAL_FORMS': '0',
        })
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        client.refresh_from_db()
        self.assertEqual(client.adresseclient_set.count(), 2)
        self.assertJournalMessage(client, "Ajout de l’adresse «2345 Petaouchnok»")

    def test_adresse_new_with_presence(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme'])
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
            'rue': 'Av. Belair 3',
            'npalocalite': '2345 Petaouchnok',
            'presences-TOTAL_FORMS': '1',
            'presences-INITIAL_FORMS': '0',
            'presences-0-id': '',
            'presences-0-adresse': '',
            'presences-0-depuis': (date.today() - timedelta(days=5)).strftime('%Y-%m-%d'),
            'presences-0-jusqua': (date.today() + timedelta(days=5)).strftime('%Y-%m-%d'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(str(response.context['form'].errors) + str(response.context['form'].formset.errors))
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        self.assertEqual(client.adresseclient_set.count(), 1)
        self.assertEqual(client.adresseclient_set.first().presences.count(), 1)

    def test_adresse_new_hospitalisation(self):
        # Idem, mais pour hospitalisation
        client = Client.objects.create(nom="Dupont", prenom="Ladislas", type_client=['alarme'])
        self.client.force_login(self.user)
        five_days_ago = date.today() - timedelta(days=5)
        response = self.client.post(reverse('client-adresse-add', args=[client.pk]), data={
            'hospitalisation': 'on',
            'npalocalite': '',
            'presences-TOTAL_FORMS': '1',
            'presences-INITIAL_FORMS': '0',
            'presences-0-depuis': five_days_ago.strftime('%Y-%m-%d'),
        })
        if response['Content-Type'] != 'application/json':
            self.fail(str(response.context['form'].errors) + str(response.context['form'].formset.errors))
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(client, f"Ajout d’une hospitalisation (dès le {five_days_ago.strftime('%d.%m.%Y')})")

    def test_adresse_delete(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme'])
        adr = AdresseClient.objects.create(client=client, npa='1234', localite='Quelquepart')
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-adresse-delete', args=[client.pk, adr.pk]), data={})
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-addresses'})
        client.refresh_from_db()
        self.assertEqual(client.adresseclient_set.count(), 0)
        self.assertJournalMessage(client, "Suppression de l’adresse «1234 Quelquepart»")

    def test_referent_new(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        Referent.objects.create(client=client, nom='Schmid', referent=1)
        self.client.force_login(self.user)
        post_data = {
            'nom': 'Tintin',
            'prenom': 'Roger',
            'npalocalite': '2345 Petaouchnok',
            'relation': 'Fils',
            'referent': '',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 3,
            'referenttel_set-0-tel': '032 111 11 11',
            'referenttel_set-1-tel': '074 444 11 11',
        }
        if current_app() == 'alarme':
            post_data.update({
                'est_referent': 'on',
                'repondant': '',
                'est_repondant': 'on',
                'courrier_envoye': 'on',
            })
        response = self.client.post(reverse('client-referent-add', args=[client.pk]), data=post_data)
        self.assertEqual(response.json(), {'result': 'OK', 'reload': '#client-contacts'})
        self.assertEqual(client.referents.count(), 2)
        referent = client.referents.get(nom='Tintin')
        if current_app() == 'alarme':
            self.assertEqual(referent.repondant, 1)
            self.assertEqual(referent.referent, 2)
            self.assertEqual(referent.courrier_envoye, date.today())
        self.assertEqual(referent.telephones().count(), 2)
        self.assertEqual(sorted(referent.referenttel_set.values_list('priorite', flat=True)), [1, 2])

    def test_referent_autres_fonctions(self):
        referent = Referent(client=None, nom='Schmid', admin=True, facturation_pour=['al-tout'])
        self.assertEqual(
            referent.autres_fonctions(),
            "Contact administratif et technique, Adresse de facturation (Alarme (tout))"
        )

    def test_referent_edit(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=None)
        self.client.force_login(self.user)
        form_data = {
            'nom': 'Schmid',
            'prenom': 'Roger',
            'npalocalite': '2345 Petaouchnok',
            'relation': '',
            'referent': '',
            'repondant': '',
            'est_repondant': 'on',
            'courrier_envoye': '',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 2,
            'referenttel_set-0-tel': '032 111 11 11',
            'referenttel_set-1-tel': '078 777 77 77',
        }
        response = self.client.post(reverse('client-referent-edit', args=[client.pk, referent.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        referent.refresh_from_db()
        self.assertEqual(referent.referenttel_set.count(), 2)
        if current_app() == 'alarme':
            self.assertEqual(referent.repondant, 1)
            msg = (
                "Modification du contact «Roger Schmid, répondant (1) pour Dupond Ladislas» (ajout de prénom («Roger»), "
                "répondant (de «faux» à «vrai»), ajout de npa («2345»), ajout de localité («Petaouchnok»), "
                "ajout de téléphone de référent, ajout de téléphone de référent)"
            )
        else:
            msg = (
                "Modification du contact «Roger Schmid,  pour Dupond Ladislas» (ajout de prénom («Roger»), "
                "ajout de npa («2345»), ajout de localité («Petaouchnok»), "
                "ajout de téléphone de référent, ajout de téléphone de référent)"
            )
            referent.repondant = 1
            referent.save()
        self.assertJournalMessage(
            client, msg
        )
        form_data.update({
            'npa': '2345',
            'localite': 'Petaouchnok',
            'repondant': '1',
            'referenttel_set-INITIAL_FORMS': 2,
            'referenttel_set-0-id': referent.referenttel_set.all()[0].pk,
            'referenttel_set-0-tel': '032 111 11 12',
            'referenttel_set-0-ORDER': '1',
            'referenttel_set-1-id': referent.referenttel_set.all()[1].pk,
            'referenttel_set-1-DELETE': 'on',
        })
        response = self.client.post(reverse('client-referent-edit', args=[client.pk, referent.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            client,
            "Modification du contact «Roger Schmid, répondant (1) pour Dupond Ladislas» "
            "(téléphone de référent: numéro (de «032 111 11 11» à «032 111 11 12»), "
            "suppression de téléphone de référent)"
        )

    def test_referent_facturation_pour(self):
        """Une adresse complète est obligatoire pour les adresses de facturation."""
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=None)
        form_data = {
            'nom': 'Schmid',
            'prenom': 'Roger',
            'rue': '',
            'npalocalite': '',
            'relation': '',
            'referent': '',
            'repondant': '',
            'est_repondant': 'on',
            'courrier_envoye': '',
            'facturation_pour': ['transp'],
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(client=client, instance=referent, data=form_data, initial={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'rue': ['La rue est obligatoire pour les contacts de facturation'],
            'npalocalite': ['La localité est obligatoire pour les contacts de facturation'],
        })

    def test_referent_other_country(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        form_data = {
            'nom': 'Schmid',
            'prenom': 'Roger',
            'referenttel_set-INITIAL_FORMS': 0,
            'referenttel_set-TOTAL_FORMS': 0,
        }
        form = ReferentForm(
            client=client,
            data={**form_data, 'npalocalite': '80027 Amiens', 'pays': 'FR'},
            initial={'langues': ['fr']}
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        form.save()
        self.assertEqual(form.instance.get_pays_display(), "France")
        self.assertEqual(form.instance.localite, "Amiens")
        self.assertEqual(form.instance.npa, "80027")
        self.assertEqual(form.instance.rue_localite(), "FR-80027 Amiens")

        form = ReferentForm(
            client=client,
            data={**form_data, 'npalocalite': 'Nothingham BFG 43R', 'pays': 'GB'},
            initial={'langues': ['fr']}
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        form.save()
        self.assertEqual(form.instance.get_pays_display(), "Grande-Bretagne")
        self.assertEqual(form.instance.localite, "Nothingham BFG 43R")
        self.assertEqual(form.instance.npa, "")
        self.assertEqual(form.instance.rue_localite(), "GB- Nothingham BFG 43R")

    def test_client_desarchiver(self):
        client = Client.objects.create(
            nom="Dupond", prenom="Ladislas", type_client=['alarme', 'transport'], archive_le='2022-05-20'
        )
        referent = Referent.objects.create(
            client=client, nom='Schmid', repondant=1, date_archive='2022-05-20'
        )
        # client-archive is a toggle, will be reactivated if already archived
        self.client.force_login(self.user)
        response = self.client.post(reverse('client-archive', args=[client.pk]))
        self.assertRedirects(response, client.get_absolute_url())
        client.refresh_from_db()
        referent.refresh_from_db()
        self.assertIsNone(client.archive_le)
        self.assertIsNone(referent.date_archive)
        self.assertEqual(client.type_client, [current_app()])

    def test_referent_archive(self):
        today = date.today()
        client = Client.objects.create(nom="Dupond", prenom="Ladislas")
        if client.get_absolute_url() is None:
            self.skipTest("Client not editable")
        referent = Referent.objects.create(client=client, nom='Schmid', repondant=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('client-referent-delete', args=[client.pk, referent.pk]), data={})
        self.assertContains(response, "Souhaitez vous vraiment archiver Schmid, répondant (1) pour Dupond Ladislas")
        response = self.client.post(reverse('client-referent-delete', args=[client.pk, referent.pk]), data={
            'date_archive': today.strftime('%Y-%m-%d'),
        })
        self.assertRedirects(response, client.get_absolute_url())
        referent.refresh_from_db()
        self.assertEqual(referent.date_archive, today)
        self.assertEqual(len(client.referents), 0)
        # Si la date archive est dans le futur, le référent est encore actif
        referent = Referent.objects.create(
            client=client, nom='Schmid', repondant=1, date_archive=today + timedelta(days=3)
        )
        self.assertEqual(len(client.referents), 1)


class ProfessionnelTests(DataMixin, TestCase):
    def test_professionnel_list(self):
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnels'))
        self.assertQuerySetEqual(response.context['object_list'], [pro])

    def test_professionnel_new(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnel-new'))
        response = self.client.post(reverse('professionnel-new'), data={
            'type_pro': 'Médecin',
            'nom': 'Dr Maboule',
            'rue': '',
            'npalocalite': '2345 Petaouchnok',
            'tel_1': '076 111 11 22',
        })
        self.assertEqual(response.json()['result'], 'OK')
        pro = Professionnel.objects.get(nom__contains='Maboule')
        self.assertEqual(pro.localite, 'Petaouchnok')

    def test_professionnel_archive(self):
        client = Client.objects.create(nom="Dupond", prenom="Ladislas", type_client=['alarme', 'transport'])
        pro = Professionnel.objects.create(
            type_pro='Médecin', nom='Dr Jekill', npa='2345', localite='Petaouchnok', tel_1='022 222 00 00'
        )
        ProfClient.objects.create(professionnel=pro, client=client, priorite=1)
        self.client.force_login(self.user)
        response = self.client.get(reverse('professionnels'))
        self.assertContains(
            response,
            'data-deleteconfirm="Voulez-vous vraiment archiver cette personne ? ATTENTION: elle '
            'sera retirée de 1 clients avec lesquels elle est actuellement liée."'
        )
        response = self.client.post(reverse('professionnel-archive', args=[pro.pk]), data={})
        self.assertEqual(response.json()['result'], 'OK')
        self.assertJournalMessage(
            client,
            "Suppression du professionnel «Dr Jekill (Petaouchnok, Médecin)» qui a été archivé.",
            user=self.user
        )
