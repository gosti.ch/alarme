from django.contrib import admin
from django.db.models import Count

from common.admin import DateAsBooleanListFilter, ExportAction
from . import models


class ArrayFieldListFilter(admin.SimpleListFilter):
    """An admin list filter for ArrayFields."""

    def lookups(self, request, model_admin):
        """Return the filtered queryset."""
        queryset_values = model_admin.model.objects.values_list(
            self.parameter_name, flat=True
        )
        values = []
        for sublist in queryset_values:
            if sublist:
                for value in sublist:
                    if value:
                        values.append((value, value))
            else:
                values.append(("null", "-"))
        return sorted(set(values))

    def queryset(self, request, queryset):
        """Return the filtered queryset."""
        lookup_value = self.value()
        if lookup_value:
            lookup_filter = (
                {"{}__isnull".format(self.parameter_name): True}
                if lookup_value == "null"
                else {"{}__contains".format(self.parameter_name): [lookup_value]}
            )
            queryset = queryset.filter(**lookup_filter)
        return queryset


class TypeClientListFilter(ArrayFieldListFilter):
    title = "Type client"
    parameter_name = "type_client"


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    ordering = ['nom', 'prenom']
    list_display = ['nom_prenom', 'rue', 'npa', 'localite', 'type_client']
    list_filter = ['tarif_avs_acquis', TypeClientListFilter, ('archive_le', DateAsBooleanListFilter)]
    search_fields = ['nom']
    actions = [ExportAction("Liste clients")]


@admin.register(models.AdresseClient)
class AdresseClientAdmin(admin.ModelAdmin):
    pass


@admin.register(models.AdressePresence)
class AdressePresenceAdmin(admin.ModelAdmin):
    list_display = ['client', 'depuis', 'jusqua', 'remarque']

    def client(self, obj):
        return obj.adresse.client


class ReferentTelInline(admin.TabularInline):
    model = models.ReferentTel


@admin.register(models.Referent)
class ReferentAdmin(admin.ModelAdmin):
    list_display = ['nom', 'prenom', 'rue', 'npa', 'localite', 'client', 'relation', 'roles']
    ordering = ['nom', 'prenom']
    search_fields = ['nom', 'complement', 'client__nom']
    actions = [ExportAction("Liste contacts et répondants")]
    inlines = [ReferentTelInline]


@admin.register(models.Professionnel)
class ProfessionnelAdmin(admin.ModelAdmin):
    list_display = ['nom', 'type_pro', 'npa', 'localite', 'tel_1', 'courriel', 'num_clients']
    list_filter = ['type_pro']
    ordering = ['nom']
    search_fields = ['nom']
    actions = [ExportAction("Liste professionnels")]

    @admin.display(description='Nbre de clients')
    def num_clients(self, obj):
        return obj.num_clients

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(num_clients=Count('client'))


@admin.register(models.ProfClient)
class ProfClientAdmin(admin.ModelAdmin):
    list_display = ['client', 'professionnel', 'priorite']


@admin.register(models.Journal)
class JournalAdmin(admin.ModelAdmin):
    list_display = ['quand', 'client', 'description']
    date_hierarchy = 'quand'
    list_filter = [
        ('fichier', admin.EmptyFieldListFilter),
        'qui'
    ]
    search_fields = ['client__nom', 'description']


@admin.register(models.Alerte)
class AlerteAdmin(admin.ModelAdmin):
    list_display = ['recu_le', 'client', 'alerte', 'traite_le']
    list_filter = ['traite_le']
