= Installation

This project can be installed like any standard Django project. However, as it
calls some geo-related methods, it requires the GeoDjango libraries.

Python dependencies are in `requirements.txt`, or `requirements_dev.txt` for
development purposes.

You should then create a custom settings file in `settings.__init__.py`. See
`settings/README` for more about the file contents.

== Initial database population

After the standard `python manage.py migrate` command to intiailize the database,
a custom command can be run to populate the database with minimal production-like
data:
    python manange.py populate_database
