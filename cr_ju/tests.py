from datetime import date, timedelta
from decimal import Decimal

from freezegun import freeze_time

from django.conf import settings
from django.core import mail
from django.db.models import Count
from django.test import TestCase, tag
from django.test.utils import override_settings
from django.urls import reverse
from django.utils import timezone

from alarme.models import (
    Alarme, ArticleFacture, Facture, Installation, ModeleAlarme,
    TypeMission
)
from alarme.tests.test_general import TestUtils
from client.models import Client, Referent


@override_settings(COMPTA_EMAIL='compta@example.org')
class JUTests(TestUtils, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ArticleFacture.objects.bulk_create([
            ArticleFacture(
                code='TAXE2', designation="Frais d'installation et boîtier à clés", prix=150, compte='3410/00'
            ),
            ArticleFacture(
                code='REFACT MONTRE LIMMEX', designation="Appareil Limmex 2.1", prix=390
            ),
            ArticleFacture(
                code='ALRM', designation="Service d’alarme mensuel", prix=25.5, compte='3410/10'
            ),
        ])
        ArticleFacture.objects.filter(code=settings.CODES_ARTICLE_INSTALLATION[0]).update(compte='3410/00')

    @tag("factures")
    @freeze_time("2022-03-10")
    def test_generer_factures_en_avance(self):
        installs = self.create_client_with_installations(2, debut_install=date(2022, 2, 25))
        for install in installs:
            # Cela va créer les factures manquantes, y compris la taxe d'installation.
            install.save()
        # Fin le mois suivant, la 3ème facture ne doit pas être générée.
        Installation.objects.filter(pk=installs[1].pk).update(
            date_fin_abo = date(2022, 5, 25)
        )
        num_factures = Installation.generer_factures(date(2022, 4, 1))
        self.assertEqual(num_factures, 5)
        self.assertEqual(
            sorted(Facture.objects.values_list('date_facture').annotate(nbr=Count('date_facture')).distinct()),
            [
                (date(2022, 3, 10), 4),  # 2 taxe install + 2 factures mens.
                (date(2022, 4, 1), 2),
                (date(2022, 5, 1), 2),
                (date(2022, 6, 1), 1)
            ]
        )

    @tag("factures")
    @freeze_time("2022-03-25")
    def test_generer_factures_en_avance_apres_20_du_mois(self):
        installs = self.create_client_with_installations(2, debut_install=date(2022, 3, 22))
        autre_cl = Client.objects.create(nom='Donzé', prenom='Nouveau')
        # Il faut qu'il existe au moins une facture pour chaque mois du trimestre prochain pour que des
        # factures individuelles soient créées pour ces mois.
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-04-01', date_facture='2022-03-04', montant=2, article=self.article
        )
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-05-01', date_facture='2022-03-04', montant=2, article=self.article
        )
        Facture.objects.create(
            client=autre_cl, mois_facture='2022-06-01', date_facture='2022-03-04', montant=2, article=self.article
        )
        # Fin le mois suivant, la 3ème facture ne doit pas être générée.
        Installation.objects.filter(pk=installs[1].pk).update(
            date_fin_abo = date(2022, 5, 25)
        )
        num_factures = Installation.generer_factures(date(2022, 3, 25))
        self.assertEqual(num_factures, 7)
        self.assertEqual(
            sorted(Facture.objects.exclude(
                client=autre_cl
            ).values_list('date_facture').annotate(nbr=Count('date_facture')).distinct()),
            [
                (date(2022, 3, 25), 2),  # 2 taxe install (pas de facture pour mars)
                (date(2022, 4, 1), 2),
                (date(2022, 5, 1), 2),
                (date(2022, 6, 1), 1)
            ]
        )

    @tag("factures")
    def test_export_factures_csv(self):
        article = ArticleFacture.objects.get(code='ALRM')
        installs = self.create_client_with_installations(3)
        factures = []
        # facture déjà exportée
        factures.append(Facture(
            client=installs[0].client, article=article,
            date_facture=date.today(), libelle="Déjà exportée", montant=1, exporte=timezone.now()
        ))
        # facture avec client sans no debiteur
        installs[1].client.no_debiteur = ''
        installs[1].client.save()
        factures.append(Facture(
            client=installs[1].client, article=article,
            date_facture=date.today(), libelle="Sans no débiteur", montant=1,
        ))
        factures.extend([
            # 2 factures avec même client/mois_facture.
            # Dans une version précédente, ce cas donnait lieu à une facture unique à plusieurs articles.
            # La CR-JU a ensuite préféré revenir à un article = une facture.
            Facture(
                client=installs[2].client, article=article,
                date_facture=date(2022, 4, 3), mois_facture=date(2022, 4, 1),
                libelle="Article 1", montant=12.5,
            ),
            Facture(
                client=installs[2].client, article=article,
                date_facture=date(2022, 4, 3), mois_facture=date(2022, 4, 1),
                libelle="Abo’22 : Mme Charpié", montant=15.5,
            ),
            Facture(
                client=installs[2].client,
                article=ArticleFacture.objects.get(code=settings.CODES_ARTICLE_INSTALLATION[0]),
                date_facture=date(2022, 4, 8), mois_facture=date(2022, 4, 1),
                libelle="Frais d'installation", montant=80,
            ),
        ])
        factures = Facture.objects.bulk_create(factures)

        self.client.force_login(self.user)
        response = self.client.post(reverse('factures-export'))
        lines = {line.split(';')[0]: line for line in response.content.decode('cp1252').splitlines()}
        self.assertTrue(lines['ID_FACTURE'].startswith('ID_FACTURE;DATE_FACTURE;ID_CLIENT;FACTURATION_ID;CODE_ARTICLE;'))
        self.assertEqual(len(lines), 4)
        fact3_no = str(90000 + factures[2].pk)
        self.assertEqual(
            lines[fact3_no],
            f'{fact3_no};03.04.2022;2;;ALRM;3410/10;avril 2022;;;1;F;12.50;12.50;5;0;ALAR'
        )
        fact5_no = str(90000 + factures[4].pk)
        self.assertEqual(
            lines[fact5_no],
            f'{fact5_no};08.04.2022;2;;TAXE;3410/00;;;;1;F;80.00;80.00;5;0;ALAR'
        )
        # Facture.exporte est rempli
        self.assertFalse(Facture.objects.exclude(client__no_debiteur='').filter(exporte__isnull=True).exists())

    @tag("factures")
    def test_facture_apres_nouvelle_install(self):
        """
        Quand une nouvelle installation est créée:
         - les factures mensuelles du prochain trimestre sont créées
        """
        self.client.force_login(self.user)
        with freeze_time("2022-02-16"):
            client = self.nouvelle_install_par_mission()
            # Should have created 3 bills (install + 02 + 03)
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-02-01', '2022-02-16', '37.45'),  # 1er mois
                 ('2022-02-16', '2022-02-16', '80.00'),  # Frais install
                 ('2022-03-01', '2022-03-01', '37.45')  # Mois suivant complet
                ],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture), str(fac.montant))
            )
        with freeze_time("2022-03-26"):
            # Facturation du trimestre suivant
            Installation.generer_factures(date(2022, 4, 1), date_factures=date(2022, 3, 26))
            self.assertEqual(
                [str(fact.montant) for fact in client.factures.filter(date_facture__gte=date(2022, 3, 26))],
                ['37.45', '37.45', '37.45']
            )

    @tag("factures")
    def test_facture_apres_nouvelle_install_meme_mois(self):
        self.client.force_login(self.user)
        with freeze_time("2022-03-04"):
            client = self.nouvelle_install_par_mission()
            # Should have created only bill for 03 (next quarter not yet billed) + installation
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-03-01', '2022-03-04'), ('2022-03-04', '2022-03-04')],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture))
            )

    @tag("factures")
    def test_factures_trimestre_suivant_si_deja_commence(self):
        # Facture pour prochain trimestre (qui va signaler au système que la facturation
        # du prochain trimestre est déjà entamée).
        Facture.objects.create(
            client=Client.objects.create(nom='Test'),
            date_facture=date(2022, 4, 1), mois_facture=date(2022, 4, 1),
            article=self.article, montant=55,
        )
        with freeze_time("2022-03-04"):
            self.client.force_login(self.user)
            client = self.nouvelle_install_par_mission()
            # Should have created bills for 03/04/05/06 (next quarter already billed)
            self.assertQuerySetEqual(
                client.factures.order_by('date_facture', 'montant'),
                [('2022-03-01', '2022-03-04'),
                 ('2022-03-04', '2022-03-04'),  # Install
                 ('2022-04-01', '2022-04-01'),
                 ('2022-05-01', '2022-05-01'),
                 ('2022-06-01', '2022-06-01')],
                transform=lambda fac: (str(fac.mois_facture), str(fac.date_facture))
            )

    @tag("factures")
    @freeze_time("2022-12-03")
    def test_facture_install_limmex(self):
        article = ArticleFacture.objects.get(code='REFACT MONTRE LIMMEX')
        modele_limmex = ModeleAlarme.objects.create(
            nom='Limmex 2.1', article_achat=article,
        )
        modele_limmex.abos.add(self.abo)
        alarme = Alarme.objects.create(modele=modele_limmex, no_serie=999)
        self.client.force_login(self.user)
        client = self.nouvelle_install_par_mission(alarme=alarme)
        self.assertQuerySetEqual(
            client.factures.order_by('montant'),
            [Decimal('37.45'), Decimal(80), Decimal(390)],
            transform=lambda f: f.montant
        )

    @tag("factures")
    @freeze_time("2022-12-03")
    def test_facture_install_boitier_cle(self):
        """
        Si le client possède un boîtier clés Croix-Rouge, les frais d'installation
        sont majorés du coût du boîtier, par la séelction d'un article de
        facturation approprié.
        """
        client = self.create_client_with_installations(
            1, client_data=[{'boitier_cle': 'c-r'}], debut_install=date.today()
        )[0]
        Installation.generer_factures(date.today())
        self.assertQuerySetEqual(
            client.factures.all().order_by('-montant'),
            [('TAXE2', Decimal(150)), ('ABO1', Decimal('37.45'))],
            transform=lambda f: (f.article.code, f.montant)
        )

    def test_mission_install_duree_seul(self):
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', no_debiteur='123987', rue="Fleurs 432", npa="2800", localite="Delémont"
        )
        in4days = date.today() + timedelta(days=4)
        form_data = {
            'description': "Installation chez Mme Trucmuche",
            'type_mission': TypeMission.objects.create(nom='Installation', code='NEW').pk,
            'planifiee_0': in4days.strftime('%d.%m.%Y'),
            'planifiee_1': '15:30',
            'delai': (date.today() + timedelta(days=10)).strftime('%d.%m.%Y'),
        }
        self.client.force_login(self.user)
        response = self.client.post(reverse('mission-new', args=[client.pk]), data=form_data)
        self.assertEqual(response.json()['result'], 'OK')
        mission = client.mission_set.first()
        self.assertEqual(mission.duree_seul.total_seconds(), 30 * 60)

    def test_message_apres_nouvelle_install(self):
        """
        Quand une nouvelle installation est créée:
         - un message est envoyé à la comptabilité pour créer le client
        """
        self.client.force_login(self.user)
        client = Client.objects.create(
            nom='Donzé', prenom='Léa', no_debiteur='123987', rue="Fleurs 432", npa="2800", localite="Delémont"
        )
        Referent.objects.create(client=client, nom='Schmid', referent=1, facturation_pour=[])
        client.refresh_from_db()
        client = self.nouvelle_install_par_mission(client=client)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['compta@example.org'])
        message = (
            "Un nouvel abonnement alarme a été créé pour Donzé Léa. Merci de "
            "bien vouloir créer un nouveau client dans votre système comptable "
            "et de communiquer le n° de débiteur à la personne responsable des alarmes.\n\n"
            "Client:\n"
            "Donzé Léa\n"
            "Fleurs 432\n"
            "2800 Delémont\n"
            "N° débiteur : 123987\n"
            "\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        )
        self.assertEqual(mail.outbox[0].body, message)

    def test_fin_abo_envoi_message(self):
        install = self.create_client_with_installations(1)[0]

        # Un changement d'alarme ne doit pas envoyer de message
        nouvelle_alarme = Alarme.objects.create(modele=ModeleAlarme.objects.first(), no_serie=999)
        self.client.force_login(self.user)
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('install-edit', args=[install.pk]), data={
                'client': install.client_id,
                'abonnement': install.abonnement_id,
                'date_debut': install.date_debut.strftime('%d.%m.%Y'),
                'nouvelle_alarme': nouvelle_alarme.pk,
                'date_changement': date.today().strftime('%d.%m.%Y'),
            })
        self.assertEqual(response.json(), {"result": "OK", "reload": "page"})
        self.assertEqual(len(mail.outbox), 0)

        install = install.client.alarme_actuelle()
        with self.captureOnCommitCallbacks(execute=True):
            response = self.client.post(reverse('install-close', args=[install.pk]), data={
                'date_fin_abo': date.today(),
                'motif_fin': 'décès',
            })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['compta@example.org'])
        message = (
            "L’abonnement alarme de Donzé Léa0 a été résilié pour le {today}. "
            "Veuillez vérifier si des factures en cours doivent être annulées."
            "\n\n--\n"
            "Ceci est un message automatique, ne pas répondre"
        ).format(today=date.today().strftime('%d.%m.%Y'))
        self.assertEqual(mail.outbox[0].body, message)
