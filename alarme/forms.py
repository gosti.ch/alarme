from binascii import a2b_base64
from datetime import date
from io import BytesIO

from django import forms
from django.db.models import Count, Max, Prefetch, Q
from django.db.models.functions import TruncMonth
from django.urls import reverse, reverse_lazy

from benevole.models import Activites, Benevole
from client.models import Client
from common.forms import (
    BootstrapMixin, BSCheckboxSelectMultiple, BSRadioSelect, DateInput,
    HiddenDeleteInlineFormSet, HMDurationField, FormsetMixin, ModelForm,
    NPALocaliteMixin, PriceInput, SplitDateTimeWidget
)
from client.forms import ClientEditFormBase, ClientFilterFormBase
from client.models import Referent
from .models import (
    Alarme, ArticleFacture, Facture, Frais, Installation, Materiel, MaterielClient, Mission,
    ModeleAlarme, TypeAbo, TypeMateriel, TypeMission
)


class ClientFilterForm(ClientFilterFormBase):
    LIST_CHOICES = (
        ('', '-------'),
        ('actifs', "Clients actuellement actifs"),
        ('no_fact', "Clients sans envoi facture"),
        ('autres_debit', "Clients avec autre débiteur"),
        ('resil', "Clients en résiliation"),
    )
    recherche_part = True
    fact_items = ['al-abo', 'al-tout']

    def filter(self, clients):
        clients = super().filter(clients)
        if self.cleaned_data['listes'] == 'actifs':
            clients = clients.actifs_entre(date.today(), date.today())
        elif self.cleaned_data['listes'] == 'no_fact':
            clients = clients.filter(envoi_facture=False).prefetch_related(
                Prefetch('referent_set', to_attr="debiteurs", queryset=Referent.objects.filter(
                    date_archive=None, facturation_pour__overlap=['al-abo', 'al-tout']
                ))
            )
            clients._hints['_export_autres_debiteurs'] = True
        elif self.cleaned_data['listes'] == 'resil':
            clients = clients.annotate(
                num_resilies=Count('installation', filter=Q(installation__date_fin_abo__isnull=False)),
                num_non_resilies=Count('installation', filter=Q(installation__date_fin_abo__isnull=True)),
                date_resiliation_annot=Max('installation__date_fin_abo')
            ).filter(
                num_resilies__gt=0, num_non_resilies=0
            ).order_by('-date_resiliation_annot')
        return clients


class VisiteursChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return f"{obj.nom_prenom}, {obj.npa} {obj.localite} ({obj.nb_visites})"


class ClientForm(NPALocaliteMixin, ClientEditFormBase):
    situation_choix = forms.ChoiceField(choices=(
        (Client.SITUATION_VIE_SEUL, 'Seul-e'),
        (Client.SITUATION_VIE_COUPLE, 'En couple / avec partenaire'),
        ('Autre', 'Autre'),
    ), widget=BSRadioSelect, required=False)
    situation_autre = forms.CharField(max_length=80, required=False)
    visiteur = VisiteursChoiceField(
        label="Visiteur",
        queryset=None,
        required=False,
    )
    courrier_envoye = forms.BooleanField(label="Le courrier de bienvenue a été envoyé", required=False)

    class Meta(ClientEditFormBase.Meta):
        fields = [
            'nom', 'prenom', 'genre', 'date_naissance',
            'nom_part', 'prenom_part', 'genre_part', 'date_naissance_part', 'tel_part',
            'c_o', 'rue', 'case_postale',
            'npa', 'localite', 'tel_1', 'tel_2', 'courriel', 'langues',
            'type_logement', 'type_logement_info', 'etage', 'nb_pieces', 'ascenseur',
            'code_entree', 'boitier_cle', 'cles',
            'situation_vie', 'prest_compl', 'no_debiteur', 'envoi_facture', 'animal_comp',
            'visiteur', 'spec_alarme', 'date_deces', 'accord_police', 'courrier_envoye',
            'remarques_int_alarme', 'remarques_ext',
        ]
        widgets = {
            **ClientEditFormBase.Meta.widgets,
            'situation_vie': forms.HiddenInput,  # Remplacé par situation_choix/situation_autre
            'cles': forms.Textarea,
        }
    benev_hidden_fields = {
        'prest_compl', 'no_debiteur', 'envoi_facture', 'courrier_envoye', 'visiteur',
        'date_deces', 'remarques_int_alarme'
    }

    def __init__(self, instance=None, **kwargs):
        if instance and instance.pk:
            kwargs['initial']['courrier_envoye'] = bool(instance.courrier_envoye)
            if instance.situation_vie in [Client.SITUATION_VIE_SEUL, Client.SITUATION_VIE_COUPLE]:
                kwargs['initial']['situation_choix'] = instance.situation_vie
            elif instance.situation_vie:
                kwargs['initial']['situation_choix'] = 'Autre'
                kwargs['initial']['situation_autre'] = instance.situation_vie
        super().__init__(instance=instance, **kwargs)
        if 'visiteur' in self.fields:
            self.fields['visiteur'].queryset = Benevole.objects.par_activite(
                Activites.VISITE, include=instance.visiteur if instance else None
            ).annotate(
                nb_visites=Count('visites', filter=Q(visites__archive_le__isnull=True))
            )

    def clean(self):
        data = super().clean()
        # Convertir situation_choix/situation_autre en texte pour situation_vie
        if 'situation_autre' in self.changed_data and data['situation_autre']:
            data['situation_vie'] = data['situation_autre']
            self.changed_data.append('situation_vie')  # Pour journalisation
        elif 'situation_choix' in self.changed_data and 'situation_choix' in data:
            data['situation_vie'] = data['situation_choix']
            self.changed_data.append('situation_vie')  # Pour journalisation
        if 'courrier_envoye' in self.changed_data:
            data['courrier_envoye'] = date.today() if data['courrier_envoye'] else None
        else:
            data['courrier_envoye'] = self.instance.courrier_envoye
        return data


class DonneesMedicForm(BootstrapMixin, ModelForm):
    """Désérialiser donnees_medic JSONField et resérialiser on save."""
    troubles_eloc = forms.CharField(max_length=200, required=False)
    troubles_aud = forms.CharField(max_length=200, required=False)
    allergies = forms.CharField(max_length=200, required=False)
    empl_medic = forms.CharField(max_length=200, required=False)
    diabete = forms.NullBooleanField(required=False)
    troubles_cardio = forms.NullBooleanField(required=False)
    epilepsie = forms.NullBooleanField(required=False)
    oxygene = forms.NullBooleanField(required=False)
    anticoag = forms.NullBooleanField(required=False)
    directives = forms.CharField(max_length=200, required=False)
    autres = forms.CharField(max_length=200, required=False)

    class Meta(BootstrapMixin.Meta):
        model = Client
        fields = ['donnees_medic']
        widgets = {
            'donnees_medic': forms.HiddenInput,
        }

    def __init__(self, **kwargs):
        kwargs['initial'] = {
            ff: (kwargs['instance'].donnees_medic or {}).get(ff)
            for ff in self.declared_fields.keys()
        }
        kwargs['instance'].client = None  # Workaround issue with form.instance.client in general_edit.html
        super().__init__(**kwargs)
        for fname in self.fields:
            self.fields[fname].label = Client.DONNEES_MEDIC_MAP.get(fname)

    def save(self, **kwargs):
        self.instance.donnees_medic = {key: self.cleaned_data[key] for key in self.declared_fields.keys()}
        self.instance.save(update_fields=['donnees_medic'])
        return self.instance


class EnvoiCentraleForm(BootstrapMixin, forms.Form):
    subject = forms.CharField(label="Sujet", max_length=100)
    body = forms.CharField(label="Corps du message", widget=forms.Textarea)


class SigField(forms.CharField):
    def to_python(self, value):
        if value not in self.empty_values:
            return BytesIO(a2b_base64(value.split(',')[1]))
        return value


class QuestionnaireForm(forms.Form):
    sig_data = SigField(required=True)
    lieu = forms.CharField(required=True)


class ContratForm(forms.Form):
    sig_data_cr = SigField(required=True)
    sig_data_cl = SigField(required=True)
    lieu = forms.CharField(required=True)


class FacturerForm(BootstrapMixin, forms.Form):
    date_facture = forms.DateField(label="Date de la facture", widget=DateInput, required=True)


class CancelWithCommentForm(BootstrapMixin, forms.Form):
    comment = forms.CharField(label="Remarque sur l’annulation", required=True, widget=forms.Textarea)


class FactureForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Facture
        fields = ['date_facture', 'article']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['article'].queryset = self.fields['article'].queryset.exclude(prix=None)

    def _get_changes(self):
        changes = [ch for ch in super()._get_changes() if not ch.startswith('article')]
        for label, old, new in self.suppl_changes:
            changes.append("{} (de «{}» à «{}»)".format(label, old, new))
        return changes

    def _post_clean(self):
        super()._post_clean()
        self.suppl_changes = []
        if self.instance.libelle != self.instance.article.designation:
            self.suppl_changes.append(('Libellé', self.instance.libelle, self.instance.article.designation))
            self.instance.libelle = self.instance.article.designation
        if self.instance.montant != self.instance.article.prix:
            self.suppl_changes.append(('Montant', self.instance.montant, self.instance.article.prix))
            self.instance.montant = self.instance.article.prix
        if self.instance.mois_facture is None:
            self.instance.mois_facture = self.instance.date_facture


class FactureFilterForm(BootstrapMixin, forms.Form):
    date_facture = forms.ChoiceField(choices=(), required=False)
    nom_client = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    libelle = forms.CharField(
        widget=forms.TextInput(attrs={'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    article = forms.ModelChoiceField(queryset=ArticleFacture.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Remplir date_facture choices avec les 16 derniers mois.
        cur_month = date.today().month
        cur_year = date.today().year
        months = []
        for i in range(16):
            if cur_month < 1:
                cur_month = 12
                cur_year -= 1
            months.append((cur_month, cur_year))
            cur_month -= 1
        self.fields['date_facture'].choices = (('', '--------'),) + tuple(
            [(f'{m[0]:02}.{m[1]}', f'{m[0]:02}.{m[1]}') for m in months]
        )

    def filter(self, factures):
        if self.cleaned_data['date_facture']:
            month, year = self.cleaned_data['date_facture'].split('.')
            factures = factures.annotate(month=TruncMonth('date_facture')
                ).filter(month=date(year=int(year), month=int(month), day=1))
        if self.cleaned_data['nom_client']:
            factures = factures.filter(client__nom__icontains=self.cleaned_data['nom_client'])
        if self.cleaned_data['libelle']:
            factures = factures.filter(libelle__icontains=self.cleaned_data['libelle'])
        if self.cleaned_data['article']:
            factures = factures.filter(article=self.cleaned_data['article'])
        return factures


class InstallationForm(BootstrapMixin, ModelForm):
    alarme = forms.ModelChoiceField(queryset=Alarme.objects.all(), widget=forms.HiddenInput)
    alarme_select = forms.CharField(label="Appareil", widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('alarme-search'),
        'data-pkfield': 'alarme',
    }))

    class Meta(BootstrapMixin.Meta):
        model = Installation
        fields = [
            'client', 'alarme', 'alarme_select', 'abonnement', 'date_debut', 'date_fin_abo',
            'motif_fin', 'retour_mat', 'remarques'
        ]

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        if kwargs['initial'].get('client'):
            self.fields['client'].widget = forms.HiddenInput()
        if kwargs['initial'].get('alarme'):
            del self.fields['alarme_select']
        if instance is None:
            # For new installs, limit choices to instances without any current installation
            self.fields['client'].queryset = Client.objects.annotate(
                current_installs=Count('installation', filter=Q(installation__date_fin_abo=None))
            ).filter(current_installs=0)
            self.fields['alarme'].queryset = Alarme.objects.non_installees()
            del self.fields['date_fin_abo']
            del self.fields['motif_fin']
            del self.fields['retour_mat']
        else:
            del self.fields['alarme_select']
            del self.fields['alarme']

    def clean(self):
        cleaned_data = super().clean()
        if (
            (cleaned_data.get('date_fin_abo') and not cleaned_data.get('motif_fin')) or
            (cleaned_data.get('motif_fin') and not cleaned_data.get('date_fin_abo'))
        ):
            raise forms.ValidationError("La fin d’abonnement et le motif de fin doivent être indiqués ensemble.")


class UninstallForm(BootstrapMixin, ModelForm):
    # https://gitlab.com/claudep/alarmes/-/issues/194
    mat_achete = forms.BooleanField(
        label="Matériel propriété du client (sera immédiatement archivé)",
        required=False
    )

    class Meta(BootstrapMixin.Meta):
        model = Installation
        fields = ['date_fin_abo', 'motif_fin', 'mat_achete', 'remarques']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['date_fin_abo'].required = True
        self.fields['motif_fin'].required = True


class MaterielClientForm(BootstrapMixin, ModelForm):
    materiel = forms.IntegerField(widget=forms.HiddenInput, required=False)
    materiel_select = forms.CharField(label="Matériel", widget=forms.TextInput(attrs={
        'class': 'autocomplete',
        'autocomplete': 'off',
        'data-searchurl': reverse_lazy('materiel-search'),
        'data-pkfield': 'materiel',
    }), required=False)

    class Meta(BootstrapMixin.Meta):
        model = MaterielClient
        fields = [
            'materiel', 'materiel_select', 'type_mat', 'abonnement', 'date_debut', 'date_fin_abo',
            'retour_mat', 'remarques'
        ]

    def __init__(self, *args, instance=None, **kwargs):
        if instance is not None:
            kwargs['initial']['materiel_select'] = str(instance.materiel)
        super().__init__(*args, instance=instance, **kwargs)
        self.fields['type_mat'].queryset = self.fields['type_mat'].queryset.order_by('nom')

    def clean_materiel(self):
        pk_val = self.cleaned_data.get('materiel')
        if pk_val:
            return Materiel.objects.get(pk=pk_val)
        return pk_val

    def clean(self):
        cleaned_data = super().clean()
        materiel = cleaned_data.get('materiel')
        if not materiel and not cleaned_data.get('type_mat'):
            self.add_error(None, "Vous devez indiquer un matériel précis ou un type de matériel")
        # Vérification de la compatibilité entre le type de matériel et l'abonnement choisi.
        abo = cleaned_data.get('abonnement')
        if materiel and abo and not materiel.abo_compatible(abo):
            self.add_error(
                'abonnement', f"Ce matériel n’est pas compatible avec l’abonnement {abo}"
            )
        elif cleaned_data.get('type_mat') and abo and not abo in cleaned_data['type_mat'].abos.all():
            self.add_error(
                'abonnement', f"Ce type de matériel n’est pas compatible avec l’abonnement {abo}"
            )
        return cleaned_data


class TypeMissionSelect(forms.Select):
    option_template_name = 'widgets/typemission_select_option.html'


class FraisMissionForm(BootstrapMixin, ModelForm):
    class Meta:
        model = Frais
        fields = ['cout', 'descriptif', 'justif']
        labels = {
            'descriptif': "Descriptif des frais",
        }
        widgets = {
            'descriptif': forms.TextInput,
            'cout': PriceInput,
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('cout') == 0:
            self.add_error(None, "Si vous n’avez pas de frais, laissez plutôt ces champs vides.")
        return cleaned_data


class FraisFormsetMixin(FormsetMixin):
    toujours_frais = False

    def __init__(self, data=None, files=None, instance=None, **kwargs):
        super().__init__(data=data, files=files, instance=instance, **kwargs)
        if instance is None and not self.toujours_frais:
            self.formset = None
        else:
            has_frais = instance is not None and instance.frais.exists()
            FraisFormSet = forms.inlineformset_factory(
                Mission, Frais, form=FraisMissionForm, formset=HiddenDeleteInlineFormSet,
                extra=0 if has_frais else 1
            )
            self.formset = FraisFormSet(instance=instance, data=data, files=files)


class MissionForm(BootstrapMixin, FraisFormsetMixin, ModelForm):
    alarme_search_url = reverse_lazy('alarme-search')

    class Meta(BootstrapMixin.Meta):
        model = Mission
        fields = [
            'type_mission', 'description', 'delai', 'planifiee', 'effectuee',
            'alarme', 'abonnement', 'benevole', 'rapport', 'duree_client', 'duree_seul', 'km'
        ]
        widgets = {
            'type_mission': TypeMissionSelect,
            'planifiee': SplitDateTimeWidget,
            'effectuee': DateInput(attrs={'class': 'inline'}),
            'alarme': forms.HiddenInput(attrs={'data-jsonurl': reverse_lazy('alarme-json')}),
            'abonnement': forms.Select(attrs={'class': 'form-select'}),
        }
        field_classes = {
            'planifiee': forms.SplitDateTimeField,
            'duree_client': HMDurationField,
            'duree_seul': HMDurationField,
        }

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        alarme_field = self.fields.pop('alarme')
        if 'type_mission' in self.fields:
            self.fields['type_mission'].queryset = TypeMission.objects.filter(avec_client=True).order_by('nom')
        if 'benevole' in self.fields:
            self.fields['benevole'].queryset = Benevole.objects.par_domaine('alarme', actifs=True)
        if instance is None:
            del self.fields['effectuee']
            del self.fields['duree_client']
            del self.fields['duree_seul']
            del self.fields['km']
        elif instance.pk and not instance.type_mission.avec_client:
            del self.fields['duree_client']
        # Si nouvelle install/changement install et mission.pk: ajout champs alarme (+ abo)
        if (
            instance and instance.pk and instance.type_mission.code in {"NEW", "CHANGE"} and
            instance.effectuee is None
        ):
            self.fields['alarme'] = alarme_field
            self.fields['alarme_select'] = forms.CharField(
                label="N° de série (SN)",
                widget=forms.TextInput(attrs={
                    'class': 'form-control autocomplete',
                    'autocomplete': 'off',
                    'data-searchurl': self.alarme_search_url,
                    'data-pkfield': 'alarme',
                }),
                required=False
            )
            if 'abonnement' in self.fields:
                if instance.type_mission.code in {"NEW", "CHANGE"}:
                    self.fields['abonnement'].queryset = TypeAbo.objects.all().order_by('nom')
                else:
                    del self.fields['abonnement']
            self.order_fields(self.field_order)
        elif instance and instance.pk:
            if 'alarme' in self.fields:
                del self.fields['alarme']
            if 'abonnement' in self.fields:
                del self.fields['abonnement']

    def clean_effectuee(self):
        date_eff = self.cleaned_data['effectuee']
        if date_eff and date_eff > date.today():
            raise forms.ValidationError(
                "Il n’est pas autorisé de mettre une date d’intervention effectuée dans le futur"
            )
        return date_eff

    def clean(self):
        cleaned_data = super().clean()
        alarme = cleaned_data.get('alarme')
        if cleaned_data.get('effectuee') and 'alarme' in self.fields:
            if not alarme:
                self.add_error(
                    'alarme_select' if 'alarme_select' in self.fields else 'alarme',
                    "Vous devez indiquer l’appareil installé/à installer."
                )
            if 'abonnement' in self.fields and not cleaned_data.get('abonnement'):
                self.add_error(
                    'abonnement',
                    "Vous devez indiquer l’abonnement choisi."
                )
        # Vérification de la compatibilité entre le modèle d'appareil et l'abonnement choisi.
        if alarme and cleaned_data.get('abonnement') and not alarme.abo_compatible(cleaned_data['abonnement']):
            self.add_error(
                'alarme_select',
                f"L’appareil {alarme} n’est pas compatible avec l’abonnement {cleaned_data['abonnement']}"
            )
        return cleaned_data


class MissionSimpleCreateForm(BootstrapMixin, ModelForm):
    """
    Création d'une mission avec champs minimaux, par ex. lors de la clôture d'une
    installation pour une mission de récupération du matériel.
    """
    class Meta(BootstrapMixin.Meta):
        model = Mission
        fields = ['type_mission', 'description', 'delai', 'benevole']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # To allow submitting an empty form
        self.fields['delai'].required = False
        self.fields['benevole'].queryset = Benevole.objects.par_domaine('alarme', actifs=True)


class VisiteCreateForm(BootstrapMixin, ModelForm):
    """Formulaire simplifié pour créer une visite depuis l'app bénévole."""
    class Meta(BootstrapMixin.Meta):
        model = Mission
        fields = ['planifiee', 'effectuee', 'duree_client', 'duree_seul', 'km']
        widgets = {
            'planifiee': SplitDateTimeWidget,
            'effectuee': DateInput,
        }
        field_classes = {
            'planifiee': forms.SplitDateTimeField,
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['planifiee'].required = True


class MissionSansClientForm(BootstrapMixin, FraisFormsetMixin, ModelForm):
    """Activité d'un bénévole non liée à un client."""
    sans_client = True
    toujours_frais = True

    class Meta(BootstrapMixin.Meta):
        model = Mission
        fields = ['type_mission', 'description', 'effectuee', 'duree_seul', 'km']
        field_classes = {
            'duree_seul': HMDurationField,
        }
        labels = {
            'duree_seul': "Durée",
        }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['type_mission'].queryset = TypeMission.objects.filter(avec_client=False).order_by('nom')


class BenevoleMissionForm(MissionForm):
    """Édition d'une mission par un bénévole (champs limités)"""
    alarme_search_url = reverse_lazy('benevole-alarme-search')

    class Meta(MissionForm.Meta):
        fields = [
            'alarme', 'abonnement', 'planifiee', 'effectuee', 'rapport', 'duree_client', 'duree_seul', 'km'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('alarme_select', None)
        if 'alarme' in self.fields:
            # Remplacer le champ autocomplété par un select, car le stock bénévole n'est pas si grand.
            self.fields['alarme'] = forms.ModelChoiceField(
                label="Appareil", queryset=self.instance.benevole.alarme_set.all(), required=False,
            )
            self.fields['alarme'].widget.attrs['class'] = 'form-control'
        # Laisser le champ abonnement seulement si pas encore rempli par gestionnaires.
        if self.instance.abonnement:
            self.fields.pop('abonnement', None)

    def clean(self):
        cleaned_data = super().clean()
        rapport_fields = {'duree_client', 'duree_seul'}
        if 'duree_client' not in self.fields:
            rapport_fields.remove('duree_client')
        # Si l'intervention est marquée comme "effectuée", tous les champs de rapport doivent être renseignés
        # (à voir si ce contrôle doit être poussé dans MissionForm)
        if cleaned_data.get('effectuee'):
            for field_name in rapport_fields:
                if cleaned_data.get(field_name) in (None, '') and field_name not in self.errors:
                    self.add_error(field_name, "Vous devez compléter ce champ")
        # Si la date planifiée est saisie, l'alarme doit être définie
        if cleaned_data.get('planifiee') and 'alarme_select' in self.fields and not cleaned_data.get('alarme'):
            self.add_error(
                'alarme_select',
                "L’appareil à installer doit être défini pour informer la centrale."
            )
        return cleaned_data


class AlarmeForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Alarme
        exclude = ['centrale', 'no_centrale', 'date_archive']

    def __init__(self, instance=None, **kwargs):
        super().__init__(instance=instance, **kwargs)
        self.fields['modele'].widget.attrs['data-url'] = reverse('refresh-select', args=['ModeleAlarme'])
        self.fields['chez_benevole'].queryset = Benevole.objects.par_domaine(
            'alarme', actifs=True, include=instance.chez_benevole if instance else None
        )

    def _changed_value(self, fname, detail=False):
        if fname == 'chez_benevole':
            if self.cleaned_data[fname]:
                return f'chez le bénévole {self.cleaned_data[fname]}'
            else:
                return 'retour au stock principal'
        return super()._changed_value(fname, detail=detail)


class ModeleAlarmeForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = ModeleAlarme
        fields = '__all__'


class AlarmeFilterForm(BootstrapMixin, forms.Form):
    modele = forms.ModelChoiceField(queryset=ModeleAlarme.objects.all().order_by('nom'), required=False)
    no_serie = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'N°', 'autocomplete': 'off', 'size': '3'}),
        required=False
    )
    carte_sim = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'SIM', 'autocomplete': 'off', 'size': '3'}),
        required=False
    )
    nom_client = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    en_rep = forms.BooleanField(label="En réparation", required=False)
    en_rev = forms.BooleanField(label="À réviser", required=False)
    en_stock = forms.ChoiceField(
        choices=(('', '---'), ('on', 'En stock'), ('off', 'Chez les clients')),
        required=False
    )
    archived = forms.BooleanField(label="Archivés", required=False)

    def filter(self, alarmes):
        if self.cleaned_data['archived']:
            alarmes = alarmes.filter(date_archive__isnull=False)
        if self.cleaned_data['modele']:
            alarmes = alarmes.filter(modele=self.cleaned_data['modele'])
        if self.cleaned_data['no_serie']:
            alarmes = alarmes.filter(no_serie__icontains=self.cleaned_data['no_serie'])
        if self.cleaned_data['carte_sim']:
            alarmes = alarmes.filter(carte_sim__icontains=self.cleaned_data['carte_sim'])
        if self.cleaned_data['nom_client']:
            alarmes = alarmes.filter(
                (Q(installation__date_fin_abo__isnull=True) | Q(installation__date_fin_abo__gt=date.today())) &
                Q(installation__client__nom__icontains=self.cleaned_data['nom_client'])
            )
        if self.cleaned_data['en_rep']:
            alarmes = alarmes.filter(en_rep_depuis__isnull=False)
        if self.cleaned_data['en_rev']:
            alarmes = Alarme.objects.a_reviser().filter(id__in=alarmes.values_list('id'))
        if self.cleaned_data.get('en_stock') == 'on':
            alarmes = Alarme.objects.non_installees().filter(id__in=alarmes.values_list('id'), en_rep_depuis__isnull=True)
        elif self.cleaned_data.get('en_stock') == 'off':
            alarmes = Alarme.objects.installees().filter(id__in=alarmes.values_list('id'))
        return alarmes


class MaterielForm(BootstrapMixin, ModelForm):
    class Meta(BootstrapMixin.Meta):
        model = Materiel
        fields = ['type_mat', 'no_ref', 'date_achat', 'prix_achat', 'fournisseur', 'chez_benevole']

    def __init__(self, instance=None, **kwargs):
        super().__init__(instance=instance, **kwargs)
        self.fields['type_mat'].widget.attrs['data-url'] = reverse('refresh-select', args=['TypeMateriel'])
        self.fields['chez_benevole'].queryset = Benevole.objects.par_domaine(
            'alarme', include=instance.chez_benevole if instance else None
        )


class MaterielFilterForm(BootstrapMixin, forms.Form):
    type_mat = forms.ModelChoiceField(queryset=TypeMateriel.objects.all().order_by('nom'), required=False)
    no_ref = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'N°', 'autocomplete': 'off', 'size': '3'}),
        required=False
    )
    nom_client = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'Nom client', 'autocomplete': 'off', 'size': '8'}),
        required=False
    )
    archived = forms.BooleanField(label="Archivés", required=False)

    def filter(self, materiels):
        if self.cleaned_data['archived']:
            materiels = materiels.filter(date_archive__isnull=False)
        if self.cleaned_data['type_mat']:
            materiels = materiels.filter(type_mat=self.cleaned_data['type_mat'])
        if self.cleaned_data['no_ref']:
            materiels = materiels.filter(no_ref__icontains=self.cleaned_data['no_ref'])
        if self.cleaned_data['nom_client']:
            materiels = materiels.filter(
                (Q(materielclient__date_fin_abo__isnull=True) | Q(materielclient__date_fin_abo__gt=date.today())) &
                Q(materielclient__client__nom__icontains=self.cleaned_data['nom_client'])
            )
        return materiels


class TypeMaterielForm(BootstrapMixin, ModelForm):
    class Meta:
        model = TypeMateriel
        fields = '__all__'
        widgets = {
            'abos': BSCheckboxSelectMultiple,
        }
